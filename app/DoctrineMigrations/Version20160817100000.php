<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160817100000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->addSql("
          ALTER TABLE  `face_detect_result` modify  `type` enum('login','register','lesson', 'testpaper') COLLATE utf8_unicode_ci NOT NULL;
        ");


        if (!$this->isFieldExist('face_detect_result', 'regRate')) {
            $this->addSql("
              ALTER TABLE  `face_detect_result` ADD  `regRate` INT( 11 ) NOT NULL DEFAULT  '0'  COMMENT  '识别度';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
