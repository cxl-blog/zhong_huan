<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160925103545 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            UPDATE user_cert_course set latestBuyTime = 0 where latestBuyTime is null; 
            ALTER TABLE  `user_cert_course` CHANGE  `latestBuyTime` `latestBuyTime`  int(11) unsigned NULL default '0' COMMENT '最近购买时间';
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

}
