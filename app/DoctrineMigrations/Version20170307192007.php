<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170307192007 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('area_certification', 'showTestpaperAnswer')) {
            $this->addSql("
                ALTER TABLE `area_certification` ADD `showTestpaperAnswer` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否显示考试答案' AFTER `maxLearningSeconds`;
            ");
        }

        if (!$this->isFieldExist('area', 'isClassInteraction')) {
            $this->addSql("
                ALTER TABLE `area` ADD `isClassInteraction` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否打开课时互动' AFTER `isTestpaperFaceable`;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
