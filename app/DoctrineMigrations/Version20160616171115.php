<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160616171115 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('course_lesson_view', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `course_lesson_view` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('course_lesson_learn', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `course_lesson_learn` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('face_detect_result', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `face_detect_result` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('question_marker_result', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `question_marker_result` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('face_marker_result', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `face_marker_result` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('testpaper_result', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `testpaper_result` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }

        if (!$this->isFieldExist('course_favorite', 'currentLoopTime')) {
            $this->addSql("ALTER TABLE  `course_favorite` ADD  `currentLoopTime` INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次';");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
