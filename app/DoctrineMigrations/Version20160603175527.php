<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160603175527 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isTableExist('certification_history')) {
            $this->addSql("
            CREATE TABLE `certification_history` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `areaCode` varchar(255) DEFAULT '' COMMENT '行政区域',
              `certificationId` int(11) unsigned DEFAULT NULL COMMENT '证书id',
              `learnMode` int(1) unsigned DEFAULT '0' COMMENT '学制',
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;"
            );
        }
        if (!$this->isFieldExist('user_certification', 'certificationHistoryId_1')) {
            $this->addSql("ALTER TABLE  `user_certification` ADD `certificationHistoryId_1` int(11) DEFAULT NULL;");
        }
        if (!$this->isFieldExist('user_certification', 'certificationHistoryId_2')) {
            $this->addSql("ALTER TABLE  `user_certification` ADD `certificationHistoryId_2` int(11) DEFAULT NULL;");
        }
        if (!$this->isFieldExist('user_certification', 'certificationHistoryId_3')) {
            $this->addSql("ALTER TABLE  `user_certification` ADD `certificationHistoryId_3` int(11) DEFAULT NULL;");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
