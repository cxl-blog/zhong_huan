<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160617153454 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if ($this->isFieldExist('certification_history', 'userId')) {
            $this->addSql("ALTER TABLE `certification_history` DROP `userId`");
        }

        if ($this->isFieldExist('certification_history', 'courseId')) {
            $this->addSql("ALTER TABLE `certification_history` DROP `courseId`");
        }

        if ($this->isFieldExist('user_certification', 'certificationHistoryId')) {
            $this->addSql("ALTER TABLE `user_certification` DROP `certificationHistoryId`");
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
