<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160627120936 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('user_certification_history')) {
            $this->addSql("
               CREATE TABLE `user_certification_history` (
                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                  `certificationHistoryId` int(11) unsigned DEFAULT NULL COMMENT '证书历史Id',
                  `userCertificationId` int(11) unsigned DEFAULT NULL COMMENT '用户证书id',
                  `currentLoopTime` int(11) unsigned DEFAULT '1' COMMENT '当前循环购买次数, 1表示第一次',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
            ");
        }

        if ($this->isFieldExist('user_certification', 'certificationHistoryId_1')) {
            $this->addSql("ALTER TABLE `user_certification` DROP `certificationHistoryId_1`");
        }

        if ($this->isFieldExist('user_certification', 'certificationHistoryId_2')) {
            $this->addSql("ALTER TABLE `user_certification` DROP `certificationHistoryId_2`");
        }

        if ($this->isFieldExist('user_certification', 'certificationHistoryId_3')) {
            $this->addSql("ALTER TABLE `user_certification` DROP `certificationHistoryId_3`");
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
