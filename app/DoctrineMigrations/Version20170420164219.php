<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170420164219 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $latestStatusList = $this->connection->fetchAll('
            select userId, courseId, currentLoopTime, max(createdTime) as createdTime, type from status group by userId, courseId, currentLoopTime;
        ');

        foreach ($latestStatusList as $latestStatus) {
            $userId = $latestStatus['userId'];
            $courseId = $latestStatus['courseId'];
            $type = $latestStatus['type'];
            $currentLoopTime = $latestStatus['currentLoopTime'];
            $createdTime = $latestStatus['createdTime'];
            $updatedTime = $latestStatus['createdTime'];
            $this->addSql("
                insert into latest_status (userId, courseId, type, currentLoopTime, createdTime, updatedTime)
                values ('{$userId}', '{$courseId}', '{$type}', '{$currentLoopTime}', '{$createdTime}', '{$updatedTime}');
            ");
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
