<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161127171024 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("
            update user_cert_course set learnModeMonths = 12 where learnMode = 0;
            update user_cert_course set learnModeMonths = years * 12 where learnMode = 1;
            update user_cert_course set learnModeMonths = 6 where learnMode = 2;
            update user_cert_course set certificationType =  concat(learnModeMonths, '个月') ;
        ");

        $this->addSql("
            update area_certification set learnModeMonths = 12 where learnMode = 0;
            update area_certification set learnModeMonths = years * 12 where learnMode = 1;
            update area_certification set learnModeMonths = 6 where learnMode = 2;
        ");

        $this->addSql("
            update course set learnModeMonths = 12 where learnMode = 0;
            update course set learnModeMonths = 6 where learnMode = 2;
        ");

        $sql = 'select course.areaCode, course.certificationId, area_certification.years'
                . ' from course inner join area_certification' 
                . ' on course.areaCode = area_certification.areaCode'
                . ' and course.certificationId = area_certification.certificationId';
        $result = $this->connection->fetchAll($sql);

        foreach ($result as $key => $value) {
            $this->connection->update(
                'course', 
                array('learnModeMonths' => $value['years'] * 12),
                array(
                    'areaCode' => $value['areaCode'], 
                    'certificationId' => $value['certificationId'],
                    'learnMode' => 1
                )
            );
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
