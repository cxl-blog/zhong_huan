<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160731151322 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        /**
        $this->addSql("
            DROP INDEX index_startTimeNum ON user_cert_course;
            DROP INDEX index_userId_startTimeNum_courseId ON user_cert_course;
            DROP INDEX index_userId_role ON branch_school_user;
            DROP INDEX index_userId_role_deadlineNotified_deadline ON course_member;
            DROP INDEX index_promoted_roles_locked ON user;
            DROP INDEX index_code ON area;
        ");
*/
        $this->addSql("
            CREATE INDEX index_startTimeNum ON user_cert_course (startTimeNum);
            CREATE INDEX index_userId_startTimeNum_courseId ON user_cert_course (userId, startTimeNum, courseId);
            CREATE INDEX index_userId_role ON branch_school_user (userId, roles);
            CREATE INDEX index_userId_role_deadlineNotified_deadline ON course_member (userId, role, deadlineNotified, deadline);
            CREATE INDEX index_promoted_roles_locked ON user (promoted, roles, locked);
            CREATE INDEX index_code ON area (code);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
