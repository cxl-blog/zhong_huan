<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160531154618 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if ($this->isTableExist('user_certification_states')) {
            $this->addSql("
                ALTER TABLE  `user_certification_states` ADD  `learnStartTime` INT( 11 ) UNSIGNED DEFAULT NULL AFTER  `verifiedMobile` ;
                ALTER TABLE  `user_certification_states` ADD  `learnEndTime` INT( 11 ) UNSIGNED DEFAULT NULL AFTER  `learnStartTime` ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
