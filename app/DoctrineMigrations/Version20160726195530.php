<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160726195530 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        
        if (!$this->isFieldExist('course_member', 'isAllLessonsFinished')) {
            $this->addSql("
              ALTER TABLE  `course_member` ADD  `isAllLessonsFinished` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT  '课时是否都已完成' AFTER  `isLearned` ;
              ALTER TABLE  `course_member` ADD  `learnedHours` INT( 11 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT  '已完成的学分' AFTER  `learnedNum` ;
            ");
        }

        if (!$this->isFieldExist('course', 'totalHours')) {
            $this->addSql("
              ALTER TABLE  `course` ADD  `totalHours` INT( 11 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT  '总共需要完成的学分' AFTER  `lessonNum` ;
              ALTER TABLE  `course` ADD  `hasTestpaper` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0' COMMENT  '是否有试卷' ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
