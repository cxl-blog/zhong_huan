<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160525203032 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isTableExist('user_certification_states')) {
            $this->addSql("
            CREATE TABLE IF NOT EXISTS `user_certification_states` (
              `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
              `userId` int(11) unsigned NOT NULL,
              `action` enum('registered','buyed','finished') NOT NULL,
              `areaCode` varchar(255) NOT NULL,
              `certificationId` int(11) unsigned NOT NULL,
              `courseId` int(11) unsigned NOT NULL,
              `idcard` varchar(24) NOT NULL,
              `verifiedMobile` varchar(32) NOT NULL,
              `createdTime` int(11) unsigned NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
            );
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
