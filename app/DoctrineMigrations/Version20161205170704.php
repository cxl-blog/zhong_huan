<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205170704 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('user_learn_time')) {
            $this->addSql("
              CREATE TABLE IF NOT EXISTS `user_learn_time` (
                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '学员每日学习限制表的主键',
                `userId` int(11) NOT NULL,
                `learnedSeconds` int(11) NOT NULL default 0 COMMENT '学员已学的时间, 转化为秒',
                `learnedMinutes` int(10) NOT NULL default 0 COMMENT '学员已学的时间, 转化为分钟',
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
