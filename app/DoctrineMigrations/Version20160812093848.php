<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160812093848 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('area', 'ftpUrl')) {
            $this->addSql("
                ALTER TABLE  `area` ADD  `ftpUrl` varchar(50)  NOT NULL   COMMENT  'ftp地址';
            ");
        }

        if (!$this->isFieldExist('area', 'ftpName')) {
            $this->addSql("
                ALTER TABLE  `area` ADD  `ftpName` varchar(20)  NOT NULL   COMMENT  'ftp用户名';
            ");
        }

        if (!$this->isFieldExist('area', 'ftpPassword')) {
            $this->addSql("
                ALTER TABLE  `area` ADD  `ftpPassword` varchar(20)  NOT NULL   COMMENT  'ftp密码';
            ");
        }

        if (!$this->isTableExist('sync_ftp_file')) {
            $this->addSql("
              CREATE TABLE IF NOT EXISTS `sync_ftp_file` (
                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                `areaCode` varchar(50) NOT NULL COMMENT '区域码',
                `sourcefile` varchar(150) NOT NULL COMMENT '源文件',
                `remotefile` varchar(150)  NOT NULL COMMENT '目标文件',
                `type` varchar(20)  NOT NULL COMMENT '图片来源类型',
                `createdTime` int(11)  NOT NULL COMMENT '创建时间',
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
        }

        $this->addSql("DELETE FROM `crontab_job` where name='SyncDataFtpFileJob';");

        $this->addSql("INSERT INTO `crontab_job` (`name`, `cycle`, `cycleTime`, `jobClass`, `jobParams`, `executing`, `nextExcutedTime`, `latestExecutedTime`, `creatorId`, `createdTime`) 
            VALUES
        ('SyncDataFtpFileJob', 'everyday', '04:00:00', 'Custom\\\\Service\\\\Sync\\\\Job\\\\SyncDataFtpFileJob', '', 0, 1440528069, 1440524469, 0, 0);");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
