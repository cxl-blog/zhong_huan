<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160720180005 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('user', 'base64FaceImgPath')) {
            $this->addSql("
              ALTER TABLE  `user` ADD  `base64FaceImgPath` varchar(100)  DEFAULT  NULL COMMENT  '同步数据时传过来的头像值路径'  ;
            ");
        }

        if (!$this->isFieldExist('user', 'stuId')) {
            $this->addSql("
              ALTER TABLE  `user` ADD  `stuId` varchar(50)  DEFAULT  NULL COMMENT  '学员ID'  ;
            ");
        }

        if (!$this->isFieldExist('user_approval', 'faceImgPath')) {
            $this->addSql("
              ALTER TABLE  `user_approval` ADD  `faceImgPath` varchar(100)  DEFAULT  NULL COMMENT  '正面照base64文件'  ;
            ");
        }

        if (!$this->isFieldExist('user_approval', 'backImgPath')) {
            $this->addSql("
              ALTER TABLE  `user_approval` ADD  `backImgPath` varchar(100)  DEFAULT  NULL COMMENT  '反面照base64文件'  ;
            ");
        }

        if (!$this->isFieldExist('user_approval', 'jobsSeniorityCardImgPath')) {
            $this->addSql("
              ALTER TABLE  `user_approval` ADD  `jobsSeniorityCardImgPath` varchar(100)  DEFAULT  NULL COMMENT  '从业资格照base64文件'  ;
            ");
        }

        if (!$this->isFieldExist('user_approval', 'driverLicenseImgPath')) {
            $this->addSql("
              ALTER TABLE  `user_approval` ADD  `driverLicenseImgPath` varchar(100)  DEFAULT  NULL COMMENT  '驾驶照base64文件'  ;
            ");
        }

        if (!$this->isFieldExist('user_approval', 'otherImgPath')) {
            $this->addSql("
              ALTER TABLE  `user_approval` ADD  `otherImgPath` varchar(50)  DEFAULT  NULL COMMENT  '其他照base64文件'  ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
