<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20161126133831 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('course', 'partIndex')) {
            $this->addSql("
              ALTER TABLE  `course` ADD  `partIndex` INT UNSIGNED NOT NULL DEFAULT  1 COMMENT '课时部分, 1表示第一部分' after isUpperPart;
              update course set partIndex = isUpperPart + 1;
              ALTER TABLE  `course` drop column isUpperPart;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
