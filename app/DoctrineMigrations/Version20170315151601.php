<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170315151601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('bind_mobile_sms')) {
            $this->addSql("
                CREATE TABLE  `bind_mobile_sms` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                `mobile` varchar(11) NOT NULL COMMENT '手机',
                `code` varchar(10) NOT NULL COMMENT '验证码',
                `userId` int(11) NOT NULL COMMENT '创建者Id',
                `createdTime` int(10) unsigned NOT NULL COMMENT '记录创建时间',
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
