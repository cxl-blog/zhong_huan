<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160720193722 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('offline_finished_course')) {
            $this->addSql("
                CREATE TABLE `offline_finished_course` (
                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `dId` varchar(64) NOT NULL COMMENT  '继续教育id, 唯一, 数据同步时, 如果dId 已存在, 则视为更新, 否则, 视为新增',
                    `courseId` int(10) NOT NULL,
                    `userId` int(10) NOT NULL,
                    `currentLoopTime`  INT NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次',
                    `createdTime` int(11) unsigned NOT NULL  COMMENT '创建时间',
                    `updateTime` int(10) unsigned NOT NULL DEFAULT '0'  COMMENT '最后更新时间',
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `dId` (`dId`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
