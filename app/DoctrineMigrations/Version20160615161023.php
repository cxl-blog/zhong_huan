<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160615161023 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('user_approval', 'jobsSeniorityCardImg')) {
            $this->addSql("ALTER TABLE  `user_approval` ADD `jobsSeniorityCardImg` varchar(500) DEFAULT '' COMMENT '从业资格证图片';");
        }
        if (!$this->isFieldExist('user_approval', 'driverLicenseImg')) {
            $this->addSql("ALTER TABLE  `user_approval` ADD `driverLicenseImg` varchar(500) DEFAULT '' COMMENT '驾驶证图片';");
        }
        if (!$this->isFieldExist('user_approval', 'otherImg')) {
            $this->addSql("ALTER TABLE  `user_approval` ADD `otherImg` varchar(500) DEFAULT '' COMMENT '其他图片';");
        }

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
