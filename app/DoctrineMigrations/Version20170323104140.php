<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170323104140 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
            CREATE TABLE IF NOT EXISTS `course_lesson_view_last` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `courseId` int(10) NOT NULL,
            `lessonId` int(10) NOT NULL,
            `userId` int(10) NOT NULL,
            `currentLoopTime` int(10) NOT NULL DEFAULT '1' ,
            `createdTime` int(10) unsigned NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
        ");

        $this->addSql("ALTER TABLE `course_lesson_view_last` ADD INDEX user_lesson_time ( `userId`, `lessonId`, `currentLoopTime` )");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
