<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160419095507 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('area', 'faceable')) {
            $this->addSql("ALTER TABLE `area` ADD `faceable` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启人脸识别';");
        }

        if (!$this->isFieldExist('area', 'schoolId')) {
            $this->addSql("ALTER TABLE `area` ADD `schoolId` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '关联分校ID';");
        }

        if(!$this->isTableExist('area_certification')){
            $this->addSql("
                CREATE TABLE IF NOT EXISTS `area_certification` (
                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `areaCode` varchar(255) NOT NULL COMMENT '行政区域码',
                    `certificationId` int(10) unsigned NOT NULL COMMENT '证书ID',
                    `learnMode` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '学制',
                    `relearn` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否补学',
                    `createdTime` int(10) unsigned NOT NULL COMMENT '创建时间',
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `areaCode` (`areaCode`,`certificationId`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
