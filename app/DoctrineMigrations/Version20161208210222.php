<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161208210222 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $sql = 'select userId, certificationId from user_certification';
        $result = $this->connection->fetchAll($sql);

        $this->connection->executeQuery('delete from user_learn_time');
        foreach ($result as $userCert) {
            $this->connection->insert(
                'user_learn_time', 
                array(
                    'userId' => $userCert['userId'],
                    'certificationId' => $userCert['certificationId']
                )
            );
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
