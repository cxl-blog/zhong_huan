<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170419141853 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
            CREATE TABLE `latest_status` (
                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '同status中的id',
                `userId` int(10) unsigned NOT NULL COMMENT '动态发布的人',
                `courseId` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '课程Id',
                `type` varchar(64) NOT NULL default '' COMMENT '动态类型',
                `currentLoopTime` INT(11) NOT NULL DEFAULT  '1',
                `createdTime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态发布时间',
                `updatedTime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '动态修改时间',
                PRIMARY KEY (`id`),
                KEY `userId` (`userId`),
                KEY `createdTime` (`createdTime`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
