<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160723210025 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('certification_cache')) {
            $this->addSql("
                CREATE TABLE `certification_cache` (
                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `cacheKey` varchar(255) NOT NULL COMMENT  '',
                    `userId` int(10) unsigned  NULL  default null COMMENT  '',
                    `value` text NOT NULL,
                    `createdTime` int(11) unsigned NOT NULL  COMMENT '创建时间',
                    PRIMARY KEY (`id`),
                    INDEX index_cacheKey (`cacheKey`),
                    INDEX index_userId (`userId`),
                    INDEX index_createdTime (`createdTime`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
