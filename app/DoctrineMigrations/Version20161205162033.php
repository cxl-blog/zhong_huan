<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205162033 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('area_certification', 'maxLearningMinutes')) {
            $this->addSql("
               ALTER TABLE  `area_certification` ADD  `maxLearningMinutes` INT UNSIGNED NOT NULL DEFAULT  1440 COMMENT '每天的最大学习时间(转换为分钟)';
               ALTER TABLE  `area_certification` ADD  `maxLearningSeconds` INT UNSIGNED NOT NULL DEFAULT  86400 COMMENT '每天的最大学习时间(转换为秒)';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
