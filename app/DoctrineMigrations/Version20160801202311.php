<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160801202311 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
            CREATE INDEX index_courseId ON course_lesson (courseId);
            CREATE INDEX index_courseId ON course_chapter (courseId);
            CREATE INDEX index_name ON theme_config (name);
            CREATE INDEX index_code ON category_group (code);
            CREATE INDEX index_groupId_parentId ON category (groupId, parentId);
            CREATE INDEX index_parentId ON category (parentId);
            CREATE INDEX index_courseId_role ON course_member (courseId, role);
            CREATE INDEX index_userId_certificationId ON user_certification (userId, certificationId);
            CREATE INDEX index_userId ON course_favorite (userId);
            CREATE INDEX index_target ON testpaper (target);
            CREATE INDEX index_targetType_targetId_status ON orders (targetType, targetId, status);
            CREATE INDEX index_lessonId ON exercise (lessonId);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
