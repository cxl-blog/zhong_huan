<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160818100000 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('revoked_user_certification')) {
            $this->addSql("
              CREATE TABLE IF NOT EXISTS `revoked_user_certification` (
                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                `userCertificationId` int(11) NOT NULL,
                `userId` int(11) NOT NULL,
                `certificationId` int(11) NOT NULL,
                `awardTime` int(10) NOT NULL  COMMENT '颁发时间',
                `dID` varchar(64)  DEFAULT  NULL COMMENT  '初学ID',
                `reason` varchar(64)  DEFAULT  NULL COMMENT  '注销原因',
                `revokedTime` int(11)  DEFAULT  NULL COMMENT  '操作时间' ,
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
