<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160822111825 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('revoked_user_certification', 'startLoopTime')) {

            $this->addSql("
              ALTER TABLE  `revoked_user_certification` ADD  `startLoopTime` int DEFAULT 1 COMMENT '开始时是第几次循环';
              CREATE INDEX index_userId_certificationId ON user_cert_course (userId, certificationId);
              CREATE INDEX index_userId ON revoked_user_certification (userId);
            ");
        }

        if (!$this->isFieldExist('revoked_user_certification', 'endLoopTime')) {
            $this->addSql("
              ALTER TABLE  `revoked_user_certification` ADD  `endLoopTime` int DEFAULT 0 COMMENT '吊销时是第几次循环';
            ");
        }

        if (!$this->isFieldExist('user_certification', 'startLoopTime')) {
            $this->addSql("
              ALTER TABLE  `user_certification` ADD  `startLoopTime` int DEFAULT 1 COMMENT '开始时是第几次循环';
            ");
        }

        if (!$this->isFieldExist('user_certification', 'endLoopTime')) {
            $this->addSql("
              ALTER TABLE  `user_certification` ADD  `endLoopTime` int DEFAULT 0 COMMENT '吊销时是第几次循环';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
