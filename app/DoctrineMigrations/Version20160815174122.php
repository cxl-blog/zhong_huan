<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160815174122 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('area', 'isTestpaperFaceable')) {
            $this->addSql("
              ALTER TABLE  `area` ADD  `isTestpaperFaceable` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT  '0'  COMMENT  '考试是否开启人脸识别';
            ");
        }

        if (!$this->isFieldExist('area', 'maxSnapNum')) {
            $this->addSql("
              ALTER TABLE  `area` ADD  `maxSnapNum` INT( 11 ) UNSIGNED NOT NULL DEFAULT  '0'  COMMENT  '考试过程中最大拍摄数量, 不包括开始及结束时拍的照片';
            ");
        }

        if (!$this->isFieldExist('area', 'snapInterval')) {
            $this->addSql("
              ALTER TABLE  `area` ADD  `snapInterval` INT(1) UNSIGNED NOT NULL DEFAULT  '0'  COMMENT  '每隔多少秒进行拍照';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
