<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160620205601 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
// this up() migration is auto-generated, please modify it to your needs
        if ($this->isFieldExist('course_lesson_learn', 'currentLoopTime')) {
            $this->addSql("
                ALTER TABLE course_lesson_learn DROP INDEX userId_lessonId;
                ALTER TABLE course_lesson_learn ADD UNIQUE KEY  `userId_lessonId_currentLoopTime` (  `userId` , `lessonId` ,  `currentLoopTime` ) ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
