<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160724214622 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isTableExist('user_cert_course')) {
            $this->addSql("
              CREATE TABLE IF NOT EXISTS `user_cert_course` (
                `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                `areaCode` varchar(255)  NOT NULL COMMENT '行政区域',
                `userId` int(11) NOT NULL COMMENT '用户id',
                `courseId` int(11) NOT NULL COMMENT '课程id',
                `certificationId` int(11) NOT NULL COMMENT '证书id',
                `startTimeNum` int(11) NOT NULL COMMENT '学制起始时间',
                `deadlineNum` int(11) NOT NULL COMMENT '学制结束时间',
                `partIndex` int(10) unsigned NOT NULL COMMENT '第N课程（学制）',
                `years` int(10) unsigned NOT NULL DEFAULT '2' COMMENT '周期',
                `learnMode` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '学制',
                `awardTime` int(11) NOT NULL COMMENT '颁发时间',
                `isRevoked` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否吊销',
                `isCompletedOffline` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否离线结业',
                `status` enum('UNPAID','LEARNING','OVERTIME','COMPLETION') NOT NULL DEFAULT 'UNPAID' COMMENT '状态',
                `currentLoopTime` int(11) NULL DEFAULT 1 COMMENT  '当前循环购买次数, 1表示第一次',
                `relearn` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否支持补学',
                `certificationCourseCount` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '证书课程',
                `certificationStartPeriod` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '本周期内证书正常学习开始时间',
                `certificationEndPeriod` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '本周期内证书正常学习结束时间',
                `certificationName`  varchar(255) NOT NULL COMMENT '证书名称',
                `certificationType`  varchar(10) NOT NULL COMMENT '证书学制中文名',
                `latestBuyTime`  int(11) unsigned NULL COMMENT '最近购买时间',
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
