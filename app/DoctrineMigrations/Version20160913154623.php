<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160913154623 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE  `orders` CHANGE  `payment`  `payment` ENUM('none','alipay','tenpay','coin','wxpay','wxpay_mobile','heepay','quickpay','iosiap') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
