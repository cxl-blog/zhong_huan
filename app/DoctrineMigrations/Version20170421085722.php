<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170421085722 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('course_member', 'isAtLeastOneLessonFinished')) {
            $this->addSql("
                ALTER TABLE `course_member` ADD `isAtLeastOneLessonFinished` INT(11) NOT NULL DEFAULT  0 comment '至少一个课时已学完, 包括考试课时' AFTER `isAllLessonsFinished`;
            ");
        }
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
