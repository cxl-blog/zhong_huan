<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170110162712 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // 修改 604800 时, 需要同时修改 LogPartitionJob内的属性
        $defaultTime = time() + 604800;
        $this->addSql("
            DROP TABLE  `log`;

            CREATE TABLE `log` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `userId` int(10) unsigned NOT NULL DEFAULT '0',
              `module` varchar(32) NOT NULL,
              `action` varchar(32) NOT NULL,
              `message` text NOT NULL,
              `data` text,
              `ip` varchar(255) NOT NULL,
              `createdTime` int(10) unsigned NOT NULL,
              `level` char(10) NOT NULL,
              PRIMARY KEY (`id`, `createdTime`),
              KEY `userId` (`userId`)
            ) 
            ENGINE=InnoDB  DEFAULT CHARSET=utf8
            PARTITION BY RANGE( createdTime ) (
                PARTITION p{$defaultTime} VALUES LESS THAN ({$defaultTime})
            );

            INSERT INTO `crontab_job` (`name`, `cycle`, `cycleTime`, `jobClass`, `jobParams`, `executing`, `nextExcutedTime`, `latestExecutedTime`, `creatorId`, `createdTime`)  VALUES
            ('LogPartitionJob', 'everyday', '02:00', 'Custom\\\\Service\\\\Log\\\\Job\\\\LogPartitionJob', '', 0, 1440528069, 1440524469, 0, 0);
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isTableExist($table)
    {
        $sql = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
