<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160905215132 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM `crontab_job` where name='KeepAliveJob';");

        $this->addSql("INSERT INTO `crontab_job` (`name`, `cycle`, `cycleTime`, `jobClass`, `jobParams`, `executing`, `nextExcutedTime`, `latestExecutedTime`, `creatorId`, `createdTime`) 
            VALUES
        ('KeepAliveJob', 'everyday', '00:00', 'Custom\\\\Service\\\\JobKeeper\\\\KeepAliveJob', '', 0, 1440528069, 0, 0, 0),
        ('KeepAliveJob', 'everyday', '12:00', 'Custom\\\\Service\\\\JobKeeper\\\\KeepAliveJob', '', 0, 1440528069, 0, 0, 0);");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
