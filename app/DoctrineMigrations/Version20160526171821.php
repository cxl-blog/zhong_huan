<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160526171821 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if ($this->isTableExist('user_certification_states')) {
            $this->addSql("
                ALTER TABLE  `user_certification_states` ADD  `nickname` VARCHAR( 64 ) NOT NULL AFTER  `userId` ;
                ALTER TABLE  `user_certification_states` ADD  `courseTitle` VARCHAR( 255 ) NOT NULL AFTER  `userId` ;
                ALTER TABLE  `user_certification_states` CHANGE  `action`  `action` TINYTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
