<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160517204228 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isTableExist('face_detect_result')) {
            $this->addSql("
                CREATE TABLE IF NOT EXISTS `face_detect_result` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `userId` int(11) unsigned NOT NULL,
                `type` enum('login','register','lesson') COLLATE utf8_unicode_ci NOT NULL,
                `courseId` int(11) unsigned DEFAULT NULL,
                `lessonId` int(11) unsigned DEFAULT NULL,
                `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `fileUrl` text COLLATE utf8_unicode_ci NOT NULL,
                `result` enum('success','fail') COLLATE utf8_unicode_ci DEFAULT NULL,
                `createdTime` int(11) unsigned NOT NULL,
                PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
            ");
        }

        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
