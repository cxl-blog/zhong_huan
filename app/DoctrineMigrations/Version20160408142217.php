<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160408142217 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
// this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('user', 'toLearn')) {
            $this->addSql("ALTER TABLE `user` add `toLearn` TINYINT( 1 ) NOT NULL DEFAULT '0';");
        }

        if (!$this->isFieldExist('user', 'areaCode')) {
            $this->addSql("ALTER TABLE `user` add `areaCode` VARCHAR( 255 ) NOT NULL DEFAULT '';");
        }

        if (!$this->isTableExist('certification')) {
            $this->addSql("
                CREATE TABLE IF NOT EXISTS `certification` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `category` varchar(255) NOT NULL,
                     PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
                ");
        }

        if (!$this->isTableExist('area')) {
            $this->addSql("
                CREATE TABLE IF NOT EXISTS `area` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `code` varchar(255) DEFAULT NULL,
                    `name` varchar(255) DEFAULT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;
                ");
        }

        if (!$this->isTableExist('user_certification')) {
            $this->addSql("
                CREATE TABLE IF NOT EXISTS `user_certification` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `userId` int(11) NOT NULL,
                    `certificationId` int(11) NOT NULL,
                    `awardTime` int(10) NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;
                ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }

    protected function isTableExist($table)
    {
        $sql    = "SHOW TABLES LIKE '{$table}'";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
