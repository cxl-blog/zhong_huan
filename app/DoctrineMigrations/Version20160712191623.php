<?php

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160712191623 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
// this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('area', 'ip')) {
            $this->addSql("
               ALTER TABLE  `area` ADD  `ip` varchar(50)  DEFAULT  '' COMMENT 'ip端口';
            ");
        }

        if (!$this->isFieldExist('area', 'createdTime')) {
            $this->addSql("
               ALTER TABLE  `area` ADD  `createdTime` int(10)  DEFAULT null COMMENT '创建时间';
            ");
        }

        if (!$this->isFieldExist('area', 'updatedTime')) {
            $this->addSql("
               ALTER TABLE  `area` ADD  `updatedTime` int(10)  DEFAULT null COMMENT '创建时间';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
