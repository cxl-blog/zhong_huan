<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170421150051 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $updatedCourseMember = $this->connection->fetchAll('
            select userId, courseId, currentLoopTime from course_member where isAllLessonsFinished = 0 and isTestpaperPassed = 0;
        ');

        if (!empty($updatedCourseMember)) {
            foreach ($updatedCourseMember as $courseMember) {
                $userId = $courseMember['userId'];
                $courseId = $courseMember['courseId'];
                $currentLoopTime = $courseMember['currentLoopTime'];
                $learns = $this->connection->fetchAll("
                    select userId, courseId, lessonId, currentLoopTime from course_lesson_learn where userId = {$userId} and courseId = {$courseId} and currentLoopTime = {$currentLoopTime} and status = 'finished';
                ");

                if (!empty($learns)) {
                    foreach ($learns as $learn) {
                        $lessonId = $learn['lessonId'];
                        $nonTestpaperLessonCount = $this->connection->fetchAssoc("
                            select count(*) as count from course_lesson where id = {$lessonId} and type != 'testpaper';
                        ");
                        if ($nonTestpaperLessonCount['count'] != 0) {
                            $this->addSql("
                                update course_member set isAtLeastOneLessonFinished = 1 where userId = {$userId} and courseId = {$courseId} and currentLoopTime = {$currentLoopTime};
                            ");
                            break;
                        }
                    }
                }
            }
        }

        $this->addSql("
            update course_member set isAtLeastOneLessonFinished = 1 where isAllLessonsFinished = 1 or isTestpaperPassed = 1;
        ");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
