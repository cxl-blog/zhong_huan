<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161122170327 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        if (!$this->isFieldExist('user_cert_course', 'months')) {
            $this->addSql("
                ALTER TABLE  `user_cert_course` modify  `years` INT UNSIGNED NULL DEFAULT  NULL COMMENT '证书周期1-12年';
                ALTER TABLE  `user_cert_course` ADD  `months` INT UNSIGNED NOT NULL DEFAULT  '2' COMMENT '证书周期1-120月' after years;
                update user_cert_course set months = years * 12;

                ALTER TABLE  `user_cert_course` modify  `learnMode` INT unsigned NULL DEFAULT '0' COMMENT '学制';
                ALTER TABLE  `user_cert_course` ADD  `learnModeMonths` INT UNSIGNED NOT NULL DEFAULT  '2' COMMENT '证书学制1-120月' after learnMode;
                update user_cert_course set learnMode = learnMode - 1; -- 升级时, 从枚举变为数字, 会导致 第n个枚举变为n
            ");
        }

        if (!$this->isFieldExist('area_certification', 'months')) {
            $this->addSql("
                ALTER TABLE  `area_certification` modify  `years` INT UNSIGNED NULL DEFAULT  NULL COMMENT '证书周期1-12年';
                ALTER TABLE  `area_certification` ADD  `months` INT UNSIGNED NOT NULL DEFAULT  '2' COMMENT '证书周期1-120月' after years;
                update area_certification set months = years * 12;

                ALTER TABLE  `area_certification` modify  `learnMode` INT unsigned NULL DEFAULT '0' COMMENT '学制';
                ALTER TABLE  `area_certification` ADD  `learnModeMonths` INT UNSIGNED NOT NULL DEFAULT  '2' COMMENT '证书学制1-120月' after learnMode;
                update area_certification set learnMode = learnMode - 1; -- 升级时, 从枚举变为数字, 会导致 第n个枚举变为n
            ");
        }

        if (!$this->isFieldExist('course', 'learnModeMonths')) {
            $this->addSql("
                ALTER TABLE  `course` modify  `learnMode` INT unsigned NULL DEFAULT '0' COMMENT '学制';
                ALTER TABLE  `course` ADD  `learnModeMonths` INT UNSIGNED NOT NULL DEFAULT  '2' COMMENT '证书学制1-120月' after learnMode;
                update course set learnMode = learnMode - 1; -- 升级时, 从枚举变为数字, 会导致 第n个枚举变为n
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
