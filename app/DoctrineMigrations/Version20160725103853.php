<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160725103853 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('user_certification', 'dID')) {
            $this->addSql("
              ALTER TABLE  `user_certification` ADD  `dID` varchar(64)  DEFAULT  NULL COMMENT  '初学ID'  ;
            ");
        }

        if (!$this->isFieldExist('user_certification', 'reason')) {
            $this->addSql("
              ALTER TABLE  `user_certification` ADD  `reason` varchar(64)  DEFAULT  NULL COMMENT  '注销原因'  ;
            ");
        }

        if (!$this->isFieldExist('user_certification', 'revokedTime')) {
            $this->addSql("
              ALTER TABLE  `user_certification` ADD  `revokedTime` int(11)  DEFAULT  NULL COMMENT  '操作时间'  ;
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
