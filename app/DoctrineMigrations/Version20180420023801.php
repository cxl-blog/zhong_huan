<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420023801 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('user_certification', 'isFrozen')) {
            $this->addSql("
                ALTER TABLE `user_certification` ADD `isFrozen` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否冻结';
            ");
        }

        if (!$this->isFieldExist('user_certification', 'frozenReason')) {
            $this->addSql("
                ALTER TABLE `user_certification` ADD `frozenReason` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作原因';
            ");
        }

        if (!$this->isFieldExist('user_certification', 'frozenTime')) {
            $this->addSql("
                ALTER TABLE `user_certification` ADD `frozenTime` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '操作时间';
            ");
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
