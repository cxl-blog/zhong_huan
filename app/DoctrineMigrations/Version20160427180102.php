<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160427180102 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (!$this->isFieldExist('course', 'isUpperpart')) {
            $this->addSql("ALTER TABLE `course` ADD `isUpperpart` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是上半部分';");
        }
        
        if (!$this->isFieldExist('course', 'areaCode')) {

            $this->addSql("ALTER TABLE `course` ADD `areaCode` varchar(255)  NOT NULL default '' COMMENT '行政区域';");
        }
        if (!$this->isFieldExist('course', 'certificationId')) {
            $this->addSql("ALTER TABLE `course` ADD `certificationId` varchar(255)  NOT NULL default '' COMMENT '证书ID';");

        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
    protected function isFieldExist($table, $filedName)
    {
        $sql    = "DESCRIBE `{$table}` `{$filedName}`;";
        $result = $this->connection->fetchAssoc($sql);
        return empty($result) ? false : true;
    }
}
