    private function mockData()
    {
        $userCount            = 100;
        $idCards = $this->generateRandomIdCards($userCount);
        $startIndex = 20000;
        $finalSql             = '';
        $sqlUserCertification = 'INSERT INTO `user_certification` (`userId`, `certificationId`, `awardTime`) VALUES';
        $sqlUser              = 'INSERT INTO `user` (`id`, `email`, `password`, `salt`, `nickname`, `type`, `roles`, `approvalStatus`, `createdTime`, `toLearn`, `areaCode`) VALUES';
        $sqlUserProfile       = 'INSERT INTO `user_profile` (`id`, `truename`, `idcard`, `gender`) VALUES';
        for ($i = 1; $i <= $userCount; $i++) {
            $userId   = $startIndex + $i;
            $email    = 'mail@qq.com'.$userId;
            $userName = '黄明'.$userId;
            $areaCode = '00'.($userId % 4 + 1);
            $sqlUser .= "<br/>($userId, '$email', 'X/xM+MqC/aMfX8GdXuWw89Blim6PB4t3deBG2pdYzAU=', 'l2en8oo3jlcscwcssws88k44448ogw','$userName', 'web_email', '|ROLE_USER|','unapprove', 1457316452, 1, '$areaCode')";
            $sqlUserProfile .= "<br/>($userId, '$userName', '{$idCards[$i-1]}', 'male')";

            $certificationCount = $i % 6 + 1;
            for ($j = 1; $j <= $certificationCount; $j++) {
                $sqlUserCertification .= "<br/>($userId, $j, 1460689930)";
                if ($i < $userCount || $j < $certificationCount) {
                    $sqlUserCertification .= ',';
                }
            }

            if ($i < $userCount) {
                $sqlUser .= ',';
                $sqlUserProfile .= ',';
            }
        }

        $finalSql = $sqlUserCertification.';<br/><br/>'.$sqlUser.';<br/><br/>'.$sqlUserProfile.';<br/><br/>';
        echo ($finalSql);
    }