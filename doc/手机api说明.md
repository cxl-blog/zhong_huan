如何测试手机API
    1.) 访问 http://ip:port/urlSender
    2.) 登陆 url输入 http://127.0.0.1/mapi_v2/User/login, 文本域内, 输入 用户名, 密码
    3.) 将登陆后返回的token记录下来, 后面再访问其他api时, 额外输入 token: ...

人脸识别相关api  见 api\src\Topxia\Api\Resource\User.php, /api/config/routing.php 中可修改路由
    1.)  获取人脸检测状态
      post http://ip:port/api/user/{userId}/faceDetectStatus
      无额外参数  
      返回结果为 
      {
        isNeedFaceDetected: true, 
            //当行政区域需要支持人脸检测时,才允许人脸采集及检测
        isFaceCollected: true,  
            // 人脸是否已经采集过, 只有 isNeedFaceDetected = true 时,才需要采集
        faceImgPath: "/files/camera/570cdcf136098.png"
            // http://ip:port/{faceImgPath} 可直接通过url获取图片/
      }

    2.) 更新用户的参照照片
      post http://ip:port/api/user/{userId}/updateUserFaceImg
      额外参数为 base64ImgData
      返回结果
        true  -> 上传成功
        不是true -> 上传失败

    3.) 用户人脸检测
      post http://ip:port/api/user/{userId}/faceDetect
      额外参数为 base64ImgData
        如果是播放课程时人脸识别,  额外传 lessonViewId
        如果是考试时人脸识别, 额外传 currentLoopTime 和 testpaperLessonId
      返回结果
        true -> 识别成功
        false -> 识别失败

    4.) 检测token是否过期
      post http://ip:port/api/user/{userId}/validateToken
      参数 token
      返回结果
        true -> token有效
        false  或 {"code":500,"message":"API Token不不正确！"} -> token无效
    
注册登陆相关API
    1.)  原注册接口  (UserProcessorImpl->regist)
      post http://ip:port/mapi_v2/User/regist  
      参数 email， nickname,  phone, password, smsCode
      参数改为 idcard, phone, password, areaCode  (areaCode 不填表示用第一个行政区域)

    2.)  原登陆接口  (UserProcessorImpl->login)
      post http://ip:port/mapi_v2/User/login 
      {
        "_username":  "1234516",   
        "_password":  "123456",
        "_areaCode":  "001"
      }
      注意: _username 必须是  身份证号/手机号
      
    3.)  获取短信密码
      post http://ip:port/mapi_v2/User/sendPasswordBySms
      额外参数， idcard, phone  
      错误信息:
        {
          "error": {"name":"idcard_exist","message":"身份证号已注册或不存在"}
        }
        {
          "error": {"name":"idcard_exist","message":"手机已存在"}
        }
          
        返回结果 true 为成功

    4.) 原获取课程信息  (CourseProcessorImpl->getCourse)
      get http://ip:port/mapi_v2/Course/getCourse
      参数: courseId, currentLoopTime, token
      额外返回 课时id的数组
      {
        "course": {
          "id": "3",
          "title": "证书课程(上半部分)",
          ...
          "courseLessons": ["1", "2", "3"]   
            //新增courseLessons, 如果无课时, 则为 "courseLessons": []
                   ...
          "teachers": [{id: 1, ...} ...]
        }, 
        "userFavorited": false, ....
        "member": {"id":1, ....},
        "isSequentialLearning": false,   //是否闯关学习
        "maxLearningMinutes": "60",     //每天最多学习时间(转化为分)
        "maxLearningSeconds": "3600"  //每天最多学习时间(转化为秒)
      }

    5.) 原获取课时信息 (LessonProcessorImpl->getLesson)
      post http://ip:port/mapi_v2/Lesson/getLesson
      参数: courseId, lessonId, currentLoopTime, token, ignoreStartLesson
        其中 ignoreStartLesson = "true" 表示不生成学习记录 -- course_lesson_view 和 course_lesson_learn 都不生成, 不填, 则表示生成学习记录
      额外返回 该课时的弹人脸及上次播放的进度
      {
        "id": "126",
        "courseId": "168",
        "chapterId": "0",
        "number": "1",
        "mediaId": "1",
        ...
        "faceMarkers" : ['1', '3', 6],  //第几秒会弹脸
        "isSequentialLearning": false,
        "maxLearningMinutes": "60",     //每天最多学习时间
        "maxLearningSeconds": "3600",  //每天最多学习时间(转化为秒)
        "areaCode": "001",
        "areaName": "杭州",
        "categoryName": "出租车",
        "lastWatchTime": 1223455666
      }

    6.) 获取所有可学习的证书课程的课程课时信息(必须为云视频)
      post http://ip:port/mapi_v2/Lesson/getAllLearnableCertificationCourseVideoLessons
      返回结果
      [
        {
          "lessons": [
            {
              "id": "15",
              "courseId": "11",
              "chapterId": "0",
              "number": "1",
              "seq": "1",
              "free": "0",
              "status": "published",
              "title": "tt",
              "summary": "tt",
              "tags": null,
              "type": "text",
              "content": "",
              "giveCredit": "0",
              "requireCredit": "0",
              "mediaId": "0",
              "mediaSource": "",
              "mediaName": "",
              "mediaUri": "",
              "homeworkId": "0",
              "exerciseId": "0",
              "length": "",
              "materialNum": "0",
              "quizNum": "0",
              "learnedNum": "0",
              "viewedNum": "0",
              "startTime": "0",
              "endTime": "0",
              "memberNum": "0",
              "replayStatus": "ungenerated",
              "maxOnlineNum": "0",
              "liveProvider": "0",
              "userId": "1",
              "createdTime": "2016-05-20T12:41:04+08:00",
              "updatedTime": "1463719264",
              "suggestHours": "60.0",
              "copyId": "0",
              "testMode": "normal",
              "testStartTime": "0",
              "itemType": "lesson",
              "uploadFile": null
            }, {
              "id": "16",
              "courseId": "11",
              "chapterId": "0",
              "number": "2",
              "seq": "2",
              "free": "0",
              "status": "published",
              "title": "ttt",
              "summary": null,
              "tags": null,
              "type": "testpaper",
              "content": "",
              "giveCredit": "0",
              "requireCredit": "0",
              "mediaId": "6",
              "mediaSource": "",
              "mediaName": "",
              "mediaUri": null,
              "homeworkId": "0",
              "exerciseId": "0",
              length": "",
              "materialNum": "0",
              "quizNum": "0",
              "learnedNum": "0",
              "viewedNum": "0",
              "startTime": "0",
              "endTime": "0",
              "memberNum": "0",
              "replayStatus": "ungenerated",
              "maxOnlineNum": "0",
              "liveProvider": "0",
              "userId": "1",
              "createdTime": "2016-05-20T12:42:09+08:00",
              "updatedTime": "1463719329",
              "suggestHours": "30.0",
              "copyId": "0",
              "testMode": "normal",
              "testStartTime": "0",
              "itemType": "lesson",
              "uploadFile": null
            }
		      ],
          "course": {
            "id": "11",
		        "title": "兰州有试卷(货运上)",
		        "subtitle": "",
		        "status": "closed",
		        "buyable": "1",
		        "type": "certification",
		        "maxStudentNum": "0",
		        "price": "0.00",
		        "originPrice": "0.00",
		        "coinPrice": "0.00",
		        "originCoinPrice": "0.00",
		        "expiryDay": "0",
		        "showStudentNumType": "opened",
		        "serializeMode": "none",
		        "income": "0.00",
		        "lessonNum": "2",
		        "giveCredit": "0",
		        "rating": "0",
		        "ratingNum": "0",
		        "vipLevelId": "0",
		        "useInClassroom": "single",
		        "singleBuy": "1",
		        "categoryId": "0",
		        "tags": [],
		        "smallPicture": "http://192.168.200.27/assets/img/default/course.png?6.16.2",
		        "middlePicture": "http://192.168.200.27/assets/img/default/course.png?6.16.2",
		        "largePicture": "http://192.168.200.27/assets/img/default/course.png?6.16.2",
		        "about": "",
		        "goals": [],
		        "audiences": [],
		        "recommended": "0",
		        "recommendedSeq": "0",
		        "recommendedTime": "0",
		        "locationId": "0",
		        "parentId": "0",
		        "schoolId": "0",
		        "address": "",
		        "studentNum": "1",
		        "hitNum": "4",
		        "noteNum": "0",
		        "userId": "1",
		        "deadlineNotify": "none",
		        "daysOfNotifyBeforeDeadline": "0",
		        "watchLimit": "0",
		        "createdTime": "2016-05-20T12:40:44+08:00",
		        "updatedTime": "1464405952",
		        "freeStartTime": "0",
		        "freeEndTime": "0",
		        "discountId": "0",
		        "discount": "10.00",
		        "approval": "0",
		        "locked": "0",
		        "maxRate": "100",
		        "tryLookable": "0",
		        "tryLookTime": "0",
		        "partIndex": "0",
		        "areaCode": "003",
		        "certificationId": "2",
		        "versionStatus": "latest",
		        "teachers": [
		          {
		            "id": "1",
		            "nickname": "测试管理员",
		            "title": "dds",
		            "following": "0",
		            "follower": "0",
		            "avatar": "http://192.168.200.27/files/default/2016/05-17/1253382ad9ec137292.jpg?6.16.2"
		          }
            ],
		        "priceType": null,
		        "coinName": null
		      }
		    }
      ]
    7.) 创建证书课程订单
      get http://ip:port/mapi_v2/Order/createCertificationOrder
      参数: targetId, targetType, currentLoopTime, token

    8.) 原获取课时信息 (LessonProcessorImpl->getLessonStatus)
      get http://ip:port/mapi_v2/Lesson/getLessonStatus
      参数: courseId, lessonId, currentLoopTime, token
      返回
      {"learnStatus":"finished","hasMaterial":false}

    9.) 原收藏课程  (CourseProcessorImpl->favoriteCourse)
      get http://ip:port/mapi_v2/Course/favoriteCourse
      参数: courseId, currentLoopTime, token

    10.) 原解除收藏课程  (CourseProcessorImpl->unFavoriteCourse)
      get http://ip:port/mapi_v2/Course/unFavoriteCourse
      参数: courseId, currentLoopTime, token

    11.) 原课程退出(CourseProcessorImpl->unLearnCourse)
      get http://ip:port/mapi_v2/Course/unLearnCourse
      参数 courseId, currentLoopTime, token

    12.) 原获取被收藏的课程 (CourseProcessorImpl->getFavoriteCourse)
      get http://ip:port/mapi_v2/course/getFavoriteCourse

    13.) 原获取课时信息 (LessonProcessorImpl->getCourseLessons)
      post http://ip:port/mapi_v2/Lesson/getCourseLessons
      参数 currentLoopTime, courseId, token

    14.) 原完成试卷功能(TestpaperProcessorImpl->finishTestpaper)
      post http://ip:port/mapi_v2/Testpaper/finishTestpaper
      参数 currentLoopTime, id, token, usedTime //考试用时

    15.) 原参加考试获取试卷内容功能(TestpaperProcessorImpl->doTestpaper)
      post http://ip:port/mapi_v2/Testpaper/doTestpaper
      参数 currentLoopTime, testId, token, targetType, targetId
      返回的json中额外返回 人脸识别信息
      {
        ....
        "faceDetectStatus":{"isNeedFaceDetected":true,"maxSnapNum":"3","snapInterval":"2"}
      }
        
  
    16.) 原重新参加考试获取试卷内容功能(TestpaperProcessorImpl->reDoTestpaper)
      post http://ip:port/mapi_v2/Testpaper/reDoTestpaper
      参数 currentLoopTime, testId, token, targetType, targetId
      返回的json中额外返回 人脸识别信息
      {
        ....
        "faceDetectStatus":{"isNeedFaceDetected":true,"maxSnapNum":"3","snapInterval":"2"}
      }

    17.) 原查看考试结果 (TestpaperProcessorImpl->getTestpaperResult)
      post http://ip:port/mapi_v2/Testpaper/getTestpaperResult
      返回的json中额外返回 属性 isAnswerDisplayed
      {
        "isAnswerDisplayed": true //true表示显示答案, false表示不显示
      }

    18.) 原创建订单功能(OrderProcessorImpl->createOrder)
      post http://ip:port/mapi_v2/Order/createOrder
      参数 token, targetType, targetId, payment (wxpay_mobile|alipay), 
           device_info(目前未发现有什么用处, 可不填)
      返回的json中额外返回 人脸识别信息
      微信支付:
      {
        status: 'ok',
        url: 'weixin://wxpay/bizpayurl?pr=RfuDiSw', 
        prepayId: 'wx20160912180447cecd53e4d50815516213',
        sign: '10BE80C6E75562D5AB56ACB2DC87AFDC',
        nonceStr: 'dNxHZFzHqseuGfZo',
        order: order表信息
      }

    19.) 创建证书订单功能(OrderProcessorImpl->createCertificationOrder)
      post http://ip:port/mapi_v2/Order/createCertificationOrder
      参数 token, targetType, targetId, payment (wxpay_mobile|alipay), 
           currentLoopTime, device_info(目前未发现有什么用处, 可不填)
      返回的json中额外返回 人脸识别信息
      微信支付:
      {
        status: 'ok',
        appid: 'xxxx',
        mch_id: 'xxxx',
        url: 'weixin://wxpay/bizpayurl?pr=RfuDiSw', 
        prepayId: 'wx20160912180447cecd53e4d50815516213',
        sign: '10BE80C6E75562D5AB56ACB2DC87AFDC',
        nonceStr: 'dNxHZFzHqseuGfZo',
        timeStamp: 1490943182,
        order: order表信息
      }

    20.) 反馈信息
      post http://ip:port/mapi_v2/School/sendSuggestion
      参数 token, info, targetId, type (bug / fix), contact

    21.) 查看课程是否已购买
      get http://ip:port/mapi_v2/Order/validateCourseLearnable
      参数: courseId, currentLoopTime, token
      返回 {"isLearnable":true}

    22.) 获取行政区域接口  (UserProcessorImpl->getAreaCode)
      post http://ip:port/mapi_v2/User/getAreaCode
      {
        "username":  "15158114121"   
      }
      注意: username 必须是  身份证号/手机号
      返回 
      [
        {"code":"001","name":"\u5317\u4eac"},
        {"code":"003","name":"\u798f\u5efa"},
        {"code":"004","name":"\u676d\u5dde"}
      ]

    23.)  修改密码接口  (UserProcessorImpl->resetPassword)
      post http://ip:port/mapi_v2/User/resetPassword
      {
        "oldPassword":  "123456",
        "newPassword": "12345"
      }
      注意: token
      返回:
        错误信息:
        {
          "error": {"name":"password_error","message":"当前密码不正确"}
        }
        返回结果 true 为成功
      
    24.)  换绑手机接口  (UserProcessorImpl->bindMobile)
      post http://ip:port/mapi_v2/User/bindMobile
      {
        "password":  "123456",
        "mobile": "15158114121",
        "smsCode": "123456"
      }
      注意: token
      返回:
        错误信息:
        {
          "error": {"name":"password_error","message":"当前密码不正确"}
        }
        {
          "error": {"name":"sms_error","message":"该使用场景未开启"}
        }
        返回结果 true 为成功

    25.)  换绑手机验证码接口  (UserProcessorImpl->bindMobileSms)
      post http://ip:port/mapi_v2/User/bindMobileSms
      {
        "password":  "123456",
        "mobile": "15158114121"
      }
      注意: token
      返回:
        错误信息:
        {
          "error": {"name":"sms_error","message":"该使用场景未开启"}
        }
        {
          "error": {"name":"mobile_error","message":"该使用场景未开启"}
        }
        {
          "error": {"name":"password_error","message":"当前密码不正确"}
        }
        返回结果 true 为成功

    26.)  实名认证接口  (UserProcessorImpl->submitUserApproval)
      post http://ip:port/mapi_v2/User/submitUserApproval
      {
        "faceImg": "1111",
        "backImg": "2222",
        "jobsSeniorityCardImg": "3333",
        "driverLicenseImg": "4444",
        "otherImg": "5555"     //可以没有
      }
      注意: token
      approvalStatus: 
        unapprove:    未实名认证
        approving:     正在实名认证审核中
        approved:      实名认证通过
        approve_fail: 实名认证失败
      返回:
        错误信息:
        {
          "error": {"name":"img_error","message":"身份证正面照、手持身份证照都得上传"}
        }
        {
          "error": {"name":"status_error","message":"实名认证审核中或已经通过"}
        }
        返回结果 true 为成功
            
    27.)  实名认证状态接口  (UserProcessorImpl->getUserApprovalStatus)
      post http://ip:port/mapi_v2/User/getUserApprovalStatus
      {}
      注意: token
      approvalStatus: 
        unapprove:    未实名认证
        approving:     正在实名认证审核中
        approved:      实名认证通过
        approve_fail: 实名认证失败
      返回:
        返回结果 {'approvalStatus': ''} 为成功

    28.) 退费接口  (OrderProcessorImpl->applyRefund)
      get http://ip:port/mapi_v2/Order/applyRefund
      {
        "courseId" : 1,
        "currentLoopTime" : 1,
        "reason": "xxxx",     //退课原因
        "applyRefund": 0或1   //申请退费
      }
      applyRefund: 0,只退出课程但不申请退费， 1,退出课程且申请退费
      注意: token
      {"error":{"name":"member_error","message":"购买课程订单当前不是已支付状态，不能退费"}}
      返回结果 true 为成功

    29.) 退费信息接口  (OrderProcessorImpl->applyRefundInfo)
      get http://ip:port/mapi_v2/Order/applyRefundInfo
      {
        "courseId" : 1,
        "currentLoopTime" : 1
      }
      注意: token
      {
        "error": {"name":"member_error'","message":"您不是学员或尚未购买，不能退学"}
      }
      {
        "error": {"name":"order_error'","message":"购买课程订单不存在"}
      }
      {"error":{"name":"member_error","message":"购买课程订单当前不是已支付状态，不能退费"}}
      返回结果 
      {
        "payment": "coin",   //购买时支付方式
        "amount": 100.00,     //购买时价格
        "maxRefundDays": 10,  //最大退费天数
        "refundOverdue": true或false   //是否超过退费期限
      }
      为成功

    30.) 登录帐号切换行政区域接口  (UserProcessorImpl->changeArea)
      post http://ip:port/mapi_v2/User/changeArea
      {
        "areaCode":  "1234516"
      }
      注意: token
      返回的用户中 有额外属性 learnableCourses,  
        //数组, 显示为所有可以学习的课程, 
        // 格式同 "获取个人的所有证书课程"

    31.) app发现页中的公开课栏目类型 (DiscoveryColumn::get方法)
      get http://ip:port/api/discovery_columns
      返回
      {
        "resources":[
          {
            "id":"1",
            "title":"乐培中国",
            "type":"course",
            "categoryId":"0",
            "orderType":"recommend",
            "showCount":"2",
            "seq":"0",
            "schoolType": "main",    
              //分为 branchSchool 和 main, 表示分校和主站
            "createdTime":"1491981686",
            "updateTime":"0"
          }
        ],
        "total":1
      }

    32.) app发现页中的某个栏目下的课程信息 (Topxia\Api\Resource\Courses::discoveryColumn 方法)
      get http://ip:port/api/courses/discovery/columns?categoryId=0&orderType=recommend&showCount=4&type=course&schoolType=main 
        //新增schoolType
      token必须放在body 中
        {
          "token": "abc"  //新增token
        }
      返回
      {
        "resources":[
          {
            "id":"7", ... 其他属性
          }
        ],
        "total": 1
      }

    33.) app发现页中的某个栏目下的更多课程 (CourseProcessorImpl::searchCourse)
      get http://ip:port/mapi_v2/Course/searchCourse?categoryId=&limit=10&start=0&type=normal&schoolType=main&token=abc  
        //新增schoolType 和 token

      错误时返回
      {
        "error": {
          "name": "not_login",
          "message": "Api Token 无效"
        }
      }
            
    34.) app获取协议  SchoolProcessorImpl->getSchoolSite
      post http://ip:port/mapi_v2/School/getSchoolSite
      {
        "mockMobile": "android"  //模拟手机类型, 分为 android 或 ios, 手机访问时无效
      }

      返回结果
      {
        "site": {
          "name": "EDUSOHO测试站",
          "url": "http://www.esdev.com/mapi_v2",
          "host": "http://www.esdev.com",
          "logo": "",
          "splashs": [],
          "apiVersionRange": {
            "min":"2016.0.2",
            "max":"2016.6.0"
          }
        },
        "auth": {
          "isRegProtocolOn":false,   //true 表示 需要注册协议
          "regProtocol":"   //注册协议内容, 富文本内容格式
            123

            \r\n"
          }
        },
        "version": {
          "min":"2016.0.2",
          "show":true,
          "androidVersion":"3.4.6",
          "currentVersion":"3.4.6",
          "updateInfo":"lalalalalala",
          "updateUrl":"http://www.edusoho.com/download/mobile?client=android&code=edusohov3"
        }
      }

    35.)  找回密码, 获取校验码 (UserProcessorImpl->sendVerifyCodeForFindPass)
      post http://ip:port/mapi_v2/User/sendVerifyCodeForFindPass
      {
        "mobile": 13677221121
      }

      返回结果
        成功:
          { "token": "123dsse" }
        失败:
          {
            "error": {"name": "mobile_not_exist'","message": "手机号不存在"}
          }

    36.) 找回密码, 重置密码 (UserProcessorImpl->resetPassPerVerifyCode)
      post http://ip:port/mapi_v2/User/resetPassPerVerifyCode
      {
        "token": "123dsse",        //获取短信验证码时返回的token
        "password": "123456",   //新密码
        "verifyCode": "165423"  //短信验证码
      }

      返回结果
      成功:
        true
      失败:
        {
            "error": {"name": "reset_pass_per_verify_code_error'","message": "验证码无效"}
        }

    37.) 获取互动开关状态 (UserProcessorImpl::getInteractionSetting)
      post http://ip:port/mapi_v2/User/getInteractionSetting
      {
        "token": "123dsse", 
      }

      返回结果
      成功:
        {
          'isCertCourseInterDisplayed': true,    //是否展示证书课程互动
          'isNormalCourseInterDisplayed': false  //是否展示普通课程互动
        }
      失败:
        {
          "error": {"name": 500,"message": "token不存在"}
        }

    38.) 查看课程下的问答记录 
      get http://ip:port/api/courses/{courseId}/threads
      {
        "token": "123dsse", 
      }

      返回结果
      成功:
        未修改
    
    39.) 提问题&发话题
      post http://ip:port/api/chaos_threads
      {
        "token": "abc",
        "threadType": "course",  // 类型， 分为 common, course, group
        "currentLoopTime": "3",  // 第几循环，不填则为1
        "title": "提问",
        "content": "why",
        "courseId": "123",
        "type": "discussion"  //question 或 discussion
      }

    40.) 评价
      post http://ip:port/mapi_v2/Course/commitCourse
      {
        "token": "abc",
        "courseId": "1", 
        "currentLoopTime": "3",  // 第几循环，不填则为1
        "rating": "3",
        "content": "why"
      }



证书课程相关API  见 api\src\Topxia\Api\Resource\Course.php, /api/config/routing.php 中可修改路由
    1.) 获取个人的所有证书课程。
      post http://ip:port/api/course/{userId}/allCertificationCourses
      参数 token
      返回结果 
        [
          {
            'certificationId': 1,   //证书id
            'courseId': 123,
            'courseName':  '证书课程1', 
            'cerfiticationName' :  '证书1',
            'learningStatus': '',   //学习状态, 包含 
                                    // learning => 正在学习,  finished => 已完成, 
                                    // overtimeMakeupYes => 已过期不可补学,
                                    // overtimeMakeupNo => 已过期可补学
                                    // unPaied => 未购买,  makeup => 已补学
                                    // completion => 已结业
            'smallPicture': '',      //格式为 完整的图片url 
            'middlePicture': '',   //格式为 完整的图片url  
            'largePicture: '',      //格式为 完整的图片url   
            'certificationType':  '',  // 学制, 直接翻译好, 1年制, 2年制, 半年制
            'startTime' : '2017-04-15', //课程开始时间
            'deadline': '2012-02-12', // 2012-02-12, 非补学时的到期时间
            'leftDays' : -100,  //正常学习的到期时间, 单位天
            'makeupDeadline': '2014-02-12', // 如果状态是补学, 这里显示补学到期时间
            'makeupLeftDays': 265,   //补学时的到期时间
            'buyable': true,       //是否可购买, 如果是 未购买 或 已过期可补学时 为 true
            'learnedLessionCount': 3, 
            'totalLessionCount': 5,
            'learnedHours': 34, //所获学时
            'totalHours': 44, //总学时
            'isTestpaperPassed': false, //考试是否通过
            'isCompleted': false  //是否结业
          } ...
        ]
  
    2.) 获取个人的所有证书。// api\src\Topxia\Api\Resource\Course.php
      post http://ip:port/api/course/{userId}/allCertifications
      参数 token
      返回结果 
        [
          {
            "certificationId": "2",
            "certificationName": "（货运）经营性道路货物运输驾驶员",
            "awardTime": "1401559380",
            "learnMode": "0",
            "relearn": "0",
            "startTime": "2014-06-01",
            "deadline": "2016-06-01"
          },
          {
            "certificationId": "3",
            "certificationName": "（危险品）道路危险货物运输驾驶员",
            "awardTime": "1365955200",
            "learnMode": "0",
            "relearn": "0",
            "startTime": "2013-04-15",
            "deadline": "2015-04-14"
          }
        ]

     3.) 获取个人的某个证书课程信息  // api\src\Topxia\Api\Resource\Course.php
      post http://ip:port/api/course/{userId}/singleCertificationCourse
      参数: courseId, token
      返回结果 
      {
        "certificationId": "2",
        "certificationName": "（货运）经营性道路货物运输驾驶员",
        "awardTime": "1401559380",
        "learnMode": "0",
        "relearn": "0",
        "startTime": "2014-06-01",
        "deadline": "2016-06-01"
      }

     4.) 获取个人的某个证书课程的学习记录
      post http://ip:port/api/course/learnRecord
      参数: userId,courseId, token
      返回结果 
      {
        "nickname":"18399677738",
        "courseName":"zxc sha3",
        "startTime":"2016-04-15",
        "deadline":"2018-04-15",
        "lessons":[
          {
            "userId": "1",
            "courseId": "123",
            "lessonId": "10",
            "chapter": "第1章",
            "title":"",
            "startTime":"2016-06-14 18:58:23",
            "finishedTime":"2016-06-14 18:58:26",
            "suggestHours":"60.0\u5206\u949f,",
            "learningProgress":50
          }, {
            "userId": "1",
            "courseId": "123",
            "lessonId": "11",
            "title":"",
            "startTime":"2016-06-14 18:58:32",
            "finishedTime":0,
            "suggestHours":"60.0\u5206\u949f",
            "learningProgress":50
          }
        ]
      }
     
     5.) 获取个人的某个证书课程的详细学习记录
      post http://ip:port/api/course/learnRecordDetail
      参数: userId,courseId,lessonId, token
      返回结果 
      {
        "nickname":"",
        "lessonTitle":"",
        "learnLessons":[
          {
            "viewDevice":"PC",
            "startTime":"2016-06-07 17:05:24",
            "finishedTime":"2016-06-07 17:05:55",
            "watchTime":"00\u5206\u949f31\u79d2"
          }, {
            "viewDevice":"PC",
            "startTime":"2016-05-31 15:17:08",
            "finishedTime":"2016-05-31 15:17:30",
            "watchTime":"00\u5206\u949f22\u79d2"
          }
        ]
      }

    6.) 添加学员学习证书课程的学习记录
      post http://ip:port/api/course/{userId}/learnRecord
      参数: courseId, lessonId, startTime, finishedTime, token
      返回结果 
        true  -> 添加成功
        不是true -> 添加失败
               
    7.) 添加学员学习证书课程的课时详情记录
      post http://ip:port/api/course/lessonView/addLearnRecordDetail
      参数: userId, courseId, lessonId, currentLoopTime, token
      返回结果 
      {
        "lessonViewId":"" /已完成返回 -1
      }

      失败
      {
        "error": {"name":"lessonView","message":"记录添加失败"}
      }

    8.) 更新学员学习证书课程的课时观看记录
      post http://ip:port/api/course/lessonView/updateLearnRecordDetail
      参数: lessonViewId, watchTime,isAddLessonView,currentLoopTime ,token
      返回结果 
      true  -> 添加成功
      {
        "error": {"name":"lessonView","message":"更新失败"}
      }
         
    9.) 我的动态  MeCourses.php (get方法)
      get http://ip:port/api/me/courses
      参数:　relation＝"learn", token

      返回结果
      {
        ...
        "latestStatusTime": "2017-04-20 13:12:09"    //额外新增 最新的动态时间
      }

    10.) 移动端版本控制 SchoolProcessorImpl.php(getClientVersion方法)
      POST http://www.edusoho-dev.com/mapi_v2/School/getClientVersion
      参数: 
      {
        "mockMobile": "android"  //模拟手机类型, 分为 android 或 ios, 手机访问时无效
      }

      返回结果
        android
        {
          "show":true,
          "min": 3,
          "androidVersion":3,
          "currentVersion":3,
          "updateInfo":"lalalalalala",
          "updateUrl":"http://www.edusoho.com/download/mobile?client=android&code=edusohov3"
        }

        IOS
        {
          "show":true,
          "min": 3,
          "androidVersion":3,
          "currentVersion":3,
          "updateInfo":"lalalalalala",
          "updateUrl":"http://www.edusoho.com/download/mobile?client=android&code=edusohov3"
        }
        通过读文件app/config 下的versionConfig.yml中的配置

    11.) 移动端版本控制 BaseProcessor.php (getSiteInfo)
      POST http://www.edusoho-dev.com/mapi_v2/User/loginWithToken
      参数 Token
      返回结果中包含
        apiVersionRange":{
          "min":"2016.0.2",
          "max":"2016.6.0"
        }
      通过读文件app/config 下的versionConfig.yml中的配置 最高版本号和最低版本号

直播相关API  /api/config/routing.php 中可修改路由
    1.) 获取直播入口信息 (LessonLiveTickets.php)
      post http://ip:port/api/lessons/{lessonId}/live_tickets
      参数 token, device (android / iphone)
      返回结果 
      {
        "no": "5857412bc7d63211415523",
        "roomId": "42056",
        "user": {
          "id": "10099",
          "nickname": "13675814015",
          "role": "student",
          "user": "10099"
        },
        "device":"desktop"
      }
    2.) 获取直播信息 (LessonLiveTicket.php)
      get http://ip:port/api/lessons/{lessonId}/live_tickets/{ticketNo}
      参数 token (url中的ticketNo为第一个接口返回的 no 参数)
      返回结果 
      {
        "no": "58574c046b3d8338207959",
        "roomId": "42060",
        "user": {
          "id":"101",
          "nickname":"好的",
          "role": "student",
          "user":"101"
        },
        "device": "android",
        "roomUrl": "http://edusoho.gensee.com/webcast/site/entry/join-e29d969aafca4134a9f029b60a259ed6?token=623092&nickName=%E5%A5%BD%E7%9A%84&uid=2147483546&k=14821167005129487b60683d79814010",
        "extra": {
          "sdk": "support",
          "domain": "edusoho.gensee.com",
          "roomNumber": "85096870", 
          "loginAccount": "gensee22368_kcx9",
          "loginPwd": "578614cce50a1",
          "joinPwd": "623092",
          "nickName": "好的",
          "serviceType": "webcast",
          "k": "14821167005129487b60683d79814010",
          "provider": "gensee"
        }
      }