<?php

namespace Topxia\Api\Resource;

use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\UrlUtils;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\DateUtils;

class Course extends BaseResource
{
    public function filter(&$res)
    {
        if (isset($res['createdTime']) && isset($res['updatedTime'])) {
            $res['createdTime'] = date('c', $res['createdTime']);
            $res['updatedTime'] = date('c', $res['updatedTime']);
        }
        
        $default            = $this->getSettingService()->get('default', array());

        if (!empty($res['smallPictureUrl']) && !empty($res['middlePictureUrl']) && !empty($res['largePictureUrl'])) {
            $res = UrlUtils::formatCoursePictures($res);
        }

        if (empty($res['smallPicture']) && empty($res['middlePicture']) && empty($res['largePicture'])) {
            $res['smallPicture']  = !isset($default['course.png']) ? '' : $default['course.png'];
            $res['middlePicture'] = !isset($default['course.png']) ? '' : $default['course.png'];
            $res['largePicture']  = !isset($default['course.png']) ? '' : $default['course.png'];
        }

        foreach (array('smallPicture', 'middlePicture', 'largePicture') as $key) {
            $res[$key] = $this->getFileUrl($res[$key]);
        }

        if (!isset($res['currentLoopTime'])) {
            $res['currentLoopTime'] = CourseLoopUtils::$START_LOOP_TIME;
        }

        return $res;
    }

    public function getAllCertificationCoursesAction(Request $request, $userId)
    {
        //$certificationCourses = $this->getCertificateService()->getAllCertificationsWithLessonStatus($userId);
        $certificationCourses = $this->getCertificateService()->getLearnableCertificationsWithLessonStatus($userId);

        return $this->filterCourses($certificationCourses);
    }

    public function getLearnRecord(Request $request)
    {
        $conditions = $request->request->all();
        $learnRecord = $this->getCourseService()->getLearnRecord($conditions['userId'],$conditions['courseId']);
        return $learnRecord;
    }

    public function getLearnRecordDetail(Request $request)
    {
        $conditions = $request->request->all();
        $learnRecordDetail = $this->getCourseService()->getLearnRecordDetail($conditions['userId'],$conditions['courseId'],$conditions['courseId']);
        return $learnRecordDetail;
    }

    public function addLearnRecordDetail(Request $request)
    {
        $conditions = $request->request->all();
        $learnRecord = $this->getCourseService()->addLearnRecordDetail($conditions['userId'],$conditions['courseId'], $conditions['lessonId'],$conditions['currentLoopTime']);
        return $learnRecord;
    }

    public function updateLearnRecordDetail(Request $request)
    {
        $conditions = $request->request->all();
        $this->recordConditionsWithUserId('updateLearnRecordDetail', $request);

        if (empty($conditions['watchTime']) || $conditions['watchTime'] == 0) {
            $this->recordConditionsWithUserId('updateLearnRecordDetail_watchTime_zero', $request);
        }

        $learnRecordDetail = $this->getCourseService()->updateLearnRecordDetail($conditions['lessonViewId'],$conditions['watchTime'], $conditions['isAddLessonView'], $conditions['currentLoopTime']);
        return $learnRecordDetail;
    }

    public function getSingleCertificationCourseAction(Request $request, $userId)
    {
        $certificationCourses = $this->getCertificateService()->getAllCertificationsWithLessonStatus($userId);
        $courseId = $request->headers->get('courseId');

        $result = array();
        foreach ($certificationCourses as $course) {
            if ($courseId == $course['courseId']) {
                $result = $course;
                break;
            }
        }
        return $this->filter($result);
    }

    public function getAllCertificationsAction(Request $request, $userId)
    {
        $certifications = $this->getCertificateService()->findFormatedCertificatesByUserId($userId);
        return $certifications;
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    private function filterCourses($courses)
    {
        return $this->multicallFilter('Course', $courses);
    }

    private function recordConditionsWithUserId($filePathName, $request)
    {
        $conditions = $request->request->all();
        $user = $this->getCurrentUser();
        $conditions['userId'] = $user['id'];
        $filePath = $this->getServiceKernel()->getParameter('topxia.upload.public_directory') . '/../../app/logs/' . $filePathName;
        $dataContent = DateUtils::number2StrWithSeconds(time()) . ' ' . json_encode($conditions);
        file_put_contents($filePath, $dataContent . "\n\n", FILE_APPEND);
    }
}