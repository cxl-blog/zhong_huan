<?php

namespace Topxia\Api\Resource;

use Silex\Application;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;

class BranchSchools extends BaseResource
{
    public function get(Application $app, Request $request)
    {
        $conditions = $request->query->all();

        $start = $request->query->get('start', 0);
        $limit = $request->query->get('limit', 10);

        $user = getCurrentUser();

        $branchSchools = $this->getBranchSchoolService()->searchBranchSchools($conditions, array('createdTime', 'DESC'), $start, $limit);

        if ($user) {
            $branchSchoolUsers = $this->getBranchSchoolService()->searchBranchSchoolUsers(array('userIds' => array($user['id'])), array('schoolId', 'DESC'), 0, 999);

            $joinBranchSchoolIds = ArrayToolkit::column($branchSchoolUsers, 'schoolId');

            foreach ($branchSchools as &$branchSchool) {
                if (in_array($branchSchool['id'], $joinBranchSchoolIds)) {
                    $branchSchool['join'] = 1;
                } else {
                    $branchSchool['join'] = 0;
                }
            }
        }

        $total = $this->getBranchSchoolService()->searchBranchSchoolCount($conditions);
        return $this->wrap($this->filter($branchSchools), $total);
    }

    public function filter(&$res)
    {
        return $this->multicallFilter('BranchSchool', $res);
    }

    protected function multicallFilter($name, &$res)
    {
        foreach ($res as &$one) {
            $this->callFilter($name, $one);
        }

        return $res;
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('User.UserService');
    }

    protected function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }
}
