<?php

namespace Topxia\Api\Resource;

use Silex\Application;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Constants\DbValue;
use Symfony\Component\HttpFoundation\Request;

class User extends BaseResource
{
    private $_unsetFields = array(
        'password', 'salt', 'payPassword', 'payPasswordSalt'
    );

    private $_publicFields = array(
        'id', 'nickname', 'title', 'roles', 'point', 'smallAvatar', 'mediumAvatar', 'largeAvatar', 'about', 'createdTime', 'updatedTime'
    );

    private $_privateFields = array(
        'id', 'nickname', 'title', 'tags', 'type', 'roles',
        'point', 'coin', 'smallAvatar', 'mediumAvatar', 'largeAvatar', 'about',
        'email', 'emailVerified', 'promoted', 'promotedTime', 'locked', 'lockDeadline',
        'loginTime', 'loginIp', 'approvalTime', 'approvalStatus', 'newMessageNum', 'newNotificationNum',
        'createdIp', 'createdTime', 'updatedTime'
    );

    private $_profileFields = array(
        'truename', 'idcard', 'gender', 'birthday', 'city', 'mobile', 'qq',
        'signature', 'about', 'company', 'job', 'school', 'class', 'weibo', 'weixin', 'site'
    );

    public function get(Application $app, Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        if (empty($user)) {
            return $this->error(404, "用户(#{$id})不存在");
        }

        $user['profile'] = $this->getUserService()->getUserProfile($id);

        return $this->filter($user);
    }

    public function filter(&$res)
    {
        foreach ($this->_unsetFields as $key) {
            unset($res[$key]);
        }

        if (isset($res['profile'])) {
            $res['about'] = $res['profile']['about'];
        }

        $currentUser = getCurrentUser();

        $returnRes = array();

        if ($currentUser->isAdmin() || ($currentUser['id'] == $res['id'])) {
            foreach ($this->_privateFields as $key) {
                $returnRes[$key] = $res[$key];
            }

            if (isset($res['profile'])) {
                foreach ($this->_profileFields as $key) {
                    $returnRes[$key] = $res['profile'][$key];
                }
            }
        } else {
            foreach ($this->_publicFields as $key) {
                if (isset($res[$key])) {
                    $returnRes[$key] = $res[$key];
                }
            }

            if (in_array('ROLE_TEACHER', $returnRes['roles'])) {
                $returnRes['roles'] = array('ROLE_TEACHER');
            } else {
                $returnRes['roles'] = array('ROLE_USER');
            }
        }

        $res = $returnRes;

        foreach (array('smallAvatar', 'mediumAvatar', 'largeAvatar') as $key) {
            $res[$key] = $this->getFileUrl($res[$key]);
        }

        foreach (array('promotedTime', 'loginTime', 'approvalTime', 'createdTime') as $key) {
            if (!isset($res[$key])) {
                continue;
            }

            $res[$key] = date('c', $res[$key]);
        }

        $res['updatedTime'] = date('c', $res['updatedTime']);

        return $res;
    }

    public function simplify($res)
    {
        $simple = array();

        $simple['id']       = $res['id'];
        $simple['nickname'] = $res['nickname'];
        $simple['title']    = $res['title'];
        $simple['avatar']   = $this->getFileUrl($res['smallAvatar']);

        return $simple;
    }

    public function getFaceDetectStatus($userId)
    {
        $currentUser = $this->getCurrentUser();
        return $this->getFaceDetectService()->getFaceDetectStatus($currentUser['id']);
    }

    public function updateUserFaceImg(Request $request, $userId)
    {
        $currentUser = $this->getCurrentUser();
        try {
            $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');
            $this->getFaceDetectService()->updateBatchUserFaceImg($userId,
                $request->get("base64ImgData"), $publicUploadDir);
            $this->getUserService()->setBatchFacePhotoFinish($userId, DbValue::TRUE);
        } catch (\RuntimeException $e) {
            return 'false';
        }

        return 'true';
    }

    public function faceDetectAction(Request $request, $userId)
    {
        $currentUser = $this->getCurrentUser();
        $lessonViewId = $request->get('lessonViewId');
        $testpaperLessonId = $request->get('testpaperLessonId');
        $currentLoopTime = $request->get('currentLoopTime');
        $imageData = $request->get('base64ImgData');
        
        //if (isset($lessonViewId) && $lessonViewId != '0') {
        if (!empty($lessonViewId)) {
            $faceDetectResult = $this->getFaceDetectResultService()->buildLessonDetectResultWithRequest($lessonViewId, $imageData);
        } else if (!empty($testpaperLessonId)) {
            $faceDetectResult = $this->getFaceDetectResultService()->buildTestpaperDetectResultWithRequest($testpaperLessonId, $imageData, $currentLoopTime);
        } else {
            $faceDetectResult = $this->getFaceDetectResultService()->buildLoginDetectResultWithRequest($imageData);
        }
        
        $faceDetectResult['userId'] = $userId;
        try {
            $result = $this->getFaceDetectService()->isFaceImgValid(
                $userId, $request->get("base64ImgData"), $faceDetectResult);
        } catch (\RuntimeException $e) {
            return 'false';
        }

        return $result ? 'true' : 'false';
    }

    public function validateTokenAction(Request $request, $userId)
    {
        try {
            $currentUser = $this->getCurrentUser();
            if ($this->isRedisOpened() && $this->getRedis()->exists("session:logined:userId:".$userId)) {
                $this->getTokenService()->deleteMobileLoginToken($userId);
                return 'false';
            }
            return $currentUser['id'] == $userId ? 'true' : 'false';
        } catch (\Exception $e) {
            return 'false';
        }
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getTokenService()
    {
        return $this->getServiceKernel()->createService('Custom:User.TokenService');
    }

    protected function getFaceDetectService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetect.FaceDetectService');
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    protected function isRedisOpened()
    {
        $redisSetting = $this->getSettingService()->get('redis', array());

        if (empty($redisSetting['opened']) || $redisSetting['opened'] == 0) {
            return false;
        }

        return true;
    }

    protected function getRedis($group = 'default')
    {
        return $this->getServiceKernel()->getRedis($group);
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

}
