<?php

namespace Topxia\Api\Resource;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class BranchSchool extends BaseResource
{
    public function get(Application $app, Request $request, $id)
    {
        $branchSchool = $this->getBranchSchoolService()->getBranchSchool($id);
        return $this->filter($branchSchool);
    }

    public function filter(&$res)
    {
        $res['logo']        = $this->getFileUrl($res['logo']);
        $res['createdTime'] = date('c', $res['createdTime']);
        return $res;
    }

    protected function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }
}
