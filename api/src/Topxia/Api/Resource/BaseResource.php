<?php

namespace Topxia\Api\Resource;

use Topxia\Service\Common\ServiceKernel;
use Topxia\MobileBundleV2\Controller\MobileBaseController;
use Topxia\Service\User\CurrentUser;
use Custom\Common\Util\StringUtils; 

abstract class BaseResource
{
    abstract function filter(&$res);

    protected function callFilter($name, &$res)
    {
        global $app;
        return $app["res.{$name}"]->filter($res);
    }

    protected function multicallFilter($name, &$res)
    {
        foreach ($res as &$one) {
            $this->callFilter($name, $one);
        }
        return $res;
    }

    protected function multicallSimplify($name, $res)
    {
        foreach ($res as $key => $one) {
            $res[$key] = $this->callSimplify($name, $one);
        }
        return $res;
    }

    protected function callSimplify($name, &$res)
    {
        global $app;
        return $app["res.{$name}"]->simplify($res);
    }

    protected function simplify($res)
    {
        return $res;
    }

    protected function error($code, $message)
    {
        return array('error' => array(
                'code' => $code,
                'message' => $message,
            ));
    }

    protected function wrap($resources, $total)
    {
        if (is_array($total)) {
            return array('resources' => $resources, 'next' => $total);
        } else {
            return array('resources' => $resources, 'total' => $total);
        }
    }

    protected function simpleUsers($users)
    {
        $newArray = array();
        foreach ($users as $key => $user) {
            $newArray[$key] = $this->simpleUser($user);
        }

        return $newArray;
    }

    protected function simpleUser($user)
    {
        $simple = array();

        $simple['id'] = $user['id'];
        $simple['nickname'] = StringUtils::formatStringForSecurity($user['nickname']);
        $simple['title'] = $user['title'];
        $simple['roles'] = $user['roles'];
        $simple['avatar'] = $this->getFileUrl($user['smallAvatar']);

        return $simple;
    }

    protected function nextCursorPaging($currentCursor, $currentStart, $currentLimit, $currentRows)
    {
        $end = end($currentRows);
        if (empty($end)) {
            return array(
                'cursor' => $currentCursor + 1,
                'start' => 0,
                'limit' => $currentLimit,
                'eof' => true,
            );
        }

        if (count($currentRows) < $currentLimit) {
            return array(
                'cursor' => $end['updatedTime'] + 1,
                'start' => 0,
                'limit' => $currentLimit,
                'eof' => true,
            );
        }


        if ($end['updatedTime'] != $currentCursor) {
            $next = array(
                'cursor' => $end['updatedTime'],
                'start' => 0,
                'limit' => $currentLimit,
                'eof' => false,
            );
        } else {
            $next = array(
                'cursor' => $currentCursor,
                'start' => $currentStart + $currentLimit,
                'limit' => $currentLimit,
                'eof' => false,
            );
        }

        return $next;
    }

    protected function filterHtml($text)
    {
        preg_match_all('/\<img.*?src\s*=\s*[\'\"](.*?)[\'\"]/i', $text, $matches);
        if (empty($matches)) {
            return $text;
        }

        foreach ($matches[1] as $url) {
            $text = str_replace($url, $this->getFileUrl($url), $text);
        }

        return $text;
    }

    public function getFileUrl($path)
    {
        if (empty($path)) {
            return '';
        }
        if (strpos($path, "http://") !== false) {
            return $path;
        }

        $cdn    = $this->getServiceKernel()->createService('System.SettingService')->get('cdn', array());
        $prefixUrl = (empty($cdn['enabled'])) ? '' : rtrim($cdn['url'], " \/");
        if (empty($prefixUrl)) {
            $prefixUrl = "http://{$_SERVER['HTTP_HOST']}/files/";
        } elseif (strpos($path, 'public://') !== false) {
            $prefixUrl .= '/files/';
        }

        $path = str_replace('public://', '', $path);
        $path = str_replace('files/', '', $path);
        return $prefixUrl . $path;
    }

    protected function getAssetUrl($path)
    {
        if (empty($path)) {
            return '';
        }
        $path = "http://{$_SERVER['HTTP_HOST']}/assets/{$path}";
        return $path;
    }

    protected function getCurrentUser()
    {
        return $this->getServiceKernel()->getCurrentUser();
    }

    protected function getUserByToken($request)
    {
        $token = $this->getToken($request);
        $token = $this->getUserService()->getToken(MobileBaseController::TOKEN_TYPE, $token);
        if ($token) {
            $this->setCurrentUser($token['userId'], $request);
        } else {
            throw new \RuntimeException('API Token不存在！');
        }
        return $this->getCurrentUser();
    }

    protected function getToken($request)
    {
        $token = $request->headers->get('token', '');
        if (empty($token) && $request->getMethod() == "GET") {
            $token = $request->query->get('token', '');
        }

        if (empty($token)) {
            $token = $request->cookies->get("token");
        }
        return $token;
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getTokenService()
    {
        return $this->getServiceKernel()->createService('User.TokenService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('User.UserService');
    }

    private function setCurrentUser($userId, $request)
    {
        $user = $this->getUserService()->getUser($userId);
        $currentUser = new CurrentUser();
        if ($user) {
            $user['currentIp'] = $request->getClientIp();
        } else {
            $user = array('id' => 0);
        }
        $currentUser = $currentUser->fromArray($user);
        $this->getServiceKernel()->setCurrentUser($currentUser);
    }
}