<?php

namespace Topxia\Api\Resource;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\CourseGroupUtils;
use Custom\Common\Constants\LearnStatus;

class MeCourses extends BaseResource
{
    public function get(Application $app, Request $request)
    {
        $conditions = $request->query->all();
        $start      = $request->query->get('start', 0);
        $limit      = $request->query->get('limit', 10);
        $type       = $request->query->get('type', '');
        $relation   = $request->query->get('relation', '');
        $user       = getCurrentUser();
        if ($relation == 'learning') {
            $total   = $this->getCourseService()->findUserLeaningCourseCount($user['id'], $conditions);
            $courses = $this->getCourseService()->findUserLeaningCourses(
                $user['id'],
                $start,
                $limit,
                empty($type) ? array() : array('type' => $type)
            );
        } elseif ($relation == 'learned') {
            $total   = $this->getCourseService()->findUserLeanedCourseCount($user['id'], $conditions);
            $courses = $this->getCourseService()->findUserLeanedCourses(
                $user['id'],
                $start,
                $limit,
                empty($type) ? array() : array('type' => $type)
            );
        } elseif ($relation == 'learn') {  //我的动态页面
            $userId = $user['id'];

            $allNormalCourses = $this->getCourseService()->findUserLeaningCourses(
                $userId,
                0,
                PHP_INT_MAX,
                array('type' => 'normal')
            );
            
            $groupedParams = CourseGroupUtils::getGroupedCourses($userId, $allNormalCourses, 
                array(
                    LearnStatus::LEARNING,
                    LearnStatus::MAKEUP,
                    LearnStatus::FINISHED,
                    LearnStatus::COMPLETION
                ),
                array(
                    'certificateService' => $this->getCertificationService(),
                    'startIndex' => $start,
                    'pageSize' => $limit
                )
            );

            $courses = $this->getLatestStatusService()->fillLatestUpdatedTimeForCourses(
                $user['id'], $groupedParams['courses']
            );
            return array(
                'resources' => $this->filter($courses),
                'total' => $groupedParams['total']
            );
        } elseif ($relation == 'teaching') {
            $total   = $this->getCourseService()->findUserTeachCourseCount(array('userId' => $user['id']), false);
            $courses = $this->getCourseService()->findUserTeachCourses(
                array('userId' => $user['id']),
                $start,
                $limit,
                false
            );
        } elseif ($relation == 'favorited') {
            $total   = $this->getCourseService()->findUserFavoritedCourseCount($user['id']);
            $courses = $this->getCourseService()->findUserFavoritedCourses(
                $user['id'],
                $start,
                $limit
            );
        } else {
            return $this->error('error', '缺少参数!');
        }

        return $this->wrap($this->filter($courses), $total);
    }

    public function filter(&$res)
    {
        return $this->multicallFilter('Course', $res);
    }

    protected function multicallFilter($name, &$res)
    {
        foreach ($res as &$one) {
            $this->callFilter($name, $one);
        }

        return $res;
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }

    protected function getLatestStatusService()
    {
        return $this->getServiceKernel()->createService('Custom:User.LatestStatusService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
