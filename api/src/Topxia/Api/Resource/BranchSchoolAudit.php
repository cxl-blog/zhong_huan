<?php

namespace Topxia\Api\Resource;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class BranchSchoolAudit extends BaseResource
{
    public function post(Application $app, Request $request, $id)
    {
        $branchSchool     = $this->getBranchSchoolService()->getBranchSchool($id);
        $user             = getCurrentUser();
        $info             = array();
        $branchSchoolUser = $this->getBranchSchoolService()->getUserByUserIdAndSchoolId($user['id'], $id);

        if ($branchSchoolUser) {
            $status = 'join';
        } else {
            if ($branchSchool['audit'] == 0) {
                $branchSchoolUser['schoolId'] = $branchSchool['id'];
                $branchSchoolUser['userId']   = $user['id'];
                $branchSchoolUser['roles']    = array('ROLE_USER');
                $user                         = $this->getBranchSchoolService()->createBranchSchoolUser($branchSchoolUser);
                $status                       = 'join';
            } else {
                $conditions         = array('userId' => $user['id'], 'schoolId' => $id);
                $branchSchoolAudits = $this->getBranchSchoolService()->searchBranchSchoolAudits(
                    $conditions,
                    array('id', 'DESC'),
                    0, 1
                );

                if ($branchSchoolAudits) {
                    if ($branchSchoolAudits[0]['status'] == 'approving') {
                        $status = 'approving';
                    }

                    if ($branchSchoolAudits[0]['status'] == 'approve_fail' || $branchSchoolAudits[0]['status'] == 'unapprove') {
                        $audit             = array('status' => 'approving');
                        $branchSchoolAudit = $this->getBranchSchoolService()->updateBranchSchoolAudit($branchSchoolAudits[0]['id'], $audit);
                        $status            = 'approving';
                    }
                } else {
                    $audit['userId']    = $user['id'];
                    $audit['schoolId']  = $id;
                    $audit['status']    = 'approving';
                    $audit['auditTime'] = time();
                    $branchSchoolAudit  = $this->getBranchSchoolService()->addBranchSchoolAudit($audit);
                    $status             = 'approving';
                }
            }
        }

        $info = array('status' => $status, 'branchSchool' => $branchSchool);

        return $info;
    }

    public function filter(&$res)
    {
        return $res;
    }

    protected function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }
}
