<?php
namespace Topxia\WebBundle\Listener;

use Topxia\Service\Common\ServiceKernel;
use Custom\Service\FaceDetect\FaceDetectService;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

class KernelControllerListener
{
    private $resolver;

    public function __construct(ControllerResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
            return;
        }

        $request = $event->getRequest();

        $currentUser = $this->getUserService()->getCurrentUser();

        if ($currentUser->isLogin()) {
            $isCatchCamera = $request->server->get('REQUEST_URI') == ''
            || strpos($request->server->get('REQUEST_URI'), '/camera') !== false;

            if (!$isCatchCamera && !$request->getSession()->has(FaceDetectService::SESSION_FACE_DETECTED)) {
                $this->renderUrl('CustomWebBundle:Default:index', $event);
            }
        }

        $route = $request->attributes->get('_route');

        $whiteList = array(
            'course_explore',
            'course_buy_modify_user_info',
            'course_note_like',
            'course_note_cancel_like',
            'course_learn_time',
            'course_watch_time',
            'course_lesson_marker_show',
            'course_lesson_marker_metas',
            'course_lesson_question_marker_show',
            'course_lesson_question_marker_answer_show',
            'course_lesson_question_marker_next',
            'course_certification_lesson_marker_show',
            'course_certification_lesson_marker_metas',
            'course_certification_learn_time',
            'course_certification_watch_time',
            ''
        );

        if (!in_array($route, $whiteList) && preg_match('/^course_/s', $route) && !array_intersect(array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'), $currentUser['roles'])) {
            $params = $request->attributes->get('_route_params');

            if (!$params) {
                return;
            }

            if (isset($params['id'])) {
                $courseId = $params['id'];
            }

            if (isset($params['courseId'])) {
                $courseId = $params['courseId'];
            }

            if (isset($courseId)) {
                $course = $this->getCourseService()->getCourse($courseId);

                if ($course['schoolId'] && $course['type'] == 'certification') {
                    $branchSchoolUser = $this->getBranchSchoolService()->getUserByUserIdAndSchoolId($currentUser['id'], $course['schoolId']);

                    if ($branchSchoolUser) {
                        return;
                    }

                    $fakeRequest = $event->getRequest()->duplicate(null, null, array('_controller' => 'BranchSchoolBundle:BranchSchool:permissions'));
                    $controller  = $this->resolver->getController($fakeRequest);
                    $event->setController($controller);

                    return;
                }
            }
        }

        $whiteList = array('branch_school_join', 'branch_school_auditjoin', 'branch_school_no_permissions', 'branch_school_homepage', 'branch_school_certification', 'branch_school_category', 'branch_school_orderBy', 'branch_school_course_explore', 'branch_school_certification_course_explore');

        if (!in_array($route, $whiteList) && preg_match('/^branch_school/s', $route) && !array_intersect(array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'), $currentUser['roles'])) {
            $params = $request->attributes->get('_route_params');

            if (isset($params['domain'])) {
                $domain       = $params['domain'];
                $branchSchool = $this->getBranchSchoolService()->getBranchSchoolByDomain($domain);

                if (!$branchSchool) {
                    return;
                }

                $branchSchoolUser = $this->getBranchSchoolService()->getUserByUserIdAndSchoolId($currentUser['id'], $branchSchool['id']);

                if ($branchSchoolUser) {
                    return;
                }

                $fakeRequest = $event->getRequest()->duplicate(null, null, array('_controller' => 'BranchSchoolBundle:BranchSchool:permissions'));
                $controller  = $this->resolver->getController($fakeRequest);
                $event->setController($controller);

                return;
            }
        }
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('User.UserService');
    }

    protected function getBranchSchoolService()
    {
        return ServiceKernel::instance()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }

    protected function getCourseService()
    {
        return ServiceKernel::instance()->createService('BranchSchool:Course.CourseService');
    }

    protected function getFaceDetectService()
    {
        return ServiceKernel::instance()->createService('Custom:FaceDetect.FaceDetectService');
    }

    private function getFaceDetectStatus($currentUser)
    {
        $service = $this->getFaceDetectService();

        return $service->getFaceDetectStatus(
            $currentUser->getId(), $currentUser);
    }

    private function renderUrl($twigPageUrl, FilterControllerEvent $event)
    {
        $fakeRequest = $event->getRequest()->duplicate(null, null, array('_controller' => $twigPageUrl));
        $controller  = $this->resolver->getController($fakeRequest);
        $event->setController($controller);
    }
}
