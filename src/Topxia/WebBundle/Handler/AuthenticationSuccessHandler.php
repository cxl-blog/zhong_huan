<?php

namespace Topxia\WebBundle\Handler;

use Custom\Common\Util\UrlUtils;
use Custom\WebBundle\Controller\CameraController;
use Topxia\Service\Common\ServiceKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Topxia\WebBundle\Handler\AuthenticationHelper;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->clearCookies();
        $forbidden = AuthenticationHelper::checkLoginForbidden($request);

        if ($forbidden['status'] == 'error') {
            $exception = new AuthenticationException($forbidden['message']);
            throw $exception;
        }



        if ($this->getAuthService()->hasPartnerAuth()) {
            $url     = $this->httpUtils->generateUri($request, 'partner_login');
            $queries = array('goto' => $this->determineTargetUrl($request));
            $url     = $url.'?'.http_build_query($queries);
            return $this->httpUtils->createRedirectResponse($request, $url);
        }

        $url = parent::onAuthenticationSuccess($request, $token);

        $targetUrl = UrlUtils::getUrlPrefix($url->getTargetUrl());
        $roles = ArrayToolkit::column($forbidden,'roles');

        $isLearner = false;
        $userId = $forbidden['user']['id'];

        if (isset($roles[0]) && !in_array('ROLE_SUPER_ADMIN', $roles[0])) {            
            $firstAdminedBranchSchool = $this->getBranchSchoolService()->
                    getFirstAdminedBranchSchool($userId);           
            if (isset($firstAdminedBranchSchool)) {
                $targetUrl  .= 'branch/school/'.$firstAdminedBranchSchool['domain'];
            } else if (in_array('ROLE_USER', $roles[0])) {                
                if (!empty($forbidden['user']['areaCode'])) {
                    if (!$this->getBranchSchoolService()->saveFirstBranchSchoolUrlToSesson($userId, $request)) {
                        $this->getBranchSchoolService()->createBranchSchoolLearner(
                            $forbidden['user']['areaCode'], $userId);
                        $this->getBranchSchoolService()->saveFirstBranchSchoolUrlToSesson($userId, $request);
                    }
                    $targetUrl .= 'my/courses/learning';
                    $isLearner = true;
                }
            }
        }
        
        if ($request->isXmlHttpRequest()) {
            $content = array(
                'success' => true
            );
            return new JsonResponse($content, 200);
        }

        if (!$isLearner) {
            CameraController::markAsFaceDetected($request);
            $this->setAsAdmin($request);
        }

        $url->setTargetUrl($targetUrl);
        self::setCookieWithLongDate('targetUrl', $targetUrl);
        return $url;
    }

    public static function isAdmin($request)
    {
        return $request->cookies->get('isAdmin');
    }

    public static function setCookieWithLongDate($name, $value)
    {
        setcookie($name, $value, time()+94608000, '/'); //cookie 有效期 3年, 60*60*24*365*3秒
    }

    protected function getSettingService()
    {
        return ServiceKernel::instance()->createService('System.SettingService');
    }

    protected function getBranchSchoolService()
    {
        return ServiceKernel::instance()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }

    private function getAuthService()
    {
        return ServiceKernel::instance()->createService('User.AuthService');
    }

    private function setAsAdmin($request)
    {
        self::setCookieWithLongDate('isAdmin', true);
    }

    private function clearCookies()
    {
        $COOKIE_OVER_TIME = 10;

        setcookie('isAdmin','',time() - $COOKIE_OVER_TIME);
        setcookie('branchSchoolUrl','',time() - $COOKIE_OVER_TIME);
        setcookie('targetUrl','',time() - $COOKIE_OVER_TIME);
    }
}
