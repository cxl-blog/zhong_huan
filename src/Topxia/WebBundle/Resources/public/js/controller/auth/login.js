define(function(require, exports, module) {
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  require("placeholder")
  require("jquery.bootstrap-datetimepicker");
  var AreaCodeSelector = require('customwebbundle/area-code-selector');

  exports.run = function() {

    var validator = new Validator({
      element: '#login-form'
    });

    validator.addItem({
      element: '[name="_username"]',
      required: true
    });

    validator.addItem({
      element: '[name="_password"]',
      required: true
    });
    $('.receive-modal').click();

    new AreaCodeSelector({
      idCardOrMobileSelector: '#login_username',
      areaCodeSelector: '#login_areacode',
      areaCodeWrapper: '.areacode-select'
    });
  };

});