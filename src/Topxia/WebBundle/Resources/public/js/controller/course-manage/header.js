define(function(require, exports, module) {
    var Notify = require('common/bootstrap-notify');
    exports.run = function() {

        $('.course-publish-btn').click(function() {
            var type = $(":input[name='type']").val();

            var code = $("select[name='areaCode']").val();
            if(typeof(code) != 'undefined' && code == ''){
                Notify.danger('此课程没有关联行政区域，发布失败！');
                return false;
            }

            var certificate = $("select[name='certificationId']").val();
            if(typeof(certificate) != 'undefined' && certificate == ''){
                Notify.danger('此课程没有关联证书，发布失败！');
                return false;
            }
            
            if (!confirm('您真的要发布该课程吗？')) {
                return ;
            }

            if (window.location.href.indexOf("certification") != -1) {
                $url = $(this).data('url').replace('course', 'course/certification').replace('/manage', '');
                $.post($url, 
                    function(html){
                        if (html.indexOf('publishResultFailed') != -1) {
                            Notify.danger('此课程下没有创建课时，发布失败！');
                        } else {
                            window.location.reload();
                        }
                    }
                );
            } else {
                $.post($(this).data('url'), 
                    function(html){
                        window.location.reload();
                    }
                );
            }
            

        });
        $('.js-exit-course').on('click', function(){
            var self = $(this);
            $.post($(this).data('url'), function(){
                window.location.href = self.data('go');
            });
        });
    };

});