define(function(require, exports, module) {

    var Lazyload = require('echo.js');
    g_cookie = require('cookie');
    require('customwebbundle/controller/course-loop/certificationCourseNavigator');

    exports.run = function() {
        Lazyload.init();
        $('#live, #free').on('click', function(event) {
        	window.location.href = $(this).val();
        });
    };

});