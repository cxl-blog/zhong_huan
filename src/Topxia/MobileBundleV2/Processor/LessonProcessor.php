<?php
namespace Topxia\MobileBundleV2\Processor;

interface LessonProcessor
{
    public function getCourseLessons();
    public function getLesson();
    public function getLessonMaterial();

    /*
     * 	courseId
     *	lessonId
     *	token
     */
    public function learnLesson();

    /*
     * 	courseId
     *	lessonId
     *	token
     */
    public function unLearnLesson();

    public function getLearnStatus();

    public function getLessonStatus();

    public function getTestpaperInfo();

    public function getVideoMediaUrl();

    public function getCourseDownLessons();

    /**
     * 获取所有可学习的证书课程的课程课时信息(必须为云视频)
     * 返回格式见 手机api说明.md
     */
    public function getAllLearnableCertificationCourseVideoLessons();

    public function getLocalVideo();
}