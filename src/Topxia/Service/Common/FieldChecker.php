<?php
namespace Topxia\Service\Common;

class FieldChecker
{
    public static function checkFieldName($name)
    {
        $formatedName = str_replace('.', '', str_replace('_', '', $name));
        if (!ctype_alnum($formatedName)) {
            throw new \InvalidArgumentException('Field name is invalid.');
        }

        return true;
    }
}