<?php
namespace Topxia\Service\Common;

use Topxia\Service\Common\ServiceException;
use Topxia\Service\Common\NotFoundException;
use Topxia\Service\Common\AccessDeniedException;
use Topxia\Service\User\CurrentUser;
use Topxia\Service\Util\HTMLPurifier;

abstract class BaseService
{

    protected function createService($name)
    {
        return $this->getKernel()->createService($name);
    }

    protected function createDao($name)
    {
        return $this->getKernel()->createDao($name);
    }

    protected function getKernel()
    {
        return ServiceKernel::instance();
    }

    public function getCurrentUser()
    {
        return $this->getKernel()->getCurrentUser();
    }

    public function getEnvVariable($key = null)
    {
        return $this->getKernel()->getEnvVariable($key);
    }

    public function getDispatcher()
    {
        return ServiceKernel::dispatcher();
    }

    public function setting($name, $default = '')
    {
        $names = explode('.', $name);
        $setting = $this->createService('System.SettingService')->get($names[0]);
        if(empty($names[1])) {
            return empty($setting) ? $default : $setting;
        } 
        return empty($setting[$names[1]]) ? $default : $setting[$names[1]];
    }
    
    protected function fetchCached()
    {
        $args     = func_get_args();
        $callback = array_pop($args);

        $key = "{$this->getServiceCache()}:service:".array_shift($args);
        $redis = $this->getKernel()->getRedis();

        if ($redis) {
            $data = $redis->get($key);

            if ($data) {
                return $data;
            }
        }

        $data = call_user_func_array($callback, $args);

        if ($redis) {
            $redis->setex($key, 2 * 60 * 60, $data);
        }

        return $data;
    }

    protected function dispatchEvent($eventName, $subject)
    {
        if ($subject instanceof ServiceEvent) {
            $event = $subject;
        } else {
            $event = new ServiceEvent($subject);
        }
        return $this->getDispatcher()->dispatch($eventName, $event);
    }

    protected function purifyHtml($html, $trusted = false)
    {
        if (empty($html)) {
            return '';
        }

        $config = array(
            'cacheDir' => $this->getKernel()->getParameter('kernel.cache_dir') .  '/htmlpurifier',
            'safeIframeDomains' => $this->setting('security.safe_iframe_domains', array())
        );

        $purifier = new HTMLPurifier($config);

        return $purifier->purify($html, $trusted);
    }

    protected function createServiceException($message = 'Service Exception', $code = 0)
    {
        throw new ServiceException($message, $code);
    }

    protected function createAccessDeniedException($message = 'Access Denied', $code = 0)
    {
        throw new AccessDeniedException($message, null, $code);
    }

    protected function createNotFoundException($message = 'Not Found', $code = 0)
    {
        throw new NotFoundException($message, $code);
    }

}