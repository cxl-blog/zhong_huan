<?php
namespace Topxia\Service\Beanstalk\Tests;

use Topxia\Service\Common\BaseTestCase;
use Topxia\Service\Beanstalk\BeanstalkdClientFactory;
use Exception;

class BeanstalkdClientFactoryTest extends BaseTestCase
{
    public function testMqReadWrite()
    {
        $tubeName = 'watchTimeWorker';
        $client = BeanstalkdClientFactory::getClient($tubeName);

        if ($client) {
            $data = array(
                'userId' => 2,
                'lessonId' => 1,
                'time' => 900
            );
            $client->put(0, 0, 100, json_encode($data));

            $configFile = $this->getContainer()->getParameter('kernel.root_dir').'/config/mq.php';

            $config  = include $configFile;

            $client->watch($tubeName);
            $job = $client->reserve();
                      
            $fetchResult = json_decode($job['body'], true);
            $this->assertArrayEquals($data, $fetchResult);
        }
    }

    public function testMqReadWriteWithNonExistTube()
    {
        $tubeName = 'nonExistTube';
        try {
            $client = BeanstalkdClientFactory::getClient($tubeName);
            $this->assertEquals($client, false);
        } catch (Exception $e) {
            $this->assertEquals(get_class($e), 'Topxia\Service\Beanstalk\Exception\NoSuchTubeException');
        }
    }
}
