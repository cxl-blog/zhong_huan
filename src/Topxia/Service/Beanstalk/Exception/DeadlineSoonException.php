<?php

namespace Topxia\Service\Beanstalk\Exception;

use Exception;

class DeadlineSoonException extends Exception
{
}
