<?php
namespace Topxia\Service\Beanstalk;

use Topxia\Service\Beanstalk\Exception\NoSuchTubeException;

class BeanstalkdClientFactory
{
    private static $clientsArray = array();  //key = tubeName, value 为 beanstalkd 中的 client

    public static function getClient($tubeName)
    {
        if (!isset(self::$clientsArray[$tubeName])) {
            $configFile = __DIR__.'/../../../../app/config/mq.php';
            if (file_exists($configFile)) {
                $config     = include $configFile;
                $client = new Client($config['message_server']);
                $client->connect();
                if (empty($config['tubes'][$tubeName])) {
                    throw new NoSuchTubeException();
                }
                $client->useTube($tubeName);
                self::$clientsArray[$tubeName] = $client;
            } else {
                self::$clientsArray[$tubeName] = false;
            }
        }

        return self::$clientsArray[$tubeName];
    }
}
