<?php

namespace Topxia\Service\Course\Worker;

use Topxia\Service\Plumber\BaseWorker;

class LessonLearnTimeWorker extends BaseWorker
{
    public function process($data)
    {
        $this->getCourseService()->waveLearningTime($data['userId'], $data['lessonId'], $data['time']);
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }
}
