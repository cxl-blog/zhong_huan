<?php

namespace Topxia\Service\Course\Worker;

use Topxia\Service\Plumber\BaseWorker;

class LessonWatchTimeWorker extends BaseWorker
{
    public function process($data)
    {
        $this->getCourseService()->waveWatchingTime($data['userId'], $data['lessonId'], $data['time']);
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }
}
