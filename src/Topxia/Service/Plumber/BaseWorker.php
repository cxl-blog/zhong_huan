<?php

namespace Topxia\Service\Plumber;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\DriverManager;
use Topxia\Service\Common\ServiceKernel;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\ParameterBag;

abstract class BaseWorker implements IWorker 
{
    private $serviceKernel;
    private $logger;

    protected function initKernel()
    {
        if (empty($this->serviceKernel)) {
            $paramaters = Yaml::parse(file_get_contents(__DIR__.'/../../../../app/config/parameters.yml'))['parameters'];
            $paramaters['kernel.root_dir'] = __DIR__.'/../../../../app';
            $connection = DriverManager::getConnection(array(
                'wrapperClass' => 'Topxia\Service\Common\Connection',
                'dbname'       => $paramaters['database_name'],
                'user'         => $paramaters['database_user'],
                'password'     => $paramaters['database_password'],
                'host'         => $paramaters['database_host'],
                'driver'       => $paramaters['database_driver'],
                'charset'      => 'utf8'
            ));

            $kernel = ServiceKernel::create('dev', false);
            $kernel->setParameterBag(new ParameterBag($paramaters));
            $kernel->setConnection($connection);

            $this->serviceKernel = $kernel;
        }
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute($data)
    {
        try {
            $this->initKernel();
            $this->process($data['body']);
            return IWorker::FINISH;
        } catch (\Exception $e) {
            $this->logger->error("job #{$job['id']} execute error, job is burried.  error message: {$e->getMessage()}", $job);
            return IWorker::BURY;
        }
        
    }

    public function getServiceKernel()
    {
        return $this->serviceKernel;
    }

    abstract public function process($data);
}
