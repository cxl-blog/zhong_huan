<?php
namespace Custom\Common\Exception;

class IpInvalidException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('ip invalid');
    }
}
