<?php
namespace Custom\Common\Exception;

class CertificationNotRevokedException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('certification not revoked');
    }
}
