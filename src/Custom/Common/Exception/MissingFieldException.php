<?php
namespace Custom\Common\Exception;

class MissingFieldException extends \RuntimeException
{
    public function __construct($fieldName)
    {
        parent::__construct('missing field: ' . $fieldName);
    }
}
