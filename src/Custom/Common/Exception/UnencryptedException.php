<?php
namespace Custom\Common\Exception;

class UnencryptedException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('can not unencrypted');
    }
}
