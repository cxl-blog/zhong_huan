<?php
namespace Custom\Common\Exception;

class ArgumentNotJsonException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('argument is not json');
    }
}
