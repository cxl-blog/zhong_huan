<?php
namespace Custom\Common\Exception;

class UnOfflineCompleteCourseException extends \RuntimeException
{
    public function __construct()
    {
        parent::__construct('can not unoffline complete an offline completed course.');
    }
}
