<?php
namespace Custom\Common\Util;

use Topxia\Component\Payment\Payment;
use Custom\Common\Constants\Weixin;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Service\Order\OrderProcessor\OrderProcessorFactory;

class PublicWeixinPayment {
    
    protected $payment = 'wxpay_public';

    /**
     * 创建微信支付的订单并返回url
     * 返回 
     * {
     *    url: 'weixin://wxpay/bizpayurl?pr=RfuDiSw', 
     *    prepayId: 'wx20160912180447cecd53e4d50815516213',
     *    sign: '10BE80C6E75562D5AB56ACB2DC87AFDC',
     *    nonceStr: 'dNxHZFzHqseuGfZo',
     *    order: order表信息
     * }
     */
    public function submitPayment($controller, $order, $settings, $deviceInfo)
    {
        $this->controller = $controller;
        $requestParams = array(
            'notifyUrl' => $controller->generateUrl('pay_notify', array('name' => 'wxpay'), true),
        );
        if (isset($deviceInfo)) {
            $requestParams['device_info'] =  $deviceInfo;
        }
        $paymentRequest = $this->createPaymentRequest($order, $requestParams, $settings);
        $paymentRequest->form();
        $returnXml = $paymentRequest->unifiedOrder();
        if (!$returnXml) {
            throw new \RuntimeException("xml数据异常！");
        }

        $returnArray = $paymentRequest->fromJsApiXml($returnXml);
        if ($returnArray['return_code'] == 'SUCCESS') {
            $timeStamp = time();
            $array = array(
                'appId' => $returnArray['appid'],
                'timeStamp' => $timeStamp,
                'nonceStr' => $returnArray['nonce_str'],
                'package' => "prepay_id=" . $returnArray['prepay_id'],
                'signType' => 'MD5'
            );
            $paySign = $paymentRequest->signParams($array);
            $array['paySign'] = strtoupper($paySign);
            
            return $array;
        } else {
            throw new \RuntimeException($returnArray['return_msg']);
        }
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('Custom:User.UserService');
    }

    private function createPaymentRequest($order, $requestParams, $settings)
    {
        $options       = $this->getPaymentOptions($order['payment'], $settings);
        $user = $this->getUserService()->getUser($order['userId']);

        $options['trade_type'] = 'JSAPI';
        $options['openid'] = $order['openid'];
        $request       = Payment::createRequest('wxpay', $options);
        $processor     = $this->getOrderProcessor($order);
        $targetId      = isset($order["targetId"]) ? $order['targetId'] : $order['id'];
        $requestParams = array_merge($requestParams, array(
            'orderSn'     => $order['sn'],
            'time_start' => date("YmdHis"),
            'time_expire' => date("YmdHis", time()+600),
            'userId'      => $order['userId'],
            'title'       => $order['title'],
            'targetTitle' => $processor->getTitle($targetId),
            'summary'     => '',
            'note'        => $processor->getNote($targetId),
            'amount'      => $order['amount'],
            'targetType'  => $order['targetType']
        ));
        return $request->setParams($requestParams);
    }
 
    private function getPaymentOptions($payment, $settings)
    {
        if (empty($settings)) {
            throw new \RuntimeException('支付参数尚未配置，请先配置。');
        }

        if (empty($settings['enabled'])) {
            throw new \RuntimeException("支付模块未开启，请先开启。");
        }

        if (empty($settings[$payment.'_enabled'])) {
            throw new \RuntimeException("支付模块({$payment})未开启，请先开启。");
        }

        if (empty($settings["{$payment}_key"]) || empty($settings["{$payment}_secret"])) {
            throw new \RuntimeException("支付模块({$payment})参数未设置，请先设置。");
        }

        return array(
            'key'    => $settings["{$payment}_key"],
            'secret' => $settings["{$payment}_secret"]
        );
    }

    private function getOrderProcessor($order)
    {
        return OrderProcessorFactory::create($order["targetType"]);
    }

}
