<?php

namespace Custom\Common\Util;

use DateTime;
use DateTimeZone;
use Topxia\Common\TimeMachine;

class DateUtils {
    
    private $timezone;
    private static $formatType = 'Y-m-d';
    private static $format;
    private static $fullFormat;

    public static function number2Str($number)
    {
        if (self::$format == null) {
            self::$format = new TimeMachine('Asia/Shanghai');
        }
        return self::$format->format(self::$formatType, $number);
    }

    public static function number2StrWithSeconds($number)
    {
        if (self::$fullFormat == null) {
            self::$fullFormat = new TimeMachine('Asia/Shanghai');
        }
        return self::$fullFormat->format('Y-m-d H:i:s', $number);
    }

    /**
     * 格式化双重数据中的时间, 将数字改为日期字符串
     * 如 将下列 数组中的buyTime 改为 日期字符串
     * array(
     *      0 => array(
     *          'courseId' => '3',
     *          'partIndex' => '0',
     *          'courseName' => 'test',
     *          'certificationId' => '3',
     *          'buyTime' => '1462497706'
     *      ) ...
     *  )
     *  可通过方法 arrayNumber2Str($array, 'buyTime');
     *  无返回
     */
    public static function arrayNumber2Str($array, $formatedArrayKey)
    {
        foreach ($array as $key => $single) {
            $formatedDateStr = self::number2Str($single[$formatedArrayKey]);
            $single[$formatedArrayKey] = $formatedDateStr;
            $array[$key] = $single;
         }

         return $array;
    }

    public static function strtotime($timeRule, $currentTime)
    {
        if ($timeRule != null) {
            return strtotime($timeRule, $currentTime);
        }
        return $currentTime;
    }

    /**
     * $endDateNum - $beginDateNum = 多少天
     */
    public static function diffDays($beginDateNum, $endDateNum)
    {
        return ceil(($endDateNum - $beginDateNum) / 86400);  //一天 = 86400秒
    }

    /**
     * 获取所给时间的昨天的最后1秒 (即 23:59:59 )
     * @return 
     * {
     *     'dateNum' => 1488297599,
     *     'dateString' => '2017-02-28'
     * }
     */
    public static function getYesterdayEndDateTime($dateNum)
    {
        $result = array();

        $yesterdayEndTimeNum = $dateNum -1;
        $yesterdayEndTimeStr = self::number2Str($yesterdayEndTimeNum);

        $result['dateNum'] = $yesterdayEndTimeNum;
        $result['dateString'] = $yesterdayEndTimeStr;
        return $result;
    }

    public static function caculateTimeDelay($startMicroTime)
    {
        $endMicroTime = microtime();
        $startTimeArray = explode(" ", $startMicroTime);
        $endTimeArray = explode(" ", $endMicroTime); 

        //microtime 格式如 0.81573100 1467278932, 
        //  空格前面是 小数, 后面是整数, 判断2个时间差多少秒, 前面减前面, 后面减后面之和
        $totalTime = $endTimeArray[0] - $startTimeArray[0] + $endTimeArray[1] - $startTimeArray[1];
        $timeCost = sprintf("%s", $totalTime); 
        $startTimeStr = self::number2StrWithSeconds($startTimeArray[1]);
        return array('timeCost' => $timeCost, 'startTimeStr' => $startTimeStr);
    }

}