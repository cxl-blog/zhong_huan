<?php
namespace Custom\Common\Util;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell_DataType;

class PHPExcelToolkit
{

	public static function export($data,$header){

        $objPHPExcel = new PHPExcel();
        $objProps = $objPHPExcel->getProperties();

        //设置表头
        $key = ord("A");
        
        foreach($header as $v ){
            $colum = chr($key);
            $objPHPExcel->setActiveSheetIndex(0) ->setCellValue($colum.'1', $v);        
            $key += 1;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        $column = 2;
        $objActSheet = $objPHPExcel->getActiveSheet();
       
        foreach($data as $key => $rows){ //行写入
            $span = ord("A");
            foreach($rows as $keyName=>$value){// 列写入
                $j = chr($span);
                
                //设置单元格格式为字符
                $objActSheet->setCellValueExplicit($j.$column, $value,PHPExcel_Cell_DataType::TYPE_STRING);
                    
                $span++;
            }
            $column++;
        }
       
        $objPHPExcel->setActiveSheetIndex(0);       
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');      
        return $objWriter;
    }

    public static function outPutExcel($filename,$data,$header)
    {
        if(empty($filename))
        {
            return false;
        }
        $objWriter = self::export($data,$header);
        $filename = iconv("utf-8", "gb2312", $filename);
        header('Content-Type: application/vnd.ms-excel;charset=UTF-8');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
}