<?php

namespace Custom\Common\Util;

use Custom\Common\Constants\LearnStatus;
use Topxia\Common\Paginator;

class CourseGroupUtils {

    /**
     * 分组, 先显示证书课程, 再显示普通课程, 支持分页
     * @param $learnStatus 学习状态, 见 LearnStatus.php
     * @param $environments
          array(
                'certificateService' => certificateSerivceImpl,
                'request'               => request,　//不填，则$startIndex必填
                'pageSize'              => 12,　　　//不填，则为12
                'startIndex'           => 11,           //不填，则$request必填
                'ignoredRevokedStatusArr' => array(LearnStatus::LEARNING),
                'addedRevokedStatusArr' => array(LearnStatus::LEARNING)
          )
     * @return
     * array(
            'courses'   => $courses,        //混合课程, 符合 $learnStatus 的证书课程 和 普通课程
            'paginator' => $paginator,
            'total'  => 123,   //总数
            'unpaidCertificationCourses' => $unpaidCertificationCourses  //待学证书课程
        )
     *
     */
    public static function getGroupedCourses($userId, $allNormalCourses, $learnStatusArr, 
        $environments)
    {
        $allCertificationCourses = $environments['certificateService']->getCertificationsForUser($userId);

        $groupedCertificationCourses = self::getCertificationCourses(
            $allCertificationCourses, $learnStatusArr, $environments);
        return self::generateCourseParams($allCertificationCourses,
            $groupedCertificationCourses, $allNormalCourses, $environments);
    }

    public static function generateCourseParams($allCertificationCourses,
        $groupedCertificationCourses, $allNormalCourses, $environments) {
        $unpaidCertificationCourses = self::getCertificationCourses(
            $allCertificationCourses, array(LearnStatus::UN_PAID), array());

        $groupedCourses = array_merge($groupedCertificationCourses, $allNormalCourses);

        if (!isset($environments['pageSize'])) {
            $environments['pageSize'] = 12;
        }

        if (isset($environments['request'])) {
            $paginator = new Paginator(
                $environments['request'],
                count($groupedCourses),
                $environments['pageSize']//一页显示12门课程
            );
            $startIndex = $paginator->getOffsetCount();
        } else {
            $startIndex =$environments['startIndex'];
            $paginator = array();
        }
        $courses = array_slice($groupedCourses, $startIndex,
            $environments['pageSize']
        );
        return array(
            'courses'                    => $courses,
            'paginator'                  => $paginator,
            'unpaidCertificationCourses' => $unpaidCertificationCourses,
            'total'  => count($groupedCourses)
        );
    }

    public static function generateNormalAndCertificationCourse($courses, $userId, $certificateService, $isFavorite = false)
    {
        $allNormalCourses            = array();
        $checkedCertificationCourses = array();

        foreach ($courses as $key => $course) {
            if ($course['type'] != 'certification') {
                array_push($allNormalCourses, $course);
            } else {
                array_push($checkedCertificationCourses, $course);
            }
        }

        $allCertificationCourses = $certificateService->getCertificationsForUser($userId);

        $groupedCertificationCourses = array();
        $courseIdAndLoopTimeArray    = array();

        foreach ($checkedCertificationCourses as $checkedCourse) {
            foreach ($allCertificationCourses as $certificationCourse) {
                if ($checkedCourse['id'] == $certificationCourse['id']) {
                    if ($isFavorite) {
//非收藏的课程要全部显示, 收藏课程必须比较currentLoopTime
                        if ($checkedCourse['currentLoopTime'] == $certificationCourse['currentLoopTime']) {
                            array_push($groupedCertificationCourses, $certificationCourse);
                        }
                    } else {
                        $currentCourseIdLoopTime = $certificationCourse['id'].'_'.$certificationCourse['currentLoopTime'];
                        if (!in_array($currentCourseIdLoopTime, $courseIdAndLoopTimeArray)) {
                            array_push($groupedCertificationCourses, $certificationCourse);
                            array_push($courseIdAndLoopTimeArray, $currentCourseIdLoopTime);
                        }
                    }
                }
            }
        }

        return array(
            'allNormalCourses'            => $allNormalCourses,
            'groupedCertificationCourses' => $groupedCertificationCourses
        );
    }

    /*
     * 获取证书课程中指定 学习状态 的所有证书
     * @param $certificationCourses  二维数组, 值参考 
     *              CertificateService::getCertificationsForUser中 2.) 只获取证书课程 说明
     * @param $learningStatus 值范围 见 LearnStatus.php
     */
    private static function getCertificationCourses($certificationCourses, $learningStatusArray, $environments)
    {
        $result = array();
        $ignoredRevokedStatusArr = isset($environments['ignoredRevokedStatusArr']) 
                ? $environments['ignoredRevokedStatusArr'] : array();
        $addedRevokedStatusArr = isset($environments['addedRevokedStatusArr']) 
                ? $environments['addedRevokedStatusArr'] : array();        

        foreach ($certificationCourses as $singleCourse) {
            if ($singleCourse['isRevoked']) {
                if (in_array($singleCourse['learningStatus'], $addedRevokedStatusArr)) {
                    array_push($result, $singleCourse);
                    continue;
                }

                if (in_array($singleCourse['learningStatus'], $ignoredRevokedStatusArr)) {
                    continue;
                }
            }

            if (in_array($singleCourse['learningStatus'], $learningStatusArray)) {
                array_push($result, $singleCourse);
            }
        }

        return $result;
    }

}