<?php

namespace Custom\Common\Util;

use Custom\Common\Constants\LearnMode;
use Symfony\Component\HttpFoundation\Request;

class CourseLoopUtils
{
    public static $START_LOOP_TIME = 1;

    /**循环购买的次数 */
    public static $LOOP_TIMES = 30;

    /**
     * 生成开始时间的数字格式
     * array (
     *     array (
     *         startTimeNum => 1463460846,    //课程的开始时间
     *         certificationStartTimeNum => 1463460846,   //证书的开始时间, 即第一门课程的开始时间
     *         currentLoopTime => 1,  //循环购买中, 属于第几次
     *         currentLoopMode => LearnMode
     *     )
     * )
     * @return
     */
    public static function caculateStartTimeNumArray($courseInfo, $certification, $allLoopModes, $existedCourses)
    {
        $startTimeNumArray         = array();
        $nextLoopInfo = self::caculateNextLoopInfo($existedCourses, $certification);
        $certificationStartTimeNum = $nextLoopInfo['nextStartPeriod'];
        $now                       = strtotime('now');
        $partIndex = $courseInfo['partIndex'];

        for ($i = 0; $i < self::$LOOP_TIMES; $i++) {
            $currentLoopTime = $i + $nextLoopInfo['nextLoopTime'];

//循环购买次数 1 表示第一次
            if (isset($allLoopModes[$currentLoopTime])) {
                $currentLoopMode = $allLoopModes[$currentLoopTime];
            } else {
                $currentLoopMode = $allLoopModes['latestMode'];
            }

            if (isset($currentLoopMode['startTime_'.$partIndex])) {
                $addedStartTime = $currentLoopMode['startTime_'.$partIndex];
            } else {
                $addedStartTime = null;
            }

            if ($now < $certificationStartTimeNum) {
                break;
            }

            $startTimeNum         = DateUtils::strtotime($addedStartTime, $certificationStartTimeNum);
            $currentStartTimeInfo = array(
                'startTimeNum'              => $startTimeNum,
                'certificationStartTimeNum' => $certificationStartTimeNum,
                'currentLoopMode'           => $currentLoopMode,
                'currentLoopTime'           => $currentLoopTime //循环购买次数 1 表示第一次
            );
            array_push($startTimeNumArray, $currentStartTimeInfo);
            $certificationStartTimeNum = DateUtils::strtotime(LearnMode::getExpireTime($currentLoopMode['months']), $certificationStartTimeNum);
        }

        return $startTimeNumArray;
    }

    /**
     * certificationCourseNavigator.js 内生成 cookie
     */
    public static function getCurrentLoopTime(Request $request, $userId, $courseId)
    {
        $cookieKey   = "{$userId}_{$courseId}_currentLoopTime";
        $cookieValue = $request->cookies->get($cookieKey);
        if (empty($cookieValue)) {
            $cookieValue = self::$START_LOOP_TIME;
        }
        return $cookieValue;
    }

    private static function caculateNextLoopInfo($existedCourses, $certification)
    {
        if (empty($existedCourses)) {
            return array(
                'nextLoopTime'  => $certification['startLoopTime'],
                'nextStartPeriod' => $certification['awardTime']
            );
        } else {
            $nextLoopTime = $certification['startLoopTime'] - 1;
            $nextStartPeriod = 0;
            foreach ($existedCourses as $key => $course) {
                if ($nextLoopTime < $course['currentLoopTime']) {
                    $nextLoopTime = $course['currentLoopTime'];
                    $nextStartPeriod = $course['certificationEndPeriod'] + 1;   //结束时间加一秒后变为下一循环开始时间
                }
            }
            return array(
                'nextLoopTime'  => $nextLoopTime + 1, // 当前循环的最大值加1为下一循环的开始次数
                'nextStartPeriod' => $nextStartPeriod
            );
        }
    }
}
