<?php
namespace Custom\Common\Util;

use Topxia\Service\Common\ServiceKernel; 

class StringUtils
{
    const STRING_START_POSITION = 0;
    const STRING_OFFSET_FOURTH_POSITION = 4;
    const STRING_LAST_OFFSET_THIRD_POSITION = -3;


    /**
     *  将数组中的value 去空, 只支持一维数组
     */
    public static function trimStrArray($strArray)
    {
        foreach ($strArray as $key => $value) {
            $strArray[$key] = trim($value);
        }
        return $strArray;
    }

    /**
     * 将长度大于11的字符串格式化成形如1356****092的形式,
     */
    public static function formatStringForSecurity($string)
    {
        if (self::isStringNeedFormat($string)) {
            $formattedString = substr($string, self::STRING_START_POSITION, self::STRING_OFFSET_FOURTH_POSITION) . '****' . substr($string, self::STRING_LAST_OFFSET_THIRD_POSITION);

            return $formattedString;
        }

        return $string;
    }

    public static function formatStringForRegular($string)
    {
        if (self::isStringNeedFormat($string)) {
            $formattedString = substr($string, self::STRING_START_POSITION, self::STRING_OFFSET_FOURTH_POSITION) . '\*\*\*\*' . substr($string, self::STRING_LAST_OFFSET_THIRD_POSITION);

            return $formattedString;
        }

        return $string;
    }

    public static function isStringNeedFormat($string)
    {
        $rule = '/^1\d{10}/';

        return preg_match($rule, $string);
    }

    public static function parseAts($fields)
    {
        preg_match_all('/@([\x{4e00}-\x{9fa5}\w]{2,16})/u', $fields['content'], $matches);

        if (!empty($fields['replyOriginUsername'])) {
            array_push($matches[1], $fields['replyOriginUsername']);
        }

        $users = self::getUserService()->findUsersByNicknames(array_unique($matches[1]));

        if (empty($users)) {
            return array();
        }

        $ats = array();

        foreach ($users as $user) {
            $ats[$user['nickname']] = $user['id'];
        }

        return $ats;
    }

    public static function getUserService()
    {
        return self::getServiceKernel()->createService('Custom:User.UserService');
    }

    public static function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
