<?php

namespace Custom\Common\Util;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\CourseLoopUtils;

class CourseHeaderUtils
{
    public static function getHeaderInfos($contoller, $member, $user, $course, $request, $infos = array())
    {

        $isInstalledVip = $contoller->get('topxia.twig.web_extension')->isPluginInstalled('Vip');
        $isInstalledDiscount = $contoller->get('topxia.twig.web_extension')->isPluginInstalled("Discount");
        $isSettingVip = $contoller->get('topxia.twig.web_extension')->getSetting('vip.enabled', null);

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $course['id']);
        $hasFavorited = self::getCourseService()->hasFavoritedCourse($course['id'], $currentLoopTime);
        $certificationCourse = self::getCertificateService()->getCertificationsForUser($user['id'], false, $course['id'],$currentLoopTime)?:null;

        $isAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']) || self::getCourseService()->canBranchAdminMangeCourse($course['id']);
        $certificationForGuest= self::getHeaderService()->getCertficationForGuest($isAdmin, $user, $course);
        $certification = self::getHeaderService()->getCertification($isAdmin, $user, $course);
        $learnedHours = self::getHeaderService()->getLearnedHours($isAdmin, $user, $course, $certificationCourse, $currentLoopTime);

        $isTestPassed = $certificationCourse['isTestpaperPassed'];

        $testpaperItem = self::getHeaderService()->getTestpaperItem($isTestPassed, $certificationCourse, $course);
        
        $nextLearnLesson = self::getHeaderService()->getNextLearnLesson($isTestPassed, $certificationCourse, $course, $currentLoopTime, $testpaperItem, $member);

        $isLogin = $user->isLogin();
        $dateForGuest = self::getHeaderService()->getDateForGuest($certificationCourse);

        if (($course['discountId'] > 0) && $isInstalledDiscount) {
            $course['discountObj'] = self::getDiscountService()->getDiscount($course['discountId']);
        }

        $userVipStatus = $courseVip = null;

        if ($isInstalledVip && $isSettingVip) {
            $courseVip = $course['vipLevelId'] > 0 ? self::getLevelService()->getLevel($course['vipLevelId']) : null;

            if ($courseVip) {
                $userVipStatus = self::getVipService()->checkUserInMemberLevel($user['id'], $courseVip['id']);
            }
        }

        $previewLesson = self::getCourseService()->searchLessons(array('courseId' => $course['id'], 'type' => 'video', 'free' => 1), array('seq', 'ASC'), 0, 1);
        $breadcrumbs = self::getCategoryService()->findCategoryBreadcrumbs($course['categoryId']);
        $headerInfos = array(
            'hasFavorited' => $hasFavorited,
            'certificationCourse' => $certificationCourse,
            'isAdmin' => $isAdmin,
            'certificationForGuest' => $certificationForGuest,
            'certification' => $certification,
            'learnedHours' => $learnedHours,
            'isTestPassed' => $isTestPassed,
            'testpaperItem' => $testpaperItem,
            'nextLearnLesson' => $nextLearnLesson,
            'isLogin' => $isLogin,
            'dateForGuest' => $dateForGuest,
            'previewLesson' => empty($previewLesson) ? null : $previewLesson[0],
            'breadcrumbs' => $breadcrumbs,
            'userVipStatus' => $userVipStatus,
            'courseVip' => $courseVip,
            'course' => $course,
            'learnPercent' => self::getCourseService()->getPercent($course['totalHours'], $learnedHours),
            'sumHours' => $course['totalHours'],
        );

        return array_merge($infos, $headerInfos);
    }

    public static function getNormalHeaderInfos($contoller, $user, $course, $member,$infos = array())
    {
        $isInstalledVip = $contoller->get('topxia.twig.web_extension')->isPluginInstalled('Vip');
        $isInstalledDiscount = $contoller->get('topxia.twig.web_extension')->isPluginInstalled("Discount");
        $isSettingVip = $contoller->get('topxia.twig.web_extension')->getSetting('vip.enabled', null);

        if (($course['discountId'] > 0)&&$isInstalledDiscount){
            $course['discountObj'] = self::getDiscountService()->getDiscount($course['discountId']);
        }

        $hasFavorited = self::getCourseService()->hasFavoritedCourse($course['id']);

        $userVipStatus = $courseVip = null;
        if ($isInstalledVip && $isSettingVip) {
            $courseVip = $course['vipLevelId'] > 0 ? self::getLevelService()->getLevel($course['vipLevelId']) : null;
            if ($courseVip) {
                $userVipStatus = self::getVipService()->checkUserInMemberLevel($user['id'], $courseVip['id']);
            }
        }

        $nextLearnLesson = $member ? self::getCourseService()->getUserNextLearnLesson($user['id'], $course['id']) : null;
        $learnProgress = $member ? self::calculateUserLearnProgress($course, $member) : null;

        $previewLesson = self::getCourseService()->searchLessons(array('courseId' => $course['id'], 'type' => 'video', 'free' => 1), array('seq', 'ASC'), 0, 1);

        $breadcrumbs = self::getCategoryService()->findCategoryBreadcrumbs($course['categoryId']);
        $headerInfos = array(
            'hasFavorited' => $hasFavorited,
            'courseVip' => $courseVip,
            'userVipStatus' => $userVipStatus,
            'nextLearnLesson' => $nextLearnLesson,
            'learnProgress' => $learnProgress,
            'previewLesson' => empty($previewLesson) ? null : $previewLesson[0],
            'breadcrumbs' => $breadcrumbs,
            'course' => $course
        );

        return array_merge($infos, $headerInfos);
    }

    public static function getCertificateService()
    {
        return self::getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    public static function getHeaderService()
    {
        return self::getServiceKernel()->createService('Custom:Header.HeaderService');
    }

    public static function getCategoryService()
    {
        return self::getServiceKernel()->createService('Taxonomy.CategoryService');
    }

    public static function getCourseService()
    {
        return self::getServiceKernel()->createService('Course.CourseService');
    }

    public static function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    public static function getLevelService()
    {
        return self::getServiceKernel()->createService('Vip:Vip.LevelService');
    }

    public static function getDiscountService()
    {
        return self::getServiceKernel()->createService('Discount:Discount.DiscountService');
    }

    public static function getVipService()
    {
        return self::getServiceKernel()->createService('Vip:Vip.VipService');
    }

    public static function calculateUserLearnProgress($course, $member)
    {
        if ($course['lessonNum'] == 0) {
            return array('percent' => '0%', 'number' => 0, 'total' => 0);
        }

        $percent = intval($member['learnedNum'] / $course['lessonNum'] * 100) . '%';

        return array (
            'percent' => $percent,
            'number' => $member['learnedNum'],
            'total' => $course['lessonNum']
        );
    }
}