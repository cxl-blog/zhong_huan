<?php

namespace Custom\Common\Util;

class CompressionUtils 
{
    
    /**
     * 将字符串 用 gzip 压缩后, 再用 base64转码
     */
    public static function encodeByGzipBase64($str)
    {
        return  base64_encode(gzencode($str));
    }

    public static function decodeByBase64Gzip($str)
    {
        return gzdecode(base64_decode($str));
    }
}