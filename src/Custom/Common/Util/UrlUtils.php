<?php
namespace Custom\Common\Util;

class UrlUtils
{
    /**
     *  如将 http://ip:port/admin 转化为 http://ip:port/
     *           https://domain/admin 转化为 https://domain/
     */
    public static function getUrlPrefix($url)
    {
        $segs   = explode('/', $url);
        $result = $segs[0].'//'.$segs[2].'/';
        return $result;
    }

    /**
     *  如将 http://ip:port/admin 转化为 admin
     */
    public static function getUrlSuffix($url)
    {
        $segs   = explode('/', $url);
        $result = '';
        $isFirstJoinSeg = true;
        $startIndex = 3; //后面3位才是 后缀
        if (isset($segs[$startIndex])) {   
            for ($i=$startIndex; $i < count($segs); $i++) {
                if ($isFirstJoinSeg) {
                    $isFirstJoinSeg = false;
                } else {
                    $result .= '/';
                }
                $result .= $segs[$i];
            }
        }
        return $result;
    }

    /**
      * 将 files/default/2016/06-23/112339b481b6549876.jpg?6.16.2.002 
      *   转化为 public://default/2016/06-23/112339b481b6549876.jpg?6.16.2.002
      */
    public static function formatCoursePictures($course)
    {
        $course['smallPicture'] = 'public://' . explode('files/', $course['smallPictureUrl'])[1];
        $course['middlePicture'] = 'public://' . explode('files/', $course['middlePictureUrl'])[1];
        $course['largePicture'] = 'public://' . explode('files/', $course['largePictureUrl'])[1];
        return $course;
    }
}
