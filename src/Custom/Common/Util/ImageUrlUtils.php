<?php

namespace Custom\Common\Util;

class ImageUrlUtils {
    
    /**
     * 将格式如 public://default/2016/05-06/201151716a8a332687.jpg 的图片url 转化为
     * files/default/2016/05-06/201151716a8a332687.jpg
     */
    public static function getImageRelativePath($url)
    {
        return 'files/'.explode('public://', $url)[1];
    }

    /**
     * 将 /var/www/edusoho/app/../web/files/camera/570de452b1812.png
     * 的url裁剪为 /files/camera/570de452b1812.png
     */
    public static function getImagePath($faceImgPath)
    {
        $result = '';

        if (!empty($faceImgPath)) {
            $result = '/files'.explode('/files', $faceImgPath)[1];
        }

        return $result;
    }

    public static function addPictureUrlToCourses($courses)
    {
        $allCourses = array();
        foreach ($courses as $course) {
            if (!empty($course['smallPicture']) && empty($course['smallPictureUrl'])) {
                $course['smallPictureUrl']  = self::getImageRelativePath($course['smallPicture']);
                $course['middlePictureUrl'] = self::getImageRelativePath($course['middlePicture']);
                $course['largePictureUrl']  = self::getImageRelativePath($course['largePicture']);
            }
            $allCourses[] = $course;
        }
        return $allCourses;
    }

    public static function addPictureUrlToCourse($course)
    {
        if (!empty($course['smallPicture']) && empty($course['smallPictureUrl'])) {
            $course['smallPictureUrl']  = self::getImageRelativePath($course['smallPicture']);
            $course['middlePictureUrl'] = self::getImageRelativePath($course['middlePicture']);
            $course['largePictureUrl']  = self::getImageRelativePath($course['largePicture']);
        }
        return $course;
    }

}