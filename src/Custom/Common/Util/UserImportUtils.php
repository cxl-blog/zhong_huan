<?php
namespace Custom\Common\Util;

use Topxia\Common\FileToolkit;

class UserImportUtils
{
    public static function isFileTypeCorrect($file, $errorMessage)
    {
        if (!is_object($file)) {
            $errorMessage = '请选择上传的文件';
            return $errorMessage;
        }

        if (FileToolkit::validateFileExtension($file, 'xls xlsx')) {
            $errorMessage = 'Excel格式不正确！';
            return $errorMessage;
        }

        return $errorMessage;
    }

    public static function isFileContentCorrect($excelFields, $necessaryFields, $rowTotal, $errorMessage)
    {
        if (!self::isStandardFormat($excelFields, $necessaryFields)) {
            $errorMessage = '请参考示例文件填写表格，勿擅自修改字段！';
            return $errorMessage;
        }

        if ($rowTotal > 1000) {
            $errorMessage = 'Excel超过1000行数据!';
            return $errorMessage;
        }

        return $errorMessage;
    }

    public static function isStandardFormat($excelFields, $necessaryFields)
    {
        $result = array_diff($excelFields, $necessaryFields);
        return empty($result);
    }
}