<?php

namespace Custom\Common\Util;

class CertificateUtils 
{
    
    /**
     * 获取所给课程中的所有证书学制下的课程数
     * @return array 
     *      key 为 {$certificationId}:{$learnModeMonths}
     *      value 为 课程数
     */
    public static function generateCertLearnModeCourseCount($courses)
    {
        $certLearnModeCourseCount = array();
        foreach ($courses as $course) {
            $key = $course['certificationId'] . ':' . $course['learnModeMonths'];
            if (empty($certLearnModeCourseCount[$key])) {
                $count = 0;
            } else {
                $count = $certLearnModeCourseCount[$key];
            }

            $count ++;
            $certLearnModeCourseCount[$key] = $count;
        }
        return $certLearnModeCourseCount;
    }

    /**
     * 获取所给课程中的所有证书下的学制
     * @param certifications  当所给课程中不包含指定证书时, 给
     * @return array 
     *      key 为 {$certificationId}
     *      value 为 array({$learnModeMonths} => {$learnModeMonths})
     */
    public static function generateCertLearnModes($courses, $certifications)
    {
        $certLearnModes = array();
        foreach ($courses as $course) {
            if (in_array($course['certificationId'], $certLearnModes)) {
                $certLearnModes[$course['certificationId']] = array();
            }
            $certLearnModes[$course['certificationId']][$course['learnModeMonths']] 
                    = $course['learnModeMonths'];
        }

        foreach ($certifications as $cert) {
            if (empty($certLearnModes[$cert['certificationId']])) {
                $certLearnModes[$cert['certificationId']] = array();
            }
            $certLearnModes[$cert['certificationId']][$cert['learnModeMonths']] = $cert['learnModeMonths'];
        }

        return $certLearnModes;
    }
}