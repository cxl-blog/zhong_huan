<?php
namespace Custom\Common\Util;

use Topxia\Service\Common\ServiceKernel;

class WeChatUtils
{
    public static function getAccessTokenAndOpenId($appId, $appSecret, $code)
    {
        $url="https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appId."&secret=".$appSecret."&code=".$code."&grant_type=authorization_code";

        return json_decode(self::httpsRequest($url),true);
    }

    public static function getUserInfos($openid, $accessToken)
    {
        $url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$accessToken."&openid=".$openid."&lang=zh_CN";
        $response = self::httpsRequest($url);
        return json_decode($response, true);
    }

    public static function httpsRequest($url, $data = null)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }

    public static function convertUserInfo($infos)
    {
        $userInfo           = array();
        $userInfo['id']     = $infos['unionid'];
        $userInfo['name']   = $infos['nickname'];
        $userInfo['avatar'] = $infos['headimgurl'];
        $userInfo['accessToken'] = $infos['access_token'];

        if ($infos['sex'] == 1) {
            $userInfo['gender'] = 'male';
        } elseif ($infos['sex'] == 2) {
            $userInfo['gender'] = 'female';
        } else {
            $userInfo['gender'] = 'secret';
        }

        return $userInfo;
    }

    public static function handleWeChatNickName($nickname)
    {
        return preg_replace('/[^\x{4e00}-\x{9fa5}a-zA-z0-9_.]+/u', '', $nickname);
    }

    public static function handleUnionId($unionId)
    {
        return str_replace('-', '_', $unionId);
    }
}
