<?php

namespace Custom\Common\Util;

use Topxia\Component\Payment\Payment;
use Topxia\Service\Order\OrderProcessor\OrderProcessorFactory;
use Custom\Service\Order\OrderProcessor\CertificationCourseOrderProcessor;

class MobileWeixinPayment {
    
    protected $payment = 'wxpay_mobile';

    /**
     * 创建微信支付的订单并返回url
     * 返回 
     * {
     *    url: 'weixin://wxpay/bizpayurl?pr=RfuDiSw', 
     *    prepayId: 'wx20160912180447cecd53e4d50815516213',
     *    sign: '10BE80C6E75562D5AB56ACB2DC87AFDC',
     *    nonceStr: 'dNxHZFzHqseuGfZo',
     *    order: order表信息
     * }
     */
    public function submitPayment($controller, $order, $settings, $deviceInfo)
    {
        $this->controller = $controller;
        $requestParams = array(
            'notifyUrl' => $controller->generateUrl('pay_notify', array('name' => 'wxpay'), true),
        );
        if (isset($deviceInfo)) {
            $requestParams['device_info'] =  $deviceInfo;
        }

        $paymentRequest = $this->createPaymentRequest($order, $requestParams, $settings);

        $formRequest    = $paymentRequest->form();
        $params         = $formRequest['params'];

        $returnXml = $paymentRequest->unifiedOrder();

        if (!$returnXml) {
            throw new \RuntimeException("xml数据异常！");
        }

        $returnArray = $paymentRequest->fromXml($returnXml);
        if ($returnArray['return_code'] == 'SUCCESS') {
            return array(
                'appid' => $returnArray['appid'],
                'mch_id' => $returnArray['mch_id'],
                'prepayId' => $returnArray['prepay_id'],
                'sign' => $returnArray['sign'],
                'nonceStr' => $returnArray['nonce_str'],
                'timeStamp' => time(),
                'order' => $order
            );
        } else {
            throw new \RuntimeException($returnArray['return_msg']);
        }
    }

    protected function getOrderService()
    {
        return $this->controller->getService('Custom:Order.OrderService');
    }

    protected function getCourseService()
    {
        return $this->controller->getService('Custom:Course.CourseService');
    }

    private function createPaymentRequest($order, $requestParams, $settings)
    {
        $options       = $this->getPaymentOptions($order['payment'], $settings);
        $options['wxpay_account'] = $settings['wxpay_mobile_account'];
        $options['trade_type'] = 'APP';
        $request       = Payment::createRequest('wxpay', $options);
        $processor     = $this->getOrderProcessor($order);
        $targetId      = isset($order["targetId"]) ? $order['targetId'] : $order['id'];
        $requestParams = array_merge($requestParams, array(
            'orderSn'     => $order['sn'],
            'userId'      => $order['userId'],
            'title'       => $order['title'],
            'targetTitle' => $processor->getTitle($targetId),
            'summary'     => '',
            'note'        => $processor->getNote($targetId),
            'amount'      => $order['amount'],
            'targetType'  => $order['targetType']
        ));
        return $request->setParams($requestParams);
    }
 
    private function getPaymentOptions($payment, $settings)
    {
        if (empty($settings)) {
            throw new \RuntimeException('支付参数尚未配置，请先配置。');
        }

        if (empty($settings['enabled'])) {
            throw new \RuntimeException("支付模块未开启，请先开启。");
        }

        if (empty($settings[$payment.'_enabled'])) {
            throw new \RuntimeException("支付模块({$payment})未开启，请先开启。");
        }

        if (empty($settings["{$payment}_key"]) || empty($settings["{$payment}_secret"])) {
            throw new \RuntimeException("支付模块({$payment})参数未设置，请先设置。");
        }

        $options = array(
            'key'    => $settings["{$payment}_key"],
            'secret' => $settings["{$payment}_secret"]
        );

        return $options;
    }

    private function getOrderProcessor($order)
    {
        if ($order['targetType'] == 'course') {
            if (isset($order['orderId'])) {
                $order = $this->getOrderService()->getOrder($order['orderId']);
                $targetId = $order['targetId'];
            } else {
                $dbOrder = $this->getOrderService()->getOrderBySn($order['sn']);
                $targetId = $dbOrder['targetId'];
            }
            $course = $this->getCourseService()->getCourse($targetId);
            if ($course['type'] == 'certification') {
                return new CertificationCourseOrderProcessor();
            }
        }
        return OrderProcessorFactory::create($order["targetType"]);
    }

}