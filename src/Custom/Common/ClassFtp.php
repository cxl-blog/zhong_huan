<?php

namespace Custom\Common;

use Topxia\Service\Common\ServiceKernel;

/**
 * 作用：FTP操作类( 拷贝、移动、删除文件/创建目录 )
 */
class ClassFtp
{
    // 返回操作状态(成功/失败)
    public $off; 

    // FTP连接
    public $conn_id; 

    protected $ftp_host;
    protected $ftp_port;
    protected $ftp_user;
    protected $ftp_password;

    /**
     * 方法：FTP连接
     * @FTP_HOST -- FTP主机
     * @FTP_PORT -- 端口
     * @FTP_USER -- 用户名
     * @FTP_PASS -- 密码
    */
    public function __construct($ftp_host, $ftp_port, $ftp_name, $ftp_password)
    {
        $ftpInfo = 'host: ' . $ftp_host . ' port: ' . $ftp_port . ' account: ' . $ftp_name . ' password: ' . $ftp_password;  
        file_put_contents($this->getLogFilePath(), "\n" . date("Y-m-d H:i:s") . ' ' . $ftpInfo . " start \n", FILE_APPEND);
        $this->ftp_host = $ftp_host;
        $this->ftp_port = $ftp_port;
        $this->ftp_user = $ftp_name;
        $this->ftp_password = $ftp_password;

        $this->conn_id = @ftp_connect($this->ftp_host, $this->ftp_port, 5);
        if (!empty($this->conn_id)) {
            @ftp_login($this->conn_id, $this->ftp_user, $this->ftp_password);
            @ftp_pasv($this->conn_id, 1); // 打开被动模拟
        }
    }

    //isdir:判断是目录/文件 
    function isdir($file) {
        return ftp_size($this->conn_id, $file) == -1;
    }

    //listfile:显示目录文件结构
    function listfile($filelist) {
        $files = array();
        $str = "";
        $br = "<br>";
        foreach ($filelist as $file) {
            if ($this->isdir($file)) {
                //如果是目录，递归执行函数
                $str .= $file . $br;
                array_push($files, $file);
                $flist = ftp_nlist($this->conn_id, $file);
                $this->listfile($flist);
            }
        }
    }

    /**
     * 方法：上传文件
     * @path -- 本地路径
     * @newpath -- 上传路径
     * @type -- 若目标目录不存在则新建
     */
    function up_file($path,$newpath,$type=true)
    {
        file_put_contents($this->getLogFilePath(), $newpath . " put file \n", FILE_APPEND);
        if ($type) {
            $result = $this->dir_mkdirs($newpath);
            if (!$result) {
                file_put_contents($this->getLogFilePath(), "failed as login or failed to create folder \n", FILE_APPEND);
                return false;
            }
        }  

        if (@ftp_nlist($this->conn_id, $newpath) && !@ftp_size($this->conn_id, $newpath)) {
            file_put_contents($this->getLogFilePath(), '0 size file ' . $newpath . " exist; \n", FILE_APPEND);
            @ftp_delete($this->conn_id, $newpath);
            file_put_contents($this->getLogFilePath(), "deleted successfully \n", FILE_APPEND);
        }

        if (@ftp_size($this->conn_id, $newpath) > 0) {
            return true;
        }  
        
        $this->off = @ftp_put($this->conn_id, $newpath, $path, FTP_BINARY);
        if (!$this->off) {
            file_put_contents($this->getLogFilePath(), $path . " failed to put file \n", FILE_APPEND);
        }

        return $this->off;
    }

    /**
      * 方法：移动文件
      * @path -- 原路径
      * @newpath -- 新路径
      * @type -- 若目标目录不存在则新建
      */
    function move_file($path,$newpath,$type=true)
    {
        if ($type) {
            $this->dir_mkdirs($newpath);
        }
        $this->off = @ftp_rename($this->conn_id,$path,$newpath);
        if (!$this->off) {
            echo "文件移动失败，请检查权限及原路径是否正确！";
        }
    }
    /**
      * 方法：复制文件
      * 说明：由于FTP无复制命令,本方法变通操作为：下载后再上传到新的路径
      * @path -- 原路径
      * @newpath -- 新路径
      * @type -- 若目标目录不存在则新建
      */
    function copy_file($path,$newpath,$type=true)
    {
        $downpath = "c:/tmp.dat";
        $this->off = @ftp_get($this->conn_id,$downpath,$path,FTP_BINARY);// 下载
        if (!$this->off) {
            echo "文件复制失败，请检查权限及原路径是否正确！";
        }
        $this->up_file($downpath,$newpath,$type);
    }
    /**
       * 方法：删除文件
       * @path -- 路径
       */
    function del_file($path)
    {
        $this->off = @ftp_delete($this->conn_id,$path);
        if (!$this->off) {
            echo "文件删除失败，请检查权限及路径是否正确！";
        }
    }
    /**
       * 方法：生成目录
       * @path -- 路径
       */
    function dir_mkdirs($path)
    {
        $path_arr = explode('/',$path); // 取目录数组
        array_pop($path_arr); // 弹出文件名
        $path_arr = array_slice($path_arr, 1);
        $path_div = count($path_arr); // 取层数

        // 创建目录
        foreach ($path_arr as $val) { 
            if (!@ftp_chdir($this->conn_id, $val)) {
                if (!@ftp_mkdir($this->conn_id, $val)) {
                    return false;
                }
                @ftp_chdir($this->conn_id, $val);
             }
         }
         
         // 回退到根
         for ($i=1; $i <= $path_div; $i++) {
             @ftp_cdup($this->conn_id);
         }

         return true;
     }
     /**
       * 方法：关闭FTP连接
       */
    function close()
    {
        @ftp_close($this->conn_id);
    }

    private function getLogFilePath() 
    {
        return $logFile = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') 
            . '/../../app/logs/sync_ftp_file';
    }
 }
