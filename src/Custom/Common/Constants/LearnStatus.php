<?php
namespace Custom\Common\Constants;

/**
 * 证书课程学习状态
 *      注意: 以下情况支持购买:
 *             学习状态 = 未购买 或 已过期能补学
 *
 * 已完成
 *      course_member 有记录, isLearned = 1
 *
 * 未购买
 *     非已完成
 *     course_member 无记录
 *     课程过期时间 >= now()  //每个待学学员的证书都有颁发时间
 *                                              //1.) 如果该证书是一年制, 则
 *                                              //     第一部分课程的有效期范围是 颁发时间 ~ 1年后
 *                                              //         即过期时间 = 颁发时间 + 1年
 *                                              //     第二部分课程的有效期范围是 颁发时间 + 1年 ~ 颁发时间 + 2年
 *                                              //         即过期时间 = 颁发时间 + 2年
 *                                              //2.) 如果该证书是两年值, 则
 *                                              //     课程的有效期范围是 颁发时间 ~ 两年后
 *                                              //         即过期时间 = 颁发时间 + 2年
 *                                              //
 *                                              //即使是补学, 课程过期时间也同上
 *
 * 正在学习
 *     course_member 有记录, isLearned = 0
 *     课程过期时间 >= now()
 *
 * (
 *    已过期分为 已过期能补学 和 已过期不能补学,
 *    能补学 => 行政区域支持补学, 当前时间在补学有效期范围内, 即当前时间<证书颁发时间+3年
 * )
 * 已过期
 *
 *      course_member 有记录, isLearned = 0, createdTime < 课程过期时间
 *                  //createdTime 指的是 证书课程购买时间
 *      课程过期时间 < now()
 *    或
 *      course_member 无记录
 *      课程过期时间 < now()
 *
 *  已过期又分为
 *
 *  补学
 *      course_member 有记录, isLearned = 0
 *      createdTime >= 课程过期时间
 *      课程过期时间 < now()
 */
interface LearnStatus
{
    /**已结业*/
    const COMPLETION = 'completion';

    /**已完成*/
    const FINISHED = 'finished';

    /**未购买*/
    const UN_PAID = 'unPaied';

    /**正在学习*/
    const LEARNING = 'learning';

    /**已过期能补学*/
    const OVERTIME_MAKEUP_YES = 'overtimeMakeupYes';

    /**已过期不能补学*/
    const OVERTIME_MAKEUP_NO = 'overtimeMakeupNo';

    /**补学,  即已经购买补学*/
    const MAKEUP = 'makeup';
}