<?php
namespace Custom\Common\Constants;

interface  CertCourseStatus{
    /**已结业*/
    const COMPLETION = 'COMPLETION';

    /**未购买*/
    const UNPAID = 'UNPAID';

    /**正在学习*/
    const LEARNING = 'LEARNING';

    /**已过期*/
    const OVERTIME = 'OVERTIME';

}