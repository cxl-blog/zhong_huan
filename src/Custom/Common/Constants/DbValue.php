<?php
namespace Custom\Common\Constants;

interface DbValue
{
    /**
     * 数据库中表示true的
     */
    const TRUE = 1;

    /**
     * 数据库中表示false的
     */
    const FALSE = 0;
}
