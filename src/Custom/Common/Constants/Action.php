<?php
namespace Custom\Common\Constants;

interface Action
{
    /**
     * 已注册
     */
    const REGISTERED = 1;

    /**
     * 已购买
     */
    const BUYED      = 2;

    /**
     * 已结业
     */
    const GRADUATED  = 3;
}
