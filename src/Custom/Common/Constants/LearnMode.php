<?php
namespace Custom\Common\Constants;

use Custom\Common\Util\CourseLoopUtils;

class LearnMode
{

    /**补学时间额外加6年*/
    public static $MAKE_UP_EXPIRE_DATE = '+6 year';

    public static $MAKE_UP_YEARS = 6;

    public static function getMakeUpExpireDate($years)
    {
        $makeUpExpireDate = $years + self::$MAKE_UP_EXPIRE_DATE;
        return "+{$makeUpExpireDate} year";
    }

    public static function getExpireTime($months)
    {
        return "+{$months} months";
    }

    public static function generateLearnMode($learnModeMonths, $months)
    {
        $mode = array();
        $mode['i18nMsg'] = $learnModeMonths . '个月';
        $mode['months'] = $months;
        $mode['courseCount'] = self::getCourseCount($learnModeMonths, $months);
        $mode['addedDays'] = "+{$learnModeMonths} months";
        for ($i = 2; $i <= $mode['courseCount']; $i++) {
            $mode['startTime_'.$i] = sprintf("+%d months", $learnModeMonths * ($i - 1));
        }
        return $mode;
    }

    public static function getCourseCount($learnModeMonths, $months)
    {
        return $months / $learnModeMonths;
    }

    public static function isMonthsValid($learnModeMonths, $months)
    {
        return $months % $learnModeMonths == 0;
    }

    public static function formatCertificationType($certCourses)
    {
        foreach ($certCourses as $key => $course) {
            $certCourses[$key] = self::formatSingleCertificationType($course);
         } 

         return $certCourses;
    }

    public static function formatSingleCertificationType($course)
    {
        if (isset($course['learnModeMonths'])) {
            $course['certificationType'] = $course['learnModeMonths'] . '个月';
         }
         return $course;
    }
}
