<?php 
namespace Custom\Service\BindMobileSms\Dao;

interface BindMobileSmsDao
{
    public function getSms($id);

    public function addSms($sms);

    public function getSmsByUserId($userId);

    public function updateSms($id, $fields);

    public function deleteSms($id);
}
