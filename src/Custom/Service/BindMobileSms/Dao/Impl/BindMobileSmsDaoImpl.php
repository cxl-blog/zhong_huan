<?php
namespace Custom\Service\BindMobileSms\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\BindMobileSms\Dao\BindMobileSmsDao;

class BindMobileSmsDaoImpl extends BaseDao implements BindMobileSmsDao
{
    protected $table = 'bind_mobile_sms';

    public function getSms($id)
    {
        $that = $this;
        return $this->fetchCached("id:{$id}", $id,
            function ($id) use ($that) {
                $sql = "SELECT * FROM {$that->table} WHERE id = ? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($id)) ?: null;
            }
        );
    }

    public function addSms($sms)
    {
        $this->getConnection()->insert($this->table, $sms);
        $this->clearCached();
        return $this->getSms($this->getConnection()->lastInsertId());
    }

    public function getSmsByUserId($userId)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}", $userId,
            function ($userId) use ($that) {
                $sql = "SELECT * FROM {$that->table} WHERE userId = ? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($userId)) ?: null;
            }
        );
    }

    public function updateSms($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->getSms($id);
    }

    public function deleteSms($id)
    {
        $result = $this->getConnection()->delete($this->table, array('id' => $id));
        $this->clearCached();
        return $result;
    }
}
