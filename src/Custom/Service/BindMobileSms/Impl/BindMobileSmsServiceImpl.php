<?php
namespace Custom\Service\BindMobileSms\Impl;

use Custom\Service\BindMobileSms\BindMobileSmsService;
use Topxia\Service\Common\BaseService;

class BindMobileSmsServiceImpl extends BaseService implements BindMobileSmsService
{
    protected function getSms($id)
    {
        return $this->getBindMobileSmsDao()->getSms($id);
    }

    protected function addSms($sms)
    {
        $sms['createdTime'] = time();
        return $this->getBindMobileSmsDao()->addSms($sms);
    }

    public function getSmsByUserId($userId)
    {
        return $this->getBindMobileSmsDao()->getSmsByUserId($userId);
    }

    public function updateSms($sms)
    {
        $existedSms = $this->getSmsByUserId($sms['userId']);
        if (empty($existedSms)) {
            return $this->addSms($sms);
        } else {
            $fields = array(
                'mobile' => $sms['mobile'],
                'code' => $sms['code'],
                'createdTime' => time()
            );
            return $this->getBindMobileSmsDao()->updateSms($existedSms['id'], $fields);
        }
    }

    public function deleteSms($id)
    {
        return $this->getBindMobileSmsDao()->deleteSms($id);
    }

    protected function getBindMobileSmsDao()
    {
        return $this->createDao('Custom:BindMobileSms.BindMobileSmsDao');
    }
}
