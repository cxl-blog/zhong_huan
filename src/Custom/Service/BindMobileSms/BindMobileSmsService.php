<?php
namespace Custom\Service\BindMobileSms;

interface BindMobileSmsService
{
    public function getSmsByUserId($userId);

    public function updateSms($sms);

    public function deleteSms($id);
}
