<?php

namespace Custom\Service\DiscoveryColumn\Impl;

use Topxia\Service\DiscoveryColumn\Impl\DiscoveryColumnServiceImpl as BaseTopxiaSerivceImpl;
use Topxia\Common\ArrayToolkit;

class DiscoveryColumnServiceImpl extends BaseTopxiaSerivceImpl
{

    public function updateDiscoveryColumn($id, $fields)
    {
        $fields = ArrayToolkit::parts($fields, array('categoryId', 'orderType', 'type', 'showCount', 'title', 'seq', 'schoolType'));
        return $this->getDiscoveryColumnDao()->updateDiscoveryColumn($id, $fields);
    }
}
