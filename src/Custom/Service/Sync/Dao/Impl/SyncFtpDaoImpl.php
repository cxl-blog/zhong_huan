<?php
namespace Custom\Service\Sync\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Sync\Dao\SyncFtpDao;

class SyncFtpDaoImpl extends BaseDao implements SyncFtpDao
{
    protected $table = 'sync_ftp_file';
    
    public function addSyncFtpInfo($fields)
    {
        $fields['createdTime'] = time();
        $affected              = $this->getConnection()->insert($this->table, $fields);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert sync_ftp_file error.');
        }

        return $this->findSyncFtpInfoById($this->getConnection()->lastInsertId());
    }

    public function waveRetryTimes($id)
    {
        $sql = "UPDATE {$this->getTable()} SET retryTimes = retryTimes + 1 WHERE id = ? LIMIT 1";
        
        $result = $this->getConnection()->executeQuery($sql, array($id));

        return $result;
    }

    public function findSyncFtpInfoById($id)
    {
        $sql ="SELECT * FROM {$this->table} WHERE id = ?;";
        return $this->getConnection()->fetchAssoc($sql, array($id));
    }

    public function findSyncFtpInfosByIds($ids)
    {
        $marks = str_repeat('?,', count($ids) - 1).'?';
        $sql = "SELECT * FROM {$this->table} WHERE id in ({$marks})";
        return $this->getConnection()->fetchAll($sql, $ids);
    }

    public function findSyncFtpInfoBySourceFile($sourceFile)
    {
        $sql ="SELECT * FROM {$this->table} WHERE sourceFile = ?;";
        return $this->getConnection()->fetchAssoc($sql, array($sourceFile));
    }

    public function searchSyncFtpInfoCountByConditions($conditions)
    {
        $conditions = array();
        $builder = $this->_createQueryBuilder($conditions)
            ->select('count(`id`) AS count')
            ->from($this->table, $this->table);
        return $builder->execute()->fetchColumn(0);
    }

    public function searchSyncFtpInfosByConditions($conditions, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $conditions = array();
        $builder = $this->_createQueryBuilder($conditions)
            ->select('*')
            ->orderBy('retryTimes', 'ASC')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        return $builder->execute()->fetchAll() ?: array();
    }

    public function delSyncFtpInfosByIds($ids)
    {   
        $marks = str_repeat('?,', count($ids) - 1).'?';
        $sql = "DELETE FROM {$this->table} WHERE id in ({$marks})";
        $result = $this->getConnection()->executeUpdate($sql, $ids);
        $this->clearCached();
        return $result;
    }

    protected function _createQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, $this->table)
            ->andWhere('type = :type');

        return $builder;
    }
}