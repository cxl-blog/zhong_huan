<?php

namespace Custom\Service\Sync\Dao;

interface SyncFtpDao 
{
    public function addSyncFtpInfo($fields);

    public function waveRetryTimes($id);

    public function findSyncFtpInfoById($id);

    public function findSyncFtpInfosByIds($ids);

    public function findSyncFtpInfoBySourceFile($sourceFile);

    public function searchSyncFtpInfoCountByConditions($conditions);

    public function searchSyncFtpInfosByConditions($conditions, $start, $limit);

    public function delSyncFtpInfosByIds($ids);
}