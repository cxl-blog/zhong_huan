<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataFtpFileJob implements Job
{
    public function execute($params)
    {
        /**
          *  每天02:00去数据库取图片信息，并保存到特定表
          **/
        $date = time();
        $yesterday = date('Y-m-d', strtotime('-1 day', $date));
        $startTime = $yesterday . ' 00:00:00';
        $endTime = $yesterday . ' 23:59:59';

        $conditions = array(
            'types' => array('lesson', 'testpaper', 'register'),
            'startDateTime' => $startTime,
            'endDateTime' => $endTime
        );

        $faceImgCount =  $this->getFaceDetectResultService()->searchFaceDetectResultsCount($conditions);
        if($faceImgCount > 0){
            $rangeCount = 500;
            $count = 0;

            $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');

            while($count < $faceImgCount){
                $faceImgs =  $this->getFaceDetectResultService()->searchFaceDetectResults(
                    $conditions,
                    'createdByAsc', 
                    $count, 
                    $rangeCount
                );
                foreach($faceImgs as $faceImg){
                    $fileUrl = $faceImg['fileUrl'];
                    $fileArray = explode('/', $fileUrl);
                    $createdDateStr = $this->caculateDateStr($fileArray, $faceImg['createdTime']);
                    $userInfo = $this->getUserService()->getUser($faceImg['userId']);
                    $userProfile = $this->getUserService()->getUserProfile($faceImg['userId']);
                    $remote_dir = '/jy/' . $createdDateStr . '/' . $userProfile['idcard'] . '/';
                    $fileName = $fileArray[count($fileArray)-1];
                    $sourceFile = str_replace('public:/', $publicUploadDir, $fileUrl);
                    $remoteFile = $remote_dir.$fileName;

                    $this->getSyncFtpService()->updateSyncFtpInfo($userInfo['areaCode'], $sourceFile, $remoteFile, 
                        'sync_certificate');
                }
                unset($faceImgs);
                $count += $rangeCount;
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getSyncFtpService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncFtpService');
    }

    private function getCrontabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    private function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    private function caculateDateStr($fileArray, $dateTime)
    {
        $createdDateStr = $fileArray[count($fileArray)-2];
        if (empty($createdDateStr) || preg_match("/[^\d-., ]/", $createdDateStr)) {
            $createdDateStr = date('Y-m-d', $dateTime);
        } else {
            $createdDateStr = date('Y-m-d', strtotime($createdDateStr));
        }
        return $createdDateStr;
    }

}
