<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;
use Custom\Common\ClassFtp;

class SyncDataSendFileJob implements Job
{
    public function execute($params)
    {
        /**
          *  每次发200张图片，成功则删除，完成以后新建下一个任务，本次任务删除
          *  可能存在的风险: 前200张图片存在问题，一直删除不了，造成死循环 
          *  02:00-04:00 快速跑， 其他时间:  慢速跑
        **/

        $this->getLogService()->info('DataSync', 'jobSendFile', '发送图片开始'.date("Y-m-d H:i:s"));
        $now = date("H:i:s");
        if($now >= "02:00:00" && $now <= "04:00:00"){
            $sleepTime = 100000;
        }
        else{
            $sleepTime = 800000;
        }

        $rangeCount = 200;
        $conditions = array();
        $nextTime = 0;
        $fileCount = $this->getSyncFtpService()->searchSyncFtpInfoCountByConditions($conditions);
        
        if($fileCount > 0){
            $fileInfos = $this->getSyncFtpService()->searchSyncFtpInfosByConditions($conditions, 0,
                $rangeCount);
            $originArea = "";
            $successFileArray = array();
            $fileInfoArray = ArrayToolkit::group($fileInfos, 'areaCode');

            
            foreach($fileInfoArray as $key=>$val){
                foreach($val as $singleFile){
                    $this->getSyncFtpService()->waveRetryTimes($singleFile['id'], $singleFile['retryTimes']);
                }
                $areaCode = $key;
                $areaInfo = $this->getAreaService()->getAreaByCode($areaCode);
                $ftpServer = $areaInfo['ftpUrl'];   
                $ftpName = $areaInfo['ftpName'];
                $ftpPassword = $areaInfo['ftpPassword'];
                if(empty($ftpServer) || empty($ftpName) || empty($ftpPassword)){
                    continue;
                }
                $ftpServer = substr($ftpServer, 6, strlen($ftpServer)-7);
                $ftpServerArray = explode(":", $ftpServer);
                $ftpHost = $ftpServerArray[0];
                $ftpPort = $ftpServerArray[1];

                $ftp = new ClassFtp($ftpHost, $ftpPort, $ftpName, $ftpPassword);
                if(empty($ftp->conn_id)){
                    $this->getLogService()->warning('DataSync', 'jobSendFile', $areaInfo['name'].'ftp服务器连接异常:'.
                        $ftpHost.':'.$ftpPort.'帐号:'.$ftpName.'密码:'.$ftpPassword);
                    continue;
                }

                foreach($val as $fileInfo){
                    $sourceFile = $fileInfo['sourcefile'];
                    $remoteFile = $fileInfo['remotefile'];

                    $result = $ftp->up_file($sourceFile, $remoteFile);
                    if($result){
                        $successFileArray[] = $fileInfo['id'];
                    }

                    //延时800毫秒，防止发的太快
                    usleep($sleepTime);
                }

                $ftp->close();
            }
            
            if(count($successFileArray)>0){
                $this->getSyncFtpService()->delSyncFtpInfosByIds($successFileArray);
            }   

            $nextTime = time() + 10;
        }
        else{
            $nextTime = time() + 1800;
        }
        
        $this->getLogService()->info('DataSync', 'jobSendFile', '发送图片结束'.date("Y-m-d H:i:s"));
        
        //新建下一个任务，本次任务结束
        $jobName = 'SyncDataSendFileJob';
        $this->getCrontabService()->createJob(array(
            'name'            => $jobName,
            'cycle'           => 'once',
            'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
            'time' => $nextTime,
            'jobParams'       => '',
            'createdTime'     => time()
        ));
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

    protected function getSyncFtpService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncFtpService');
    }

    private function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    private function getCrontabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

}
