<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataClearLogJob implements Job
{
    public function execute($params)
    {
        //删除三个月前的数据同步日志
        $module = "DataSync";
        $createdTime = strtotime("-3 month");
        $this->getLogService()->clearLogByModuleAndCreatedTime($module, $createdTime);
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('Custom:System.LogService');
    }

}
