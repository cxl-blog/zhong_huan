<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataOrderRefundJob implements Job
{
    public function execute($params)
    {
        $orderRefundIds = explode("|",$params);
        foreach($orderRefundIds as $orderRefundId){
            $result = $this->getSyncService()->syncSingleOrderRefund($orderRefundId);
            if(!$result){   //同步失败
                $fields = array(
                    'type' => 'SyncDataOrderRefundJob',
                    'content' => $orderRefundId
                );
                $this->getJobDataService()->addJobData($fields);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

}
