<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataBuyCourseJob implements Job
{
    public function execute($params)
    {
        $orderIds = explode("|",$params);
        foreach($orderIds as $orderId){
            $result = $this->getSyncService()->syncSingleBuyCourse($orderId);
            if(!$result){   //同步失败
                $fields = array(
                    'type' => 'SyncDataBuyCourseJob',
                    'content' => $orderId
                );
                $this->getJobDataService()->addJobData($fields);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

}
