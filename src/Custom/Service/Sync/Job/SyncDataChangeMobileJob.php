<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataChangeMobileJob implements Job
{
    public function execute($params)
    {
        $changeMobileIds = explode("|",$params);
        foreach($changeMobileIds as $changeMobileId){
            $result = $this->getSyncService()->syncSingleChangeMobile($changeMobileId);
            if($result){
                $this->getChangeMobileService()->delChangeMobileInfosById($changeMobileId);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

    protected function getChangeMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:User.ChangeMobileService');
    }

}
