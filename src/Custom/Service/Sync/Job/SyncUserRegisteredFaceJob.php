<?php

namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncUserRegisteredFaceJob implements Job
{
    public function execute($params)
    {
        $faceDetectResultIds = explode("|",$params);
        foreach($faceDetectResultIds as $faceDetectResultId){
            $result = $this->getSyncService()->syncUserRegisteredFace($faceDetectResultId);
            if(!$result){   //同步失败
                $fields = array(
                    'type' => 'SyncUserRegisteredFaceJob',
                    'content' => $faceDetectResultId
                );
                $this->getJobDataService()->addJobData($fields);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }
}
