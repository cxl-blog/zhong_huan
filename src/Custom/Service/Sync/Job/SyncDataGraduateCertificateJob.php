<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataGraduateCertificateJob implements Job
{
    /**
     * @param ${userId},${courseId},${currentLoopTime}|${userId},${courseId},${currentLoopTime}
     */
    public function execute($params)
    {
        $states = explode("|",$params);
        foreach($states as $state){
            $info = explode(",",$state);
            $result = $this->getSyncService()->syncSingleGraduateCertificate($info[0], $info[1], $info[2]);
            if(!$result){   //同步失败
                $fields = array(
                    'type' => 'SyncDataGraduateCertificateJob',
                    'content' => $state
                );
                $this->getJobDataService()->addJobData($fields);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

}
