<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataJob implements Job
{
    public function execute($params)
    {

        $this->getLogService()->info('DataSync', 'job', '后台同步数据，购买课程以及结业信息任务');

        $startEndTime = $this->getStartEndTime();
        $startTime = $startEndTime['startTime'];
        $endTime = $startEndTime['endTime'];

        //同步退出学习信息, 先取数据库中的上次执行失败的信息，再取昨天的信息

        $jobName = 'SyncDataOrderRefundJob';
        $rangeCount = 100;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'status' => 'success',
            'targetType' => 'course',
            'startDateTime' => $startTime,
            'endDateTime' => $endTime
        );
        $orderRefundCount = $this->getOrderService()->searchRefundCount($conditions);
        if($orderRefundCount > 0){
            $orderRefundIds = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $orderRefundCount){
                $orderRefunds = $this->getOrderService()->searchRefunds($conditions, 
                    array('updatedTime','ASC'), $count, $rangeCount);
                unset($orderRefundIds);
                $orderRefundIds = ArrayToolkit::column($orderRefunds, 'id');
                unset($orderRefunds);

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $orderRefundIds),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }
        }
        
        
        //同步手机号修改信息, 先取数据库中的上次执行失败的信息，再取昨天的信息

        $jobName = 'SyncDataChangeMobileJob';
        $rangeCount = 100;
        $this->getFailData($jobName, $rangeCount);
        $changeMobileInfoCount = $this->getChangeMobileService()->searchChangeMobileInfoCount();
        if($changeMobileInfoCount > 0){
            $changeMobileIds = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $changeMobileInfoCount){
                $changeMobileInfos = $this->getChangeMobileService()->
                    searchChangeMobileInfosByConditions($count,$rangeCount);
                unset($changeMobileIds);
                $changeMobileIds = ArrayToolkit::column($changeMobileInfos, 'id');
                unset($changeMobileInfos);

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $changeMobileIds),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }
        }
        
        
        //同步实名认证信息, 先取数据库中的上次执行失败的信息，再取昨天的信息
        
        $jobName = 'SyncDataUserApprovalJob';
        $rangeCount = 100;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'startTime' => $startTime,
            'endTime' => $endTime
        );
        
        $approvalCount = $this->getUserApprovalDao()->searchApprovalsCount($conditions);
        if($approvalCount > 0){
            $approvalIds = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $approvalCount){
                $userApprovalInfos = $this->getUserApprovalDao()->searchApprovals($conditions, 
                    array('createdTime', 'ASC'), $count, $rangeCount);
                unset($approvalIds);
                $approvalIds = ArrayToolkit::column($userApprovalInfos, 'id');
                unset($userApprovalInfos);

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $approvalIds),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }
        }
        
        
        //同步购买课程, 先取数据库中的上次执行失败的信息，再取昨天的信息

        $jobName = 'SyncDataBuyCourseJob';
        $rangeCount = 100;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'status' => 'published',
            'type' => 'certification'
        );
        $courseCount = $this->getCourseService()->searchCourseCount($conditions);
        if($courseCount > 0){
            $courseIds = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $courseCount){
                $certificationCourses = $this->getCourseService()->searchCourses(
                    $conditions, 
                    array('createdTime', 'ASC'), 
                    $count, 
                    1000
                );
                $courseIds = array_merge($courseIds,  ArrayToolkit::column($certificationCourses, 'id'));
                unset($certificationCourses);
                $count += 1000;
            }

            $conditions = array(
                'courseIds' => $courseIds,
                'joinedType' => 'course',
                'role' => 'student',
                'startTimeGreaterThan' => $startTime,
                'startTimeLessThan' => $endTime
            );
            
            $courseMemberCount = $this->getCourseService()->searchMemberCount($conditions);
            if($courseMemberCount > 0){
                $count = 0;
                $orderIds = array();
                while($count < $courseMemberCount){
                    $courseMembers = $this->getCourseService()->searchMember($conditions, $count, $rangeCount);
                    unset($orderIds);
                    $orderIds = ArrayToolkit::column($courseMembers, 'orderId');
                    $orderIds = array_unique(array_diff($orderIds, array(0)));
                    unset($courseMembers);

                    $this->getCrontabService()->createJob(array(
                        'name'            => $jobName,
                        'cycle'           => 'once',
                        'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                        'time' => time()+60,
                        'jobParams'       => implode("|", $orderIds),
                        'createdTime'     => time()
                    ));

                    $count += $rangeCount;   
                }
            }
        }

        
        //同步结业信息,先取数据库中的上次执行失败的信息，再取昨天的信息
        //证书下的课程学完以后就发送，每次课程信息只有一门课程
        $jobName = 'SyncDataGraduateCertificateJob';
        $rangeCount = 50;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'role' => 'student',
            'isCompleted' => 1,
            'updatedTimeGreaterThan' => $startTime,
            'updatedTimeLessThan' => $endTime
        );
        $courseMemberCount = $this->getCourseService()->searchMemberCount($conditions);

        if($courseMemberCount > 0){
            $params = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $courseMemberCount){
                $courseMembers = $this->getCourseService()->searchMembers($conditions, 
                    array('updatedTime', 'ASC'), $count, $rangeCount);
                unset($params);

                foreach($courseMembers as $member){
                    $params[] = $member['userId'].",".$member['courseId'].",".$member['currentLoopTime'];
                }
                unset($courseMembers);

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $params),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }
        }
        /*
        $jobName = 'SyncDataGraduateCertificateJob';
        $rangeCount = 5;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'action' => 3,
            'startDateTime' => $startTime,
            'endDateTime' => $endTime
        );
        $userCertificationStates = $this->getUserCertificationStatesService()->
            searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions);
        if(!empty($userCertificationStates)){
            $stateCount = count($userCertificationStates);
            $count = 0;
            $childStates = array();
            $params = array();
            while($count < $stateCount){
                unset($childStates);
                $childStates = array_slice($userCertificationStates, $count, $rangeCount);
                foreach($childStates as $state){
                    $params[] = $state['userId'].",".$state['certificationId'].",".$state['currentLoopTime'];
                }
                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $params),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;
            }
        }
        */
        //同步调用用户人脸信息,先取数据库中的上次执行失败的信息，再取昨天的信息
        $jobName = 'SyncUserRegisteredFaceJob';
        $rangeCount = 50;
        $this->getFailData($jobName, $rangeCount);

        $conditions = array(
            'type' => 'register', //返回注册+修改人脸的信息
            'result' => 'success', //返回成功的人脸信息
            'startDateTime' => date('Y-m-d H:i:s', $startTime),
            'endDateTime' => date('Y-m-d H:i:s', $endTime)
        );

        $faceDetectResultCount = $this->getFaceDetectResultService()->searchFaceDetectResultsCount($conditions);

        if($faceDetectResultCount > 0){
            $faceDetectResultIds = array();
            $count = 0;
            //避免一下子取太多，内存益出
            while($count < $faceDetectResultCount){
                $faceDetectResults = $this->getFaceDetectResultService()->searchFaceDetectResults(
                    $conditions,
                    'createdByAsc',
                    $count,
                    $rangeCount
                );
                unset($faceDetectResultIds);
                $faceDetectResultIds = ArrayToolkit::column($faceDetectResults, 'id');
                unset($faceDetectResults);

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $faceDetectResultIds),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }
        }
    }

    private function getFailData($jobName, $rangeCount)
    {
        $jobDatas = $this->getJobDataService()->getJobDatasByType($jobName);
        if(!empty($jobDatas)){
            $jobDataCount = count($jobDatas);
            $count = 0;
            $params = array();
            $childJobDatas = array();
            while($count < $jobDataCount){
                unset($childJobDatas);
                $childJobDatas = array_slice($jobDatas, $count, $rangeCount);
                unset($params);
                foreach($childJobDatas as $data){
                    $params[] = $data['content'];
                }

                $this->getCrontabService()->createJob(array(
                    'name'            => $jobName,
                    'cycle'           => 'once',
                    'jobClass'        => 'Custom\\Service\\Sync\\Job\\'.$jobName,
                    'time' => time()+60,
                    'jobParams'       => implode("|", $params),
                    'createdTime'     => time()
                ));

                $count += $rangeCount;   
            }

            $this->getJobDataService()->deleteJobDataByType($jobName);
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    private function getCrontabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getCourseMemberDao()
    {
        return $this->getServiceKernel()->createDao('Course.CourseMemberDao');
    }

    protected function getUserCertificationStatesService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getUserApprovalDao()
    {
        return $this->getServiceKernel()->createDao('User.UserApprovalDao');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

    protected function getChangeMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:User.ChangeMobileService');
    }

    protected function getOrderService()
    {
        return $this->getServiceKernel()->createService('Custom:Order.OrderService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    private function getStartEndTime()
    {
        $syncDataConfigFile = __DIR__.'/../../../../../app/config/syncDataTest.php';
        if (file_exists($syncDataConfigFile)) {
            $syncDataTestConfig = include $syncDataConfigFile;
            $endTime = time();
            $startTime = strtotime($syncDataTestConfig['startTime'], $endTime);
        } else {
            $yesterday = date("Y-m-d", strtotime("-1 day", time()));
            $startTime = strtotime($yesterday." 00:00:00");
            $endTime = strtotime($yesterday." 23:59:59");
        }
        return array(
            'startTime' => $startTime,
            'endTime'  => $endTime
        );
    }

}
