<?php
namespace Custom\Service\Sync\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;

class SyncDataUserApprovalJob implements Job
{
    public function execute($params)
    {
        $approvalIds = explode("|",$params);
        foreach($approvalIds as $approvalId){
            $result = $this->getSyncService()->syncSingleApprovalSubmit($approvalId);
            if(!$result){   //同步失败
                $fields = array(
                    'type' => 'SyncDataUserApprovalJob',
                    'content' => $approvalId
                );
                $this->getJobDataService()->addJobData($fields);
            }
        }
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getJobDataService()
    {
        return $this->getServiceKernel()->createService('Custom:JobData.JobDataService');
    }

}
