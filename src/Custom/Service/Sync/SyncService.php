<?php

namespace Custom\Service\Sync;

/**
 * 所有的接口返回只有2种格式
 * 
 * 成功
 * {
 *     "result": "true"
 * }
 * 
 * 失败
 * {
 *     "result": "false",
 *     "errorMsg": "缺少ip"
 * }
 */
interface SyncService
{
    /** 
     * 同步行政区域, 一条一条同步
     * cityName 是主键, cityName如果已存在, 则视为修改
     * @param jsonStr 格式为
      {
         "cityName" : "兴安盟",  //行政区域名称
         "competencyno": "（客运）经营性道路旅客运输驾驶员,（货运）经营性道路货物运输驾驶员", //证书名称, 用逗号分隔
         "province": "内蒙古",
         "ip": "127.0.0.1"
      }
     * @return 成功返回
     * 
     */
    public function syncSingleArea($jsonStr);

    /** 
     * 同步个人基本信息 一条一条同步
     * stuId 是主键, stuId如果已存在, 则视为修改
     * @param jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "cityName": " ", //行政区域
         "stuname": "", //学员姓名
         "stusex": "",  //学员性别
         "stuidentitycard": "",   //身份证号
         "stuphoto": "",  //人脸照片（Base64）
         "stuselftel": ""  //手机号码
      }
     * @return 成功返回
     * 
     */
    public function syncSingleUser($jsonStr);

    /** 
     * 同步学员证书 一条一条同步
     * 
     * @param jsonStr 格式为
      {
         "dID" : "",  //初学ID
         "stuId": " ", //学员ID
         "competencyno": "", //从业资格类别
         "qualitydate": "", //初领日期
      }
     * @return 成功返回
     * 
     */
    public function syncSingleCertificate($jsonStr);

    /** 
     * 同步继续教育 一条一条同步
     * 
     * @param jsonStr 格式为
      {
         "dID" : "",  //初学ID
         "stuId": " ", //学员ID
         "competencyno": "", //从业资格类别
         "beginTime": "", //周期开始年
         "endTime": "", //周期结束年
         "dataFrom": ""  //数据来源（1线上2线下）
      }
     * @return 成功返回
     * 
     */
    public function syncSingleOfflineFinishedCourse($jsonStr);

    /** 
     * 同步实名认证信息提交审核 一条一条同步
     * @param  approvalId
     * @return 成功返回
     {
         "stuId" : "",  //学员ID
         "identitycard": " ", //身份证号
         "createtime": "", //
         "idCardPhotoFront": "",  //身份证正面片（Base64）
         "idCardPhotoBack": "",   //身份证反面照片（Base64）
         "driverPhoto": "",  //驾驶证照片（Base64）
         "employmentPhoto": "",  //从业资格证书正面照片（Base64）
         "otherPhoto": "" //其他照片（Base64）
      }
     * 
     */
    public function syncSingleApprovalSubmit($id);

    /** 
     * 同步实名认证信息审核结果 一条一条同步
     * @param jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "identitycard": " ", //身份证号
         "result": "", //审核结果 approved = 通过审核 approve_fail= 未通过审核
         "reason": ""  //原因
      }
     * @return 成功返回
     * 
     */
    public function syncSingleApprovalResult($jsonStr);

    /** 
     * 推送购课信息 一条一条推送
     * @param  $orderId  订单ID   
     * @return 成功返回
     * jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "identitycard": " ", //身份证号
         "className": "",  //课程名称
         "classMoney": "",  //课程金额
         "buyWay": "",   //购买方式（1微信；2支付宝; 3其他; 4线下支付）
         "buyTime": "",   //支付时间
         "dataFrom": ""  //数据来源（1APP；2web）
      }
     */
    public function syncSingleBuyCourse($orderId);

    /** 
     * 推送结业信息 一条一条推送
     * @param  
     {
        "userId": "",   
        "certificationId": "",
        "currentLoopTime": ""
    }
     * @return 成功返回
     * jsonStr 格式为
      {
            “stuId”: “112”,
            “certfication”: {   //证书信息
                “awardTime” : “2012-5-12”,    //证书颁发时间
                “years”: “3”       //证书周期，3年
                “learnMode”: “一年制”,  //学制
                “graduatedTime”: “2016-3-12 20:33:12”   //结业
            },
            “courses”: [  //课程，一个证书有多门课程
            {
                “courseName”: “机动车驾驶”,  //课程名称
                “buyTime”: “2014-3-12 12:33:12”,  //购课时间
                “startPeriod”: “2014-3-12”,  //周期开始时间
                “endPeriod”: “2015-3-11” //周期结束时间
                “percent”: “100%”,    //学习进度
                “lessons”: [  //课时, 一门课程有多个课时
                {
                    “lessonName”: “第一课时”,  //课时名称
                    “startTime”: “2012-2-13 22:13:11”,  //学习开始时间
                    “finishTime”: “2012-2-13 23:13:11”,  //学习结束时间，未学完时，不会有此属性
                    “hours”: “60.0”,  //课时学时，单位分钟
                    “status”: “finished”,  //课时状态，finished 和 learning 2种
                    “learningRecords”: [ //学习记录，每次观看时，产生一条记录
                        “device”: “desktop”,  //学习设备， 分为 desktop和 mobile
                        “startTime”: “2012-2-13 22:13:11”, //学习开始时间
                        “watchTime”: “123”, //观看时间，单位为秒
                        “faceDetect”: [ //每次观看时，可能产生多条人脸识别记录
                        {
                            “isPassed”: “true”, //人脸识别是否通过，false为未通过
                            “base64ImageData”: “....”,
                            “createTime”: “2012-2-13 12:33:12”
                        }
                        ]
                    ]
                }
                ],
                 “testpaper”: {  //考试， 如果没试卷，则无此属性
                    “isPassed”: “true”, //是否通过， false为未通过
                    “markers”: “92” //考试分数
                }
            }
            ]
     }
     */
    public function syncSingleGraduateCertificate($userId, $cerficationId, $currentLoopTime);

    /**
     * 返回格式同 syncSingleGraduateCertificate，
     * 只生成json, 不负责发送
     */
    public function genereateSingleGraduateCertificateInfo($userId, $courseId, $currentLoopTime);

    /** 
     * 同步学员证书信息 一条一条同步
     * @param  
     {
        "stuId": "",   //学员ID
        "competencyno": "",   //从业资格类别
        "startTime": "",     //证书周期开始
        "endTime": ""    //结束时间
    }
     * @return 成功返回
     * jsonStr 格式为
      {
            “stuId”: “112”,
            “certfication”: {   //证书信息
                “awardTime” : “2012-5-12”,    //证书颁发时间
                “years”: “3”       //证书周期，3年
                “learnMode”: “一年制”,  //学制
                “graduatedTime”: “2016-3-12 20:33:12”   //结业
            },
            “courses”: [  //课程，一个证书有多门课程
            {
                “courseName”: “机动车驾驶”,  //课程名称
                “buyTime”: “2014-3-12 12:33:12”,  //购课时间
                “startPeriod”: “2014-3-12”,  //周期开始时间
                “endPeriod”: “2015-3-11” //周期结束时间
                “percent”: “100%”,    //学习进度
                “lessons”: [  //课时, 一门课程有多个课时
                {
                    “lessonName”: “第一课时”,  //课时名称
                    “startTime”: “2012-2-13 22:13:11”,  //学习开始时间
                    “finishTime”: “2012-2-13 23:13:11”,  //学习结束时间，未学完时，不会有此属性
                    “hours”: “60.0”,  //课时学时，单位分钟
                    “status”: “finished”,  //课时状态，finished 和 learning 2种
                    “learningRecords”: [ //学习记录，每次观看时，产生一条记录
                        “device”: “desktop”,  //学习设备， 分为 desktop和 mobile
                        “startTime”: “2012-2-13 22:13:11”, //学习开始时间
                        “watchTime”: “123”, //观看时间，单位为秒
                        “faceDetect”: [ //每次观看时，可能产生多条人脸识别记录
                        {
                            “isPassed”: “true”, //人脸识别是否通过，false为未通过
                            “base64ImageData”: “....”,
                            “createTime”: “2012-2-13 12:33:12”
                        }
                        ]
                    ]
                }
                ],
                 “testpaper”: {  //考试， 如果没试卷，则无此属性
                    “isPassed”: “true”, //是否通过， false为未通过
                    “markers”: “92” //考试分数
                }
            }
            ]
     }
     */
    public function syncSingleStuCertificate($jsonStr);

    /** 
     * 同步注销证书 一条一条同步   
      * @param jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "competencyno": " ", //注销的证书
         "reason": "",  //原因,
         "createtime": ""   //操作时间
      }
     * @return 成功返回
     * 
     */
    public function syncSingleRevokeCertificate($jsonStr);

    /** 
     * 同步修改密码 一条一条同步   
      * @param jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "password": " " //新密码
      }
     * @return 成功返回
     * 
     */
    public function syncSingleResetPassword($jsonStr);

    /** 
     * 推送手机号修改 一条一条推送
      * @param $id
     * @return 成功返回
     {
         "stuId" : "",  //学员ID
         "mobile": " " //手机号
      }
     */
    public function syncSingleChangeMobile($id);

    /** 
     * 推送退出学习信息 一条一条推送
     * @param  $orderRefundId  取消订单ID   
     * @return 成功返回
     * jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "className": "",  //课程名称
         "buyTime": ""  //支付时间
      }
     */
    public function syncSingleOrderRefund($id);

    /** 
     * 同步学员注册 一条一条同步
     * @return 成功返回
     * @param jsonStr 格式为
      {
         "stuId": "", //学员ID
         "mobile": ""  //手机号码
      }
     * 
     */
    public function syncSingleUserRegister($jsonStr);

    /** 
     * 同步冻结学员证书 一条一条同步
     * 
     * @param jsonStr 格式为
      {
         "stuId" : "",  //学员ID
         "competencyno": "", //冻结的证书
         "type": freeze //操作类型 freeze:冻结,unfreeze:解冻
         "reason": "",  //操作原因(选填)
      }
     * @return 成功返回
     * 
     */
    public function syncSingleUserCertificateFrozen($jsonStr);

    /** 
     * 同步冻结学员课程 一条一条同步
     * 
     * @param jsonStr 格式为
      {
        "stuId": "" //学员ID
        "competencyno": "" //冻结的证书
        "loopStartTime": "" //证书开始时间
        "loopEndTime": "" //证书结束时间
        "type": "freeze" //操作类型 freeze:冻结,unfreeze:解冻
        "reason": "" //操作原因(选填)
      }
     * @return 成功返回
     * 
     */
    public function syncSingleUserCourseFrozen($jsonStr);

    /** 
     * 同步用户注册后的人脸信息 一条一条同步
     * @param  faceDetectResultId
     * @return 成功返回
     {
         "stuId" : "",  //学员ID
         "photoStr": "",  //人脸识别照片（Base64）
      }
     * 
     */
    public function syncUserRegisteredFace($faceDetectResultId);
}
