<?php
namespace Custom\Service\Sync;

interface SyncFtpService
{
    public function addSyncFtpInfo($fields);

    public function updateSyncFtpInfo($areaCode, $sourceFile, $remoteFile, $type);

    public function waveRetryTimes($id);

    public function findSyncFtpInfoById($id);

    public function findSyncFtpInfosByIds($ids);

    public function findSyncFtpInfoBySourceFile($sourceFile);

    public function searchSyncFtpInfoCountByConditions($conditions);

    public function searchSyncFtpInfosByConditions($conidtions,$start,$limit);

    public function delSyncFtpInfosByIds($ids);
}
