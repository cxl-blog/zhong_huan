<?php
namespace Custom\Service\Sync\Impl;

use Topxia\Service\Common\BaseService;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Constants\DbValue;
use Custom\Common\Constants\Action;
use Custom\Common\Exception\ArgumentNotJsonException;
use Custom\Common\Exception\MissingFieldException;
use Custom\Common\Exception\UnOfflineCompleteCourseException;
use Custom\Service\Sync\SyncService;
use Custom\Common\Util\StringUtils;
use Custom\Service\FaceDetect\FaceDetectService;
use Custom\Common\Util\CompressionUtils;
use Custom\Service\Sms\Impl\SmsServiceImpl;
use Custom\Common\Util\DateUtils;
use Topxia\Common\SimpleValidator;
use Custom\Service\Sms\SmsService;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class SyncServiceImpl extends BaseService implements SyncService
{
    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleArea($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_area', $jsonStr);
        $areaInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($areaInfo, array('ip', 'province', 'cityName', 'competencyno', 'ftp_url', 
            'ftp_name', 'ftp_password'));
        $areaInfo['areaCode'] = $this->generateAreaCodePerAreaName($areaInfo['cityName']);

        $existedAreaInfo = $this->getAreaService()->getAreaByCode($areaInfo['areaCode']);

        $that = $this;
        $this->getAreaDao()->getConnection()->transactional(
            function($conn) use ($that, $areaInfo, $existedAreaInfo) {
                $certifications = $this->createOrQueryDbCertifications($areaInfo['competencyno']);
                if (isset($existedAreaInfo)) {
                    $this->updateArea($existedAreaInfo['id'], $areaInfo);
                } else {
                    $this->addArea($areaInfo);
                }
                $this->updateOrAddAreaCertifications($areaInfo, $certifications);
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleUser($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_user', $jsonStr);

        $userInfo = $this->transferToArray($jsonStr);

        $this->validateRequiredFields($userInfo, array('stuId', 'cityName', 'stuname', 'stusex', 'stuidentitycard'));
       
        $areaInfo = $this->getAreaService()->getAreaByName($userInfo['cityName']);

        if(empty($areaInfo)){
            throw new \Exception("区域信息有误");
        }
        $areaCode = $areaInfo['code'];
        $userInfo['areaCode'] = $areaCode;

        $existedUserInfo = $this->getUserService()->findUserByStuId($userInfo['stuId']);

        $that = $this;
        $this->getUserDao()->getConnection()->transactional(
            function($conn) use ($that, $userInfo, $existedUserInfo) {
                if (!empty($existedUserInfo)) {
                    $this->updateUser($existedUserInfo['id'], $userInfo);
                    $this->updateUserProfile($existedUserInfo['id'], $userInfo);

                    if(!empty($userInfo['stuphoto'])){
                        if(empty($existedUserInfo['base64FaceImgPath'])){
                            $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');
                            $this->getFaceDetectService()->updateUserFaceImg($existedUserInfo['id'], $userInfo['stuphoto'], $publicUploadDir);
                            $this->getUserService()->setFacePhotoFinish($existedUserInfo['id'], DbValue::TRUE);
                        } else {
                            $oldBase64ImgData = file_get_contents($existedUserInfo['base64FaceImgPath']);
                            if($oldBase64ImgData != $userInfo['stuphoto']){
                                @unlink($existedUserInfo['base64FaceImgPath']);
                                @unlink($existedUserInfo['faceImgPath']);

                                $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');
                                $this->getFaceDetectService()->updateUserFaceImg($existedUserInfo['id'], $userInfo['stuphoto'], $publicUploadDir);
                                $this->getUserService()->setFacePhotoFinish($existedUserInfo['id'], DbValue::TRUE);
                            }
                        }
                    } else {    
                        if(!empty($existedUserInfo['faceImgPath'])){   //清除人脸
                            $this->getUserService()->clearFace($existedUserInfo['id']);
                            $this->getUserService()->setFacePhotoFinish($existedUserInfo['id'], DbValue::FALSE);
                        }
                    }
                } else {
                    //判断身份证信息是否注册过，如果注册过，则是一个用户多个行政区域
                    $existedUserCount = $this->getUserService()->countUserProfileByIdcard($userInfo['stuidentitycard']);
                    if ($existedUserCount > 0) {
                        $existedUserProfile = $this->getUserService()->findUserProfileByIdcard($userInfo['stuidentitycard']);
                        $existedUser = $this->getUserService()->getUser($existedUserProfile['id']);

                        $newUserInfo = $this->addUser($userInfo, $existedUser, $existedUserCount);
                        $userInfo['id'] = $newUserInfo['id'];
                        $this->addUserProfile($userInfo, $existedUserProfile);
                    } else {
                        $newUserInfo = $this->addUser($userInfo, array(), 0);
                        $userInfo['id'] = $newUserInfo['id'];
                        $this->addUserProfile($userInfo, array());
                        
                        if(!empty($userInfo['stuphoto'])){
                            $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');
                            $this->getFaceDetectService()->updateUserFaceImg($userInfo['id'], $userInfo['stuphoto'], $publicUploadDir);
                            $this->getUserService()->setFacePhotoFinish($userInfo['id'], DbValue::TRUE);
                        }
                    }
                }
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleCertificate($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_certificate', $jsonStr);
        $certificateInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($certificateInfo, array('dID','stuId', 'competencyno','qualitydate'));

        $userInfo = $this->getUserService()->findUserByStuId($certificateInfo['stuId']);
        $userProfile = $this->getUserService()->getUserProfile($userInfo['id']);
        
        $certificate = $this->getCertificateService()->findCertificateByName($certificateInfo['competencyno']);
        
        $existedUserCertificateInfo = $this->getCertificateService()->findCertificationBydID($certificateInfo['dID']);

        $certificateInfo['userId'] = $userInfo['id'];
        $certificateInfo['areaCode'] = $userInfo['areaCode'];
        $certificateInfo['verifiedMobile'] = $userInfo['verifiedMobile'];
        $certificateInfo['idcard'] = $userProfile['idcard'];
        $certificateInfo['certificateId'] = $certificate['id'];
        $certificateInfo['nickname'] = $userInfo['nickname'];
        $that = $this;
        $this->getUserCertificateDao()->getConnection()->transactional(
            function($conn) use ($that, $certificateInfo, $existedUserCertificateInfo, $userInfo) {
                if (!empty($existedUserCertificateInfo)) {
                     if (($existedUserCertificateInfo['userId'] != $certificateInfo['userId']) 
                        || ($existedUserCertificateInfo['certificationId'] != $certificateInfo['certificateId'])){
                        
                        $this->getCertificateService()->deleteUserCertificateById($existedUserCertificateInfo['id']);
                        $this->addUserCertificate($certificateInfo);
                        $this->sendAddNewCertSmsIfNeed($userInfo, $certificateInfo);
                     } else {
                        $needUpdate = false;
                        if(!empty($certificateInfo['qualitydate'])){
                            if(strtotime($certificateInfo['qualitydate']) != $existedUserCertificateInfo['awardTime']){
                                if ($existedUserCertificateInfo['isRevoked']) { //已吊销需要重新颁发证书
                                    $this->getCertificateService()->reAwardUserCertification(
                                            $certificateInfo['userId'], $certificateInfo['certificateId'], 
                                            strtotime($certificateInfo['qualitydate']), $certificateInfo['dID']);
                                    // 泰州市的证书吊销后重新颁发，　不会发送短信
                                    if ($userInfo['areaCode'] != '3998969470') { 
                                        $this->sendAddNewCertSmsIfNeed($userInfo, $certificateInfo);
                                    }
                                    return;
                                }
                                $courses = $this->getCourseService()->findCoursesByCertificationIdAndAreaCode(
                                    $certificateInfo['certificateId'], $certificateInfo['areaCode']);
                                if(!empty($courses)){
                                    $courseIds = ArrayToolkit::column($courses, "id");                      
                                    $courseMembers = $this->getCourseService()->findCourseStudentsByCourseIds($courseIds);
                                    if(!empty($courseMembers)){
                                        $userIds = array_unique(ArrayToolkit::column($courseMembers, "userId"));
                                        if(!in_array($certificateInfo['userId'], $userIds)){
                                            $needUpdate = true;
                                        }
                                    } else {
                                        $needUpdate = true;
                                    }
                                } else {
                                    $needUpdate = true;
                                }
                            }
                        }

                        if($needUpdate){
                            $this->updateUserCertificate($certificateInfo['userId'], $certificateInfo['certificateId'], strtotime($certificateInfo['qualitydate']));
                        }
                     }
                } else {
                    //如果userId已经跟certificateId关联，则报错
                    $existedUserCertificateInfo = $this->getCertificateService()->findCertificationIdAndUserId($certificateInfo['userId'], $certificateInfo['certificateId']);
                    if (!empty($existedUserCertificateInfo)) {
                        throw new \Exception("user already has certfication");
                    } else {
                        $this->addUserCertificate($certificateInfo);
                        $this->sendAddNewCertSmsIfNeed($userInfo, $certificateInfo);
                    }
                }
            }
        );

        $this->getCertificateService()->generateUserCertCourses($userInfo['id']);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleOfflineFinishedCourse($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_offline_finished_course', $jsonStr);
        $courseInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($courseInfo, array('dID', 'stuId', 'competencyno', 'beginTime','endTime'));
        $userInfo = $this->getUserService()->findUserByStuId($courseInfo['stuId']);
        $certificate = $this->getCertificateService()->findCertificateByName($courseInfo['competencyno']);
        $courseInfo['userId'] = $userInfo['id'];
        $courseInfo['areaCode'] = $userInfo['areaCode'];
        $courseInfo['certificationId'] = $certificate['id'];
        $existedCourseInfo = $this->getCourseService()->getOfflineFinishedCourseByDid($courseInfo['dID']);
        $that = $this;
        $this->getOfflineFinishedCourseDao()->getConnection()->transactional(
            function($conn) use ($that, $courseInfo, $existedCourseInfo) {
                if(empty($existedCourseInfo)){
                    $this->addOfflineFinishedCourse($courseInfo);
                } else {
                    $this->updateOfflineFinishedCourse($courseInfo);
                }
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleApprovalSubmit($id)
    {
        $userApprovalInfo = $this->getUserApprovalDao()->getApproval($id);
        $userId = $userApprovalInfo['userId'];
        $userInfo = $this->getUserService()->getUser($userId);
        $areaCode = $userInfo['areaCode'];
        $userProfile = $this->getUserService()->getUserProfile($userInfo['id']);

        $remote_dir = "/sh/".date("Y-m-d")."/".$userProfile['idcard']."/";

        $fileUrlPath = ltrim(ServiceKernel::instance()->getParameter('topxia.upload.public_url_path')."/approval/", '/');
        $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory')."/approval/";

        $idCardPhotoFront = str_replace($fileUrlPath, $publicUploadDir, $userApprovalInfo['faceImg']);
        $idCardPhotoFront = $this->approvalImgSave($areaCode, $idCardPhotoFront, $remote_dir);

        $idCardPhotoBack = str_replace($fileUrlPath, $publicUploadDir, $userApprovalInfo['backImg']);
        $idCardPhotoBack = $this->approvalImgSave($areaCode, $idCardPhotoBack, $remote_dir);

        if (empty($userApprovalInfo['driverLicenseImg'])) {
            $driverPhoto = "";
        } else {
            $driverPhoto = str_replace($fileUrlPath, $publicUploadDir, $userApprovalInfo['driverLicenseImg']);
            $driverPhoto = $this->approvalImgSave($areaCode, $driverPhoto, $remote_dir);
        }

        if (empty($userApprovalInfo['jobsSeniorityCardImg'])) {
            $employmentPhoto = "";
        } else {
            $employmentPhoto = str_replace($fileUrlPath, $publicUploadDir, $userApprovalInfo['jobsSeniorityCardImg']);
            $employmentPhoto = $this->approvalImgSave($areaCode, $employmentPhoto, $remote_dir);
        }

        if (empty($userApprovalInfo['otherImg'])) {
            $otherPhoto = "";
        } else {
            $otherPhoto = str_replace($fileUrlPath, $publicUploadDir, $userApprovalInfo['otherImg']);
            $otherPhoto = $this->approvalImgSave($areaCode, $otherPhoto, $remote_dir);
        }
         
        $sendContent = array(
            "stuId" => $userInfo['stuId'],  //学员ID
            "identitycard" => $userApprovalInfo['idcard'], //身份证号
            "createtime" => date("Y-m-d H:i:s", $userApprovalInfo['createdTime']), 
            "idCardPhotoFront" => $idCardPhotoFront,  //身份证正面片（Base64）
            "idCardPhotoBack" => $idCardPhotoBack,   //身份证反面照片（Base64）
            "driverPhoto" => $driverPhoto,  //驾驶证照片（Base64）
            "employmentPhoto" => $employmentPhoto,  //从业资格证书正面照片（Base64）
            "otherPhoto" => $otherPhoto //其他照片（Base64）
        );

        return $this->soapClientSendData('user_approval', $userInfo['areaCode'], $sendContent);
    }

    public function approvalImgSave($areaCode, $fileUrl, $remote_dir){
        $fileArray = explode("/", $fileUrl);
        $fileName = $fileArray[count($fileArray)-1];
        $sourceFile = $fileUrl;
        $remoteFile = $remote_dir.$fileName;

        $this->getSyncFtpService()->updateSyncFtpInfo($areaCode, $sourceFile, $remoteFile, 
            'sync_approval');

        return $remoteFile;
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleApprovalResult($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_approval_result', $jsonStr);
        $approvalInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($approvalInfo, array('stuId', 'identitycard', 'result'));

        $userInfo = $this->getUserService()->findUserByStuId($approvalInfo['stuId']);

        if ($approvalInfo['result'] == 'approved') {
            $this->getUserService()->passApproval($userInfo['id'], $approvalInfo['reason']);
        } else {
            $this->getUserService()->rejectApproval($userInfo['id'], $approvalInfo['reason']);
        }
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleBuyCourse($orderId)
    {
        $this->getLogService()->info('DataSync', 'sync_buycourse', "订单:".$orderId);
        $order = $this->getOrderService()->getOrder($orderId);
        $userInfo = $this->getUserService()->getUser($order['userId']);
        $userProfile = $this->getUserService()->getUserProfile($userInfo['id']);
        $course = $this->getCourseService()->getCourse($order['targetId']);
        $payment = $order['payment'];
        if ($payment == 'wxpay' || $payment == 'wxpay_mobile') {
            $payment = 1;
        } elseif ($payment == 'alipay') {
            $payment = 2;
        } elseif ($payment == 'offlinepay' ) {
            $payment = 4;
        } else {
            $payment = 3;
        }
         
        $sendContent = array(
            'stuId' => $userInfo['stuId'],  //学员ID
            'orderSn' => $order['sn'],
            'identitycard' => $userProfile['idcard'], //身份证号
            'className' => $course['title'],  //课程名称
            'classMoney' => $order['totalPrice'],  //课程金额
            'buyWay' => $payment,   //购买方式（1微信；2支付宝; 3其他;　４线下支付）
            'buyTime' => date('Y-m-d H:i:s', $order['paidTime'])  //支付时间
        );

        $logFile = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') . '/../../app/logs/sync_buycourse';
        file_put_contents($logFile,  "\n\n", FILE_APPEND); 
        file_put_contents($logFile, '[' . date('Y-m-d H:i:s') . '] ' . json_encode($sendContent), FILE_APPEND);

        return $this->soapClientSendData('lesson', $userInfo['areaCode'], $sendContent);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleGraduateCertificate($userId, $courseId, $currentLoopTime)
    {
        $this->getLogService()->info('DataSync', 'sync_graduate_certificate', 
            "证书结业:".$userId.",".$courseId.",".$currentLoopTime);

        $sendContent = $this->genereateSingleGraduateCertificateInfo($userId, $courseId, $currentLoopTime); 

        $logFile = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') . '/../../app/logs/sync_graduate_certificate';
        file_put_contents($logFile,  "\n\n", FILE_APPEND); 
        file_put_contents($logFile, '[' . date('Y-m-d H:i:s') . '] ' . json_encode($sendContent), FILE_APPEND);

        $userInfo = $this->getUserService()->getUser($userId);

        return $this->soapClientSendData('graduate', $userInfo['areaCode'], $sendContent);
    }

    public function genereateSingleGraduateCertificateInfo($userId, $courseId, $currentLoopTime)
    {
        $userInfo = $this->getUserService()->getUser($userId);

        $userCertCourseInfo = $this->getUserCertCourseDao()->getUserCertCourse($userId, $courseId, 
            $currentLoopTime);
        $certificationId = $userCertCourseInfo['certificationId'];
        $certification = $this->getCertificateService()->getCertification($certificationId);
        $courses = $this->getCertificateService()->getCertificationsForUser($userId);
        
        $isContinue = false;
        foreach($courses as $course){
            if($course['certificationId'] == $certificationId && $course['currentLoopTime'] == $currentLoopTime){
                $startTime = $course['certificationStartPeriod'];
                $endTime = $course['certificationEndPeriod'];
                $isContinue = true;
                break;
            }
        }

        if(!$isContinue){  //以防万一
            return true;
        }

        return $this->generateCertificateInfo($userInfo, $certification, $startTime, $endTime, 
            Action::BUYED, $courseId); 
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleStuCertificate($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_stu_certificate', $jsonStr);
        $certificateInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($certificateInfo, array('stuId', 'competencyno', 'startTime', 'endTime'));
        $userInfo = $this->getUserService()->findUserByStuId($certificateInfo['stuId']);
        $certification = $this->getCertificateService()->findCertificateByName($certificateInfo['competencyno']);
        $startTime = strtotime($certificateInfo['startTime']." 00:00:00");
        $endTime = strtotime($certificateInfo['endTime']." 23:59:59");

        $content = $this->generateCertificateInfo($userInfo, $certification, $startTime, $endTime, 
            Action::BUYED, 0);

        $logFile = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') . '/../../app/logs/sync_stu_certificate';
        file_put_contents($logFile,  "\n\n", FILE_APPEND); 
        file_put_contents($logFile, '[' . date('Y-m-d H:i:s') . '] ' . json_encode($content), FILE_APPEND);

        return $this->getEncryptedText($content);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleRevokeCertificate($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_revoke_certificate', $jsonStr);
        $certificateInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($certificateInfo, array('stuId', 'competencyno', 'reason', 'createtime'));
        $userInfo = $this->getUserService()->findUserByStuId($certificateInfo['stuId']);
        $certificate = $this->getCertificateService()->findCertificateByName($certificateInfo['competencyno']);
        $certificateInfo['userId'] = $userInfo['id'];
        $certificateInfo['certificateId'] = $certificate['id'];

        $existedUserCertificateInfo = $this->getCertificateService()->findCertificationIdAndUserId($certificateInfo['userId'],$certificateInfo['certificateId']);

        $that = $this;
        $this->getUserCertificateDao()->getConnection()->transactional(
            function($conn) use ($that, $certificateInfo, $existedUserCertificateInfo) {
                if(empty($existedUserCertificateInfo)){
                    throw new \Exception("用户与证书的关联信息不存在");
                }
                else{
                    if($existedUserCertificateInfo['isRevoked']==0){
                        $this->getCertificateService()->revokeUserCertification($existedUserCertificateInfo['id'],
                            $certificateInfo['reason'], strtotime($certificateInfo['createtime']));

                        $msg = "数据同步：注销学号为".$certificateInfo['stuId']."的学员《".$certificateInfo['competencyno']
                            ."》证书";
                        $this->getLogService()->info('DataSync', 'revoke_certificate', $msg);
                    }
                }
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleResetPassword($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_reset_password', $jsonStr);
        $passwordInfo = $this->transferToArray($jsonStr);
        $this->validateRequiredFields($passwordInfo, array('stuId', 'password'));

        $userInfo = $this->getUserService()->findUserByStuId($passwordInfo['stuId']);
        $this->getUserService()->changePassword($userInfo['id'], $passwordInfo['password']);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleChangeMobile($id)
    {
        $this->getLogService()->info('DataSync', 'sync_change_mobile', '修改手机号:'.$id);
        $changeMobileInfo = $this->getChangeMobileService()->findChangeMobileInfoById($id);
        $userInfo = $this->getUserService()->getUser($changeMobileInfo['userId']);

        $sendContent = array(
            "stuId" => $userInfo['stuId'],  //学员ID
            "mobile" => $changeMobileInfo['mobile']
        );

        return $this->soapClientSendData('change_mobile', $userInfo['areaCode'], $sendContent);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleOrderRefund($id)
    {
        $this->getLogService()->info('DataSync', 'sync_order_refund', '退出学习:'.$id);
        $orderRefundInfo = $this->getOrderService()->findRefundById($id);
        $userInfo = $this->getUserService()->getUser($orderRefundInfo['userId']);
        $orderInfo = $this->getOrderService()->getOrder($orderRefundInfo['orderId']);
        $course = $this->getCourseService()->getCourse($orderInfo['targetId']);

        $sendContent = array(
            "stuId" => $userInfo['stuId'],  //学员ID
            "className" => $course['title'],
            "buyTime" => date("Y-m-d H:i:s", $orderInfo['paidTime'])
        );
        
        return $this->soapClientSendData('order_refund', $userInfo['areaCode'], 
            $sendContent);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleUserRegister($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_user_register', $jsonStr);

        $fields = $this->transferToArray($jsonStr);
        $this->validateSyncRegisterFields($fields);
    
        $user = $this->getUserService()->findUserByStuId($fields['stuId']);
        if (empty($user)) {
            throw new \Exception("user stuId #{$fields['stuId']} not found");
        }
        if(!$user['toLearn']) {
            throw new \Exception(" user is not toLearn student");
        }

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        if (empty($userProfile)) {
            throw new \Exception("userProfile id #{$user['id']} not found");
        }

        $password = substr(md5(time().mt_rand(1,100)), 0, 6);
        $salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);

        $registerFields = array(
            'salt'     => $salt,
            'password' => $this->getPasswordEncoder()->encodePassword($password, $salt),
            'toLearn' => 0,
            'verifiedMobile' => $fields['mobile'],
        );

        $that = $this;
        $this->getUserDao()->getConnection()->transactional(
            function($conn) use ($that, $user, $registerFields) {
                $this->getUserDao()->updateUser($user['id'], $registerFields);
            }
        );

        $smsParams = array(
            'mobile' => $fields['mobile'],
            'category'   => SmsService::REGIST,
            'parameters' => array(
                'password' => $password,
            ),
        );

        $this->getSmsService()->sendMsgBySms($user['id'], $userProfile['idcard'], $smsParams);
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleUserCertificateFrozen($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_user_certificate_frozen', $jsonStr);

        $fields = $this->transferToArray($jsonStr);

        $this->validateRequiredFields($fields, array('stuId', 'competencyno', 'type'));

        $user = $this->getUserService()->findUserByStuId($fields['stuId']);
        if (empty($user)) {
            throw new \Exception("user stuId #{$fields['stuId']} not found");
        }

        $certificate = $this->getCertificateService()->findCertificateByName($fields['competencyno']);
        if (empty($certificate)) {
            throw new \Exception("certificate competencyno #{$fields['competencyno']} not found");
        }

        $userCertification = $this->getCertificateService()->findCertificationIdAndUserId($user['id'],$certificate['id']);
        if (empty($userCertification)) {
            throw new \Exception("user certificate not found");
        }
        //如果该用户证书已经被吊销则抛出异常
        if ($userCertification['isRevoked']) {
            throw new \Exception("user #{$user['id']} certificate #{$certificate['id']} is revoked");
        }

        $that = $this;
        $this->getUserCertificateDao()->getConnection()->transactional(
            function($conn) use ($that, $userCertification, $fields) {
                if ($fields['type'] == 'freeze') {
                    $this->getCertificateService()->freezeUserCertification($userCertification['id'], $fields);
                } else if ($fields['type'] == 'unfreeze') {
                    $this->getCertificateService()->unfreezeUserCertification($userCertification['id'], $fields);
                } else {
                    throw new \Exception("type is invalid");
                }
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncSingleUserCourseFrozen($jsonStr)
    {
        $this->getLogService()->info('DataSync', 'sync_user_course_frozen', $jsonStr);

        $fields = $this->transferToArray($jsonStr);

        $this->validateRequiredFields($fields, array('stuId', 'loopStartTime', 'loopEndTime', 'competencyno', 'type'));

        $user = $this->getUserService()->findUserByStuId($fields['stuId']);

        if (empty($user)) {
            throw new \Exception("user stuId #{$fields['stuId']} not found");
        }

        $certificate = $this->getCertificateService()->findCertificateByName($fields['competencyno']);
        if (empty($certificate)) {
            throw new \Exception("certificate competencyno #{$fields['competencyno']} not found");
        }

        $userCertCourseInfo = $this->getUserCertCourseDao()->getUserCertCourseByCertificationIdAndLoopTime(
            $user['id'],
            $certificate['id'], 
            $fields['loopStartTime'],
            $fields['loopEndTime']
        );

        if (empty($userCertCourseInfo)) {
            throw new \Exception("user cert course not found in this loop time");
        }

        $that = $this;
        $this->getUserCertificateDao()->getConnection()->transactional(
            function($conn) use ($that, $userCertCourseInfo, $fields) {
                if ($fields['type'] == 'freeze') {
                    $this->getCertificateService()->freezeUserCertCourse($userCertCourseInfo['id'], $fields);
                } else if ($fields['type'] == 'unfreeze') {
                    $this->getCertificateService()->unfreezeUserCertCourse($userCertCourseInfo['id'], $fields);
                } else {
                    throw new \Exception("type is invalid");
                }
            }
        );
    }

    /**
     * @see Custom\Service\Sync\SyncService
     */
    public function syncUserRegisteredFace($faceDetectResultId)
    {
        $faceDetectResult = $this->getFaceDetectResultDao()->getFaceDetectResult($faceDetectResultId);

        $user = $this->getUserService()->getUser($faceDetectResult['userId']);

        // $userProfile = $this->getUserService()->getUserProfile($user['id']);

        // $photoStr = $this->generateFacePhotoStr($faceDetectResult, $userProfile);

        $photoStr = $this->generateBase64PhpotoStr($faceDetectResult);

        $sendContent = array(
            'stuId' => $user['stuId'],  //学员ID
            'photoStr' => $photoStr, //人脸识别照片（Base64）
        );

        return $this->soapClientSendData('user_face_detect', $user['areaCode'], $sendContent);
    }

    protected function getFaceDetectResultDao()
    {
        return $this->createDao('Custom:FaceDetectResult.FaceDetectResultDao');
    }

    protected function getPasswordEncoder()
    {
        return new MessageDigestPasswordEncoder('sha256');
    }

    private function generateBase64PhpotoStr($faceDetectResult)
    {
        $fileParse = $this->getFileService()->parseFileUri($faceDetectResult['fileUrl']);

        $filepath = ServiceKernel::instance()->getParameter('kernel.root_dir').'/../web/files/'.$fileParse['path'];

        $base64File = '';

        if (file_exists($filepath)) {
            $base64File = base64_encode(file_get_contents($filepath));
        }

        return $base64File;
    }

    private function generateFacePhotoStr($faceDetectResult, $userProfile)
    {
        //按照SyncDataFtpFileJob的方式生成photoStr
        $fileUrl = $faceDetectResult['fileUrl'];
        $fileArray = explode('/', $fileUrl);
        $createdDateStr = $this->caculateDateStr($fileArray, $faceDetectResult['createdTime']);
        $remote_dir = '/jy/' . $createdDateStr . '/' . $userProfile['idcard'] . '/';
        $fileName = $fileArray[count($fileArray)-1];
        return $remote_dir.$fileName;
    }

    private function caculateDateStr($fileArray, $dateTime)
    {
        $createdDateStr = $fileArray[count($fileArray)-2];
        if (empty($createdDateStr) || preg_match("/[^\d-., ]/", $createdDateStr)) {
            $createdDateStr = date('Y-m-d', $dateTime);
        } else {
            $createdDateStr = date('Y-m-d', strtotime($createdDateStr));
        }
        return $createdDateStr;
    }

    private function validateSyncRegisterFields($fields)
    {
        $this->validateRequiredFields($fields, array('stuId', 'mobile'));

        if (!SimpleValidator::mobile($fields['mobile'])){
            throw new \Exception("mobile is invalid");
        }
    }

    private function transferToArray($jsonStr) 
    {
        try {
            $data = json_decode($jsonStr, true);

        } catch (\Exception $e) {
            throw new ArgumentNotJsonException();
        }
        return $data;
    }

    private function generateAreaCodePerAreaName($areaName)
    {
        return crc32($areaName);
    }

    /*
     * 检验 $data中是否包含所给属性, 如果不包含, 则抛出 MissingFieldException
     * @param  $data 数组
     * @fieldNames  属性名, 数组
     */
    private function validateRequiredFields($data, $fieldNames)
    {
        foreach ($fieldNames as $field) {
            if (!isset($data[$field]) || empty($data[$field])) {
                throw new MissingFieldException($field);
            }
        }
    }

    private function updateArea($areaId, $areaInfo)
    {
        $this->getAreaService()->updateArea($areaId, 
            array(
                'province' => $areaInfo['province'],
                'ip' => $areaInfo['ip'],
                'updatedTime' => strtotime('now'),
                'ftpUrl' => $areaInfo['ftp_url'],
                'ftpName' => $areaInfo['ftp_name'],
                'ftpPassword' => $areaInfo['ftp_password']
            )
        );
    }

    private function createOrQueryDbCertifications($commarSeperatedNames)
    {
        $certificationNames = StringUtils::trimStrArray(explode(',', $commarSeperatedNames));
        $certifications = $this->getCertificateService()->findCertificatesByNames($certificationNames);
        $createdCertifications = array();

        foreach ($certificationNames as $certName) {
            $isFound = false;
            if (!empty($certifications)) {
                foreach ($certifications as $cert) {
                    if ($cert['category'] == $certName) {
                        $isFound = true;
                        break;
                    }
                }
            }

            if (!$isFound) {
                array_push($createdCertifications, $certName);
            }
        }
        
        $alreadyCreatedCertifications = $this->getCertificateService()->createCertifications($createdCertifications);

        $result = array_merge($certifications, $alreadyCreatedCertifications);
        
        
        return $result;
    }

    private function updateOrAddAreaCertifications($areaInfo, $certifications)
    {
        $areaCertifications = $this->getCertificateService()->getAreaCertifications($areaInfo['areaCode']);

        //数据库中比同步的信息要多, 需要做删除
        $deletedCertificationIds = array();
        foreach ($areaCertifications as $areaCertification) {
            $isFound = false;
            foreach ($certifications as $certification) {
                if ($areaCertification['certificationId'] == $certification['id']) {
                    $isFound = true;
                    break;
                }
            }

            if (!$isFound) {
                array_push($deletedCertificationIds, $areaCertification['certificationId']);
            }
        }

        foreach ($deletedCertificationIds as $deletedCertificationId) {
            $this->getCertificateService()->deleteAreaCertfication($areaInfo['areaCode'], $deletedCertificationId);
        }

        //同步的信息中比数据库要多, 需要做添加
        $addedCertificationIds = array();
        foreach ($certifications as $certification) {
            $isFound = false;
            foreach ($areaCertifications as $areaCertification) {
                if ($areaCertification['certificationId'] == $certification['id']) {
                    $isFound = true;
                    break;
                }
            }

            if (!$isFound) {
                array_push($addedCertificationIds, $certification['id']);
            }
        }

        foreach ($addedCertificationIds as $addedCertificationId) {
            $this->getCertificateService()->createAreaCertification(
                array(
                    'areaCode' => $areaInfo['areaCode'],
                    'certificationId' => $addedCertificationId
                )
            );
        }
    }

    private function addArea($areaInfo)
    {
        $this->getAreaService()->addArea(
            array(
                'code' => $areaInfo['areaCode'],
                'name' => $areaInfo['cityName'],
                'province' => $areaInfo['province'],
                'ip' => $areaInfo['ip'],
                'createdTime' => strtotime('now'),
                'ftpUrl' => $areaInfo['ftp_url'],
                'ftpName' => $areaInfo['ftp_name'],
                'ftpPassword' => $areaInfo['ftp_password']
            )
        );
    }

    private function addUser($userInfo, $existedUser, $existedUserCount)
    {
        if ($existedUserCount == 0) {
            $users = array(
                'stuId' => $userInfo['stuId'],
                'email' => $userInfo['stuId'].'@qq.com',
                'nickname'  => $userInfo['stuidentitycard'], 
                'type'  => 'import',
                'roles'  => '|ROLE_USER|',
                'toLearn'  => DbValue::TRUE,
                'areaCode'  => $userInfo['areaCode'],
                'password' => '',
                'salt' => '',
            ); 
            if(!empty($userInfo['stuselftel'])){
                $users['verifiedMobile'] = $userInfo['stuselftel'];
            } else {
                $users['verifiedMobile'] = '';
            }
        } else {
            $users = array(
                'stuId' => $userInfo['stuId'],
                'email' => $userInfo['stuId'].'@qq.com',
                'nickname'  => $existedUser['verifiedMobile'].str_repeat('_', $existedUserCount), 
                'type'  => 'import',
                'roles'  => '|ROLE_USER|',
                'toLearn'  => $existedUser['toLearn'],
                'areaCode'  => $userInfo['areaCode'],
                'verifiedMobile' => $existedUser['verifiedMobile'],
                'faceImgPath' => $existedUser['faceImgPath'],
                'faceImgFeature' => $existedUser['faceImgFeature'],
                'base64FaceImgPath' => $existedUser['base64FaceImgPath'],
                'password' => $existedUser['password'],
                'salt' => $existedUser['salt'],
                'approvalStatus' => $existedUser['approvalStatus'],
                'approvalTime' => $existedUser['approvalTime'],
                'facePhotoFinished' => $existedUser['facePhotoFinished']
            ); 
        }

        return $this->getUserDao()->addUser($users);
    }

    private function addUserProfile($userInfo, $existedUserProfile)
    {
        if (empty($existedUserProfile)) {
            $users = array(
                'id' => $userInfo['id'],
                'truename'  => $userInfo['stuname'],
                'idcard'  => $userInfo['stuidentitycard'],
                'gender'   => $userInfo['stusex']
            );
            if(!empty($userInfo['stuselftel'])){
                $users['mobile'] = $userInfo['stuselftel'];
            }
        } else {
            $users = array(
                'id' => $userInfo['id'],
                'truename'  => $existedUserProfile['truename'],
                'idcard'  => $existedUserProfile['idcard'],
                'gender'  => $existedUserProfile['gender'],
                'mobile'  => $existedUserProfile['mobile']
            );
        }
        $this->getUserService()->addUserProfile($users);
    }

    private function updateUser($userId, $userInfo)
    {
        $users = array(
            //'email' => $userInfo['stuId'].'@qq.com',
            //'nickname'  => $userInfo['stuidentitycard'], 
            'areaCode'  => $userInfo['areaCode']
        );
        if(!empty($userInfo['stuselftel'])){
            $users['verifiedMobile'] = $userInfo['stuselftel'];
        }
        $this->getUserService()->updateFields($userId, $users);
    }

    private function updateUserProfile($userId, $userInfo)
    {
        $users = array(
            'idcard' => $userInfo['stuidentitycard'],
            'truename'  => $userInfo['stuname'],
            'gender'   => $userInfo['stusex']
        );
        if(!empty($userInfo['stuselftel'])){
            $users['mobile'] = $userInfo['stuselftel'];
        }
        $this->getUserProfileDao()->updateProfile($userId, $users);
    }

    private function addUserCertificate($certificateInfo)
    {
        $awardTime = empty($certificateInfo['qualitydate']) ? time() : strtotime($certificateInfo['qualitydate']);
        $certificateInfo = array(
            'userId' => $certificateInfo['userId'],
            'certificationId' => $certificateInfo['certificateId'],
            'awardTime' => $awardTime,
            'isRevoked' => 0,
            'dID' => $certificateInfo['dID']
        );
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed(
            $certificateInfo['userId'], 
            $certificateInfo['certificationId']
        );
        $this->getCertificateService()->addUserCertification($certificateInfo);
    }

    private function updateUserCertificate($userId, $certificateId, $awardTime)
    {
        $this->getCertificateService()->updateUserCertificateAwardTime($userId,$certificateId,$awardTime);
    }

    private function addOfflineFinishedCourse($courseInfo)
    {
        $courses = $this->getCertificateService()->getCertificationsForUser($courseInfo['userId']);
        $beginTime = strtotime($courseInfo['beginTime']);
        $endTime = strtotime($courseInfo['endTime']);
        if(!empty($courses)){
            $isFind = false;
            foreach($courses as $course){
                if($course['certificationId'] != $courseInfo['certificationId']){
                    continue;
                }
                $startTimeNum = $course['startTimeNum'];
                $deadLineNum = strtotime($course['deadline']);
                if(($beginTime - $startTimeNum >= 0 ) && ($endTime - $beginTime >= 0) 
                    && ($deadLineNum - $endTime >=0) && !$course['isRevoked'])
                {
                    $array = array(
                        'dId' => $courseInfo['dID'],
                        'userId' => $courseInfo['userId'],
                        'currentLoopTime' => $course['currentLoopTime'],
                        'courseId' => $course['courseId'],
                        'certificationId' => $course['certificationId']
                    );
                    $this->getCourseService()->createOfflineFinishedCourse($array);
                    $isFind = true;
                }
            }

            if(!$isFind){
                throw new \Exception("no course");
            }
        }
    }

    private function updateOfflineFinishedCourse($courseInfo)
    {
        $this->getCourseService()->deleteOfflineFinishedCoursesByDid($courseInfo['dID']);
        $this->addOfflineFinishedCourse($courseInfo);
    }

    function object_array($array)
    {
        if(is_object($array)){
            $array = (array)$array;
        }
        if(is_array($array)){
            foreach($array as $key=>$value){
                $array[$key] = $this->object_array($value);
            }
         }
         return $array;
    } 

    private function getEncryptedText($text)
    {
        $encryptedText = CompressionUtils::encodeByGzipBase64(json_encode($text, JSON_UNESCAPED_SLASHES));
        return $encryptedText;
    }

    private function soapClientSendData($interface, $areaCode, $sendContent){
        $content = $this->getEncryptedText($sendContent);
        $areaInfo = $this->getAreaService()->getAreaByCode($areaCode);
        
        $ip = $areaInfo['ip'];

        if(empty($ip)){
            return false;
        }

        try {
            libxml_disable_entity_loader(false);
            $client = new \SoapClient('http://'.$ip.'/zhInterface/services/zhstu?wsdl');
            switch($interface){
                case 'lesson': $result = $client->insertLessonInfo(array('in0' => $content)); break;
                case 'graduate': $result = $client->insertGraduatedInfo(array('in0' => $content)); break;
                case 'user_approval': $result = $client->authenticationStuInfo(array('in0' => $content)); break;
                case 'change_mobile': $result = $client->updateStuInfo(array('in0' => $content)); break;
                case 'order_refund': $result = $client->deleteLessonInfo(array('in0' => $content)); break;
                case 'user_face_detect': $result = $client->updateStuPhotoStr(array('in0' => $content)); //updateStuPhotoStr接口他们那边还没做好，做完通知这边调试
                    break;
            }
            
            $array = $this->object_array($result);
            $info = json_decode($array['out'], true);
            if($info["status"]=="fail"){
                return false;
            } else{
                return true;
            }
        } catch (\Exception $e) {
            $this->getLogService()->error(
                'DataSync', 
                'sendData', 
                json_encode($sendContent) . ' error: ' .$e->getMessage(), 
                array(
                    'url' => 'http://'.$ip.'/zhInterface/services/zhstu?wsdl',
                    'detail' => $e->getTraceAsString()
                )
            );
            return false;
        }
    }

    private function getAreaService()
    {
        return $this->createService('Custom:Area.AreaService');
    }

    protected function getAreaDao()
    {
        return $this->createDao('Custom:Area.AreaDao');
    }

    private function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getCertificateDao()
    {
        return $this->createDao('Custom:Certificate.CertificateDao');
    }

    protected function getUserCertificateDao()
    {
        return $this->createDao('Custom:Certificate.UserCertificateDao');
    }

    private function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function getUserDao()
    {
        return $this->createDao('Custom:User.UserDao');
    }

    protected function getUserProfileDao()
    {
        return $this->createDao('User.UserProfileDao');
    }

    protected function getFaceDetectService()
    {
        return $this->createService('Custom:FaceDetect.FaceDetectService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getUserApprovalDao()
    {
        return $this->createDao('Custom:User.UserApprovalDao');
    }

    protected function getOrderService()
    {
        return $this->createService('Custom:Order.OrderService');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getOfflineFinishedCourseDao()
    {
        return $this->createDao('Custom:Course.OfflineFinishedCourseDao');
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }

    protected function getTestPaperService()
    {
        return $this->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getUserCertificationStatesService()
    {
        return $this->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getChangeMobileService()
    {
        return $this->createService('Custom:User.ChangeMobileService');
    }

    protected function getUserLearnTimeService()
    {
        return $this->createService('Custom:Course.UserLearnTimeService');
    }

    protected function getSyncFtpService()
    {
        return $this->createService('Custom:Sync.SyncFtpService');
    }

    protected function getUserCertCourseDao()
    {
        return $this->createDao('Custom:Certificate.UserCertCourseDao');
    }

    protected function getSmsService()
    {
        return $this->createService('Custom:Sms.SmsService');
    }

    protected function getFileService()
    {
        return $this->createService('Content.FileService');
    }

    private function sendAddNewCertSmsIfNeed($userInfo, $certificateInfo)
    {
        if ($userInfo['toLearn'] == DbValue::FALSE) {  //新增证书时, 如果学员已注册, 给该学员发送一条新增证书的短信通知
            $certificateTitle = SmsServiceImpl::getCertificationCategory($certificateInfo['competencyno']);
            $qualitydate = explode(" ", $certificateInfo['qualitydate']);
            $this->getCertificateService()->sendAddNewCertificationSms($certificateInfo['userId'], 
               $certificateInfo['idcard'], $certificateInfo['verifiedMobile'], $certificateInfo['nickname'], 
               $qualitydate[0], $certificateTitle);
        }
    }

    private function generateCertificateInfo($userInfo, $certification, $startTime, $endTime, $action, 
        $curCourseId)
    {
        $resultArray = array();
        $resultArray['stuId'] = $userInfo['stuId'];
        $userId = $userInfo['id'];

        $userProfile = $this->getUserService()->getUserProfile($userInfo['id']);
        //$remote_dir = "/jy/".date("Y-m-d",strtotime("-1 day"))."/".$userProfile['idcard']."/";
        $areaCode = $userInfo['areaCode'];
        $certificationId = $certification['id'];
        $areaCertification = $this->getCertificateService()->findByAreaCodeAndCertificationId($areaCode, 
            $certificationId);
        $userCertification = $this->getCertificateService()->findCertificationIdAndUserId($userId , $certificationId);

        //如果是action为2,则判断证书是否已经结业，如果结业则action变为3
        if($action == Action::BUYED){
            $conditions = array(
                'userId' => $userId,
                'action' => Action::GRADUATED,
                'areaCode' => $areaCode,
                'certificationId' => $certificationId
            );
            $userCertificationStates = $this->getUserCertificationStatesService()->searchStates(
                $conditions,
                array('id', 'desc'),
                0,
                PHP_INT_MAX
            );
            if(!empty($userCertificationStates)){
                $action = Action::GRADUATED;
            }
        }
        
        $conditions = array(
            'userId' => $userId,
            'action' => $action,
            'areaCode' => $areaCode,
            'certificationId' => $certificationId
        );
        $userCertificationStates = $this->getUserCertificationStatesService()->searchStates(
            $conditions,
            array('id', 'desc'),
            0,
            PHP_INT_MAX
        );
        
        if(!empty($userCertificationStates) && $action==Action::GRADUATED){
            $graduatedTime = date("Y-m-d H:i:s", $userCertificationStates[0]['createdTime']);
        }
        else{
            $graduatedTime = "";
        }

        //certification
        $certificateArray = array(
            'competencyno' => $certification['category'],
            'awardTime' => date("Y-m-d", $userCertification['awardTime']),
            'months' => $areaCertification['months'],
            'learnModeMonths' => $areaCertification['learnModeMonths'],
            'graduatedTime' => $graduatedTime
        );
        $resultArray['certification'] = $certificateArray;
        unset($certificateArray);

        $publicUploadDir = ServiceKernel::instance()->getParameter('topxia.upload.public_directory');
        
        //courses
        $courses = $this->getCertificateService()->getCertificationsForUser($userId);
        $courseArray = array();
        foreach($courses as $course){
            if($course['certificationStartPeriod'] != $startTime || $course['certificationEndPeriod'] != $endTime || 
                    $course['certificationName'] != $certification['category']) {
                continue;
            }
            
            $courseId = $course['id'];
            if($curCourseId != 0 && $curCourseId != $courseId){
                   continue;
            }
            $currentLoopTime = $course['currentLoopTime'];

            $conditions = array(
                'userId' => $userId,
                'action' => $action,
                'areaCode' => $areaCode,
                'certificationId' => $certificationId,
                'courseId' => $courseId,
                'currentLoopTime' => $currentLoopTime
            );

            if($action==Action::GRADUATED){
                $percent = '100%';
            } else{
                $percent = floor($course['learnedHours']*100/$course['totalHours']).'%'; 
            }

            if(!isset($course['buyTime'])){
                continue;
            }

            $courseDetailArray = array();
            $courseMember = $this->getCourseService()->getCourseMember($courseId, $userId, $currentLoopTime);
            $courseDetailArray = array(
                'courseName' => $course['courseName'],  
                'buyTime' => date("Y-m-d H:i:s",$course['buyTime']),  
                'startPeriod' => $course['startTime'],
                'endPeriod' => $course['deadline'],
                'percent' =>  $percent,
                'graduatedTime' => DateUtils::number2StrWithSeconds($courseMember['updatedTime'])
            );

            //lessons
            $lessons = $this->getCourseService()->getCourseLessons($courseId);
            $lessonLearns = $this->getCourseService()->findUserLearnedLessons($userId, $courseId, $currentLoopTime);
            $lessonViews = $this->getCourseService()->findLessonViewsByUserIdAndCourseId($userId, $courseId, 
                $currentLoopTime);

            $lessonArray = array();
            foreach($lessons as $lesson){
                if(!isset($lessonLearns[$lesson['id']])){
                    continue;
                }
                $lessonLearn = $lessonLearns[$lesson['id']];
                $lessonDetailArray = array();
                $lessonDetailArray = array(
                    'lessonName' => $lesson['title'], 
                    'startTime' => date('Y-m-d H:i:s', $lessonLearn['startTime']),  
                    'finishTime' => empty($lessonLearn['finishedTime'])? '': date('Y-m-d H:i:s', $lessonLearn['finishedTime']),  
                    'hours' => $lesson['suggestHours'],  //课时学时，单位分钟
                    'status' => $lessonLearn['status'],  //课时状态，finished 和 learning 2种
                );
                
                //learningrecords
                $learningRecordArray = array();
                foreach($lessonViews as $lessonView){
                    if($lessonView['lessonId'] == $lesson['id'] && $lessonView['watchTime'] != 0){
                        $learningRecordDetailArray = array();
                        $learningRecordDetailArray = array(
                            'device' => $lessonView['viewDevice'],   
                            'startTime' => date('Y-m-d H:i:s', $lessonView['createdTime']),  
                            'watchTime' => $lessonView['watchTime'] //观看时间，单位为秒
                        );

                        //faceDetect
                        $lessonFaceDects =  $this->getFaceDetectResultService()->findFaceDectResultByUserIdAndLessonViewId($userId, 
                            $lessonView['id']);
                        $lessonFaceDectArray = array();
                        if(!empty($lessonFaceDects)){
                            foreach($lessonFaceDects as $lessonFaceDect){
                                $fileUrl = $lessonFaceDect['fileUrl'];
                                $fileArray = explode("/", $fileUrl);
                                $fileName = $fileArray[count($fileArray)-1];
                                $sourceFile = str_replace('public:/', $publicUploadDir, $fileUrl);

                                $createdDateStr = $this->caculateDateStr($fileArray, $lessonFaceDect['createdTime']);
                                $remote_dir = "/jy/" . $createdDateStr . "/" . $userProfile['idcard'] . "/";
                                $remoteFile = $remote_dir.$fileName;
                                
                                $lessonFaceDectArray[] = array(
                                    'isPassed' => $lessonFaceDect['result'] == 'success' ? true : false,
                                    'base64ImageData' => $remoteFile,
                                    'createTime' => date("Y-m-d H:i:s", $lessonFaceDect['createdTime']),
                                    'regRate' => $lessonFaceDect['regRate']
                                );
                                
                                unset($file_content);
                            }
                            $learningRecordDetailArray['faceDetect'] = $lessonFaceDectArray;
                        }

                        unset($lessonFaceDects);

                        $learningRecordArray[] = $learningRecordDetailArray;
                    }
                }
                $lessonDetailArray['learningrecords'] = $learningRecordArray;
                $lessonArray[] = $lessonDetailArray;
            }

            unset($lessons);
            unset($lessonLearns);
            unset($lessonViews);

            $courseDetailArray['lessons'] = $lessonArray;
            
            //testpaper
            $testPaperIds = $this->getTestPaperService()->getAllTestpaperIdsByCourseId($courseId);
            if(!empty($testPaperIds)){
                $testPaperResults = $this->getTestPaperService()->findTestpaperResultsByTestpaperIdsAndUserId($testPaperIds, 
                    $userId, $currentLoopTime);

                $testPaperArray = array();
                //faceDetect
                $testPaperFaceDects =  $this->getFaceDetectResultService()->
                    findFaceDectResultByUserIdAndCourseIdAndCurrentLoopTime($userId, $courseId, 
                    $currentLoopTime);

                foreach($testPaperResults as $testPaperResult) {
                    $testpaper = $this->getTestPaperService()->getTestpaper($testPaperResult['testId']);
                    $testPaperDetailArray = array(
                        'isPassed' => $testPaperResult['passedStatus'] == 'passed',
                        'markers' => $testPaperResult['score'],
                        'beginTime' => date("Y-m-d H:i:s", $testPaperResult['beginTime']),
                        'endTime' => date("Y-m-d H:i:s", $testPaperResult['endTime']),
                        'usedTime' => $testPaperResult['usedTime'],
                        'testpaperName' => $testpaper['name']
                    );

                    $testPaperFaceDectArray = array();
                    if(!empty($testPaperFaceDects)){
                        foreach($testPaperFaceDects as $testPaperFaceDect){

                            $target = 'course-' . $testPaperFaceDect['courseId'] . '/lesson-' . $testPaperFaceDect['lessonId'];
                            if ($testPaperResult['target'] == $target && 
                                    $testPaperResult['beginTime'] <= $testPaperFaceDect['createdTime'] && 
                                    $testPaperResult['endTime'] >= $testPaperFaceDect['createdTime']) {
                                $fileUrl = $testPaperFaceDect['fileUrl'];
                                $fileArray = explode("/", $fileUrl);
                                $fileName = $fileArray[count($fileArray)-1];
                                $sourceFile = str_replace('public:/', $publicUploadDir, $fileUrl);

                                $createdDateStr = $this->caculateDateStr($fileArray, $testPaperFaceDect['createdTime']);
                                $remote_dir = "/jy/" . $createdDateStr . "/" . $userProfile['idcard'] . "/";
                                $remoteFile = $remote_dir.$fileName;
                                
                                $testPaperFaceDectArray[] = array(
                                    'isPassed' => $testPaperFaceDect['result'] == 'success' ? true : false,
                                    'base64ImageData' => $remoteFile,
                                    'createTime' => date("Y-m-d H:i:s", $testPaperFaceDect['createdTime']),
                                    'regRate' => $testPaperFaceDect['regRate']
                                );
                                
                                unset($file_content);
                            }
                        }
                        $testPaperDetailArray['faceDetect'] = $testPaperFaceDectArray;
                    }

                    $testPaperArray[] = $testPaperDetailArray;
                }
                $courseDetailArray['testpaper'] = $testPaperArray;
            }
            unset($testPaperFaceDects);
            unset($testPaperIds);

            $courseArray[] = $courseDetailArray;
        }

        unset($courses);
        
        $resultArray['course'] = $courseArray;
        unset($courseArray);

        return $resultArray;
    }
}
