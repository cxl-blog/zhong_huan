<?php
namespace Custom\Service\Sync\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\Sync\SyncFtpService;

class SyncFtpServiceImpl extends BaseService implements SyncFtpService
{
    public function addSyncFtpInfo($fields)
    {
        return $this->getSyncFtpDao()->addSyncFtpInfo($fields);
    }

    public function updateSyncFtpInfo($areaCode, $sourceFile, $remoteFile, $type)
    {
        $syncFtpInfo = $this->findSyncFtpInfoBySourceFile($sourceFile);
        if(empty($syncFtpInfo)){
            $fields = array(
                'areaCode' => $areaCode,
                'sourceFile' => $sourceFile,
                'remoteFile' => $remoteFile,
                'type' => $type
            );
            $this->addSyncFtpInfo($fields);
        }
    }

    public function waveRetryTimes($id)
    {
        $this->getSyncFtpDao()->waveRetryTimes($id);
    }

    public function findSyncFtpInfoById($id)
    {
        return $this->getSyncFtpDao()->findSyncFtpInfoById($id);
    }

    public function findSyncFtpInfosByIds($ids)
    {
        return $this->getSyncFtpDao()->findSyncFtpInfosByIds($ids);
    }


    public function findSyncFtpInfoBySourceFile($sourceFile)
    {
        return $this->getSyncFtpDao()->findSyncFtpInfoBySourceFile($sourceFile);
    }

    public function searchSyncFtpInfoCountByConditions($conditions)
    {   
        return $this->getSyncFtpDao()->searchSyncFtpInfoCountByConditions($conditions);
    }

    public function searchSyncFtpInfosByConditions($conditions, $start, $limit)
    {
        return $this->getSyncFtpDao()->searchSyncFtpInfosByConditions($conditions, $start, $limit);
    }

    public function delSyncFtpInfosByIds($ids)
    {   
        return $this->getSyncFtpDao()->delSyncFtpInfosByIds($ids);
    }

    protected function getSyncFtpDao()
    {
        return $this->createDao('Custom:Sync.SyncFtpDao');
    }
}
