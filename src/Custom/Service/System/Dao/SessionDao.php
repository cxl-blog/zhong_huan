<?php 
namespace Custom\Service\System\Dao;

interface SessionDao
{
	public function getOnlineCount($conditions);

	public function getOnlineCountByUserIds($retentionTime,$userIds);

	public function deletePCLoginRecord($userId);
}