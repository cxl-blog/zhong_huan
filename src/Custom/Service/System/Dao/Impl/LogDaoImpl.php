<?php

namespace Custom\Service\System\Dao\Impl;

use Topxia\Service\System\Dao\Impl\LogDaoImpl as BaseDaoImpl;

class LogDaoImpl extends BaseDaoImpl
{
    public function analysisLoginDataByTimeWithUserId($startTime, $endTime, $userId)
    {
        $sql = "SELECT count(userId) as count, from_unixtime(createdTime,'%Y-%m-%d') as date FROM `{$this->table}` WHERE `action`='login_success' AND `createdTime`>= ? AND `createdTime`<= ? AND `userId` = ? group by from_unixtime(`createdTime`,'%Y-%m-%d') order by date ASC ";

        return $this->getConnection()->fetchAll($sql, array($startTime, $endTime, $userId));
    }

    public function clearLogByModuleAndCreatedTime($module, $createdTime)
    {
        $sql = "DELETE FROM {$this->table} WHERE module = ? AND createdTime < ?";
        $result = $this->getConnection()->executeUpdate($sql, array($module, $createdTime));
        $this->clearCached();
        return $result;
    }
}
