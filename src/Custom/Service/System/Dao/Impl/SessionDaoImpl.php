<?php

namespace Custom\Service\System\Dao\Impl;

use Custom\Service\System\Dao\SessionDao;
use Topxia\Service\System\Dao\Impl\SessionDaoImpl as TopxiaSessionDaoImpl;

class SessionDaoImpl extends TopxiaSessionDaoImpl implements SessionDao
{
        public function getOnlineCountByUserIds($retentionTime,$userIds)
        {
                if(empty($userIds)){
                        return 0;
                }
                $marks = str_repeat('?,', count($userIds) - 1).'?';

                $sql   = "SELECT COUNT(*) FROM {$this->table} WHERE `sess_time` >= (unix_timestamp(now()) - ?) AND sess_user_id IN ({$marks});";

                return $this->getConnection()->fetchColumn($sql,array_merge(array($retentionTime),$userIds));
        }


        public function getOnlineStudentCount($conditions)
        {
            $params=array($conditions['retentionTime']);
            $sql = "SELECT COUNT(*) FROM {$this->table} INNER JOIN user ON {$this->table}.sess_user_id = user.id WHERE sess_time  >= (unix_timestamp(now()) - ?) AND areaCode !=''";

            if (!empty($conditions['areaCode'])) {
                $params[]=$conditions['areaCode'];
                $sql .= " AND areaCode = ? ";
             }

            return $this->getConnection()->fetchColumn($sql, $params) ?: 0;
        }

        public function deletePCLoginRecord($userId)
        {
            return $this->getConnection()->delete($this->table, array('sess_user_id' => $userId));
        }

}