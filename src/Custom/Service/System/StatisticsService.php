<?php

namespace Custom\Service\System;

interface StatisticsService
{
    public function getOnlineCountNotInAdminAndTeacher($conditions);

    public function findRegistedUsersGroupByCertificateAndArea($province = null);

    public function deletePCLoginRecord($userId);

    public function getOnlineCount($retentionTime);
	
    public function getloginCount($retentionTime);
}
