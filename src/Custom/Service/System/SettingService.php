<?php

namespace Custom\Service\System;

interface SettingService
{
    public function isNormalClassInteractionDisplayed();
}
