<?php

namespace Custom\Service\System\Impl;

use Topxia\Service\System\Impl\LogServiceImpl as BaseServiceImpl;

class LogServiceImpl extends BaseServiceImpl
{
    public function analysisLoginDataByTimeWithUserId($startTime, $endTime, $userId)
    {
        return $this->getCustomLogDao()->analysisLoginDataByTimeWithUserId($startTime, $endTime, $userId);
    }

    public function clearLogByModuleAndCreatedTime($module, $createdTime)
    {
        return $this->getCustomLogDao()->clearLogByModuleAndCreatedTime($module, $createdTime);
    }

    protected function getCustomLogDao()
    {
        return $this->createDao('Custom:System.LogDao');

    }
}
