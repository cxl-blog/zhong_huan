<?php
namespace Custom\Service\System\Impl;

use Topxia\Service\Common\BaseService;
use Topxia\Service\System\Impl\SettingServiceImpl as TopxiaBase;
use Custom\Service\System\SettingService;

class SettingServiceImpl extends TopxiaBase implements SettingService
{
    public function isNormalClassInteractionDisplayed()
    {
        $courseSetting = $this->get('course');

        if (!empty($courseSetting['is_show_classInteraction'])) {
            return true;
        }

        return false;
    }
}
