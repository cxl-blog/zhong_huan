<?php
namespace Custom\Service\System\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Service\System\StatisticsService;
use Topxia\Service\System\Impl\StatisticsServiceImpl as TopxiaStatisticsServiceImpl;

class StatisticsServiceImpl extends TopxiaStatisticsServiceImpl implements StatisticsService
{
    public function getOnlineCountNotInAdminAndTeacher($conditions)
    {
        /*$teachers           = $this->getUserService()->searchUsers(array('roles' => 'ROLE_TEACHER'), array('id', 'DESC'), 0, 9999);
        $teacherIds         = ArrayToolKit::column($teachers, 'id');
        $teacherOnlineCount = $this->getSessionDao()->getOnlineCountByUserIds($retentionTime, $teacherIds);
        $userOnlineCount    = $this->getSessionDao()->getOnlineCount($retentionTime);
        $studentOnlineCount = $userOnlineCount - $teacherOnlineCount;
        return $studentOnlineCount;*/

        if ($this->isRedisOpened()) {
            if (!empty($conditions['areaCode'])) {
                $key = "session:logined:".$conditions['areaCode'].":*";
            } else {
                $key = "session:logined:*";
            }

            $onlines = $this->getRedis()->keys($key);

            return count($onlines);
        } else {
            return $this->getSessionDao()->getOnlineStudentCount($conditions);
        }
    }

    public function findRegistedUsersGroupByCertificateAndArea($province = null)
    {
        if ($province) {
            $areas = $this->getAreaService()->getAreaByProvince($province);
        } else {
            $areas = $this->getAreaService()->getAreas();
        }

        $areaCodes     = ArrayToolkit::column($areas, 'code');
        $users         = $this->getUserDao()->findRegistedUsersGroupByCertificateAndArea($areaCodes);
        $formatedUsers = array();

        foreach ($users as $key => $user) {
            $formatedUsers[$user['certificationId']][$user['areaCode']] = $user['count'];
        }

        foreach ($areas as $area) {
            foreach ($formatedUsers as $key => $result) {
                if (!array_key_exists($area['code'], $result)) {
                    $formatedUsers[$key][$area['code']] = 0;
                }
            }
        }

        return $formatedUsers;
    }

    public function getOnlineCount($retentionTime)
    {
        if ($this->isRedisOpened()) {
            $onlines = $this->getRedis()->keys("session:online:*");
            return count($onlines);
        } else {
            return $this->getSessionDao()->getOnlineCount($retentionTime);
        }
    }

    public function getloginCount($retentionTime)
    {
        if ($this->isRedisOpened()) {
            $logins = $this->getRedis()->keys("session:logined:*");
            return count($logins);
        } else {
            return $this->getSessionDao()->getLoginCount($retentionTime);
        }
    }

    protected function isRedisOpened()
    {
        $redisSetting = $this->getSettingService()->get('redis', array());

        if (empty($redisSetting['opened']) || $redisSetting['opened'] == 0) {
            return false;
        }

        return true;
    }


    public function deletePCLoginRecord($userId)
    {
        if ($this->isRedisOpened()) {
            $redis = $this->getRedis();
            if ($redis->exists("session:logined:userId:".$userId)) {

                $sessionId = $redis->get("session:logined:userId:".$userId);
                $data = $redis->get('session:logined:'.$sessionId);
                $keyArray = array($data, 'session:logined:'.$sessionId, 'session:logined:userId:'.$userId, 'session:online:'.$sessionId);
                $redis->delete($keyArray);

                return true;
            } else {
                return true;
            }
        } else {
            return $this->getSessionDao()->deletePCLoginRecord($userId);
        }
    }

    protected function getAreaService()
    {
        return $this->createService('Custom:Area.AreaService');
    }

    protected function getUserDao()
    {
        return $this->createDao('Custom:User.UserDao');
    }

    protected function getSessionDao()
    {
        return $this->createDao('Custom:System.SessionDao');
    }

    protected function getRedis($group = 'default')
    {
        return $this->getKernel()->getRedis($group);
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function getSettingService()
    {
        return $this->getKernel()->createService('System.SettingService');
    }

}
