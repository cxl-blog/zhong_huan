<?php
namespace Custom\Service\Certificate\Dao;

interface RevokedUserCertDao
{
    public function addRevokedUserCert($fields);

    public function deleteRevokedUserCerts($userId, $certificationId);

    public function listRevokedUserCerts($userId);
    
}
