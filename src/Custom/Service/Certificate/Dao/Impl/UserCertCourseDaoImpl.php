<?php
namespace Custom\Service\Certificate\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Certificate\Dao\UserCertCourseDao;
use Custom\Common\Constants\CertCourseStatus;
use Custom\Common\Constants\DbValue;
use Custom\Common\Constants\LearnMode;

class UserCertCourseDaoImpl extends BaseDao implements UserCertCourseDao{

    protected $table = 'user_cert_course';

    public function getUserCertCourseById($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function addUserCertCourse($userCertCourse)
    {
        $affected = $this->getConnection()->insert($this->table, $userCertCourse);
        if ($affected <= 0) {
            throw $this->createDaoException('Insert user_cert_course error.');
        }
        $this->clearCached();
    }

    public function updateUserCertCourse($id, $userCertCourse)
    {
        $this->getConnection()->update($this->table, $userCertCourse, array('id'=>$id));
        $this->clearCached();
    }

    public function updateUserCertCoursePerDyncConditions($dynConditions, $updatedFields)
    {
        $this->getConnection()->update($this->table, $updatedFields, $dynConditions);
        $this->clearCached();
    }

    public function getUserCertCourse($userId, $courseId, $currentLoopTime)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:courseId:{$courseId}:currentLoopTime:{$currentLoopTime}", 
                $userId, $courseId, $currentLoopTime, function ($userId, $courseId, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and courseId=? and currentLoopTime=?";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $courseId, $currentLoopTime)) ?: null;
        }

        );
    }

    public function getUserCertCoursePerCert($userId, $certificationId, $currentLoopTime, $learnModeMonths, $partIndex)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:certificationId:{$certificationId}:currentLoopTime:{$currentLoopTime}:learnModeMonths:{$learnModeMonths}:partIndex:{$partIndex}", 
                $userId, $certificationId, $currentLoopTime, $learnModeMonths, $partIndex,  function ($userId, $certificationId, $currentLoopTime, $learnModeMonths, $partIndex) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and certificationId=? and currentLoopTime = ? and learnModeMonths = ? and partIndex = ?";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $certificationId, $currentLoopTime, $learnModeMonths, $partIndex)) ?: null;
        }

        );
    }

    public function getMaxLoopTime($userId, $certificationId)
    {
        $sql = "SELECT max(currentLoopTime) as maxLoopTime FROM {$this->getTable()} where userId=? and certificationId=?";
        return $this->getConnection()->fetchAssoc($sql, array($userId, $certificationId)) ?: null;
    }

    public function getFirstUserCertCourse($userId, $certificationId, $currentLoopTime)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:certificationId:{$certificationId}:currentLoopTime:{$currentLoopTime}", 
                $userId, $certificationId, $currentLoopTime, function ($userId, $certificationId, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and certificationId=? and currentLoopTime = ? limit 1";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $certificationId, $currentLoopTime)) ?: null;
        }

        );
    }

    public function getUserCertCoursePerCertId($userId, $certificationId)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:certificationId:{$certificationId}", 
                $userId, $certificationId, function ($userId, $certificationId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and certificationId=? and isRevoked = ?";
            return $that->getConnection()->fetchAll($sql, array($userId, $certificationId, DbValue::FALSE)) ?: null;
        }

        );
    }

    public function deleteUserCertCourse($userId, $certificationId, $currentLoopTime)
    {
        $this->getConnection()->delete($this->table, array('userId' => $userId, 'certificationId' => $certificationId, 'currentLoopTime' => $currentLoopTime));
        $this->clearCached();
    }

    public function deleteUserCertCourseForWholeCert($userId, $certificationId)
    {
        $this->getConnection()->delete($this->table, array('userId' => $userId, 'certificationId' => $certificationId));
        $this->clearCached();
    }

    public function getAvailableUserCertCoursesByUserId($userId)
    {
        $that = $this;
        return $this->fetchCached("available_userId:{$userId}", 
                $userId, function ($userId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and startTimeNum < ? and courseId != ? order by startTimeNum desc";
            return $that->getConnection()->fetchAll($sql, array($userId, time(), 0));
        }

        );
    }

    public function findUserCertCoursesByUserIdAndStatus($userId,$status, $orderBy, $start, $limit) {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy(array($orderBy, 'DESC'));
        $sql = "SELECT * FROM {$this->getTable()} where userId=? and status=? order by ? DESC limit ?,?";
        return $this->getConnection()->fetchAll($sql, array($userId,$status,$orderBy, $start, $limit));
    }

    /**
     * @see UserCertCourseDao::searchUserCertCoursesCount
     */
    public function searchUserCertCoursesCount($params) {
        $sql = "SELECT count(*) FROM {$this->getTable()}";

        if (isset($params['areaCode'])) {
             $sql .= ' where areaCode=?';
        } 

        if (isset($params['certificationId'])) {
             $sql .= ' and certificationId=?';
        }

        if (isset($params['learnModeMonths'])) {
            $sql .= ' and learnModeMonths = ?';
        }

        if (isset($params['partIndex'])) {
            $sql .= ' and partIndex = ?';
        }
        
        $data = $this->getConnection()->fetchAssoc($sql, array_values($params));
        return $data['count(*)'];
    }

    public function searchUserCertCourses($params, $start, $limit) {
        $this->filterStartLimit($start, $limit);
        $builder = $this->createDynamicQueryBuilder($params);  
        $builder
            ->from($this->table)
            ->andWhere('areaCode = :areaCode')
            ->andWhere('certificationId = :certificationId')
            ->andWhere('learnModeMonths = :learnModeMonths')
            ->andWhere('partIndex = :partIndex')
            ->select("{$this->getTable()}.*")
            ->setFirstResult($start)
            ->setMaxResults($limit);

        return $builder->execute()->fetchAll() ?: array();
    }

    /**
     * @see UserCertCourseDao::updatePerWhenSwitchToUnrelearn
     */
    public function updateRecordsWhenSwitchToUnrelearn($areaCode, $certificationId)
    {
        $sql = "update {$this->getTable()} set status = ? "
                 . 'where areaCode = ? and certificationId = ? and  deadlineNum < ? and status != ?';
        $this->getConnection()->executeUpdate($sql, 
            array(
                CertCourseStatus::OVERTIME, $areaCode, $certificationId, time(), CertCourseStatus::COMPLETION
            )
        );

        $this->clearCached();
    }

    /**
     * @see UserCertCourseDao::updateRecordsWhenSwitchToRelearn
     */
    public function updateRecordsWhenSwitchToRelearn($areaCode, $certificationId)
    {
        $sql = "update {$this->getTable()} set status = ? "
                 .  'where areaCode = ? and certificationId = ? and  deadlineNum < ? and latestBuyTime > deadlineNum '
                 .  "and unix_timestamp(date_add(from_unixtime(certificationEndPeriod), interval ? year)) > ? and status = ?";
        
        $this->getConnection()->executeUpdate($sql, 
            array(
                CertCourseStatus::LEARNING, $areaCode, $certificationId, 
                time(), LearnMode::$MAKE_UP_YEARS, time(), CertCourseStatus::OVERTIME
            )
        );

        $this->clearCached();
    }

    public function getUserCertCourseByLoopTime($userId, $courseId, $startTimeNum, $deadlineNum) {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:courseId:{$courseId}:startTimeNum:{$startTimeNum}:deadlineNum:{$deadlineNum}", 
                $userId, $courseId, $startTimeNum, $deadlineNum, function ($userId, $courseId, $startTimeNum, $deadlineNum) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and courseId=? and startTimeNum=? and deadlineNum=?";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $courseId, $startTimeNum, $deadlineNum)) ?: null;
        }

        );
    }

    public function getUserCertCourseByCertificationIdAndLoopTime($userId, $certificationId, $startTimeNum, $deadlineNum)
    {
        $that = $this;
        return $this->fetchCached("userId:{$userId}:certificationId:{$certificationId}:startTimeNum:{$startTimeNum}:deadlineNum:{$deadlineNum}", 
                $userId, $certificationId, $startTimeNum, $deadlineNum, function ($userId, $certificationId, $startTimeNum, $deadlineNum) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where userId=? and certificationId=? and DATE_FORMAT(FROM_UNIXTIME(startTimeNum), '%Y-%m-%d')=? and DATE_FORMAT(FROM_UNIXTIME(deadlineNum), '%Y-%m-%d')=? and isRevoked = 0";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $certificationId, $startTimeNum, $deadlineNum)) ?: null;
        }

        );
    }
}