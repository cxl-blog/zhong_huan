<?php
namespace Custom\Service\Certificate\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Certificate\Dao\UserCertificateDao;
use Custom\Common\Constants\DbValue;

class UserCertificateDaoImpl extends BaseDao implements UserCertificateDao
{
    protected $table = 'user_certification';

    public function findCertificatesByUserId($userId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? ORDER BY userId ASC";
        return $this->getConnection()->fetchAll($sql, array($userId));
    }

    public function findCertificationIdAndUserId($userId, $certificationId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? AND certificationId = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($userId, $certificationId)) ?: null;
    }

    public function findCertificationBydID($dID)
    {
        $sql = "SELECT * FROM {$this->table} WHERE dID=? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($dID)) ?: null;
    }

    public function findByCertificationIdsAndUserId($userId, $certificationIds)
    {
        $certificationIdArr = str_repeat('?,', count($certificationIds) - 1).'?';
        $params = array_merge(array($userId), $certificationIds);
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? AND certificationId in ({$certificationIdArr})";
        return $this->getConnection()->fetchAll($sql, $params) ?: null;
    }

    public function getUserIdsByCertificationId($certificationId)
    {
        $sql = "SELECT userId FROM {$this->table} WHERE certificationId = ?";
        return $this->getConnection()->fetchAll($sql, array($certificationId)) ?: null;
    }

    public function findCertificateCountByUserId($userId)
    {
        $sql = "SELECT COUNT(*) FROM {$this->table} WHERE userId = ? ORDER BY userId ASC";
        return $this->getConnection()->fetchColumn($sql, array($userId));
    }

    public function updateUserCertificateAwardTime($userId, $certificationId, $awardTime)
    {
        $this->getConnection()->update($this->table,
            array('awardTime' => $awardTime),
            array('userId' => $userId, 'certificationId' => $certificationId, 'isRevoked' => DbValue::FALSE)
        );

        $this->clearCached();
    }

    public function updateUsersCertification($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id'=>$id));
        $this->clearCached();
        return $this->getUserCertification($id);
    }

    public function addUserCertification($fields)
    {
        $affected = $this->getConnection()->insert($this->table, $fields);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert user certification error:');
        }

        return $this->getUserCertification($this->getConnection()->lastInsertId());
    }

    public function getUserCertification($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function deleteUserAllCertificates($userId)
    {
        $result = $this->getConnection()->delete($this->table, array('userId' => $userId));
        $this->clearCached();
        return $result;
    }

    public function deleteUserCertificateById($id)
    {
        $result = $this->getConnection()->delete($this->table, array('id' => $id));
        $this->clearCached();
        return $result;
    }

    public function findFrozenUserCertification($userId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId =? AND isFrozen = 1";
        return $this->getConnection()->fetchAll($sql, array($userId)) ?: null;
    }
}
