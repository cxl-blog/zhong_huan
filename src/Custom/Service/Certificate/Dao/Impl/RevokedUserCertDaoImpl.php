<?php
namespace Custom\Service\Certificate\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Certificate\Dao\RevokedUserCertDao;

class RevokedUserCertDaoImpl extends BaseDao implements RevokedUserCertDao
{
    protected $table = 'revoked_user_certification';

    public function addRevokedUserCert($fields)
    {
        $affected = $this->getConnection()->insert($this->table, $fields);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert revoked user cert error:');
        }

        $this->clearCached();
    }

    public function deleteRevokedUserCerts($userId, $certificationId)
    {
        $result = $this->getConnection()->delete($this->table, array('userId' => $userId, 'certificationId' => $certificationId));
        $this->clearCached();
        return $result;
    }

    public function listRevokedUserCerts($userId)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}", $userId, function ($userId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId = ?";
            return $that->getConnection()->fetchAll($sql, array($userId)) ?: null;
        }

        );
    }
}
