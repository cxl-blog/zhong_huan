<?php
namespace Custom\Service\Certificate\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Certificate\Dao\CertificationDao;

class CertificationDaoImpl extends BaseDao implements CertificationDao
{
    protected $table = 'certification';

    public function getCertification($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function getCertificationByName($certificationName)
    {
        $sql = "SELECT * FROM {$this->table} WHERE category = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($certificationName)) ?: null;
    }

    public function getCertificates()
    {
        $sql = "SELECT * FROM {$this->table}";
        return $this->getConnection()->fetchAll($sql, array());
    }

    public function addCertification($certification)
    {
        $affected = $this->getConnection()->insert($this->table, $certification);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert certification error:');
        }

        return $this->getCertification($this->getConnection()->lastInsertId());
    }

    public function batchAddCertifications($certificationInfos)
    {
        $insertedCertifications = array();
        foreach ($certificationInfos as $certification) {
            array_push($insertedCertifications, $this->addCertification($certification));
        }

        return $insertedCertifications;
    }

    public function updateCertification($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        return $this->getCertification($id);
    }

    public function deleteCertification($id)
    {
        return $this->getConnection()->delete($this->table, array('id' => $id));
    }

    public function getCertifications($ids)
    {
        if (empty($ids)) {
            return array();
        }

        $marks = str_repeat('?,', count($ids) - 1).'?';

        $intIds = array();
        foreach ($ids as $id) {
            array_push($intIds, intval($id));
        }
        $sql   = "SELECT * FROM {$this->table} WHERE id IN ({$marks})";
        return $this->getConnection()->fetchAll($sql, $intIds);
    }

    public function getCertificationsForUser($userId)
    {
        $sql = 'select user_certification.certificationId, category, awardTime, learnMode, learnModeMonths, relearn, years, months, isRevoked, startLoopTime from certification'
            .'   inner join user_certification'
            .'     on certification.id =  user_certification.certificationId'
            .'   inner join area_certification'
            .'     on area_certification.certificationId = user_certification.certificationId'
            .'   inner join user'
            .'     on area_certification.areaCode = user.areaCode and user.id = user_certification.userId'
            .' WHERE userId = ?';
        return $this->getConnection()->fetchAll($sql, array($userId));
    }

    public function getCertificationsByAreaCode($areaCode)
    {
        $sql = 'select certificationId, category, learnMode, relearn, years, 0 as isRevoked from certification'
            .'   inner join area_certification'
            .'     on area_certification.certificationId = certification.id'
            .' WHERE areaCode = ?';
        return $this->getConnection()->fetchAll($sql, array($areaCode));
    }
}
