<?php
namespace Custom\Service\Certificate\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Certificate\Dao\AreaCertificationDao;

class AreaCertificationDaoImpl extends BaseDao implements AreaCertificationDao{

    protected $table = 'area_certification';

    public function getAreaCertification($id)
    {
        $that = $this;

        return $this->fetchCached("id:{$id}",$id,function($id)use($that){
            $sql = "SELECT * FROM {$that->getTable()} where id=? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($id)) ?: null;
        });
    }

    public function addAreaCertification($areaCertification)
    {
        $areaCertification['createdTime'] = time();

        $affected = $this->getConnection()->insert($this->table,$areaCertification);

        $this->clearCached();

        if ($affected <= 0) {
            throw $this->createDaoException('Insert area_certification error.');
        }
        return $this->getAreaCertification($this->getConnection()->lastInsertId());
    }

    public function deleteAreaCertification($areaCode, $certificationId)
    {
        $result = $this->getConnection()->delete($this->table, array('areaCode' => $areaCode, 'certificationId' => $certificationId));
        $this->clearCached();
        return $result;
    }

    public function updateAreaCertification($id,$fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id'=>$id));

        $this->clearCached();

        return $this->getAreaCertification($id);
    }

    public function findByAreaCode($areaCode)
    {
        $that = $this;

        return $this->fetchCached("areaCode:{$areaCode}",$areaCode,function($areaCode)use($that){
            
            $sql = "SELECT * FROM {$that->getTable()} where areaCode=?";
            
            return $that->getConnection()->fetchAll($sql,array($areaCode));
        });
    }

    public function findByCertificationIds(array $certificationIds)
    {
        if (empty($certificationIds)) {
            return array();
        }
        $that  = $this;
        $marks = str_repeat('?,', count($certificationIds) - 1).'?';
        $keys  = implode(',', $certificationIds);
        return $this->fetchCached("certificationIds:{$keys}",$marks,$certificationIds,function ($marks, $certificationIds) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE certificationId IN ({$marks});";
            return $that->getConnection()->fetchAll($sql,$certificationIds);
        });
    }

    public function findByCertificationId($certificationId)
    {
        $that = $this;

        return $this->fetchCached("certificationId:{$certificationId}",$certificationId,function($certificationId)use($that){
            
            $sql = "SELECT * FROM {$that->getTable()} where certificationId=?";
            
            return $that->getConnection()->fetchAll($sql,array($certificationId));
        });
    }

    public function findByAreaCodeAndCertificationId($areaCode, $certificationId)
    {
        $that = $this;

        return $this->fetchCached("areaCode:{$areaCode}:certificationId:{$certificationId}",$areaCode,$certificationId,
            function($areaCode,$certificationId) use ($that) {
                $sql = "SELECT * FROM {$that->getTable()} where areaCode=? AND certificationId=? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($areaCode,$certificationId)) ?: null;
            }
        );
    }

    public function searchAreaCertifications($conditions,$orderBys,$start,$limit)
    {
        $this->filterStartLimit($start,$limit);
        $builder = $this->createAreaCertificationQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        if(!empty($orderBys)){
            $this->checkOrderBy($orderBys);
            $builder->addOrderBy($orderBys[0], $orderBys[1]);
        }

        return $builder->execute()->fetchAll() ?: array();
    }

    public function searchAreaCertificationsCount($conditions)
    {
        $builder = $this->createAreaCertificationQueryBuilder($conditions)
            ->select('COUNT(id)');
        return $builder->execute()->fetchColumn(0);
    }

    protected function createAreaCertificationQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table,$this->table)
            ->andWhere('areaCode = :areaCode')
            ->andWhere('areaCode IN (:areaCodes)');

        return $builder;
    }
}