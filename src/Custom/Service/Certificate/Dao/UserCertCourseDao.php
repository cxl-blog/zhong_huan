<?php

namespace Custom\Service\Certificate\Dao;

interface UserCertCourseDao{
    public function getUserCertCourseById($id);

    public function addUserCertCourse($userCertCourse);

    public function updateUserCertCourse($id, $userCertCourse);

    public function updateUserCertCoursePerDyncConditions($dynConditions, $updatedFields);

    public function getAvailableUserCertCoursesByUserId($userId);

    public function getUserCertCourse($userId, $courseId, $currentLoopTime);

    public function getUserCertCoursePerCert($userId, $certificationId, $currentLoopTime, $learnMode, $partIndex);

    public function getMaxLoopTime($userId, $certificationId);

    public function getFirstUserCertCourse($userId, $certificationId, $currentLoopTime);

    public function getUserCertCoursePerCertId($userId, $certificationId);

    public function deleteUserCertCourse($userId, $certificationId, $currentLoopTime);

    public function deleteUserCertCourseForWholeCert($userId, $certificationId);

    public function findUserCertCoursesByUserIdAndStatus($userId,$status, $orderBy, $start, $limit);

    /**
     * 支持用 areaCode, certificationId, partIndex 进行查询, 且 这3个参数要么都没有, 要么至少有 areaCode
     */
    public function searchUserCertCoursesCount($param);

    public function searchUserCertCourses($param, $start, $limit);

    /**
     * 补学状态改为 关闭时调用, 将所有 deadlineNum > now() 的非完结课程改为已过期
     */
    public function updateRecordsWhenSwitchToUnrelearn($areaCode, $certificationId);

    /**
     * 补学状态改为 开启时调用, 将所有
     *    deadlineNum > now() && certificationEndPeriod + 6年 < now() 的已过期课程取出来, 
     *    如果购课时间 >= deadlineNum, 则更新为LEARNING, 否则不更新
     */
    public function updateRecordsWhenSwitchToRelearn($areaCode, $certificationId);

    public function getUserCertCourseByLoopTime($userId, $courseId, $startTimeNum, $deadlineNum);

    public function getUserCertCourseByCertificationIdAndLoopTime($userId, $certificationId, $startTimeNum, $deadlineNum);
}