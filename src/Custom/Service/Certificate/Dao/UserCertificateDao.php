<?php
namespace Custom\Service\Certificate\Dao;

interface UserCertificateDao
{
    public function findCertificatesByUserId($userId);

    public function findCertificationIdAndUserId($userId, $certificationId);

    public function findCertificationBydID($dID);

    /**
     * @param $certificationIds 数组
     */
    public function findByCertificationIdsAndUserId($userId, $certificationIds);

    public function findCertificateCountByUserId($userId);

    /**
     * @param  [type] $userId          [description]
     * @param  [type] $certificationId [description]
     * @param  [type] $awardTime       必须为数字时间
     * @return [type] [description]
     */
    public function updateUserCertificateAwardTime($userId, $certificationId, $awardTime);

    public function updateUsersCertification($id, $fields);
    
    public function addUserCertification($fields);

    public function getUserCertification($id);

    public function deleteUserAllCertificates($userId);

    public function deleteUserCertificateById($id);

    public function getUserIdsByCertificationId($certificationId);

    public function findFrozenUserCertification($userId);
}
