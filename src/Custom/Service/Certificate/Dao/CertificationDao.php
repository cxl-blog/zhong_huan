<?php
namespace Custom\Service\Certificate\Dao;

interface CertificationDao
{
    public function getCertification($id);

    public function getCertificationByName($certificationName);

    public function addCertification($certification);

    public function batchAddCertifications($certificationInfos);

    public function deleteCertification($id);

    public function getCertifications($ids);

    public function getCertificates();

    /**
     * 获取用户的所有证书信息
     * array (
     *     0 => array (
     *         'certificationId' => '1',
     *         'category' => '自行车',
     *         'awardTime' => '1460689930',   //证书颁发时间, 及允许开始学习的时间
     *         'learnMode' => 0,   //0表示1年制
     *         'relearn' => 0,     //0表示不允许补学
     *         'years' => 3,
     *         'isRevoked' => 1,
     *         'startLoopTime' => 1
     *     ),
     *     1 => array (
     *         'certificationId' => '2',
     *         'category' => '火车',
     *         'awardTime' => '1460689930',
     *         'learnMode' => 1,  //1表示2年制
     *         'relearn' => 1     //1表示允许补学
     *         'years' => 3,
     *         'isRevoked' => 1,
     *         'startLoopTime' => 1
     *     )
     *  )
     * @return
     */
    public function getCertificationsForUser($userId);

    /**
     * 获取行政区域下的所有证书信息
     * array (
     *     0 => array (
     *         'certificationId' => '1',
     *         'category' => '自行车',
     *         'learnMode' => 0,   //0表示1年制
     *         'relearn' => 0     //0表示不允许补学
     *     ),
     *     1 => array (
     *         'certificationId' => '2',
     *         'category' => '火车',
     *         'learnMode' => 1,  //1表示2年制
     *         'relearn' => 1     //1表示允许补学
     *     )
     *  )
     * @return
     */
    public function getCertificationsByAreaCode($areaCode);

}
