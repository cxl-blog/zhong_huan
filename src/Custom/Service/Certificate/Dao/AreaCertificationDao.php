<?php

namespace Custom\Service\Certificate\Dao;

interface AreaCertificationDao{

    public function getAreaCertification($id);

    public function addAreaCertification($areaCertification);

    public function deleteAreaCertification($areaCode, $certificationId);

    public function updateAreaCertification($id,$fields);

    public function findByAreaCode($areaCode);

    public function findByCertificationId($certificationId);

    public function findByCertificationIds(array $certificationIds);

    public function findByAreaCodeAndCertificationId($areaCode,$certificationId);

    public function searchAreaCertifications($conditions,$orderBys,$start,$limit);

    public function searchAreaCertificationsCount($conditions);

}