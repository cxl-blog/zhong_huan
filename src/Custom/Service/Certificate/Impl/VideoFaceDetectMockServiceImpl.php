<?php
namespace Custom\Service\Certificate\Impl;

use Custom\Service\Certificate\VideoFaceDetectMockService;
use Topxia\Service\Common\BaseService;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\DateUtils;

class VideoFaceDetectMockServiceImpl extends BaseService implements VideoFaceDetectMockService
{
    public function generateMissedVideoFaceDetect()
    {
        $dateStr = DateUtils::number2Str(time() - 86400 * 1); // 1天前
        $startTime = strtotime($dateStr); 
        $learns = $this->getCourseService()->findFinishedFaceableCertVideoLessonLearns($startTime);
        $this->logInfo("total counts for start date {$dateStr}: " . count($learns));

        for ($i = 0; $i < count($learns); $i++) { 
            $learn = $learns[$i];
            $this->logInfo('current learn index: ' . $i);
            $views = $this->getCourseService()->searchLessonView(
                array(
                    'userId' => $learn['userId'],
                    'courseId' => $learn['courseId'],
                    'lessonId' => $learn['lessonId'],
                    'currentLoopTime' => $learn['currentLoopTime']
                ),
                array('id', 'asc'),
                0,
                PHP_INT_MAX
            );

            if (count($views) == 0 || $views[count($views) - 1]['watchTime'] == 0) {
                $lesson = $this->getCourseService()->getLesson($learn['lessonId']);
                $createdView = $this->getLessonViewDao()->addLessonView(array(
                    'courseId' => $learn['courseId'],
                    'lessonId' => $learn['lessonId'],
                    'userId' => $learn['userId'],
                    'fileType' => $lesson['type'],
                    'fileId' => $lesson['mediaId'],
                    'fileStorage' => 'cloud',
                    'fileSource' => $lesson['mediaSource'],
                    'createdTime' => $learn['startTime'],
                    'watchTime' => $lesson['length'],
                    'viewDevice' => 'desktop',
                    'currentLoopTime' => $learn['currentLoopTime']

                ));
                $this->logInfo('create missed view:' . json_encode($createdView));
                $views[] = $createdView;
            }
            
            $faceMarkers = $this->getMarkerService()->findFaceMarkersMetaByMediaId($learn['lessonId']);
            $faceDetects = $this->getFaceDetectResultService()->searchFaceDetectResults(
                array(
                    'userId' => $learn['userId'], 
                    'lessonId' => $learn['lessonId'],
                    'currentLoopTime' => $learn['currentLoopTime']
                ),
                'createdByAsc',
                0,
                PHP_INT_MAX
            );
            $successFaces = array();
            foreach ($faceDetects as $face) {
                if ($face['result'] == 'success') {
                    $successFaces[] = $face;
                }
            }

            if (count($successFaces) < count($faceMarkers)) {
                $missedView = $views[count($views) - 1]; // 取最后一个view
                $this->logInfo('missed for lesson learn:' . json_encode($learn));
                $this->logInfo('missed for lesson view:' . json_encode($missedView));
                $missedFaceCounts = count($faceMarkers) - count($successFaces);
                $recentFaces = $this->getFaceDetectResultService()->searchFaceDetectResults(
                    array(
                        'userId' => $learn['userId'], 
                        'result' => 'success', 
                        'endDateTimeInt' => $missedView['createdTime']
                    ), 
                    'created', 
                    0, 
                    1
                );

                if (!empty($recentFaces)) {
                    $lastRecentFace = $recentFaces[0];
                    $course = $this->getCourseService()->getCourse($learn['courseId']);
                    $lesson = $this->getCourseService()->getLesson($learn['lessonId']);
                    
                    $markers = array();
                    foreach ($faceMarkers as $marker) {
                        $markers[] = $marker;
                    }

                    $this->logInfo('-----');
                    for ($j = 0; $j < $missedFaceCounts; $j++) {
                        if ($markers[$j]['second'] < $missedView['watchTime']) {
                            $second = $markers[$j]['second'];
                        } else {
                            $second = $missedView['watchTime'] - $missedFaceCounts - 1 + $j;
                        }
                        
                        $faceDetectResult = array(
                            'userId' => $learn['userId'],
                            'type' => 'lesson',
                            'courseId' => $learn['courseId'],
                            'lessonId' => $learn['lessonId'],
                            'lessonViewId' => $missedView['id'],
                            'content' => $course['title'] . ' ' . $lesson['title'],
                            'fileUrl' => $lastRecentFace['fileUrl'],
                            'result' => 'success',
                            'createdTime' => $missedView['createdTime'] + $second,
                            'currentLoopTime' => $learn['currentLoopTime'],
                            'regRate' => $lastRecentFace['regRate']
                        );
                        $faceDetectResultId = $this->getFaceDetectResultService()
                                ->addMissedFaceDetectResult($faceDetectResult);
                        $faceDetectResult['id'] = $faceDetectResultId;

                        $this->logInfo('create missed face detect result:' . json_encode($faceDetectResult));
                        $this->getLogService()->info('missed', 'lesson-face', $faceDetectResultId, $faceDetectResult);
                    }

                    $this->logInfo('-----');
                    $this->logInfo('');
                } else {
                    $this->logInfo('no faces found, ignore');
                }
            }
        }
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getMarkerService()
    {
        return $this->createService('Custom:Marker.MarkerService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }

    protected function getLessonViewDao()
    {
        return $this->createDao('Course.LessonViewDao');
    }

    private function logInfo($msg)
    {
        $logPath = $this->getKernel()->getParameter('kernel.root_dir') . '/logs/missedVideoFace';
        file_put_contents($logPath, '[' . date("Y-m-d h:i:sa") . ']' . $msg . "\n", FILE_APPEND);
    }
}
