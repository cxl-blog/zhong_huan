<?php
namespace Custom\Service\Certificate\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\DateUtils;
use Custom\Common\Constants\DbValue;
use Custom\Common\Util\ImageUrlUtils;
use Custom\Common\Constants\LearnMode;
use Custom\Common\Constants\Action;
use Topxia\Service\Common\BaseService;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CertificateUtils;
use Custom\Common\Constants\LearnStatus;
use Custom\Common\Constants\CertCourseStatus;
use Custom\Common\Exception\CertificationNotRevokedException;
use Custom\Service\Certificate\CertificateService;
use Custom\Service\Sms\SmsService;

class CertificateServiceImpl extends BaseService implements CertificateService
{
    public function findCertificationIdAndUserId($userId, $certificationId)
    {
        return $this->getUserCertificateDao()->findCertificationIdAndUserId($userId, $certificationId);
    }

    public function findCertificationBydID($dID)
    {
        return $this->getUserCertificateDao()->findCertificationBydID($dID);
    }

    public function findCertificatesByUserId($userId)
    {
        return $this->getUserCertificateDao()->findCertificatesByUserId($userId);
    }

    public function findFrozenUserCertification($userId)
    {
        return $this->getUserCertificateDao()->findFrozenUserCertification($userId);
    }
    
    public function findCertificatesIncludingRevokedByUserId($userId)
    {
        $unRevokedUserCerts = $this->getUserCertificateDao()->findCertificatesByUserId($userId);
        $revokedUserCerts = $this->getRevokedUserCertDao()->listRevokedUserCerts($userId);

        //key 为 {$certificationId}_{$startLoopTime}
        $existedUserCerts = array(); 
        $result = array();

        if (isset($revokedUserCerts)) {
            foreach ($revokedUserCerts as $key => $userCert) {
                $key = $userCert['certificationId'] . '_' . $userCert['startLoopTime'];
                if (empty($existedUserCerts[$key])) {
                    $existedUserCerts[$key] = true;
                    array_push($result, $userCert);
                }
            }
        }

        foreach ($unRevokedUserCerts as $key => $userCert) {
            $key = $userCert['certificationId'] . '_' . $userCert['startLoopTime'];
            if (empty($existedUserCerts[$key])) {
                $existedUserCerts[$key] = 0;
                array_push($result, $userCert);
            }
        }

        return ArrayToolkit::sortTwoDimensionArray($result, 'certificationId', true);
    }

    public function findFormatedCertificatesByUserId($userId)
    {
        $certifications = $this->getCertificationDao()->getCertificationsForUser($userId);
        
        foreach ($certifications as $key => $singleCertification) {
            $startTimeNum = $singleCertification['awardTime'];

            //-1, 是因为不是完整的一天, 如2012-1-1 00:00:00 一天后应该是 2012-1-1 23:59:59
            $endTimeNum                            = strtotime( "+{$singleCertification['years']} year", $startTimeNum);
            $endTimeInfo                              = DateUtils::getYesterdayEndDateTime($endTimeNum);
            $singleCertification['startTime']         = DateUtils::number2Str($startTimeNum);
            $singleCertification['deadline']          = $endTimeInfo['dateString'];
            $singleCertification['certificationName'] = $singleCertification['category'];
            $certifications[$key]                     = $singleCertification;
        }
        return $certifications;
    }

    public function findCertificateCountByUserId($userId)
    {
        return $this->getUserCertificateDao()->findCertificateCountByUserId($userId);
    }

    public function findCertificateByName($certificationName)
    {
        return $this->getCertificationDao()->getCertificationByName($certificationName);
    }

    public function findCertificatesByNames($certificationNames)
    {
        $result = array();

        foreach ($certificationNames as $certName) {
            $certification = $this->findCertificateByName($certName);
            if (!empty($certification)) {
                array_push($result, $certification);
            }
        }

        return $result;
    }

    public function createCertifications($certificationNames)
    {
        $certificationInfos = array();
        foreach ($certificationNames as $name) {
            array_push($certificationInfos, 
                array(
                    'category' => $name
                )
            );
        }
        return $this->getCertificationDao()->batchAddCertifications($certificationInfos);
    }

    /**
     * @see CertificateService::resetUserCertificate
     */
    public function resetUserCertificate($userId, $certificationId, $awardTime)
    {
        $userCert = $this->getUserCertificateDao()->findCertificationIdAndUserId($userId, $certificationId);
        $this->getUserCertificateDao()->updateUsersCertification(
            $userCert['id'], 
            array(
                'awardTime' => $awardTime,
                'isRevoked' => 0,
                'reason' => '',
                'revokedTime' => 0,
                'startLoopTime' => CourseLoopUtils::$START_LOOP_TIME,
                'endLoopTime' => 0
            )
        );
    }

    public function updateUserCertificateAwardTime($userId, $certificationId, $awardTime)
    {
        $this->getUserCertificateDao()->updateUserCertificateAwardTime($userId, $certificationId, $awardTime);
    }

    public function reAwardUserCertification($userId, $certificationId, $awardTime, $dId = null)
    {
        $userCertification = $this->getUserCertificateDao()->findCertificationIdAndUserId($userId, $certificationId);
        if (!$userCertification['isRevoked']) {
            throw new CertificationNotRevokedException();
        }

        $startLoopTime = $userCertification['endLoopTime'] + 1;

        $updatedFields = array(
            'dId' => $dId,
            'startLoopTime' => $startLoopTime,
            'isRevoked' => false,
            'revokedTime' => 0,
            'reason' => '',
            'endLoopTime' => 0,
            'awardTime' => $awardTime
        );

        $this->getUserCertificateDao()->updateUsersCertification(
            $userCertification['id'], $updatedFields
        );
    }

    public function revokeUserCertification($userCertificationId, $reason = '', $revokedTime = null)
    {
        $dbUserCertification = $this->getUserCertificateDao()->getUserCertification($userCertificationId);
        $maxLoopTimeInfo = $this->getUserCertCourseDao()->getMaxLoopTime(
                $dbUserCertification['userId'], $dbUserCertification['certificationId']);
        $maxLoopTime = $maxLoopTimeInfo['maxLoopTime'];

        $userCertification = $this->getUserCertificateDao()->updateUsersCertification($userCertificationId, 
            array(
                'reason' => $reason,
                'isRevoked' => DbValue::TRUE,
                'revokedTime' => isset($revokedTime) ? $revokedTime : time(),
                'endLoopTime' => $maxLoopTime
            )
        );

        $revokedUserCert = ArrayToolkit::createAndCopyToArray($userCertification, 
                array('userId', 'certificationId', 'awardTime', 'dID', 'reason', 'revokedTime', 'startLoopTime'));
        $revokedUserCert['userCertificationId'] = $userCertification['id'];     
        $revokedUserCert['endLoopTime'] =  $maxLoopTime;

        $this->getRevokedUserCertDao()->addRevokedUserCert($revokedUserCert);
        $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
            array(
                'userId' => $userCertification['userId'],
                'certificationId' => $userCertification['certificationId']
            ),
            array('isRevoked' => DbValue::TRUE)
        );
    }

    public function freezeUserCertification($userCertificationId, $fields)
    {
        $oldUserCertification = $this->getUserCertificateDao()->getUserCertification($userCertificationId);

        if ($oldUserCertification['isFrozen']) {
            throw new \Exception("userCertification #{$oldUserCertification['id']} is frozen");
        }

        $timestamp = time();

        $updatedFields = array(
            'isFrozen' => DbValue::TRUE,
            'frozenReason' => isset($fields['reason']) ? $fields['reason'] : '',
            'frozenTime' => $timestamp,
        );

        $userCertification = $this->getUserCertificateDao()->updateUsersCertification($userCertificationId, $updatedFields);

        $msg = "冻结用户#{$userCertification['userId']} 课程证书#{$userCertification['certificationId']} 操作时间为".date("Y-m-d H:i:s", $timestamp);

        $this->getLogService()->info('DataSync', 'freeze_user_certificate', $msg);
    }

    public function unfreezeUserCertification($userCertificationId, $fields)
    {
        $oldUserCertification = $this->getUserCertificateDao()->getUserCertification($userCertificationId);

        if (!$oldUserCertification['isFrozen']) {
            throw new \Exception("userCertification #{$oldUserCertification['id']} is unfrozen");
        }

        $timestamp = time();

        $updatedFields = array(
            'isFrozen' => DbValue::FALSE,
            'frozenReason' => isset($fields['reason']) ? $fields['reason'] : '',
            'frozenTime' => $timestamp,
        );

        $userCertification = $this->getUserCertificateDao()->updateUsersCertification($userCertificationId, $updatedFields);

        $msg = "解冻用户#{$userCertification['userId']} 课程证书#{$userCertification['certificationId']} 操作时间为".date("Y-m-d H:i:s", $timestamp);

        $this->getLogService()->info('DataSync', 'unfreeze_user_certificate', $msg);
    }

    //冻结用户课程
    public function freezeUserCertCourse($userCertCourseId, $fields)
    {
        $oldUserCertCourse = $this->getUserCertCourseDao()->getUserCertCourseById($userCertCourseId);

        if ($oldUserCertCourse['isFrozen']) {
            throw new \Exception("userCertCourse #{$oldUserCertCourse['id']} is frozen");
        }

        $timestamp = time();

        $updatedFields = array(
            'isFrozen' => DbValue::TRUE,
            'frozenReason' => isset($fields['reason']) ? $fields['reason'] : '',
            'frozenTime' => $timestamp,
        );

        $userCertCourse = $this->getUserCertCourseDao()->updateUserCertCourse($userCertCourseId, $updatedFields);

        $msg = "冻结用户#{$userCertCourse['userId']} 课程#{$userCertCourse['courseId']} 操作时间为".date("Y-m-d H:i:s", $timestamp);

        $this->getLogService()->info('DataSync', 'freeze_user_course', $msg);   
    }

    //解冻用户课程
    public function unfreezeUserCertCourse($userCertCourseId, $fields)
    {
        $oldUserCertCourse = $this->getUserCertCourseDao()->getUserCertCourseById($userCertCourseId);

        if (!$oldUserCertCourse['isFrozen']) {
            throw new \Exception("userCertCourse #{$oldUserCertCourse['id']} is unfrozen");
        }

        $timestamp = time();

        $updatedFields = array(
            'isFrozen' => DbValue::FALSE,
            'frozenReason' => isset($fields['reason']) ? $fields['reason'] : '',
            'frozenTime' => $timestamp,
        );

        $userCertCourse = $this->getUserCertCourseDao()->updateUserCertCourse($userCertCourseId, $updatedFields);

        $msg = "冻结用户#{$userCertCourse['userId']} 课程#{$userCertCourse['courseId']} 操作时间为".date("Y-m-d H:i:s", $timestamp);

        $this->getLogService()->info('DataSync', 'unfreeze_user_course', $msg);  
    }

    public function deleteRevokedUserCerts($userId, $certificationId)
    {
        $this->getRevokedUserCertDao()->deleteRevokedUserCerts($userId, $certificationId);
    }

    public function deleteUserAllCertificates($userId)
    {
        $this->getUserCertificateDao()->deleteUserAllCertificates($userId);
    }

    public function deleteUserCertificateById($id)
    {
        $this->getUserCertificateDao()->deleteUserCertificateById($id);
    }

    public function sendAddNewCertificationSms($userId, $idcard, $mobileNum, $nickName, $date, $certificateName)
    {
        $contentParam = array(
            'nickname' => $nickName,
            'date' => $date,
            'title' => $certificateName
        );

        $params = array(
            'mobile'     => $mobileNum,
            'category'   => SmsService::SYNC_NEW_CERTIFICATION,
            'parameters' => $contentParam
        );

        $this->getSmsService()->sendMsgBySms($userId, $idcard, $params);
    }

    public function getCertification($id)
    {
        return $this->getCertificationDao()->getCertification($id);
    }

    public function addCertification($certification)
    {
        return $this->getCertificationDao()->addCertification($certification);
    }

    public function deleteCertification($id)
    {
        return $this->getCertificationDao()->deleteCertification($id);
    }

    public function getAllCertifications()
    {
        return $this->getCertificationDao()->getCertificates();
    }

    public function getCertifications($ids)
    {
        return $this->getCertificationDao()->getCertifications($ids);
    }

    public function getCertificationsForUser($userId, $cascadeCertification = false,
        $courseId = null, $currentLoopTime = null) {
        $certCourses = $this->getUserCertCourseDao()->getAvailableUserCertCoursesByUserId($userId);

        $userCertifications = $this->findFrozenUserCertification($userId);
        $frozenCertificationIds = empty($userCertifications) ? array() : ArrayToolkit::column($userCertifications, 'certificationId');

        $courseIds = ArrayToolkit::unique(ArrayToolkit::column($certCourses, 'courseId'));
        $indexCourses = $this->getCourseService()->findCoursesByIds($courseIds);

        $courseMembers = $this->getCourseMemberDao()->findCourseMembersForStudentId($userId);
        $indexCourseMembers = ArrayToolkit::mulPropertiesIndex($courseMembers, array('courseId', 'currentLoopTime'));

        $copiedFields = array(
            'id', 'studentNum', 'price', 'ratingNum', 'teacherIds', 'versionStatus', 'discountId', 'id', 'title', 'hasTestpaper', 'totalHours', 'smallPicture', 'middlePicture', 'largePicture'
        );

        $result = array();
        foreach($certCourses as $single) {
            if ($this->isCourseDisplayed($single, $courseId, $currentLoopTime) 
                    && isset($indexCourses[$single['courseId']])) {
                if ($this->isCertCourseFrozen($single, $frozenCertificationIds)) {
                    $single['frozenCourse'] = DbValue::TRUE;
                } else {
                    $single['frozenCourse'] = DbValue::FALSE;
                }
                $courseInfo = $indexCourses[$single['courseId']];
                if ($courseInfo['status'] == 'published') {
                    $certCourse = ArrayToolkit::copyToAnotherArray($courseInfo, $single, $copiedFields);
                    $certCourse['courseName'] = $courseInfo['title'];
                    $certCourse = ImageUrlUtils::addPictureUrlToCourse($certCourse);
                    $certCourse = $this->generateCertCourseInfo($certCourse, $indexCourseMembers);
                    array_push($result, $certCourse);
                }
            }
        }

        if (!empty($result) && isset($courseId) && isset($currentLoopTime) && !empty($certCourses)) {
            return $result[0];
        }

        return $result;
    }

    /**
     * @see CertificateService::updateNotBuyUserCertCourse
     */
    public function updateNotBuyUserCertCourse($courseInfo)
    {
        if ($courseInfo['versionStatus'] == 'old') {
            return;
        }

        $queryParam = array(
            'areaCode' => $courseInfo['areaCode'],
            'certificationId' => $courseInfo['certificationId'],
            'learnModeMonths' => $courseInfo['learnModeMonths'],
            'partIndex' => $courseInfo['partIndex']
        );
        $userCertCoursesCount = $this->getUserCertCourseDao()->searchUserCertCoursesCount($queryParam);
        $pageSize = 500;
        $maxPageSize = intval(($userCertCoursesCount - 1) / $pageSize) + 1; 
        for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
            $startIndex = ($currentPage-1) * $pageSize;
            $this->getUserCertCourseDao()->getConnection()->transactional(
                function() use ($courseInfo, $startIndex, $pageSize, $queryParam) {
                    $userCertCourses = $this->getUserCertCourseDao()->searchUserCertCourses($queryParam, $startIndex, $pageSize);
                    foreach ($userCertCourses as $userCertCourse) {
                        if ($userCertCourse['status'] == CertCourseStatus::UNPAID 
                            ||  $this->isOvertimeNotBuy($userCertCourse)
                            ||  ($userCertCourse['courseId'] == DbValue::FALSE && $userCertCourse['isCompletedOffline'])) {
                            $this->getUserCertCourseDao()->updateUserCertCourse($userCertCourse['id'], 
                                array('courseId' => $courseInfo['id'])
                            );
                        }
                    }
                }
            );
        }
    }

    /**
     * @see CertificateService::regenerateNeverPaidLoopUserCertCourses
     */
    public function regenerateNeverPaidLoopUserCertCourses($areaCertification)
    {
        $regeneratedUserCerts = $this->findRegenerateUserCerts($areaCertification);   //格式见方法说明

        $connection = $this->getUserCertCourseDao()->getConnection();
        $connection->beginTransaction();
        try {
            $regeneratedUserIds = array();
            foreach ($regeneratedUserCerts as $userCert) {
                if (!$userCert['keepStatus']) {
                    $this->getUserCertCourseDao()->deleteUserCertCourse(
                        $userCert['userId'], $userCert['certificationId'], $userCert['currentLoopTime']);
                    $regeneratedUserIds[$userCert['userId']] = $userCert['userId'];
                }
            }

            $pageSize = 200;

            //1 条数据 1页, 200条数据 1页 
            $maxPageSize = intval((count($regeneratedUserIds) - 1) / $pageSize) + 1;
            for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
                $startIndex = ($currentPage-1) * $pageSize;
                $userIds =  array_slice($regeneratedUserIds, $startIndex, $pageSize);
                $subJob = array(
                    'name'       => "reGenerateUserCertCourseAsLearnModeOrYearUpdated(page_{$currentPage})",
                    'cycle'      => 'once',
                    'jobClass'   => 'Custom\\Service\\Certificate\\Job\\ReGenerateUserCertCourseJob',
                    'jobParams'  => $userIds,
                    'time' => time()
                );

                $this->getCrobtabService()->createJob($subJob);
            }
            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollback();
            throw $e;
        }
    }

    /**
     * @see CertificateService::markOvertimeUserCertCourses
     */
    public function markOvertimeUserCertCourses()
    {
        $userCertCoursesCount = $this->getUserCertCourseDao()->searchUserCertCoursesCount(array());
        $pageSize = 500;
        $maxPageSize = intval(($userCertCoursesCount - 1) / $pageSize) + 1; 
        for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
            $startIndex = ($currentPage-1) * $pageSize;
            $this->getUserCertCourseDao()->getConnection()->transactional(
                function() use ($startIndex, $pageSize) {
                    $userCertCourses = $this->getUserCertCourseDao()->searchUserCertCourses(array(), $startIndex, $pageSize);

                    foreach ($userCertCourses as $userCertCourse) {
                        if ($userCertCourse['deadlineNum'] < time() && 
                                ($userCertCourse['status'] == CertCourseStatus::UNPAID || $this->isLearningAndOvertime($userCertCourse))) {
                            $this->getUserCertCourseDao()->updateUserCertCourse($userCertCourse['id'], 
                                array('status' => CertCourseStatus::OVERTIME)
                            );
                        }
                    }
                }
            );
        }
    }

    public function getCertificationsForArea($areaCode)
    {
        $latestCertificationCourses = $this->getCourseDao()->getAreaLatestCertificationCourses($areaCode);
        $certificationIds = array_unique(ArrayToolkit::column($latestCertificationCourses, 'certificationId'));
        
        $certifications             = $this->getCertificationDao()->getCertifications($certificationIds);
        $indexCertifications = ArrayToolkit::index($certifications, 'id');

        $areaCertifications = $this->getAreaCertificationDao()->findByAreaCode($areaCode);
        $indexAreaCertifications = ArrayToolkit::index($areaCertifications, 'certificationId');

        $result = array();
        foreach ($latestCertificationCourses as $course) {
            $certficationId = $course['certificationId'];
            if ($course['learnModeMonths'] == $indexAreaCertifications[$certficationId]['learnModeMonths']) {
                $course['certificationName'] = $indexCertifications[$certficationId]['category'];
                $course = LearnMode::formatSingleCertificationType($course);
                $course = ArrayToolkit::fillDefaultIntValues($course, array('learnPercent', 'isCompletedOffline'), false);
                array_push($result, $course);
            } 
        }
        return $result;
    }

    public function getAreaCertificationsHaveCourse($areaCode)
    {
        $publishedCourses = $this->getCourseDao()->findAreaPublishedCertCourses($areaCode);
        if (isset($publishedCourses)) {
            $certIds = ArrayToolkit::column($publishedCourses, 'certificationId');
            $certs = $this->getCertificationDao()->getCertifications($certIds);
            return $certs;
        }
        return array();
    }

    public function isCertificationCourseBuyable($userId, $courseId, $currentLoopTime)
    {
        $certifcationCourse = $this->getCertificationsForUser($userId, false, $courseId, $currentLoopTime);
        return isset($certifcationCourse['buyable']) && $certifcationCourse['buyable'] === true;
    }

    /**
     * 行政区域证书
     */
    public function getAreaCertification($id)
    {
        return $this->getAreaCertificationDao()->getAreaCertification($id);
    }

    public function deleteAreaCertfication($areaCode, $certificationId)
    {
        return $this->getAreaCertificationDao()->deleteAreaCertification($areaCode, $certificationId);
    }

    public function getAreaCertifications($code)
    {
        return $this->getAreaCertificationDao()->findByAreaCode($code);
    }

    public function getAllCertificationsWithLessonStatus($userId)
    {
        return $this->getCertificationsForUser($userId);
    }

    public function getLearnableCertificationsWithLessonStatus($userId)
    {
        $certificationCourses = $this->getAllCertificationsWithLessonStatus($userId);

        //在分组的情况下, 再按学习开始时间倒序排序
        $unpaidCourses = array();
        $learningCourses = array();
        $completionCourses = array();
        $overtimeCourses = array();

        foreach ($certificationCourses as $course) {
            switch ($course['learningStatus']) {
                case LearnStatus::UN_PAID: 
                    $unpaidCourses[] = $course; 
                    break;

                case LearnStatus::LEARNING: 
                case LearnStatus::FINISHED:
                case LearnStatus::MAKEUP:
                    $learningCourses[] = $course;
                    break;
                
                case LearnStatus::COMPLETION:
                    $completionCourses[] = $course;
                    break;
                 
                case LearnStatus::OVERTIME_MAKEUP_YES:
                case LearnStatus::OVERTIME_MAKEUP_NO:
                    $overtimeCourses[] = $course;
                    break;
            }
        }

        $sortUnpaidCourses = $this->sortCoursesByStartTime($unpaidCourses);
        $sortLearningCourses = $this->sortCoursesByStartTime($learningCourses);
        $sortCompletionCourses = $this->sortCoursesByStartTime($completionCourses);
        $sortOvertimeCourses = $this->sortCoursesByStartTime($overtimeCourses);

        return array_merge(
            $sortUnpaidCourses,
            $sortLearningCourses,
            $sortCompletionCourses,
            $sortOvertimeCourses
        );
    }

    protected function sortCoursesByStartTime($courses)
    {
        if (empty($courses)) {
            return  array();
        }
        
        return ArrayToolkit::sortTwoDimensionArray($courses, 'startTimeNum', false);
    }

    public function createAreaCertification($areaCertification)
    {
        $areaCertification = $this->getAreaCertificationDao()->addAreaCertification($areaCertification);
    }

    public function updateAreaCertification($id, $fields)
    {
        $existedAreaCertification = $this->getAreaCertificationDao()->getAreaCertification($id);
        
        $fields['maxLearningSeconds'] = $fields['maxLearningMinutes'] * 60;
        $result = $this->getAreaCertificationDao()->updateAreaCertification($id, $fields);

        if ($existedAreaCertification['relearn'] != $result['relearn']) {
            $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
                array('areaCode' => $result['areaCode'], 'certificationId' => $result['certificationId']),
                array('relearn' => $result['relearn'])
            );
            if ($result['relearn']) {
                $userCertCourses = $this->getUserCertCourseDao()->updateRecordsWhenSwitchToRelearn(
                    $result['areaCode'], $result['certificationId']);
            } else {
                $userCertCourses = $this->getUserCertCourseDao()->updateRecordsWhenSwitchToUnrelearn(
                    $result['areaCode'], $result['certificationId']);
            }
        }

        if ($existedAreaCertification['months'] != $result['months'] || $existedAreaCertification['learnModeMonths'] != $result['learnModeMonths'] ) {
            $this->regenerateNeverPaidLoopUserCertCourses($existedAreaCertification);
        }

        return $result;
    }

    public function findAreaCertificationsByAreaCode($areaCode)
    {
        return $this->getAreaCertificationDao()->findByAreaCode($areaCode);
    }

    /**
     * @see CertificateService::findIndexCertCategoryByAreaCode
     */
    public function findIndexCertCategoryByAreaCode($areaCode)
    {
        $result = array();
        $areaCerts = $this->getAreaCertificationDao()->findByAreaCode($areaCode);
        $certIds = ArrayToolkit::column($areaCerts, 'certificationId');
        $certifications = $this->getCertificationDao()->getCertifications($certIds);

        foreach ($certifications as $cert) {
            $result[$cert['id']] = $cert['category'];
        }
        return $result;
    }

    public function findAreaCertificationsByCertificationIds($certificationIds)
    {
        return $this->getAreaCertificationDao()->findByCertificationIds($certificationIds);
    }

    public function findAreaCertificationsByCertificationId($certificationId)
    {
        return $this->findByCertificationId($certificationId);
    }

    public function findByAreaCodeAndCertificationId($areaCode, $certificationId)
    {
        return $this->getAreaCertificationDao()->findByAreaCodeAndCertificationId($areaCode, $certificationId);
    }

    public function searchAreaCertifications($conditions, $orderBys, $start, $limit)
    {
        return $this->getAreaCertificationDao()->searchAreaCertifications($conditions, $orderBys, $start, $limit);
    }

    public function searchAreaCertificationsCount($conditions)
    {
        return $this->getAreaCertificationDao()->searchAreaCertificationsCount($conditions);
    }

    public function getUserCompletionCertificateCourses($userId, $certificationId, $currentLoopTime = null)
    {
        $allCourses                   = $this->getCertificationsForUser($userId);
        $learnedCourses               = $this->getLearnedCertificationCourses($allCourses, $certificationId);
        $completionCertificateCourses = $this->getCompleteCoursesFromLearnedCertificationCourses($userId, $learnedCourses);
        return empty($completionCertificateCourses) ? array() : $completionCertificateCourses;
    }

    /**
     * @see CertificateService::generateUserCertCourses()
     */
    public function generateUserCertCourses($userId)
    {
        $user = $this->getUserDao()->getUser($userId);
        $latestCertificationCourses = $this->getCourseDao()->getAreaLatestCertificationCoursesIgnorePublished($user['areaCode']);
        $certifications             = $this->getCertificationDao()->getCertificationsForUser($userId);
        $generatedUserCertCourses  = $this->generateCertificationCourses($latestCertificationCourses, $certifications, $userId);
        $filteredUserCertCourses = $this->filterFeatureNonFirstLoopTimes($generatedUserCertCourses);
        $this->addAllUserCertCourses($filteredUserCertCourses, $user);
    }

    /**
     * @see CertificateService::generateAllUsersCertCourses()
     */
    public function generateAllUsersCertCourses()
    {
        $userCount = $this->getUserDao()->searchUserCount(array());

        $pageSize = 500;

        //1 条数据 1页, 500条数据 1页
        $maxPageSize = intval(($userCount - 1) / $pageSize) + 1; 

        for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
            $startIndex = ($currentPage-1) * $pageSize;
            $this->transactionalReturn(
                function() use ($startIndex, $pageSize) {
                    $allUsers = $this->getUserDao()->searchUsers(array(), array('id', ' '), $startIndex, $pageSize);
                    foreach ($allUsers as $user) {
                        $this->generateUserCertCourses($user['id']);
                    }
                }
            );
        }
    }

    public function addUserCertCourse($userCertCourse)
    {
        $firstCertCourse =$this->getUserCertCourseDao()->getFirstUserCertCourse($userCertCourse['userId'], 
                $userCertCourse['certificationId'],  $userCertCourse['currentLoopTime']);

        if ($firstCertCourse['isRevoked'] == DbValue::TRUE) {
            return;
        }

        if ($this->isCertRevoked(
                $userCertCourse['userId'], 
                $userCertCourse['certificationId'],  
                $userCertCourse['currentLoopTime'])) {
            
            return;
        }

        //该学员未生成过证书课程时, 创建一个当前学制的课程
        if (empty($firstCertCourse)) {
            if ($userCertCourse['learnModeMonths'] == $userCertCourse['currentCertLearnMode']) {
                unset($userCertCourse['currentCertLearnMode']);
                $this->getUserCertCourseDao()->addUserCertCourse($userCertCourse);
            }
        } else if ($firstCertCourse['months'] == $userCertCourse['months'] 
                && $firstCertCourse['learnModeMonths'] == $userCertCourse['learnModeMonths']) {
            //该学员已经有证书课程, 必须周期和学制都一样的才能创建
            $exitedUserCertCourse = $this->getUserCertCourseDao()->getUserCertCoursePerCert($userCertCourse['userId'], 
                    $userCertCourse['certificationId'], $userCertCourse['currentLoopTime'], $userCertCourse['learnModeMonths'], $userCertCourse['partIndex']);
            if (empty($exitedUserCertCourse)) {
                unset($userCertCourse['currentCertLearnMode']);
                $this->getUserCertCourseDao()->addUserCertCourse($userCertCourse);
            }
        }
    }

    public function updateUserCertCourse($id, $userCertCourse)
    {
        $this->getUserCertCourseDao()->updateUserCertCourse($id, $certCourse);
    }

    public function getUserCertCourse($userId, $courseId, $currentLoopTime)
    {
        return $this->getUserCertCourseDao()->getUserCertCourse($userId, $courseId, $currentLoopTime);
    }

    public function getCertificateCourseByBuyTime($userId, $courseId, $startPeriod, $endPeriod, $buyable=true)
    {
        $allCourses = $this->getCertificationsForUser($userId,false,$courseId);
       

        $startPeriodNum = strtotime($startPeriod);
        $endPeriodNum = strtotime($endPeriod);
        foreach($allCourses as $course) {           
            if ($buyable && !$course['buyable']) {  
                continue;
            }

            //一天的秒数 -1, 存到数据库中的为前一天 23:59:59, 导入时, 指定为 00:00:00, 需要减掉
            if ($startPeriodNum == $course['certificationStartPeriod'] 
                    && $endPeriodNum == $course['certificationEndPeriod'] - 86399) {
                return $course;
            }
        }

        return false;
    }

    private function getLearnedCertificationCourses($allCourses, $certificationId)
    {
        $coursesArray = array();

        foreach ($allCourses as $allCourse) {
            if ($allCourse['certificationId'] == $certificationId) {
                $coursesArray[$allCourse['currentLoopTime']][] = $allCourse;
            }
        }

        foreach ($coursesArray as $currentLoopTime => $courses) {
            $learnedCount = count(array_unique(ArrayToolkit::column($courses, 'partIndex')));
            if ($courses[0]['certificationCourseCount'] != $learnedCount) {
                unset($coursesArray[$currentLoopTime]);
            }
        }

        return $coursesArray;
    }

    private function getCompleteCoursesFromLearnedCertificationCourses($userId, $learnedCourses)
    {
        $completionCourses            = array();
        $completionCertificateCourses = array();

        foreach ($learnedCourses as $courses) {
            foreach ($courses as $key => $course) {
                if ($this->getCourseService()->isCourseCompletion($userId, $course['courseId'], $course['currentLoopTime'])) {
                    $completionCourses[$key]['courseId']        = $course['courseId'];
                    $completionCourses[$key]['courseTitle']     = $course['courseName'];
                    $completionCourses[$key]['certificationId'] = $course['certificationId'];
                    $completionCourses[$key]['areaCode']        = $course['areaCode'];
                    $completionCourses[$key]['learnStartTime']  = strtotime($course['startTime']);
                    $completionCourses[$key]['learnEndTime']    = strtotime($course['deadline']);
                    $completionCourses[$key]['currentLoopTime'] = $course['currentLoopTime'];
                    unset($courses[$key]);
                }
            }

            if (empty($courses)) {
                $completionCertificateCourses = array_merge($completionCertificateCourses, $completionCourses);
            }
        }

        return $completionCertificateCourses;
    }

    /**
     * 默认只生成第一次循环, 如果进入第N次循环, 才额外生成2~N次的循环课程数据 (N>=2)
     */
    private function filterFeatureNonFirstLoopTimes($userCertCourses)
    {
        $result = array();

        $addedFeatureLoopTimes = array();
        $now = time();

        foreach ($userCertCourses as $course) {
            if ($course['currentLoopTime'] == $course['startLoopTime']) {
                array_push($result, $course);
            } elseif ($course['startTimeNum'] < $now) {
                array_push($addedFeatureLoopTimes, $course['currentLoopTime']);
            }
        }

        $distinctLoopTimes = array_unique($addedFeatureLoopTimes);

        foreach ($userCertCourses as $course) {
            if (in_array($course['currentLoopTime'], $distinctLoopTimes)) {
                array_push($result, $course);
            }
        }

        return $result;
    }

    private function addAllUserCertCourses($userCertCourses, $user)
    {
        foreach ($userCertCourses as $course) {
            $userCertCourse = ArrayToolkit::createAndCopyToArray($course, 
                array('courseId', 'certificationId', 'startTimeNum', 'deadlineNum',
                    'months', 'learnModeMonths' ,'awardTime', 'status', 'partIndex',
                    'currentLoopTime', 'relearn', 'certificationCourseCount',
                    'certificationType', 'certificationStartPeriod', 'certificationEndPeriod',
                    'certificationName', 'currentCertLearnMode'
                ),
                //防止复制时, 将0置空
                array(
                    'partIndex' => 0,
                    'isRevoked' => 0, 
                    'isCompletedOffline' => 0,
                    'certificationCourseCount' => 0,
                    'relearn' => 0,
                    'courseId' => 0
                )
            );

            $userCertCourse['userId'] = $user['id'];
            $userCertCourse['areaCode'] = $user['areaCode'];
            $this->addUserCertCourse($userCertCourse);
        }
    }

    /*
     * 如果某个证书部分的课程不存在, 则生成courseId = 0的记录
     */
    private function generateCertificationCourses($latestCertificationCourses, $certifications, $userId) {
        if (empty($certifications)) {
            return array();
        }

        $allCourses = array();

        $indexCourses = ArrayToolkit::mulPropertiesIndex($latestCertificationCourses, 
            array('certificationId', 'learnModeMonths', 'partIndex'));

        //key 为 {$certificationId}:{$learnModeMonths}, value为课程数
        $certLearnModeCourseCount = CertificateUtils::generateCertLearnModeCourseCount(
            $latestCertificationCourses
        );

        $certLearnModes = CertificateUtils::generateCertLearnModes($latestCertificationCourses, $certifications);
        foreach ($certifications as $key => $certification) {
            $certification['currentCertLearnMode'] = $certification['learnModeMonths'];
            $certId = $certification['certificationId'];
            if (isset($certLearnModes[$certId])) {
                foreach ($certLearnModes[$certId] as $learnModeMonths => $value) {
                    $certification['currentLoopMode'] = LearnMode::generateLearnMode(
                        $learnModeMonths, 
                        $certification['months']
                    );
                    $certification['loopMode:' . $learnModeMonths] = $certification['currentLoopMode'];
                    $certification['learnModeMonths'] = $learnModeMonths;
                    $latestCertificationCourses = $this->generateMissedCourses(
                        $certification, 
                        $latestCertificationCourses, 
                        $indexCourses, 
                        $certLearnModeCourseCount
                    );
                }
            }
            unset($certification['learnModeMonths'], $certification['currentLoopMode']);
            $certifications[$key] = $certification;
        }
        $indexCertifications = ArrayToolkit::index($certifications, 'certificationId');
        foreach ($latestCertificationCourses as $certCourse) {
            if (isset($indexCertifications[$certCourse['certificationId']])) {
                $certification = $indexCertifications[$certCourse['certificationId']];
                $allLatestCourses = $this->generateCourseInfos($certCourse, $certification, $userId);
                foreach ($allLatestCourses as $singleCourse) {
                    array_push($allCourses, $singleCourse);
                }
            }
        }
        return $allCourses;
    }

    private function generateMissedCourses($certification, $latestCertificationCourses, $indexCourses, $certLearnModeCourseCount)
    {
        $certLearnModeKey = $certification['certificationId'] . ':' . $certification['learnModeMonths'];
        $currentLoopMode = $certification['currentLoopMode'];
        if (empty($certLearnModeCourseCount[$certLearnModeKey]) ||
                $certLearnModeCourseCount[$certLearnModeKey] < $currentLoopMode['courseCount']) {
            for ($i = 1; $i <= $currentLoopMode['courseCount']; $i++) {
                $courseIndexKey = $certLearnModeKey . ':'. $i;

                if (!isset($indexCourses[$courseIndexKey])) {
                    $course = array(
                        'courseId' => 0,
                        'certificationId' => $certification['certificationId'],
                        'partIndex' => $i,
                        'learnModeMonths' => $certification['learnModeMonths']
                    );
                    array_push($latestCertificationCourses, $course);
                }
            }
        }

        return $latestCertificationCourses;
    }

    private function generateCourseInfos($courseInfo, $certification, $userId)
    {
        $allLoopModes = array('latestMode' => $certification['loopMode:' . $courseInfo['learnModeMonths']]);
        $totalCourseInfos = array();
        $existedCourses = $this->getUserCertCourseDao()->getUserCertCoursePerCertId($userId, $courseInfo['certificationId']);
        $startTimeInfoArray = CourseLoopUtils::caculateStartTimeNumArray($courseInfo, $certification, $allLoopModes, $existedCourses);
        $courseInfo['awardTime'] = $certification['awardTime'];
        $courseInfo['relearn'] = $certification['relearn'];
        $courseInfo['certificationName'] = $certification['category'];
        $courseInfo['months'] = $certification['months'];
        $courseInfo['startLoopTime'] = $certification['startLoopTime'];
        $courseInfo['currentCertLearnMode'] = $certification['currentCertLearnMode'];

        if (isset($startTimeInfoArray)) {
            foreach ($startTimeInfoArray as $startTimeInfo) {
                $clonedCouseInfo = $this->generateCourseExtInfos($certification, $courseInfo, $startTimeInfo);
                if ($clonedCouseInfo['partIndex'] <= $clonedCouseInfo['certificationCourseCount']) {
                    array_push($totalCourseInfos, $clonedCouseInfo);
                }
            }
        }  
        return $totalCourseInfos;
    }

    /*
     * 设置证书课程的状态
     * 已过期的证书课程, 同时计算其 补学截止日期 及 剩余补学天数
     */
    private function generateCourseStatus($courseInfo)
    {
        if ($courseInfo['deadlineNum'] <= time()) {
            $courseInfo['status'] = CertCourseStatus::OVERTIME;
        } else {
            $courseInfo['status'] = CertCourseStatus::UNPAID;
        }
        return $courseInfo;
    }

    private function generateCourseExtInfos($certification, $courseInfo, $startTimeInfo)
    {
        $currentLoopMode                         = $startTimeInfo['currentLoopMode'];
        $endTimeNum                              = strtotime($currentLoopMode['addedDays'], $startTimeInfo['startTimeNum']) - 1;
        $endTimeInfo                             = DateUtils::getYesterdayEndDateTime($endTimeNum);
        $courseInfo['currentLoopTime']           = $startTimeInfo['currentLoopTime'];
        $courseInfo['startTimeNum']              = $startTimeInfo['startTimeNum'];
        $courseInfo['deadlineNum']         = $endTimeNum;
        $courseInfo['certificationType']         = $currentLoopMode['i18nMsg'];
        $courseInfo                              = $this->generateCourseStatus($courseInfo);
        $courseInfo['certificationCourseCount']  = $currentLoopMode['courseCount'];

        $endPeriodAddedTime =  $startTimeInfo['currentLoopMode']['months'];
        $courseInfo['certificationStartPeriod'] = $startTimeInfo['certificationStartTimeNum'];
        $courseInfo['certificationEndPeriod'] = strtotime("+{$endPeriodAddedTime} months", $startTimeInfo['certificationStartTimeNum']) - 1; //前一天的23:59:59
        return $courseInfo;
    }

    public function getProfile($userId)
    {
        return $this->getUserProfileDao()->getProfile($userId);
    }

    public function addUserCertification($fields)
    {
        if (!isset($fields['userId']) || empty($fields['userId'])) {
            return false;
        }

        if (!isset($fields['certificationId']) || empty($fields['certificationId'])) {
            return false;
        }

        if (!isset($fields['awardTime']) || empty($fields['awardTime'])) {
            return false;
        }
        return $this->getUserCertificateDao()->addUserCertification($fields);
    }

    protected function getUserCertificationStatesService()
    {
        return $this->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getTestpaperService()
    {
        return $this->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getUserCertificateDao()
    {
        return $this->createDao('Custom:Certificate.UserCertificateDao');
    }

    protected function getRevokedUserCertDao()
    {
        return $this->createDao('Custom:Certificate.RevokedUserCertDao');
    }

    protected function getCertificationDao()
    {
        return $this->createDao('Custom:Certificate.CertificationDao');
    }

    protected function getCourseMemberDao()
    {
        return $this->createDao('Custom:Course.CourseMemberDao');
    }

    protected function getCourseDao()
    {
        return $this->createDao('Custom:Course.CourseDao');
    }

    protected function getUserDao()
    {
        return $this->createDao('Custom:User.UserDao');
    }

    protected function getOrderService()
    {
        return $this->createService('Order.OrderService');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getAreaCertificationDao()
    {
        return $this->createDao('Custom:Certificate.AreaCertificationDao');
    }

    protected function getUserProfileDao()
    {
        return $this->createDao('User.UserProfileDao');
    }

    protected function getLessonDao()
    {
        return $this->createDao('Course.LessonDao');
    }

    protected function getSmsService()
    {
        return $this->createService('Custom:Sms.SmsService');
    }   

    protected function getCrobtabService()
    {
        return $this->createService('Topxia:Crontab.CrontabService');
    }

    protected function getUserCertCourseDao()
    {
        return $this->createDao('Custom:Certificate.UserCertCourseDao');
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }

    /*
     * @param indexCourseMembers  key为 {$courseId}:{$currentLoopTime}, value 为 courseMember
     */
    private function generateCertCourseInfo($certCourse, $indexCourseMembers)
    {
        $certCourse['startTime'] = DateUtils::number2Str($certCourse['startTimeNum']);
        $certCourse['deadline'] = DateUtils::number2Str($certCourse['deadlineNum']);
        $certCourse['leftDays'] = DateUtils::diffDays(time(), $certCourse['deadlineNum']);
        $certCourse = ArrayToolkit::fillDefaultIntValues($certCourse, 
            array('isTestpaperPassed', 'isAllLessonsFinished', 'isCompleted', 'learnedHours', 'percent'), false);

        $courseMemberIndex = $certCourse['courseId'] . ":" . $certCourse['currentLoopTime'];
        if (isset($indexCourseMembers[$courseMemberIndex]) ) {
            $courseMember = $indexCourseMembers[$courseMemberIndex];
            $certCourse['buyTime'] = $courseMember['createdTime'];
            $copiedFields = array('learnedHours', 'isAllLessonsFinished', 'isTestpaperPassed', 'isCompleted');
            $certCourse = ArrayToolkit::copyToAnotherArray($courseMember, $certCourse, $copiedFields);
        }

        if ($certCourse['frozenCourse']) {
            $certCourse['learningStatus'] = LearnStatus::OVERTIME_MAKEUP_NO;
            $certCourse['status'] = CertCourseStatus::OVERTIME;
        } else if ($certCourse['status'] == CertCourseStatus::UNPAID) {
            $certCourse['learningStatus'] = LearnStatus::UN_PAID;
        } else if ($certCourse['status'] == CertCourseStatus::OVERTIME) {
            $makeupDeadlineNum = strtotime(LearnMode::$MAKE_UP_EXPIRE_DATE, $certCourse['certificationEndPeriod']);
            if ($certCourse['relearn'] == DbValue::TRUE && time() < $makeupDeadlineNum)  {
                $certCourse['learningStatus'] = LearnStatus::OVERTIME_MAKEUP_YES;
                $certCourse['makeupDeadline'] = DateUtils::number2Str($makeupDeadlineNum);
                $certCourse['makeupLeftDays'] = DateUtils::diffDays(time(), $makeupDeadlineNum);
            } else {
                $certCourse['learningStatus'] = LearnStatus::OVERTIME_MAKEUP_NO;
            }
        } else if ($certCourse['status'] == CertCourseStatus::LEARNING) {
            if (time() > $certCourse['deadlineNum']) {
                $makeupDeadlineNum = strtotime(LearnMode::$MAKE_UP_EXPIRE_DATE, $certCourse['certificationEndPeriod']);
                $certCourse['learningStatus'] = LearnStatus::MAKEUP;
                $certCourse['makeupDeadline'] = DateUtils::number2Str($makeupDeadlineNum);
                $certCourse['makeupLeftDays'] = DateUtils::diffDays(time(), $makeupDeadlineNum);
            } else {
                $certCourse['learningStatus'] = LearnStatus::LEARNING;
            }

            if (isset($courseMember) && $courseMember['isAllLessonsFinished'] == DbValue::TRUE) {
                $certCourse['learningStatus'] = LearnStatus::FINISHED; 
            }
        } else {
            $certCourse['learningStatus'] = LearnStatus::COMPLETION;
        }

        $certCourse['learnPercent'] = $this->getCourseService()->getPercent($certCourse['totalHours'], $certCourse['learnedHours']);
        $certCourse['buyable'] = $this->isCourseBuyable($certCourse);
        $certCourse['learnable'] = $this->isCourseLearnable($certCourse);

        return $certCourse;
    }

    private function isCourseBuyable($courseInfo)
    {
        return in_array($courseInfo['learningStatus'],
                    array(LearnStatus::UN_PAID, LearnStatus::OVERTIME_MAKEUP_YES));
    }

    private function isCourseLearnable($courseInfo)
    {
        return in_array($courseInfo['status'],
                    array(CertCourseStatus::LEARNING, CertCourseStatus::COMPLETION));
    }

    private function isCourseDisplayed($certCourse, $courseId, $currentLoopTime)
    {
        if ($certCourse['isRevoked'] == DbValue::TRUE && empty($certCourse['latestBuyTime']) 
                && !$certCourse['isCompletedOffline']) {
            return false;
        }

        $isDisplayed = false;
        if (isset($courseId) && isset($currentLoopTime)) {
            $isDisplayed = $certCourse['courseId'] == $courseId && $certCourse['currentLoopTime'] == $currentLoopTime;
        } else if (isset($courseId)) {
            $isDisplayed = $certCourse['courseId'] == $courseId;
        } else {
            $isDisplayed = true;
        }

        return $isDisplayed;
    }

    /*
     * @return 
     *      array(
     *          {$userId}:{$certificationId}:{$currentLoopTime} => array(keepStatus => ,  currentLoopTime =>,  certificationId=>, userId=>)
     *      )
     */
    private function findRegenerateUserCerts($areaCertification) {
        $regeneratedUserCerts = array();
        
        $queryParam = array(
            'areaCode' => $areaCertification['areaCode'], 
            'certificationId' => $areaCertification['certificationId']
        );                                             
        $userCertCoursesCount = $this->getUserCertCourseDao()->searchUserCertCoursesCount($queryParam);
        $pageSize = 500;

        //1 条数据 1页, 500条数据 1页 
        $maxPageSize = intval(($userCertCoursesCount - 1) / $pageSize) + 1;

        for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
            $startIndex = ($currentPage-1) * $pageSize;
            $regeneratedUserCerts = $this->transactionalReturn(
                function() use ($startIndex, $pageSize, $regeneratedUserCerts, $queryParam) {
                    $userCertCourses = $this->getUserCertCourseDao()->searchUserCertCourses($queryParam, $startIndex, $pageSize);
                    foreach ($userCertCourses as $userCertCourse) {
                        $key = $userCertCourse['userId'] . ':' . $userCertCourse['certificationId'] . ':' . $userCertCourse['currentLoopTime'];

                        if (isset($regeneratedUserCerts[$key])) {
                            $regeneratedUserCert = $regeneratedUserCerts[$key];
                            if ($regeneratedUserCert['keepStatus']) {
                                continue;
                            }
                        } else {
                            $regeneratedUserCert = array(
                                'currentLoopTime' => $userCertCourse['currentLoopTime'],
                                'certificationId' => $userCertCourse['certificationId'],
                                'userId' => $userCertCourse['userId'],
                                'keepStatus' => false
                            );
                            $regeneratedUserCerts[$key] = $regeneratedUserCert;
                        }

                        if (!empty($userCertCourse['latestBuyTime'])
                            || ($userCertCourse['certificationEndPeriod'] < time() && $userCertCourse['courseId'] != 0)
                            || $userCertCourse['isRevoked'] == DbValue::TRUE) {
                            $regeneratedUserCert['keepStatus'] = true;
                            $regeneratedUserCerts[$key] = $regeneratedUserCert;
                        }
                    }
                    return $regeneratedUserCerts;
                }
            );
        }
        return $regeneratedUserCerts;
    }

    private function isOvertimeNotBuy($userCertCourse)
    {
        return $userCertCourse['status'] == CertCourseStatus::OVERTIME 
                && (empty($userCertCourse['latestBuyTime']) ||$userCertCourse['latestBuyTime'] < $userCertCourse['deadlineNum']); //未在过期时间范围内买过课程
    }

    private function isLearningAndOvertime($userCertCourse)
    {
        $isLearningAndOvertime = false;

        if ($userCertCourse['status'] == CertCourseStatus::LEARNING) {
            // 在正常学习周期内购买
            $isLatestBuyInNormalCoursePeriod = !empty($userCertCourse['latestBuyTime']) && 
                    $userCertCourse['latestBuyTime'] < $userCertCourse['deadlineNum'];

            // 已过补学周期
            $isLatestBuyAfterMakeupPeriod = !empty($userCertCourse['latestBuyTime']) &&
                time() > strtotime(LearnMode::$MAKE_UP_EXPIRE_DATE, $userCertCourse['certificationEndPeriod']);

            $isLearningAndOvertime = $isLatestBuyInNormalCoursePeriod || $isLatestBuyAfterMakeupPeriod;
        }
        
        return $isLearningAndOvertime;
    }

    private function transactionalReturn(\Closure $func)
    {
        $connection = $this->getUserCertCourseDao()->getConnection();
        $connection->beginTransaction();
        try {
            $data = $func($this);
            $connection->commit();
            return $data;
        } catch (\Exception $e) {
            $connection->rollback();
            throw $e;
        }
    }

    public function getUserIdCompletionCertificateCoursesNumber($userId)
    {
        $allCourses              = $this->getAllCertificationsWithLessonStatus($userId);
        $completionCoursesNumber = 0;

        foreach ($allCourses as $course) {
            if ($this->getCourseService()->isCourseCompletion($userId, $course['courseId'], $course['currentLoopTime'])) {
                $completionCoursesNumber++;
            }
        }

        return $completionCoursesNumber;
    }

    public function getUserIdsByCertificationId($certificationId)
    {
        return $this->getUserCertificateDao()->getUserIdsByCertificationId($certificationId);
    }

    private function isCertRevoked($userId, $certId, $currentLoopTime)
    {
        $userCertInfo = $this->getUserCertificateDao()->findCertificationIdAndUserId($userId, $certId);
        if ($userCertInfo['startLoopTime'] > $currentLoopTime || 
                ($userCertInfo['endLoopTime'] < $currentLoopTime && $userCertInfo['endLoopTime'] != 0)) {
            return true;
        }

        return false;
    }

    private function isCertCourseFrozen($singleCertCourse, $frozenCertificationIds)
    {
        if ($singleCertCourse['isFrozen']) {
            return true;
        }

        if (!empty($frozenCertificationIds) && in_array($singleCertCourse['certificationId'], $frozenCertificationIds)) {
            return true;
        }

        return false;
    }
}


