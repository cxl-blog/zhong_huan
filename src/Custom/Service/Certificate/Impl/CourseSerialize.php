<?php 

namespace Custom\Service\Certificate\Impl;

class CourseSerialize
{
    public static function unserialize(array $course = null)
    {
        if (empty($course)) {
            return $course;
        }

        if (empty($course['teacherIds'])) {
            $course['teacherIds'] = array();
        } else {
            $course['teacherIds'] = explode('|', trim($course['teacherIds'], '|'));
        }

        return $course;
    }

    public static function unserializes(array $courses)
    {
        return array_map(function ($course) {
            return CourseSerialize::unserialize($course);
        }, $courses);
    }
}