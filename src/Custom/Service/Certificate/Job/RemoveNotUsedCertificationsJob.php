<?php
namespace Custom\Service\Certificate\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\DateUtils;

/**
 * 数据同步时, 可能会因为输入错误, 同步了一些不被任何行政区域使用的证书, 需要删除它
 */
class RemoveNotUsedCertificationsJob implements Job
{
    public function execute($params)
    {
        $this->getUserDao()->getConnection()->transactional(
            function($conn) {
                $allCertifications = $this->getCertificateService()->getAllCertifications();
                $areaCerts = $this->getCertificateService()->searchAreaCertifications(array(), array('id', ' '), 0, PHP_INT_MAX);
                $usedCertIds = ArrayToolkit::column($areaCerts, 'certificationId');
                
                foreach ($allCertifications as $cert) {
                    if (!in_array($cert['id'], $usedCertIds)) {
                        $this->getCertificateService()->deleteCertification($cert['id']);
                    }
                }
            }
        );
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
