<?php
namespace Custom\Service\Certificate\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 修改行政区域下 证书的周期或学制时, 所有受影响的学员, 每200人生成一个该任务后台执行
 *    见 CertificateServiceImpl::regenerateNeverPaidLoopUserCertCourses 方法
 */
class ReGenerateUserCertCourseJob implements Job
{
    public function execute($params)
    {
        $userIds = $params;
        $this->getUserDao()->getConnection()->transactional(
            function($conn) use ($userIds) {
                foreach ($userIds as $userId) {
                    $this->getCertificateService()->generateUserCertCourses($userId);
                }
            }
        );
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
