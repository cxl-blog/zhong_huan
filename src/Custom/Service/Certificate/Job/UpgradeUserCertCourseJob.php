<?php
namespace Custom\Service\Certificate\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 每天凌晨, 会根据用户的证书有效期, 选择性生成下一个循环的内容
 *    该任务会自动给所有用户创建 生成用户证书课程 的子任务, 每200个用户一个子任务
 */
class UpgradeUserCertCourseJob implements Job
{
    public function execute($params)
    {
        $this->getCertificateService()->markOvertimeUserCertCourses();
        
        $userCount = $this->getUserDao()->searchUserCount(array());

        $pageSize = 200;
        $maxPageSize = intval(($userCount - 1) / $pageSize) + 1;   //1 条数据 1页, 200条数据 1页 

        for ($currentPage = 1;  $currentPage <= $maxPageSize; $currentPage++) {
            $startIndex = ($currentPage-1) * $pageSize;
            $subJob = array(
                'name'       => "generateUsersCertCourse(page_{$currentPage})",
                'cycle'      => 'once',
                'jobClass'   => 'Custom\\Service\\Certificate\\Job\\GenerateUsersCertCourseJob',
                'jobParams'  => array('startIndex' => $startIndex, 'pageSize' => $pageSize),
                'time' => time()
            );

            $this->getCrobtabService()->createJob($subJob);
        }
        
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCrobtabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
