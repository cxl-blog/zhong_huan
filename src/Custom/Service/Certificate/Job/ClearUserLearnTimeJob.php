<?php
namespace Custom\Service\Certificate\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 每天凌晨12点, 将 已学习时间 置为0
 */
class ClearUserLearnTimeJob implements Job
{
    public function execute($params)
    {
        $this->getUserLearnTimeService()->clearUserLearnTime();
    }

    protected function getUserLearnTimeService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.UserLearnTimeService');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
