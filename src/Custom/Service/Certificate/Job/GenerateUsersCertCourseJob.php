<?php
namespace Custom\Service\Certificate\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 该任务由 UpgradeUserCertCourseJob 生成, 主要用于生成用户下一循环的内容
 */
class GenerateUsersCertCourseJob implements Job
{
    public function execute($params)
    {
        $startIndex = $params['startIndex'];
        $pageSize = $params['pageSize'];
        $this->getUserDao()->getConnection()->transactional(
            function($conn) use ($startIndex, $pageSize) {
                $allUsers = $this->getUserDao()->searchUsers(array(), array('id', ' '), $startIndex, $pageSize);
                foreach ($allUsers as $user) {
                    $this->getCertificateService()->generateUserCertCourses($user['id']);
                }
            }
        );
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
