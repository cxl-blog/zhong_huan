<?php

namespace Custom\Serivce\Certificate\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class CertificateServiceTest extends BaseTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->prepareData();
    }

    public function testFindCertificationIdAndUserId()
    {
        $result = $this->getCertificateService()->findCertificationIdAndUserId(2, 3);
        $this->assertEquals('1460789930', $result['awardTime']);
    }

    public function testFindCertificationBydID()
    {
        $result = $this->getCertificateService()->findCertificationBydID(112231);
        $this->assertEquals('1460789930', $result['awardTime']);
    }

    public function testFindCertificatesByUserId()
    {
        $userCerts = $this->getCertificateService()->findCertificatesByUserId(2);
        $this->assertEquals(2, count($userCerts));
        $this->assertEquals('1460689931', $userCerts[0]['awardTime']);
    }

    public function testFindCertificatesIncludingRevokedByUserId()
    {
        $result = $this->getCertificateService()->findCertificatesIncludingRevokedByUserId(3);
        $this->assertEquals(2, count($result));
        $this->assertEquals(4, $result[0]['startLoopTime']);
        $this->assertEquals(1, $result[1]['startLoopTime']);
        $this->assertEquals(222, $result[1]['userCertificationId']);
    }

    public function testFindFormatedCertificatesByUserId()
    {
        $result = $this->getCertificateService()->findFormatedCertificatesByUserId(2);
        $this->assertEquals(2, count($result));
        $this->assertEquals('（货运）经营性道路货物运输驾驶员', $result[0]['certificationName']);
        $this->assertEquals('2016-04-15', $result[0]['startTime']);
        $this->assertEquals('2018-04-15', $result[0]['deadline']);
        $this->assertEquals('（危险品）道路危险货物运输驾驶员', $result[1]['certificationName']);
        $this->assertEquals('2016-04-16', $result[1]['startTime']);
        $this->assertEquals('2018-04-16', $result[1]['deadline']);
    }

    public function testFindCertificateCountByUserId()
    {
        $result = $this->getCertificateService()->findCertificateCountByUserId(2);
        $this->assertEquals(2, $result);
    }

    public function testFindCertificateByName()
    {
        $result = $this->getCertificateService()->findCertificateByName('（货运）经营性道路货物运输驾驶员');
        $this->assertEquals(2, $result['id']);
    }

    public function testFindCertificatesByNames()
    {
        $certNames = array('（货运）经营性道路货物运输驾驶员', '（危险品）道路危险货物运输驾驶员');
        $result = $this->getCertificateService()->findCertificatesByNames($certNames);
        $this->assertEquals(2, count($result));
        $this->assertEquals(2, $result[0]['id']);
        $this->assertEquals(3, $result[1]['id']);
    }

    public function testCreateCertifications()
    {
        $certNames = array('（出租车）出租汽车驾驶员', '（危险品）道路危险货物运输押运人员');
        $this->getCertificateService()->createCertifications($certNames);
        $dbCerts = $this->getCertificateService()->findCertificatesByNames($certNames);
        $this->assertEquals(2, count($dbCerts));
        $this->assertEquals('（出租车）出租汽车驾驶员', $dbCerts[0]['category']);
        $this->assertEquals('（危险品）道路危险货物运输押运人员', $dbCerts[1]['category']);
    }

    public function testResetUserCertificate()
    {
        $resetAwardTime = time();
        $this->getUserCertificateDao()->updateUsersCertification(222, array(
            'isRevoked' => 1,
            'revokedTime' => time(),
            'dID' => 1231231,
            'reason' => 'this is reason',
            'endLoopTime' => 3,
            'startLoopTIme' => 2,
            'awardTime' => 0
        ));
        $result = $this->getUserCertificateDao()->findCertificationIdAndUserId(3, 2);
        $this->assertEquals('this is reason', $result['reason']);

        $this->getCertificateService()->resetUserCertificate(3, 2, $resetAwardTime);
        $resetResult = $this->getUserCertificateDao()->findCertificationIdAndUserId(3, 2);
        $this->assertEquals('', $resetResult['reason']);
        $this->assertEquals($resetAwardTime, $resetResult['awardTime']);
        $this->assertEquals(0, $resetResult['isRevoked']);
        $this->assertEquals('1231231', $resetResult['dID']);
        $this->assertEquals(0, $resetResult['revokedTime']);
        $this->assertEquals(1, $resetResult['startLoopTime']);
        $this->assertEquals(0, $resetResult['endLoopTime']);
    }

    public function testUpdateUserCertificateAwardTime()
    {
        $updatedAwardTime = time();
        $this->getCertificateService()->updateUserCertificateAwardTime(3, 2, $updatedAwardTime);
        $result = $this->getUserCertificateDao()->findCertificationIdAndUserId(3, 2);
        $this->assertEquals($updatedAwardTime, $result['awardTime']);
    }

    /**
     * @expectedException Exception
     */
    public function testReAwardUserCertificationWithException()
    {
        $this->getCertificateService()->reAwardUserCertification(3, 2, time(), '1123');
    }

    public function testReAwardUserCertification()
    {
        $reAwardTime = time();
        $this->getCertificateService()->reAwardUserCertification(4, 4, $reAwardTime, '11232');
        $result = $this->getCertificateService()->findCertificationIdAndUserId(4, 4);
        $this->assertEquals(5, $result['startLoopTime']);
        $this->assertEquals('11232', $result['dID']);
        $this->assertEquals($reAwardTime, $result['awardTime']);
    }

    public function testRevokeUserCertification()
    {
        $revokedTime = time();
        $this->getCertificateService()->revokeUserCertification(222, '吊销', $revokedTime);
        $revokedUserCerts = $this->getRevokedUserCertDao()->listRevokedUserCerts(3);
        $this->assertEquals(2, count($revokedUserCerts));
        $this->assertEquals(4, $revokedUserCerts[1]['startLoopTime']);
        $this->assertEquals(4, $revokedUserCerts[1]['endLoopTime']);
        $this->assertEquals('吊销', $revokedUserCerts[1]['reason']);
        $this->assertEquals($revokedTime, $revokedUserCerts[1]['revokedTime']);

        $userCertCourse = $this->getUserCertCourseDao()->getUserCertCourse(3, 2, 4);
        $this->assertEquals(1, $userCertCourse['isRevoked']);
    }

    public function testDeleteRevokedUserCerts()
    {
        $revokedUserCerts = $this->getRevokedUserCertDao()->listRevokedUserCerts(3);
        $this->assertEquals(1, count($revokedUserCerts));
        $this->getCertificateService()->deleteRevokedUserCerts(3, 2);
        $revokedUserCertsAfterDelete = $this->getRevokedUserCertDao()->listRevokedUserCerts(3);
        $this->assertEquals(0, count($revokedUserCertsAfterDelete));
    }

    public function testDeleteUserAllCertificates()
    {
        $count = $this->getUserCertificateDao()->findCertificateCountByUserId(2);
        $this->assertEquals(2, $count);
        $this->getCertificateService()->deleteUserAllCertificates(2);
        $countAfterDelete = $this->getUserCertificateDao()->findCertificateCountByUserId(2);
        $this->assertEquals(0, $countAfterDelete);
    }

    public function testDeleteUserCertificateById()
    {
        $count = $this->getUserCertificateDao()->findCertificateCountByUserId(3);
        $this->assertEquals(1, $count);
        $this->getCertificateService()->deleteUserCertificateById(222);
        $countAfterDelete = $this->getUserCertificateDao()->findCertificateCountByUserId(3);
        $this->assertEquals(0, $countAfterDelete);
    }

    public function testGetCertificationsForUser()
    {
        $this->mockUserCertData();
        $certInfos = $this->getCertificateService()->getCertificationsForUser(1);

        $this->assertEquals(false, $certInfos[0]['learnable']);
        $this->assertEquals(false, $certInfos[0]['buyable']);
        $this->assertEquals('completion', $certInfos[0]['learningStatus']);
        $this->assertEquals('证书课程1', $certInfos[0]['courseName']);
        
        $this->assertEquals(false, $certInfos[1]['learnable']);
        $this->assertEquals(false, $certInfos[1]['buyable']);
        $this->assertEquals('completion', $certInfos[1]['learningStatus']);
        $this->assertEquals('证书课程2', $certInfos[1]['courseName']);
    }

    protected function getUserCertificateDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.UserCertificateDao');
    }

    protected function getRevokedUserCertDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.RevokedUserCertDao');
    }

    protected function getAreaCertDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.AreaCertificationDao');
    }

    protected function getCourseMemberDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.CourseMemberDao');
    }

    protected function getCertificationDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.CertificationDao');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getUserCertCourseDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.UserCertCourseDao');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    private function prepareData()
    {
        $this->batchCreateUserCerts();
        $this->createRevokedUserCert();
        $this->createCertifications();
        $this->createAreaCertifications();
        $this->createUsers();
        $this->createUserCertCourses();
    }

    private function batchCreateUserCerts()
    {
        $userCert1 = array(
            'userId' => 2,
            'certificationId' => 2,
            'awardTime' => 1460689931,
            'startLoopTime' => 1,
            'endLoopTime' => 0,
            'dID' => 112232
        );
        $userCert2 = array(
            'userId' => 2,
            'certificationId' => 3,
            'awardTime' => 1460789930,
            'startLoopTime' => 1,
            'endLoopTime' => 0,
            'dID' => 112231
        );
        $userCert3 = array(
            'id' => 222,
            'userId' => 3,
            'certificationId' => 2,
            'awardTime' => 1328284800,
            'startLoopTime' => 4,
            'endLoopTime' => 0,
            'dID' => 112234
        );
        $userCert4 = array(
            'userId' => 4,
            'certificationId' => 4,
            'awardTime' => 1328284800,
            'startLoopTime' => 1,
            'endLoopTime' => 4,
            'isRevoked' => 1,
            'dID' => 112235
        );
        $this->getUserCertificateDao()->addUserCertification($userCert1);
        $this->getUserCertificateDao()->addUserCertification($userCert2);
        $this->getUserCertificateDao()->addUserCertification($userCert3);
        $this->getUserCertificateDao()->addUserCertification($userCert4);
    }

    private function createRevokedUserCert()
    {
        $revokedUserCert = array(
            'userCertificationId' => 222,
            'userId' => 3,
            'certificationId' => 2,
            'awardTime' => '1328025600',
            'dID' => 112233,
            'revokedTime' => 1479729995,
            'startLoopTime' => 1,
            'endLoopTime' => 3
        );
        $this->getRevokedUserCertDao()->addRevokedUserCert($revokedUserCert);
    }

    private function createCertifications()
    {
        $certification1 = array(
            'id' => 2,
            'category' => '（货运）经营性道路货物运输驾驶员'
        );
        $certification2 = array(
            'id' => 3,
            'category' => '（危险品）道路危险货物运输驾驶员'
        );
        $this->getCertificationDao()->addCertification($certification1);
        $this->getCertificationDao()->addCertification($certification2);
    }

    private function createAreaCertifications()
    {
        $areaCert1 = array(
            'areaCode' => '001',
            'certificationId' => 2,
            'learnMode' => 1,
            'relearn' => 1,
            'createdTime' => '1479707094',
            'years' => 2
        );
        $areaCert2 = array(
            'areaCode' => '001',
            'certificationId' => 3,
            'learnMode' => 1,
            'relearn' => 1,
            'createdTime' => '1479707094',
            'years' => 2
        );
        $this->getAreaCertDao()->addAreaCertification($areaCert1);
        $this->getAreaCertDao()->addAreaCertification($areaCert2);
    }

    private function createUsers()
    {
        $user1 = array(
            'id' => '2',
            'email' => '2@howzhi.com',
            'password' => 'FIPWI1hWeyJrdut1O8PDjFe33s8B+tGMk1Qjm4tnOiw=',
            'salt' => '4dg0xmtlo1s00s0w4kwo0kwc0kwoscg',
            'nickname' => '成员2',
            'roles' => '|ROLE_USER|',
            'areaCode' => '001',
            'createdTime' => '1479707094'
        );
        $user2 = array(
            'id' => '3',
            'email' => '3@howzhi.com',
            'password' => 'FIPWI1hWeyJrdut1O8PDjFe33s8B+tGMk1Qjm4tnOiw=',
            'salt' => '4dg0xmtlo1s00s0w4kwo0kwc0kwoscg',
            'nickname' => '成员3',
            'roles' => '|ROLE_USER|',
            'areaCode' => '001',
            'createdTime' => '1479707094'
        );
        $this->getUserDao()->addUser($user1);
        $this->getUserDao()->addUser($user2);
    }

    private function createUserCertCourses()
    {
        $userCertCourse  = array(
            'areaCode' => '001',
            'userId' => 3,
            'courseId' => 2,
            'certificationId' => 2,
            'startTimeNum' => '1454256000',
            'deadlineNum' => '1517414399',
            'partIndex' => 0,
            'years' => 2,
            'learnMode' => 1,
            'awardTime' => '1328025600',
            'isRevoked' => 0,
            'status' => 'UNPAID',
            'currentLoopTime' => 4,
            'relearn' => 1,
            'certificationCourseCount' => 3,
            'certificationStartPeriod' => '1454256000',
            'certificationEndPeriod' => '1517414399',
            'certificationName' => '（货运）经营性道路货物运输驾驶员',
            'certificationType' => '全年制'
        );
        $this->getUserCertCourseDao()->addUserCertCourse($userCertCourse);
    }

    private function mockUserCertData()
    {
        $this->mock('Custom:Certificate.UserCertCourseDao');
        $this->mock('Custom:Course.CourseService');
        $this->mock('Custom:Course.CourseMemberDao');
        $this->getUserCertCourseDao()->shouldReceive('getAvailableUserCertCoursesByUserId')->andReturn(
            array(
                array(
                    'id' => 1,
                    'courseId' => 1,
                    'isRevoked' => 1,
                    'isCompletedOffline' => 0,
                    'startTimeNum' => '1328025600',
                    'deadlineNum' => '1391183999',
                    'currentLoopTime' => 1,
                    'status' => 'finished',
                    'isFrozen' => 0,
                ),
                array(
                    'id' => 1,
                    'courseId' => 1,
                    'isRevoked' => 0,
                    'isCompletedOffline' => 0,
                    'startTimeNum' => '1328025600',
                    'deadlineNum' => '1391183999',
                    'currentLoopTime' => 2,
                    'status' => 'unpaid',
                    'isFrozen' => 0,
                ),
                array(
                    'id' => 1,
                    'courseId' => 2,
                    'isRevoked' => 0,
                    'isCompletedOffline' => 0,
                    'startTimeNum' => '1328025600',
                    'deadlineNum' => '1391183999',
                    'currentLoopTime' => 1,
                    'status' => 'unpaid',
                    'isFrozen' => 0,
                )
            )
        );
        $args = array(array('1', '2'));
        $this->getCourseService()->shouldReceive('getPercent')->andReturn('10%');
        $this->getCourseService()->shouldReceive('findCoursesByIds')->withArgs($args)->andReturn(
            array(
                '1' => array(
                    'title' => '证书课程1',
                    'smallPicture' => 'public://default/2016/05-06/small.jpg',
                    'middlePicture' => 'public://default/2016/05-06/middle.jpg',
                    'largePicture' => 'public://default/2016/05-06/large.jpg',
                    'status' => 'published',
                    'id' => 1,
                    'studentNum' => 1, 
                    'price' =>1, 
                    'ratingNum' => 5, 
                    'teacherIds' => 1, 
                    'versionStatus' => 'newest', 
                    'discountId' => 0, 
                    'hasTestpaper' => 0, 
                    'totalHours' => 30
                ),
                '2' => array(
                    'title' => '证书课程2',
                    'status' => 'published',
                    'id' => 2,
                    'studentNum' => 1, 
                    'price' =>1, 
                    'ratingNum' => 5, 
                    'teacherIds' => 1, 
                    'versionStatus' => 'newest', 
                    'discountId' => 0, 
                    'hasTestpaper' => 0, 
                    'totalHours' => 30
                )
            )
        );
        $this->getCourseMemberDao()->shouldReceive('findCourseMembersForStudentId')->andReturn(
            array(
                array(
                    'courseId' => 1,
                    'currentLoopTime' => 1,
                    'createdTime' => '1328025600',
                    'learnedHours' => 12,
                    'isAllLessonsFinished' => 0,
                    'isTestpaperPassed' => 0,
                    'isCompleted' => 0
                ),
                array(
                    'courseId' => 1,
                    'currentLoopTime' => 2,
                    'createdTime' => '1328025600',
                    'learnedHours' => 12,
                    'isAllLessonsFinished' => 0,
                    'isTestpaperPassed' => 0,
                    'isCompleted' => 0
                )
            )
        );
    }
}
