<?php
namespace Custom\Service\Certificate;

interface CertificateService
{
    public function findCertificatesByUserId($userId);

    /**
     * 获取证书信息, 包括已吊销的证书
     */
    public function findCertificatesIncludingRevokedByUserId($userId);

    /**
     * 返回
     * array () {
     *     array() {
     *         certificationId => 1,
     *         certificationName => 证书1,
     *         category => 证书1,
     *         startTime => '2014-12-2',
     *         deadline  => '2016-12-2',
     *         awardTime => 1,
     *         relearn => 1,
     *         learnMode => 1
     *     }
     * }
     */
    public function findFormatedCertificatesByUserId($userId);

    public function findCertificateCountByUserId($userId);

    public function findCertificateByName($certificationName);

    public function findCertificatesByNames($certificationNames);

    public function createCertifications($certificationNames);

    /**
     * 重置用户证书, 如果状态为已吊销, 则变为非吊销 (只影响 user_certification 表)
     *  该方法只给 HelperController用
     */
    public function resetUserCertificate($userId, $certificationId, $awardTime);

    public function updateUserCertificateAwardTime($userId, $certificationId, $awardTime);

    /** 重新颁发证书, 注意, 当前证书必须已吊销, 否则会抛出异常 */
    public function reAwardUserCertification($userId, $certificationId, $awardTime, $dId = null);

    public function revokeUserCertification($userCertificationId, $reason = '', $revokedTime = null);

    //冻结用户证书
    public function freezeUserCertification($userCertificationId, $fields);

    //解冻用户证书
    public function unfreezeUserCertification($userCertificationId, $fields);

    //冻结用户课程
    public function freezeUserCertCourse($userCertCourseId, $fields);

    //解冻用户课程
    public function unfreezeUserCertCourse($userCertCourseId, $fields);

    public function findFrozenUserCertification($userId);

    public function deleteRevokedUserCerts($userId, $certificationId);

    public function findCertificationIdAndUserId($userId, $certificationId);

    public function findCertificationBydID($dID);
    
    public function deleteUserAllCertificates($userId);

    public function deleteUserCertificateById($id);

    public function sendAddNewCertificationSms($userId, $idcard, $mobileNum, $nickName, $date, $certificateName);

    /**
     * 行政区域证书
     */
    public function getAreaCertification($id);

    /**
     * 删除行政区域下的证书时, 默认删除其下所有证书历史
     */
    public function deleteAreaCertfication($areaCode, $certificationId);

    /**
     * 生成行政区域下的证书时, 默认插入一条证书历史
     */
    public function createAreaCertification($areaCertification);

    public function updateAreaCertification($id, $fields);

    public function findAreaCertificationsByAreaCode($areaCode);

    /**
     * @return array(
     *    '123' => '证书名'    //key 为证书id, value 为证书名称
     * )
     */
    public function findIndexCertCategoryByAreaCode($areaCode);

    public function findAreaCertificationsByCertificationId($certificationId);

    public function findAreaCertificationsByCertificationIds($certificationIds);

    public function findByAreaCodeAndCertificationId($areaCode, $certificationId);

    public function searchAreaCertifications($conditions, $orderBys, $start, $limit);

    public function searchAreaCertificationsCount($conditions);

    public function getAreaCertifications($code);

    /**
     * 给当个学员生成 证书课程记录
     * 如果当前时间已经进入下一个循环, 但课程记录中无改循环记录, 则生成该循环记录
     * 如果 当前循环的证书学制, 周期 和 最新证书学制/周期一样, 同时发现 证书部分缺失, 则生成缺失部分的课程记录,
     *      如果缺失部分的课程尚未创建, 则创建一个 courseId = 0 的课程
     */
    public function generateUserCertCourses($userId);

    public function generateAllUsersCertCourses();

    public function addUserCertCourse($userCertificationCourse);

    public function updateUserCertCourse($id, $userCertificationCourse);

    /**
     * 获取用户所有的证书课程, 同时列出课程下已学的课时数量和总课时数量
     */
    public function getAllCertificationsWithLessonStatus($userId);

    /**
     * 获取所有可学习的证书课程, 同时列出课程下已学的课时数量和总课时数量
     */
    public function getLearnableCertificationsWithLessonStatus($userId);

    /**
     * 获取一个用户的所有证书(包括证书课程) 或 只获取证书课程
     *                 默认为null
     *                 否则, 只获取证书课程
     *                 默认为 false
     * 1.) 获取所有证书及证书课程  ($cascadeCertification == true)
     * array(
     *     0 => array(
     *         'certificationId' => '1',
     *         'certificationName' => '自行车',
     *         'awardTime' =>  '2016-04-15',
     *         'certificationType' =>  '全年制',
     *         'courses' => array (...)  // 同获取所有证书课程格式(2维数组)
     *     ),
     *     1 => array (size=5)
     *         'certificationId' => '2',
     *         'certificationName' => '火车',
     *         'awardTime' => '2016-04-15',
     *         'certificationType' =>  '一年制',
     *         'courses' => array ()  //如果该证书下没课程, 则显示空数组, 即array()
     *
     * 2.) 只获取证书课程  ($cascadeCertification == false)
     * array(
     *     0 => array(
     *         'courseId' => '3',
     *         'partIndex' => '0',     //n-1 表示 第n部分, 如1表示第二部分
     *         'courseName' => '证书课程(上半部分)',
     *         'certificationId' =>  '3',
     *         'price' => '123',
     *         'startTime' => '2016-04-15',
     *         'deadline' => '2017-04-15',
     *         'certificationName' => '风火轮',
     *         'certificationType' => '一年制',
     *         'learningStatus' => 'learning',  //所有支持的学习状态 详见 LearnStatus.php
     *         'leftDays' => 128,  //正常学习的到期时间, 单位天
     *         'buyable' => false,  //是否支持购买, 逻辑见 LearnStatus.php
     *         'learnable' => false,  //是否支持学习
     *         'teacherIds' => array(1, 2, 3)
     *     ),
     *     1 => array(
     *         'courseId' => '5',
     *         'partIndex' => '1',
     *         'courseName' => '证书课程(下半部分)',
     *         'certificationId' => '3',
     *         'price' => '123',
     *         'startTime' => '2017-04-15',
     *         'deadline' => '2018-04-15',
     *         'certificationName' => '风火轮',
     *         'certificationType' => '一年制',
     *         'smallPictureUrl' => 'files/default/2016/05-06/2011517178a9789024.jpg',
     *         'middlePictureUrl' => 'files/default/2016/05-06/2011517178a9789024.jpg',
     *         'largePictureUrl' => 'files/default/2016/05-06/2011517178a9789024.jpg',
     *         'learningStatus' => 'overtimeMakeupYes',
     *         'leftDays' => -100,  //正常学习的到期时间, 单位天
     *         'makeupDeadline' => '2019-04-15',
     *         'makeupLeftDays' => 265,
     *         'buyable' => true, //是否支持购买, 逻辑见 LearnStatus.php
     *         'learnable' => false,  //是否支持学习
     *         'teacherIds' => array(1, 2, 3)
     *     )
     *
     *  3.) 只获取单门课程信息 ($courseId != null)
     *     格式同获取所有证书课程, 只有一维数组(即只有一门课的信息)
     * @param     $courseId             有值时, 只获取所给课程, 无值时, 则获取用户所有课程
     * @param     $cascadeCertification =          true 时 获取所有证书及证书课程
     * @return
     */
    public function getCertificationsForUser($userId, $cascadeCertification = false, $courseId = null, $currentLoopTime = null);

    /**
     * 新建课程时调用, 用户证书课程内, 同一行政区域, 同一证书, 同一部分的课程中, 如果用户没买过, 则更新其courseId
     */
    public function updateNotBuyUserCertCourse($courseInfo);

    /**
     * 修改 周期或学制时调用, 对于某个循环内从来没买过的未过期课程(未吊销), 需要重新生成
     *    注意, 是否发生修改 需要自行判断
     * @param $areaCertification
     * {
     *      'areaCode' => ...
     *      'certificationId' => ...
     * }
     */
    public function regenerateNeverPaidLoopUserCertCourses($areaCertification);

    /**
     * 定时任务调用, 将未购买或学习中的课程, 按照实际情况标记为已过期
     */
    public function markOvertimeUserCertCourses();

    /**
     * 获取行政区域下所有证书课程
     */
    public function getCertificationsForArea($areaCode);

    public function getAreaCertificationsHaveCourse($areaCode);

    /**判断用户是否可以购买此课程*/
    public function isCertificationCourseBuyable($userId, $courseId, $currentLoopTime);

    /**
     * 证书
     */
    public function getAllCertifications();

    public function getCertifications($ids);

    public function getCertification($id);
    
    public function getUserCompletionCertificateCourses($userId, $certificationId, $currentLoopTime = null);

    /**证书学制修改历史 */

    public function addUserCertification($fields);
    
    public function getProfile($userId);

    /**获取某个用户的已结业证书课程数量*/
    public function getUserIdCompletionCertificateCoursesNumber($userId);

    public function getUserIdsByCertificationId($certificationId);

}
