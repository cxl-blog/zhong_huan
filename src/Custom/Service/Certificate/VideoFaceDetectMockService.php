<?php
namespace Custom\Service\Certificate;

interface VideoFaceDetectMockService
{
    public function generateMissedVideoFaceDetect();
}
