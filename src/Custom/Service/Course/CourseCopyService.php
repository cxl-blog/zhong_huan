<?php

namespace Custom\Service\Course;

interface CourseCopyService
{
    public function copy($course, $link);

    public function newCopy($courseId,$course, $link);
}
