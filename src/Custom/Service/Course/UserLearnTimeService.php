<?php

namespace Custom\Service\Course;

interface UserLearnTimeService
{
    /**
     * 如果不存在记录, 才新增
     */
    public function addUserLearnTimeIfNeed($userId, $certificationId);

    /**
     * 获取当天的学习时长信息 
     * {
     *     isExceeding: true,  //true 表示已经超出
     *     maxLearningSeconds: 3600,  //最长学习时长, 转化为秒
     *     maxLearningMinutes: 60,      //最长学习时长, 转化为分
     *     sequentialLearning: true,       //true表示闯关学习
     *     currentLearningSeconds: 3300, //今天已学时长, 转化为秒
     *     currentLearningMinutes: 55, //今天已学时长, 转化为分
     * }
     */
    public function getCurrentLearnTimeInfo($userId, $courseId, $areaCode = null, $certificationId = null);

    public function waveUserLearnTime($userId, $certificationId, $learnedSeconds);

    public function clearUserLearnTime();

    public function clearSingleUserLearnTime($userId);
}
