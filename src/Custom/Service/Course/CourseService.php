<?php

namespace Custom\Service\Course;

interface CourseService
{
    public function updateCourse($id, $fields);

    public function tryManageCourse($courseId);

    public function getCourse($id);

    /**
     * 获取普通或证书课程
     *                如果是证书课程, 返回 CertificateService::getCertificationsForUser的结果
     * @param  [type]                                                     $id              [description]
     * @param  integer                                                    $currentLoopTime [description]
     * @return 如果是普通课程，返回getCourse($id)的结果，
     */
    public function getCertificationOrNormalCourse($userId, $id, $currentLoopTime = 1);

    public function searchCourses($conditions, $orderBy, $start, $limit);

    public function searchCourseCount($conditions);

    public function getSumHours($items);

    public function getLearnedHours($items, $userId, $currentLoopTime = null);

    public function getTextSuggestedHours($items, $userId, $currentLoopTime = null);

    public function getChapterSuggestedHours($items, $isAdmin, $userId);

    public function getUnitSuggestedHours($items, $isAdmin, $userId, $currentLoopTime = null);

    public function getTextSuggestedHoursForAdmin($items, $userId);

    public function getCertificationId($courseId);

    public function getCertificationCourseLearnedPercent($sumHours, $learnedHours);

    public function searchLessonView($conditions, $orderBy, $start, $limit);

    public function searchLessonViewCount($conditions);

    public function findFinishedFaceableCertVideoLessonLearns($startTime);

    public function getUserLearnLessonStatus($userId, $courseId, $lessonId, $currentLoopTime = null);

    public function findUserLearnedLessons($userId, $courseId, $currentLoopTime = null);

    public function getUserLearnLessonStatuses($userId, $courseId, $currentLoopTime = null);

    public function waveWatchingTime($userId, $lessonId, $time, $currentLoopTime = null);

    public function waveLearningTime($userId, $lessonId, $time, $currentLoopTime = null);

    public function getSumWatchTimeGroupByLesson($userId, $courseId, $currentLoopTime = 1);

    public function setCoursesPriceWithDiscount($discountId);

    public function findCoursesByCertificationIdAndAreaCode($certificationId,$areaCode);

    public function getFirstCourseMember($userId, $courseId);

    public function updateAsLatestCourse($courseId);

    public function updateOtherCoursesAsOld($latestCourse);

    /**
     * 获取线下完成的证书课程
     * 返回
     * {
     *     'id' => 1,
     *     'dId' => 1,
     *     'courseId' => 1,
     *     'userId' => 1,
     *     'currentLoopTime' => 1,
     *     'courseStartTime' => '12231232212',
     *     'courseEndTime' => '12231232212',
     *     'createdTime' => '12231232212',
     *     'updatedTime' => '12231232212'
     * }
     * @param dId  数据同步时 的唯一标识符
     */
    public function getOfflineFinishedCourseByDid($dId);

    /**
     * 获取线下完成的证书课程, 返回二维数组, 里面的一维数组格式同 getOfflineFinishedCourseByDid
     */
    public function getOfflineFinishedCoursesByUserId($userId);

    /**
     * @param offlineFinishedCourse
     * {
     *     'dId' => 1,
     *     'userId' => 1,
     *     'currentLoopTime' => 1,
     *     'courseId' => 1
     * }
     */
    public function createOfflineFinishedCourse($offlineFinishedCourse);

    public function deleteOfflineFinishedCoursesByDid($dId);

    /**
     * 删除线下已结业的课程数据
     */
    public function deleteOfflineFinishedCourses($userId, $certificationId);

    /**
     * 包括清除
     * 1.) 课程学习历史  course_lesson_learn  course_lesson_view
     * 2.) 课时学习进度cookie
     * 3.) 人脸和弹题记录 question_marker_result   face_marker_result
     * 4.) 考试记录 testpaper_item_result 和 testpaper_result
     * @param $currentLoopTime, 第几次循环学习, 1 = 第一次
     */
    public function clearCourseHistory($userId, $courseId, $currentLoopTime = null);

    public function isTestpaperPassed($userId, $courseId, $currentLoopTime);

    /**
     * 学习记录 API
     */
    public function getLearnRecord($userId, $courseId);

    public function getLearnRecordDetail($userId, $courseId, $lessonId);

    public function durationTextFilter($value);

    public function getLearnLessons($learns, $userId, $currentLoopTime = null);

    public function getTestpaperLesson($courseId);

    public function updateCourseTotalHoursAndHasTestPaperFields($courseId);

    public function getPercent($totalHours, $learnedHours);

    /**
     * 获取所有课时信息, course_lesson_learn 表中所有信息, 同时赋予 course_lesson 中的type属性
     *   如果是试卷, 则 type = 'testpaper'
     * @return 
     */
    public function getLessonLearnsWithLessonType($userId);

    /**
     *  最新课程观看信息（冗余表）
     */
    //public function getLastLessonView($userId, $lessonId, $currentLoopTime);
    public function getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime);

    public function updateLastLessonView($userId, $courseId, $lessonId, $currentLoopTime, $watchTime);

    public function getLessonStatus($userId, $courseId, $currentLoopTime, $lessons);

    public function searchRecommendedCourses($conditions, $orderBy, $start, $limit);

    public function markAsAtLeastOneLessonFinished($userId, $courseId, $currentLoopTime);
}
