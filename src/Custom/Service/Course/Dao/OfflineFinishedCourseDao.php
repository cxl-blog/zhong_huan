<?php

namespace Custom\Service\Course\Dao;

interface OfflineFinishedCourseDao
{
    public function getOfflineFinishedCourseByDid($dId);

    public function getOfflineFinishedCoursesByUserId($userId);

    public function createOfflineFinishedCourse($offlineFinishedCourse);

    public function deleteOfflineFinishedCoursesByDid($dId);

    public function deleteOfflineFinishedCourses($userId, $certificationId);
}
