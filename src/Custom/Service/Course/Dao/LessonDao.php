<?php

namespace Custom\Service\Course\Dao;

interface LessonDao
{
    public function getTestpaperLesson($courseId);
}
