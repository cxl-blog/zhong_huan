<?php

namespace Custom\Service\Course\Dao;

interface CourseDao
{
    public function updateCourse($id, $fields);

    public function getCourse($id);

    public function searchCourses($conditions, $orderBy, $start, $limit);

    public function searchCourseCount($conditions);

    public function  findCoursesByCertificationIdAndAreaCode($certificationId,$areaCode);

    /**
     * 获取某行政区域下所有证书课程, 该课程必须已发布
     * @return 同 getLatestCertificationCourses
     */
    public function getAreaLatestCertificationCourses($areaCode);

    public function getCertificationId($courseId);

    public function findAreaPublishedCertCourses($areaCode);
}
