<?php

namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Course\Dao\Impl\LessonViewDaoImpl as BaseDaoImpl;

class LessonViewDaoImpl extends BaseDaoImpl
{
    public function updateLessonView($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->getLessonView($id);
    }

    public function deleteUserCourseLessonViews($userId, $courseId, $currentLoopTime = 1)
    {
        $sql    = "DELETE FROM {$this->table} WHERE userId = ? and courseId = ? and currentLoopTime = ?";
        $result = $this->getConnection()->executeUpdate($sql, array($userId, $courseId, $currentLoopTime));
        $this->clearCached();
        return $result;
    }

    public function getSumWatchTimeGroupByLesson($userId, $courseId, $currentLoopTime = 1)
    {
        $sql    = "SELECT lessonId, SUM( watchTime ) AS watchTime FROM  `course_lesson_view` WHERE userId = ? AND courseId = ? AND currentLoopTime =? GROUP BY lessonId ";
        $result = $this->getConnection()->fetchAll($sql,array($userId,$courseId,$currentLoopTime));
        return $result ? $result : array();
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, 'course_lesson_view')
            ->andWhere('fileType = :fileType')
            ->andWhere('fileStorage = :fileStorage')
            ->andWhere('courseId = :courseId')
            ->andWhere('lessonId = :lessonId')
            ->andWhere('userId = :userId')
            ->andWhere('currentLoopTime = :currentLoopTime')
            ->andWhere('createdTime >= :startTime')
            ->andWhere('createdTime <= :endTime')
            ->andWhere('watchTime != :ignoredWatchTime');
        return $builder;
    }
}
