<?php

namespace Custom\Service\Course\Dao\Impl;

use Custom\Common\Util\CourseLoopUtils;
use Custom\Service\Course\Dao\LessonLearnDao;
use Topxia\Service\Course\Dao\Impl\LessonLearnDaoImpl as BaseLessonLearnDao;

class LessonLearnDaoImpl extends BaseLessonLearnDao implements LessonLearnDao
{
    public function deleteUserCourseLessonLearns($userId, $courseId, $currentLoopTime = 1)
    {
        $sql    = "DELETE FROM {$this->table} WHERE userId = ? and courseId = ? and currentLoopTime = ?";
        $result = $this->getConnection()->executeUpdate($sql, array($userId, $courseId, $currentLoopTime));
        $this->clearCached();
        return $result;
    }

    public function getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime = 1)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}:lessonId:{$lessonId}:currentLoopTime:{$currentLoopTime}", $userId, $lessonId, $currentLoopTime, function ($userId, $lessonId, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId=? AND lessonId=? AND currentLoopTime = ?";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $lessonId, $currentLoopTime)) ?: null;
        }

        );
    }

    public function findLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime = 1)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}:courseId:{$courseId}:currentLoopTime:{$currentLoopTime}", $userId, $courseId, $currentLoopTime, function ($userId, $courseId, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId=? AND courseId=? AND currentLoopTime=?";
            return $that->getConnection()->fetchAll($sql, array($userId, $courseId, $currentLoopTime)) ?: array();
        }

        );
    }

    public function findLearnsByUserIdAndCourseIdAndStatus($userId, $courseId, $status, $currentLoopTime = 1)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}:courseId:{$courseId}:status:{$status}:currentLoopTime:{$currentLoopTime}", $userId, $courseId, $status, $currentLoopTime, function ($userId, $courseId, $status, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId=? AND courseId=? AND status = ? AND currentLoopTime = ?";
            return $that->getConnection()->fetchAll($sql, array($userId, $courseId, $status, $currentLoopTime)) ?: array();
        }

        );
    }

    public function findLearnsByUserId($userId)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}", $userId, function ($userId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId=?";
            return $that->getConnection()->fetchAll($sql, array($userId)) ?: array();
        }

        );
    }
}
