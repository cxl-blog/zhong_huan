<?php

namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Course\Dao\OfflineFinishedCourseDao;

class OfflineFinishedCourseDaoImpl extends BaseDao implements OfflineFinishedCourseDao
{
    protected $table = 'offline_finished_course';

    public function getOfflineFinishedCourseByDid($dId)
    {
        $that = $this;

        return $this->fetchCached("dId:{$dId}", $dId, function ($dId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE dId = ? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($dId)) ?: null;
        }

        );
    }

    public function getOfflineFinishedCoursesByUserId($userId)
    {
        $that = $this;

        return $this->fetchCached("userId:{$userId}", $userId, function ($userId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId = ?";
            return $that->getConnection()->fetchAll($sql, array($userId)) ?: null;
        }

        );
    }

    public function createOfflineFinishedCourse($offlineFinishedCourse)
    {
        $offlineFinishedCourse['createdTime'] = time();
        $offlineFinishedCourse['updateTime'] = $offlineFinishedCourse['createdTime'];

        $affected              = $this->getConnection()->insert($this->table, $offlineFinishedCourse);
        $this->clearCached();

        if ($affected <= 0) {
            throw $this->createDaoException('Insert offlineFinishedCourse error.');
        }
    }

    public function deleteOfflineFinishedCoursesByDid($dId)
    {
        $this->getConnection()->delete($this->table, array('dID' => $dId));
    }

    public function deleteOfflineFinishedCourses($userId, $certificationId)
    {
        $this->getConnection()->delete($this->table, array('userId' => $userId, 'certificationId' => $certificationId));
    }
}
