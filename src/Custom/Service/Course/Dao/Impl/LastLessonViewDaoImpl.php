<?php
namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Course\Dao\LastLessonViewDao;

class LastLessonViewDaoImpl extends BaseDao implements LastLessonViewDao
{
    protected $table = 'course_lesson_view_last';

    public function getLastLessonView($id)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function addLastLessonView($fields)
    {
        $fields['createdTime'] = time();
        $affected = $this->getConnection()->insert($this->table, $fields);
        $this->clearCached();
        return $this->getLastLessonView($this->getConnection()->lastInsertId());
    }

    public function updateLastLessonView($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->getLastLessonView($id);
    }

    public function getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? AND lessonId = ? AND currentLoopTime = ?";
        return $this->getConnection()->fetchAssoc($sql, array($userId, $lessonId, $currentLoopTime)) ?: null;
    }
}
