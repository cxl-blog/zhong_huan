<?php

namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Course\Dao\UserLearnTimeDao;

class UserLearnTimeDaoImpl extends BaseDao implements UserLearnTimeDao
{
    protected $table = 'user_learn_time';

    public function getUserLearnTime($userId, $certificationId)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE userId = ? and certificationId = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($userId, $certificationId)) ?: null;
    }

    public function addUserLearnTime($fields)
    {
        $this->getConnection()->insert($this->table, $fields);
    }

    public function increaseLearnTime($userId, $certificationId, $addedTime)
    {
        $sql  = "UPDATE {$this->getTable()} SET learnedSeconds = (learnedSeconds + ?), learnedMinutes = ceil(learnedSeconds / 60) WHERE userId = ? and certificationId = ?";
        $this->getConnection()->executeUpdate($sql, array($addedTime, $userId, $certificationId));
    }

    public function clearLearnTime()
    {
        $sql  = "UPDATE {$this->getTable()} SET learnedSeconds = 0, learnedMinutes = 0";
        $this->getConnection()->executeUpdate($sql);
    }

    public function clearSingleLearnTime($userId)
    {
        $sql  = "UPDATE {$this->getTable()} SET learnedSeconds = 0, learnedMinutes = 0 where userId = ?";
        $this->getConnection()->executeUpdate($sql, array($userId));
    }
}
