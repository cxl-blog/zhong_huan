<?php

namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Course\Dao\Impl\FavoriteDaoImpl as BaseFavoriteDaoImpl;
use Custom\Common\Util\CourseLoopUtils;

class FavoriteDaoImpl extends BaseFavoriteDaoImpl
{
    public function getFavoriteByUserIdAndCourseId($userId, $courseId, $currentLoopTime = null)
    {
        if (empty($currentLoopTime)) {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
            //throw new \RuntimeException("currentLoopTime未设置！");
        }
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? AND courseId = ? AND currentLoopTime = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($userId, $courseId, $currentLoopTime)) ? : null; 
    }

    public function deleteFavorite($id, $currentLoopTime = null)
    {
        if ($currentLoopTime == null) {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
        }
        return $this->getConnection()->delete($this->table, array('id' => $id, 'currentLoopTime' => $currentLoopTime));
    }

}