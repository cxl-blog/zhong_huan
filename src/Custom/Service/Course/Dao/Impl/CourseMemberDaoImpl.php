<?php
namespace Custom\Service\Course\Dao\Impl;

use Custom\Common\Util\CourseLoopUtils;
use Topxia\Service\Course\Dao\Impl\CourseMemberDaoImpl as BaseDaoImpl;

class CourseMemberDaoImpl extends BaseDaoImpl
{
    protected $table = 'course_member';

    public function getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime = null)
    {
        $that = $this;
        if (empty($currentLoopTime)) {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
            //throw new \RuntimeException("currentLoopTime未设置！");
        }

        return $this->fetchCached("courseId:{$courseId}:userId:{$userId}:currentLoopTime:{$currentLoopTime}", $courseId, $userId, $currentLoopTime, function ($courseId, $userId, $currentLoopTime) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE userId = ? AND courseId = ? AND currentLoopTime = ? ORDER BY id desc LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($userId, $courseId, $currentLoopTime)) ?: null;
        }

        );
    }

    public function findCourseMembersForStudentId($userId)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE userId = ? AND role = 'student'";
        return $this->getConnection()->fetchAll($sql, array($userId));
    }

    public function findFinishedCoursesByStudentsIds($studentsIds)
    {
        $marks = str_repeat('?,', count($studentsIds) - 1).'?';
        $sql   = "SELECT * FROM {$this->getTable()} WHERE userId in ({$marks}) AND role = 'student' AND isLearned = 1 and isCompleted = 0";
        return $this->getConnection()->fetchAll($sql, $studentsIds);
    }

    public function getMemberByOrderId($orderId)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE orderId = ? limit 1";
        return $this->getConnection()->fetchAssoc($sql, array($orderId)) ?: null;
    }

    public function updateMember($id, $member)
    {
        $memberInfo = $this->getMember($id);
        if (empty($memberInfo['isCompleted'])) {
            $member['updatedTime'] = time();
        } else {
            unset($member['updatedTime']);
        }
        $this->getConnection()->update($this->table, $member, array('id' => $id));
        $this->clearCached();
        return $this->getMember($id);
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, 'course_member')
            ->andWhere('userId = :userId')
            ->andWhere('courseId = :courseId')
            ->andWhere('isLearned = :isLearned')
            ->andWhere('noteNum > :noteNumGreaterThan')
            ->andWhere('role = :role')
            ->andWhere('createdTime >= :startTimeGreaterThan')
            ->andWhere('createdTime < :startTimeLessThan')
            ->andWhere('courseId IN (:courseIds)')
            ->andWhere('currentLoopTime = :currentLoopTime')
            ->andWhere('userId IN (:userIds)')
            ->andWhere('classroomId = :classroomId')
            ->andWhere('joinedType = :joinedType')
            ->andWhere('isCompleted = :isCompleted')
            ->andWhere('updatedTime >= :updatedTimeGreaterThan')
            ->andWhere('updatedTime < :updatedTimeLessThan');
        return $builder;
    }
}
