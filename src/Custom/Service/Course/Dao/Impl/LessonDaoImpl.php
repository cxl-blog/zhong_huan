<?php

namespace Custom\Service\Course\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Topxia\Service\Course\Dao\Impl\LessonDaoImpl as BaseLessonDao;
use Custom\Service\Course\Dao\LessonDao;

class LessonDaoImpl extends BaseLessonDao implements LessonDao
{
    public function getTestpaperLesson($courseId)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE courseId = ? and type = 'testpaper' LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($courseId)) ?: null;
    }
}
