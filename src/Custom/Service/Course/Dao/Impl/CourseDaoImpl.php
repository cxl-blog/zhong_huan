<?php

namespace Custom\Service\Course\Dao\Impl;

use Custom\Service\Course\Dao\CourseDao;
use Topxia\Service\Course\Dao\Impl\CourseDaoImpl as TopxiaCourseDaoImpl;

class CourseDaoImpl extends TopxiaCourseDaoImpl implements CourseDao
{
    protected $table = 'course';

    public function getCourse($id)
    {
        $that = $this;

        return $this->fetchCached("id:{$id}", $id, function ($id) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE id = ? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($id)) ?: null;
        }

        );
    }

    public function updateCourse($id, $fields)
    {
        $fields['updatedTime'] = time();
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->getCourse($id);
    }

    public function searchCourses($conditions, $orderBy, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBy);
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('*')
            ->orderBy($orderBy[0], $orderBy[1])
            ->setFirstResult($start)
            ->setMaxResults($limit);

        if ($orderBy[0] == 'recommendedSeq') {
            $builder->addOrderBy('recommendedTime', 'DESC');
        }

        return $builder->execute()->fetchAll() ?: array();
    }

    public function searchCourseCount($conditions)
    {
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('COUNT(id)');
        return $builder->execute()->fetchColumn(0);
    }

    public function  findCoursesByCertificationIdAndAreaCode($certificationId,$areaCode)
    {
        $sql = "SELECT * FROM {$this->table} where certificationId = ? AND areaCode = ?;";
        return $this->getConnection()->fetchAll($sql, array($certificationId, $areaCode));
    }

    public function getAreaLatestCertificationCoursesIgnorePublished($areaCode)
    {
        $sql = 'select course.id as courseId, partIndex, course.title courseName, certificationId, course.smallPicture, course.middlePicture, course.largePicture, course.price, course.studentNum, course.ratingNum, teacherIds, course.versionStatus, 1 as currentLoopTime, discountId, learnModeMonths'
            .'  from course '
            .'where course.areaCode = ? and course.type = ? and course.versionStatus = ?';

        return $this->getConnection()->fetchAll($sql, array($areaCode, 'certification', 'latest'));
    } 

    public function getAreaLatestCertificationCourses($areaCode)
    {
        $sql = 'select course.id as courseId, partIndex, course.title courseName, certificationId, course.smallPicture, course.middlePicture, course.largePicture, course.price, course.studentNum, course.ratingNum, teacherIds, course.versionStatus, 1 as currentLoopTime, discountId, learnModeMonths'
            .'  from course '
            .'where course.areaCode = ? and course.type = ? and course.versionStatus = ? and course.status = ?';

        return $this->getConnection()->fetchAll($sql, array($areaCode, 'certification', 'latest', 'published'));
    }

    public function getCertificationId($courseId)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($courseId)) ?: null;
    }

    public function findAreaPublishedCertCourses($areaCode)
    {
        $sql = " select * from course "
            . " where course.areaCode = ? and course.type = 'certification' and course.status = 'published' ";

        return $this->getConnection()->fetchAll($sql, array($areaCode)) ?: null;
    }

    public function findAllCoursesByAreaCodeOrSchoolId($areaCode, $schoolId)
    {
        $sql = " SELECT * FROM {$this->table} WHERE areaCode = ? OR schoolId = ? ";
        return $this->getConnection()->fetchAll($sql, array($areaCode, $schoolId)) ?: array();
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        if (!empty($conditions['title'])) {
            $conditions['titleLike'] = "%{$conditions['title']}%";
        }
        unset($conditions['title']);

        if (isset($conditions['tagId'])) {
            $tagId = (int) $conditions['tagId'];

            if (!empty($tagId)) {
                $conditions['tagsLike'] = "%|{$conditions['tagId']}|%";
            }

            unset($conditions['tagId']);
        }

        if (empty($conditions['status']) || $conditions['status'] == "") {
            unset($conditions['status']);
        }
        /*
        if (!isset($conditions['schoolId'])) {
            $conditions['schoolId'] = 0;
        }
        */
        if (isset($conditions['type']) && $conditions['type'] == 'all') {
            unset($conditions['type']);
            unset($conditions['schoolId']);
        }
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, 'course')
            ->andWhere('updatedTime >= :updatedTime_GE')
            ->andWhere('status = :status')
            ->andWhere('type = :type')
            ->andWhere('type IN ( :types )')
            ->andWhere('price = :price')
            ->andWhere('price > :price_GT')
            ->andWhere('originPrice > :originPrice_GT')
            ->andWhere('originPrice = :originPrice')
            ->andWhere('coinPrice > :coinPrice_GT')
            ->andWhere('coinPrice = :coinPrice')
            ->andWhere('originCoinPrice > :originCoinPrice_GT')
            ->andWhere('originCoinPrice = :originCoinPrice')
            ->andWhere('title LIKE :titleLike')
            ->andWhere('userId = :userId')
            ->andWhere('recommended = :recommended')
            ->andWhere('tags LIKE :tagsLike')
            ->andWhere('startTime >= :startTimeGreaterThan')
            ->andWhere('startTime < :startTimeLessThan')
            ->andWhere('rating > :ratingGreaterThan')
            ->andWhere('vipLevelId >= :vipLevelIdGreaterThan')
            ->andWhere('vipLevelId = :vipLevelId')
            ->andWhere('createdTime >= :startTime')
            ->andWhere('createdTime <= :endTime')
            ->andWhere('categoryId = :categoryId')
            ->andWhere('smallPicture = :smallPicture')
            ->andWhere('categoryId IN ( :categoryIds )')
            ->andWhere('vipLevelId IN ( :vipLevelIds )')
            ->andWhere('parentId = :parentId')
            ->andWhere('parentId > :parentId_GT')
            ->andWhere('parentId IN ( :parentIds )')
            ->andWhere('id NOT IN ( :excludeIds )')
            ->andWhere('id IN ( :courseIds )')
            ->andWhere('locked = :locked')
            ->andWhere('lessonNum > :lessonNumGT')
            ->andWhere('schoolId = :schoolId')
            ->andWhere('areaCode = :areaCode')
            ->andWhere('certificationId = :certificationId')
            ->andWhere('certificationId != :NoCertificationId')
            ->andWhere('certificationId IN (:certificationIds)')
            ->andWhere('versionStatus = (:versionStatus)')
            ->andWhere('partIndex = (:partIndex)')
            ->andWhere('learnModeMonths = (:learnModeMonths)');

        if (isset($conditions['tagIds'])) {
            $tagIds = $conditions['tagIds'];

            foreach ($tagIds as $key => $tagId) {
                $conditions['tagIds_'.$key] = '%|'.$tagId.'|%';
                $builder->andWhere('tags LIKE :tagIds_'.$key);
            }

            unset($conditions['tagIds']);
        }

        return $builder;
    }

    private function fetchCertificationCourseResult($sql, $userIdOrAreaCode, $courseId = null)
    {
        $params = array($userIdOrAreaCode, 'certification');

        if (!empty($courseId)) {
            $sql .= ' and course.id = ?';
            array_push($params, $courseId);
        }

        $sql .= ' and course.status = ?';
        array_push($params, 'published');

        return $this->getConnection()->fetchAll($sql, $params);
    }
}
