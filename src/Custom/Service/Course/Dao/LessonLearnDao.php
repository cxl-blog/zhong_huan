<?php

namespace Custom\Service\Course\Dao;

interface LessonLearnDao
{
    public function deleteUserCourseLessonLearns($userId, $courseId, $currentLoopTime = 1);

    public function getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime = 1);

    public function findLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime = 1);

    public function findLearnsByUserIdAndCourseIdAndStatus($userId, $courseId, $status, $currentLoopTime = 1);

    public function findLearnsByUserId($userId);
}
