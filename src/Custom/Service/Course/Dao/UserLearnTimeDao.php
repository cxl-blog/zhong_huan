<?php

namespace Custom\Service\Course\Dao;

interface UserLearnTimeDao
{
    public function getUserLearnTime($userId, $certificationId);

    public function addUserLearnTime($fields);

    public function increaseLearnTime($userId, $certificationId, $addedTime);

    public function clearLearnTime();

    public function clearSingleLearnTime($userId);
}
