<?php
namespace Custom\Service\Course\Dao;

interface LastLessonViewDao
{
    public function getLastLessonView($id);

    public function addLastLessonView($fields);

    public function updateLastLessonView($id, $fields);

    public function getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime);
}
