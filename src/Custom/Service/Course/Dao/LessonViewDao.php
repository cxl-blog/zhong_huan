<?php

namespace Custom\Service\Course\Dao;

interface LessonViewDao
{
    public function deleteUserCourseLessonViews($userId, $courseId, $currentLoopTime = 1);

    public function getSumWatchTimeGroupByLesson($userId, $courseId, $currentLoopTime = 1);
}
