<?php

namespace Custom\Serivce\Certificate\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class CourseServiceTest extends BaseTestCase
{

    public function testMarkAsAtLeastOneLessonFinished()
    {
        $this->mock('Custom:Course.CourseMemberDao');
        $this->getMemberDao()->shouldReceive('getMemberByCourseIdAndUserId')->andReturn(
            array(
                'id' => 1,
                'isAtLeastOneLessonFinished' => 0
            )
        );
        $this->getMemberDao()->shouldReceive('updateMember');
        $this->getCourseService()->markAsAtLeastOneLessonFinished(1, 1, 1);

        $this->getMemberDao()->shouldHaveReceived('getMemberByCourseIdAndUserId');
        $this->getMemberDao()->shouldHaveReceived('updateMember');
    }

    protected function getMemberDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.CourseMemberDao');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

}
