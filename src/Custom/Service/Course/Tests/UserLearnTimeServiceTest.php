<?php

namespace Custom\Serivce\Certificate\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class UserLearnTimeServiceTest extends BaseTestCase
{

    public function testAddUserLearnTimeIfNeed()
    {
        $userId = 1137;
        $certificationId = 11;
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed($userId, $certificationId);
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($userId, $certificationId);
        $this->assertEquals($userId, $result['userId']);
    }

    public function testIsLearnTimeExceeding()
    {
        $mockedCourse = array(
            'areaCode' => '001',
            'certificationId' => 1
        );
        $mockedAreaCertification = array(
            'maxLearningSeconds' => 3600,
            'maxLearningMinutes' => 60,
            'sequentialLearning' => 1
        );

        $this->mock('Custom:Course.CourseService');
        $this->mock('Custom:Certificate.CertificateService');

        $this->getCourseService()->shouldReceive('getCourse')->andReturn($mockedCourse);
        $this->getCertificateService()->shouldReceive('findByAreaCodeAndCertificationId')
                ->andReturn($mockedAreaCertification);

        $userId = 1137;
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed($userId, $mockedCourse['certificationId']);
        $learnTimeInfo = $this->getUserLearnTimeService()->getCurrentLearnTimeInfo($userId, 1);
        $this->assertFalse($learnTimeInfo['isExceeding']);
        $this->assertEquals(3600, $learnTimeInfo['maxLearningSeconds']);
        $this->assertEquals(0, $learnTimeInfo['learnedSeconds']);

        for ($i=0; $i < 36; $i++) { 
            $this->getUserLearnTimeService()->waveUserLearnTime($userId, $mockedCourse['certificationId'], 100);
            $learnTimeInfo = $this->getUserLearnTimeService()->getCurrentLearnTimeInfo($userId, 1);
            $this->assertEquals(($i+1) * 100, $learnTimeInfo['learnedSeconds']);
        }
        
        $this->assertTrue($learnTimeInfo['isExceeding']);
    }

    public function testWaveUserLearnTime()
    {
        $userId = 11221;
        $certificationId = 33;
        $firstLearned = 50;
        $secondLearned = 9;
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed($userId, $certificationId);
        $this->getUserLearnTimeService()->waveUserLearnTime($userId, $certificationId, $firstLearned);
        $this->getUserLearnTimeService()->waveUserLearnTime($userId, $certificationId, $secondLearned);
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($userId, $certificationId);
        $this->assertEquals(59, $result['learnedSeconds']);
        $this->assertEquals(1, $result['learnedMinutes']);
    }

    public function testClearUserLearnTime()
    {
        $userId = 11221;
        $learnedSeconds = 50;
        $certificationId = 1123;
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed($userId, $certificationId);
        $this->getUserLearnTimeService()->waveUserLearnTime($userId, $certificationId, $learnedSeconds);
        $this->getUserLearnTimeService()->clearUserLearnTime();
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($userId, $certificationId);
        $this->assertEquals(0, $result['learnedSeconds']);
        $this->assertEquals(0, $result['learnedMinutes']);
    }

    public function testClearSingleUserLearnTime()
    {
        $userId = 11221;
        $certificationId = 11234;
        $learnedSeconds = 50;
        $this->getUserLearnTimeService()->addUserLearnTimeIfNeed($userId, $certificationId);
        $this->getUserLearnTimeService()->waveUserLearnTime($userId, $certificationId, $learnedSeconds);
        $this->getUserLearnTimeService()->clearUserLearnTime($userId);
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($userId, $certificationId);
        $this->assertEquals(0, $result['learnedSeconds']);
        $this->assertEquals(0, $result['learnedMinutes']);
    }

    protected function getUserLearnTimeService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.UserLearnTimeService');
    }

    protected function getUserLearnTimeDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.UserLearnTimeDao');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

}
