<?php

namespace Custom\Serivce\Certificate\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class UserLearnTimeDaoTest extends BaseTestCase
{

    public function testGetUserLearnTime()
    {
        $fields = array(
            'userId' => 223,
            'learnedSeconds' => 60,
            'learnedMinutes' => 1,
            'certificationId' => 2
        );
        $this->getUserLearnTimeDao()->addUserLearnTime($fields);
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($fields['userId'], $fields['certificationId']);
        $this->assertEquals(60, $result['learnedSeconds']);
    }

    public function testIncreaseLearnTime()
    {
        $fields = array(
            'userId' => 225,
            'learnedSeconds' => 60,
            'learnedMinutes' => 1,
            'certificationId' => 3
        );
        $this->getUserLearnTimeDao()->addUserLearnTime($fields);

        $increasedTime = 59;
        $this->getUserLearnTimeDao()->increaseLearnTime($fields['userId'], $fields['certificationId'], $increasedTime);

        $result = $this->getUserLearnTimeDao()->getUserLearnTime($fields['userId'], $fields['certificationId']);
        $this->assertEquals(119, $result['learnedSeconds']);
        $this->assertEquals(2, $result['learnedMinutes']);
    }

    public function testClearLearnTime()
    {
        $fields = array(
            'userId' => 226,
            'learnedSeconds' => 60,
            'learnedMinutes' => 1,
            'certificationId' => 4
        );
        $this->getUserLearnTimeDao()->addUserLearnTime($fields);
        $this->getUserLearnTimeDao()->clearLearnTime();
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($fields['userId'], $fields['certificationId']);
        $this->assertEquals(0, $result['learnedSeconds']);
        $this->assertEquals(0, $result['learnedMinutes']);
    }

    public function testClearSingleLearnTime()
    {
        $fields = array(
            'userId' => 227,
            'learnedSeconds' => 60,
            'learnedMinutes' => 1,
            'certificationId' => 5
        );
        $this->getUserLearnTimeDao()->addUserLearnTime($fields);
        $this->getUserLearnTimeDao()->clearLearnTime();
        $result = $this->getUserLearnTimeDao()->getUserLearnTime($fields['userId'], $fields['certificationId']);
        $this->assertEquals(0, $result['learnedSeconds']);
        $this->assertEquals(0, $result['learnedMinutes']);
    }

    protected function getUserLearnTimeDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.UserLearnTimeDao');
    }

}
