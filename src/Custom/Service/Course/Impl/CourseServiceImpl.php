<?php

namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Topxia\Service\Common\ServiceEvent;
use Custom\Service\Course\CourseService;
use Custom\Common\Constants\DbValue;
use Custom\Common\Constants\LearnMode;
use Custom\Common\Constants\CertCourseStatus;
use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\Course\Impl\CourseServiceImpl as TopxiaCourseServiceImpl;

class CourseServiceImpl extends TopxiaCourseServiceImpl implements CourseService
{
    public function updateLesson($courseId, $lessonId, $fields)
    {
        $argument = $fields;
        $course   = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createServiceException("课程(#{$courseId})不存在！");
        }

        $lesson = $this->getCourseLesson($courseId, $lessonId);

        if (empty($lesson)) {
            throw $this->createServiceException("课时(#{$lessonId})不存在！");
        }

        $fields = ArrayToolkit::filter($fields, array(
            'title'         => '',
            'summary'       => '',
            'content'       => '',
            'media'         => array(),
            'mediaId'       => 0,
            'number'        => 0,
            'seq'           => 0,
            'chapterId'     => 0,
            'free'          => 0,
            'length'        => 0,
            'startTime'     => 0,
            'giveCredit'    => 0,
            'requireCredit' => 0,
            'homeworkId'    => 0,
            'exerciseId'    => 0,
            'testMode'      => 'normal',
            'testStartTime' => 0,
            'suggestHours'  => '0.0',
            'replayStatus'  => 'ungenerated'
        ));

        if (isset($fields['title'])) {
            $fields['title'] = $this->purifyHtml($fields['title']);
        }

        $fields['type'] = $lesson['type'];

        if ($fields['type'] == 'live' && isset($fields['startTime'])) {
            $fields['endTime']      = $fields['startTime'] + $fields['length'] * 60;
            $fields['suggestHours'] = $fields['length'] / 60;
        }

        if (array_key_exists('media', $fields)) {
            $this->fillLessonMediaFields($fields);
        }

        $updatedLesson = LessonSerialize::unserialize(
            $this->getLessonDao()->updateLesson($lessonId, LessonSerialize::serialize($fields))
        );

        $this->updateCourseCounter($course['id'], array(
            'giveCredit' => $this->getLessonDao()->sumLessonGiveCreditByCourseId($course['id'])
        ));

        // Update link count of the course lesson file, if the lesson file is changed
        if (array_key_exists('mediaId', $fields) && $fields['mediaId'] != $lesson['mediaId']) {
            // Incease the link count of the new selected lesson file
            if (!empty($fields['mediaId'])) {
                $this->getUploadFileService()->waveUploadFile($fields['mediaId'], 'usedCount', 1);
            }

            // Decrease the link count of the original lesson file
            if (!empty($lesson['mediaId'])) {
                $this->getUploadFileService()->waveUploadFile($lesson['mediaId'], 'usedCount', -1);
            }
        }

        $this->getLogService()->info('course', 'update_lesson', "更新课时《{$updatedLesson['title']}》({$updatedLesson['id']})", $updatedLesson);

        $updatedLesson['fields'] = $lesson;
        $this->dispatchEvent("course.lesson.update", array('argument' => $argument, 'lesson' => $updatedLesson));

        return $updatedLesson;
    }

    public function getCertificationCourseLearnedPercent($sumHours, $learnedHours)
    {
        if ($sumHours == '0') {
            $learnPercent = '0%';
        } else {
            $learnPercent = round($learnedHours / $sumHours * 100, 2).'%';
        }

        return $learnPercent;
    }

    public function getSumHours($items)
    {
        $sum = 0;

        foreach ($items as $value) {
            if ($this->hasSuggestHours($value['type']) && $value['status']=='published') {
                $sum += round($value['suggestHours'], 0);
            }
        }

        return $sum;
    }

    public function getSumWatchTimeGroupByLesson($userId, $courseId, $currentLoopTime = 1)
    {
        return $this->getLessonViewDao()->getSumWatchTimeGroupByLesson($userId, $courseId, $currentLoopTime);
    }

    public function setCoursesPriceWithDiscount($discountId)
    {
        $setting = $this->getSettingService()->get('coin');

        if (!empty($setting['coin_enabled']) && !empty($setting['price_type']) && strtolower($setting['price_type']) == 'coin') {
            $currency = 'coin';
        } else {
            $currency = 'default';
        }

        $discount = $this->getDiscountService()->getDiscount($discountId);

        if (empty($discount)) {
            throw $this->createServiceException("折扣活动#{#discountId}不存在！");
        }

        if ($discount['type'] == 'global') {
            $conditions = array();
            $conditions['originPrice_GT'] = '0.00';
            $conditions['type'] = 'all';

            $count   = $this->searchCourseCount($conditions);
            $courses = $this->searchCourses($conditions, 'latest', 0, $count);

            foreach ($courses as $course) {
                $fields = array(
                    'price' => $course['originPrice'] * $discount['globalDiscount'] / 10
                );
                $fields['discountId'] = $discount['id'];
                $fields['discount']   = $discount['globalDiscount'];

                $this->getCourseDao()->updateCourse($course['id'], $fields);
            }
        } else {
            $count     = $this->getDiscountService()->findItemsCountByDiscountId($discountId);
            $items     = $this->getDiscountService()->findItemsByDiscountId($discountId, 0, $count);
            $courseIds = ArrayToolkit::column($items, 'targetId');
            $courses   = $this->findCoursesByIds($courseIds);

            foreach ($items as $item) {
                if (empty($courses[$item['targetId']])) {
                    continue;
                }

                $course = $courses[$item['targetId']];
                $fields = array(
                    'price' => $course['originPrice'] * $item['discount'] / 10
                );
                $fields['discountId'] = $discount['id'];
                $fields['discount']   = $item['discount'];

                $this->getCourseDao()->updateCourse($course['id'], $fields);
            }
        }
    }

    public function  findCoursesByCertificationIdAndAreaCode($certificationId,$areaCode)
    {
        return $this->getCourseDao()->findCoursesByCertificationIdAndAreaCode($certificationId,$areaCode);
    }

    public function getOfflineFinishedCourseByDid($dId)
    {
        return $this->getOfflineFinishedCourseDao()->getOfflineFinishedCourseByDid($dId);
    }

    public function getOfflineFinishedCoursesByUserId($userId)
    {
        return $this->getOfflineFinishedCourseDao()->getOfflineFinishedCoursesByUserId($userId);
    }

    public function createOfflineFinishedCourse($offlineFinishedCourse)
    {
        $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
            array(
                'userId' => $offlineFinishedCourse['userId'],
                'courseId' => $offlineFinishedCourse['courseId'],
                'currentLoopTime' => $offlineFinishedCourse['currentLoopTime'],
                'isRevoked' => DbValue::FALSE
            ),
            array(
                'isCompletedOffline' => DbValue::TRUE,
                'status' => CertCourseStatus::COMPLETION
            )
        );
        $this->getOfflineFinishedCourseDao()->createOfflineFinishedCourse($offlineFinishedCourse);
    }

    public function deleteOfflineFinishedCoursesByDid($dId)
    {
        $this->getOfflineFinishedCourseDao()->deleteOfflineFinishedCoursesByDid($dId);
    }

    public function deleteOfflineFinishedCourses($userId, $certificationId)
    {
        $this->getOfflineFinishedCourseDao()->deleteOfflineFinishedCourses($userId, $certificationId);
    }

    public function getLearnedHours($items, $userId, $currentLoopTime = null)
    {
        $learnedHours = 0;

        foreach ($items as $value) {
            if ($this->hasSuggestHours($value['type']) && $this->isLessionFinished($userId, $value, $currentLoopTime)) {
                $learnedHours += round($value['suggestHours'], 0);
            }
        }

        return $learnedHours;
    }

    public function getUserLearnLessonStatuses($userId, $courseId, $currentLoopTime = 1)
    {
        $learns = $this->getLessonLearnDao()->findLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime) ?: array();

        $statuses = array();

        foreach ($learns as $learn) {
            $statuses[$learn['lessonId']] = $learn['status'];
        }

        return $statuses;
    }

    public function findUserLearnedLessons($userId, $courseId, $currentLoopTime = null)
    {
        return ArrayToolkit::index($this->getLessonLearnDao()->findLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime) ?: array(), 'lessonId');
    }

    public function getChapterSuggestedHours($items, $isAdmin, $userId, $currentLoopTime = null)
    {
        $chapterHour = array();
        foreach ($items as $value) {
            if ($value['type'] == 'chapter') {
                $chapterHour = $this->updateChapterAndUnitHours($value, $items, $chapterHour, $isAdmin, $userId, $currentLoopTime);
            }
        }

        return $chapterHour;
    }

    public function getUnitSuggestedHours($items, $isAdmin, $userId, $currentLoopTime = null)
    {
        $unitHour = array(array());

        foreach ($items as $value) {
            $sum = '0';

            if ($value['type'] == 'unit' && !$isAdmin) {
                foreach ($items as $compareKey => $comparedValue) {
                    if ($this->hasSuggestHours($comparedValue['type']) && $comparedValue['chapterId'] == $value['id'] && $this->isLessionFinished($userId, $comparedValue, $currentLoopTime)) {
                        $sum += round($comparedValue['suggestHours'], 0);

                        $unitHour[$value['parentId']][$value['number']] = $sum;
                    }

                    $unitHour[$value['parentId']][$value['number']] = $sum;
                }
            } elseif ($value['type'] == 'unit' && $isAdmin) {
                foreach ($items as $compareKey => $comparedValue) {
                    if ($this->hasSuggestHours($comparedValue['type']) && $comparedValue['chapterId'] == $value['id']) {
                        $sum += round($comparedValue['suggestHours'], 0);

                        $unitHour[$value['parentId']][$value['number']] = $sum;
                    }

                    $unitHour[$value['parentId']][$value['number']] = $sum;
                }
            }
        }
        return $unitHour;
    }

    public function getTextSuggestedHours($items, $userId, $currentLoopTime = null)
    {
        $textHour = array();

        foreach ($items as $value) {
            if ($this->hasSuggestHours($value['type']) && $this->isLessionFinished($userId, $value, $currentLoopTime)) {
                $textHour[$value['id']] = round($value['suggestHours'], 0);
            } else {
                $textHour[$value['id']] = '0';
            }
        }

        return $textHour;
    }

    public function getTextSuggestedHoursForAdmin($items, $userId)
    {
        $textHour = array();

        foreach ($items as $value) {
            if ($this->hasSuggestHours($value['type'])) {
                $textHour[$value['id']] = round($value['suggestHours'], 0);
            }
        }

        return $textHour;
    }

    public function getCourse($id, $inChanging = false)
    {
        $course = CourseSerialize::unserialize($this->getCourseDao()->getCourse($id));
        return LearnMode::formatSingleCertificationType($course);
    }

    public function getCertificationOrNormalCourse($userId, $id, $currentLoopTime = 1)
    {
        $course = CourseSerialize::unserialize($this->getCourseDao()->getCourse($id));

        if ($course['type'] == 'certification') {
            $result = $this->getCertificateService()->getCertificationsForUser($userId, false, $id, $currentLoopTime);
            $result = ArrayToolkit::copyToAnotherArray($course, $result, array('goals', 'audiences', 'vipLevelId', 'about'));
            return $result;
        } else {
            return $course;
        }
    }

    public function canTakeCourse($course, $currentLoopTime = 1)
    {
        $course = !is_array($course) ? $this->getCourse(intval($course)) : $course;

        if (empty($course)) {
            return false;
        }

        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            return false;
        }

        if (count(array_intersect($user['roles'], array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) > 0) {
            return true;
        }

        if (!empty($this->getBranchSchoolService()->getFirstAdminedBranchSchool($user['id']))) {
            return true;
        }

        if ($course['parentId'] && $this->isClassroomMember($course, $user['id'])) {
            return true;
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($course['id'], $user['id'], $currentLoopTime);

        if ($member && in_array($member['role'], array('teacher', 'student'))) {
            return true;
        }

        return false;
    }

    public function canManageCourse($courseId, $currentLoopTime = null)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        $course = $this->getCourse($courseId);

        if (empty($course)) {
            return $user->isAdmin();
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $user->id, $currentLoopTime);

        if ($member && ($member['role'] == 'teacher')) {
            return true;
        }

        return false;
    }

    public function tryTakeCourse($courseId, $currentLoopTime = null)
    {
        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            throw $this->createAccessDeniedException('您尚未登录用户，请登录后再查看！');
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $user['id'], $currentLoopTime);

        if (count(array_intersect($user['roles'], array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) > 0) {
            return array($course, $member);
        }

        if (!empty($this->getBranchSchoolService()->getFirstAdminedBranchSchool($user['id']))) {
            return array($course, $member);
        }

        if (empty($member) && $this->isClassroomMember($course, $user['id']) &&
                !$this->isCourseTeacher($course['id'], $user['id']) && !$this->isCourseStudent($course['id'], $user['id'])) {
            $member = $this->becomeStudentByClassroomJoined($course['id'], $user['id']);
            return array($course, $member);
        }

        if (empty($member) || !in_array($member['role'], array('teacher', 'student'))) {
            throw $this->createAccessDeniedException('您不是课程学员，不能查看课程内容，请先购买课程！');
        }

        return array($course, $member);
    }

    public function tryManageCourse($courseId)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            throw $this->createAccessDeniedException('未登录用户，无权操作！');
        }

        $course = $this->getCourseDao()->getCourse($courseId);

        $branchAdminMangeCourse = $this->canBranchAdminMangeCourse($courseId);

        if ($branchAdminMangeCourse) {
            return $branchAdminMangeCourse;
        }

        if (empty($course)) {
            throw $this->createNotFoundException();
        }

        if (!$this->hasCourseManagerRole($courseId, $user['id'])) {
            throw $this->createAccessDeniedException('您不是课程的教师或管理员，无权操作！');
        }

        return CourseSerialize::unserialize($course);
    }

    public function tryAdminCourse($courseId)
    {
        $course = $this->getCourseDao()->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if (empty($user->id)) {
            throw $this->createAccessDeniedException('未登录用户，无权操作！');
        }

        if (!$this->canBranchAdminMangeCourse($courseId) && count(array_intersect($user['roles'], array('ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) == 0) {
            throw $this->createAccessDeniedException('您不是管理员，无权操作！');
        }

        return CourseSerialize::unserialize($course);
    }

    public function canBranchAdminMangeCourse($courseId)
    {
        $course = $this->getCourseDao()->getCourse($courseId);

        if (!isset($course['schoolId']) && !isset($course['areaCode'])) {
            return false;
        }

        if ($course['schoolId']) {
            $schoolId = $course['schoolId'];
        } elseif ($course['areaCode']) {
            $area     = $this->getAreaService()->getAreaByCode($course['areaCode']);
            $schoolId = $area['schoolId'];
        } else {
            $schoolId = 0;
        }

        if (!$schoolId) {
            return false;
        }

        $user = $this->getCurrentUser();

        if ($this->canBranchAdminMangeSchool($user['id'], $schoolId)) {
            return CourseSerialize::unserialize($course);
        }

        return false;
    }

    public function canBranchAdminArea($userId, $areaCode)
    {
        $area = $this->getAreaService()->getAreaByCode($areaCode);
        return $this->canBranchAdminMangeSchool($userId,$area['schoolId']);
    }

    public function canBranchAdminMangeSchool($userId, $schoolId)
    {
        $conditions['userId']   = $userId;
        $conditions['schoolId'] = $schoolId;
        $schoolUser             = $this->getBranchSchoolService()->searchBranchSchoolUsers($conditions, ['id', 'desc'], 0, 1);

        return !empty($schoolUser) && count(array_intersect($schoolUser[0]['roles'], array('ROLE_ADMIN'))) > 0;
    }

    public function lockStudent($courseId, $userId, $currentLoopTime = null)
    {
        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException("课程(#${$courseId})不存在，封锁学员失败。");
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);

        if (empty($member) || ($member['role'] != 'student')) {
            throw $this->createServiceException("用户(#{$userId})不是课程(#{$courseId})的学员，封锁学员失败。");
        }

        if ($member['locked']) {
            return;
        }

        $this->getMemberDao()->updateMember($member['id'], array('locked' => 1));
    }

    public function unlockStudent($courseId, $userId, $currentLoopTime = null)
    {
        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException("课程(#${$courseId})不存在，封锁学员失败。");
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);

        if (empty($member) || ($member['role'] != 'student')) {
            throw $this->createServiceException("用户(#{$userId})不是课程(#{$courseId})的学员，解封学员失败。");
        }

        if (empty($member['locked'])) {
            return;
        }

        $this->getMemberDao()->updateMember($member['id'], array('locked' => 0));
    }

    public function updateCourse($id, $fields)
    {
        $argument = $fields;
        $course   = $this->getCourseDao()->getCourse($id);

        if (empty($course)) {
            throw $this->createServiceException('课程不存在，更新失败！');
        }

        $fields = $this->_filterCourseFields($fields);

        $this->getLogService()->info('course', 'update', "更新课程《{$course['title']}》(#{$course['id']})的信息", $fields);

        $fields = CourseSerialize::serialize($fields);

        $updatedCourse = $this->getCourseDao()->updateCourse($id, $fields);

        $this->dispatchEvent("course.update", array('argument' => $argument, 'course' => $updatedCourse));

        return CourseSerialize::unserialize($updatedCourse);
    }

    public function getCertificationId($courseId)
    {
        return $this->getCourseDao()->getCertificationId($courseId);
    }

    public function getUserLearnLessonStatus($userId, $courseId, $lessonId, $currentLoopTime = null)
    {
        $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime);

        if (empty($learn)) {
            return null;
        }

        return $learn['status'];
    }

    public function startLearnLesson($courseId, $lessonId, $currentLoopTime = null, $ignoreCreateLessonView = false)
    {
        list($course, $member) = $this->tryTakeCourse($courseId, $currentLoopTime);
        $user                  = $this->getCurrentUser();

        $result['status']     = false;
        $result['lessonView'] = '';

        $lessonLearnStatus = $this->getUserLearnLessonStatus($user['id'], $courseId, $lessonId, $currentLoopTime);

        if ($lessonLearnStatus == 'finished') {
            return $result;
        }

        $lesson = $this->getCourseLesson($courseId, $lessonId);

        if (!empty($lesson)) {
            if ($lesson['type'] == 'video' && !$ignoreCreateLessonView) {
                $createLessonView['courseId'] = $courseId;
                $createLessonView['lessonId'] = $lessonId;
                $createLessonView['fileId']   = $lesson['mediaId'];

                $viewDevice                     = $this->getDevice();
                $createLessonView['viewDevice'] = $viewDevice;
                $createLessonView['osInfo'] = $this->getClientOs();

                $file = array();

                if (!empty($createLessonView['fileId'])) {
                    $file = $this->getUploadFileService()->getFile($createLessonView['fileId']);
                }

                $createLessonView['fileStorage']     = empty($file) ? "net" : $file['storage'];
                $createLessonView['fileType']        = $lesson['type'];
                $createLessonView['fileSource']      = $lesson['mediaSource'];
                $createLessonView['currentLoopTime'] = $currentLoopTime;

                $result['lessonView'] = $this->createLessonView($createLessonView);
            }

            $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($user['id'], $lessonId, $currentLoopTime);

            if ($learn) {
                return $result;
            }

            $learn = $this->getLessonLearnDao()->addLearn(array(
                'userId'          => $user['id'],
                'courseId'        => $courseId,
                'lessonId'        => $lessonId,
                'status'          => 'learning',
                'startTime'       => time(),
                'finishedTime'    => 0,
                'currentLoopTime' => $currentLoopTime

            ));

            $this->dispatchEvent(
                'course.lesson_start',
                new ServiceEvent($lesson, array('course' => $course, 'learn' => $learn))
            );
            $result['status'] = true;
            return $result;
        }

        return $result;
    }

    public function createLessonView($createLessonView)
    {
        $createLessonView                = ArrayToolkit::parts($createLessonView, array('courseId', 'lessonId', 'fileId', 'fileType', 'fileStorage', 'fileSource', 'viewDevice', 'osInfo', 'currentLoopTime'));
        $createLessonView['userId']      = $this->getCurrentUser()->id;
        $createLessonView['createdTime'] = time();

        $lessonView = $this->getLessonViewDao()->addLessonView($createLessonView);

        $this->updateLastLessonView($createLessonView['userId'], $createLessonView['courseId'], $createLessonView['lessonId'], $createLessonView['currentLoopTime'], time());

        $lesson = $this->getCourseLesson($createLessonView['courseId'], $createLessonView['lessonId']);

        $this->getLogService()->info('course', 'create', "{$this->getCurrentUser()->nickname}观看课时《{$lesson['title']}》");

        return $lessonView;
    }

    public function finishLearnLesson($courseId, $lessonId, $currentLoopTime = null)
    {
        list($course, $member) = $this->tryLearnCourse($courseId, $currentLoopTime);

        $lesson = $this->getCourseLesson($courseId, $lessonId);

        if (empty($lesson)) {
            throw $this->createServiceException("课时#{$lessonId}不存在！");
        }

        $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($member['userId'], $lessonId, $currentLoopTime);

        if ($learn) {
            $learn = $this->getLessonLearnDao()->updateLearn($learn['id'], array(
                'status'       => 'finished',
                'finishedTime' => time()
            ));
        } else {
            $learn = $this->getLessonLearnDao()->addLearn(array(
                'userId'          => $member['userId'],
                'courseId'        => $courseId,
                'lessonId'        => $lessonId,
                'status'          => 'finished',
                'startTime'       => time(),
                'finishedTime'    => time(),
                'currentLoopTime' => $currentLoopTime
            ));
        }

        $learns = $this->getLessonLearnDao()->findLearnsByUserIdAndCourseIdAndStatus($member['userId'], $course['id'], 'finished', $currentLoopTime);

        $totalCredits = $this->getLessonDao()->sumLessonGiveCreditByLessonIds(ArrayToolkit::column($learns, 'lessonId'));

        $memberFields               = array();
        $memberFields['learnedNum'] = count($learns);

        if ($course['serializeMode'] != 'serialize') {
            $memberFields['isLearned'] = $memberFields['learnedNum'] >= $course['lessonNum'] ? 1 : 0;
        }

        $memberFields['credit'] = $totalCredits;
        $memberFields = $this->fillPercentInfoWithMarkAllLessonsFinished($memberFields, $member['userId'], $course['id'], $currentLoopTime);

        $this->getMemberDao()->updateMember($member['id'], $memberFields);

        $this->dispatchEvent(
            'course.lesson_finish',
            new ServiceEvent($lesson, array('course' => $course, 'learn' => $learn, 'currentLoopTime' => $currentLoopTime))
        );
    }

    public function fillPercentInfoWithMarkAllLessonsFinished($memberFields, $userId, $courseId, $currentLoopTime)
    {
        $items  = $this->getCourseItems($courseId);
        $memberFields['learnedHours'] = $this->getLearnedHours($items, $userId, $currentLoopTime);
        $memberFields['isAllLessonsFinished'] = $this->isAllLessonsFinished($items, $userId, $currentLoopTime);
        return $memberFields;
    }

    public function tryLearnCourse($courseId, $currentLoopTime = null)
    {
        $course = $this->getCourseDao()->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if (empty($user)) {
            throw $this->createAccessDeniedException('未登录用户，无权操作！');
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $user['id'], $currentLoopTime);

        if (empty($member) || !in_array($member['role'], array('admin', 'teacher', 'student'))) {
            throw $this->createAccessDeniedException('您不是课程学员，不能学习！');
        }

        return array($course, $member);
    }

    public function getCourseMember($courseId, $userId, $currentLoopTime = null)
    {
        return $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);
    }

    public function getFirstCourseMember($userId, $courseId)
    {
        return $this->getMemberDao()->getFirstMemberByCourseIdAndUserId($courseId, $userId);
    }

    public function updateAsLatestCourse($courseId)
    {
        $course  = $this->updateCourse($courseId, array('versionStatus' => 'latest'));
        $oldCourseIds = $this->updateOtherCoursesAsOld($course);
        $this->getCertificateService()->updateNotBuyUserCertCourse($course);
        return $oldCourseIds;
    }

    public function updateOtherCoursesAsOld($latestCourse)
    {
        $conditions['type']     = "certification";
        $conditions['parentId'] = 0;
        $conditions['areaCode'] = $latestCourse['areaCode'];
        $conditions['learnModeMonths'] = $latestCourse['learnModeMonths'];
        $conditions['certificationId'] = $latestCourse['certificationId'];
        $conditions['partIndex'] = $latestCourse['partIndex'];

        $oldCourseIds = array();
        $allCourses  = $this->searchCourses($conditions, null, 0, PHP_INT_MAX);
        foreach ($allCourses as $allCourse) {
            if ($allCourse['id'] == $latestCourse['id']) {
                continue;
            }

            $this->updateCourse($allCourse['id'], array("versionStatus" => "old"));
            array_push($oldCourseIds, $allCourse['id']);
        }

        return $oldCourseIds;
    }

    public function getUserNextLearnLesson($userId, $courseId, $currentLoopTime = null)
    {
        $lessonIds = $this->getLessonDao()->findLessonIdsByCourseId($courseId);

        $learns = $this->getLessonLearnDao()->findLearnsByUserIdAndCourseIdAndStatus($userId, $courseId, 'finished', $currentLoopTime);

        $learnedLessonIds = ArrayToolkit::column($learns, 'lessonId');

        $unlearnedLessonIds = array_diff($lessonIds, $learnedLessonIds);
        $nextLearnLessonId  = array_shift($unlearnedLessonIds);

        if (empty($nextLearnLessonId)) {
            return null;
        }

        return $this->getLessonDao()->getLesson($nextLearnLessonId);
    }

    public function waveLearningTime($userId, $lessonId, $time, $currentLoopTime = null)
    {
        $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime);

        if ($time <= 200) {
            $this->getLessonLearnDao()->updateLearn($learn['id'], array(
                'learnTime' => $learn['learnTime'] + intval($time)
            ));
        }
    }

    public function waveWatchingTime($userId, $lessonId, $time, $currentLoopTime = null)
    {
        $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime);

        if ($time <= 200) {
            $learn = $this->getLessonLearnDao()->updateLearn($learn['id'], array(
                'watchTime'  => $learn['watchTime'] + intval($time),
                'updateTime' => time()
            ));

            if ($learn['status'] != 'finished') {
                $course = $this->getCourse($learn['courseId']);
                $this->getUserLearnTimeService()->waveUserLearnTime($userId, $course['certificationId'], $time);
                $currentLearnTimeInfo = $this->getUserLearnTimeService()->getCurrentLearnTimeInfo(
                    $userId, 
                    $learn['courseId']
                );
                $learn = array_merge($learn, $currentLearnTimeInfo);
            }
        }

        return $learn;
    }

    public function removeStudent($courseId, $userId, $currentLoopTime = null)
    {
        $course                    = $this->getCourse($courseId);
        $course['currentLoopTime'] = $currentLoopTime;

        if (empty($course)) {
            throw $this->createNotFoundException("课程(#${$courseId})不存在，退出课程失败。");
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);

        if (empty($member) || ($member['role'] != 'student')) {
            throw $this->createServiceException("用户(#{$userId})不是课程(#{$courseId})的学员，退出课程失败。");
        }

        $this->getMemberDao()->deleteMember($member['id']);

        $this->getCourseDao()->updateCourse($courseId, array(
            'studentNum' => $this->getCourseStudentCount($courseId)
        ));

        $this->getLogService()->info('course', 'remove_student', "课程《{$course['title']}》(#{$course['id']})，移除学员#{$member['id']}");
        $this->dispatchEvent(
            'course.quit',
            new ServiceEvent($course, array('userId' => $member['userId']))
        );

        $userCertCourse = $this->getUserCertCourseDao()->getUserCertCourse($userId, $courseId, $currentLoopTime);
        if (time() < $userCertCourse['deadlineNum']) {
            $userCertCourse['status'] = CertCourseStatus::UNPAID;
        } else {
            $userCertCourse['status'] = CertCourseStatus::OVERTIME;
        }

        $userCertCourse['latestBuyTime'] = 0;
        $courseMember = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);
        if (isset($courseMember)) {
            $userCertCourse['latestBuyTime'] = $courseMember['createdTime'];
        }

        $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
            array(
                'userId' => $userId,
                'courseId' => $courseId,
                'currentLoopTime' => $currentLoopTime
            ),
            $userCertCourse
        );
    }

    protected function getDevice()
    {
        $useragent = Request::createFromGlobals()->server->get('HTTP_USER_AGENT');

        if (preg_match('/(android|bb\d+|meego).+mobile|ipad|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            return 'mobile';
        } else {
            return 'desktop';
        }
    }

    protected function _filterCourseFields($fields)
    {
        $fields = ArrayToolkit::filter($fields, array(

            'title'           => '',
            'subtitle'        => '',
            'about'           => '',
            'expiryDay'       => 0,
            'serializeMode'   => 'none',
            'categoryId'      => 0,
            'vipLevelId'      => 0,
            'goals'           => array(),
            'audiences'       => array(),
            'tags'            => '',
            'startTime'       => 0,
            'endTime'         => 0,
            'locationId'      => 0,
            'address'         => '',
            'maxStudentNum'   => 0,
            'watchLimit'      => 0,
            'approval'        => 0,
            'maxRate'         => 0,
            'locked'          => 0,
            'tryLookable'     => 0,
            'tryLookTime'     => 0,
            'schoolId'        => 0,
            'buyable'         => 0,
            'partIndex'     => 0,
            'areaCode'        => '',
            'certificationId' => 0,
            'versionStatus'   => '',
            'learnModeMonths'  => 0

        ));

        if (!empty($fields['about'])) {
            $fields['about'] = $this->purifyHtml($fields['about'], true);
        }

        if (!empty($fields['tags'])) {
            $fields['tags'] = explode(',', $fields['tags']);
            $fields['tags'] = $this->getTagService()->findTagsByNames($fields['tags']);
            array_walk($fields['tags'], function (&$item, $key) {
                $item = (int) $item['id'];
            }

            );
        }

        return $fields;
    }

    protected function hasCourseManagerRole($courseId, $userId)
    {
        if ($this->getUserService()->hasAdminRoles($userId)) {
            return true;
        }

        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId);

        if ($member && ($member['role'] == 'teacher')) {
            return true;
        }

        return false;
    }

    public function searchCourses($conditions, $sort, $start, $limit)
    {
        $conditions = $this->_prepareCourseConditions($conditions);

        if (is_array($sort)) {
            $orderBy = $sort;
        } elseif ($sort == 'popular' || $sort == 'hitNum') {
            $orderBy = array('hitNum', 'DESC');
        } elseif ($sort == 'recommended') {
            $orderBy = array('recommendedTime', 'DESC');
        } elseif ($sort == 'Rating') {
            $orderBy = array('Rating', 'DESC');
        } elseif ($sort == 'studentNum') {
            $orderBy = array('studentNum', 'DESC');
        } elseif ($sort == 'recommendedSeq') {
            $orderBy = array('recommendedSeq', 'ASC');
        } elseif ($sort == 'createdTimeByAsc') {
            $orderBy = array('createdTime', 'ASC');
        } else {
            $orderBy = array('createdTime', 'DESC');
        }

        return CourseSerialize::unserializes($this->getCourseDao()->searchCourses($conditions, $orderBy, $start, $limit));
    }

    public function searchCourseCount($conditions)
    {
        $conditions = $this->_prepareCourseConditions($conditions);
        return $this->getCourseDao()->searchCourseCount($conditions);
    }

    public function waveLessonViewWatchingTime($lessonViewId, $time)
    {
        $lessonView = $this->getLessonViewDao()->getLessonView($lessonViewId);

        if (empty($lessonView)) {
            return false;
        }

        $lessonView = $this->getLessonViewDao()->updateLessonView($lessonView['id'], array(
            'watchTime' => $lessonView['watchTime'] + intval($time)
        ));

        $this->updateLastLessonView(
            $lessonView['userId'], 
            $lessonView['courseId'], 
            $lessonView['lessonId'], 
            $lessonView['currentLoopTime'], 
            $lessonView['createdTime'] + $lessonView['watchTime']
        );

        return $lessonView;
    }

    protected function _prepareCourseConditions($conditions)
    {
        $conditions = array_filter($conditions, function ($value) {
            if ($value == 0) {
                return true;
            }

            return !empty($value);
        });

        if (isset($conditions['date'])) {
            $dates = array(
                'yesterday'  => array(
                    strtotime('yesterday'),
                    strtotime('today')
                ),
                'today'      => array(
                    strtotime('today'),
                    strtotime('tomorrow')
                ),
                'this_week'  => array(
                    strtotime('Monday this week'),
                    strtotime('Monday next week')
                ),
                'last_week'  => array(
                    strtotime('Monday last week'),
                    strtotime('Monday this week')
                ),
                'next_week'  => array(
                    strtotime('Monday next week'),
                    strtotime('Monday next week', strtotime('Monday next week'))
                ),
                'this_month' => array(
                    strtotime('first day of this month midnight'),
                    strtotime('first day of next month midnight')
                ),
                'last_month' => array(
                    strtotime('first day of last month midnight'),
                    strtotime('first day of this month midnight')
                ),
                'next_month' => array(
                    strtotime('first day of next month midnight'),
                    strtotime('first day of next month midnight', strtotime('first day of next month midnight'))
                )
            );

            if (array_key_exists($conditions['date'], $dates)) {
                $conditions['startTimeGreaterThan'] = $dates[$conditions['date']][0];
                $conditions['startTimeLessThan']    = $dates[$conditions['date']][1];
                unset($conditions['date']);
            }
        }

        if (isset($conditions['creator']) && !empty($conditions['creator'])) {
            $user                 = $this->getUserService()->getUserByNickname($conditions['creator']);
            $conditions['userId'] = $user ? $user['id'] : -1;
            unset($conditions['creator']);
        }

        if (isset($conditions['categoryId'])) {
            $childrenIds               = $this->getCategoryService()->findCategoryChildrenIds($conditions['categoryId']);
            $conditions['categoryIds'] = array_merge(array($conditions['categoryId']), $childrenIds);
            unset($conditions['categoryId']);
        }

        if (isset($conditions['nickname'])) {
            $user                 = $this->getUserService()->getUserByNickname($conditions['nickname']);
            $conditions['userId'] = $user ? $user['id'] : -1;
            unset($conditions['nickname']);
        }

        if (!isset($conditions['type'])) {
            $conditions['types']     = array('normal', 'live');
        }

        return $conditions;
    }

    public function getUserValideNextLearnLesson($userId, $courseId, $currentLoopTime = null)
    {
        $conditions['courseId'] = $courseId;
        $conditions['status']   = 'published';
        $lessons                = $this->getLessonDao()->searchLessons($conditions, ['seq', 'asc'], 0, 1000);
        $lessonIds              = ArrayToolkit::column($lessons, 'id');

        $learns = $this->getLessonLearnDao()->findLearnsByUserIdAndCourseIdAndStatus($userId, $courseId, 'finished', $currentLoopTime);

        $learnedLessonIds = ArrayToolkit::column($learns, 'lessonId');

        $unlearnedLessonIds = array_diff($lessonIds, $learnedLessonIds);
        $nextLearnLessonId  = array_shift($unlearnedLessonIds);

        if (empty($nextLearnLessonId)) {
            return null;
        }

        return $this->getLessonDao()->getLesson($nextLearnLessonId);
    }

    public function buildCourseLayoutData($request, $id)
    {
        $course = $this->getCourse($id);

        if (empty($course)) {
            throw $this->createNotFoundException("课程不存在");
        }

        $previewAs = $request->query->get('previewAs');
        $user      = $this->getCurrentUser();

        if (!empty($user['roles']) &&
                ($this->getUserService()->hasAdminRoles($user['id']) 
                || $this->canBranchAdminMangeCourse($course['id']))) {
            $member = array(
                'id'          => 0,
                'courseId'    => $course['id'],
                'userId'      => $user['id'],
                'levelId'     => 0,
                'learnedNum'  => 0,
                'isLearned'   => 0,
                'seq'         => 0,
                'isVisible'   => 0,
                'orderId'     => 0,
                'joinedType'  => 'course',
                'role'        => 'teacher',
                'fake'        => true,
                'locked'      => 0,
                'createdTime' => time(),
                'deadline'    => 0,
                'currentLoopTime' => 1
            );
        } else {
            if ($user) {
                $member = $this->getCourseMember(
                    $course['id'], 
                    $user['id'],
                    CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $course['id'])
                );
            } else {
                $member = null;
            }
        }

        if (in_array($previewAs, array('member', 'guest'))) {
            if (empty($member) || $member['role'] != 'teacher') {
                return array($course, $member);
            }

            if ($previewAs == 'member') {
                $member['role'] = 'student';
            } else {
                $member = null;
            }
        }

        return array($course, $member);
    }

    public function completeSchoolCourses($schoolId)
    {
        $conditions['schoolId'] = $schoolId;
        $conditions['role']     = "ROLE_USER";
        $schoolUserCount        = $this->getBranchSchoolService()->searchBranchSchoolUsersCount($conditions);
        $schoolUsers            = $this->getBranchSchoolService()->searchBranchSchoolUsers($conditions, array('id', 'desc'), 0, $schoolUserCount);

        $schooluserIds = ArrayToolkit::column($schoolUsers, 'userId');
        $courseMembers = $this->getMemberDao()->findFinishedCoursesByStudentsIds($schooluserIds);
 
        if (empty($courseMembers)) {
            return false;
        }

        foreach ($courseMembers as $courseMember) {
            $this->completeCourse($courseMember['userId'], $courseMember['courseId'], $courseMember['currentLoopTime']);
        }
    }

    public function passCourse($userId,$courseId,$currentLoopTime = 1, $ignoreIsLearned = false)
    {
        $courseMember = $this->getCourseMember($courseId, $userId, $currentLoopTime);
        
        if (!$ignoreIsLearned && $courseMember['isLearned'] == DbValue::FALSE) {
            return false;
        }

        if ($courseMember['isTestpaperPassed']) {
            return false;
        }

        if ($this->getTestpaperService()->isAllTestResultPassed($userId, $courseId, $currentLoopTime)) {
            $courseMember = $this->getMemberDao()->updateMember($courseMember['id'], 
                array(
                    'isTestpaperPassed' => DbValue::TRUE,
                    'isLearned' => DbValue::TRUE
                )
            );
        }
    }

    public function completeCourse($userId, $courseId, $currentLoopTime = null)
    {
        $isApproved = $this->getUserService()->isUserApprovaled($userId);

        if (!$isApproved) {
            return false;
        }

        $courseMember = $this->getCourseMember($courseId, $userId, $currentLoopTime);

        if (empty($courseMember)) {
            return false;
        }
        if (!$courseMember['isAllLessonsFinished'] || !$courseMember['isTestpaperPassed']) {
            return false;
        }

        if ($courseMember['isCompleted']) {
            return $courseMember;
        }        
        
        $courseMember = $this->getMemberDao()->updateMember($courseMember['id'], array('isCompleted' => DbValue::TRUE));
        $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
            array(
                'userId' => $userId,
                'courseId' => $courseId,
                'currentLoopTime' => $currentLoopTime
            ),
            array(
                'status' => CertCourseStatus::COMPLETION
            )
        );
        $this->dispatchEvent('course.completed', new ServiceEvent($courseMember));
        return $courseMember;        
    }

    public function isCourseCompletion($userId, $courseId, $currentLoopTime = null)
    {
        $courseMember = $this->getCourseMember($courseId, $userId, $currentLoopTime);

        if (empty($courseMember)) {
            return false;
        }

        return $courseMember['isCompleted'] == 1;
    }

    public function isCourseTeacher($courseId, $userId)
    {
        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, CourseLoopUtils::$START_LOOP_TIME);

        if (!$member) {
            return false;
        } else {
            return empty($member) || $member['role'] != 'teacher' ? false : true;
        }
    }

    public function canLearnLesson($courseId, $lessonId, $currentLoopTime = null)
    {
        list($course, $member) = $this->tryTakeCourse($courseId, $currentLoopTime);
        $lesson                = $this->getCourseLesson($courseId, $lessonId);

        if (empty($lesson) || $lesson['courseId'] != $courseId) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if (empty($lesson['requireCredit'])) {
            return array('status' => 'yes');
        }

        if ($member['credit'] >= $lesson['requireCredit']) {
            return array('status' => 'yes');
        }

        return array('status' => 'no', 'message' => sprintf('本课时需要%s学分才能学习，您当前学分为%s分。', $lesson['requireCredit'], $member['credit']));
    }

    public function findLessonViewsByUserIdAndCourseId($userId, $courseId, $currentLoopTime)
    {
        $conditions['userId']          = $userId;
        $conditions['courseId']        = $courseId;
        $conditions['currentLoopTime'] = $currentLoopTime;
        $count                         = $this->searchLessonViewCount($conditions);
        return $this->searchLessonView($conditions, array('id', 'desc'), 0, $count);
    }

    public function searchLessonView($conditions, $orderBy, $start, $limit)
    {
        return $this->getLessonViewDao()->searchLessonView($conditions, $orderBy, $start, $limit);
    }

    public function searchLessonViewCount($conditions)
    {
        return $this->getLessonViewDao()->searchLessonViewCount($conditions);
    }

    public function findFinishedFaceableCertVideoLessonLearns($startTime)
    {
        $finishedLessonLearns = $this->getLessonLearnDao()->searchLearns(
            array('startTime' => $startTime, 'status' => 'finished'), array('id', 'asc'), 0, PHP_INT_MAX
        );

        $result = array();
        foreach ($finishedLessonLearns as $lessonLearn) {
            $course = $this->getCourseDao()->getCourse($lessonLearn['courseId']);
            if ($course['type'] == 'certification') {
                $lesson = $this->getLessonDao()->getLesson($lessonLearn['lessonId']);
                if ($lesson['type'] == 'video') {
                    $faceMarkers = $this->getMarkerService()->findFaceMarkersMetaByMediaId($lesson['id']);
                    if (!empty($faceMarkers)) {
                        $result[] = $lessonLearn;
                    }
                }
            }
        }
        return $result;
    }

    public function clearCourseHistory($userId, $courseId, $currentLoopTime = null)
    {
        $lessons = $this->getCourseLessons($courseId);

        if (!empty($lessons)) {
            $this->getLessonLearnDao()->deleteUserCourseLessonLearns($userId, $courseId, $currentLoopTime);
            $this->getLessonViewDao()->deleteUserCourseLessonViews($userId, $courseId, $currentLoopTime);
            $this->getFaceDetectResultDao()->deleteFaceDetectResult($userId, $courseId, $currentLoopTime);

            $lessonIds = ArrayToolkit::column($lessons, 'id');

            foreach ($lessonIds as $lessonId) {
                setcookie($userId . '_' . $lessonId . '_' . $currentLoopTime.'_time', 0, time() - 10, '/'); //清除播放进度, 进度的设置键 customweb/js/controller/course/learn.js
            }

            $this->getMarkerService()->deleteUserLessonsMarkers($userId, $lessonIds, $currentLoopTime);
            $this->getTestpaperService()->deleteUserCourseTestpaperResults($userId, $courseId, $currentLoopTime);
            $this->getStatusService()->deleteUnBecomeStudentStatus($userId, $courseId, $currentLoopTime);
        }
    }

    public function isTestpaperPassed($userId, $courseId, $currentLoopTime = null)
    {
        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);
        return $member['isTestpaperPassed'] == DbValue::TRUE;
    }

    public function isCourseStudent($courseId, $userId, $currentLoopTime = null)
    {
        $member = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);

        if (!$member) {
            return false;
        } else {
            return empty($member) || $member['role'] != 'student' ? false : true;
        }
    }

    public function favoriteCourse($courseId, $currentLoopTime = 1)
    {
        $user = $this->getCurrentUser();

        if (empty($user['id'])) {
            throw $this->createAccessDeniedException();
        }

        $course = $this->getCourse($courseId);

        if ($course['status'] != 'published') {
            throw $this->createServiceException('不能收藏未发布课程');
        }

        if (empty($course)) {
            throw $this->createServiceException("该课程不存在,收藏失败!");
        }

        $favorite = $this->getFavoriteDao()->getFavoriteByUserIdAndCourseId($user['id'], $course['id'], $currentLoopTime);

        if ($favorite) {
            throw $this->createServiceException("该收藏已经存在，请不要重复收藏!");
        }

        //添加动态
        $this->dispatchEvent(
            'course.favorite',
            new ServiceEvent($course, array('currentLoopTime' => $currentLoopTime))
        );

        $this->getFavoriteDao()->addFavorite(array(
            'courseId'        => $course['id'],
            'userId'          => $user['id'],
            'createdTime'     => time(),
            'currentLoopTime' => $currentLoopTime
        ));

        return true;
    }

    public function unFavoriteCourse($courseId, $currentLoopTime = null)
    {
        $user = $this->getCurrentUser();

        if (empty($user['id'])) {
            throw $this->createAccessDeniedException();
        }

        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createServiceException("该课程不存在,收藏失败!");
        }

        $favorite = $this->getFavoriteDao()->getFavoriteByUserIdAndCourseId($user['id'], $course['id'], $currentLoopTime);

        if (empty($favorite)) {
            throw $this->createServiceException("你未收藏本课程，取消收藏失败!");
        }

        $this->getFavoriteDao()->deleteFavorite($favorite['id'], $currentLoopTime);

        return true;
    }

    public function hasFavoritedCourse($courseId, $currentLoopTime = 1)
    {
        $user = $this->getCurrentUser();

        if (empty($user['id'])) {
            return false;
        }

        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createServiceException("课程{$courseId}不存在");
        }

        $favorite = $this->getFavoriteDao()->getFavoriteByUserIdAndCourseId($user['id'], $course['id'], $currentLoopTime);

        return $favorite ? true : false;
    }

    /**
     * 证书课程的收藏, 每个循环可以单独收藏, 需要额外获取currentLoopTime
     * 如果课程被多次收藏, 则显示多个, 每个coursecurrentLoopTime 不一样
     */
    public function findUserFavoritedCourses($userId, $start, $limit)
    {
        $courseFavorites = $this->getFavoriteDao()->findCourseFavoritesByUserId($userId, $start, $limit);
        $favoriteCourses = $this->getCourseDao()->findCoursesByIds(ArrayToolkit::column($courseFavorites, 'courseId'));

        $result = array();

        foreach ($favoriteCourses as $courseKey => $course) {
            foreach ($courseFavorites as $favorite) {
                if ($course['id'] == $favorite['courseId']) {
                    $course['currentLoopTime'] = $favorite['currentLoopTime'];
                    array_push($result, $course);
                }
            }
        }

        return CourseSerialize::unserializes($result);
    }

    public function isLessionFinished($userId, $lesson, $currentLoopTime)
    {
        $courseId = 0;

        //service实现层根本用不到$courseId参数；
        if ($lesson['type'] == 'testpaper') {
            $target            = sprintf("course-%s/lesson-%s", $lesson['courseId'], $lesson['id']);
            $passedTestResults = $this->getTestpaperResultDao()->findPassedTestResult($target, $userId, $currentLoopTime);
            return !empty($passedTestResults);
        }

        return ('finished' == $this->getUserLearnLessonStatus($userId, $courseId, $lesson['id'], $currentLoopTime));
    }

    public function getLearnRecord($userId, $courseId)
    {
        $learns        = $this->findUserLearnedLessons($userId, $courseId);
        $user          = $this->getUserService()->getUser($userId);
        $certification = $this->getCertificateService()->getCertificationsForUser($userId, $cascadeCertification = false, $courseId);

        $items    = $this->getCourseItems($courseId);
        $sumHours = $this->getSumHours($items);
        $learnSum = 0;
        $lessons  = array();
        foreach ($learns as $key => $learn) {
            $lesson = $this->getLesson($learn['lessonId']);
            $learnSum += $lesson['suggestHours'];
            $chapter = $this->getChapter($lesson['courseId'], $lesson['chapterId']);
            if ($chapter['parentId'] > 0) {
                $parentChapter = $this->getChapter($lesson['courseId'], $chapter['parentId']);
                $learn         = array_merge($learns[$key], array("title" => $lesson['title'], "learnSum" => $learnSum, "suggestHours" => $lesson['suggestHours'], "chapterNumber" => $chapter['number'], "chapterType" => $chapter['type'], "parentChapterNumber" => $parentChapter['number']));
            } else {
                $learn = array_merge($learns[$key], array("title" => $lesson['title'], "learnSum" => $learnSum, "suggestHours" => $lesson['suggestHours'], "chapterNumber" => $chapter['number'], "chapterType" => $chapter['type'], "parentChapterNumber" => ""));
            }

            array_push($lessons, $learn);
        }

        $results = array();
        foreach ($lessons as $key => $lesson) {
            $params['userId']   = $lesson['userId'];
            $params['courseId'] = $lesson['courseId'];
            $params['lessonId'] = $lesson['lessonId'];
            if ($lesson['parentChapterNumber']) {
                $params['chapter'] = "第".$lesson['parentChapterNumber'].'章';
            }

            if ($lesson['chapterNumber']) {
                $lesson['chapterType'] == 'chapter' ? $params['chapter'] = "第".$lesson['chapterNumber']."章" : "第".$lesson['chapterNumber']."节";
            }

            $params['title']            = $lesson['title'];
            $params['startTime']        = date('Y-m-d H:i:s', $lesson['startTime']);
            $params['finishedTime']     = ($lesson['finishedTime'] == 0 ? 0 : date('Y-m-d H:i:s', $lesson['finishedTime']));
            $params['suggestHours']     = $lesson['status'] == "finished" ? $lesson['suggestHours']."分钟" : "-";
            $params['learningProgress'] = $lesson['status'] == "finished" ? round(($lesson['learnSum'] / $sumHours) * 100, 2)."%" : round((($lesson['learnSum'] - $lesson['suggestHours']) / $sumHours) * 100, 2)."%";
            array_push($results, $params);
        }

        $data = array(
            "nickname"   => $user['nickname'],
            "courseName" => $certification['courseName'],
            "startTime"  => $certification['startTime'],
            "deadline"   => $certification['deadline'],
            "lessons"    => $results
        );
        return $data;
    }

    public function getLearnRecordDetail($userId, $courseId, $lessonId)
    {
        $user                          = $this->getUserService()->getUser($userId);
        $lesson                        = $this->getLesson($lessonId);
        $chapter                       = $this->getChapter($courseId, $lesson['chapterId']);
        $parentChapter                 = $this->getChapter($lesson['courseId'], $chapter['parentId']);
        $lesson['parentChapterNumber'] = !empty($parentChapter) ? $parentChapter['number'] : "";
        $lesson['chapterNumber']       = $chapter['number'];
        $lesson['chapterType']         = $chapter['type'];
        $lesson['userName']            = $user['nickname'];
        $lesson['lessonId']            = $lessonId;
        $lesson['userId']              = $userId;
        $conditions                    = array(
            "userId"   => $userId,
            "courseId" => $courseId,
            "lessonId" => $lessonId
        );

        $lessonViews = $this->searchLessonView(
            $conditions,
            array('id', 'desc'),
            0,
            $this->searchLessonViewCount($conditions)
        );

        $results = array();
        foreach ($lessonViews as $key => $lessonView) {
            if ($lessonView['viewDevice'] == 'desktop') {
                $params['viewDevice'] = "PC";
            } else {
                $params['viewDevice'] = "移动设备";
            }

            $params['startTime']    = date('Y-m-d H:i:s', $lessonView['createdTime']);
            $params['finishedTime'] = date('Y-m-d H:i:s', $lessonView['createdTime'] + $lessonView['watchTime']);
            $params['watchTime']    = $this->durationTextFilter($lessonView['watchTime']);
            array_push($results, $params);
        }

        $data = array(
            "nickname"     => $user['nickname'],
            "lessonTitle"  => $lesson['title'],
            "learnLessons" => $results
        );
        return $data;
    }

    public function durationTextFilter($value)
    {
        $minutes = intval($value / 60);
        $seconds = $value - $minutes * 60;

        if ($minutes === 0) {
            return $seconds.'秒';
        }

        return "{$minutes}分钟{$seconds}秒";
    }

    public function addLearnRecordDetail($userId, $courseId, $lessonId, $currentLoopTime = null)
    {
        list($course, $member) = $this->tryTakeCourse($courseId, $currentLoopTime);

        $lesson            = $this->getCourseLesson($courseId, $lessonId);
        $lessonLearnStatus = $this->getUserLearnLessonStatus($userId, $courseId, $lessonId, $currentLoopTime);

        if ($lessonLearnStatus == 'finished') {
            return array("lessonViewId" => -1);
        }

        if (!empty($lesson)) {
            if ($lesson['type'] == 'video') {
                $createLessonView['courseId'] = $courseId;
                $createLessonView['lessonId'] = $lessonId;
                $createLessonView['fileId']   = $lesson['mediaId'];

                $file = array();

                if (!empty($createLessonView['fileId'])) {
                    $file = $this->getUploadFileService()->getFile($createLessonView['fileId']);
                }

                $createLessonView['fileStorage']     = empty($file) ? "net" : $file['storage'];
                $createLessonView['fileType']        = $lesson['type'];
                $createLessonView['fileSource']      = $lesson['mediaSource'];
                $createLessonView['currentLoopTime'] = $currentLoopTime;

                $createLessonView                = ArrayToolkit::parts($createLessonView, array('courseId', 'lessonId', 'fileId', 'fileType', 'fileStorage', 'fileSource', 'currentLoopTime'));
                $createLessonView['userId']      = $userId;
                $createLessonView['viewDevice']  = 'mobile';
                $createLessonView['osInfo'] = $this->getClientOs();
                $createLessonView['createdTime'] = time();

                $lessonView = $this->getLessonViewDao()->addLessonView($createLessonView);

                $this->updateLastLessonView($createLessonView['userId'], $createLessonView['courseId'], $createLessonView['lessonId'], $currentLoopTime, time());

                $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($userId, $lessonId, $currentLoopTime);

                if (!$learn) {
                    $learn = $this->getLessonLearnDao()->addLearn(array(
                        'userId'          => $userId,
                        'courseId'        => $courseId,
                        'lessonId'        => $lessonId,
                        'status'          => 'learning',
                        'startTime'       => time(),
                        'finishedTime'    => 0,
                        'currentLoopTime' => $currentLoopTime
                    ));
                }

                return array("lessonViewId" => $lessonView['id']);
            }
        }

        return array(
            "error" => array(
                "name"    => "lessonView",
                "message" => "记录添加失败"
            )
        );
    }

    public function updateLearnRecordDetail($lessonViewId, $watchTime, $isAddLessonView, $currentLoopTime)
    {
        $lessonView = $this->getLessonViewDao()->getLessonView($lessonViewId);
        $newwatchTime = $lessonView['watchTime'] + $watchTime;

        $learn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($lessonView['userId'], $lessonView['lessonId'], $currentLoopTime);
        if ($learn) {
            $learn = $this->getLessonLearnDao()->updateLearn($learn['id'],array(
                "watchTime" => $learn['watchTime']+$watchTime,
                "learnTime" => $learn['watchTime']+$watchTime
            ));
            $course = $this->getCourse($learn['courseId']);
            $this->getUserLearnTimeService()->waveUserLearnTime($lessonView['userId'], $course['certificationId'], $watchTime);
        }

        if ($isAddLessonView == "true" || $isAddLessonView == "ture") {
            $lesson = $this->getLessonViewDao()->updateLessonView($lessonViewId, array(
                'watchTime' => $newwatchTime
            ));

            $this->updateLastLessonView(
                $lesson['userId'], 
                $lesson['courseId'], 
                $lesson['lessonId'], 
                $lesson['currentLoopTime'], 
                $lesson['createdTime'] + $lesson['watchTime']
            );

            if ($lesson) {
                return 'true';
            } else {
                return array(
                    "error" => array(
                        "name"    => "lessonView",
                        "message" => "更新失败"
                    )
                );
            }
        }

        if ($learn) {
            return 'true';
        } else {
            return array(
                "error" => array(
                    "name"    => "lessonLearn",
                    "message" => "更新失败"
                )
            );
        }
    }

    public function getMemberByOrderId($orderId)
    {
        return $this->getMemberDao()->getMemberByOrderId($orderId);
    }

    public function getLearnLessons($learns, $userId, $currentLoopTime = null)
    {
        $learnSum = 0;
        $lessons  = array();
        foreach ($learns as $key => $learn) {
            $lesson = $this->getLesson($learn['lessonId']);
            $learnSum += $lesson['suggestHours'];
            $chapter    = $this->getChapter($lesson['courseId'], $lesson['chapterId']);
            $unsortedTestpapers = $this->getTestpaperService()->listTestpaperResultByTargetUserId($lesson['id'], $userId, $currentLoopTime);
            $testpapers = ArrayToolkit::sortTwoDimensionArray($unsortedTestpapers, 'checkedTime');
            if ($lesson['type'] == "testpaper") {
                $testpaperStatus = array("excellent", "good", "passed");
                if ($chapter['parentId'] > 0) {
                    $parentChapter = $this->getChapter($lesson['courseId'], $chapter['parentId']);
                    foreach ($testpapers as $testpaper) {
                        array_push($lessons, 
                            array(
                                "id"                  => $learn['id'],
                                "userId"              => $learn['userId'],
                                "courseId"            => $learn['courseId'],
                                "lessonId"            => $learn['lessonId'],
                                "title"               => $lesson['title'],
                                "type"                => $lesson['type'],
                                "learnSum"            => $learnSum,
                                "suggestHours"        => $lesson['suggestHours'],
                                "chapterNumber"       => $chapter['number'],
                                "chapterType"         => $chapter['type'],
                                "parentChapterNumber" => $parentChapter['number'],
                                "status"              => (in_array($testpaper['passedStatus'], $testpaperStatus)) ? "finished" : "learning",
                                "startTime"           => $testpaper['beginTime'],
                                "finishedTime"        => $testpaper['endTime'],
                                "currentLoopTime"     => $testpaper['currentLoopTime']
                            )
                        );

                        if ($testpaper['passedStatus'] == 'passed') {
                            break;
                        }
                    }
                } else {
                    foreach ($testpapers as $testpaper) {
                        array_push($lessons, 
                            array(
                                "id"                  => $learn['id'],
                                "userId"              => $learn['userId'],
                                "courseId"            => $learn['courseId'],
                                "lessonId"            => $learn['lessonId'],
                                "title"               => $lesson['title'],
                                "type"                => $lesson['type'],
                                "learnSum"            => $learnSum,
                                "suggestHours"        => $lesson['suggestHours'],
                                "chapterNumber"       => $chapter['number'],
                                "chapterType"         => $chapter['type'],
                                "parentChapterNumber" => "",
                                "status"              => (in_array($testpaper['passedStatus'], $testpaperStatus)) ? "finished" : "learning",
                                "startTime"           => $testpaper['beginTime'],
                                "finishedTime"        => $testpaper['endTime'],
                                "currentLoopTime"     => $testpaper['currentLoopTime']
                            )
                        );
                        if ($testpaper['passedStatus'] == 'passed') {
                            break;
                        }
                    }
                }
            } else {
                if ($chapter['parentId'] > 0) {
                    $parentChapter = $this->getChapter($lesson['courseId'], $chapter['parentId']);
                    $learn         = array_merge($learns[$key], array(
                        "title"               => $lesson['title'],
                        "type"                => $lesson['type'],
                        "learnSum"            => $learnSum,
                        "suggestHours"        => $lesson['suggestHours'],
                        "chapterNumber"       => $chapter['number'],
                        "chapterType"         => $chapter['type'],
                        "parentChapterNumber" => $parentChapter['number'],
                        "currentLoopTime"     => $learn['currentLoopTime']

                    ));
                } else {
                    $learn = array_merge($learns[$key], array(
                        "title"               => $lesson['title'],
                        "type"                => $lesson['type'],
                        "learnSum"            => $learnSum,
                        "suggestHours"        => $lesson['suggestHours'],
                        "chapterNumber"       => $chapter['number'],
                        "chapterType"         => $chapter['type'],
                        "parentChapterNumber" => "",
                        "currentLoopTime"     => $learn['currentLoopTime']
                    ));
                }
            }

            array_push($lessons, $learn);
        }

        $lessonArray = array();
        foreach ($lessons as $lesson) {
            if (isset($lesson['type'])) {
                array_push($lessonArray, $lesson);
            }
        }

        return $lessonArray;
    }

    public function getTestpaperLesson($courseId)
    {
        return $this->getLessonDao()->getTestpaperLesson($courseId);
    }

    public function updateCourseTotalHoursAndHasTestPaperFields($courseId)
    {
        $items  = $this->getCourseItems($courseId);

        $fields = array(
            'totalHours' => $this->getSumHours($items),
            'hasTestpaper' => null != $this->getTestpaperLesson($courseId),
            'lessonNum' => $this->getLessonsCount($items)
        );

        $this->getCourseDao()->updateCourse($courseId, $fields);
    }

    public function getPercent($totalHours, $learnedHours)
    {
        if (empty($totalHours)) {
            return '0%';
        } else {
            return round($learnedHours / $totalHours, 2) * 100 . '%';
        }
    }

    public function getLessonLearnsWithLessonType($userId)
    {
        $allLessonLearns = $this->getLessonLearnDao()->findLearnsByUserId($userId);
        $courseIds = (array_unique(ArrayToolkit::column($allLessonLearns, 'courseId')));

        $allCourseLessons = array();
        foreach ($courseIds as $courseId) {
            $currentCourseLessons = $this->getLessonDao()->findLessonsByCourseId($courseId);
            $allCourseLessons = array_merge($allCourseLessons, $currentCourseLessons);
        } 

        foreach ($allLessonLearns as $lessonLearnKey => $lessonLearn) {
            foreach ($allCourseLessons as $courseLesson) {
                if ($lessonLearn['lessonId'] == $courseLesson['id']) {
                    $allLessonLearns[$lessonLearnKey]['type'] = $courseLesson['type'];
                    $allLessonLearns[$lessonLearnKey]['suggestHours'] = $courseLesson['suggestHours'];
                    break;
                }
            }
        }

        return $allLessonLearns;
    }

    public function getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime)
    {
        return $this->getLastLessonViewDao()->getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime);
    }

    public function updateLastLessonView($userId, $courseId, $lessonId, $currentLoopTime, $watchTime)
    {
        $lastLessonView = $this->getLastLessonViewByUserIdAndLessonIdAndCurrentLoopTime($userId, $lessonId, $currentLoopTime);
        if (empty($lastLessonView)) {
            return $this->getLastLessonViewDao()->addLastLessonView(array(
                'userId' => $userId,
                'courseId' => $courseId,
                'lessonId' => $lessonId,
                'currentLoopTime' => $currentLoopTime
            ));
        } else {
            return $this->getLastLessonViewDao()->updateLastLessonView($lastLessonView['id'], array('createdTime' => $watchTime));
        }
    }

    public function getLessonStatus($userId, $courseId, $currentLoopTime, $lessons)
    {
        $learnStatuses = $this->getUserLearnLessonStatuses($userId, $courseId, $currentLoopTime);
        $testUnPassed = $this->getUnpassedLessonIds($lessons, $userId, $currentLoopTime);
            
        array_walk($learnStatuses, function(&$value, $key) use($testUnPassed) {
            if (!empty($testUnPassed) && in_array($key, $testUnPassed)) {
                $value='learning';
            }
        });

        return $learnStatuses;
    }

    public function searchRecommendedCourses($conditions, $orderBy, $start, $limit)
    {
        $conditions['recommended'] = 1;
        $recommendCount     = $this->searchCourseCount($conditions);
        $currentPage               = floor($start / $limit) + 1;
        $recommendPage      = intval($recommendCount / $limit);
        $recommendLeft        = $recommendCount % $limit;

        if ($currentPage <= $recommendPage) {
            $courses = $this->searchCourses(
                $conditions,
                $orderBy,
                ($currentPage - 1) * $limit,
                $limit
            );
        } elseif (($recommendPage + 1) == $currentPage) {
            $courses = $this->searchCourses(
                $conditions,
                $orderBy,
                ($currentPage - 1) * $limit,
                $limit
            );
            $conditions['recommended'] = 0;
            $coursesTemp               = $this->searchCourses(
                $conditions,
                'createdTime',
                0,
                $limit - $recommendLeft
            );
            $courses = array_merge($courses, $coursesTemp);
        } else {
            $conditions['recommended'] = 0;
            $courses                   = $this->searchCourses(
                $conditions,
                'createdTime',
                ($limit - $recommendLeft) + ($currentPage - $recommendPage - 2) * $limit ,
                $limit 
            );
        }

        return $courses;
    }

    public function markAsAtLeastOneLessonFinished($userId, $courseId, $currentLoopTime)
    {
        $courseMember = $this->getMemberDao()->getMemberByCourseIdAndUserId($courseId, $userId, $currentLoopTime);
        if (!$courseMember['isAtLeastOneLessonFinished']) {
            $this->getMemberDao()->updateMember(
                $courseMember['id'], 
                array('isAtLeastOneLessonFinished' => DbValue::TRUE)
            );
        }
    }

    protected function getTestpaperService()
    {
        return $this->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getCourseDao()
    {
        return $this->createDao('Custom:Course.CourseDao');
    }

    protected function getDiscountService()
    {
        return $this->createService('Discount:Discount.DiscountService');
    }

    protected function getTagService()
    {
        return $this->createService('Taxonomy.TagService');
    }

    protected function getMemberDao()
    {
        return $this->createDao('Custom:Course.CourseMemberDao');
    }

    protected function getLessonLearnDao()
    {
        return $this->createDao('Custom:Course.LessonLearnDao');
    }

    protected function getFavoriteDao()
    {
        return $this->createDao('Custom:Course.FavoriteDao');
    }

    protected function getLessonDao()
    {
        return $this->createDao('Custom:Course.LessonDao');
    }

    protected function getFaceDetectResultDao()
    {
        return $this->createDao('Custom:FaceDetectResult.FaceDetectResultDao');
    }

    protected function getLessonViewDao()
    {
        return $this->createDao('Custom:Course.LessonViewDao');
    }

    protected function getLastLessonViewDao()
    {
        return $this->createDao('Custom:Course.LastLessonViewDao');
    }

    protected function getMarkerService()
    {
        return $this->createService('Custom:Marker.MarkerService');
    }

    protected function getUserService()
    {
        return $this->createService('User.UserService');
    }

    protected function getBranchSchoolService()
    {
        return $this->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }

    protected function getAreaService()
    {
        return $this->createService('Custom:Area.AreaService');
    }

    protected function getStatusService()
    {
        return $this->createService('Custom:User.StatusService');
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getTestpaperResultDao()
    {
        return $this->createDao('Custom:Testpaper.TestpaperResultDao');
    }

    protected function getOfflineFinishedCourseDao()
    {
        return $this->createDao('Custom:Course.OfflineFinishedCourseDao');
    }

    protected function getUserCertCourseDao()
    {
        return $this->createDao('Custom:Certificate.UserCertCourseDao');
    }

    protected function getUserLearnTimeService()
    {
        return $this->createService('Custom:Course.UserLearnTimeService');
    }

    private function hasSuggestHours($type)
    {
        $lessonType = array('text', 'video', 'ppt', 'audio', 'document', 'flash', 'testpaper');

        if (in_array($type, $lessonType)) {
            return true;
        }
    }

    private function isAllLessonsFinished($items, $userId, $currentLoopTime = null)
    {
        $isAllLessonsFinished = true;

        foreach ($items as $key => $value) {
            if ($this->hasSuggestHours($value['type']) && 
                    $value['type'] != 'testpaper' &&
                    !$this->isLessionFinished($userId, $value, $currentLoopTime)) {
                $isAllLessonsFinished = false;
                break;
            }
        }

        return $isAllLessonsFinished;
    }

    private function getLessonsCount($items)
    {
        $count = 0;

        foreach ($items as $key => $value) {
            if ($this->hasSuggestHours($value['type']) && $value['status']=='published') {
                $count ++;
            }
        }

        return $count;
    }

    private function updateChapterAndUnitHours($value, $items, $chapterHour, $isAdmin, $userId, $currentLoopTime)
    {
        $sum = 0;
        foreach ($items as $chapterChildValue) {
            if ($chapterChildValue['type'] == 'unit' && $chapterChildValue['parentId'] == $value['id']) {
                foreach ($items as $textValue) {
                    if ($this->hasSuggestHours($textValue['type']) && 
                            $textValue['chapterId'] == $chapterChildValue['id'] && 
                            ($isAdmin || $this->isLessionFinished($userId, $textValue, $currentLoopTime))) {
                        $sum += round($textValue['suggestHours'], 0);
                    }
                }
            } elseif ($this->hasSuggestHours($chapterChildValue['type']) && 
                    $chapterChildValue['chapterId'] == $value['id'] && 
                    ($isAdmin || $this->isLessionFinished($userId, $chapterChildValue, $currentLoopTime))) {
                $sum += round($chapterChildValue['suggestHours'], 0);
            }
        }

        $chapterHour[$value['number']] = $sum;
        return $chapterHour;
    }

    private function getUnpassedLessonIds($lessons, $userId, $currentLoopTime)
    {
        $testUnPassed = array();
        foreach($lessons as $lesson) {
            if ($lesson['type'] == 'testpaper') {
                if(!$this->isLessionFinished($userId, $lesson, $currentLoopTime)) {
                    array_push($testUnPassed,$lesson['id']);
                }
            }
        }
        return $testUnPassed;
    }

    private function getClientOs()
    {
        $osPrefix = null;
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            $osPrefix = 'IOS';
        } else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')) {
            $osPrefix = 'Android';
        } else {
            $osPrefix = 'Other';
        }
        return $osPrefix . ': ' . $_SERVER['HTTP_USER_AGENT'];
    }

}

class CourseSerialize
{
    public static function serialize(array &$course)
    {
        if (isset($course['tags'])) {
            if (is_array($course['tags']) && !empty($course['tags'])) {
                $course['tags'] = '|'.implode('|', $course['tags']).'|';
            } else {
                $course['tags'] = '';
            }
        }

        if (isset($course['goals'])) {
            if (is_array($course['goals']) && !empty($course['goals'])) {
                $course['goals'] = '|'.implode('|', $course['goals']).'|';
            } else {
                $course['goals'] = '';
            }
        }

        if (isset($course['audiences'])) {
            if (is_array($course['audiences']) && !empty($course['audiences'])) {
                $course['audiences'] = '|'.implode('|', $course['audiences']).'|';
            } else {
                $course['audiences'] = '';
            }
        }

        if (isset($course['teacherIds'])) {
            if (is_array($course['teacherIds']) && !empty($course['teacherIds'])) {
                $course['teacherIds'] = '|'.implode('|', $course['teacherIds']).'|';
            } else {
                $course['teacherIds'] = null;
            }
        }

        return $course;
    }

    public static function unserialize(array $course = null)
    {
        if (empty($course)) {
            return $course;
        }

        $course['tags'] = empty($course['tags']) ? array() : explode('|', trim($course['tags'], '|'));

        if (empty($course['goals'])) {
            $course['goals'] = array();
        } else {
            $course['goals'] = explode('|', trim($course['goals'], '|'));
        }

        if (empty($course['audiences'])) {
            $course['audiences'] = array();
        } else {
            $course['audiences'] = explode('|', trim($course['audiences'], '|'));
        }

        if (empty($course['teacherIds'])) {
            $course['teacherIds'] = array();
        } else {
            $course['teacherIds'] = explode('|', trim($course['teacherIds'], '|'));
        }

        return $course;
    }

    public static function unserializes(array $courses)
    {
        return array_map(function ($course) {
            return CourseSerialize::unserialize($course);
        }, $courses);
    }
}

class LessonSerialize
{
    public static function serialize(array $lesson)
    {
        return $lesson;
    }

    public static function unserialize(array $lesson = null)
    {
        return $lesson;
    }

    public static function unserializes(array $lessons)
    {
        return array_map(function ($lesson) {
            return LessonSerialize::unserialize($lesson);
        }, $lessons);
    }
}
