<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Course\Impl\ThreadServiceImpl as BaseServiceImpl;

class ThreadServiceImpl extends BaseServiceImpl
{
    public function createThread($thread, $currentLoopTime = 1)
    {
        if (empty($thread['courseId'])) {
            throw $this->createServiceException('Course ID can not be empty.');
        }

        if (empty($thread['type']) || !in_array($thread['type'], array('discussion', 'question'))) {
            throw $this->createServiceException(sprintf('Thread type(%s) is error.', $thread['type']));
        }

        $event = $this->dispatchEvent('course.thread.before_create', $thread);

        if ($event->isPropagationStopped()) {
            throw $this->createServiceException('发帖次数过多，请稍候尝试。');
        }

        $thread['content']     = $this->sensitiveFilter($thread['content'], 'course-thread-create');
        $thread['title']       = $this->sensitiveFilter($thread['title'], 'course-thread-create');
        list($course, $member) = $this->getCourseService()->tryTakeCourse($thread['courseId'], $currentLoopTime);

        if (empty($thread['userId'])) {
            $thread['userId'] = $this->getCurrentUser()->id;
        }
        
        $thread['title']  = $this->purifyHtml(empty($thread['title']) ? '' : $thread['title']);

        //创建thread过滤html
        $thread['content']          = $this->purifyHtml($thread['content']);
        $thread['createdTime']      = time();
        $thread['latestPostUserId'] = $thread['userId'];
        $thread['latestPostTime']   = $thread['createdTime'];
        $thread['private']          = $course['status'] == 'published' ? 0 : 1;
        $thread                     = $this->getThreadDao()->addThread($thread);

        foreach ($course['teacherIds'] as $teacherId) {
            if ($teacherId == $thread['userId']) {
                continue;
            }

            if ($thread['type'] != 'question') {
                continue;
            }

            $this->getNotifiactionService()->notify($teacherId, 'thread', array(
                'threadId'           => $thread['id'],
                'threadUserId'       => $thread['userId'],
                'threadUserNickname' => $this->getCurrentUser()->nickname,
                'threadTitle'        => $thread['title'],
                'threadType'         => $thread['type'],
                'courseId'           => $course['id'],
                'courseTitle'        => $course['title']
            ));
        }

        $event = $this->dispatchEvent('course.thread.create', $thread);

        return $thread;
    }

    public function createPost($post, $currentLoopTime = null)
    {
        $requiredKeys = array('courseId', 'threadId', 'content');

        if (!ArrayToolkit::requireds($post, $requiredKeys)) {
            throw $this->createServiceException('参数缺失');
        }

        $event = $this->dispatchEvent('course.thread.post.before_create', $post);

        if ($event->isPropagationStopped()) {
            throw $this->createServiceException('发帖次数过多，请稍候尝试。');
        }

        $thread = $this->getThread($post['courseId'], $post['threadId']);

        if (empty($thread)) {
            throw $this->createServiceException(sprintf('课程(ID: %s)话题(ID: %s)不存在。', $post['courseId'], $post['threadId']));
        }

        $post['content']       = $this->sensitiveFilter($post['content'], 'course-thread-post-create');
        list($course, $member) = $this->getCourseService()->tryTakeCourse($post['courseId'], $currentLoopTime);

        $post['userId']      = $this->getCurrentUser()->id;
        $post['isElite']     = $this->getCourseService()->isCourseTeacher($post['courseId'], $post['userId']) ? 1 : 0;
        $post['createdTime'] = time();

        //创建post过滤html
        $post['content'] = $this->purifyHtml($post['content']);
        $post            = $this->getThreadPostDao()->addPost($post);

        // 高并发的时候， 这样更新postNum是有问题的，这里暂时不考虑这个问题。
        $threadFields = array(
            'postNum'          => $thread['postNum'] + 1,
            'latestPostUserId' => $post['userId'],
            'latestPostTime'   => $post['createdTime']
        );
        $this->getThreadDao()->updateThread($thread['id'], $threadFields);

        $this->dispatchEvent('course.thread.post.create', $post);

        return $post;
    }
}
