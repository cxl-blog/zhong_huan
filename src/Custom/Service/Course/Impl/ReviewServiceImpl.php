<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\ServiceEvent;
use Topxia\Service\Course\Impl\ReviewServiceImpl as BaseServiceImpl;

class ReviewServiceImpl extends BaseServiceImpl
{
    public function saveReview($fields, $currentLoopTime = 1)
    {

        if (!ArrayToolkit::requireds($fields, array('courseId', 'userId', 'rating'))) {
            throw $this->createServiceException('参数不正确，评价失败！');
        }

        if ($fields['rating'] > 5) {
            throw $this->createServiceException($this->getKernel()->trans('参数不正确，评价数太大'));
        }

        list($course, $member) = $this->getCourseService()->tryTakeCourse($fields['courseId'], $currentLoopTime);

        $userId = $this->getCurrentUser()->id;

        if (empty($course)) {
            throw $this->createServiceException("课程(#{$fields['courseId']})不存在，评价失败！");
        }

        $user = $this->getUserService()->getUser($fields['userId']);

        if (empty($user)) {
            throw $this->createServiceException("用户(#{$fields['userId']})不存在,评价失败!");
        }

        $review = $this->getReviewDao()->getReviewByUserIdAndCourseId($user['id'], $course['id']);

        if (empty($review)) {
            $review = $this->getReviewDao()->addReview(array(
                'userId'      => $fields['userId'],
                'courseId'    => $fields['courseId'],
                'rating'      => $fields['rating'],
                'private'     => $course['status'] == 'published' ? 0 : 1,
                'content'     => empty($fields['content']) ? '' : $fields['content'],
                'createdTime' => time()
            ));
            $this->dispatchEvent('courseReview.add', new ServiceEvent($review));
        } else {
            $review = $this->getReviewDao()->updateReview($review['id'], array(
                'rating'  => $fields['rating'],
                'content' => empty($fields['content']) ? '' : $fields['content']
            ));
        }

        $this->calculateCourseRating($course['id']);

        return $review;
    }
}
