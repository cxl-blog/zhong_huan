<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Service\Course\CourseCopyService;
use Topxia\Service\Course\Impl\CourseCopyServiceImpl as BaseService;

class CourseCopyServiceImpl extends BaseService implements CourseCopyService
{
    public function newCopy($courseId,$course, $link = false)
    {
        $this->getCourseDao()->getConnection()->beginTransaction();
        try {

            $newChapters = $this->newCopyChapters($courseId, $course);

            $this->newCopyLessons($courseId, $course, $newChapters);

            $this->getCourseDao()->getConnection()->commit();
        } catch (\Exception $e) {
            $this->getCourseDao()->getConnection()->rollback();
            throw $e;
        }

        return $course;
    }

    protected function newCopyLessons($courseId, $course, $chapters)
    {
        $lessons = $this->getLessonDao()->findLessonsByCourseId($course['id']);
        $currentLessons = $this->getLessonDao()->findLessonsByCourseId($courseId);
        $map     = array();
        array_multisort($lessons,SORT_ASC);

        foreach ($lessons as $lesson) {
            $fields             = ArrayToolkit::parts($lesson, array('number', 'seq', 'free', 'status', 'title', 'summary', 'tags', 'type', 'content', 'giveCredit', 'requireCredit', 'mediaId', 'mediaSource', 'mediaName', 'mediaUri', 'length', 'materialNum', 'startTime', 'endTime', 'liveProvider', 'userId', 'replayStatus', 'suggestHours'));
            $fields['courseId'] = $courseId;
            foreach($currentLessons as $currentLesson) {
                if($currentLesson['copyId'] == $lesson['id']) {
                    continue 2;
                }
            }
            if('testpaper' == $fields['type']) {
                continue;
            }

            if ($lesson['chapterId']) {
                $fields['chapterId'] = empty($chapters) ? 0 : $chapters[$lesson['chapterId']]['id'] ;
            } else {
                $fields['chapterId'] = 0;
            }

            $fields['createdTime'] = time();

            $fields['copyId'] = $lesson['id'];
            $fields['number']      = $lesson['number'];
            $fields['seq']         = $lesson['seq'];

            $copiedLesson     = $this->getLessonDao()->addLesson($fields);
            
            if (array_key_exists('type', $lesson) && $lesson['type'] == 'live' && $lesson['status'] == 'published') {
                $this->createJob($lesson);
            }

            $map[$lesson['id']] = $copiedLesson;

            if (array_key_exists("mediaId", $copiedLesson) && $copiedLesson["mediaId"] > 0 && in_array($copiedLesson["type"], array('video', 'audio', 'ppt'))) {
                $this->getUploadFileDao()->waveUploadFile($copiedLesson["mediaId"], 'usedCount', 1);
            }

            if (array_key_exists('type', $lesson) && $lesson['type'] == 'live' && $lesson['replayStatus'] == 'generated' && !empty($copiedLesson)) {
                $courseLessonReplay                = $this->getCourseService()->getCourseLessonReplayByCourseIdAndLessonId($courseId, $lesson['id']);
                $courseLessonReplay                = array('title' => $courseLessonReplay['title'], 'replayId' => $courseLessonReplay['replayId'], 'userId' => $courseLessonReplay['userId']);
                $courseLessonReplay['courseId']    = $copiedLesson['courseId'];
                $courseLessonReplay['lessonId']    = $copiedLesson['id'];
                $courseLessonReplay['createdTime'] = time();
                $this->getCourseService()->addCourseLessonReplay($courseLessonReplay);
            }
        }
        return $map;
    }

    protected function newCopyChapters($courseId, $course)
    {
        $chapters = $this->getCourseChapterDao()->findChaptersByCourseId($course['id']);
        $currentChapters = $this->getCourseChapterDao()->findChaptersByCourseId($courseId);

        $map = array();
        array_multisort($chapters,SORT_ASC);

        foreach ($chapters as $chapter) {
            if (!empty($chapter['parentId'])) {
                continue;
            }
            foreach($currentChapters as $currentChapter) {
                if($currentChapter['copyId'] == $chapter['id']) {
                    continue 2;
                }
            }

            $orgChapterId      = $chapter['id'];
            $chapter['copyId'] = $chapter['id'];

            $chapter['courseId']    = $courseId;
            $chapter['createdTime'] = time();
            unset($chapter['id']);

            $map[$orgChapterId] = $this->getCourseChapterDao()->addChapter($chapter);
        }

        foreach ($chapters as $chapter) {
            if (empty($chapter['parentId'])) {
                continue;
            }
            foreach($currentChapters as $currentChapter) {
                if($currentChapter['copyId'] == $chapter['id']) {
                    continue 2;
                }
            }
            $orgChapterId = $chapter['id'];

            $chapter['courseId']    = $courseId;
            $chapter['parentId']    = $map[$chapter['parentId']]['id'];
            $chapter['createdTime'] = time();
            $chapter['copyId']      = $chapter['id'];
            unset($chapter['id']);

            $map[$orgChapterId] = $this->getCourseChapterDao()->addChapter($chapter);
        }

        return $map;
    }

    public function getNextLessonNumber($courseId)
    {
        return $this->getLessonDao()->getLessonCountByCourseId($courseId) + 1;
    }

    public function getNextChapterNumber($courseId)
    {
        $counter = $this->getChapterDao()->getChapterCountByCourseIdAndType($courseId, 'chapter');
        return $counter + 1;
    }

    protected function getNextCourseItemSeq($courseId)
    {
        $chapterMaxSeq = $this->getChapterDao()->getChapterMaxSeqByCourseId($courseId);
        $lessonMaxSeq  = $this->getLessonDao()->getLessonMaxSeqByCourseId($courseId);
        return ($chapterMaxSeq > $lessonMaxSeq ? $chapterMaxSeq : $lessonMaxSeq) + 1;
    }

    protected function getChapterDao()
    {
        return $this->createDao('Course.CourseChapterDao');
    }
}
