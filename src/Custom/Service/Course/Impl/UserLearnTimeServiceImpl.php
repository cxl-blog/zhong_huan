<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\BaseService;

class UserLearnTimeServiceImpl extends BaseService
{
    public function addUserLearnTimeIfNeed($userId, $certificationId)
    {
        $userLearnTime = $this->getUserLearnTimeDao()->getUserLearnTime(
            $userId, 
            $certificationId
        );
        if (empty($userLearnTime)) {
            $this->getUserLearnTimeDao()->addUserLearnTime(
                array(
                    'userId' => $userId, 
                    'certificationId' => $certificationId
                )
            );
        }
    }

    public function getCurrentLearnTimeInfo($userId, $courseId, $areaCode = null, $certificationId = null)
    {
        if (empty($areaCode) || empty($certificationId)) {
            $course = $this->getCourseService()->getCourse($courseId);
            $areaCode = $course['areaCode'];
            $certificationId = $course['certificationId'];
        }
        
        $userLearnTime = $this->getUserLearnTimeDao()->getUserLearnTime(
            $userId, 
            $certificationId
        );
        $areaCertification = $this->getCertificateService()->findByAreaCodeAndCertificationId(
            $areaCode, 
            $certificationId
        );

        if ($areaCertification['maxLearningMinutes'] == 1440) {
            $areaCertification['maxLearningMinutes'] = '144000';
            $areaCertification['maxLearningSeconds'] = '8640000';
        }

        return array(
            'isExceeding' => $userLearnTime['learnedSeconds'] >= $areaCertification['maxLearningSeconds'],
            'maxLearningSeconds' => $areaCertification['maxLearningSeconds'],
            'maxLearningMinutes' => $areaCertification['maxLearningMinutes'],
            'isSequentialLearning' => $areaCertification['sequentialLearning'] != 0,
            'learnedSeconds' => $userLearnTime['learnedSeconds'],
            'learnedMinutes' => $userLearnTime['learnedMinutes']
        );
    }

    public function waveUserLearnTime($userId, $certificationId, $learnedSeconds)
    {
        $this->getUserLearnTimeDao()->increaseLearnTime($userId, $certificationId, $learnedSeconds);
    }

    public function clearUserLearnTime()
    {
        $this->getUserLearnTimeDao()->clearLearnTime();
    }

    public function clearSingleUserLearnTime($userId)
    {
        $this->getUserLearnTimeDao()->clearSingleLearnTime($userId);
    }

    protected function getUserLearnTimeDao()
    {
        return $this->createDao('Custom:Course.UserLearnTimeDao');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }
}
