<?php
namespace Custom\Service\Course\Impl;

use Topxia\Service\Course\Impl\CourseOrderServiceImpl;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\StringToolkit;
use Topxia\Service\Common\ServiceKernel;

class CertificationCourseOrderServiceImpl extends CourseOrderServiceImpl
{

    public function createOrder($info, $currentLoopTime = null)
    {
        $connection = ServiceKernel::instance()->getConnection();
        try {
            $connection->beginTransaction();
            
            $user = $this->getCurrentUser();
            if (!$user->isLogin()) {
                throw $this->createServiceException('用户未登录，不能创建订单');
            }

            if (!ArrayToolkit::requireds($info, array('targetId', 'payment'))) {
                throw $this->createServiceException('订单数据缺失，创建课程订单失败。');
            }

            // 获得锁
            $user = $this->getUserService()->getUser($user['id'], true);

            if (!$this->getCertificateService()->isCertificationCourseBuyable($user['id'], $info['targetId'], $currentLoopTime)) {
                return array('error' => '该课程不可购买，如有需要，请联系客服');
            }

            $course = $this->getCourseService()->getCourse($info['targetId']);
            if (empty($course)) {
                throw $this->createServiceException('课程不存在，操作失败。');
            }
            if ($course['approval'] && $user['approvalStatus'] != 'approved') {
                throw $this->createServiceException('该课程需要实名认证，你还没有实名认证，不可购买。');
            }

            $order = array();

            $order['userId'] = $user['id'];
            $order['title'] = "购买课程《{$course['title']}》";
            $order['targetType'] = 'course';
            $order['targetId'] = $course['id'];
            if(!empty($course['discountId'])){
                $order['discountId'] = $course['discountId'];
                $order['discount'] = $course['discount'];
            }

            $order['payment'] = $info['payment'];
            $order['amount'] = empty($info['amount'])? 0 : $info['amount'];
            $order['priceType'] = $info['priceType'];
            $order['totalPrice'] = $info["totalPrice"];
            $order['coinRate'] = $info['coinRate'];
            $order['coinAmount'] = $info['coinAmount'];
            $order['currentLoopTime'] = $currentLoopTime;

            $courseSetting=$this->getSettingService()->get('course',array());

            if (array_key_exists("coursesPrice", $courseSetting)) {
                $notShowPrice = $courseSetting['coursesPrice'];
            }else{
                $notShowPrice = 0;
            }

            if($notShowPrice == 1) {
                $order['amount'] = 0;
                $order['totalPrice'] = 0;
            }

            $order['snPrefix'] = 'C';

            if (!empty($info['coupon'])) {
                $order['coupon'] = $info['coupon'];
                $order['couponDiscount'] = $info['couponDiscount'];
            }

            if (!empty($info['note'])) {
                $order['data'] = array('note' => $info['note']);
            }
            $order = $this->getOrderService()->createOrder($order, $currentLoopTime);
            if (empty($order)) {
                throw $this->createServiceException('创建课程订单失败！');
            }

            // 免费课程或VIP用户，就直接将订单置为已购买
            if ((intval($order['amount']*100) == 0 && intval($order['coinAmount']*100) == 0 && empty($order['coupon']))||!empty($info["becomeUseMember"])) {
                list($success, $order) = $this->getOrderService()->payOrder(array(
                    'sn' => $order['sn'],
                    'status' => 'success', 
                    'amount' => $order['amount'], 
                    'paidTime' => time()
                ));
                $info = array(
                    'orderId' => $order['id'],
                    'remark'  => empty($order['data']['note']) ? '' : $order['data']['note'],
                );
                $info['currentLoopTime'] = $currentLoopTime;
                $this->getCourseService()->becomeStudent($order['targetId'], $order['userId'], $info);
            }

            $connection->commit();

            return $order;
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }

    }

    public function doSuccessPayOrder($id)
    {
        $order = $this->getOrderService()->getOrder($id);
        if (empty($order) || $order['targetType'] != 'course') {
            throw $this->createServiceException('非课程订单，加入课程失败。');
        }

        $info = array(
            'orderId' => $order['id'],
            'remark'  => empty($order['data']['note']) ? '' : $order['data']['note'],
            'currentLoopTime' => $order['currentLoopTime']
        );
        
        $this->getCourseService()->becomeStudent($order['targetId'], $order['userId'], $info);

        return ;
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CertificationCourseService');
    }

    protected function getOrderService()
    {
        return $this->createService('Custom:Order.OrderService');
    }

 }