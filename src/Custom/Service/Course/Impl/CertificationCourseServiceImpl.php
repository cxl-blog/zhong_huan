<?php
namespace Custom\Service\Course\Impl;

use Custom\Service\Course\Impl\CourseServiceImpl as BaseCourseServiceImpl;
use Custom\Common\Constants\CertCourseStatus;
use Topxia\Service\Common\ServiceEvent;

class CertificationCourseServiceImpl extends BaseCourseServiceImpl
{

    /**
     * 证书课程才用这个方法
     */
    public function becomeStudent($courseId, $userId, $info = array())
    {
        $course = $this->getCourse($courseId);

        if (empty($course)) {
            throw $this->createNotFoundException();
        }

        if (!in_array($course['status'], array('published'))) {
            throw $this->createServiceException('不能加入未发布课程');
        }

        $user = $this->getUserService()->getUser($userId);

        if (empty($user)) {
            throw $this->createServiceException("用户(#{$userId})不存在，加入课程失败！");
        }

        $levelChecked = '';

        if (!empty($info['becomeUseMember'])) {
            $levelChecked = $this->getVipService()->checkUserInMemberLevel($user['id'], $course['vipLevelId']);

            if ($levelChecked != 'ok') {
                throw $this->createServiceException("用户(#{$userId})不能以会员身份加入课程！");
            }

            $userMember = $this->getVipService()->getMemberByUserId($user['id']);
        }

        if ($course['expiryDay'] > 0) {
            $deadline = $course['expiryDay'] * 24 * 60 * 60 + time();
        } else {
            $deadline = 0;
        }

        if (!empty($info['orderId'])) {
            $order = $this->getOrderService()->getOrder($info['orderId']);

            if (empty($order)) {
                throw $this->createServiceException("订单(#{$info['orderId']})不存在，加入课程失败！");
            }
        } else {
            $order = null;
        }

        $conditions = array(
            'userId'   => $userId,
            'status'   => 'finished',
            'courseId' => $courseId,
            'currentLoopTime' => $info['currentLoopTime']
        );
        $count  = $this->getLessonLearnDao()->searchLearnCount($conditions);
        $now = time();
        $fields = array(
            'courseId'    => $courseId,
            'userId'      => $userId,
            'orderId'     => empty($order) ? 0 : $order['id'],
            'deadline'    => $deadline,
            'levelId'     => empty($info['becomeUseMember']) ? 0 : $userMember['levelId'],
            'role'        => 'student',
            'remark'      => empty($order['note']) ? '' : $order['note'],
            'learnedNum'  => $count,
            'createdTime' => $now,
            'currentLoopTime' => $info['currentLoopTime']
        );

        $fields = $this->fillPercentInfoWithMarkAllLessonsFinished($fields, $userId, $courseId, $info['currentLoopTime']);

        if (empty($fields['remark'])) {
            $fields['remark'] = empty($info['note']) ? '' : $info['note'];
        }
        
        $member = $this->getMemberDao()->addMember($fields);
        $this->clearCourseHistory($userId, $courseId, $info['currentLoopTime']);

        $this->setMemberNoteNumber(
            $courseId,
            $userId,
            $this->getNoteDao()->getNoteCountByUserIdAndCourseId($userId, $courseId),
            $info['currentLoopTime']
        );

        $setting = $this->getSettingService()->get('course', array());

        if (!empty($setting['welcome_message_enabled']) && !empty($course['teacherIds'])) {
            $message = $this->getWelcomeMessageBody($user, $course);
            $this->getMessageService()->sendMessage($course['teacherIds'][0], $user['id'], $message);
        }

        $fields = array(
            'studentNum' => $this->getCourseStudentCount($courseId)
        );

        if ($order) {
            $fields['income'] = $this->getOrderService()->sumOrderPriceByTarget('course', $courseId);
        }

        $this->getCourseDao()->updateCourse($courseId, $fields);
        $this->dispatchEvent(
            'course.join',
            new ServiceEvent($course, array('userId' => $member['userId'], 'currentLoopTime' => $info['currentLoopTime']))
        );

        $this->getUserCertCourseDao()->updateUserCertCoursePerDyncConditions(
            array(
                'userId' => $userId,
                'courseId' => $courseId,
                'currentLoopTime' => $info['currentLoopTime']
            ),
            array(
                'status' => CertCourseStatus::LEARNING,
                'latestBuyTime' => $now
            )
        );

        return $member;
    }

    public function setMemberNoteNumber($courseId, $userId, $number, $currentLoopTime = null)
    {
        $member = $this->getCourseMember($courseId, $userId, $currentLoopTime);

        if (empty($member)) {
            return false;
        }

        $this->getMemberDao()->updateMember($member['id'], array(
            'noteNum'            => (int) $number,
            'noteLastUpdateTime' => time()
        ));

        return true;
    }

}
