<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\ServiceEvent;
use Topxia\Service\Course\Impl\NoteServiceImpl as BaseServiceImpl;

class NoteServiceImpl extends BaseServiceImpl
{
    /**
     * 类似这样的，提交数据保存到数据的流程是：
     *
     *  1. 检查参数是否正确，不正确就抛出异常
     *  2. 过滤数据
     *  3. 插入到数据库
     *  4. 更新其他相关的缓存字段
     */
    public function saveNote(array $note, $currentLoopTime = null)
    {
        if (!ArrayToolkit::requireds($note, array('lessonId', 'courseId', 'content'))) {
            throw $this->createServiceException('缺少必要的字段，保存笔记失败');
        }

        list($course, $member) = $this->getCourseService()->tryTakeCourse($note['courseId'], $currentLoopTime);
        $user                  = $this->getCurrentUser();

        if (!$this->getCourseService()->getCourseLesson($note['courseId'], $note['lessonId'])) {
            throw $this->createServiceException('课时不存在，保存笔记失败');
        }

        $note = ArrayToolkit::filter($note, array(
            'courseId' => 0,
            'lessonId' => 0,
            'content'  => '',
            'status'   => 0
        ));

        $note['content'] = $this->purifyHtml($note['content']) ?: '';
        $note['length']  = $this->calculateContnentLength($note['content']);

        $existNote = $this->getUserLessonNote($user['id'], $note['lessonId']);

        if (!$existNote) {
            $note['userId']      = $user['id'];
            $note['createdTime'] = time();
            $note['updatedTime'] = time();
            $note                = $this->getNoteDao()->addNote($note);
            $this->getDispatcher()->dispatch('course.note.create', new ServiceEvent($note));
        } else {
            $note['updatedTime'] = time();
            $note                = $this->getNoteDao()->updateNote($existNote['id'], $note);
            $this->getDispatcher()->dispatch('course.note.update', new ServiceEvent($note, array('preStatus' => $existNote['status'])));
        }

        $this->getCourseService()->setMemberNoteNumber(
            $note['courseId'],
            $note['userId'],
            $this->getNoteDao()->getNoteCountByUserIdAndCourseId($note['userId'], $note['courseId'])
        );

        return $note;
    }
}
