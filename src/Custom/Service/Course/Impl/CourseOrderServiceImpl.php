<?php
namespace Custom\Service\Course\Impl;

use Topxia\Common\StringToolkit;
use Topxia\Service\Course\Impl\CourseOrderServiceImpl as BaseServiceImpl;

class CourseOrderServiceImpl extends BaseServiceImpl
{
    public function applyRefundOrder($id, $amount, $reason, $container)
    {
        $user  = $this->getCurrentUser();
        $order = $this->getOrderService()->getOrder($id);

        if (empty($order)) {
            throw $this->createServiceException('订单不存在，不能申请退款。');
        }

        $refund = $this->getOrderService()->applyRefundOrder($id, $amount, $reason);

        if ($refund['status'] == 'created') {
            $this->getCourseService()->lockStudent($order['targetId'], $order['userId'], $order['currentLoopTime']);

            $setting   = $this->getSettingService()->get('refund');
            $message   = (empty($setting) || empty($setting['applyNotification'])) ? '' : $setting['applyNotification'];
            $course    = $this->getCourseService()->getCourse($order["targetId"]);
            $courseUrl = $container->get('router')->generate('course_show', array('id' => $course['id']));

            if ($message) {
                $variables = array(
                    'item' => "<a href='{$courseUrl}'>{$course['title']}</a>"
                );
                $message = StringToolkit::template($message, $variables);
                $this->getNotificationService()->notify($refund['userId'], 'default', $message);
            }

            $adminmessage = '用户'."{$user['nickname']}".'申请退款'."<a href='{$courseUrl}'>{$course['title']}</a>".'课程，请审核。';
            $adminCount   = $this->getUserService()->searchUserCount(array('roles' => 'ADMIN'));
            $admins       = $this->getUserService()->searchUsers(array('roles' => 'ADMIN'), array('id', 'DESC'), 0, $adminCount);

            foreach ($admins as $key => $admin) {
                $this->getNotificationService()->notify($admin['id'], 'default', $adminmessage);
            }
        } elseif ($refund['status'] == 'success') {
            $this->getCourseService()->removeStudent($order['targetId'], $order['userId'], $order['currentLoopTime']);
        }

        return $refund;
    }

    public function cancelRefundOrder($id)
    {
        $order = $this->getOrderService()->getOrder($id);

        if (empty($order) || $order['targetType'] != 'course') {
            throw $this->createServiceException('订单不存在，取消退款申请失败。');
        }

        $this->getOrderService()->cancelRefundOrder($id);

        if ($this->getCourseService()->isCourseStudent($order['targetId'], $order['userId'], $order['currentLoopTime'])) {
            $this->getCourseService()->unlockStudent($order['targetId'], $order['userId'], $order['currentLoopTime']);
        }
    }
}
