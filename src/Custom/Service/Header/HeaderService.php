<?php

namespace Custom\Service\Header;

interface HeaderService
{
    public function getCertficationForGuest($isAdmin, $user, $course);

    public function getCertification($isAdmin, $user, $course);

    public function getLearnedHours($isAdmin, $user, $course, $certificationCourse, $currentLoopTime);

    public function getTestpaperItem($isTestPassed, $certificationCourse, $course);

    public function getNextLearnLesson($isTestPassed, $certificationCourse, $course, $currentLoopTime, $testpaperItem, $member);

    public function getDateForGuest($certificationCourse);
}