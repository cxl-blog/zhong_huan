<?php

namespace Custom\Service\Header\Impl;

use Custom\Service\Header\HeaderService;
use Topxia\Service\Common\BaseService;
use Custom\Common\Constants\LearnMode;
use Custom\Common\Util\CourseLoopUtils;

class HeaderServiceImpl extends BaseService implements HeaderService
{
    public function getCertficationForGuest($isAdmin, $user, $course)
    {
        if (!$isAdmin && $user['id'] != 0) {
            return null;
        } else {
            return array(
                'learnModeName' => LearnMode::formatSingleCertificationType($course)['certificationType']
            );
        }        
    }

    public function getCertification($isAdmin, $user, $course)
    {
        if (!$isAdmin && $user['id'] != 0) {
            return null;
        } else {
            return $this->getCertificateService()->getCertification($course['certificationId']);
        }                
    }

    public function getLearnedHours($isAdmin, $user, $course, $certificationCourse, $currentLoopTime)
    {
        if (!$isAdmin && $user['id'] != 0) {
            if (empty($certificationCourse)) {
                $certificationCourse = $this->getCertificateService()->getCertificationsForUser($user['id'], false, $course['id'], $currentLoopTime);
            }
            return empty($certificationCourse['learnedHours']) ? 0 : $certificationCourse['learnedHours'];
        } else {
            return 0;
        }
    }

    public function getTestpaperItem($isTestPassed, $certificationCourse, $course)
    {
        if (!$isTestPassed && $certificationCourse['isAllLessonsFinished']) {
            return $testpaperItem = $this->getCourseService()->getTestpaperLesson($course['id']);
        } else {
            return null;
        }        
    }

    public function getNextLearnLesson($isTestPassed, $certificationCourse, $course, $currentLoopTime, $testpaperItem, $member)
    {
        $user = $this->getCurrentUser();

        if (!$isTestPassed && $certificationCourse['isAllLessonsFinished']) {
            return $testpaperItem;
        } else if (!$certificationCourse['isAllLessonsFinished'] && !empty($member)) {
            return $this->getCourseService()->getUserNextLearnLesson($user['id'], $course['id'], $currentLoopTime);
        }
    }

    public function getDateForGuest($certificationCourse)
    {
        $dateForGuest = array('nowTime' => strtotime(date('Y-m-d H:i:s')));

        if (isset($certificationCourse['makeupDeadline'])) {
            $dateForGuest['makeupDeadline'] = strtotime($certificationCourse['makeupDeadline']);
        }
        return $dateForGuest;
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->createService('Course.CourseService');
    }   
}