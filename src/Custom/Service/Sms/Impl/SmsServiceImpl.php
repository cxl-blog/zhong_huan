<?php

namespace Custom\Service\Sms\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Service\Sms\SmsService;
use Topxia\Service\CloudPlatform\CloudAPIFactory;
use Topxia\Service\Sms\Impl\SmsServiceImpl as TopxiaSmsServiceImpl;

class SmsServiceImpl extends TopxiaSmsServiceImpl implements SmsService
{
    /**
     * @see SmsService::sendMsgBySms
     */
    public function sendMsgBySms($userId, $idcard, $params)
    {
        try {
            $api    = CloudAPIFactory::create('leaf');
            $api->post("/sms/send", $params);
        } catch (\RuntimeException $e) {
            throw new \RuntimeException("发送失败！");
        }

        $users = $this->getUserService()->findUsersByIds(array($userId));

        $message = '对';

        if (!empty($users)) {
            $message .= 'idcard: ' . $idcard . ';';
        }

        $smsType = $params['category'];
        $mobiles = array($params['mobile']);
        $message .= '发送用于'.$smsType.'的通知短信';
        $this->getLogService()->info('sms', $smsType, $message, $mobiles);

        return true;
    }

    public function sendVerifyCodeForFindPass($mobile)
    {
        $description = '登录密码重置';
        $targetUser  = $this->getUserService()->getUserByVerifiedMobile($mobile);
        if (empty($targetUser)) {
            return array('error' => '用户不存在');
        }

        $smsCode = $this->generateSmsCode();
        $params = array(
            'mobile' => $mobile,
            'category' => SmsService::FORGET_PASSWORD,
            'description' => '登录密码重置',
            'verify' => $smsCode
        );

        try {
            $api    = CloudAPIFactory::create('leaf');
            $result = $api->post("/sms/{$api->getAccessKey()}/sendVerify", $params);

            if (isset($result['error'])) {
                return array('error' => "发送失败, {$result['error']}");
            }
        } catch (\RuntimeException $e) {
            $message = $e->getMessage();
            return array('error' => "发送失败, {$message}");
        }

        return array(
            'userId' => $targetUser['id'],
            'smsCode' => $smsCode
        );
    }

    public function sendCustomMsg($params, $logMessage = '')
    {
        if (empty($params['mobile'])) {
            return false;
        }

        try {
            $api        = CloudAPIFactory::create('leaf');
            $api->post("/sms/send", $params);
            $sendStatus = true;
        } catch (\RuntimeException $e) {
            $sendStatus = false;
        }

        if ($logMessage) {
            $sendResult = $sendStatus ? '成功' : '失败';
            $logMessage = $logMessage.$sendResult;
            $this->getLogService()->info('sms', $params['category'], $logMessage, $params);
        }

        return $sendStatus;
    }

    public function smsRemindSend($smsType, $students, $description, $parameters = array())
    {
        $users               = $this->getUserService()->findUsersByIds($students['id']);
        $mobiles = ArrayToolkit::column($users, 'verifiedMobile');
        foreach ($parameters['mobile'] as $key => $value) {
            $parameter['student'] = $parameters['student'][$key];
            $parameter['course'] = $parameters['course'];
            $parameter['date'] = $parameters['date'][$key];
            $parameter['days'] = $parameters['days'][$key];
            $parameter['mobile'] = $parameters['mobile'][$key];
            try {
                $api    = CloudAPIFactory::create('leaf');
                $api->post("/sms/send", array('mobile' => $parameter['mobile'], 'category' => $smsType, 'description' => $description, 'parameters' => $parameter));
            } catch (\RuntimeException $e) {
                throw new \RuntimeException("发送失败！");
            }
        }
        $message = '对';

        foreach ($users as $user) {
            $message .= 'userId: '.$user['id'].';nickname: '.$user['nickname'].';verifiedMobile: '.$user['verifiedMobile'].';';
        }

        $message .= '发送用于到期提醒的短信通知';
        $this->getLogService()->info('sms', $smsType, $message, $mobiles);

        return true;
    }

    public function getNeedToRemindStudents($targetType, $memberUserIds, $id)
    {
        $certificationCourse = array();

        $students = '';

        foreach ($memberUserIds as $key => $userId) {
            $user = $this->getUserService()->getUser($userId, false);
            $isAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']);

            if (!$isAdmin && isset($user['verifiedMobile'])) {
                $certificationCourse = $this->getCertificateService()->getCertificationsForUser($userId, false, $id);
                foreach ($certificationCourse as $key => $value) {
                    if (isset($value['learningStatus'])) {
                        $students = $this->findTargetStudent($userId, $user, $value, $targetType, $students);
                    }
                }
            }
        }

        if(!empty($students)){
            $needToRemindStudentsItems['students'] = $students;
        } else{
            $needToRemindStudentsItems['students']['nickname'] = '';
            $needToRemindStudentsItems['students']['leftDays'] = '';
        }

        foreach ($certificationCourse as $key => $value) {
            if(!empty($value)){
                $needToRemindStudentsItems['certificationName'] = self::getCertificationCategory($value['certificationName']);
             } else{
                $needToRemindStudentsItems['certificationName'] = 'xxxx';
            }
            $needToRemindStudentsItems['startTime'] = 'xxxx-xx-xx';
            $needToRemindStudentsItems['deadline'] = '—xxxx-xx-xx' ;
        }

        return $needToRemindStudentsItems;
    }

    public static function getCertificationCategory($certificationName)
    {
        $splitedChars = self::splitStringToCharArrays($certificationName);

        //只取 在 startedChars 和 endedChars之间的字符串
        $startedChars = array('(', '（');
        $endedChars = array(')', '）');

        $result = '';
        $isLetterValid = false;
        foreach ($splitedChars as $char) {
            if (in_array($char, $startedChars)) {
                $isLetterValid = true;
            } else if (in_array($char, $endedChars)) {
                break;
            } else {
                if ($isLetterValid) {
                    $result .= $char;
                }
            }
        }

        return $result;
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function generateSmsCode($length = 6)
    {
        $code = rand(0, 9);

        for ($i = 1; $i < $length; $i++) {
            $code = $code.rand(0, 9);
        }

        return $code;
    }
    
    private static function splitStringToCharArrays($str) {
        $charset = 'utf-8';
        $strlen = mb_strlen($charset);
        while ($strlen) {
            $array[] = mb_substr($str, 0, 1,$charset);
            $str = mb_substr($str, 1, $strlen, $charset);
            $strlen = mb_strlen($str);
        }
        return $array;
    }

    private function getStudents($userId, $user, $leftDays, $students,$certificationCourse)
    {
        $students['id'][]       = $userId;
        $studentsProfile = $this->getCertificateService()->getProfile($userId);
        $students['nickname'][] = $studentsProfile['truename'];
        $students['mobile'][] = $user['verifiedMobile'];
        $students['leftDays'][] = $leftDays;
        $students['term'][] = $certificationCourse['startTime'].'—'.$certificationCourse['deadline'];
        return $students;
    }

    private function findTargetStudent($userId, $user, $certificationCourse, $targetType, $students)
    {
        if ($certificationCourse['learningStatus'] == 'learning' || $certificationCourse['learningStatus'] == 'unPaied') {
            if ($this->isStudentOvertimeSoon($targetType, $certificationCourse['leftDays'])) {
                $students = $this->getStudents($userId, $user, $certificationCourse['leftDays'], $students,$certificationCourse);
            }
        } elseif ($certificationCourse['learningStatus'] == 'makeup' || $certificationCourse['learningStatus'] == 'overtimeMakeupYes') {
            if ($this->isStudentOvertimeSoon($targetType, $certificationCourse['makeupLeftDays'])) {
                $students = $this->getStudents($userId, $user, $certificationCourse['makeupLeftDays'], $students,$certificationCourse);
            }
        }

        return $students;
    }

    private function isStudentOvertimeSoon($targetType, $leftDays)
    {
        return $this->isThreeMonthsLeft($targetType, $leftDays) ||
                $this->isOneMonthsLeft($targetType, $leftDays) ||
                $this->isOneWeekLeft($targetType, $leftDays);
    }

    private function isThreeMonthsLeft($targetType, $leftDays)
    {
        return $targetType == 'threeMonthLeft' && $leftDays >= 30 && $leftDays < 90;
    }

    private function isOneMonthsLeft($targetType, $leftDays)
    {
        return $targetType == 'oneMonthLeft' && $leftDays >= 7 && $leftDays < 30;
    }

    private function isOneWeekLeft($targetType, $leftDays)
    {
        return $targetType == 'oneWeekLeft' && $leftDays >= 0 && $leftDays < 7;
    }
}
