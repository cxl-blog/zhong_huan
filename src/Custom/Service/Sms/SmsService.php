<?php

namespace Custom\Service\Sms;

interface SmsService
{
    //欢迎您来到乐培中国http://www.lepeichina.com，您的初识密码为4e16e9（请勿泄露）。
    const REGIST = 'sms_zhonghuan_login_password';   

    //用户您好！您于#date#获得#title#从业资格，请登录http://www.lepeichina.com进行学习
    // param: date & title
    const SYNC_NEW_CERTIFICATION = 'sms_zhonghuan_title_certificate';  
    
    const FORGET_PASSWORD = 'sms_zhonghuan_forget_password';

    /*
     * Topxia\Service\Sms\Impl\SmsServiceImpl -> smsSend 方法改造而来
     * @param params 格式为
     * array(
     *     'mobile' => $to,
     *     'category' => 'sms_login_password',
     *     'parameters' => array(
     *         'password' => 'password'
     *     )
     *  )
     */
    public function sendMsgBySms($userId, $idcard, $params);

    public function sendVerifyCodeForFindPass($mobile);

    public function sendCustomMsg($params, $logMessage = '');

    public function smsRemindSend($smsType, $students, $description, $parameters);

    public function getNeedToRemindStudents($targetType, $memberUserIds, $id);

}
