<?php
namespace Custom\Service\JobKeeper;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;
use Custom\Common\Constants\DbValue;

/**
 * 该任务由 UpgradeUserCertCourseJob 生成, 主要用于生成用户下一循环的内容
 */
class KeepAliveJob implements Job
{
    public function execute($params)
    {
        $this->getJobDao()->getConnection()->transactional(
            function($conn) {
                $overTime = 3000; //半小时还未执行完, 则视为执行出错, 将 executing 改为 0
                $executingJobs = $this->getJobDao()->searchJobs(
                    array("executing" => DbValue::TRUE), 
                    array('id', ' '), 0, PHP_INT_MAX);
                foreach ($executingJobs as $job) {
                    if ($job['nextExcutedTime'] + $overTime < time()) {
                        $this->getJobDao()->updateJob($job['id'], array('executing' => DbValue::FALSE));
                    }
                }
            }
        );
    }

    protected function getJobDao()
    {
        return $this->getServiceKernel()->createDao('Crontab.JobDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
