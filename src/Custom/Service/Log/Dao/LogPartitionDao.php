<?php
namespace Custom\Service\Log\Dao;

interface LogPartitionDao
{
    public function createPartition();
}