<?php
namespace Custom\Service\Log\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Log\Dao\LogPartitionDao;

class LogPartitionDaoImpl extends BaseDao implements LogPartitionDao {

    public function createPartition()
    {
        // 修改 604800 时, 需要同时修改 Version20170110162712 内的属性
        $partitionTime = time() + 604800;
        $sql = "alter table `log`  ADD PARTITION (PARTITION p{$partitionTime} VALUES LESS THAN ({$partitionTime}));";
        $this->getConnection()->executeUpdate($sql);
    }
}