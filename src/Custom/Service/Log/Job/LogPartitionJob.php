<?php
namespace Custom\Service\Log\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 每天凌晨2点, 给log表新建分区
 */
class LogPartitionJob implements Job
{
    public function execute($params)
    {
        $this->getLogPartitionDao()->createPartition();
    }

    protected function getLogPartitionDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Log.LogPartitionDao');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
