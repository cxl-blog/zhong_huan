<?php

namespace Custom\Service\Testpaper\Dao\Impl;

use Custom\Service\Testpaper\Dao\TestpaperDao;
use Topxia\Service\Testpaper\Dao\Impl\TestpaperDaoImpl as BaseDao;

class TestpaperDaoImpl extends BaseDao implements TestpaperDao
{
    protected $table = 'testpaper';

    private $serializeFields = array(
            'metas' => 'json'
    );

    public function findAllTestpapersByTargetAndStatus($target, $status = 'open')
    {
        $sql = "SELECT * FROM {$this->table} WHERE target = ? And status = ?";
        $results = $this->getConnection()->fetchAll($sql, array($target,$status)) ? : array();
        return $this->createSerializer()->unserialize($results, $this->serializeFields);
    }

    public function findByCourseIdAndTestpaperName($courseId, $testpaperName)
    {
        $sql = "SELECT * FROM {$this->table} WHERE target = ? And name = ? And status = ?";
        $results = $this->getConnection()->fetchAll($sql, array('course-' . $courseId, $testpaperName, 'open')) ? : array();
        return $this->createSerializer()->unserialize($results, $this->serializeFields);
    }
}