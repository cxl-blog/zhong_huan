<?php
namespace Custom\Service\Testpaper\Dao\Impl;

use Custom\Service\Testpaper\Dao\TestpaperItemResultDao;
use Topxia\Service\Testpaper\Dao\Impl\TestpaperItemResultDaoImpl as BaseDao;

class TestpaperItemResultDaoImpl extends BaseDao implements TestpaperItemResultDao
{
    public function deleteUserAllTestpaperItemsResult($userId, $testPaperResultIds)
    {
        if (empty($testPaperResultIds)) {
            return false;
        }

        $testpaperIdsSql = str_repeat('?,', count($testPaperResultIds) - 1).'?';
        $sql             = "DELETE FROM {$this->table} WHERE userId = ? and testPaperResultId in ($testpaperIdsSql)";
        $params          = array_merge(array($userId), $testPaperResultIds);
        $result          = $this->getConnection()->executeUpdate($sql, $params);
        $this->clearCached();
        return $result;
    }
}
