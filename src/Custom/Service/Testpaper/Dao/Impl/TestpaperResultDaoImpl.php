<?php
namespace Custom\Service\Testpaper\Dao\Impl;

use Custom\Service\Testpaper\Dao\TestpaperResultDao;
use Topxia\Service\Testpaper\Dao\Impl\TestpaperResultDaoImpl as BaseDao;

class TestpaperResultDaoImpl extends BaseDao implements TestpaperResultDao
{
    public function findPassedTestResult($target, $userId, $currentLoopTime = null)
    {
        $sql         = "SELECT * FROM {$this->table} WHERE target = ? AND userId = ? AND passedStatus in ('excellent', 'good', 'passed') AND status = 'finished' AND currentLoopTime = ?";
        $testResults = $this->getConnection()->fetchAssoc($sql, array($target, $userId, $currentLoopTime)) ?: null;
        return $testResults ?: null;
    }

    public function deleteUserAllTestpaperResult($userId, $testpaperIds, $currentLoopTime = null)
    {
        $testpaperIdsSql = str_repeat('?,', count($testpaperIds) - 1).'?';
        $sql             = "DELETE FROM {$this->table} WHERE userId = ? and currentLoopTime = ? and testId in ($testpaperIdsSql)";
        $params          = array_merge(array($userId, $currentLoopTime), $testpaperIds);
        $result          = $this->getConnection()->executeUpdate($sql, $params);
        $this->clearCached();
        return $result;
    }

    public function findTestPaperResultsByTestIdAndStatusAndUserId($testpaperId, array $status, $userId, $currentLoopTime = null)
    {
        $marks = str_repeat('?,', count($status) - 1).'?';
        array_push($status, $testpaperId, $userId, $currentLoopTime);
        $sql = "SELECT * FROM {$this->table} WHERE `status` IN ({$marks}) AND `testId` = ? AND `userId` = ? AND `currentLoopTime` = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, $status) ?: null;

    }

    public function findTestpaperResultByTestpaperIdAndUserIdAndActive($testpaperId, $userId, $currentLoopTime = 1)
    {
        $sql = "SELECT * FROM {$this->table} WHERE testId = ? AND userId = ? AND active = 1 AND currentLoopTime = ? ORDER BY id DESC ";
        return $this->getConnection()->fetchAssoc($sql, array($testpaperId, $userId, $currentLoopTime));
    }

    public function findTestpaperResultByTargetAndUserIdAndActive($target, $userId, $currentLoopTime = 1)
    {
        $sql = "SELECT * FROM {$this->table} WHERE target = ? AND userId = ? AND active = 1 AND currentLoopTime = ? ORDER BY id DESC ";
        return $this->getConnection()->fetchAssoc($sql, array($target, $userId, $currentLoopTime));
    }

    public function listTestpaperResultByTargetUserId($target, $userId, $currentLoopTime = 1)
    {
        $sql = "SELECT * FROM {$this->table} WHERE target = ? AND userId = ? AND currentLoopTime = ? ORDER BY id DESC ";
        return $this->getConnection()->fetchAll($sql, array($target, $userId, $currentLoopTime));
    }

    public function findTestpaperResultsByTestpaperIdAndUserId($testpaperId, $userId ,$currentLoopTime = 1)
    {
        $sql = "SELECT * FROM {$this->table} WHERE testId = ? AND userId = ? AND currentLoopTime = ?";
        return $this->getConnection()->fetchAll($sql, array($testpaperId, $userId, $currentLoopTime));
    }

    public function findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime = 1)
    {
        $marks     = str_repeat('?,', count($testpaperIds) - 1).'?';
        $parmaters = array_merge($testpaperIds, array($userId, $currentLoopTime));
        $sql       = "SELECT * FROM {$this->table} WHERE testId IN ({$marks}) AND userId = ? AND currentLoopTime = ?";
        return $this->getConnection()->fetchAll($sql, $parmaters);
    }

    public function updateTestpaperResultActiveByTarget($target, $userId,$currentLoopTime = 1)
    {
        $sql = "UPDATE {$this->table} SET `active` = 0 WHERE `target` = ? AND `userId` = ? AND `active` = 1 AND currentLoopTime = ? ";
        return $this->getConnection()->executeQuery($sql, array($target, $userId,$currentLoopTime));
    }

}
