<?php

namespace Custom\Service\Testpaper\Dao;

interface TestpaperDao
{
    public function findAllTestpapersByTargetAndStatus($target,$status);

    public function findByCourseIdAndTestpaperName($courseId, $testpaperName);
}