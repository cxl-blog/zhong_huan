<?php

namespace Custom\Service\Testpaper\Dao;

interface TestpaperItemResultDao
{
    public function deleteUserAllTestpaperItemsResult($userId, $testpaperResultIds);
}
