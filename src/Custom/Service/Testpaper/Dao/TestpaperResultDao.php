<?php

namespace Custom\Service\Testpaper\Dao;

interface TestpaperResultDao
{
    public function findPassedTestResult($target, $userId, $currentLoopTime = null);

    public function deleteUserAllTestpaperResult($userId, $testpaperIds, $currentLoopTime = null);

    public function findTestPaperResultsByTestIdAndStatusAndUserId($testpaperId, array $status, $userId, $currentLoopTime = null);

    public function findTestpaperResultByTestpaperIdAndUserIdAndActive($testpaperId, $userId, $currentLoopTime = 1);

    public function findTestpaperResultByTargetAndUserIdAndActive($target, $userId, $currentLoopTime = 1);

    public function listTestpaperResultByTargetUserId($target, $userId, $currentLoopTime = 1);

    public function findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime = null);

    public function findTestpaperResultsByTestpaperIdAndUserId($testpaperId, $userId, $currentLoopTime=1);

    public function updateTestpaperResultActiveByTarget($target, $userId,$currentLoopTime=1);
}
