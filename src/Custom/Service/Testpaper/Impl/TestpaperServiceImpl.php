<?php
namespace Custom\Service\Testpaper\Impl;

use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\ServiceEvent;
use Topxia\Service\Testpaper\Impl\TestpaperServiceImpl as BaseServiceImpl;

class TestpaperServiceImpl extends BaseServiceImpl
{
    public function getAllTestpaperIdsByCourseId($courseId)
    {
        $testpapers = $this->findAllTestpapersByTargetAndStatus($courseId);
        $randTestpapers = array();
        foreach ($testpapers as $key => $testpaper) {
            $randTestpapers[] = $testpaper['id'];
        }
        return $randTestpapers;
    }

    public function findAllTestpapersByTargetAndStatus($courseId,$status='open')
    {
        $target = 'course-'.$courseId;
        return $this->getTestpaperDao()->findAllTestpapersByTargetAndStatus($target,$status);
    }

    public function isAllTestResultPassed($userId, $courseId, $currentLoopTime = null)
    {
        $testLessons      = $this->getTestLessonsByCourseId($courseId);

        foreach ($testLessons as $key => $testLesson) {
            $target = sprintf("course-%s/lesson-%s", $courseId, $testLesson['id']);

            $passedTestResults = $this->getTestpaperResultDao()->findPassedTestResult($target, $userId, $currentLoopTime);

            if ($passedTestResults) {
                unset($testLessons[$key]);
            }
        }

        return empty($testLessons);
    }

    public function getTestLessonsByCourseId($courseId)
    {
        $conditions['courseId'] = $courseId;
        $conditions['type']     = 'testpaper';
        $conditions['status']   = 'published';
        $order                  = ['id', ' '];
        return $this->getCourseService()->searchLessons($conditions, $order, 0, PHP_INT_MAX);
    }

    public function deleteUserCourseTestpaperResults($userId, $courseId, $currentLoopTime = null)
    {
        $testpapers = $this->findAllTestpapersByTarget($courseId);

        if (!empty($testpapers)) {
            $testpaperIds       = ArrayToolkit::column($testpapers, 'id');
            $testpaperResults   = $this->getTestpaperResultDao()->findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime);
            $testpaperResultIds = ArrayToolkit::column($testpaperResults, 'id');
            $this->getTestpaperItemResultDao()->deleteUserAllTestpaperItemsResult($userId, $testpaperResultIds);
            $this->getTestpaperResultDao()->deleteUserAllTestpaperResult($userId, $testpaperIds, $currentLoopTime);
        }
    }

    public function canTeacherCheck($id)
    {
        $paper = $this->getTestpaperDao()->getTestpaper($id);

        if (!$paper) {
            throw $this->createServiceException('试卷不存在');
        }

        $user = $this->getCurrentUser();

        if ($user->isAdmin()) {
            return $user['id'];
        }

        if ($this->canBranchAdminMangeTestpaper($paper['id'])) {
            return $user['id'];
        }

        $target = explode('-', $paper['target']);

        if ($target[0] == 'course') {
            $targetId = explode('/', $target[1]);
            $member   = $this->getCourseService()->getCourseMember($targetId[0], $user['id']);

// @todo: 这个是有问题的。

            if ($member['role'] == 'teacher') {
                return $user['id'];
            }

            $classroom = $this->getClassroomService()->findClassroomByCourseId($targetId[0]);

            if (!empty($classroom)) {
                $isTeacher              = $this->getClassroomService()->isClassroomTeacher($classroom['classroomId'], $user['id']);
                $isAssistant            = $this->getClassroomService()->isClassroomAssistant($classroom['classroomId'], $user['id']);
                $isClassroomHeadTeacher = $this->getClassroomService()->isClassroomHeadTeacher($classroom['classroomId'], $user['id']);

                if ($isTeacher || $isAssistant || $isClassroomHeadTeacher) {
                    return $user['id'];
                }
            }
        }

        return false;
    }

    public function canLookTestpaper($resultId)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            throw $this->createAccessDeniedException('未登录用户，无权操作！');
        }

        $paperResult = $this->getTestpaperResult($resultId);

        if (!$paperResult) {
            throw $this->createNotFoundException('试卷不存在!');
        }

        $paper = $this->getTestpaperDao()->getTestpaper($paperResult['testId']);

        if (!$paper) {
            throw $this->createNotFoundException('试卷不存在!');
        }

        if (($paperResult['status'] == 'doing' || $paper['status'] == 'paused') && ($paperResult['userId'] != $user['id'])) {
            throw $this->createNotFoundException('无权查看此试卷');
        }

        if ($user->isAdmin()) {
            return $user['id'];
        }

        if ($this->canBranchAdminMangeTestpaper($paper['id'])) {
            return $user['id'];
        }

        $target = explode('-', $paper['target']);

        if ($target[0] == 'course') {
            $targetId = explode('/', $target[1]);
            $member   = $this->getCourseService()->getCourseMember($targetId[0], $user['id']);

            if ($member['role'] == 'teacher') {
                return $user['id'];
            }

            if ($paperResult['userId'] == $user['id']) {
                return $user['id'];
            }

            $classroom = $this->getClassroomService()->findClassroomByCourseId($targetId[0]);

            if (!empty($classroom)) {
                $isTeacher              = $this->getClassroomService()->isClassroomTeacher($classroom['classroomId'], $user['id']);
                $isAssistant            = $this->getClassroomService()->isClassroomAssistant($classroom['classroomId'], $user['id']);
                $isClassroomHeadTeacher = $this->getClassroomService()->isClassroomHeadTeacher($classroom['classroomId'], $user['id']);

                if ($isTeacher || $isAssistant || $isClassroomHeadTeacher) {
                    return $user['id'];
                }
            }
        }

        return false;
    }

    public function canBranchAdminMangeTestpaper($paperId)
    {
        $paper = $this->getTestpaper($paperId);
        if (empty($paper)) {
            return false;
        }

        $target = explode('-', $paper['target']);
        if ($target[0] == 'course') {
            $courseId = $target[1];
            return $this->getCourseService()->canBranchAdminMangeCourse($courseId);
        }

        return false;
    }

    public function findTestpaperResultByTestpaperIdAndUserIdAndActive($testpaperId, $userId, $currentLoopTime = 1)
    {
        return $this->getTestpaperResultDao()->findTestpaperResultByTestpaperIdAndUserIdAndActive($testpaperId, $userId, $currentLoopTime);
    }

    public function findTestpaperResultByLessonIdAndUserIdAndActive($lessonId, $userId, $currentLoopTime = 1)
    {
        $lesson = $this->getCourseService()->getLesson($lessonId);
        $target = sprintf('course-%d/lesson-%d',$lesson['courseId'],$lessonId);

        return $this->getTestpaperResultDao()->findTestpaperResultByTargetAndUserIdAndActive($target, $userId, $currentLoopTime);
    }

    public function listTestpaperResultByTargetUserId($lessonId, $userId, $currentLoopTime = 1)
    {
        $lesson = $this->getCourseService()->getLesson($lessonId);
        $target = sprintf('course-%d/lesson-%d',$lesson['courseId'],$lessonId);

        return $this->getTestpaperResultDao()->listTestpaperResultByTargetUserId($target, $userId, $currentLoopTime);
    }

    public function findTestpaperResultsByTestIdAndStatusAndUserId($testpaperId, $userId, array $status, $currentLoopTime = null)
    {
        return $this->getTestpaperResultDao()->findTestpaperResultsByTestIdAndStatusAndUserId($testpaperId, $status, $userId, $currentLoopTime);
    }

    public function startTestpaper($id, $target, $currentLoopTime = 1)
    {
        $testpaper = $this->getTestpaperDao()->getTestpaper($id);

        $testpaperResult = array(
            'paperName'       => $testpaper['name'],
            'testId'          => $id,
            'userId'          => $this->getCurrentUser()->id,
            'limitedTime'     => $testpaper['limitedTime'],
            'beginTime'       => time(),
            'active'          => 1,
            'status'          => 'doing',
            'usedTime'        => 0,
            'target'          => empty($target['type']) ? '' : $testpaper['target']."/".$target['type']."-".$target['id'],
            'currentLoopTime' => $currentLoopTime
        );

        return $this->getTestpaperResultDao()->addTestpaperResult($testpaperResult);
    }

    public function findTestpaperResultsByTestpaperIdAndUserId($testpaperId, $userId ,$currentLoopTime = null)
    {
        return $this->getTestpaperResultDao()->findTestpaperResultsByTestpaperIdAndUserId($testpaperId, $userId ,$currentLoopTime);
    }

    public function findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime = null)
    {
        return $this->getTestpaperResultDao()->findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime);
    }

    public function finishTest($id, $userId, $usedTime)
    {
        $itemResults = $this->getTestpaperItemResultDao()->findTestResultsByTestpaperResultId($id);

        $testpaperResult = $this->getTestpaperResultDao()->getTestpaperResult($id);

        $testpaper = $this->getTestpaperDao()->getTestpaper($testpaperResult['testId']);

        $fields['status'] = $this->isExistsEssay($itemResults) ? 'reviewing' : 'finished';

        $accuracy                 = $this->sumScore($itemResults);
        $fields['objectiveScore'] = $accuracy['sumScore'];

        $fields['score'] = 0;

        if (!$this->isExistsEssay($itemResults)) {
            $fields['score'] = $fields['objectiveScore'];
        }

        $fields['rightItemCount'] = $accuracy['rightItemCount'];

        $fields['passedStatus'] = $fields['score'] >= $testpaper['passedScore'] ? 'passed' : 'unpassed';

        $fields['usedTime']    = $usedTime + $testpaperResult['usedTime'];
        $fields['endTime']     = time();
        $fields['active']      = 1;
        $fields['checkedTime'] = time();

        $this->getTestpaperResultDao()->updateTestpaperResultActiveByTarget($testpaperResult['target'], $testpaperResult['userId'],$testpaperResult['currentLoopTime']);

        $testpaperResult = $this->getTestpaperResultDao()->updateTestpaperResult($id, $fields);

        $this->dispatchEvent(
            'testpaper.finish',
            new ServiceEvent($testpaper, array('testpaperResult' => $testpaperResult))
        );

        return $testpaperResult;
    }

    public function isTestpaperAnswerDisplayed($testpaperResultId)
    {
        $testpaperResult = $this->getTestpaperResult($testpaperResultId);

        if ($testpaperResult['passedStatus'] == 'passed') {
            return true;
        }

        $courseId = $this->splitTarget2CourseId($testpaperResult['target']);

        $course = $this->getCourseService()->getCourse($courseId);

        $areaCertificate = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'], $course['certificationId']);

        if ($areaCertificate['showTestpaperAnswer']) {
            return true;
        }

        return false;
    }

    /**
     * @see TestpaperService::createTestpaperResultIfNeed
     */
    public function createTestpaperResultIfNeed($config)
    {
        if (empty($config['isCreate'])) {
            $existedTestpaperResult = $this->findTestpaperResultByLessonIdAndUserIdAndActive(
                $config['lessonId'], 
                $config['userId'], 
                $config['currentLoopTime']
            );

            if (empty($existedTestpaperResult)) {
                return $this->createRandomTestpaperResult($config);
            } else {
                return $existedTestpaperResult;
            }
        } else {
            return $this->createRandomTestpaperResult($config);
        }
    }

    public function searchAndDistinctTestpapers($conditions, $orderBy, $start, $limit)
    {
        $testpapers = $this->searchTestpapers(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            1000
        );

        $distinctTestpapersMap = ArrayToolkit::index($testpapers, 'name');
        $distinctTestpapers = array();
        foreach ($distinctTestpapersMap as $testpaper) {
            array_push($distinctTestpapers, $testpaper);
        }
        return $distinctTestpapers;

    }

    protected function getTestpaperItemResultDao()
    {
        return $this->createDao('Custom:Testpaper.TestpaperItemResultDao');
    }

    protected function getTestpaperResultDao()
    {
        return $this->createDao('Custom:Testpaper.TestpaperResultDao');
    }

    protected function getTestpaperDao()
    {
        return $this->createDao('Custom:Testpaper.TestpaperDao');
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    private function splitTarget2CourseId($target)
    {
        $target = explode('-', $target);

        return $target[1]; 
    }

    private function createRandomTestpaperResult($config)
    {
        $testpaper = $this->getTestpaper($config['testpaperId']);
        $lesson = $this->getCourseService()->getLesson($config['lessonId']);
        $allTestpapers = $this->getTestpaperDao()->findByCourseIdAndTestpaperName($lesson['courseId'], $testpaper['name']);
        $allTestpaperIds = ArrayToolkit::column($allTestpapers, 'id');
        $randomTestpaperId = $this->getRandTestpaper($allTestpaperIds);

        $testpaperResult = $this->startTestpaper(
            $randomTestpaperId, 
            array('type' => 'lesson', 'id' => $config['lessonId']),
            $config['currentLoopTime']
        );
        return $testpaperResult;
    }

}
