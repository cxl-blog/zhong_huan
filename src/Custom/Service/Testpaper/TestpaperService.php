<?php

namespace Custom\Service\Testpaper;

interface TestpaperService
{
    public function getAllTestpaperIdsByCourseId($courseId);
    	
    public function isAllTestResultPassed($userId, $courseId, $currentLoopTime = null);

    public function getTestLessonsByCourseId($courseId);

    public function deleteUserCourseTestpaperResults($userId, $courseId, $currentLoopTime = null);

    public function findTestpaperResultsByTestIdAndStatusAndUserId($testpaperId, $userId, array $status, $currentLoopTime = null);

    public function findTestpaperResultByTestpaperIdAndUserIdAndActive($testpaperId, $userId, $currentLoopTime = 1);

    public function findTestpaperResultByLessonIdAndUserIdAndActive($lessonId, $userId, $currentLoopTime = 1);

    public function listTestpaperResultByTargetUserId($lessonId, $userId, $currentLoopTime = 1);

    public function startTestpaper($id, $target, $currentLoopTime = 1);

    public function findTestpaperResultsByTestpaperIdAndUserId($testpaperId, $userId, $currentLoopTime);

    public function findTestpaperResultsByTestpaperIdsAndUserId($testpaperIds, $userId, $currentLoopTime = null);

    public function findAllTestpapersByTargetAndStatus($id,$status = 'open');

    public function isTestpaperAnswerDisplayed($testpaperResultId);

    /**
     * 当该用户已经有考试记录， 如果 isCreated = false, 则返回已有的考试记录，
     * 如果该用户没有考试记录， 或 isCreated = true, 则生成考试记录， 
     *  考试记录会从考试课时中指定的同名试卷中随机抽取
     *
     * @param config 
     * {
     *   'testpaperId': 1,     //必填，课时设置的试卷id 或上次参加考试的试卷
     *   'userId': 1,          //必填，用户id
     *   'lessonId': 1,        //必填，课时id
     *   'currentLoopTime': 1, //必填
     *   'isCreate': true      //如果为true， 则视为新建随机试卷， 默认为false
     * }
     * @return testpaperResult
     */
    public function createTestpaperResultIfNeed($config);

    public function searchAndDistinctTestpapers($conditions, $orderBy, $start, $limit);
}
