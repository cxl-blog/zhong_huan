<?php

namespace Custom\Service\File\Impl;

use Custom\Service\File\UploadFileService;
use Topxia\Service\File\Impl\UploadFileServiceImpl as BaseUploadFileServiceImpl;
    
class UploadFileServiceImpl extends BaseUploadFileServiceImpl implements UploadFileService
{
    public function generateMissLengthVideoLength()
    {
        $files = $this->getUploadFileDao()->findNoLengthVideos();
        if (isset($files)) {
            foreach ($files as $key => $file) {
                $info = $this->getMediaInfo($file['hashId'], $file['type']);
                $updatedFields = array(
                    'length' => $info['format']['duration']
                );
                $this->getUploadFileDao()->updateFile($file['id'], $updatedFields);
            }
        }
    }

    public function uploadBase64Img($userId, $base64ImgData, $publicUploadDir, $publicUploadPath)
    {
        try {
            $uploadFolder = $publicUploadDir.'/approval/';
            $img          = str_replace(' ', '+', $base64ImgData);
            $data         = base64_decode($img);

            if (!file_exists($uploadFolder)) {
                mkdir($uploadFolder);
            }

            $filename = date("Y-m-d").uniqid().'.jpg';
            $filePath = $uploadFolder.$filename;
            file_put_contents($filePath, $data);

            return $publicUploadPath.'/approval/'.$filename;
        } catch (\RuntimeException $e) {
            return "";
        }
    }

    protected function getUploadFileDao()
    {
        return $this->createDao('Custom:File.UploadFileDao');
    }
    
}
