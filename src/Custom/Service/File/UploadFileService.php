<?php
namespace Custom\Service\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface UploadFileService
{   
    /** 
     * 给没有长度的云视频生成长度
     * @return [type] [description]
     */
    public function generateMissLengthVideoLength();

    public function uploadBase64Img($userId, $base64ImgData, $publicUploadDir, $publicUploadPath);
}
