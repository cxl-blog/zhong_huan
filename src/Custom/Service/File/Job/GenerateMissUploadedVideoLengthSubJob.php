<?php
namespace Custom\Service\File\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 对于没生成长度的云视频文件, 重新生成长度
 */
class GenerateMissUploadedVideoLengthSubJob implements Job
{
    public function execute($params)
    {
        $this->getUploadFileService()->generateMissLengthVideoLength();
    }

    protected function getUploadFileService()
    {
        return $this->getServiceKernel()->createService('Custom:File.UploadFileService');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
