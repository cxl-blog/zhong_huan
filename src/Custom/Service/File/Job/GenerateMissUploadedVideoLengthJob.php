<?php
namespace Custom\Service\File\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 每小时执行一次, 每次生成 4个子任务, 每隔15分钟执行一次
 */
class GenerateMissUploadedVideoLengthJob implements Job
{
    public function execute($params)
    {
        $generateNum = 4;          //每小时4个任务, 每15分钟 执行一个任务 
        $jobInterval = 900;
        $nextJobSeconds = time() + $jobInterval;  //15分钟执行一个子任务
        for ($i = 1;  $i <= $generateNum; $i++) {
            $subJob = array(
                'name'       => "GenerateMissUploadedVideoLengthSubJob({$i})",
                'cycle'      => 'once',
                'jobClass'   => 'Custom\\Service\\File\\Job\\GenerateMissUploadedVideoLengthSubJob',
                'time' => $nextJobSeconds
            );
            $this->getCrobtabService()->createJob($subJob);
            $nextJobSeconds += $jobInterval;
        }
    }

    protected function getCrobtabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
