<?php

namespace Custom\Service\File\Dao;

interface UploadFileDao
{
    /**
     * 找出所有没有长度的视频文件
     */
    public function findNoLengthVideos();
}