<?php

namespace Custom\Service\File\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Topxia\Service\File\Dao\Impl\UploadFileDaoImpl as BaseUploadFileDaoImpl;
use Custom\Service\File\Dao\UploadFileDao;
    
class UploadFileDaoImpl extends BaseUploadFileDaoImpl implements UploadFileDao
{
    public function findNoLengthVideos()
    {
        $sql = "SELECT * FROM {$this->table} WHERE type = ? and length = ?";
        return $this->getConnection()->fetchAll($sql, array('video', '0')) ? : null;
    }
}