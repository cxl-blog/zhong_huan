<?php
namespace Custom\Service\Importer;

use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Topxia\Common\FileToolkit;
use Topxia\Service\Importer\CourseUserImporterProcessor as BaseImporterProcessor;
use Topxia\Service\Importer\ImporterProcessor;
use Custom\Common\Util\UserImportUtils;

class CourseUserImporterProcessor extends BaseImporterProcessor implements ImporterProcessor
{
    public function validateExcelFile($file)
    {
        $errorMessage = '';

        $errorMessage = UserImportUtils::isFileTypeCorrect($file, $errorMessage);

        if (!empty($errorMessage)) {
            return $errorMessage;
        }

        $this->excelAnalyse($file);

        $errorMessage = UserImportUtils::isFileContentCorrect($this->excelFields, $this->necessaryFields, $this->rowTotal, $errorMessage);

        if (!empty($errorMessage)) {
            return $errorMessage;
        }

        if (!$this->checkNecessaryFields($this->excelFields)) {
            $errorMessage = '缺少必要的字段';
            return $errorMessage;
        }

        return $errorMessage;
    }
}
