<?php
namespace Custom\Service\Importer;

use Custom\Service\Importer\ImporterProcessor;
use \Exception;

class ImporterProcessorFactory
{

	public static function create($target)
    {
    	if(empty($target) || !in_array($target,array('classroom','course','certificateCourse'))) {
    		throw new Exception("用户导入类型不存在");
    	}

    	$class = __NAMESPACE__ . '\\' . ucfirst($target). 'UserImporterProcessor';

    	return new $class();
    }

}


