<?php
namespace Custom\Service\Importer;

use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Topxia\Common\FileToolkit;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\UserImportUtils;

class CertificateCourseUserImporterProcessor implements ImporterProcessor
{
    protected $necessaryFields = array(
        'areaName' => '行政区域', 
        'idcard'=>'身份证', 
        'verifiedMobile' => '手机', 
        'category' => '证书名称', 
        'awardTime'=>'颁发时间',
        'startPeriod'=>'周期开始时间', 
        'endPeriod'=>'周期结束时间', 
        'amount'=>'付款金额'
    );

    protected $exceptionFields = array('buyTime','amount');
    protected $objWorksheet;
    protected $rowTotal         = 0;
    protected $colTotal         = 0;
    protected $excelFields      = array();
    protected $passValidateUser = array();
    protected $excelExample     = 'bundles/customweb/example/certificate_coursemember_import_example.xls';
    protected $validateRouter   = 'certification_course_manage_student_import';
    protected $importingRouter  = 'certification_course_manage_student_to_base';

    public function validateExcelFile($file)
    {
        $errorMessage = '';

        $errorMessage = UserImportUtils::isFileTypeCorrect($file, $errorMessage);

        if (!empty($errorMessage)) {
            return $errorMessage;
        }

        $this->excelAnalyse($file);

        $errorMessage = UserImportUtils::isFileContentCorrect($this->excelFields, $this->necessaryFields, $this->rowTotal, $errorMessage);

        if (!empty($errorMessage)) {
            return $errorMessage;
        }

        if (!$this->checkNecessaryFields($this->excelFields)) {
            $errorMessage = '缺少必要的字段';
            return $errorMessage;
        }

        return $errorMessage;
    }

    public function excelAnalyse($file)
    {
        $objPHPExcel        = PHPExcel_IOFactory::load($file);
        $objWorksheet       = $objPHPExcel->getActiveSheet();
        $highestRow         = $objWorksheet->getHighestRow();
        $highestColumn      = $objWorksheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
        $excelFields        = array();

        for ($col = 0; $col < $highestColumnIndex; $col++) {
            $fieldTitle                                  = $objWorksheet->getCellByColumnAndRow($col, 2)->getValue();
            empty($fieldTitle) ? '' : $excelFields[$col] = $this->trim($fieldTitle);
        }

        $rowAndCol = array('rowLength' => $highestRow, 'colLength' => $highestColumnIndex);

        $this->objWorksheet = $objWorksheet;
        $this->rowTotal     = $highestRow;
        $this->colTotal     = $highestColumnIndex;
        $this->excelFields  = $excelFields;

        return array($objWorksheet, $rowAndCol, $excelFields);
    }

    public function getExcelFieldsValue()
    {
        $columnsData = array();

        for ($col = 0; $col < $this->colTotal; $col++) {
            $infoData          = $this->objWorksheet->getCellByColumnAndRow($col, 2)->getFormattedValue();
            $columnsData[$col] = $infoData."";
        }

        return $columnsData;
    }

    public function validExcelFieldValue($userData, $row, $fieldCol,$targetObject=array())
    {   

        $errorInfo = '';

        if($errorInfo = $this->checkEmpty($userData, $row, $fieldCol))
        {
            return $errorInfo;
        }

        list($errorInfo,$user) = $this->checkUserInfo($userData,$row,$fieldCol);
        if($errorInfo)
        {
            return $errorInfo;
        }
        
        if($errorInfo = $this->checkUserArea($userData,$row,$fieldCol,$user,$targetObject))
        {
            return $errorInfo;
        }        

        if($errorInfo = $this->checkUserCertificate($userData,$row,$fieldCol,$user,$targetObject))
        {
            return $errorInfo;
        }

        return $errorInfo;
    }

    protected function checkUserCertificate($userData,$row,$fieldCol,$user,$targetObject)
    {
        if(!empty($userData['amount']) && !is_numeric($userData['amount']))
        {
            return "第{$row}行的信息有误，付款金额不是数字，请检查。";
        }

        $certificate = $this->getCertificateService()->findCertificateByName($userData['category']);

        if(empty($certificate))
        {
            return "第{$row}行的信息有误，系统中没有该证书信息《{$userData['category']}》，请检查。";
        }

        if($certificate['id']!=$targetObject['certificationId'])
        {
            return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）证书《{$userData['category']}》与目标课程证书不一致，请检查。";
        }

        $userCertificate = $this->getCertificateService()->findCertificationIdAndUserId($user['id'], $certificate['id']);

        if(empty($userCertificate))
        {
            return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）没有该证书信息《{$userData['category']}》，请检查。";
        }
        if($userCertificate['isRevoked'])
        {
            return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）该证书《{$userData['category']}》已吊销，请检查。";
        }

        try {
            $userData['awardTime'] = strtotime(trim($userData['awardTime'] . ' 00:00:00'));
        } catch (\Exception $e) {
            return "第{$row}行的信息有误，颁发时间可能有多余的空格或者格式错误（请使用：年-月-日  的格式），请检查。";
        }

        $userData['startPeriod'] = trim($userData['startPeriod']);
        $userData['endPeriod'] = trim($userData['endPeriod']);

        if(date('YmdHi',$userData['awardTime']) != date('YmdHi',$userCertificate['awardTime']))
        {
            return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）证书颁发时间(".date('Y-m-d H:i:s',$userData['awardTime']) . ")与系统中(" . date('Y-m-d H:i:s',$userCertificate['awardTime']) . ")不符，请检查。";
        }

        $toLearnCourse= $this->getCertificateService()->getCertificateCourseByBuyTime($user['id'],$targetObject['id'],$userData['startPeriod'],$userData['endPeriod'],false);

        if(empty($toLearnCourse))
        {
            return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）尚不能购买或已购买该证书课程《{$targetObject['title']}》，请检查。";
        }

        if(!$toLearnCourse['buyable'])
        {
            $isCourseStudent = $this->getCourseService()->isCourseStudent($targetObject['id'], $user['id'], $toLearnCourse['currentLoopTime']);
            if($isCourseStudent)
            {
                return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）已经购买该证书课程《{$targetObject['title']}）》 了，请检查。";
            }else
            {
                return "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）该证书课程《{$targetObject['title']}》不可购买，请检查。";
            }            
        }
        return false;
    }

    protected function checkUserArea($userData,$row,$fieldCol,$user,$targetObject)
    {        
        $currentUser = $this->getUserService()->getCurrentUser();
        if (!$this->getUserService()->hasAdminRoles($currentUser['id']) && !$this->getCourseService()->canBranchAdminArea($currentUser['id'],$user['areaCode'])) {
            return "第{$row}行的信息有误，该学员不属于本管理员所管理的分校，请检查。";
        }
        $area = $this->getAreaService()->getAreaByCode($user['areaCode']);
        if ($area['name'] != $userData['areaName']) {
            return "第{$row}行的信息有误，该学员导入excel中行政区域({$userData['areaName']})与系统中行政区域({$area['name']})不符，请检查。";
        }
        $targetArea = $this->getAreaService()->getAreaByCode($targetObject['areaCode']);
        if ($area['code'] != $targetObject['areaCode']) {
            return "第{$row}行的信息有误，导入学员（{$area['name']}）不属于本证书课程《{$targetObject['title']}》所在行政区（{$targetArea['name']}），请检查。";
        }
        return false;
    }

    protected function checkUserInfo($userData,$row,$fieldCol)
    {
        
        $errorInfo = '';
        $user = $this->getUserService()->getUserByVerifiedMobileAndAreaName($userData['verifiedMobile'], $userData['areaName']);
        if (empty($user)) {
            $errorInfo = "第{$row}行的信息有误，系统中未找到{$userData['areaName']}行政区域下该导入学员手机号({$userData['verifiedMobile']})，请检查。";
            return array($errorInfo,array());
        }
        if ($user['toLearn']) {
            $errorInfo = "第{$row}行的信息有误，导入学员（{$userData['verifiedMobile']}）未注册，请检查。";
            return array($errorInfo,array());
        }

        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        if ($userProfile['idcard'] != $userData['idcard']) {
            $errorInfo = "第{$row}行的信息有误，导入学员（{$userData['idcard']}）身份证与系统中不一致，请检查。";
            return array($errorInfo,array());
        }
        $user['profile'] = $userProfile;
        return array($errorInfo,$user);
    }

    protected function checkEmpty($userData, $row, $fieldCol)
    {   
        $exceptionFields = $this->exceptionFields;
        foreach($userData as $key=>$data)
        {
            if(empty($data) && !in_array($key,$exceptionFields))
            {
                return "第{$row}行的信息有误，{$this->necessaryFields[$key]}信息为空，请检查。";
            }
        }
    }

    public function checkRepeatData()
    {
        // $errorInfo   = array();
        // $checkFields = array_keys($this->necessaryFields);
        // $fieldSort   = $this->getFieldSort();

        // foreach ($checkFields as $checkField) {
        //     $nicknameData = array();

        //     foreach ($fieldSort as $key => $value) {
        //         if ($value['fieldName'] == $checkField) {
        //             $nickNameCol = $value['num'];
        //         }
        //     }

        //     for ($row = 3; $row <= $this->rowTotal; $row++) {
        //         $nickNameColData = $this->objWorksheet->getCellByColumnAndRow($nickNameCol, $row)->getValue();

        //         $nicknameData[] = $nickNameColData."";
        //     }

        //     $info = $this->arrayRepeat($nicknameData, $nickNameCol);

        //     empty($info) ? '' : $errorInfo[] = $info;
        // }

        return array();
    }

    public function arrayRepeat($array, $nickNameCol)
    {
        $repeatArray      = array();
        $repeatArrayCount = array_count_values($array);
        $repeatRow        = "";

        foreach ($repeatArrayCount as $key => $value) {
            if ($value > 1 && !empty($key)) {
                $repeatRow .= '第'.($nickNameCol + 1)."列重复:<br>";

                for ($i = 1; $i <= $value; $i++) {
                    $row = array_search($key, $array) + 3;

                    $repeatRow .= "第".$row."行"."    ".$key."<br>";

                    unset($array[$row - 3]);
                }
            }
        }

        return $repeatRow;
    }

    public function getFieldSort()
    {
        $fieldSort       = array();
        $necessaryFields = $this->necessaryFields;
        $excelFields     = $this->excelFields;

        foreach ($excelFields as $key => $value) {
            if (in_array($value, $necessaryFields)) {
                foreach ($necessaryFields as $fieldKey => $fieldValue) {
                    if ($value == $fieldValue) {
                        $fieldSort[$fieldKey] = array("num" => $key, "fieldName" => $fieldKey);
                        break;
                    }
                }
            }
        }

        return $fieldSort;
    }

    public function checkNecessaryFields($excelFields)
    {
        $necessaryFields = $this->necessaryFields;
        if ($necessaryFields = array_intersect($necessaryFields, array_values($excelFields))) {
            return true;
        }

        return false;
    }

    public function getUserData($targetObject=array())
    {
        $userCount   = 0;
        $fieldSort   = $this->getFieldSort();
        $validate    = array();
        $allUserData = array();

        for ($row = 3; $row <= $this->rowTotal; $row++) {
            for ($col = 0; $col < $this->colTotal; $col++) {
                $infoData          = $this->objWorksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                $columnsData[$col] = $infoData."";
            }

            foreach ($fieldSort as $sort) {
                $userData[$sort['fieldName']] = $columnsData[$sort['num']];
                $fieldCol[$sort['fieldName']] = $sort['num'] + 1;
            }

            $emptyData = array_count_values($userData);

            if (isset($emptyData[""]) && count($userData) == $emptyData[""]) {
                continue;
            }

            $info                            = $this->validExcelFieldValue($userData, $row, $fieldCol,$targetObject);
            empty($info) ? '' : $errorInfo[] = $info;

            $userCount     = $userCount + 1;
            $allUserData[] = $userData;

            if (empty($errorInfo)) {
                if (!empty($userData['nickname'])) {
                    $user = $this->getUserService()->getUserByNickname($userData['nickname']);
                } elseif (!empty($userData['email'])) {
                    $user = $this->getUserService()->getUserByEmail($userData['email']);
                } elseif (!empty($userData['verifiedMobile'])) {
                    $user = $this->getUserService()->getUserByVerifiedMobile($userData['verifiedMobile']);
                }

                $validate[] = array_merge($user, array('row' => $row));
            }

            unset($userData);
        }

        $this->passValidateUser = $validate;
        $allUserData            = json_encode($allUserData);

        $data['errorInfo']   = empty($errorInfo) ? array() : $errorInfo;
        $data['checkInfo']   = empty($checkInfo) ? array() : $checkInfo;
        $data['userCount']   = $userCount;
        $data['allUserData'] = empty($allUserData) ? array() : $allUserData;

        return $data;
    }

    public function checkPassedRepeatData()
    {
        $passedUsers = $this->passValidateUser;
        $ids         = array();
        $repeatRow   = array();
        $repeatIds   = array();

        foreach ($passedUsers as $key => $passedUser) {
            if (in_array($passedUser['id'], $ids) && !in_array($passedUser['id'], $repeatIds)) {
                $repeatIds[] = $passedUser['id'];
            } else {
                $ids[] = $passedUser['id'];
            }
        }

        foreach ($passedUsers as $key => $passedUser) {
            if (in_array($passedUser['id'], $repeatIds)) {
                $repeatRow[$passedUser['id']][] = $passedUser['row'];
            }
        }

        $repeatRowInfo = '';
        $repeatArray   = array();

        if (!empty($repeatRow)) {
            $repeatRowInfo .= "字段对应用户数据重复</br>";

            foreach ($repeatRow as $row) {
                $repeatRowInfo .= "重复行：</br>";

                foreach ($row as $value) {
                    $repeatRowInfo .= "第".$value."行 ";
                }

                $repeatRowInfo .= "</br>";

                $repeatArray[] = $repeatRowInfo;
                $repeatRowInfo = '';
            }
        }

        return $repeatArray;
    }

    public function tryManage($targetId)
    {
        return $this->getCourseService()->tryManageCourse($targetId);
    }

    public function getExcelExample()
    {
        return $this->excelExample;
    }

    public function getExcelInfoValidateUrl()
    {
        return $this->validateRouter;
    }

    public function setExcelInfoValidateUrl($url)
    {
        $this->validateRouter = $url;
    }

    public function getExcelInfoImportUrl()
    {
        return $this->importingRouter;
    }

    public function excelDataImporting($targetObject, $userData, $userUrl, $currentUser = null)
    {
        $existsUserCount = 0;
        $successCount    = 0;
        $errorCount = 0;

        $userProfile = $this->getUserService()->getUserProfile($currentUser['id']);
        foreach ($userData as $key => $user) {
            $importUser = $user;
            $user = $this->getUserService()->getUserByVerifiedMobileAndAreaName($user['verifiedMobile'], $user['areaName']);
            $certificate = $this->getCertificateService()->findCertificateByName($importUser['category']);
            $course = $this->getCertificateService()->getCertificateCourseByBuyTime($user['id'],$targetObject['id'],$importUser['startPeriod'],$importUser['endPeriod']);

            if(empty($course))
            {
                $errorCount++;
                continue;
            }
            
            $isCourseTeacher = $this->getCourseService()->isCourseTeacher($course['id'], $user['id']);
            if ($isCourseTeacher) {
                $errorCount++;
            } else {

                if($this->getCourseService()->isCourseStudent($course['id'], $user['id'], $course['currentLoopTime']))
                {
                    $existsUserCount++;
                    $this->getCourseService()->removeStudent($course['courseId'], $user['id'],$course['currentLoopTime']);
                } 
                $this->getCourseService()->clearCourseHistory($user['id'], $course['courseId'], $course['currentLoopTime']);                              

                $amount = empty($importUser['amount']) ? $course['price'] : round($importUser['amount'],2); 
                $currentUser = $this->getUserService()->getCurrentUser();
                $order = $this->getOrderService()->createOrder(array(
                    'userId'     => $user['id'],
                    'title'      => "购买课程《{$targetObject['title']}》({$userProfile['truename']}添加)",
                    'targetType' => 'course',
                    'targetId'   => $targetObject['id'],
                    'amount'     => $amount,
                    'totalPrice' => $amount,
                    'payment'    => $amount ? 'offlinepay' : 'none',
                    'snPrefix'   => 'CR',
                    'currentLoopTime' => $course['currentLoopTime']
                ));

                $this->getOrderService()->payOrder(array(
                    'sn'       => $order['sn'],
                    'status'   => 'success',
                    'amount'   => $amount,
                    'paidTime' => time()
                ));

                $this->createRMBFlow($order);

                $info = array(
                    'orderId' => $order['id'],
                    'note'    => '通过批量导入添加',
                    'currentLoopTime' =>$course['currentLoopTime']
                );

                if ($this->getCertificationCourseService()->becomeStudent($order['targetId'], $order['userId'], $info)) {
                    $successCount++;
                };

                $member = $this->getCourseService()->getCourseMember($targetObject['id'], $user['id'],$course['currentLoopTime']);

                $message = array(
                    'courseId'    => $targetObject['id'],
                    'courseTitle' => $targetObject['title'],
                    'userId'      => $currentUser['id'],
                    'userName'    => $currentUser['nickname'],
                    'type'        => 'create');

                $this->getNotificationService()->notify($member['userId'], 'course-student', $message);

                $this->getLogService()->info('course', 'add_student', "课程《{$targetObject['title']}》(#{$targetObject['id']})，添加学员{$user['nickname']}(#{$user['id']})，备注：通过批量导入添加");
            }
        }

        return array('existsUserCount' => $existsUserCount, 'successCount' => $successCount,'errorCount'=>$errorCount);
    }

    protected function trim($data)
    {
        $data = trim($data);
        $data = str_replace(" ", "", $data);
        $data = str_replace('\n', '', $data);
        $data = str_replace('\r', '', $data);
        $data = str_replace('\t', '', $data);

        return $data;
    }

    protected function createRMBFlow($order)
    {
        if ($order['amount'] > 0) {
            $inFlow = array(
                'userId'   => $order['userId'],
                'amount'   => $order['amount'],
                'name'     => '入账',
                'orderSn'  => $order['sn'],
                'category' => 'outflow',
                'note'     => '',
                'payment'  => $order['payment']
            );

            $rmbInFlow = $this->getCashService()->inflowByRmb($inFlow);

            $rmbOutFlow = array(
                'userId'   => $order['userId'],
                'amount'   => $order['amount'],
                'name'     => $order['title'],
                'orderSn'  => $order['sn'],
                'category' => 'inflow',
                'note'     => '',
                'parentSn' => $rmbInFlow['sn']
            );
            $this->getCashService()->outflowByRmb($rmbOutFlow);
        }
    }

    protected function getCertificateService()
    {
        return ServiceKernel::instance()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return ServiceKernel::instance()->createService('Custom:Area.AreaService');
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('Custom:User.UserService');
    }

    protected function getCourseService()
    {
        return ServiceKernel::instance()->createService('Custom:Course.CourseService');
    }

    protected function getCertificationCourseService()
    {
        return ServiceKernel::instance()->createService('Custom:Course.CertificationCourseService');
    }

    protected function getOrderService()
    {
        return ServiceKernel::instance()->createService('Custom:Order.OrderService');
    }

    protected function getNotificationService()
    {
        return ServiceKernel::instance()->createService('User.NotificationService');
    }

    protected function getLogService()
    {
        return ServiceKernel::instance()->createService('System.LogService');
    }

    protected function getCashService()
    {
        return ServiceKernel::instance()->createService('Cash.CashService');
    }
}
