<?php

namespace Custom\Service\User\Impl;

class ToLearnUserServiceImpl extends UserServiceImpl
{
    protected function getUserDao()
    {
        return $this->createDao('Custom:User.ToLearnUserDao');
    }
}
