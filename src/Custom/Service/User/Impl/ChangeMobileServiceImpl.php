<?php
namespace Custom\Service\User\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\User\ChangeMobileService;

class ChangeMobileServiceImpl extends BaseService implements ChangeMobileService
{
    public function addChangeMobileInfo($fields)
    {
        return $this->getChangeMobileDao()->addChangeMobileInfo($fields);
    }

    public function updateChangeMobileInfo($userId, $mobile)
    {
        $changeMobileInfo = $this->findChangeMobileInfoByUserId($userId);
        if(empty($changeMobileInfo)){
            $userInfo = $this->getUserService()->getUser($userId);
            if($userInfo['verifiedMobile'] != $mobile){
                $fields = array(
                    'userId' => $userId,
                    'mobile' => $mobile
                );
                $this->addChangeMobileInfo($fields);
            }
        }
        else{
            $fields = array(
                'mobile' => $mobile,
                'createdTime' => time()
            );
            $this->getChangeMobileDao()->updateChangeMobileInfo($changeMobileInfo['id'], $fields);
        }
    }

    public function updateBranchChangeMobileInfo($userId, $mobile)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        $curCount = 0;
        foreach ($userIds as $userId) {
            $this->updateChangeMobileInfo($userId, $mobile);
            $this->getUserService()->changeMobile($userId, $mobile, time(), $curCount);
            $curCount++;
        }
    }

    public function findChangeMobileInfoById($id)
    {
        return $this->getChangeMobileDao()->findChangeMobileInfoById($id);
    }

    public function findChangeMobileInfoByUserId($userId)
    {
        return $this->getChangeMobileDao()->findChangeMobileInfoByUserId($userId);
    }

    public function searchChangeMobileInfoCount()
    {   
        return $this->getChangeMobileDao()->searchChangeMobileInfoCount();
    }

    public function searchChangeMobileInfosByConditions($start,$limit)
    {
        return $this->getChangeMobileDao()->searchChangeMobileInfosByConditions($start,$limit);
    }

    public function delChangeMobileInfosById($id)
    {   
        return $this->getChangeMobileDao()->delChangeMobileInfosById($id);
    }

    protected function getChangeMobileDao()
    {
        return $this->createDao('Custom:User.ChangeMobileDao');
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.userService');
    }
}
