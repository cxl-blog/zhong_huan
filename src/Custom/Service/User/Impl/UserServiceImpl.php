<?php

namespace Custom\Service\User\Impl;

use Topxia\Common\SimpleValidator;
use Topxia\Service\Common\ServiceKernel;
use Custom\Service\User\UserService;
use Custom\Common\Constants\DbValue;
use Topxia\Service\Common\ServiceEvent;
use Topxia\Common\ArrayToolkit;
use Topxia\Service\User\Impl\UserServiceImpl as TopxiaUserServiceImpl;

class UserServiceImpl extends TopxiaUserServiceImpl implements UserService
{
    public function searchUserCount(array $conditions)
    {
        return $this->getUserDao()->searchUserCount($conditions);
    }

    public function getUserByLoginField($keyword)
    {
        if (SimpleValidator::email($keyword)) {
            $user = $this->getUserDao()->findRegistedUserByEmail($keyword);
        } elseif (SimpleValidator::mobile($keyword)) {
            $user = $this->getUserDao()->findRegistedUserByVerifiedMobile($keyword);
        } else {
            $user = $this->getUserDao()->findRegistedUserByIdcard($keyword);
        }

        return !$user ? null : UserSerialize::unserialize($user);
    }

    public function getUserByLoginFieldAndAreaCode($keyword, $areaCode)
    {
        if (SimpleValidator::email($keyword)) {
            $user = $this->getUserDao()->findRegistedUserByEmail($keyword);
        } elseif (SimpleValidator::mobile($keyword)) {
            $user = $this->getUserDao()->findRegistedUserByVerifiedMobileAndAreaCode($keyword, $areaCode);
        } else {
            $user = $this->getUserDao()->findRegistedUserByIdcardAndAreaCode($keyword, $areaCode);
        }

        return !$user ? null : UserSerialize::unserialize($user);
    }

    public function findAreaCodesByKeyword($keyword)
    {
        if (SimpleValidator::mobile($keyword)) {
            $userProfiles = $this->getProfileDao()->findAllUserProfilesByMobile($keyword);
        } else {
            $userProfiles = $this->getProfileDao()->findAllUserProfilesByIdcard($keyword);
        }
        if (!empty($userProfiles)) {
            $users = $this->getUserDao()->findUsersByIds(ArrayToolkit::column($userProfiles, 'id'));
            $areaCodes = $this->getAreaService()->getAreaByCodes(ArrayToolkit::column($users, 'areaCode'));

            return $areaCodes;
        }

        return array();
    }

    public function isIdCardAvaliable($idcard)
    {
        if (empty($idcard)) {
            return false;
        }

        $userProfile = $this->getProfileDao()->findUnregistedProfileByIdCard($idcard);
        return empty($userProfile) ? false : true;
    }

    public function findUserProfileByIdcard($idcard)
    {
        return $this->getProfileDao()->findProfileByIdCard($idcard);
    }

    public function findUserProfileByIdCardAndAreaCode($idCard, $areaCode)
    {
        if (empty($areaCode)) {
            return $this->getProfileDao()->findProfileByIdCard($idCard);
        } else {
            $profiles = $this->getProfileDao()->findAllUserProfilesByIdcard($idCard);
            $userIds = ArrayToolkit::column($profiles, 'id');
            return $this->getUserDao()->getUserByIdsAndAreaCode($userIds, $areaCode);
        }
    }

    public function findAllUserProfilesByIdcard($idcard)
    {
        return $this->getProfileDao()->findAllUserProfilesByIdcard($idcard);
    }

    public function countUserProfileByIdcard($idcard)
    {
        return $this->getProfileDao()->countUserProfileByIdcard($idcard);
    }

    public function findUserByStuId($stuId)
    {
        return $this->getUserDao()->findUserByStuId($stuId);
    }

    public function checkMobileValiableWithIdcard($mobile,$idcard)
    {
        $user = $this->getUserByVerifiedMobile($mobile);
        if(empty($user))
        {
            return 'none';
        }
        $userProfile = $this->getUserProfile($user['id']); 
        return $userProfile['idcard']==$idcard ? 'mine' : 'others';
    }

    public function setFacePhotoFinish($id, $facePhotoFinish)
    {
        return $this->getUserDao()->setFacePhotoFinish($id, $facePhotoFinish);
    }

    public function setBatchFacePhotoFinish($userId, $facePhotoFinish)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->setFacePhotoFinish($userId, $facePhotoFinish);
        }
    }

    public function findAnalysisRegisterData($conditions)
    {
        return $this->getUserDao()->findAnalysisRegisterData($conditions);
    }

    public function findAnalysisRegistedUserData(array $areaCodes)
    {
        return $this->getUserDao()->findAnalysisRegistedUserData($areaCodes);
    }

    public function findAnalysisRegistedUserDataMaxCount(array $areaCodes)
    {
        return $this->getUserDao()->findAnalysisRegistedUserDataMaxCount($areaCodes);
    }

    public function findRegisterUserCount($conditions)
    {
        return $this->getUserDao()->findRegisterUserCount($conditions);
    }

    public function getAreaLearnUserIds($learnUserIds, $code)
    {
        if ($code == '') {
            return $learnUserIds;
        } else {
            $areaLearnUserIds = array();

            foreach ($learnUserIds as $userId) {
                $areaCode = $this->getUserDao()->getAreaCodeByUserId($userId);

                if ($areaCode['areaCode'] == $code) {
                    array_push($areaLearnUserIds, $userId);
                }
            }

            return $areaLearnUserIds;
        }
    }

    public function findStartRegisterUser()
    {
        return $this->getUserDao()->findStartRegisterUser();
    }

    public function getUserByVerifiedMobileAndAreaName($verifiedMobile, $areaName)
    {
        $area = $this->getAreaService()->getAreaByName($areaName);
        return $this->getUserDao()->getByVerifiedMobieAndAreaCode($verifiedMobile, $area['code']);
    }

    public function setNickname($id, $field)
    {
        return UserSerialize::unserialize($this->getUserDao()->updateUser($id, array('nickname' => $field)));
    }

    public function setToLearn($id, $field)
    {
        $updatedFields = array('toLearn' => $field);
        if ($field == DbValue::FALSE) {
            $updatedFields['createdTime'] = time();
        }
        return UserSerialize::unserialize($this->getUserDao()->updateUser($id, array('toLearn' => $field)));
    }

    public function setMobile($id, $field)
    {
        $this->getUserDao()->updateUser($id, array('verifiedMobile' => $field));
        $this->getProfileDao()->updateProfile($id, array('mobile' => $field));
    }

    public function changeMobile($id, $mobile, $createdTime = null, $rangeCount = 0)
    {
        $user = $this->getUser($id);

        if (empty($user) || empty($mobile)) {
            throw $this->createServiceException('参数不正确，更改失败。');
        }

        $fields = array(
            'verifiedMobile' => $mobile,
            'nickname'       => $mobile.str_repeat('_', $rangeCount)
        );
        if(!empty($createdTime)){
            $fields['createdTime'] = $createdTime;
        }

        $this->getUserDao()->updateUser($id, $fields);

        $this->updateUserProfile($id, array(
            'mobile' => $mobile
        ));
        $this->dispatchEvent('mobile.change', new ServiceEvent($user));

        $this->getLogService()->info('user', 'verifiedMobile-changed', "用户{$user['email']}(ID:{$user['id']})重置mobile成功");

        return true;
    }

    public function isUserApprovaled($userId)
    {
        $user = $this->getUser($userId);

        if (!empty($userId) && $this->hasAdminRoles($userId)) {
            return true;
        }

        if (empty($user['areaCode'])) {
            return false;
        }

        $area = $this->getAreaService()->getAreaByCode($user['areaCode']);

        return empty($area['isApproval']) || $user['approvalStatus'] == 'approved';
    }

    public function getUserIdsByTolearnAndAreaCode($tolearn, $areaCode)
    {
        return $this->getUserDao()->getUserIdsByTolearnAndAreaCode($tolearn, $areaCode);
    }

    public function newApplyUserApproval($userId, $faceImg, $backImg, $jobsSeniorityCardImg, $driverLicenseImg, $otherImg)
    {
        $user = $this->getUser($userId);

        if (empty($user)) {
            throw $this->createServiceException("用户#{$userId}不存在！");
        }

        $userProfile = $this->getUserProfile($user['id']);
        
        $createdTime = time();
         
        $approval['userId']               = $user['id'];
        $approval['faceImg']              = $faceImg;
        $approval['backImg']              = $backImg;
        $approval['jobsSeniorityCardImg'] = isset($jobsSeniorityCardImg) ? $jobsSeniorityCardImg : '';
        $approval['driverLicenseImg']     = isset($driverLicenseImg) ? $driverLicenseImg : '';
        $approval['otherImg']             = isset($otherImg) ? $otherImg : '';
        $approval['status']               = 'approving';
        $approval['createdTime']          = $createdTime;
        $approval['idcard']               = $userProfile['idcard'];
        $approval['truename']             = $userProfile['truename'];

        if ($user['approvalStatus'] != 'approving' && $user['approvalStatus'] != 'approved') {
            $this->getUserDao()->updateUser($userId, array(
                'approvalStatus' => 'approving',
                'approvalTime'   => $createdTime
            ));

            $this->getUserApprovalDao()->addApproval($approval);
        }

        return true;
    }

    public function newBatchApplyUserApproval($userId, $faceImg, $backImg, $jobsSeniorityCardImg, $driverLicenseImg, $otherImg)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->newApplyUserApproval($userId, $faceImg, $backImg, $jobsSeniorityCardImg, $driverLicenseImg, $otherImg);
        }
    }

    private function base64ImgDataSave($file, $uploadFolder, $positionNo)
    {
        if (!file_exists($uploadFolder."/{$positionNo}/")) {
            mkdir($uploadFolder."/{$positionNo}/");
        }
        
        $fp=fopen($file,"r")or die("Can't open file");
        $file_content=chunk_split(base64_encode(fread($fp,filesize($file))));//base64编码
        $imgFilePath = $uploadFolder."{$positionNo}/".uniqid().'.txt';
        file_put_contents($imgFilePath, $file_content);
        return $imgFilePath; 
    }

    public function updateFields($id, $fields)
    {
        return $this->getUserDao()->updateUser($id, $fields);
    }

    public function getFaceDetectStatus($id)
    {
        $user = $this->getUserDao()->getUser($id);
        $area = $this->getAreaService()->getAreaByCode($user['areaCode']);
        return array(
            'faceImgPath' => $user['faceImgPath'],
            'faceable' => $area['faceable'],
            'facePhotoFinished' => $user['facePhotoFinished']
        );
    }

    public function addUser($field)
    {
        $userId = $this->getCurrentUser()->id;

        if (!$this->hasAdminRoles($userId)) {
            return array();
        }

        return $this->getUserDao()->addUser($field);
    }

    public function addUserProfile($userProfile)
    {
        return $this->getProfileDao()->addProfile($userProfile);
    }

    public function findApprovalByUserId($userId)
    {
        return $this->getUserApprovalDao()->findApprovalByUserId($userId);
    }

    protected function getAreaService()
    {
        return $this->createService('Custom:Area.AreaService');
    }

    protected function getProfileDao()
    {
        return $this->createDao('Custom:User.UserProfileDao');
    }

    protected function getUserDao()
    {
        return $this->createDao('Custom:User.UserDao');
    }

    protected function getUserApprovalDao()
    {
        return $this->createDao('Custom:User.UserApprovalDao');
    }

    public function clearFace($id)
    {
        return $this->getUserDao()->clearFace($id);
    }

    public function clearBatchFace($userId)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->clearFace($userId);
            $this->setFacePhotoFinish($userId, DbValue::FALSE);
        }
    }

    public function updateAdminAreaNameIfNeed($users, $conditions)
    {
        if (in_array($conditions['roles'], array('ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN'))) {
            $userInfos      = $this->generateCheckedUserIdsAndUpdateAdminAreaName($users);
            $checkedUserIds = $userInfos['checkedUserIds'];
            $users          = $userInfos['users'];

            if (!empty($checkedUserIds)) {
                $users = $this->updateBranchSchoolAdminAreaName($checkedUserIds, $users);
            }
        }

        return $users;
    }

    /*
     * 返回
     * {
     *    users => array(),
     *    checkedUserIds => array()
     * }
     */
    private function generateCheckedUserIdsAndUpdateAdminAreaName($users)
    {
        $checkedUserIds = array();

        foreach ($users as $key => $user) {
            if (in_array('ROLE_ADMIN', $user['roles']) || in_array('ROLE_SUPER_ADMIN', $user['roles'])) {
                $users[$key]['areaName'] = '全部';
            } else {
                array_push($checkedUserIds, $user['id']);
            }
        }

        return array('users' => $users, 'checkedUserIds' => $checkedUserIds);
    }

    private function updateBranchSchoolAdminAreaName($checkedUserIds, $users)
    {
        $adminedAreas = $this->getAreaService()->getAreaAdminedBranchSchoolByUserIds($checkedUserIds);

        if (!empty($adminedAreas)) {
            foreach ($users as $key => $user) {
                foreach ($adminedAreas as $adminedArea) {
                    if ($user['id'] == $adminedArea['userId']) {
                        $users[$key]['areaName'] = $adminedArea['name'];
                        break;
                    }
                }
            }
        }

        return $users;
    }

    public function fillAnalysisGraduationDatas($graduationsInfo)
    {
        $graduationData              = array();
        $graduations                 = $this->updateGraduations($graduationsInfo);
        $timeRanges                  = array('一年以上', '一年', '半年', '二个月', '一个月', '二周', '一周', '三天');
        $graduationData['timeRange'] = $timeRanges;
        $series                      = array();

        foreach ($timeRanges as $range) {
            foreach ($graduations as $graduation) {
                $series[$graduation['name']][$range] = 0;
            }
        }

        foreach ($timeRanges as $range) {
            foreach ($graduations as $graduation) {
                if ($range == $graduation['timeRange']) {
                    $series[$graduation['name']][$range]++;
                }
            }
        }

        if (!empty($series)) {
            foreach ($series as $key => $serie) {
                $graduationData['series'][] = array(
                    'name' => $key,
                    'data' => array_values($serie)
                );
            }
        } else {
            $graduationData['series'] = array(
                'name' => '',
                'data' => []
            );
        }

        return json_encode($graduationData);
    }

    /*获取结业花费时间所在时间范围并去掉重复的结业情况*/
    protected function updateGraduations($graduationsInfo)
    {
        $graduations              = array();
        $existUserCertificationId = array();

        foreach ($graduationsInfo as $key => $graduationInfo) {
            $UserCertificationId = $graduationInfo['userId'].+
                $graduationInfo['certificationId'];

            if (!in_array($UserCertificationId, $existUserCertificationId)) {
                array_push($existUserCertificationId, $UserCertificationId);
                $minLearnStartTime = (int) $graduationInfo['userStartTime'] > (int) $graduationInfo['learnStartTime'] ? (int) $graduationInfo['userStartTime'] : (int) $graduationInfo['learnStartTime'];

                $maxLearnEndTime = (int) $graduationInfo['createdTime'] < (int) $graduationInfo['learnEndTime'] ? (int) $graduationInfo['createdTime'] : (int) $graduationInfo['learnEndTime'];

                foreach ($graduationsInfo as $tkey => $Info) {
                    $infoUserCertificationId = $Info['userId'].+$Info['certificationId'];

                    if (!in_array($infoUserCertificationId, $existUserCertificationId)) {
                        if ($graduationInfo['userId'] == $Info['userId'] &&
                            $graduationInfo['certificationId'] == $Info['certificationId']) {
                            $minLearnStartTime = (int) $Info['userStartTime'] > (int) $Info['learnStartTime'] ? (int) $Info['userStartTime'] : (int) $Info['learnStartTime'];
                            $maxLearnEndTime   = (int) $Info['createdTime'] < (int) $Info['learnEndTime'] ? (int) $Info['createdTime'] : (int) $Info['learnEndTime'];
                            array_push($existUserCertificationId, $infoUserCertificationId);
                        }
                    }
                }

                $graduationSpendTime         = $maxLearnEndTime - $minLearnStartTime;
                $graduationInfo['timeRange'] = $this->getGraduationTimeRange($graduationSpendTime);
                array_push($graduations, $graduationInfo);
            }
        }

        return $graduations;
    }

    protected function getGraduationTimeRange($spendTime)
    {
        if ($spendTime <= 3 * 86400) {
            $timeRange = '三天';
        } elseif ($spendTime > 3 * 86400 && $spendTime <= 7 * 86400) {
            $timeRange = '一周';
        } elseif ($spendTime > 7 * 86400 && $spendTime <= 14 * 86400) {
            $timeRange = '二周';
        } elseif ($spendTime > 14 * 86400 && $spendTime <= 30 * 86400) {
            $timeRange = '一个月';
        } elseif ($spendTime > 30 * 86400 && $spendTime <= 60 * 86400) {
            $timeRange = '二个月';
        } elseif ($spendTime > 60 * 86400 && $spendTime <= 183 * 86400) {
            $timeRange = '半年';
        } elseif ($spendTime > 183 * 86400 && $spendTime <= 365 * 86400) {
            $timeRange = '一年';
        } else {
            $timeRange = '一年以上';
        }

        return $timeRange;
    }

    public function findMoreAreaCodesByUserId($userId)
    {
        //判断是否关联多个行政区域
        $userProfile = $this->getUserProfile($userId);

        if (empty($userProfile['idcard'])) {
            return array($userId);
        }
        $userCount = $this->countUserProfileByIdcard($userProfile['idcard']);

        if ($userCount > 1) {
            $users = $this->findAllUserProfilesByIdcard($userProfile['idcard']);
            return ArrayToolkit::column($users, 'id');
        }
        return array($userId);
    }

    public function passBatchApproval($userId, $note = null)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->passApproval($userId, $note);   
        }
    }

    public function rejectBatchApproval($userId, $note = null)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->rejectApproval($userId, $note);   
        }
    }

    public function changeBatchAvatar($userId, $data)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changeAvatar($userId, $data);
        }
    }

    public function addBatchUserSecureQuestionsWithUnHashedAnswers($userId, $fieldsWithQuestionTypesAndUnHashedAnswers)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->addUserSecureQuestionsWithUnHashedAnswers($userId, $fieldsWithQuestionTypesAndUnHashedAnswers);
        }
    }

    public function changeBatchPayPasswordWithoutLoginPassword($userId, $newPayPassword)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changePayPasswordWithoutLoginPassword($userId, $newPayPassword);
        }
    }

    public function batchUpdatePassword($userId, $password)
    {
        $userIds = $this->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changePassword($userId, $password);
        }
    }

    public function findUsersByNicknames(array $nicknames)
    {
        return $this->getUserDao()->findUsersByNicknames($nicknames);
    }
}

class UserSerialize
{
    public static function serialize(array $user)
    {
        $user['roles'] = empty($user['roles']) ? '' : '|'.implode('|', $user['roles']).'|';
        return $user;
    }

    public static function unserialize(array $user = null)
    {
        if (empty($user)) {
            return null;
        }

        $user['roles'] = empty($user['roles']) ? array() : explode('|', trim($user['roles'], '|'));

        $user = self::_userRolesSort($user);

        return $user;
    }

    public static function unserializes(array $users)
    {
        return array_map(function ($user) {
            return UserSerialize::unserialize($user);
        }, $users);
    }

    private static function _userRolesSort($user)
    {
        if (!empty($user['roles'][1]) && $user['roles'][1] == 'ROLE_USER') {
            $temp             = $user['roles'][1];
            $user['roles'][1] = $user['roles'][0];
            $user['roles'][0] = $temp;
        }

        //交换学员角色跟roles数组第0个的位置;

        return $user;
    }
}
