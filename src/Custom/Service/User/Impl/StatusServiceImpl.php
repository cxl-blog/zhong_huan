<?php
namespace Custom\Service\User\Impl;

use Topxia\Service\User\Impl\StatusServiceImpl as BaseStatusService;
use Custom\Service\User\StatusService;

class StatusServiceImpl extends BaseStatusService implements StatusService
{
    public function deleteUnBecomeStudentStatus($userId, $courseId, $currentLoopTime)
    {
        return $this->getStatusDao()->deleteAll($userId, $courseId, $currentLoopTime, 'become_student');
    }

    protected function getStatusDao()
    {
        return $this->createDao('Custom:User.StatusDao');
    }
}
