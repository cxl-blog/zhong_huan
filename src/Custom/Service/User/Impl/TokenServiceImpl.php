<?php
namespace Custom\Service\User\Impl;

use Custom\Service\User\TokenService;
use Topxia\Service\User\Impl\TokenServiceImpl as BaseService;

class TokenServiceImpl extends BaseService implements TokenService
{
    public function deleteMobileLoginToken($userId){
        $this->getTokenDao()->deleteMobileLoginToken($userId);
    }

    protected function getTokenDao()
    {
        return $this->createDao('Custom:User.TokenDao');
    }
}