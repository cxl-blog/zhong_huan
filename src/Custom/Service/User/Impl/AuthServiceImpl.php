<?php
namespace Custom\Service\User\Impl;

use Custom\Service\User\AuthService;
use Topxia\Service\User\Impl\AuthServiceImpl as TopxiaAuthServiceImpl;

class AuthServiceImpl extends TopxiaAuthServiceImpl implements AuthService
{
    public function checkIdCard($idcard)
    {
        $avaliable = $this->getUserService()->isIdCardAvaliable($idcard);

        if (!$avaliable) {
            return array('error_duplicate', '您的身份证号在系统中不存在或已注册，如有问题请联系管理员。');
        }

        return array('success', '');
    }

    public function checkRegistPass($idcard, $password)
    {
        $userProfile = $this->getUserService()->findUserProfileByIdcard($idcard);

        if (!$this->getUserService()->verifyPassword($userProfile['id'], $password)) {
            return array('error_duplicate', '密码不正确!');
        }

        return array('success', '');
    }

    public function changeBatchPassword($userId, $oldPassword, $newPassword)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changePassword($userId, $oldPassword, $newPassword);
        }
    }

    public function registBatch($userId, $mobile)
    {
        $user = $this->getUserService()->getUser($userId);
        $curUserId = $userId;
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        $curCount = 0;
        foreach ($userIds as $userId) {
            $this->registSingle(
                $userId, 
                $mobile, 
                $curCount, 
                $user['password'], 
                $user['salt']
            );
            $curCount++;
        }
        return $this->getUserService()->getUser($curUserId);
    }

    protected function registSingle($userId, $mobile, $curCount, $password, $salt)
    {
        $this->getChangeMobileService()->updateChangeMobileInfo($userId, $mobile);

        $this->getUserService()->setMobile($userId, $mobile);

        $this->getUserService()->updateFields(
            $userId, 
            array(
                'password' => $password,
                'salt' => $salt,
                'nickname' => $mobile.str_repeat('_', $curCount),
                'toLearn' => 0,
                'createdTime' => time()
            )
        );
    }

    public function registBatchAddBranchSchoolInfo($userId)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $user = $this->getUserService()->getUser($userId);
            $this->registAddBranchSchoolInfo($userId, $user['areaCode']);
        }
    }

    protected function registAddBranchSchoolInfo($userId, $areaCode)
    {
        $this->getBranchSchoolService()->createBranchSchoolLearner($areaCode, $userId);
        $this->getBranchSchoolService()->saveFirstBranchSchoolUrlToSesson($userId, '');
    }

    public function changeBatchPayPassword($userId, $userLoginPassword, $newPayPassword)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changePayPassword($userId, $userLoginPassword, $newPayPassword);
        }
    }

    public function changeBatchPayPasswordWithoutLoginPassword($userId, $newPayPassword)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->changePayPasswordWithoutLoginPassword($userId, $newPayPassword);
        }
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function getChangeMobileService()
    {
        return $this->createService('Custom:User.ChangeMobileService');
    }

    protected function getBranchSchoolService()
    {
        return $this->createService("BranchSchool:BranchSchool.BranchSchoolService");
    }
}
