<?php
namespace Custom\Service\User\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\User\LatestStatusService;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\DateUtils;
use Custom\Common\Util\CourseLoopUtils;

class LatestStatusServiceImpl extends BaseService implements LatestStatusService
{
    public function addOrUpdateLatestStatus($status)
    {
        $copiedAttrs = array(
            'userId', 
            'courseId', 
            'type', 
            'currentLoopTime'
        );

        $copiedLatestStatus = ArrayToolkit::createAndCopyToArray($status, $copiedAttrs);

        $latestStatusList = $this->getLatestStatusDao()->search(
            array(
                'userId' => $copiedLatestStatus['userId'],
                'courseId' => $copiedLatestStatus['courseId'],
                'currentLoopTime' => $copiedLatestStatus['currentLoopTime']
            ),
            array('id', 'asc'),
            0,
            PHP_INT_MAX
        );

        if (empty($latestStatusList)) {
            $this->getLatestStatusDao()->add($copiedLatestStatus);
        } else {
            $latestStatusId = $latestStatusList[0]['id'];
            $this->getLatestStatusDao()->update($latestStatusId, $copiedLatestStatus);
        }
    }

    public function fillLatestUpdatedTimeForCourses($userId, $courses)
    {
        $latestStatusList = $this->getLatestStatusDao()->search(
            array('userId' => $userId),
            array('id', 'asc'),
            0,
            PHP_INT_MAX
        );

        $indexedCourses = ArrayToolkit::indexWithMulProperties(
            $latestStatusList, 
            array('courseId', 'currentLoopTime')
        );

        foreach ($courses as $key => $course) {
            $currentLoopTime = isset($course['currentLoopTime']) ? 
                    $course['currentLoopTime'] : CourseLoopUtils::$START_LOOP_TIME;
            $index = $course['id'] . ArrayToolkit::$MUL_PROPERTY_SEPERATOR . $currentLoopTime;
            if (empty($indexedCourses[$index])) {
                $latestStatusTime = time();
            } else {
                $latestStatusTime = $indexedCourses[$index]['updatedTime'];
            }
            $courses[$key]['latestStatusTimeNum'] = $latestStatusTime;
            $courses[$key]['latestStatusTime'] = DateUtils::number2StrWithSeconds($latestStatusTime);
        }

        return ArrayToolkit::sortTwoDimensionArray($courses, 'latestStatusTimeNum', false);
    }

    protected function getLatestStatusDao()
    {
        return $this->createDao('Custom:User.LatestStatusDao');
    }
}
