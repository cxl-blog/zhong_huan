<?php
namespace Custom\Service\User\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\User\UserCertificationStatesService;

class UserCertificationStatesServiceImpl extends BaseService implements UserCertificationStatesService
{
    public function getUserCertificationStates($id)
    {
        return $this->getUserCertificationStatesDao()->getUserCertificationStates($id);
    }

    public function addUserCertificationStates($fields)
    {
        return $this->getUserCertificationStatesDao()->addUserCertificationStates($fields);
    }

    public function deleteUserCertificationStates($userId, $certificationId)
    {
        return $this->getUserCertificationStatesDao()->deleteUserCertificationStates($userId, $certificationId);
    }

    public function searchStates($conditions, $orderBys, $start, $limit)
    {
        return $this->getUserCertificationStatesDao()->searchStates($conditions, $orderBys, $start, $limit);
    }

    public function searchStatesCount($conditions, $useActionEqualsQuery = false)
    {
        return $this->getUserCertificationStatesDao()->searchStatesCount($conditions, $useActionEqualsQuery);
    }

    public function searchGraduationCount($conditions)
    {
        return $this->getUserCertificationStatesDao()->searchGraduationCount($conditions);
    }
    
    public function findAnalysisBuyData($conditions)
    {
        return $this->getUserCertificationStatesDao()->findAnalysisBuyData($conditions);
    }

    public function findAnalysisGraduationData($conditions)
    {
        return $this->getUserCertificationStatesDao()->findAnalysisGraduationData($conditions);
    }

    public function getExportPageInfos($conditions, $pageSize = 10000)
    {
        $pageInfos['count'] = $this->searchStatesCount($conditions);
        $pageInfos['pages'] = ceil($pageInfos['count'] / $pageSize);

        for ($i = 0; $i < $pageInfos['pages']; $i++) {
            $pageInfo['page']     = $i + 1;
            $pageInfo['start']    = $i * $pageSize;
            $pageInfo['limit']    = $pageSize;
            $pageInfos['limit'][] = $pageInfo;
        }

        return $pageInfos;
    }

    public function addCompletedCertifications($userId, array $certificationIds, $currentLoopTime = null)
    {
        if (!is_array($certificationIds)) {
            return false;
        }

        foreach ($certificationIds as $certificationId) {
            if (!$certificationId) {
                continue;
            }

            $completedCourses = $this->getCertificateService()->getUserCompletionCertificateCourses($userId, $certificationId, $currentLoopTime);

            if (!$completedCourses) {
                continue;
            }

            $this->addCompletedCourses($userId, $completedCourses);
        }
    }

    protected function addCompletedCourses($userId, $completedCourses)
    {
        $user        = $this->getUserService()->getUser($userId);
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        foreach ($completedCourses as $completedCourse) {
            $conditions['userId']          = $completedCourse['userId']          = $user['id'];
            $conditions['action']          = $completedCourse['action']          = 3;
            $conditions['certificationId'] = $completedCourse['certificationId'];
            $conditions['courseId']        = $completedCourse['courseId'];
            $conditions['learnStartTime']  = $completedCourse['learnStartTime'];
            $conditions['learnEndTime']    = $completedCourse['learnEndTime'];
            $conditions['currentLoopTime'] = $completedCourse['currentLoopTime'];

            if ($this->searchStatesCount($conditions)) {
                continue;
            }

            $completedCourse['nickname']       = $user['nickname'];
            $completedCourse['idcard']         = $userProfile['idcard'];
            $completedCourse['truename']       = $userProfile['truename'];
            $completedCourse['verifiedMobile'] = $user['verifiedMobile'];
            $this->addUserCertificationStates($completedCourse);
        }
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function getCertificateService()
    {
        return $this->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserCertificationStatesDao()
    {
        return $this->createDao('Custom:User.UserCertificationStatesDao');
    }

    public function findDetailAnalysisGraduationData($conditions)
    {
        return $this->getUserCertificationStatesDao()->findDetailAnalysisGraduationData($conditions);
    }

    public function searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions)
    {
        return $this->getUserCertificationStatesDao()->searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions);
    }
}
