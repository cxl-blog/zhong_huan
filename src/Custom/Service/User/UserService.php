<?php

namespace Custom\Service\User;

interface UserService
{
    public function isIdCardAvaliable($idcard);

    public function findUserProfileByIdcard($idcard);

    public function findUserProfileByIdCardAndAreaCode($idCard, $areaCode);

    public function findAllUserProfilesByIdcard($idcard);

    public function countUserProfileByIdcard($idcard);

    public function findUserByStuId($stuId);

    public function findAnalysisRegisterData($conditions);

    public function findAnalysisRegistedUserData(array $areaCodes);

    public function findAnalysisRegistedUserDataMaxCount(array $areaCodes);

    public function findRegisterUserCount($conditions);

    public function findStartRegisterUser();

    public function getUserByVerifiedMobileAndAreaName($verifiedMobile, $areaName);

    public function setNickname($id, $field);

    public function setToLearn($id, $field);

    public function setMobile($id, $field);

    public function clearFace($id);

    public function clearBatchFace($userId);

    public function addUser($field);

    public function addUserProfile($userProfile);

    public function checkMobileValiableWithIdcard($idcard,$mobile);

    public function setFacePhotoFinish($id, $facePhotoFinish);

    public function setBatchFacePhotoFinish($userId, $facePhotoFinish);

    public function getUserByLoginFieldAndAreaCode($keyword, $areaCode);

    public function findAreaCodesByKeyword($keyword);

    public function findMoreAreaCodesByUserId($userId);

    public function changeBatchAvatar($userId, $data);

    public function addBatchUserSecureQuestionsWithUnHashedAnswers($userId, $fieldsWithQuestionTypesAndUnHashedAnswers);

    public function changeBatchPayPasswordWithoutLoginPassword($userId, $newPayPassword);

    /**
     * rangeCount: 一个身份证关联多个行政区域, 处理nickname
     **/
    public function changeMobile($id, $mobile, $createdTime = null, $rangeCount = 0);
    
    /**
     * fields格式为数组, key = 数据库字段名, value为值
     * 如
     * {
     *     'faceImgPath' => ''
     * }
     */
    public function updateFields($id, $fields);

    /**
     * 获取人脸识别配置
     * {
     *      faceImgPath: ''.
     *      faceable: 0 / 1   //1表示需要人脸识别
     * }, 如果找不到, 则返回 {}
     * @return 见描述中数组
     */
    public function getFaceDetectStatus($id);

    public function searchUserCount(array $conditions);

    public function isUserApprovaled($userId);

    public function updateAdminAreaNameIfNeed($users, $conditions);

    public function getAreaLearnUserIds($learnUserIds,$code);

    public function fillAnalysisGraduationDatas($graduationsInfo);

    public function getUserIdsByTolearnAndAreaCode($tolearn, $areaCode);

    /**
     *  实名认证
     **/
    public function findApprovalByUserId($userId);

    public function passBatchApproval($userId, $note = null);

    public function rejectBatchApproval($userId, $note = null);

    public function newBatchApplyUserApproval($userId, $faceImg, $backImg, $jobsSeniorityCardImg, $driverLicenseImg, $otherImg);

    public function batchUpdatePassword($userId, $password);

    public function findUsersByNicknames(array $nicknames);
}
