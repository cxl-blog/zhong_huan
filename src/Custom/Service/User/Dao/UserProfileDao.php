<?php

namespace Custom\Service\User\Dao;

interface UserProfileDao
{
    public function findProfileByIdCard($idcard);

    public function findAllUserProfilesByIdcard($idcard);

    public function findAllUserProfilesByMobile($mobile);

    public function findUnregistedProfileByIdCard($idcard);

    public function countUserProfileByIdcard($idcard);

}
