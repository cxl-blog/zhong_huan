<?php

namespace Custom\Service\User\Dao;

interface UserApprovalDao
{
    function findApprovalByUserId($userId);
}
