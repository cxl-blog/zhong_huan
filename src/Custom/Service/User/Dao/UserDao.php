<?php

namespace Custom\Service\User\Dao;

interface UserDao
{
    public function findRegistedUserByIdcard($idcard);

    public function findRegistedUserByIdcardAndAreaCode($idcard, $areaCode);

    public function findRegistedUserByEmail($email);

    public function findRegistedUserByVerifiedMobile($mobile);

    public function findRegistedUserByVerifiedMobileAndAreaCode($mobile, $areaCode);

    public function findUserByStuId($stuId);

    /**
     * isAll=ture　　表示查询全国
     * isAll=false　表示查询省
     */
    public function findAnalysisRegistedUserData(array $areaCodes);

    public function findAnalysisRegistedUserDataMaxCount(array $areaCodes);

    public function findAnalysisRegisterData($conditions);

    public function findRegisterUserCount($conditions);

    public function getAreaCodeByUserId($userId);

    public function findStartRegisterUser();

    /* 获取人脸识别配置
     * {
     *      faceImgPath: ''.
     *      faceable: 0 / 1   //1表示需要人脸识别
     * }
     * @return 见描述中数组
     */
    public function findFaceDetectStatus($userId);

    public function searchUserCount($conditions);

    public function searchUsers($conditions, $orderBys, $start, $limit);

    public function clearFace($id);

    public function findUsersByAreaCode($areaCode);

    public function getUserIdsByTolearnAndAreaCode($tolearn, $areaCode);

    public function setFacePhotoFinish($id, $facePhotoFinish);

    public function getUserByIdsAndAreaCode($userIds, $areaCode);

    public function getByVerifiedMobieAndAreaCode($verifiedMobile, $areaCode);

}
