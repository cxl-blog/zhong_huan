<?php

namespace Custom\Service\User\Dao;

interface LatestStatusDao
{
    public function get($id);

    public function add($fields);

    public function update($id, $fields);

    public function search($conditions, $orderBy, $start, $limit);
}
