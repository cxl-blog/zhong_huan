<?php

namespace Custom\Service\User\Dao;

interface TokenDao 
{
    public function deleteMobileLoginToken($userId);
}