<?php

namespace Custom\Service\User\Dao;

interface ChangeMobileDao 
{
    public function addChangeMobileInfo($fields);

    public function updateChangeMobileInfo($id, $fields);

    public function findChangeMobileInfoById($id);

    public function findChangeMobileInfoByUserId($userId);

    public function searchChangeMobileInfoCount();

    public function searchChangeMobileInfosByConditions($start,$limit);

    public function delChangeMobileInfosById($id);
}