<?php
namespace Custom\Service\User\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\User\Dao\ChangeMobileDao;

class ChangeMobileDaoImpl extends BaseDao implements ChangeMobileDao
{
    protected $table = 'user_change_mobile';
    
    public function addChangeMobileInfo($fields)
    {
        $fields['createdTime'] = time();
        $affected              = $this->getConnection()->insert($this->table, $fields);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert user_change_mobile error.');
        }

        return $this->findChangeMobileInfoById($this->getConnection()->lastInsertId());
    }

    public function updateChangeMobileInfo($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->findChangeMobileInfoById($id);
    }

    public function findChangeMobileInfoById($id)
    {
        $sql ="SELECT * FROM {$this->table} WHERE id = ?;";
        return $this->getConnection()->fetchAssoc($sql, array($id));
    }

    public function findChangeMobileInfoByUserId($userId)
    {
        $sql ="SELECT * FROM {$this->table} WHERE userId = ?;";
        return $this->getConnection()->fetchAssoc($sql, array($userId));
    }

    public function searchChangeMobileInfoCount()
    {
        $conditions = array();
        $builder = $this->_createQueryBuilder($conditions)
            ->select('count(`id`) AS count')
            ->from($this->table, $this->table);
        return $builder->execute()->fetchColumn(0);
    }

    public function searchChangeMobileInfosByConditions($start,$limit)
    {
        $this->filterStartLimit($start, $limit);
        $conditions = array();
        $builder = $this->_createQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        return $builder->execute()->fetchAll() ?: array();
    }

    public function delChangeMobileInfosById($id)
    {   
        $result = $this->getConnection()->delete($this->table, array('id' => $id));
        $this->clearCached();
        return $result;
    }

    protected function _createQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, $this->table);

        return $builder;
    }
}