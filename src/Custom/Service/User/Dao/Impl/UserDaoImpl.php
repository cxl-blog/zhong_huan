<?php

namespace Custom\Service\User\Dao\Impl;

use Custom\Common\Constants\DbValue;
use Custom\Service\User\Dao\UserDao;
use Topxia\Service\User\Dao\Impl\UserDaoImpl as TopxiaUserDaoImpl;

class UserDaoImpl extends TopxiaUserDaoImpl implements UserDao
{
    public function findRegistedUserByIdcard($idcard)
    {
        $that = $this;
        return $this->fetchCached("idcard:{$idcard}", $idcard, function ($idcard) use ($that) {
            $sql = "SELECT {$that->getTable()}.* FROM {$that->getTable()} "
            ."iNNER JOIN user_profile ON {$that->getTable()}.id = user_profile.id WHERE idcard = ?"
            .$that->getRegistedUserSql()
                .'LIMIT 1';
            return $that->getConnection()->fetchAssoc($sql, array($idcard));
        }

        );
    }

    public function findRegistedUserByIdcardAndAreaCode($idcard, $areaCode)
    {
        $that = $this;
        return $this->fetchCached("idcard:{$idcard}:areaCode:{$areaCode}", $idcard, $areaCode, function ($idcard, $areaCode) use ($that) {
            $sql = "SELECT {$that->getTable()}.* FROM {$that->getTable()} "
                ."iNNER JOIN user_profile ON {$that->getTable()}.id = user_profile.id WHERE idcard = ? AND areaCode = ?"
                .$that->getRegistedUserSql()
                .'LIMIT 1';
            return $that->getConnection()->fetchAssoc($sql, array($idcard, $areaCode));
        }

        );
    }

    public function findRegistedUserByEmail($email)
    {
        $that = $this;

        return $this->fetchCached("email:{$email}", $email, function ($email) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE email = ? "
            .$that->getRegistedUserSql()
                .'LIMIT 1';
            return $that->getConnection()->fetchAssoc($sql, array($email));
        }

        );
    }

    public function findUsersByAreaCode($areaCode)
    {
            $sql = "SELECT * FROM {$this->getTable()} WHERE areaCode = ?";
            return $this->getConnection()->fetchAll($sql, array($areaCode));
    }

    public function findUserByStuId($stuId)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE stuId = ?";
        return $this->getConnection()->fetchAssoc($sql, array($stuId));
    }

    public function findRegistedUserByVerifiedMobile($mobile)
    {
        $that = $this;

        return $this->fetchCached("mobile:{$mobile}", $mobile, function ($mobile) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE verifiedMobile = ? "
            .$that->getRegistedUserSql()
                .'LIMIT 1';
            return $that->getConnection()->fetchAssoc($sql, array($mobile));
        }

        );
    }

    public function findRegistedUserByVerifiedMobileAndAreaCode($mobile, $areaCode)
    {
        $that = $this;
        $mobile = $mobile .'%';

        return $this->fetchCached("mobile:{$mobile}:areaCode:{$areaCode}", $mobile, $areaCode, function ($mobile, $areaCode) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE verifiedMobile like ? AND areaCode = ? "
            .$that->getRegistedUserSql()
                .'LIMIT 1';

            return $that->getConnection()->fetchAssoc($sql, array($mobile, $areaCode));
        }

        );
    }

    public function findAnalysisRegisterData($conditions)
    {
        $params = array($conditions['startDateTime'], $conditions['endDateTime']);
        $sql    = "SELECT count(id) as count, from_unixtime(createdTime,'%Y-%m-%d') as date FROM {$this->table} WHERE toLearn = 0 AND areaCode!='' AND createdTime >= ? AND createdTime <= ? ";

        if ($conditions['areaCode'] != "") {
            $params[] = $conditions['areaCode'];
            $sql .= " AND areaCode = ? ";
        }

        $sql .= " group by from_unixtime(createdTime,'%Y-%m-%d')";

        return $this->getConnection()->fetchAll($sql, $params);
    }

    public function findRegisterUserCount($conditions)
    {
        if (isset($conditions['startDateTime'])){
            $params = array($conditions['toLearn'],$conditions['startDateTime'], $conditions['endDateTime']);
            $sql    = "SELECT count({$this->table}.id) FROM {$this->table} WHERE toLearn = ? AND areaCode!='' AND createdTime >= ? AND createdTime <= ? ";
        }
        else
        {
            $params = array($conditions['toLearn']);
            $sql    = "SELECT count({$this->table}.id) FROM {$this->table} WHERE toLearn = ? AND areaCode!=''";
        }

        if ($conditions['areaCode'] != "") {
            $params[] = $conditions['areaCode'];
            $sql .= " AND areaCode = ? ";
        }

        return $this->getConnection()->fetchColumn($sql, $params);
    }

    public function findStartRegisterUser()
    {
        $sql = "SELECT * from {$this->table} where toLearn=0 AND areaCode!='' ORDER BY createdTime ASC LIMIT 1";

        return $this->getConnection()->fetchAssoc($sql) ?: null;
    }

    public function findFaceDetectStatus($userId)
    {
        $that = $this;
        return $this->fetchCached("faceDetectStatusId:{$userId}", $userId,
            function ($userId) use ($that) {
                $sql = "SELECT faceImgPath,faceable,facePhotoFinished FROM {$that->getTable()} inner join area on area.code =  {$that->getTable()}.areaCode WHERE {$that->getTable()}.id = ? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($userId));
            }
        );
    }

    public function searchUserCount($conditions)
    {
        $builder = $this->createUserQueryBuilder($conditions)
            ->select("COUNT({$this->getTable()}.id)");
        return $builder->execute()->fetchColumn(0);
    }

    public function searchUsers($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBys);
        $builder = $this->createUserQueryBuilder($conditions)
            ->select("{$this->getTable()}.*")
            ->setFirstResult($start)
            ->setMaxResults($limit);

        for ($i = 0; $i < count($orderBys); $i = $i + 2) {
            $builder->addOrderBy($orderBys[$i], $orderBys[$i + 1]);
        };

        return $builder->execute()->fetchAll() ?: array();
    }

    public function clearFace($id)
    {
        $that = $this;
        $sql  = "UPDATE {$that->getTable()} SET faceImgPath = '' WHERE id = ?";
        $that->getConnection()->executeUpdate($sql, array($id));
        $this->clearCached();
    }

    public function setFacePhotoFinish($id, $facePhotoFinish)
    {
        $fields['facePhotoFinished'] = $facePhotoFinish;
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
    }

    public function findAnalysisRegistedUserData(array $areaCodes)
    {
        if(empty($areaCodes)){
            return array();
        }
        $marks = str_repeat('?,',count($areaCodes)-1).'?';
        $sql   = "SELECT uc.certificationId,u.areaCode,count(1) count FROM user_certification uc LEFT JOIN user u ON 
            userId = u.id WHERE u.toLearn = 0 AND u.areaCode IN 
            ({$marks}) GROUP BY u.areaCode,uc.certificationId";

        return $this->getConnection()->fetchAll($sql,$areaCodes);
    }

    public function findAnalysisRegistedUserDataMaxCount(array $areaCodes)
    {
        if(empty($areaCodes)){
            return 0;
        }
        $marks = str_repeat('?,',count($areaCodes)-1).'?';
        $sql   = "SELECT count(1) count FROM user_certification uc LEFT JOIN user u ON userId = u.id WHERE u.toLearn = 0 
            AND u.areaCode IN ({$marks}) GROUP BY u.areaCode ORDER 
            BY count DESC LIMIT 1";

        return $this->getConnection()->fetchColumn($sql,$areaCodes);
    }

    public function getUserIdsByTolearnAndAreaCode($tolearn, $areaCode)
    {
        $sql = "SELECT id FROM {$this->getTable()} WHERE tolearn = ? AND areaCode = ? ";
        return $this->getConnection()->fetchAll($sql, array($tolearn, $areaCode)) ?: null;
    }

    /**
     * 数据统计中, 中获取待学学员数据
     */
    public function analysisRegisterDataByTime($startTime, $endTime)
    {
        $that = $this;

        return $this->fetchCached("startTime:{$startTime}:endTime:{$endTime}:count", $startTime, $endTime, function ($startTime, $endTime) use ($that) {
            $sql = "SELECT count(id) as count, from_unixtime(createdTime,'%Y-%m-%d') as date FROM `{$that->getTable()}` WHERE`createdTime`>=? AND `createdTime`<=? AND `toLearn` = ? group by from_unixtime(`createdTime`,'%Y-%m-%d') order by date ASC ";
            return $that->getConnection()->fetchAll($sql, array($startTime, $endTime, DbValue::FALSE));
        }

        );
    }

    public function analysisUserSumByTime($endTime)
    {
        $that = $this;

        return $this->fetchCached("endTime:{$endTime}:date:count", $endTime, function ($endTime) use ($that) {
            $sql = "select date, count(*) as count from (SELECT from_unixtime(o.createdTime,'%Y-%m-%d') as date from user o where o.createdTime<=? and o.toLearn = ?) dates group by dates.date order by date desc";
            return $that->getConnection()->fetchAll($sql, array($endTime, DbValue::FALSE));
        }

        );
    }

    public function getAreaCodeByUserId($userId)
    {
        $sql = "SELECT areaCode FROM user where id = ?";
        return $this->getConnection()->fetchAssoc($sql, array($userId));
    }

    public function getUserByIdsAndAreaCode($userIds, $areaCode)
    {
        $marks = str_repeat('?,', count($userIds) - 1) . '?';
        $sql = "SELECT * FROM user where id in (${marks}) and areaCode = ?";
        $params = $userIds;
        array_push($params, $areaCode);
        return $this->getConnection()->fetchAssoc($sql, $params);
    }

    public function getByVerifiedMobieAndAreaCode($verifiedMobile, $areaCode)
    {
        $sql = 'select * from user where verifiedMobile = ? and areaCode = ?';
        return $this->getConnection()->fetchAssoc($sql, array($verifiedMobile, $areaCode));
    }

    protected function createUserQueryBuilder($conditions)
    {
        $builder = Parent::createUserQueryBuilder($conditions);
        
        $builder->andWhere('toLearn = :toLearn')
                ->andWhere('user.createdTime >= :startTime')
                ->andWhere('user.createdTime <= :endTime');

        if (isset($conditions['keywordType']) && isset($conditions['keyword'])) {
            if ($conditions['keywordType']=='idcard') {
                $conditions['idcard'] = "%{$conditions['keyword']}%";
                $builder->innerJoin('user','user_profile','user_profile','user_profile.id=user.id');
                $builder->andWhere('user_profile.idcard LIKE :idcard');
            }else if($conditions['keywordType']=='code') {
                $conditions['code'] = "%{$conditions['keyword']}%";
                $builder->innerJoin('user','area','area','area.code=user.areaCode');
                $builder->andWhere('area.name LIKE :code');
            }
            unset($conditions['keywordType']);
            unset($conditions['keyword']);
        }

        return $builder;
    }

    private function getRegistedUserSql($isRegisted = true)
    {
        return ' and '.$this->getTable().'.toLearn = '.DbValue::FALSE.' ';
    }

}
