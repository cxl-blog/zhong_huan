<?php

namespace Custom\Service\User\Dao\Impl;
use Topxia\Service\Common\BaseDao;
use Custom\Service\User\Dao\UserApprovalDao;
use Topxia\Service\User\Dao\Impl\UserApprovalDaoImpl as TopxiaUserApprovalDaoImpl;

class UserApprovalDaoImpl extends TopxiaUserApprovalDaoImpl implements UserApprovalDao
{
    protected $table = 'user_approval';

    public function findApprovalByUserId($userId)
    {   
        $sql ="SELECT * FROM {$this->table} WHERE userId = ?;";
        return $this->getConnection()->fetchAssoc($sql, array($userId));
    }
}
