<?php

namespace Custom\Service\User\Dao\Impl;

class ToLearnUserDaoImpl extends UserDaoImpl
{
    public function searchUserCount($conditions)
    {
        $builder = $this->createUserQueryBuilder($conditions)
            ->select("COUNT({$this->getTable()}.id)");
        return $builder->execute()->fetchColumn(0);
    }

    public function searchUsers($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBys);
        $builder = $this->createUserQueryBuilder($conditions)
            ->select("{$this->getTable()}.*")
            ->setFirstResult($start)
            ->setMaxResults($limit);

        for ($i = 0; $i < count($orderBys); $i = $i + 2) {
            $builder->addOrderBy($orderBys[$i], $orderBys[$i + 1]);
        };

        return $builder->execute()->fetchAll() ?: array();
    }
    
    protected function createUserQueryBuilder($conditions)
    {
        $keyword = $conditions['keyword'];
        $keywordType = $conditions['keywordType'];

        $keywordValue = '%'. $keyword . '%';
        if (isset($keyword)) {
            if (!empty($keywordType)) {
                if($keywordType == 'code'){
                    $conditions['area'] = $keywordValue;
                }
                else{
                    $conditions[$keywordType] = $keywordValue;
                }
            } else {
                $conditions['area'] = $keywordValue;
                $conditions['idcard'] = $keywordValue;
            }
            unset($conditions['keywordType']);
            unset($conditions['keyword']);
        }

        $builder = Parent::createUserQueryBuilder($conditions);

        $builder->innerJoin($this->getTable(), 'area', 'area', "area.code = {$this->getTable()}.areaCode")
                      ->innerJoin($this->getTable(), 'user_profile', 'user_profile', "user_profile.id = {$this->getTable()}.id");

        if (empty($keywordType)) {
            $builder -> andWhere('idcard LIKE :idcard or area.name LIKE :area');
        } else {
            $builder -> andWhere('idcard LIKE :idcard');
            $builder -> andWhere('area.name LIKE :area');
        }
                
        return $builder;
    }
}
