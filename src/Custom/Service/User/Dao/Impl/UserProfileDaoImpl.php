<?php

namespace Custom\Service\User\Dao\Impl;

use Custom\Common\Constants\DbValue;
use Custom\Service\User\Dao\UserProfileDao;
use Topxia\Service\User\Dao\Impl\UserProfileDaoImpl as TopxiaUserProfileDaoImpl;

class UserProfileDaoImpl extends TopxiaUserProfileDaoImpl implements UserProfileDao
{
    public function findProfileByIdCard($idcard)
    {
        $that = $this;

        return $this->fetchCached("idcard:{$idcard}", $idcard, function ($idcard) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE idcard = ? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($idcard));
        }
        );
    }

    public function findUnregistedProfileByIdCard($idcard)
    {
        $that = $this;

        return $this->fetchCached("idcard:{$idcard}", $idcard, function ($idcard) use ($that) {
            $sql = "SELECT {$that->getTable()}.* FROM {$that->getTable()} "
            ."INNER JOIN user on user.id =  {$that->getTable()}.id "
            .'WHERE idcard = ? and user.toLearn = '.DbValue::TRUE.' LIMIT 1';
            return $that->getConnection()->fetchAssoc($sql, array($idcard));
        }
        );
    }

    public function findAllUserProfilesByIdcard($idcard)
    {
        $sql = "SELECT * FROM {$this->table} WHERE  idcard = ?";
        return $this->getConnection()->fetchAll($sql, array($idcard));
    }

    public function findAllUserProfilesByMobile($mobile)
    {
        $sql = "SELECT * FROM {$this->table} WHERE  mobile = ?";
        return $this->getConnection()->fetchAll($sql, array($mobile));
    }

    public function countUserProfileByIdcard($idcard)
    {
        $sql    = "SELECT count(id) FROM {$this->table} WHERE idcard = ?";
        return $this->getConnection()->fetchColumn($sql, array($idcard));
    }
}
