<?php
namespace Custom\Service\User\Dao\Impl;

use Custom\Service\User\Dao\TokenDao;
use Topxia\Service\User\Dao\Impl\TokenDaoImpl as BaseDao;

class TokenDaoImpl extends BaseDao implements TokenDao
{
    protected $table = 'user_token';
    public function deleteMobileLoginToken($userId)
    {
        $sql = "DELETE FROM {$this->table} WHERE type = 'mobile_login' and userId = ? ";
        $result = $this->getConnection()->executeUpdate($sql, array($userId));
        $this->clearCached();
        return $result;
    }
}