<?php

namespace Custom\Service\User\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\User\Dao\LatestStatusDao;

class LatestStatusDaoImpl extends BaseDao implements LatestStatusDao
{
    protected $table = 'latest_status';

    public function get($id)
    {
        $sql    = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function add($fields)
    {
        $fields['createdTime'] = time();
        $fields['updatedTime'] = time();
        $affected = $this->getConnection()->insert($this->table, $fields);
        if ($affected <= 0) {
            throw $this->createDaoException('Insert status error.');
        }
        return $this->get($this->getConnection()->lastInsertId());
    }

    public function update($id, $fields)
    {
        $fields['updatedTime'] = time();
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        return $this->get($id);
    }

    public function search($conditions, $orderBy, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBy, array('id'));
        $builder = $this->_createSearchQueryBuilder($conditions)
                        ->select('*')
                        ->setFirstResult($start)
                        ->setMaxResults($limit)
                        ->orderBy($orderBy[0], $orderBy[1]);
        return $builder->execute()->fetchAll() ?: array();
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        return $this->createDynamicQueryBuilder($conditions)
                    ->from($this->table, $this->table)
                    ->andWhere('userId = :userId')
                    ->andWhere('courseId = :courseId')
                    ->andWhere('currentLoopTime = :currentLoopTime');
    }
}
