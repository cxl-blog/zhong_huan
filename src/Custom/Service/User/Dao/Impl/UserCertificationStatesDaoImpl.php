<?php
namespace Custom\Service\User\Dao\Impl;

use Topxia\Service\Common\BaseDao as BaseDao;
use Custom\Service\User\Dao\UserCertificationStatesDao;

class UserCertificationStatesDaoImpl extends BaseDao implements UserCertificationStatesDao
{
    protected $table = 'user_certification_states';

    public function getUserCertificationStates($id)
    {
        $sql = "SELECT * FROM {$this->getTable()} where id=? LIMIT 1";

        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function addUserCertificationStates($fields)
    {
        $fields['createdTime'] = time();
        $affected              = $this->getConnection()->insert($this->table, $fields);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert user_certification_states error.');
        }

        return $this->getUserCertificationStates($this->getConnection()->lastInsertId());
    }

    public function deleteUserCertificationStates($userId, $certificationId)
    {
        $result = $this->getConnection()->delete($this->table, array('userId' => $userId, 'certificationId' => $certificationId));
        return $result;
    }

    public function searchStatesCount($conditions, $useActionEqualsQuery = false)
    {
        $builder = $this->_createQueryBuilder($conditions, $useActionEqualsQuery)
            ->select("COUNT({$this->getTable()}.id)");
        return $builder->execute()->fetchColumn(0);
    }

    public function searchGraduationCount($conditions)
    {
        $builder = $this->_createQueryBuilder($conditions)
            ->select("COUNT(distinct(concat(concat({$this->getTable()}.userId, ','), {$this->getTable()}.certificationId)))");
        return $builder->execute()->fetchColumn(0);
    }

    public function searchStates($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBys);
        $builder = $this->_createQueryBuilder($conditions)
            ->select("*")
            ->setFirstResult($start)
            ->setMaxResults($limit);

        for ($i = 0; $i < count($orderBys); $i = $i + 2) {
            $builder->addOrderBy($orderBys[$i], $orderBys[$i + 1]);
        };

        return $builder->execute()->fetchAll() ?: array();
    }

    public function findAnalysisBuyData($conditions)
    {
        $params = array($conditions['startDateTime'], $conditions['endDateTime']);
        $sql    = "SELECT count(u.courseId) as count,u.certificationId as certificationId,from_unixtime(u.createdTime,'%Y-%m-%d') as date,c.category as name FROM {$this->getTable()} u INNER JOIN certification c ON u.certificationId = c.id WHERE action=2 AND u.createdTime >= ? AND u.createdTime <= ?";

        if ($conditions['areaCode'] != "") {
            $params[] = $conditions['areaCode'];
            $sql .= " AND `areaCode` = ? ";
        }

        $sql .= " group by date,certificationId";

        return $this->getConnection()->fetchAll($sql, $params);
    }

    public function findAnalysisGraduationData($conditions)
    {
        $params = array($conditions['startDateTime'], $conditions['endDateTime']);
        $sql    = "SELECT u.certificationId as certificationId,from_unixtime(u.createdTime,'%Y-%m-%d') as date,c.category as name FROM {$this->getTable()} u INNER JOIN certification c ON u.certificationId = c.id WHERE action=3 AND u.createdTime >= ? AND u.createdTime <= ?";

        if ($conditions['areaCode'] != "") {
            $params[] = $conditions['areaCode'];
            $sql .= " AND `areaCode` = ? ";
        }

        $sql .= " group by date,certificationId,currentLoopTime";

        return $this->getConnection()->fetchAll($sql, $params);
    }

    public function findDetailAnalysisGraduationData($conditions)
    {
        $params = array($conditions['startDateTime'], $conditions['endDateTime']);
        $sql    = "SELECT userId, certificationId,learnStartTime,learnEndTime,createdTime,courseId,currentLoopTime,c.category as name FROM {$this->getTable()} INNER JOIN certification c ON {$this->getTable()}.certificationId = c.id WHERE action=3 AND createdTime >= ? AND createdTime <= ?";

        if ($conditions['areaCode'] != "") {
            $params[] = $conditions['areaCode'];
            $sql .= " AND `areaCode` = ? ";
        }
        
        return $this->getConnection()->fetchAll($sql, $params);
    }

    public function searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions)
    {
        $params = array($conditions['action'], $conditions['startDateTime'], $conditions['endDateTime']);
        $sql = "SELECT  userId,certificationId,currentLoopTime FROM {$this->getTable()} WHERE action=? AND "
            ."createdTime >= ? AND createdTime <= ? GROUP BY userId, certificationId, currentLoopTime;";
        return $this->getConnection()->fetchAll($sql, $params)?: array();
    }

    /**
     *                 当值为 false时, 用 action >= $action 来查询
     *                当值为 frue时, 则用 action = $action 来查询
     * @param $useActionEqualsQuery 默认为false,
     */
    protected function _createQueryBuilder($conditions, $useActionEqualsQuery = false)
    {
        $conditions = array_filter($conditions);

        if (isset($conditions['keywordType']) && isset($conditions['keyword'])) {
            $conditions[$conditions['keywordType']] = "%{$conditions['keyword']}%";
            unset($conditions['keywordType']);
            unset($conditions['keyword']);
        }

        if (!empty($conditions['startDate'])) {
            $conditions['startDateTime'] = strtotime($conditions['startDate']);
        }

        if (!empty($conditions['endDate'])) {
            $conditions['endDateTime'] = strtotime($conditions['endDate']);
        }

        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, 'user_certification_states')
            ->andWhere('areaCode = :areaCode')
            ->andWhere('userId = :userId')
            ->andWhere('userId in ( :userIds )')
            ->andWhere('action in ( :actions )')
            ->andWhere('courseId = :courseId')
            ->andWhere('courseIds IN (:courseIds)')
            ->andWhere('certificationId = :certificationId')
            ->andWhere('certificationId IN (:certificationIds)')
            ->andWhere('idcard LIKE :idcard')
            ->andWhere('verifiedMobile LIKE :verifiedMobile')
            ->andWhere('createdTime > :startDateTime')
            ->andWhere('createdTime < :endDateTime')
            ->andWhere('learnStartTime =:learnStartTime')
            ->andWhere('learnEndTime =:learnEndTime')
            ->andWhere('currentLoopTime =:currentLoopTime')
            ->andWhere('learnEndTime =:learnEndTime');

        if ($useActionEqualsQuery) {
            $builder->andWhere('action = :action');
        } else {
            $builder->andWhere('action >= :action');
        }

        return $builder;
    }
}
