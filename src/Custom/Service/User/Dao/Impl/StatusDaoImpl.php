<?php
namespace Custom\Service\User\Dao\Impl;

use Custom\Service\User\Dao\StatusDao;
use Topxia\Service\User\Dao\Impl\StatusDaoImpl as BaseStatusDao;

class StatusDaoImpl extends BaseStatusDao implements StatusDao
{
    public function deleteAll($userId, $courseId, $currentLoopTime, $excludeType)
    {
        $sql = "delete from {$this->table} where userId = ? and courseId = ? and currentLoopTime = ? and type != ?";
        return $this->getConnection()->executeUpdate(
            $sql, 
            array($userId, $courseId, $currentLoopTime, $excludeType)
        );
    }
}
