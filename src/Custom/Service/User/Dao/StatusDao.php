<?php

namespace Custom\Service\User\Dao;

interface StatusDao
{
    public function deleteAll($userId, $courseId, $currentLoopTime, $excludeType);
}
