<?php

namespace Custom\Service\User\Dao;

interface UserCertificationStatesDao
{
    public function getUserCertificationStates($id);

    public function addUserCertificationStates($fields);

    public function deleteUserCertificationStates($userId, $certificationId);

    public function searchStatesCount($conditions, $useActionEqualsQuery = false);
    
    public function searchStates($conditions, $orderBys, $start, $limit);

    public function findAnalysisBuyData($conditions);

    public function findAnalysisGraduationData($conditions);

    public function findDetailAnalysisGraduationData($conditions);

    public function searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions);
}
