<?php
namespace Custom\Service\User;

interface LatestStatusService
{
    public function addOrUpdateLatestStatus($status);

    public function fillLatestUpdatedTimeForCourses($userId, $courses);
}
