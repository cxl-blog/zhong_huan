<?php

namespace Custom\Service\User\Event;

use Topxia\Service\Common\ServiceEvent;
use Topxia\Service\Common\ServiceKernel;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCertificationEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            'branch.school.users.create' => 'onUserRegister',
            'course.join'                => 'onCourseJoin',
            'course.lesson_finish'       => 'onLessonFinish',
            'testpaper.finish'           => 'onTestpaperFinish',
            'testpaper.reviewed'         => 'onTestpaperFinish',
            'realname.approval'          => 'onUserApproval',
            'course.completed'           => 'onCourseCompleted'
        );
    }

    public function onCourseCompleted(ServiceEvent $event)
    {
        $courseMember = $event->getSubject();
        $course       = $this->getCourseService()->getCourse($courseMember['courseId']);

        if (empty($course['certificationId'])) {
            return false;
        }

        $this->getUserCertificationStatesService()->addCompletedCertifications($courseMember['userId'], array($course['certificationId']), $courseMember['currentLoopTime']);
    }

    public function onUserApproval(ServiceEvent $event)
    {
        $user                    = $event->getSubject();
        $conditions['userId']    = $user['id'];
        $conditions['isLearned'] = 1;
        $count                   = $this->getCourseService()->searchMemberCount($conditions);
        $courseMembers           = $this->getCourseService()->searchMembers($conditions, array('id', 'desc'), 0, $count);

        if (empty($courseMembers)) {
            return false;
        }

        $courseIds = array();

        foreach ($courseMembers as $courseMember) {
            $this->getCourseService()->completeCourse($user['id'], $courseMember['courseId'], $courseMember['currentLoopTime']);
        }
    }

    public function onLessonFinish(ServiceEvent $event)
    {   
        $course      = $event->getArgument('course');
        $lessonLearn = $event->getArgument('learn');
        $userId      = $lessonLearn['userId'];
        $courseLesson = $this->getCourseService()->getCourseLesson($lessonLearn['courseId'], $lessonLearn['lessonId']);
        
        if ($courseLesson['type'] != 'testpaper') {
            $this->getCourseService()->markAsAtLeastOneLessonFinished($userId, $course['id'], $event->getArgument('currentLoopTime'));
        }
        
        $this->getCourseService()->completeCourse($userId, $course['id'], $event->getArgument('currentLoopTime'));
    }

    public function onTestpaperFinish(ServiceEvent $event)
    {
        $testpaper                = $event->getSubject();
        $dipatchedTestpaperResult = $event->getArgument('testpaperResult');
        $testpaperResult          = $this->getTestpaperService()->getTestpaperResult($dipatchedTestpaperResult['id']);

        if ($testpaperResult['passedStatus'] == 'none' || $testpaperResult['passedStatus'] == 'unpassed') {
            return false;
        }

        $userId   = $testpaperResult['userId'];
        $target   = explode("-", $testpaper['target']);
        $courseId = array_pop($target);
        $course   = $this->getCourseService()->getCourse($courseId);

        $this->getCourseService()->markAsAtLeastOneLessonFinished($userId, $course['id'], $testpaperResult['currentLoopTime']);
        $this->getCourseService()->passCourse($userId, $course['id'], $testpaperResult['currentLoopTime'], true);
        $this->getCourseService()->completeCourse($userId, $course['id'], $testpaperResult['currentLoopTime']);

    }

    public function onUserRegister(ServiceEvent $event)
    {
        return  false;

        $branchSchoolUser = $event->getSubject();
        $user             = $this->getUserService()->getUser($branchSchoolUser['userId']);
        $courses          = $this->getCertificateService()->getCertificationsForUser($user['id']);

        if (empty($courses)) {
            return false;
        }

        foreach ($courses as $course) {
            if ($course['versionStatus'] == 'latest') {
                $this->addCourseToStates($user['id'], 1, $course['id'], $course['currentLoopTime']);
            }
        }
    }

    public function onCourseJoin(ServiceEvent $event)
    {
        $course = $event->getSubject();

        if (empty($course['certificationId'])) {
            return false;
        }

        $userId          = $event->getArgument('userId');
        $currentLoopTime = $event->getArgument('currentLoopTime');
        $this->addCourseToStates($userId, 2, $course['id'], $currentLoopTime);
    }

    private function addCourseToStates($userId, $action, $courseId, $currentLoopTime = null)
    {
        $user        = $this->getUserService()->getUser($userId);
        $userProfile = $this->getUserService()->getUserProfile($userId);

        if ($currentLoopTime == null) {
            //注册时, 所有循环次数的课程都变为注册过
            $certificateCourseInfos = $this->getCertificateService()->getCertificationsForUser($userId, false, $courseId);

            foreach ($certificateCourseInfos as $certificateCourseInfo) {
                $this->addSingleUserCertificationState($user, $userProfile, $action, $courseId, $certificateCourseInfo);
            }
        } else {
            $certificateCourseInfo = $this->getCertificateService()->getCertificationsForUser($userId, false, $courseId, $currentLoopTime);
            $this->addSingleUserCertificationState($user, $userProfile, $action, $courseId, $certificateCourseInfo, $currentLoopTime);
        }
    }

    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('Custom:User.UserService');
    }

    protected function getCertificateService()
    {
        return ServiceKernel::instance()->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserCertificationStatesService()
    {
        return ServiceKernel::instance()->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getCourseService()
    {
        return ServiceKernel::instance()->createService('Custom:Course.CourseService');
    }

    protected function getLogService()
    {
        return ServiceKernel::instance()->createService('System.LogService');
    }

    protected function getTestpaperService()
    {
        return ServiceKernel::instance()->createService('Testpaper.TestpaperService');
    }

    private function addSingleUserCertificationState($user, $userProfile, $action, $courseId, $certificateCourseInfo, $currentLoopTime = null)
    {
        $course = $this->getCourseService()->getCourse($courseId);

        $fields                    = array();
        $fields['userId']          = $user['id'];
        $fields['nickname']        = $user['nickname'];
        $fields['action']          = $action;
        $fields['areaCode']        = $course['areaCode'];
        $fields['certificationId'] = $course['certificationId'];
        $fields['courseId']        = $course['id'];
        $fields['courseTitle']     = $course['title'];
        $fields['idcard']          = $userProfile['idcard'];
        $fields['truename']          = $userProfile['truename'];
        $fields['verifiedMobile']  = $user['verifiedMobile'];
        $fields['learnStartTime']  = strtotime($certificateCourseInfo['startTime']);
        $fields['learnEndTime']    = strtotime($certificateCourseInfo['deadline']);

        if ($currentLoopTime) {
            $fields['currentLoopTime'] = $currentLoopTime;
        }

        $this->getUserCertificationStatesService()->addUserCertificationStates($fields);
    }
}
