<?php
namespace Custom\Service\User;

interface UserCertificationStatesService
{
    public function getUserCertificationStates($id);

    public function searchStatesCount($conditions, $useActionEqualsQuery = false);

    public function searchGraduationCount($conditions);

    public function searchStates($conditions, $orderBys, $start, $limit);

    public function addUserCertificationStates($fields);

    public function deleteUserCertificationStates($userId, $certificationId);

    public function findAnalysisBuyData($conditions);

    public function findAnalysisGraduationData($conditions);

    public function addCompletedCertifications($userId, array $certificationIds, $currentLoopTime = null);

    public function getExportPageInfos($conditions, $pageSize = 10000);

    public function findDetailAnalysisGraduationData($conditions);

    public function searchGraduationsGroupByUserIdAndCerteficationIdAndCurrentLoopTime($conditions);
}
