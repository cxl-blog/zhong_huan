<?php
namespace Custom\Service\User;

interface ChangeMobileService
{
    public function addChangeMobileInfo($fields);

    public function updateChangeMobileInfo($userId, $mobile);

    public function updateBranchChangeMobileInfo($userId, $mobile);

    public function findChangeMobileInfoById($id);

    public function findChangeMobileInfoByUserId($userId);

    public function searchChangeMobileInfoCount();

    public function searchChangeMobileInfosByConditions($start,$limit);

    public function delChangeMobileInfosById($id);
}
