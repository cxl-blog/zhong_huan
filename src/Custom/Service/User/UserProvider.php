<?php
namespace Custom\Service\User;

use Topxia\Service\Common\ServiceKernel;
use Topxia\Service\User\UserProvider as BaseUserProvider;

class UserProvider extends BaseUserProvider
{
    protected function getUserService()
    {
        return ServiceKernel::instance()->createService('Custom:User.UserService');
    }
}
