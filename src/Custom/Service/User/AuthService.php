<?php
namespace Custom\Service\User;

interface AuthService
{
    public function checkIdCard($idCard);

    public function checkRegistPass($idCard, $password);

    public function changeBatchPassword($userId, $oldPassword, $newPassword);

    public function registBatch($userId, $mobile);

    public function registBatchAddBranchSchoolInfo($userId);

    public function changeBatchPayPassword($userId, $userLoginPassword, $newPayPassword);

    public function changeBatchPayPasswordWithoutLoginPassword($userId, $newPayPassword);
}
