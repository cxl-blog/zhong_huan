<?php

namespace Custom\Serivce\User\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class LatestStatusTest extends BaseTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testAdd()
    {
        $status = array(
            'id' => 100,
            'userId' => 300,
            'courseId' => 200,
            'type' => 'start_learn_lesson',
            'currentLoopTime' => 2
        );
        $this->getLatestStatusDao()->add($status);
        $dbStatus = $this->getLatestStatusDao()->get(100);
        $this->assertEquals(300, $dbStatus['userId']);
        $this->assertEquals('start_learn_lesson', $dbStatus['type']);
    }

    public function testUpdate()
    {
        $this->testAdd();
        $updatedStatus = array(
            'type' => 'finish_lesson',
        );
        $this->getLatestStatusDao()->update(100, $updatedStatus);
        $dbStatus = $this->getLatestStatusDao()->get(100);
        $this->assertEquals('finish_lesson', $dbStatus['type']);
    }

    public function testSearch()
    {
        $this->testAdd();
        $conditions = array('userId' => 300);
        $result = $this->getLatestStatusDao()->search(
            $conditions, 
            array('id', 'asc'), 
            0, 
            PHP_INT_MAX
        );
        $this->assertEquals(1, count($result));
        $this->assertEquals('start_learn_lesson', $result[0]['type']);
    }

    protected function getLatestStatusDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.LatestStatusDao');
    }
}
