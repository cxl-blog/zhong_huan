<?php

namespace Custom\Serivce\User\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class UserServiceTest extends BaseTestCase
{
    public function testFindUserProfileByIdCardAndAreaCodeWithoutAreaCode()
    {
        $this->mock('Custom:User.UserProfileDao');
        $this->getUserProfileDao()->shouldReceive('findProfileByIdCard')->andReturn(
            array('id' => 1)
        );
        $info = $this->getUserService()->findUserProfileByIdCardAndAreaCode(array(1, 2), null);
        $this->getUserProfileDao()->shouldHaveReceived('findProfileByIdCard');
        $this->assertArrayEquals(array('id' => 1), $info);
    }

    public function testFindUserProfileByIdCardAndAreaCodeWithAreaCode()
    {
        $this->mock('Custom:User.UserProfileDao');
        $this->mock('Custom:User.UserDao');
        $this->getUserProfileDao()->shouldReceive('findAllUserProfilesByIdcard')->andReturn(
            array('id' => 1), array('id' => 2)
        );
        $this->getUserDao()->shouldReceive('getUserByIdsAndAreaCode')->andReturn(
            array('nickname' => 'hello')
        );
        $info = $this->getUserService()->findUserProfileByIdCardAndAreaCode(array(1, 2), '001');
        $this->getUserProfileDao()->shouldHaveReceived('findAllUserProfilesByIdcard');
        $this->getUserDao()->shouldHaveReceived('getUserByIdsAndAreaCode');
        $this->assertArrayEquals(array('nickname' => 'hello'), $info);
    }

    public function testBatchUpdatePassword()
    {
        $this->mock('Custom:User.UserProfileDao');
        $this->mock('Custom:User.UserDao');
        $this->mock('System.LogService');

        $this->getUserProfileDao()->shouldReceive('getProfile')->andReturn(
            array('idcard' => '620000199910130012')
        );

        $this->getUserProfileDao()->shouldReceive('countUserProfileByIdcard')->andReturn(2);

        $this->getUserProfileDao()->shouldReceive('findAllUserProfilesByIdcard')->andReturn(
            array(
                array('id' => 100),
                array('id' => 101)
            )
        );

        $this->getUserDao()->shouldReceive('getUser')->andReturn(array('id' => 100, 'email' => 'hello@yahoo.cn'));
        $this->getUserDao()->shouldReceive('updateUser');
        $this->getLogService()->shouldReceive('info');

        $this->getUserService()->batchUpdatePassword(1, '123456');

        $this->getUserProfileDao()->shouldHaveReceived('getProfile');
        $this->getUserProfileDao()->shouldHaveReceived('countUserProfileByIdcard');
        $this->getUserProfileDao()->shouldHaveReceived('findAllUserProfilesByIdcard');
        $this->getUserDao()->shouldHaveReceived('getUser');
        $this->getUserDao()->shouldHaveReceived('updateUser');
        $this->getLogService()->shouldHaveReceived('info');

    }

    public function testFindUsersByNicknames()
    {
        $userInfo1       = array(
            'nickname' => 'test_nickname1',
            'password' => 'test_password1',
            'email'    => 'test_email1@email.com'
        );
        $this->getUserService()->register($userInfo1);

        $userInfo2       = array(
            'nickname' => 'test_nickname2',
            'password' => 'test_password2',
            'email'    => 'test_email2@email.com'
        );
        $this->getUserService()->register($userInfo2);

        $nicknames = array($userInfo1['nickname'], $userInfo2['nickname']);
        
        $result = $this->getUserService()->findUsersByNicknames($nicknames);

        $this->assertEquals(2, count($result));
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getUserProfileDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserProfileDao');
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createDao('System.LogService');
    }
}
