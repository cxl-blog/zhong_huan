<?php

namespace Custom\Serivce\User\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class LatestStatusServiceTest extends BaseTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testAddLatestStatus()
    {
        $this->mock('Custom:User.LatestStatusDao');

        $status = array(
            'id' => 100,
            'userId' => 300,
            'courseId' => 200,
            'type' => 'start_learn_lesson',
            'classroomId' => 3,
            'objectId' => 1,
            'currentLoopTime' => 2
        );
        $this->getLatestStatusDao()->shouldReceive('search')->andReturn(null);
        $this->getLatestStatusDao()->shouldReceive('add');

        $this->getLatestStatusService()->addOrUpdateLatestStatus($status);

        $this->getLatestStatusDao()->shouldHaveReceived('search');
        $this->getLatestStatusDao()->shouldHaveReceived('add');
    }

    public function testUpdateLatestStatus()
    {
        $this->mock('Custom:User.LatestStatusDao');

        $status = array(
            'id' => 100,
            'userId' => 300,
            'courseId' => 200,
            'type' => 'start_learn_lesson',
            'classroomId' => 3,
            'objectId' => 1,
            'currentLoopTime' => 2
        );

        $dbStatus = array(
            array(
                'id' => 100,
                'userId' => 400,
                'courseId' => 300,
                'type' => 'finish_lesson',
                'currentLoopTime' => 1
            )
        );
        $this->getLatestStatusDao()->shouldReceive('search')->andReturn($dbStatus);
        $this->getLatestStatusDao()->shouldReceive('update');

        $this->getLatestStatusService()->addOrUpdateLatestStatus($status);

        $this->getLatestStatusDao()->shouldHaveReceived('search');
        $this->getLatestStatusDao()->shouldHaveReceived('update');
    }

    public function testFillLatestUpdatedTimeForCourses()
    {
        $this->mock('Custom:User.LatestStatusDao');
        $this->getLatestStatusDao()->shouldReceive('search')->andReturn(
            array(
                array(
                    'courseId' => 1,
                    'currentLoopTime' => 2,
                    'updatedTime' => 1485923613
                ),
                array(
                    'courseId' => 2,
                    'currentLoopTime' => 1,
                    'updatedTime' => 1485923614
                )
            )
        );

        $filledCourses = array(
            array(
                'id' => 1,
                'currentLoopTime' => 2
            ),
            array(
                'id' => 2,
                'currentLoopTime' => 1
            ),
            array(
                'id' => 3,
                'currentLoopTime' => 1
            )
        );

        $result = $this->getLatestStatusService()->fillLatestUpdatedTimeForCourses(1, $filledCourses);

        $this->assertEquals(3, count($result));
        $this->assertEquals('2017-02-01 12:33:33', $result[2]['latestStatusTime']);
        $this->assertEquals('2017-02-01 12:33:34', $result[1]['latestStatusTime']);
    }

    protected function getLatestStatusDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.LatestStatusDao');
    }

    protected function getLatestStatusService()
    {
        return $this->getServiceKernel()->createService('Custom:User.LatestStatusService');
    }
}
