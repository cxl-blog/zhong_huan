<?php

namespace Custom\Serivce\User\Tests;

use Topxia\Service\Common\BaseTestCase;
use Mockery;

class UserDaoTest extends BaseTestCase
{
    public function testGetUserByIdsAndAreaCode()
    {
        $this->getUserDao()->addUser(
            array(
                'id' => 100,
                'nickname' => '13675818888',
                'password' => '123',
                'areaCode' => '001',
                'email' => 'abc@yahoo.com'
            )
        );

        $this->getUserDao()->addUser(
            array(
                'id' => 101,
                'nickname' => '13675818888_',
                'password' => '123',
                'areaCode' => '002',
                'email' => 'abc@yahoo1.com'
            )
        );
        
        $user = $this->getUserDao()->getUserByIdsAndAreaCode(array(100, 101), '002');
        $this->assertEquals(101, $user['id']);
    }

    protected function getUserDao()
    {
        return $this->getServiceKernel()->createDao('Custom:User.UserDao');
    }
}
