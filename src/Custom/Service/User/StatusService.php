<?php
namespace Custom\Service\User;

interface StatusService
{
    public function deleteUnBecomeStudentStatus($userId, $courseId, $currentLoopTime);
}
