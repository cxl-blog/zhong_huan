<?php

namespace Custom\Service\Register;

/**
 * 人脸识别用
 */
interface RegisterService
{
    public function sendLoginPassBySms($userId, $idcard, $mobileNum);
}
