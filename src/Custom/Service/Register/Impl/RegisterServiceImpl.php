<?php
namespace Custom\Service\Register\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\Register\RegisterService;
use Custom\Service\Sms\SmsService;

/**
 * 人脸识别说明 见 {项目}/doc/人脸识别说明.md
 */
class RegisterServiceImpl extends BaseService implements RegisterService
{
    public function sendLoginPassBySms($userId, $idcard, $mobileNum)
    {
        $contentParam = array(
            'password' => $this->generatePassword($userId)
        );

        $params = array(
            'mobile'     => $mobileNum,
            'category'   => SmsService::REGIST,
            'parameters' => $contentParam
        );

        $this->getSmsService()->sendMsgBySms($userId, $idcard, $params);
    }

    protected function getSmsService()
    {
        return $this->createService('Custom:Sms.SmsService');
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    private function generatePassword($userId)
    {
        $loginPass = substr(md5(time().mt_rand(1,100)), 0, 6);
        $this->getUserService()->batchUpdatePassword($userId, $loginPass);
        return $loginPass;
    }
}
