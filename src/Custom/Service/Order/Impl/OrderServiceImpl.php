<?php
namespace Custom\Service\Order\Impl;

use Topxia\Service\Order\Impl\OrderServiceImpl as BaseServiceImpl;
use Topxia\Common\ArrayToolkit;

class OrderServiceImpl extends BaseServiceImpl
{
    public function applyRefundOrder($id, $expectedAmount = null, $reason = array())
    {
        $refund = parent::applyRefundOrder($id, $expectedAmount, $reason);

        if ($refund['status'] == 'created' && $refund['targetType'] == 'course') {
            $user    = $this->getUserService()->getUser($refund['userId']);
            $smsType = 'sms_zhonghuan_course_buy_refund';
            $course  = $this->getCourseService()->getCourse($refund['targetId']);
            $params  = array(
                'mobile'     => $user['verifiedMobile'],
                'category'   => $smsType,
                'parameters' => array(
                    'course' => $course['title'],
                    'days'   => 7
                )
            );
            $logMessage = '发送用于通知用户退费的短信';
            $this->getSmsService()->sendCustomMsg($params, $logMessage);
        }

        return $refund;
    }

    public function createOrder($order)
    {
        if (!ArrayToolkit::requireds($order, array('userId', 'title', 'amount', 'targetType', 'targetId', 'payment'))) {
            throw $this->createServiceException('创建订单失败：缺少参数。');
        }
        
        $order = ArrayToolkit::parts($order, array(
            'userId',
            'title',
            'amount',
            'targetType',
            'targetId',
            'payment',
            'note',
            'snPrefix',
            'data',
            'couponCode',
            'coinAmount',
            'coinRate',
            'priceType',
            'totalPrice',
            'coupon',
            'couponDiscount',
            'discountId',
            'discount',
            'currentLoopTime'
        ));

        $orderUser = $this->getUserService()->getUser($order['userId']);
        if (empty($orderUser)) {
            throw $this->createServiceException("订单用户(#{$order['userId']})不存在，不能创建订单。");
        }
        if (!in_array($order['payment'], array('none', 'alipay', 'alipaydouble', 'tenpay', 'coin', 'wxpay_mobile', 'wxpay_public', 'offlinepay'))) {
            throw $this->createServiceException('创建订单失败：payment取值不正确。');
        }
        $order['sn'] = $this->generateOrderSn($order);
        unset($order['snPrefix']);

        if (!empty($order['couponCode'])) {
            $couponInfo = $this->getCouponService()->checkCouponUseable($order['couponCode'], $order['targetType'], $order['targetId'], $order['amount']);

            if ($couponInfo['useable'] != 'yes') {
                throw $this->createServiceException("优惠码不可用");
            }
        }

        unset($order['couponCode']);

        $order['amount'] = number_format($order['amount'], 2, '.', '');

        if (intval($order['amount'] * 100) == 0) {
            $order['payment'] = 'none';
        }

        $order['status']      = 'created';
        $order['createdTime'] = time();
        $order = $this->getOrderDao()->addOrder($order);

        $this->_createLog($order['id'], 'created', '创建订单');
        return $order;
    }

    public function searchRefunds($conditions, $sort, $start, $limit)
    {
        $conditions = array_filter($conditions);
        $orderBy    = array('createdTime', 'DESC');
        return $this->getOrderRefundDao()->searchRefunds($conditions, $orderBy, $start, $limit);
    }

    public function searchRefundCount($conditions)
    {
        $conditions = array_filter($conditions);
        return $this->getOrderRefundDao()->searchRefundCount($conditions);
    }

    public function findRefundById($id)
    {
        return $this->getOrderRefundDao()->findRefundById($id);
    }

    protected function getSmsService()
    {
        return $this->createService('Custom:Sms.SmsService');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getOrderRefundDao()
    {
        return $this->createDao('Custom:Order.OrderRefundDao');
    }

    protected function getOrderDao()
    {
        return $this->createDao('Custom:Order.OrderDao');
    }
}
