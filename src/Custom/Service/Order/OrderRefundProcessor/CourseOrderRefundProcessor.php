<?php
namespace Custom\Service\Order\OrderRefundProcessor;

use Topxia\Service\Order\OrderRefundProcessor\OrderRefundProcessor;
use Topxia\Service\Order\OrderRefundProcessor\CourseOrderRefundProcessor as BaseProcessor;

class CourseOrderRefundProcessor extends BaseProcessor
{
    public function getTargetMember($targetId, $userId, $currentLoopTime = null)
    {
        return $this->getCourseService()->getCourseMember($targetId, $userId, $currentLoopTime);
    }

    public function applyRefundOrder($orderId, $amount, $reason, $container)
    {
        return $this->getCourseOrderService()->applyRefundOrder($orderId, $amount, $reason, $container);
    }

    public function removeStudent($targetId, $userId, $currentLoopTime = null)
    {
        $this->getCourseService()->removeStudent($targetId, $userId, $currentLoopTime);
    }

    public function auditRefundOrder($id, $pass, $data)
    {
        $order = $this->getOrderService()->getOrder($id);

        if ($pass) {
            if ($this->getCourseService()->isCourseStudent($order['targetId'], $order['userId'], $order['currentLoopTime'])) {
                $this->getCourseService()->removeStudent($order['targetId'], $order['userId'], $order['currentLoopTime']);
            }
        } else {
            if ($this->getCourseService()->isCourseStudent($order['targetId'], $order['userId'], $order['currentLoopTime'])) {
                $this->getCourseService()->unlockStudent($order['targetId'], $order['userId'], $order['currentLoopTime']);
            }
        }
    }
}
