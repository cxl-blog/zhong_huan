<?php
namespace Custom\Service\Order\Dao\Impl;

use Topxia\Service\Order\Dao\Impl\OrderDaoImpl as BaseDao;
use Custom\Service\Order\Dao\OrderDao;

class OrderDaoImpl extends BaseDao implements OrderDao
{
    protected function _createSearchQueryBuilder($conditions)
    {
        if (isset($conditions["title"])) {
            $conditions["title"] = '%'.$conditions["title"]."%";
        }

        if (isset($conditions['payment'])) {
            if ($conditions['payment'] == 'free') {
                $conditions['freeOrderAmount'] = 0;
                unset($conditions['payment']);
            }
            $conditions['statusCreated'] = 'created';
            $conditions['statusCancelled'] = 'cancelled';
        }

        return $this->createDynamicQueryBuilder($conditions)
                    ->from($this->table, 'course_order')
                    ->andWhere('sn = :sn')
                    ->andWhere('targetType = :targetType')
                    ->andWhere('targetId = :targetId')
                    ->andWhere('userId = :userId')
                    ->andWhere('amount > :amount')
                    ->andWhere('totalPrice >= totalPrice')
                    ->andWhere('coinAmount > :coinAmount')
                    ->andWhere('status = :status')
                    ->andWhere('status <> :statusPaid')
                    ->andWhere('status <> :statusCreated')
                    ->andWhere('status <> :statusCancelled')
                    ->andWhere('payment = :payment')
                    ->andWhere('createdTime >= :createdTimeGreaterThan')
                    ->andWhere('paidTime >= :paidStartTime')
                    ->andWhere('paidTime < :paidEndTime')
                    ->andWhere('createdTime >= :startTime')
                    ->andWhere('createdTime < :endTime')
                    ->andWhere('createdTime < :createdTime_LT')
                    ->andWhere('amount = :freeOrderAmount')
                    ->andWhere('title LIKE :title');
    }
}