<?php

namespace Custom\Service\Order\Dao\Impl;

use Topxia\Service\Order\Dao\Impl\OrderRefundDaoImpl as BaseDao;
use Custom\Service\Order\Dao\OrderRefundDao;

class OrderRefundDaoImpl extends BaseDao implements OrderRefundDao
{
    public function searchRefunds($conditions, $orderBy, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $this->checkOrderBy($orderBy);
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('*')
            ->orderBy($orderBy[0], $orderBy[1])
            ->setFirstResult($start)
            ->setMaxResults($limit);
        return $builder->execute()->fetchAll() ? : array(); 
    }

    public function searchRefundCount($conditions)
    {
        $builder = $this->_createSearchQueryBuilder($conditions)
            ->select('COUNT(id)');
        return $builder->execute()->fetchColumn(0);
    }

    public function findRefundById($id)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE id = ?";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    protected function _createSearchQueryBuilder($conditions)
    {
        $builder = $this->createDynamicQueryBuilder($conditions)
                    ->from($this->table, $this->table)
                    ->andWhere('status = :status')
                    ->andWhere('userId = :userId')
                    ->andWhere('targetType = :targetType')
                    ->andWhere('orderId = :orderId')
                    ->andWhere('targetId IN ( :courseIds )')
                    ->andWhere('updatedTime > :startDateTime')
                    ->andWhere('updatedTime < :endDateTime');

        if (isset($conditions['targetIds'])) {
            $builder->andWhere("targetId IN ( :targetIds )");
        }

        return $builder;           
    }
}