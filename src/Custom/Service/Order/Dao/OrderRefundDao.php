<?php

namespace Custom\Service\Order\Dao;

interface OrderRefundDao
{
    public function searchRefunds($conditions, $orderBy, $start, $limit);
		
    public function searchRefundCount($conditions);

    public function findRefundById($id);
}