<?php

namespace Custom\Service\Order;

interface OrderService
{
    public function searchRefunds($conditions, $sort, $start, $limit);

    public function searchRefundCount($conditions);

    public function findRefundById($id);
}
