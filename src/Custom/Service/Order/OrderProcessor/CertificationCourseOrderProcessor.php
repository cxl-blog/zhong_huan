<?php
namespace Custom\Service\Order\OrderProcessor;

use Topxia\Service\Common\ServiceKernel;
use Topxia\Service\Order\OrderProcessor\CourseOrderProcessor;

class CertificationCourseOrderProcessor extends CourseOrderProcessor
{
    protected $router = "course_certification_show";
    
    public function preCheck($targetId, $userId, $currentLoopTime = null)
    {
        if (!$this->getCertificateService()->isCertificationCourseBuyable($userId, $targetId, $currentLoopTime)) {
            return array('error' => '该课程不可购买，如有需要，请联系客服');
        }

        $course = $this->getCourseService()->getCourse($targetId);

        if (!$course['buyable']) {
            return array('error' => '该课程不可购买，如有需要，请联系客服');
        }

        if ($course['status'] != 'published') {
            return array('error' => '不能加入未发布课程!');
        }

        if ($course["type"] == "live" && $course["studentNum"] >= $course["maxStudentNum"]) {
            return array('error' => '名额已满，不能加入!');
        }

        return array();
    }

    public function createOrder($orderInfo, $fields, $currentLoopTime = null)
    {
        return $this->getCourseOrderService()->createOrder($orderInfo, $currentLoopTime);
    }

    protected function getCertificateService()
    {
        return ServiceKernel::instance()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseOrderService()
    {
        return ServiceKernel::instance()->createService("Custom:Course.CertificationCourseOrderService");
    }
}
