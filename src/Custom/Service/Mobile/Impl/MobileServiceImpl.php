<?php

namespace Custom\Service\Mobile\Impl;

use Custom\Service\Mobile\MobileService;
use Topxia\Service\Common\BaseService;

class MobileServiceImpl extends BaseService implements MobileService
{
    const API_MAX_VERSION = '2016.6.0';
    const API_MIN_VERSION = '2016.0.2';

    public function writeMobileInfosToFile($fileName, $filePath, $versionInfos)
    {
        $versionInfos['minVersion'] = str_replace(' ', '', $versionInfos['minVersion']);
        
        $versionInfos['appPath'] = str_replace(' ', '', $versionInfos['appPath']);
        $versionInfos['currentVersion'] = str_replace(' ', '', $versionInfos['currentVersion']);

        $versionInfos['updateInfo'] = str_replace(' ', '', $versionInfos['updateInfo']);
        $versionInfos['updateInfo'] = str_replace("\r\n", '||', $versionInfos['updateInfo']);
        $contents = '';
        $contents = 'minVersion: '.$versionInfos['minVersion']."\r\n";
        
        $contents = $contents.'appPath: '.$versionInfos['appPath']."\r\n";
        $contents = $contents.'currentVersion: '.$versionInfos['currentVersion']."\r\n";
        
        $contents = $contents.'updateInfo: '.$versionInfos['updateInfo'];
        $this->getMobileDao()->saveVersionInfo($filePath.$fileName, $contents);

        $this->getLogService()->info('mobileVersion', 'version', $contents);
    }

    public function readMobileInfosFromFile($fileName, $filePath)
    {
        if (!file_exists($filePath.$fileName)) {
            $contents = '';
            $contents = 'minVersion: '.self::API_MIN_VERSION."\r\n";
            $contents = $contents.'maxVersion: '.self::API_MAX_VERSION."\r\n";
            $contents = $contents.'appPath: '.'http://www.edusoho.com/download/mobile?client=android&code=edusohov3'."\r\n";
            $contents = $contents.'code:'.' '."\r\n";
            $contents = $contents.'currentVersion:'.' '."\r\n";
            $contents = $contents.'updateInfo:'.' ';
            $this->getMobileDao()->saveVersionInfo($filePath.$fileName, $contents);
        }

        $fileContent = $this->getMobileDao()->getVersionInfo($filePath.$fileName);
        if (isset($fileContent['updateInfo'])) {
            $fileContent['updateInfo'] = str_replace('||', "\r\n", $fileContent['updateInfo']);
        }
        return $fileContent;
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }

    protected function getMobileDao()
    {
        return $this->createDao('Custom:Mobile.MobileDao');
    }
}