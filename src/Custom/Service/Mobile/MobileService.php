<?php

namespace Custom\Service\Mobile;

interface MobileService
{
    public function writeMobileInfosToFile($fileName, $filePath, $versionInfos);

    public function readMobileInfosFromFile($fileName, $filePath);
}