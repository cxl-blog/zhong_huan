<?php

namespace Custom\Service\Mobile\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Mobile\Dao\MobileDao;
use Symfony\Component\Yaml\Yaml;

class MobileDaoImpl extends BaseDao implements MobileDao
{
    //这个表不存在, 只是为了用现成的redis
    protected $table = 'mobileVersion'; 

    public function saveVersionInfo($path, $versionInfo)
    {
        file_put_contents($path, $versionInfo);
        $this->clearCached();
    }

    public function getVersionInfo($path)
    {
        $that = $this;
        return $this->fetchCached("mobileVersion", $path, function($path) use ($that) {
            return Yaml::parse(file_get_contents($path));
        });
    }
}