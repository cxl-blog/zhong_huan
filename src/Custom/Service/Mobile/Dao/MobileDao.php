<?php 
namespace Custom\Service\Mobile\Dao;

interface MobileDao
{
    public function saveVersionInfo($path, $versionInfo);

    public function getVersionInfo($path);
}