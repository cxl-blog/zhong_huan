<?php
namespace Custom\Service\FaceDetectResult\Job;

use Topxia\Service\Crontab\Job;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\Util\DateUtils;

/**
 * 每天凌晨 12点执行
 */
class GenerateMissLessonFacesJob implements Job
{
    public function execute($params)
    {
        $this->getVideoFaceDetectMockService()->generateMissedVideoFaceDetect();
    }

    protected function getVideoFaceDetectMockService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.VideoFaceDetectMockService');
    }

    protected function getServiceKernel()
    {
        return ServiceKernel::instance();
    }
}
