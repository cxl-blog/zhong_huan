<?php
namespace Custom\Service\FaceDetectResult\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\FaceDetectResult\Dao\FaceDetectResultDao;

class FaceDetectResultDaoImpl extends BaseDao implements FaceDetectResultDao
{
    protected $table = 'face_detect_result';

    public function getFaceDetectResult($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function addFaceDetectResult($faceDetectResult)
    {
        $faceDetectResult['createdTime'] = time();
        $affected                        = $this->getConnection()->insert($this->table, $faceDetectResult);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert area error:');
        }

        return $this->getConnection()->lastInsertId();
    }

    public function updateFaceDetectResult($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        return $this->getFaceDetectResult($id);
    }

    public function deleteFaceDetectResult($userId, $courseId, $currentLoopTime = null)
    {
        $sql    = "DELETE FROM {$this->table} WHERE userId = ? and courseId = ? and currentLoopTime = ?";

        $result = $this->getConnection()->executeUpdate($sql, array($userId, $courseId, $currentLoopTime));
        $this->clearCached();
        return $result;
    }

    public function searchFaceDetectResults($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $builder = $this->_createQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        if (!empty($orderBys)) {
            $this->checkOrderBy($orderBys);
            $builder->addOrderBy($orderBys[0], $orderBys[1]);
        }

        return $builder->execute()->fetchAll() ?: array();
    }

    public function searchFaceDetectResultsCount($conditions)
    {
        $builder = $this->_createQueryBuilder($conditions)
            ->select('count(`id`) AS count')
            ->from($this->table, $this->table);
        return $builder->execute()->fetchColumn(0);
    }

    public function findLessonCountGroupsByUserIdAndLessonViewIds($userId, $lessonViewIds, $currentLoopTime = 1)
    {
        $marks  = str_repeat('?,', count($lessonViewIds) - 1).'?';
        $params = array_merge(array($userId), $lessonViewIds, array($currentLoopTime));
        $sql    = "SELECT userId,courseId,lessonId, count(*) as count FROM {$this->table} WHERE userId = ? and lessonViewId in ({$marks}) and type='lesson' AND  currentLoopTime = ? group by lessonId";
        return $this->getConnection()->fetchAll($sql, $params) ?: null;
    }

    public function findFaceDectResultByUserIdAndLessonViewId($userId, $lessonViewId)
    {
        $sql    = "SELECT * FROM {$this->table} WHERE userId = ? and lessonViewId = ? ;";
        return $this->getConnection()->fetchAll($sql, array($userId, $lessonViewId)) ?: null;
    }

    public function findFaceDectResultByUserIdAndCourseIdAndCurrentLoopTime($userId, $courseId, 
        $currentLoopTime)
    {
        $sql    = "SELECT * FROM {$this->table} WHERE userId = ?  and courseId=? and currentLoopTime = ? ;";
        return $this->getConnection()->fetchAll($sql, array($userId, $courseId, $currentLoopTime)) ?: null;
    }

    protected function _createQueryBuilder($conditions)
    {
        if (isset($conditions['type']) && empty($conditions['type'])) {
            unset($conditions['type']);
        }

        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, $this->table)
            ->andWhere('userId = :userId')
            ->andWhere('type = :type')
            ->andWhere('type IN ( :types )')
            ->andWhere('courseId = :courseId')
            ->andWhere('lessonId = :lessonId')
            ->andWhere('lessonViewId = :lessonViewId')
            ->andWhere('currentLoopTime = :currentLoopTime')
            ->andWhere('createdTime >= :startDateTime')
            ->andWhere('createdTime <= :endDateTime')
            ->andWhere('userId IN ( :userIds )')
            ->andWhere('result = :result');

        return $builder;
    }
}
