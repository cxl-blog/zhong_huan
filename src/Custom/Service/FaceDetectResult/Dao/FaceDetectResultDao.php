<?php
namespace Custom\Service\FaceDetectResult\Dao;

interface FaceDetectResultDao
{
    public function getFaceDetectResult($id);

    public function addFaceDetectResult($faceDetectResult);

    public function updateFaceDetectResult($id, $fields);

    public function deleteFaceDetectResult($userId, $courseId, $currentLoopTime = null);

    public function searchFaceDetectResults($conditions, $orderBys, $start, $limit);

    public function searchFaceDetectResultsCount($conditions);

    public function findLessonCountGroupsByUserIdAndLessonViewIds($userId, $lessonViewIds, $currentLoopTime = 1);

    public function findFaceDectResultByUserIdAndLessonViewId($userId, $lessonViewId);

    public function findFaceDectResultByUserIdAndCourseIdAndCurrentLoopTime($userId, $courseId, 
        $currentLoopTime);
}
