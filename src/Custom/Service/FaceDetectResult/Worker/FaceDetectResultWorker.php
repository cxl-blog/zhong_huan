<?php

namespace Custom\Service\FaceDetectResult\Worker;

use Topxia\Service\Plumber\BaseWorker;

class FaceDetectResultWorker extends BaseWorker
{
    public function process($data)
    {
        $this->getFaceDetectResultService()->addFaceDetectResult($data);
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }
}
