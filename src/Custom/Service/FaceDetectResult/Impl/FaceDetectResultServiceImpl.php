<?php

namespace Custom\Service\FaceDetectResult\Impl;

use Topxia\Service\Common\BaseService;
use Custom\Service\FaceDetectResult\FaceDetectResultService;
use Topxia\Service\Beanstalk\BeanstalkdClientFactory;

class FaceDetectResultServiceImpl extends BaseService implements FaceDetectResultService
{
    public function getFaceDetectResult($id)
    {
        return $this->getFaceDetectResultDao()->getFaceDetectResult($id);
    }

    public function buildLessonDetectResultWithRequest($lessonViewId, $imageData)
    {
        if (!isset($lessonViewId) || empty($lessonViewId)) {
            return false;
        }

        $lessonView = $this->getLessonViewDao()->getLessonView($lessonViewId);

        $lesson                              = $this->getCourseService()->getLesson($lessonView['lessonId']);
        $course                              = $this->getCourseService()->getCourse($lessonView['courseId']);
        $faceDetectResult['type']            = 'lesson';
        $faceDetectResult['courseId']        = $lesson['courseId'];
        $faceDetectResult['lessonId']        = $lesson['id'];
        $faceDetectResult['lessonViewId']    = $lessonViewId;
        $faceDetectResult['content']         = $course['title']."-".$lesson['title'];
        $faceDetectResult['currentLoopTime'] = $lessonView['currentLoopTime'];
        $fileUrl                             = $this->getFaceDetectService()->saveFaceImg($imageData, $faceDetectResult['type']);
        $faceDetectResult['fileUrl']         = $fileUrl['fileUrl'];
        return $faceDetectResult;
    }

    public function buildTestpaperDetectResultWithRequest($testpaperLessonId, $imageData, $currentLoopTime)
    {
        if (!isset($testpaperLessonId) || empty($testpaperLessonId)) {
            return false;
        }

        $lesson  = $this->getCourseService()->getLesson($testpaperLessonId);
        $course = $this->getCourseService()->getCourse($lesson['courseId']);
        $faceDetectResult['type']            = 'testpaper';
        $faceDetectResult['courseId']        = $lesson['courseId'];
        $faceDetectResult['lessonId']        = $lesson['id'];
        $faceDetectResult['content']         = $course['title']."-".$lesson['title'];
        $faceDetectResult['currentLoopTime'] = $currentLoopTime;
        $fileUrl  = $this->getFaceDetectService()->saveFaceImg($imageData, $faceDetectResult['type']);
        $faceDetectResult['fileUrl']         = $fileUrl['fileUrl'];
        return $faceDetectResult;
    }

    public function buildLoginDetectResultWithRequest($imageData)
    {
        $faceDetectResult['type'] = 'login';

        $fileUrl                     = $this->getFaceDetectService()->saveFaceImg($imageData, $faceDetectResult['type']);
        $faceDetectResult['fileUrl'] = $fileUrl['fileUrl'];

        return $faceDetectResult;
    }

    public function searchFaceDetectResults($conditions, $orderBys, $start, $limit)
    {
        $conditions = $this->_prepareSearchConditions($conditions);

        switch ($orderBys) {
            case 'created':
                $orderBys = array('createdTime', 'DESC');
                break;
            case 'createdByAsc':
                $orderBys = array('createdTime', 'ASC');
                break;

            default:
                throw $this->createServiceException('参数sort不正确。');
                break;
        }

        return $this->getFaceDetectResultDao()->searchFaceDetectResults($conditions, $orderBys, $start, $limit);
    }

    public function searchFaceDetectResultsCount($conditions)
    {
        $conditions = $this->_prepareSearchConditions($conditions);
        return $this->getFaceDetectResultDao()->searchFaceDetectResultsCount($conditions);
    }

    public function findLessonCountGroupsByUserIdAndLessonViewIds($userId, $lessonViewIds, $currentLoopTime = 1)
    {
        return $this->getFaceDetectResultDao()->findLessonCountGroupsByUserIdAndLessonViewIds($userId, $lessonViewIds, $currentLoopTime);
    }

    public function findFaceDectResultByUserIdAndLessonViewId($userId, $lessonViewId)
    {
        return $this->getFaceDetectResultDao()->findFaceDectResultByUserIdAndLessonViewId($userId, $lessonViewId);
    }

    public function findFaceDectResultByUserIdAndCourseIdAndCurrentLoopTime($userId, $courseId, 
        $currentLoopTime)
    {
        return $this->getFaceDetectResultDao()->findFaceDectResultByUserIdAndCourseIdAndCurrentLoopTime(
            $userId, $courseId, $currentLoopTime);
    }

    public function putToMqIfNeed($faceDetectResult)
    {
        $client = $this->getMqClient('faceDetectResultWorker');
        if ($client) {
            $client->put(0, 0, 100, json_encode($faceDetectResult));
        } else {
            $this->addFaceDetectResult($faceDetectResult);
        }
    }

    public function addMissedFaceDetectResult($faceDetectResult)
    {
        return $this->addFaceDetectResult($faceDetectResult);
    }

    public function addFaceDetectResult($faceDetectResult)
    {
        $userInfo = $this->getUserService()->getUser($faceDetectResult['userId']);
        $faceDetectResult['mobile'] = $userInfo['verifiedMobile'];
        $dbResult = $this->getFaceDetectResultDao()->addFaceDetectResult($faceDetectResult);
        
        if ($faceDetectResult['type'] == 'lesson' ||  $faceDetectResult['type'] == 'testpaper') {
            $today = date('Y-m-d');
            $publicUploadDir = __DIR__.'/../../../../../web/files';
            //$userInfo = $this->getUserService()->getUser($faceDetectResult['userId']);
            $userProfile = $this->getUserService()->getUserProfile($faceDetectResult['userId']);
            $remote_dir = '/jy/'.$today.'/'.$userProfile['idcard'].'/';
            $fileUrl = $faceDetectResult['fileUrl'];
            $fileArray = explode('/', $fileUrl);
            $fileName = $fileArray[count($fileArray)-1];
            $sourceFile = str_replace('public:/', $publicUploadDir, $fileUrl);
            $remoteFile = $remote_dir.$fileName;

            $data = array(
                'areaCode' => $userInfo['areaCode'],
                'sourceFile' => $sourceFile,
                'remoteFile' => $remoteFile,
                'type' => 'sync_certificate'
            );
            $this->getSyncFtpService()->addSyncFtpInfo($data);
        }

        return $dbResult;
    }

    public function updateFaceDetectResult($id, $fields)
    {
        return $this->getFaceDetectResultDao()->updateFaceDetectResult($id, $fields);
    }

    public function deleteFaceDetectResult($userId, $courseId, $currentLoopTime = null)
    {
        return $this->getFaceDetectResultDao()->deleteFaceDetectResult($userId, $courseId, $currentLoopTime);
    }

    protected function getFaceDetectResultDao()
    {
        return $this->createDao('Custom:FaceDetectResult.FaceDetectResultDao');
    }

    protected function getFaceDetectService()
    {
        return $this->createService('Custom:FaceDetect.FaceDetectService');
    }

    protected function getCourseService()
    {
        return $this->createService('Course.CourseService');
    }

    protected function getSyncFtpService()
    {
        return $this->createService('Custom:Sync.SyncFtpService');
    }

    protected function getLessonViewDao()
    {
        return $this->createDao('Course.LessonViewDao');
    }

    protected function getUserService()
    {
        return $this->createService('Custom:User.UserService');
    }

    protected function _prepareSearchConditions($conditions)
    {
        if (!empty($conditions['startDateTime']) && !empty($conditions['endDateTime'])) {
            $conditions['startDateTime'] = strtotime($conditions['startDateTime']);
            $conditions['endDateTime']   = strtotime($conditions['endDateTime']);
        } else {
            unset($conditions['startDateTime']);
            unset($conditions['endDateTime']);
        }

        if (!empty($conditions['endDateTimeInt'])) {
            $conditions['endDateTime'] = $conditions['endDateTimeInt'];
            unset($conditions['endDateTimeInt']);
        }

        return $conditions;
    }

    private function getMqClient($tubeName)
    {
        if (isset($this->mqClient)) {
            $client = $this->mqClient;
        } else {
            $client = BeanstalkdClientFactory::getClient($tubeName);
        }
        return $client;
    }
}
