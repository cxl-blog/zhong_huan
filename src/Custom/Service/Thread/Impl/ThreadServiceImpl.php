<?php

namespace Custom\Service\Thread\Impl;

use Custom\Service\Thread\ThreadService;
use Topxia\Service\Thread\Impl\ThreadServiceImpl as BaseService;
use Custom\Common\Util\StringUtils;

class ThreadServiceImpl extends BaseService implements ThreadService
{
    public function createPost($fields)
    {
        $thread = null;

        if (!empty($fields['threadId'])) {
            $thread = $this->getThread($fields['threadId']);

            $fields['targetType'] = $thread['targetType'];
            $fields['targetId']   = $thread['targetId'];
        }

        $this->tryAccess('post.create', $fields);

        $event = $this->dispatchEvent('thread.post.before_create', $fields);

        if ($event->isPropagationStopped()) {
            throw $this->createServiceException('回复次数过多，请稍候尝试。');
        }

        $fields['content'] = $this->sensitiveFilter($fields['content'], $fields['targetType'].'-thread-post-create');

        $fields['content']     = $this->purifyHtml($fields['content']);
        $fields['ats']         = StringUtils::parseAts($fields);

        $user                  = $this->getCurrentUser();
        $fields['userId']      = $user['id'];
        $fields['createdTime'] = time();
        $fields['parentId']    = empty($fields['parentId']) ? 0 : intval($fields['parentId']);

        $parent = null;

        if ($fields['parentId'] > 0) {
            $parent = $this->getThreadPostDao()->getPost($fields['parentId']);

            if (empty($parent)) {
                throw $this->createServiceException("parentId参数不正确！");
            }

            $this->getThreadPostDao()->wavePost($parent['id'], 'subposts', 1);
        }

        if (!empty($fields['replyOriginUsername'])) {
            unset($fields['replyOriginUsername']);
        }

        $post = $this->getThreadPostDao()->addPost($fields);

        if (!empty($fields['threadId'])) {
            $this->getThreadDao()->updateThread($thread['id'], array(
                'lastPostUserId' => $post['userId'],
                'lastPostTime'   => $post['createdTime']
            ));

            $this->getThreadDao()->waveThread($thread['id'], 'postNum', +1);
        }

        $notifyData = $this->getPostNotifyData($post, $thread, $user);

        if (!empty($post['ats'])) {
            foreach ($post['ats'] as $userId) {
                if ($user['id'] == $userId) {
                    continue;
                }

                $this->getNotifiactionService()->notify($userId, 'thread.post_at', $notifyData);
            }
        }

        //给主贴主人发通知
        $atUserIds = array_values($post['ats']);

        if ($post['parentId'] == 0 && $thread && ($thread['userId'] != $post['userId']) && (!in_array($thread['userId'], $atUserIds))) {
            $this->getNotifiactionService()->notify($thread['userId'], 'thread.post_create', $notifyData);
        }

        //回复的回复的人给该回复的作者发通知

        if ($post['parentId'] > 0 && ($parent['userId'] != $post['userId']) && (!in_array($parent['userId'], $atUserIds))) {
            $this->getNotifiactionService()->notify($parent['userId'], 'thread.post_create', $notifyData);
        }

        $this->dispatchEvent('thread.post.create', $post);

        return $post;
    }
}