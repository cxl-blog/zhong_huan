<?php

namespace Custom\Serivce\Thread\Tests;

use Topxia\Service\Common\BaseTestCase;

class ThreadServiceTest extends BaseTestCase
{
    public function testCreatePost()
    {
        $this->setServiceKernel();

        $fields = array(
            'threadId' => 1,
            'content' => '@1356****088: 11111',
            'parentId' => 1,
            'replyOriginUsername' => '13567730088'
        );

        $this->mock('Thread.ThreadDao');
        $this->mock('SensitiveWord:Sensitive.SensitiveService');
        $this->mock('Custom:User.UserService');
        $this->mock('Thread.ThreadPostDao');
        $this->mock('User.NotificationService');

        $this->getThreadDao()->shouldReceive('getThread')->andReturn(array(
            'id' => 1,
            'targetType' => 'article',
            'targetId' => 1,
            'userId' => 2,
            'title' => '测试'
        ));

        $this->getSensitiveService()->shouldReceive('sensitiveCheck')->andReturn('11111');

        $this->getUserService()->shouldReceive('findUsersByNicknames')->andReturn(array(
            '13567730096' => 4
        ));

        $this->getThreadPostDao()->shouldReceive('getPost')->andReturn(array(
            'id' => 1,
        ));

        $this->getThreadPostDao()->shouldReceive('wavePost');

        $this->getThreadPostDao()->shouldReceive('addPost')->andReturn(array(
            'id' => 2,
            'userId' => 1,
            'createdTime' => '1324123124',
            'parentId' => 0,
            'content' => '111111',
            'ats' => array('13567730096' => 4),
            'targetType' => 'article',
            'targetId' => 1
        ));

        $this->getThreadDao()->shouldReceive('updateThread');

        $this->getThreadDao()->shouldReceive('waveThread');

        $this->getNotifiactionService()->shouldReceive('notify');

        $this->getThreadService()->createPost($fields);

        $this->getThreadDao()->shouldHaveReceived('getThread');

        $this->getSensitiveService()->shouldHaveReceived('sensitiveCheck');

        $this->getUserService()->shouldHaveReceived('findUsersByNicknames');

        $this->getThreadPostDao()->shouldHaveReceived('getPost');

        $this->getThreadPostDao()->shouldHaveReceived('wavePost');

        $this->getThreadPostDao()->shouldHaveReceived('addPost');

        $this->getThreadDao()->shouldHaveReceived('updateThread');

        $this->getThreadDao()->shouldHaveReceived('waveThread');

        $this->getNotifiactionService()->shouldHaveReceived('notify');
    }

    /**
     * @expectedException Exception
     */
    public function testCreatePostWithEmptyParent()
    {
        $this->setServiceKernel();

        $fields = array(
            'threadId' => 1,
            'content' => '@1356****088: 11111',
            'parentId' => 1,
            'replyOriginUsername' => '13567730088'
        );

        $this->mock('Thread.ThreadDao');
        $this->mock('SensitiveWord:Sensitive.SensitiveService');
        $this->mock('Custom:User.UserService');
        $this->mock('Thread.ThreadPostDao');

        $this->getThreadDao()->shouldReceive('getThread')->andReturn(array(
            'id' => 1,
            'targetType' => 'article',
            'targetId' => 1,
            'userId' => 2,
            'title' => '测试'
        ));

        $this->getSensitiveService()->shouldReceive('sensitiveCheck')->andReturn('11111');

        $this->getUserService()->shouldReceive('findUsersByNicknames')->andReturn(array(
            '13567730096' => 4
        ));

        $this->getThreadPostDao()->shouldReceive('getPost')->andReturn(array(
        ));

        $this->getThreadService()->createPost($fields);

        $this->getThreadDao()->shouldHaveReceived('getThread');

        $this->getSensitiveService()->shouldHaveReceived('sensitiveCheck');

        $this->getUserService()->shouldHaveReceived('findUsersByNicknames');

        $this->getThreadPostDao()->shouldHaveReceived('getPost');
    }

    protected function getThreadService()
    {
        return $this->getServiceKernel()->createService('Custom:Thread.ThreadService');
    }

    protected function getThreadPostDao()
    {
        return $this->getServiceKernel()->createDao('Thread.ThreadPostDao');
    }

    protected function getThreadDao()
    {
        return $this->getServiceKernel()->createDao('Thread.ThreadDao');
    }

    protected function getSensitiveService()
    {
        return $this->getServiceKernel()->createService('SensitiveWord:Sensitive.SensitiveService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getNotifiactionService()
    {
        return $this->getServiceKernel()->createService('User.NotificationService');
    }
}