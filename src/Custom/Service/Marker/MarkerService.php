<?php

namespace Custom\Service\Marker;

interface MarkerService
{
    public function addMarker($mediaId, $fields);

    public function addQuestionMarker($mediaId, $fields);

    public function findMarkersByMediaId($mediaId);

    public function findMarkersMetaByMediaId($mediaId);

    public function findFaceMarkersMetaByMediaId($mediaId);

    public function findQuestionMarkersMetaByMediaId($mediaId);

    public function searchMarkersCount($conditions);

    /**
     * 更新弹脸的状态
     * @param isPassed true / false
     */
    public function addFaceMarkerResult($markerId, $userId, $isPassed, $currentLoopTime = null);

    /**
     * @return [type] 支持2种类型, face 和 question
     */
    public function getMarkers($mediaId, $type);

    public function deleteUserLessonsMarkers($userId, $lessonIds, $currentLoopTime = null);
}
