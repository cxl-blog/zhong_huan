<?php
namespace Custom\Service\Marker\Impl;

use Topxia\Service\Marker\Impl\QuestionMarkerResultServiceImpl as BaseServiceImpl;

class QuestionMarkerResultServiceImpl extends BaseServiceImpl
{
    public function finishCurrentQuestion($markerId, $userId, $questionMarkerId, $answer, $type, $lessonId, $currentLoopTime = null)
    {
        $questionMarker = $this->getQuestionMarkerService()->getQuestionMarker($questionMarkerId);

        if (in_array($type, array('single_choice', 'determine'))) {
            $status = in_array($answer, $questionMarker['answer']) ? 'right' : 'wrong';
        }

        if ($type == 'uncertain_choice') {
            if (array_diff($questionMarker['answer'], $answer) || array_diff($answer, $questionMarker['answer'])) {
                if (array_diff($questionMarker['answer'], $answer) && !array_diff($answer, $questionMarker['answer'])) {
                    $status = 'partRight';
                } else {
                    $status = 'wrong';
                }
            } else {
                $status = 'right';
            }
        }

        if ($type == 'fill') {
            foreach ($questionMarker['answer'] as $key => $questionMarkerAnswer) {
                $status = in_array($answer, $questionMarkerAnswer) ? 'right' : 'wrong';
            }
        }

        if ($type == 'choice') {
            if (array_diff($questionMarker['answer'], $answer) && array_diff($answer, $questionMarker['answer'])) {
                $status = 'wrong';
            } else {
                $status = 'right';
            }
        }

        $questionMarkerResult = $this->findByUserIdAndQuestionMarkerId($userId, $questionMarkerId);
        return $this->addQuestionMarkerResult(array(
            'markerId'         => $markerId,
            'questionMarkerId' => $questionMarkerId,
            'lessonId'         => $lessonId,
            'userId'           => $userId,
            'status'           => $status,
            'answer'           => serialize($answer),
            'createdTime'      => time(),
            'updatedTime'      => time(),
            'currentLoopTime'  => $currentLoopTime
        ));
    }

    public function findByUserIdAndMarkerId($userId, $markerId, $currentLoopTime = null)
    {
        return $this->getQuestionMarkerResultDao()->findByUserIdAndMarkerId($userId, $markerId, $currentLoopTime);
    }

    protected function getQuestionMarkerResultDao()
    {
        return $this->createDao('Custom:Marker.QuestionMarkerResultDao');
    }
}
