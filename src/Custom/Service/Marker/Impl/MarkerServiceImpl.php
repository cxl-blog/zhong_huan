<?php

namespace Custom\Service\Marker\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Constants\DbValue;
use Custom\Service\Marker\MarkerService;
use Topxia\Service\Marker\Impl\MarkerServiceImpl as TopxiaMarkerServiceImpl;

class MarkerServiceImpl extends TopxiaMarkerServiceImpl implements MarkerService
{
    /**
     * 这里的mediaId 指的是 lessonId
     */
    public function addMarker($mediaId, $fields)
    {
        $media = $this->getCourseService()->getLesson($mediaId);

        if (empty($media)) {
            $media['id'] = 0;
            $this->getLogService()->error('mediaId', 'isNotExist', "视频文件不存在！");
        }

        if (!isset($fields['second']) || $fields['second'] == "") {
            throw $this->createServiceException("请输入弹题时间！");
        }

        $marker = array(
            'mediaId'     => $media['id'],
            'createdTime' => time(),
            'updatedTime' => time(),
            'second'      => $fields['second']
        );

        return $this->getMarkerDao()->addMarker($marker);
    }

    /**
     * 这里的mediaId 指的是 lessonId
     */
    public function addQuestionMarker($mediaId, $fields)
    {
        $marker   = $this->addMarker($mediaId, $fields);
        $question = $this->getQuestionMarkerService()->addQuestionMarker($fields['questionId'], $marker['id'], 1);
        return $question;
    }

    public function findMarkersByMediaId($mediaId)
    {
        return $this->getMarkerDao()->findMarkersByMediaId($mediaId);
    }

    public function findFaceMarkersMetaByMediaId($mediaId)
    {
        $markers = $this->findMarkersByMediaId($mediaId);

        if (empty($markers)) {
            return array();
        }

        $markerIds         = ArrayToolkit::column($markers, 'id');
        $questionMarkers   = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionMarkerIds = ArrayToolkit::column($questionMarkers, 'markerId');
        $faceMarkerIds     = array_diff($markerIds, $questionMarkerIds);

        // 原先是 array( 1 => 'markId1', 2=> 'markerId2'), 改成 array(0=> 'markerId1', 1=>'markerId2'), 否则dao这边会报错
        $formatedFaceMarkerIds = array();

        foreach ($faceMarkerIds as $faceMarkerId) {
            array_push($formatedFaceMarkerIds, $faceMarkerId);
        }

        $faceMarkers = $this->getMarkersByIds($formatedFaceMarkerIds);

        return $faceMarkers;
    }

    public function findQuestionMarkersMetaByMediaId($mediaId)
    {
        $markers = $this->findMarkersByMediaId($mediaId);

        if (empty($markers)) {
            return array();
        }

        $markers         = ArrayToolkit::index($markers, 'id');
        $markerIds       = ArrayToolkit::column($markers, 'id');
        $questionMarkers = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionGroups  = ArrayToolkit::index($questionMarkers, 'markerId');

        foreach ($markers as &$marker) {
            if (!empty($questionGroups[$marker['id']])) {
                $questionMarker = $questionGroups[$marker['id']];

                if (!empty($questionMarker['stem'])) {
                    $questionMarker['includeImg'] = (preg_match('/<img (.*?)>/', $questionMarker['stem'])) ? true : false;
                }

                if (!empty($questionMarker['type']) && $questionMarker['type'] == 'fill') {
                    $questionMarker['stem'] = preg_replace('/\[\[.+?\]\]/', '____', $questionMarker['stem']);
                }

                $marker['questionMarkers'] = $questionMarker;
            } else {
                unset($markers[$marker['id']]);
            }
        }

        return $markers;
    }

    public function findMarkersMetaByMediaId($mediaId)
    {
        $markers = $this->findMarkersByMediaId($mediaId);

        if (empty($markers)) {
            return array();
        }

        $markerIds         = ArrayToolkit::column($markers, 'id');
        $questionMarkers   = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionMarkerIds = ArrayToolkit::column($questionMarkers, 'markerId');
        $faceMarkerIds     = array_diff($markerIds, $questionMarkerIds);
        $faceMarkers       = $this->getMarkersByIds($faceMarkerIds);
        $questionGroups    = ArrayToolkit::group($questionMarkers, 'markerId');

        foreach ($markers as &$marker) {
            if (!empty($questionGroups[$marker['id']])) {
                $questionMarker = $questionGroups[$marker['id']];

                if (!empty($questionMarker['stem'])) {
                    $questionMarker['includeImg'] = (preg_match('/<img (.*?)>/', $questionMarker['stem'])) ? true : false;
                }

                if (!empty($questionMarker['type']) && $questionMarker['type'] == 'fill') {
                    $questionMarker['stem'] = preg_replace('/\[\[.+?\]\]/', '____', $questionMarker['stem']);
                }

                $marker['questionMarkers'] = $questionMarker;
            }
        }

        return $markers;
    }

    public function searchMarkersCount($conditions)
    {
        $conditions = $this->prepareMarkerConditions($conditions);

        return $this->getMarkerDao()->searchMarkersCount($conditions);
    }

    public function addFaceMarkerResult($markerId, $userId, $isPassed, $currentLoopTime = null)
    {
        $result = array(
            'isPassed'        => $isPassed,
            'markerId'        => $markerId,
            'userId'          => $userId,
            'currentLoopTime' => $currentLoopTime
        );
        $this->getFaceMarkerResultDao()->addFaceMarkerResult($result);
    }

    public function getMarkers($mediaId, $type)
    {
        $markers           = $this->findMarkersByMediaId($mediaId);
        $markerIds         = ArrayToolkit::column($markers, 'id');
        $questionMarkers   = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionMarkers   = ArrayToolkit::index($questionMarkers, 'markerId');
        $questionMarkerIds = ArrayToolkit::column($questionMarkers, 'markerId');
        $faceMarkerIds     = array_diff($markerIds, $questionMarkerIds);

        if (isset($type) && $type == 'question') {
            $markers = $this->getMarkersByIds($questionMarkerIds);
        } elseif (isset($type) && $type == 'face') {
            $markers = $this->getMarkersByIds($faceMarkerIds);
        }

        return $markers;
    }

    public function deleteUserLessonsMarkers($userId, $lessonIds, $currentLoopTime = null)
    {
        $markers = $this->getMarkerDao()->getMarkersByMediaIds($lessonIds);

        if (!empty($markers)) {
            $markerIds = ArrayToolkit::column($markers, 'id');

            $this->getFaceMarkerResultDao()->deleteUserLessonsFaceMarkers($userId, $markerIds, $currentLoopTime);
            $this->getQuestionMarkerResultDao()->deleteUserLessonsQuestionMarkers($userId, $lessonIds, $currentLoopTime);
        }
    }

    public function getFaceMarkerStatus($userId, $faceMarkIds, $currentLoopTime = null)
    {
        $result = array();

        if (!empty($faceMarkIds)) {
            $faceMarkerResult = $this->getFaceMarkerResultDao()->getFaceMarkerResult($userId, $faceMarkIds, $currentLoopTime);

            $existedFaceMarkerStatus = array();

            if (isset($faceMarkerResult)) {
                foreach ($faceMarkerResult as $key => $singleMarker) {
                    $existedFaceMarkerStatus[$singleMarker['markerId']] = $singleMarker['isPassed'];
                }
            }

            foreach ($faceMarkIds as $key => $faceMarkId) {
                if (!empty($existedFaceMarkerStatus) && isset($existedFaceMarkerStatus[$faceMarkId])) {
                    $result[$faceMarkId] = $existedFaceMarkerStatus[$faceMarkId];
                } else {
                    $result[$faceMarkId] = DbValue::FALSE;
                }

                if ($result[$faceMarkId] == DbValue::TRUE) {
                    $result[$faceMarkId] = true;
                }
            }
        }

        return $result;
    }

    public function isFinishMarker($userId, $markerId, $currentLoopTime = null)
    {
        $questionMarkers = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerId($markerId);

        if (empty($questionMarkers)) {
            return true;
        }

        $questionMarkerResults = $this->getQuestionMarkerResultService()->findByUserIdAndMarkerId($userId, $markerId, $currentLoopTime);

        if (!in_array('none', ArrayToolkit::column($questionMarkerResults, 'status')) && !array_diff(ArrayToolkit::column($questionMarkers, 'id'), ArrayToolkit::column($questionMarkerResults, 'questionMarkerId'))) {
            return true;
        }

        return false;
    }

    protected function getFaceMarkerResultDao()
    {
        return $this->createDao('Custom:Marker.FaceMarkerResultDao');
    }

    protected function getQuestionMarkerResultDao()
    {
        return $this->createDao('Custom:Marker.QuestionMarkerResultDao');
    }

    protected function getQuestionMarkerResultService()
    {
        return $this->createService('Custom:Marker.QuestionMarkerResultService');
    }

    protected function getMarkerDao()
    {
        return $this->createDao('Custom:Marker.MarkerDao');
    }

    protected function getCourseService()
    {
        return $this->createService('Course.CourseService');
    }
}
