<?php
namespace Custom\Service\Marker\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Topxia\Service\Marker\Dao\Impl\QuestionMarkerResultDaoImpl as BaseQuestionMarkerResultDaoImpl;
use Custom\Service\Marker\Dao\QuestionMarkerResultDao;

class QuestionMarkerResultDaoImpl extends BaseQuestionMarkerResultDaoImpl implements QuestionMarkerResultDao
{
    public function deleteUserLessonsQuestionMarkers($userId, $lessonIds, $currentLoopTime = null)
    {
        $lessonIdsSql = str_repeat('?,', count($lessonIds) - 1).'?';
        $sql    = "DELETE FROM {$this->table} WHERE userId = ? and currentLoopTime = ? and lessonId in ($lessonIdsSql)";

        $params = array_merge(array($userId, $currentLoopTime), $lessonIds);
        $result = $this->getConnection()->executeUpdate($sql, $params);
        $this->clearCached();
        return $result;
    }

    public function findByUserIdAndMarkerId($userId, $markerId, $currentLoopTime = null)
    {
        $sql = "SELECT * FROM {$this->table} WHERE userId = ? and markerId = ? and currentLoopTime = ?";
        return $this->getConnection()->fetchAll($sql, array($userId, $markerId, $currentLoopTime)) ?: array();
    }
}
