<?php
namespace Custom\Service\Marker\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Marker\Dao\FaceMarkerResultDao;

class FaceMarkerResultDaoImpl extends BaseDao implements FaceMarkerResultDao
{
    protected $table = 'face_marker_result';

    public function getFaceMarkerResult($userId, $faceMarkerIds, $currentLoopTime = null)
    {
        $markQuotaStr = str_repeat('?,', count($faceMarkerIds) - 1).'?';
        $sql          = "SELECT * FROM {$this->table} WHERE userId = ? and currentLoopTime = ? and markerId in ({$markQuotaStr})";
        $params       = array_merge(array($userId, $currentLoopTime), $faceMarkerIds);
        return $this->getConnection()->fetchAll($sql, $params) ?: null;
    }

    public function addFaceMarkerResult($markerResult)
    {
        $sql           = "SELECT * FROM {$this->table} WHERE markerId = ? and currentLoopTime = ? limit 1";
        $existedMarker = $this->getConnection()->fetchAll($sql, array($markerResult['markerId'], $markerResult['currentLoopTime'])) ?: null;

        if (!isset($existedMarker)) {
            $affected = $this->getConnection()->insert($this->table, $markerResult);

            if ($affected <= 0) {
                throw $this->createDaoException('Insert course error.');
            }
        }
    }

    public function deleteUserLessonsFaceMarkers($userId, $markerIds, $currentLoopTime = null)
    {
        $markerIdsSql = str_repeat('?,', count($markerIds) - 1).'?';
        $sql          = "DELETE FROM {$this->table} WHERE userId = ? and currentLoopTime = ? and markerId in ($markerIdsSql)";
        $params       = array_merge(array($userId, $currentLoopTime), $markerIds);
        $result       = $this->getConnection()->executeUpdate($sql, $params);

        $this->clearCached();
        return $result;
    }
}
