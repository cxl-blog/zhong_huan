<?php
namespace Custom\Service\Marker\Dao\Impl;

use Topxia\Service\Marker\Dao\Impl\MarkerDaoImpl as BaseMarkerDaoImpl;
use Custom\Service\Marker\Dao\MarkerDao;

class MarkerDaoImpl extends BaseMarkerDaoImpl implements MarkerDao
{
    public function getMarkersByMediaIds($mediaIds)
    {
        $mediaIdsSql = str_repeat('?,', count($mediaIds) - 1).'?';
        $sql   = "SELECT * FROM {$this->table} WHERE id IN ({$mediaIdsSql});";

        return $this->getConnection()->fetchAll($sql, $mediaIds) ?: array();
    }
}
