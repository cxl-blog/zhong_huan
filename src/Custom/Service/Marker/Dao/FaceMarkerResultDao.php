<?php
namespace Custom\Service\Marker\Dao;

interface FaceMarkerResultDao
{
    /**
     * 未人脸检测过的点, 将不返回数据
     */
    public function getFaceMarkerResult($userId, $faceMarkerIds, $currentLoopTime = null);

    /**
     * 新增弹脸的状态
     */
    public function addFaceMarkerResult($markerResult);

    public function deleteUserLessonsFaceMarkers($userId, $lessonIds, $currentLoopTime = null);
}
