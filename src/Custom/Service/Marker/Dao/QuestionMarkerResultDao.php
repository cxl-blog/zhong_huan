<?php
namespace Custom\Service\Marker\Dao;

interface QuestionMarkerResultDao
{
    public function deleteUserLessonsQuestionMarkers($userId, $lessonIds, $currentLoopTime = null);
}
