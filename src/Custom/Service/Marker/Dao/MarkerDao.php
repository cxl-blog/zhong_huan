<?php

namespace Custom\Service\Marker\Dao;

interface MarkerDao
{
    public function getMarkersByMediaIds($mediaIds);
}
