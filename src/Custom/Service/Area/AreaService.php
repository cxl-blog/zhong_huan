<?php
namespace Custom\Service\Area;

interface AreaService
{
    public function getArea($id);

    public function addArea($area);

    public function getAreaByCode($code);

    public function getAreaBySchoolId($schoolId);

    public function updateArea($id, $fields);

    public function deleteArea($id);

    public function searchAreas($conditions, $orderBys, $start, $limit);

    public function searchAreasCount($conditions);

    public function getAreas();

    public function getAreaByCodes($codes);

    public function getAreaByProvince($province);

    public function getAreaCodeByProvince($province);

    public function getAreaByName($name);

    public function getUserSchoolIdBySchoolType($schoolType, $user);
}
