<?php
namespace Custom\Service\Area\Impl;

use Custom\Service\Area\AreaService;
use Topxia\Service\Common\BaseService;

class AreaServiceImpl extends BaseService implements AreaService
{
    public function getArea($id)
    {
        return $this->getAreaDao()->getArea($id);
    }

    public function addArea($area)
    {
        return $this->getAreaDao()->addArea($area);
    }

    public function getAreaByCode($code)
    {
        return $this->getAreaDao()->getAreaByCode($code);
    }

    public function getAreaBySchoolId($schoolId)
    {
        return $this->getAreaDao()->getAreaBySchoolId($schoolId);
    }

    public function updateArea($id, $fields)
    {
        return $this->getAreaDao()->updateArea($id, $fields);
    }

    public function deleteArea($id)
    {
        return $this->getAreaDao()->deleteArea($id);
    }

    public function searchAreas($conditions, $orderBys, $start, $limit)
    {
        return $this->getAreaDao()->searchAreas($conditions, $orderBys, $start, $limit);
    }

    public function searchAreasCount($conditions)
    {
        return $this->getAreaDao()->searchAreasCount($conditions);
    }

    public function getAreaByProvince($province)
    {
        $conditions['province'] = $province;
        $count                  = $this->searchAreasCount($conditions);
        return $this->searchAreas(
            $conditions,
            array('id', 'desc'),
            0,
            $count

        );
    }

    public function getAreaByName($name)
    {
        return $this->getAreaDao()->getAreaByName($name);
    }

    public function getUserSchoolIdBySchoolType($schoolType, $user)
    {
        if ($schoolType == 'branchSchool') {
            $area = $this->getAreaByCode($user['areaCode']);
            return $area['schoolId'];
        } else {
            return 0;
        }
    }

    protected function getAreaDao()
    {
        return $this->createDao('Custom:Area.AreaDao');
    }

    public function getAreas()
    {
        return $this->getAreaDao()->getAreas();
    }

    public function getAreaByCodes($codes)
    {
        return $this->getAreaDao()->getAreaByCodes($codes);
    }
    
    public function getAreaAdminedBranchSchoolByUserIds($userIds)
    {
        return $this->getAreaDao()->getAreaAdminedBranchSchoolByUserIds($userIds);
    }

    public function getAreaCodeByProvince($province)
    {
        return $this->getAreaDao()->getAreaCodeByProvince($province);
    }
}
