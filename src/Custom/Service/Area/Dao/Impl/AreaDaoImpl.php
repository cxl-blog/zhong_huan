<?php
namespace Custom\Service\Area\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\Area\Dao\AreaDao;

class AreaDaoImpl extends BaseDao implements AreaDao
{
    protected $table = 'area';

    public function getArea($id)
    {
        $that = $this;
        return $this->fetchCached("id:{$id}", $id,
            function ($id) use ($that) {
                $sql = "SELECT * FROM {$that->table} WHERE id = ? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($id)) ?: null;
            }
        );
    }

    public function getAreaByCodes($codes)
    {
        if (empty($codes)) {
            return array();
        }

        $marks = str_repeat('?,', count($codes) - 1).'?';

        $that = $this;
        $keys = implode(',', $codes);
        return $this->fetchCached("codes:{$keys}", $marks, $codes, function ($marks, $codes) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} WHERE code IN ({$marks});";

            return $that->getConnection()->fetchAll($sql, $codes);
        }

        );
    }

    public function getAreas()
    {
        $that = $this;
        return $this->fetchCached("areas:all", 
            function () use ($that) {
                $sql = "SELECT * FROM {$that->table}";
                return $that->getConnection()->fetchAll($sql, array());
            }
        );
    }

    public function addArea($area)
    {
        $affected = $this->getConnection()->insert($this->table, $area);

        if ($affected <= 0) {
            throw $this->createDaoException('Insert area error:');
        }

        $this->clearCached();

        return $this->getArea($this->getConnection()->lastInsertId());
    }

    public function getAreaByCode($code)
    {
        $that = $this;
        return $this->fetchCached("code:{$code}", $code,
            function ($code) use ($that) {
                $sql = "SELECT * FROM {$that->table} WHERE code = ? LIMIT 1";
                return $that->getConnection()->fetchAssoc($sql, array($code)) ?: null;
            }
        );
    }

    public function getAreaBySchoolId($schoolId)
    {
        $that = $this;
        return $this->fetchCached("schoolId:{$schoolId}", $schoolId, function ($schoolId) use ($that) {
            $sql = "SELECT * FROM {$that->getTable()} where schoolId=? LIMIT 1";
            return $that->getConnection()->fetchAssoc($sql, array($schoolId)) ?: null;
        });
    }

    public function updateArea($id, $fields)
    {
        $this->getConnection()->update($this->table, $fields, array('id' => $id));
        $this->clearCached();
        return $this->getArea($id);
    }

    public function deleteArea($id)
    {
        $result = $this->getConnection()->delete($this->table, array('id' => $id));
        $this->clearCached();
        return $result;
    }

    public function searchAreas($conditions, $orderBys, $start, $limit)
    {
        $this->filterStartLimit($start, $limit);
        $builder = $this->createAreaQueryBuilder($conditions)
            ->select('*')
            ->setFirstResult($start)
            ->setMaxResults($limit);

        if (!empty($orderBys)) {
            $this->checkOrderBy($orderBys);
            $builder->addOrderBy($orderBys[0], $orderBys[1]);
        }

        return $builder->execute()->fetchAll() ?: array();
    }

    public function searchAreasCount($conditions)
    {
        $builder = $this->createAreaQueryBuilder($conditions)
            ->select('COUNT(id)');
        return $builder->execute()->fetchColumn(0);
    }

    protected function createAreaQueryBuilder($conditions)
    {
        if (isset($conditions['name'])) {
            $conditions['name'] = "%{$conditions['name']}%";
        }

        $builder = $this->createDynamicQueryBuilder($conditions)
            ->from($this->table, $this->table)
            ->andWhere('name LIKE :name')
            ->andWhere('code IN (:codes)')
            ->andWhere('schoolId =:schoolId')
            ->andWhere('schoolId !=:NoSchoolId')
            ->andWhere('province = :province');

        return $builder;
    }

    public function getAreaAdminedBranchSchoolByUserIds($userIds)
    {
        $userIdsSql     = str_repeat('?,', count($userIds) - 1).'?';
        $sql ="select userId, area.name from branch_school_user bUser ".
                  "inner join area ".
                  "on area.schoolId = bUser.schoolId ".
                  "where roles like '%ROLE_ADMIN%' and userId in ($userIdsSql)";
        return $this->getConnection()->fetchAll($sql, $userIds) ?: null;
       
    }

    public function getAreaCodeByProvince($province)
    {
        $sql = "SELECT code FROM area where province=?";
        return $this->getConnection()->fetchAssoc($sql, array($province)) ?: null;
    }

    public function getAreaByName($name)
    {
        $sql = "SELECT * FROM area where name=?";
        return $this->getConnection()->fetchAssoc($sql, array($name)) ?: null;
    }
}
