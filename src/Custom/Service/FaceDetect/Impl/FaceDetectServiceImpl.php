<?php
namespace Custom\Service\FaceDetect\Impl;

use Topxia\Common\CurlToolkit;
use Topxia\Service\User\CurrentUser;
use Topxia\Service\Common\BaseService;
use Custom\Service\FaceDetect\FaceDetectService;
use Custom\Common\Constants\DbValue;
use Custom\Common\Util\ImageUrlUtils;

/**
 * 人脸识别说明 见 {项目}/doc/人脸识别说明.md
 */
class FaceDetectServiceImpl extends BaseService implements FaceDetectService
{
    public function isFaceImgUploaded($userId)
    {
        $user = $this->getUserService()->getUser($userId);
        return !empty($user['faceImgPath']);
    }

    public function updateUserFaceImg($userId, $base64ImgData, $publicUploadDir)
    {
        $faceDetectResult['userId'] = $userId;//$this->getCurrentUser()->getId();
        $faceDetectResult['type']   = 'register';
        try {
            $uploadFolder = $publicUploadDir.'/camera/';
            $img          = str_replace(' ', '+', $base64ImgData);
            $data         = base64_decode($img);

            if (!file_exists($uploadFolder)) {
                mkdir($uploadFolder);
            }

            $filePath = $uploadFolder.uniqid().'.jpg';
            file_put_contents($filePath, $data);

            //base64ImgData info
            $imgFilePath = $uploadFolder.uniqid().'.txt';
            file_put_contents($imgFilePath, $base64ImgData);

            $faceDetectResult['fileUrl'] = 'public://camera/'.basename($filePath);

            $faceImgFeature = $this->convertImg2Feature($base64ImgData);
            $userInfo = $this->getUserDao()->updateUser($userId,
                array(
                    'faceImgPath'    => $filePath,
                    'faceImgFeature' => $faceImgFeature,
                    'base64FaceImgPath' => $imgFilePath
                )
            );
            $faceDetectResult['result']  = 'success';
            $this->getFaceDetectResultService()->putToMqIfNeed($faceDetectResult);
            return $userInfo;
        } catch (\RuntimeException $e) {
            $faceDetectResult['result'] = 'fail';
            $this->getFaceDetectResultService()->putToMqIfNeed($faceDetectResult);
            throw $e;
        }
    }

    public function updateBatchUserFaceImg($userId, $base64ImgData, $publicUploadDir)
    {
        $userIds = $this->getUserService()->findMoreAreaCodesByUserId($userId);
        foreach ($userIds as $userId) {
            $this->updateUserFaceImg($userId, $base64ImgData, $publicUploadDir);
        }
    }

    public function isFaceImgValid($userId, $base64ImgData, $faceDetectResult)
    {
        try{
            $recognition = floor($this->getImgRecognition($userId, $base64ImgData));
            $result = $recognition >= $this->getDbRecognition();

            $faceDetectResult['result'] = $result ? 'success' : 'fail';
            $faceDetectResult['regRate'] = $recognition;
            $this->getFaceDetectResultService()->putToMqIfNeed($faceDetectResult);
            return $result;
        }catch(\RuntimeException $e){
            $faceDetectResult['result'] = 'fail';
            $faceDetectResult['regRate'] = -1;
            $this->getFaceDetectResultService()->putToMqIfNeed($faceDetectResult);
            throw $e;
        }
    }

    public function getFaceDetectStatus($userId, CurrentUser $currentUser = null)
    {
        $status = array();

        if (!empty($currentUser) && ($currentUser->isAdmin() || $currentUser->isTeacher())) {
            $status = $this->getDefaultStatus();
        }

        if (empty($status)) {
            $user = $this->getUserService()->getFaceDetectStatus($userId);
            
            if ($user) {
                $status['isNeedFaceDetected'] = $user['faceable'] ? true : false;
                $status['isFaceCollected']    = !empty($user['faceImgPath']) && $user['facePhotoFinished'];
                $status['faceImgPath']        = ImageUrlUtils::getImagePath($user['faceImgPath']);
            } else {
                $status = $this->getDefaultStatus();
            }
        }

        return $status;
    }

    public function getFaceDetectStatusForTestpaper($userId, $testpaperId, $currentLoopTime)
    {
        $testpaper = $this->getTestpaperService()->getTestpaper($testpaperId);
        $courseId = explode('-', $testpaper['target'])[1]; //target 值如 course-1, 1即为courseId
        $courseMember = $this->getCourseService()->getCourseMember($courseId, $userId, $currentLoopTime);
        
        $faceDetectStatus = array(
            'isNeedFaceDetected' => false
        );

        if (!$courseMember['isTestpaperPassed']) {
            $course = $this->getCourseService()->getCourse($courseId);
            $area = $this->getAreaService()->getAreaByCode($course['areaCode']);

            $faceDetectStatus = array(
                'isNeedFaceDetected' => $area['isTestpaperFaceable'] == DbValue::TRUE,
                'maxSnapNum' => $area['maxSnapNum'],
                'snapInterval' => $area['snapInterval']
            );
        }

        return $faceDetectStatus;
    }

    public function saveFaceImg($base64ImgData, $type)
    {
        $img  = str_replace(' ', '+', $base64ImgData);
        $data = base64_decode($img);

        $publicUploadDir     = $this->getKernel()->getParameter('topxia.upload.public_directory');
        $fileFolder          = '/camera/'.$type.'/'.date('Ymd')."/";
        $url['fileUrl']      = 'public:/'.$fileFolder;
        $url['uploadFolder'] = $publicUploadDir.$fileFolder;

        if (!file_exists($url['uploadFolder'])) {
            mkdir($url['uploadFolder'], 0755, true);
        }

        $fileName = uniqid().'.jpg';
        $url['uploadFolder'] .= $fileName;
        $url['fileUrl'] .= $fileName;
        file_put_contents($url['uploadFolder'], $data);

        return $url;
    }

    protected function getLogService()
    {
        return $this->createService('System.LogService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getCourseService()
    {
        return $this->createService('Custom:Course.CourseService');
    }

    protected function getTestpaperService()
    {
        return $this->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getAreaService()
    {
        return $this->createService('Custom:Area.AreaService');
    }

    protected function getUserService()
    {
        return $this->createService('User.UserService');
    }

    protected function getUserDao()
    {
        return $this->createDao('User.UserDao');
    }

    protected function getSettingService()
    {
        return $this->createService('System.SettingService');
    }

    private function deleteFaceImgIfHas($userId)
    {
        $user = $this->getUserService()->getUser($userId);

        if (!empty($user['faceImgPath']) && file_exists($user['faceImgPath'])) {
            unlink($user['faceImgPath']);
        }
    }

    private function convertImg2Feature($base64ImgData)
    {
        $param = array(
            'faceimage' => $base64ImgData
        );
        $result = $this->getRemoteFaceDetectResult(
            $this->getImg2FeatureUrl(), $param);

        $preMaxFaceSize = 0;
        $maxFaceFeature = '';

        if ($result['result'] == 'true') {
            foreach ($result['facemodels'] as $key => $value) {
                $maxFaceSize = $value['facerectanglewidth'] * $value['facerectangleheight'];

                if ($preMaxFaceSize < $maxFaceSize) {
                    $preMaxFaceSize = $maxFaceSize;
                    $maxFaceFeature = $value['base64feature'];
                }
            }
        } else {
            $errorInfo = $result['errorinfo'];
            throw new \RuntimeException($errorInfo);
        }

        return $maxFaceFeature;
    }

    /*
     * return double
     */
    private function getImgRecognition($userId, $base64ImgData)
    {
        $user  = $this->getUserService()->getUser($userId);
        $param = array(
            'faceimage' => $base64ImgData,
            'feature'   => $user['faceImgFeature']
        );
        $result = $this->getRemoteFaceDetectResult(
            $this->getFaceCompareUrl(), $param);

        if ($result['result'] == 'true') {
            return $result['similar'] * 100;
        } else {
            $errorInfo = $result['errorinfo'];
            throw new \RuntimeException($errorInfo);
        }
    }

    private function getDbRecognition()
    {
        $loginBind = $this->getSettingService()->get('login_bind');

        if (!isset($loginBind['face_detect_rate'])) {
            return FaceDetectService::DEFAULT_FACE_DETECT_RATE;
        }

        return $loginBind['face_detect_rate'];
    }

    private function getImg2FeatureUrl()
    {
        return FaceDetectService::FACE_DETECT_URL_PREFIX.'FaceDetect';
    }

    private function getFaceCompareUrl()
    {
        return FaceDetectService::FACE_DETECT_URL_PREFIX.'Feature2FaceImage';
    }

    /*
     * 调用远程人脸检测相关api获取结果
     * @return array 对象
     */
    private function getRemoteFaceDetectResult($url, $param)
    {
        $paramStr     = '';
        $isFirstParam = true;

        foreach ($param as $key => $value) {
            if ($isFirstParam) {
                $isFirstParam = false;
            } else {
                $paramStr = $paramStr."&";
            }

            $paramStr = $paramStr.$key.'='.rawurlencode($value);
        }

        $jsonResult = CurlToolkit::request('POST', $url, $paramStr);
        return $jsonResult;
    }

    private function getDefaultStatus()
    {
        return array(
            'isNeedFaceDetected' => false,
            'isFaceCollected'    => false,
            'faceImgPath'        => ''
        );
    }
}
