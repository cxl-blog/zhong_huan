<?php

namespace Custom\Service\FaceDetect;

use Topxia\Service\User\CurrentUser;

/**
 * 人脸识别用
 */
interface FaceDetectService
{
    const DEFAULT_FACE_DETECT_RATE = 75;

    const SESSION_FACE_DETECTED = 'faceDetectedSuccessfully';

    //const FACE_DETECT_URL_PREFIX = 'http://api.facecore.net/face/';

    const FACE_DETECT_URL_PREFIX = 'http://192.144.164.192/face/';

    public function isFaceImgUploaded($userId);

    public function updateUserFaceImg($userId, $base64ImgData, $publicUploadDir);

    public function updateBatchUserFaceImg($userId, $base64ImgData, $publicUploadDir);

    /*
     * 判断人脸检测识别率是否小于数据库中设置的识别率,
     * 小于或等于 则合法
     */
    public function isFaceImgValid($userId, $base64ImgData, $faceDetectResult);

    /**
     * 获取人脸识别状态
     * 返回格式为数组
     * {
     *     isNeedFaceDetected: true, //当行政区域需要支持人脸检测时,才允许人脸采集及检测
     *     isFaceCollected: true  // 人脸是否已经采集过, 只有 isNeedFaceDetected = true 时,才需要采集
     *     faceImgPath: "/files/camera/570cdcf136098.png"
     * }
     * @param $currentUser 可不填, 用于获取角色,如果不是学员,则必然不支持人脸识别
     */
    public function getFaceDetectStatus($userId, CurrentUser $currentUser);

    /**
     * 获取考试时的人脸识别状态
     * {
     *     'isNeedFaceDetected': true,  //当行政区域需要支持考试人脸检测且考试未通过时, 才允许人脸检测
     *     'maxSnapNum': 12, //最大拍摄照片
     *     'snapInterval': 12     //拍照时间间隔
     * }
     */
    public function getFaceDetectStatusForTestpaper($userId, $testpaperLessonId, $currentLoopTime);

    public function saveFaceImg($base64ImgData, $uploadFolder);
}
