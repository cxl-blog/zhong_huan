<?php

namespace Custom\Service\JobData;

interface JobDataService
{
    public function getJobDatasByType($type);

    public function addJobData($fields);

    public function deleteJobDataByType($type);
}
