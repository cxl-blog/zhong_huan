<?php
namespace Custom\Service\JobData\Impl;

use Topxia\Common\ArrayToolkit;
use Custom\Service\JobData\JobDataService;
use Topxia\Service\Common\BaseService;

class JobDataServiceImpl extends BaseService implements JobDataService
{
     public function getJobDatasByType($type)
     {
        return $this->getJobDataDao()->getJobDatasByType($type);
     }

    public function addJobData($fields)
    {
        return $this->getJobDataDao()->addJobData($fields);
    }

    public function deleteJobDataByType($type)
    {
        return $this->getJobDataDao()->deleteJobDataByType($type);
    }

    protected function getJobDataDao()
    {
        return $this->createDao('Custom:JobData.JobDataDao');
    }
}
