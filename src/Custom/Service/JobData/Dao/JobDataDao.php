<?php 
namespace Custom\Service\JobData\Dao;

interface JobDataDao
{
    public function getJobDatasByType($type);

    public function addJobData($fields);

    public function deleteJobDataByType($type);

    public function deteleAllJobData();
}