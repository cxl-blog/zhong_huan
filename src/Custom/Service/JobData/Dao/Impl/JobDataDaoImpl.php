<?php

namespace Custom\Service\JobData\Dao\Impl;

use Topxia\Service\Common\BaseDao;
use Custom\Service\JobData\Dao\JobDataDao;

class JobDataDaoImpl extends BaseDao implements JobDataDao
{
    protected $table = 'crontab_job_data';

    public function getJobData($id)
    {
        $sql = "SELECT * FROM {$this->table} WHERE id = ? LIMIT 1";
        return $this->getConnection()->fetchAssoc($sql, array($id)) ?: null;
    }

    public function getJobDatasByType($type)
    {
        $sql = "SELECT * FROM {$this->getTable()} WHERE type = ?";
        return $this->getConnection()->fetchAll($sql, array($type)) ?: array();
    }

    public function addJobData($fields)
    {
        $affected            = $this->getConnection()->insert($this->table, $fields);
        $this->clearCached();
        if ($affected <= 0) {
            throw $this->createDaoException('Insert crontab_job_data error.');
        }

        return $this->getJobData($this->getConnection()->lastInsertId());
    }

    public function deleteJobDataByType($type)
    {
        return $this->getConnection()->delete($this->table, array('type' => $type));
    }

    public function deteleAllJobData()
    {
        return $this->getConnection()->delete($this->table, array('1'  => 1));
    }
}