<?php

namespace Custom\WebBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;

class CertificationDataTag extends BaseDataTag implements DataTag
{
    /**
     * 获取证书信息
     *
     * 可传入的参数：
     *   certificationId 必需 证书ID
     *
     * @param  array $arguments 参数
     * @return array 证书
     */

    public function getData(array $arguments)
    {
        if (empty($arguments['certificationId'])) {
            throw new \InvalidArgumentException("certificationId参数缺失");
        }

        $certification = $this->getCertificationService()->getCertification($arguments['certificationId']);

        return $certification;
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
