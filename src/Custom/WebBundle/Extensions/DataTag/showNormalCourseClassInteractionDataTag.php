<?php

namespace Custom\WebBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;

class showNormalCourseClassInteractionDataTag extends BaseDataTag implements DataTag
{
    public function getData(array $arguments)
    {
        return $this->getSettingService()->isNormalClassInteractionDisplayed();
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('Custom:System.SettingService');
    }
}
