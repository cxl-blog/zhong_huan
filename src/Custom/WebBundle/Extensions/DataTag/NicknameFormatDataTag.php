<?php

namespace Custom\WebBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;
use Custom\Common\Util\StringUtils;

class NicknameFormatDataTag extends BaseDataTag implements DataTag
{
    public function getData(array $arguments)
    {
        if (empty($arguments['nickname'])) {
            return '';
        }

        return StringUtils::formatStringForSecurity($arguments['nickname']);
    }
}