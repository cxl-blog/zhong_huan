<?php

namespace Custom\WebBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;

class AreaDataTag extends BaseDataTag implements DataTag
{
    /**
     * 获取属地信息
     *
     * 可传入的参数：
     *   areaCode 必需 属地CODE
     *
     * @param  array $arguments 参数
     * @return array 属地
     */

    public function getData(array $arguments)
    {
        if (empty($arguments['areaCode'])) {
            throw new \InvalidArgumentException("areaCode参数缺失");
        }

        $area = $this->getAreaService()->getAreaByCode($arguments['areaCode']);

        return $area;
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }
}
