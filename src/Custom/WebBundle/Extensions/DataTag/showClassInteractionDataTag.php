<?php

namespace Custom\WebBundle\Extensions\DataTag;

use Topxia\WebBundle\Extensions\DataTag\DataTag;
use Topxia\WebBundle\Extensions\DataTag\BaseDataTag;

class showClassInteractionDataTag extends BaseDataTag implements DataTag
{
    public function getData(array $arguments)
    {
        if (empty($arguments['id'])) {
            throw new \InvalidArgumentException("courseId参数缺失");
        }

        $course = $this->getCourseService()->getCourse($arguments['id']);

        $area = $this->getAreaService()->getAreaByCode($course['areaCode']);

        if ($area['isClassInteraction']) {
            return true;
        }

        return false;
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }
}
