define(function(require, exports, module) {

  var Widget = require('widget');
  g_cookie = require('cookie');

  var AreaCodeSelector = Widget.extend({

    attrs: {
      idCardOrMobileSelector: null,  
           // 身份证或手机号输入框, 必填, jquery 选择器, 如 "#login_username", 必须为身份证或手机
      
      areaCodeSelector: null,  
           // 显示的行政区域下拉框, 必填
      
      areaCodeWrapper: null
           // 行政区域的容器, 包含文案和下拉框, 必填, jquery 选择器
    },

    setup: function() {
      var current = this;
      $(this.get('idCardOrMobileSelector')).blur(function(){
        current._fetchAreaCode();
      });

      current._fetchAreaCode();
    },

    _fetchAreaCode: function() {
      var idCardLength = 18;
      var mobileLength = 11;
      var current = this;

      var idCardOrMobile = $.trim($(this.get('idCardOrMobileSelector')).val());
      if (idCardOrMobile.length === idCardLength || idCardOrMobile.length === mobileLength) {
        $.post('/login/get/areacode', {username: idCardOrMobile}, function(data){
          var areaCodeSelector = $(current.get('areaCodeSelector'));
          var areaCodeWrapper = $(current.get('areaCodeWrapper'));
          areaCodeSelector.empty();
          if (data.length > 1) {
            var latestLoginAreaCode = g_cookie.get('latestLoginAreaCode');
            for (var i = 0; i < data.length; i++) {
              var selectedAttr = '';
              if (typeof latestLoginAreaCode !== 'undefined' && latestLoginAreaCode === data[i].code) {
                selectedAttr = ' selected '
              }
              areaCodeSelector.append("<option value='" + data[i].code + "'" + selectedAttr + ">" + data[i].name + "</option>");
              
            }
            areaCodeWrapper.show();
          } else {
            areaCodeWrapper.hide();
          }
        });
      }
    }

  });

  module.exports = AreaCodeSelector;

});
