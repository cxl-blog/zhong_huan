/**
 * [AjaxContext description]
 * @param  config  格式为
  {
    url:  '/camera',
    token: "token",  //后台ajax访问时,如果需要token, 则使用此属性
    cache: false,   //默认cache = false
    type: 'POST', //默认type = POST
    dataType: 'JSON',  //默认为json, 即返回类型是什么
    async: true, //默认为true
    param: {
      'a' : 'b'      //ajax传递的参数
    },
    jqueryDomExpression: jqueryDomExpression: '.table',  
        //非必填，
        // 填了后会将当前页面的$(jqueryDomExpression)节点内容替换为
        // ajax返回页面的$(jqueryDomExpression)内的内容

    success: function(data, textStatus) {  }, //成功后的信息
    error: function(XMLHttpRequest, textStatus, errorThrown) {} //后台出错时执行的方法, 默认为 _errorFunc
  }
 */
function AjaxContext(config) {

  this._config = config;

  var current = this;

  this.load= function() {
    current._initConfigIfNeed();

    var ajaxContextParams = current._generateAjaxContextParams();

    $.ajax(ajaxContextParams);
  };

  this._initConfigIfNeed = function() {
    var defaultConfigValues = {
      "type" : "POST",
      "cache" : false,
      "dataType": "JSON",
      "error": current._errorFunc
    };

    for (var key in defaultConfigValues) {
      if (typeof current._config[key] == "undefined") {
        current._config[key] = defaultConfigValues[key];
      } 
    }
  };

  this._generateAjaxContextParams = function() {
    var ajaxContextParams = {
      cache: current._config.cache,
      success: function(data, textStatus) {
        current._replaceHtml(data);
        if (typeof current._config.success != "undefined") {
          current._config.success(data, textStatus);
        }
      },
      complete: current._completeFunc,
      error: current._errorFunc,
      type: current._config.type,
      dataType: current._config.dataType,
      url: current._config.url
    };

    if (typeof current._config.async != "undefined") {
      ajaxContextParams.async = current._config.async;
    }

    if (typeof current._config.token != "undefined") {
      ajaxContextParams.headers = {
        "X-CSRF-Token": current._config.token
      };
    }

    if (typeof current._config.param != "undefined") {
      ajaxContextParams.data = current._config.param;
    }

    return ajaxContextParams;
  };

  this._completeFunc = function(XMLHttpRequest, textStatus) {
    
  };

  this._errorFunc = function(XMLHttpRequest, textStatus, errorThrown) {
    try {
      var jsonData = $.parseJSON(XMLHttpRequest.responseText);
      alert(jsonData.error.message);
    } catch(e) {}
  };

  this._replaceHtml = function(data) {
    if (typeof current._config.jqueryDomExpression != "undefined") {
      $(current._config.jqueryDomExpression).html(
        $(data).find(current._config.jqueryDomExpression).html()
      );
    }
  };

}