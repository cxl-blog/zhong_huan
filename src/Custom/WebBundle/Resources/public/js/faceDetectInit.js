define(function(require, exports, module) {
    require("customwebbundle/webCamera");
    g_cookie = require('cookie');
    $('#face-detect-modal').modal('show');
    
    var webCamera = new WebCamera(
        {
            tokenDomId: "tokenDomId",
            uploadUrl: "/camera/faceDetect"
        }
    );
    webCamera.init();

    $(".cameraSnapBtn").click(webCamera.snapAndUpload);
    $(".cameraReSnapBtn").click(webCamera.reSnap);
});