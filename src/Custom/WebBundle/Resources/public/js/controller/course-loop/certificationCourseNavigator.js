function saveCurrentLoopTimeBeforeNavigate(url, userId, courseId, currentLoopTime) {
    var cookieKey = userId + "_" + courseId + "_currentLoopTime";
    g_cookie.set(cookieKey, currentLoopTime, {path: "/", expires: 10});
    location.href = url;
}