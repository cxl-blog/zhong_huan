define(function(require, exports, module) {
  var Notify = require('common/bootstrap-notify');
  require('customwebbundle/ajax');

  exports.run = function() {
    window.onload = function() {
      $('.selectedType').change(
        function() {
          var sendMsg = $('.' + $('.selectedType').val()).html();
          $('.sendMsg').val(sendMsg);
          $('.doc').val($('.' + $('.selectedType').val() + '_doc').html());
        }
      );

      $('.selectedType').change();
    }

    $('.sendBtn').click(
      function() {
        $('.result').html('获取信息中...');
        new AjaxContext(
          {
            'url': '/data_sync_test/sync/' + $(".selectedType").val(),
            'param': {'data': $('.sendMsg').val()},
            'success': function(data) {
              $('.result').html(JSON.stringify(data));
            }
          }
        ).load();
      }
    );
  }
});