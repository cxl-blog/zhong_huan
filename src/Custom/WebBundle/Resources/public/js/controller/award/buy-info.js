define(function(require, exports, module) {
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');

  var params;
  var order;
  var $form = $('#award-form');
  var allowPay = true;
  var openid;

  exports.run = function() {

    $("#idcard").blur(function(){
      var result = true;
      var verifiedFields = ["#idcard"];
      for (var i = 0; i < verifiedFields.length; i++) {
        result &= $(verifiedFields[i]).parents(".has-error").length == 0;
        result &= $(verifiedFields[i]).val() != "";
      }

      if (!result) {
        $("#idcard:focus").css({'border':'3px solid red'});
        return;
      }
      if (result) {
        $("input").css({'border':'none'});
      }
      var idcard = $.trim($("#idcard").val());
      $.post('/login/get/areacode', {username: idcard}, function(data){
        var select_areacode = $("#areacode");
        var areacode_div = $(".areacode-select");
        select_areacode.empty();
        if (data.length > 1) {
           for (var i = 0, length = data.length; i < length; i++) {
             select_areacode.append("<option value='" + data[i].code + "'>" + data[i].name + "</option>"); 
           }
           areacode_div.show();
         } else {
           areacode_div.hide();
        }
      });
    });

    //没有关注微信公众号的情况下提示关注公众号
    openid = $("#openid").val();
    
    function showAttention() {
      if (openid === '') {
        alert('缺少微信支付必要的参数，请联系管理员');
        return false;
      }
      return true;
    }

    showAttention();
    
    var validator = new Validator({
      element: $form,
      autoSubmit: false,
      onFormValidated: function(error, results, $form) {
        if (error) {        
          $("input:focus").css({'border':'3px solid red'});
          return ;
        }

        if (results) {
          $("input").css({'border':'none'});
        }

        if (!showAttention()) {
          return;
        }

        if (!allowPay) {
          return false;
        }

        allowPay = false;
        
        sendPost();
      }
    });

    validator.addItem({
      element: '[name="idcard"]',
      required: true,
      rule: 'idcard'
    });

    validator.addItem({
      element: '[name="mobile"]',
      required: true,
      rule: 'mobile'
    });
    
    validator.addItem({
      element: '[name="amount"]',
      required: true,
      rule: 'currency'
    });
  };

  function jsApiCall(){
    WeixinJSBridge.invoke(
      'getBrandWCPayRequest',
      {
        'appId': params.appId,
        'timeStamp': params.timeStamp.toString(),
        'nonceStr': params.nonceStr,
        'package': params.package,
        'signType': params.signType,
        'paySign': params.paySign
      },
      function(res){
        if (res.err_msg === "get_brand_wcpay_request:ok") {
          //alert('支付成功');
          window.location.href = '/weixinpay/buy_success/' + order.id + '/' + openid;
        } else {
          allowPay = true;
        } 
      }
    );
  }

  function callpay()
  {
    if (typeof WeixinJSBridge === "undefined") {
      if (document.addEventListener) {
        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
      } else if (document.attachEvent) {
        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
      }
    } else {
      jsApiCall();
    }
  }
  
  function sendPost()
  {
    $.post($form.attr('action'), $form.serialize(), function(data) {
      if (!data.success) {
        alert(data.message);
        allowPay = true;
        return;
      }
      params = data.params;
      order = data.order;

      callpay();
    }).error(function() {
      allowPay = true;
    });  
  }

});