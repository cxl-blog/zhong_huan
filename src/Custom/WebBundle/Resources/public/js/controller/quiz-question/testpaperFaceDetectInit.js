define(function(require, exports, module) {
    require("customwebbundle/webCamera");
    $('#camera-control-modal').modal({'show': true, 'backdrop': 'static'});;
    
    var webCamera = new WebCamera(
        {
            tokenDomId: "tokenDomId",
            uploadUrl: "/camera/faceDetect?testpaperLessonId="+$("#testpaperLessonId").html()
        }
    );
    webCamera.init();
    $(".cameraBtn").unbind("click");
    $(".cameraBtn").click(webCamera.snapAndUpload);
    $(".resetBtn").click(webCamera.reSnap);

    $("#delayToSnapAgain").click(
        function() {
            var currentSnapNum = parseInt($("#currentSnapNum").html(), 10);
            var maxSnapNum = parseInt($("#maxSnapNum").html(), 10);
            var snapInterval = parseInt($("#snapInterval").html(), 10) * 1000;  //转化为毫秒
            if (currentSnapNum < maxSnapNum) {
                setTimeout(
                    function() {
                        webCamera.snapAndUpload();
                        $("#currentSnapNum").html(currentSnapNum + 1);
                    }, 
                    snapInterval
                );
            }
        }
    );
});