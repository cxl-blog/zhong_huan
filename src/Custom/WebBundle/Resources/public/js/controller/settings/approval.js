define(function(require, exports, module) {
    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    var WebUploader = require('edusoho.webuploader');
    var Notify = require('common/bootstrap-notify');
    var Uploader = require('upload');


    exports.run = function() {


        var $form = $("#approval-form");


        var uploader = new WebUploader({
            element: '#faceImg'
        });
        uploader.on('uploadSuccess', function(file, response ) {
            var gotoUrl = $("#faceImg").data("goto-url");
            $.post(gotoUrl, response ,function(data){
                $("#faceImg-container").html('<img src="' + data.url + '" width="250px" height="150px">');
                $form.find('[name=faceImg]').val(data.path);

            });
        });

        var uploader = new WebUploader({
            element: '#backImg'
        });
        uploader.on('uploadSuccess', function(file, response ) {
            var gotoUrl = $("#backImg").data("goto-url");
            $.post(gotoUrl, response ,function(data){
                $("#backImg-container").html('<img src="' + data.url + '" width="250px" height="150px">');
                $form.find('[name=backImg]').val(data.path);

            });
        });

        var uploader = new WebUploader({
            element: '#jobsSeniorityCardImg'
        });
        uploader.on('uploadSuccess', function(file, response ) {
            var gotoUrl = $("#jobsSeniorityCardImg").data("goto-url");
            $.post(gotoUrl, response ,function(data){
                $("#jobsSeniorityCardImg-container").html('<img src="' + data.url + '" width="250px" height="150px">');
                $form.find('[name=jobsSeniorityCardImg]').val(data.path);

            });
        });

        var uploader = new WebUploader({
            element: '#driverLicenseImg'
        });
        uploader.on('uploadSuccess', function(file, response ) {
            var gotoUrl = $("#driverLicenseImg").data("goto-url");
            $.post(gotoUrl, response ,function(data){
                $("#driverLicenseImg-container").html('<img src="' + data.url + '" width="250px" height="150px">');
                $form.find('[name=driverLicenseImg]').val(data.path);

            });
        });

        var uploader = new WebUploader({
            element: '#otherImg'
        });
        uploader.on('uploadSuccess', function(file, response ) {
            var gotoUrl = $("#otherImg").data("goto-url");
            $.post(gotoUrl, response ,function(data){
                $("#otherImg-container").html('<img src="' + data.url + '" width="250px" height="150px">');
                $form.find('[name=otherImg]').val(data.path);

            });
        });

        var validator = new Validator({
            element: '#approval-form'
        });

        validator.addItem({
            element: '[name="faceImg"]',
            required: true
        });

        validator.addItem({
            element: '[name="backImg"]',
            required: true
        });

    };

});