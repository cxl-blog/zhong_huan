define(function(require, exports, module) {
    require('topxiawebbundle/controller/course/common');

    exports.run = function() {

        $("#favorite-btn").off('click').on('click', function() {
            var $btn = $(this);
            $.post($btn.data('url'), function() {
                $btn.hide();
                $("#unfavorite-btn").show();
            });
        });

        $("#unfavorite-btn").off('click').on('click', function() {
            var $btn = $(this);
            $.post($btn.data('url'), function() {
                $btn.hide();
                $("#favorite-btn").show();
            });
        });

        $(".lesson-item").on('click', function() {
            var status = $("input[name='order_status']").val();
            if(status == "refunding"){
                //alert("您已申请退款,要继续学习该课程请取消退款申请!");
                window.location.reload();
            }
        });
    };

});