define(function(require, exports, module) {
    require("../../webCamera");
    require("customwebbundle/ajax");

    var webCamera = new WebCamera(
        {
            tokenDomId: "tokenDomId",
            uploadUrl: "/camera/faceDetect?lessonViewId="+window.lessonViewId
        }
    );
    webCamera.init();
    $(".cameraBtn").unbind("click");
    $(".cameraBtn").click(webCamera.snapAndUpload);
    $(".resetBtn").click(webCamera.reSnap);
});