define(function(require, exports, module) {
    var Widget = require('widget'),
        Class = require('class'),
        Store = require('store'),
        Backbone = require('backbone'),
        swfobject = require('swfobject'),
        Scrollbar = require('jquery.perfect-scrollbar'),
        Notify = require('common/bootstrap-notify');
    g_cookie = require('cookie');

    chapterAnimate = require('topxiawebbundle/controller/course/widget/chapter-animate');
    var Messenger = require('../player/messenger');

    require('mediaelementplayer');

    var Toolbar = require('customwebbundle/controller/lesson/lesson-toolbar');

    var SlidePlayer = require('topxiawebbundle/controller/widget/slider-player');
    var DocumentPlayer = require('topxiawebbundle/controller/widget/document-player');

    var iID = null;

    var LessonDashboard = Widget.extend({

        _router: null,

        _toolbar: null,

        _lessons: [],

        _counter: null,

        events: {
            'click [data-role=next-lesson]': 'onNextLesson',
            'click [data-role=prev-lesson]': 'onPrevLesson',
            'click [data-role=finish-lesson]': 'onFinishLesson',
            'click [data-role=ask-question]': 'onAskQuestion',
        },

        attrs: {
            courseId: null,
            courseUri: null,
            dashboardUri: null,
            lessonId: null,
            type: null,
            watchLimit: false,
            starttime: null
        },

        setup: function() {

            this._readAttrsFromData();
            this._initToolbar();
            this._initRouter();
            this._initListeners();
            this._initChapter();

            $('.prev-lesson-btn, .next-lesson-btn').tooltip();
        },

        onNextLesson: function(e) {
            var next = this._getNextLessonId();
            if (next > 0) {
                rememberPlayTimeWhenNextPrevLesson(next);
            }
        },

        onPrevLesson: function(e) {
            var prev = this._getPrevLessonId();
            if (prev > 0) {
                rememberPlayTimeWhenNextPrevLesson(prev);
            }
        },

        onFinishLesson: function(e) {
            var $btn = this.element.find('[data-role=finish-lesson]');
            if (!$btn.hasClass('btn-success')) {
                this._onFinishLearnLesson();
            }
        },

        _startLesson: function() {
            var toolbar = this._toolbar,
                self = this;
            var url = '/course/' + this.get('courseId') + '/lesson/' + this.get('lessonId') + '/learn/start';
                
            $.post(url, function(result) {
                
                if(result.lessonView != '' && parseInt(result.lessonView.id) > 0)
                {  
                    self.attrs.lessonViewId.value = result.lessonView.id;
                }
                if (result.status == true) {
                    toolbar.trigger('learnStatusChange', {
                        lessonId: self.get('lessonId'),
                        status: 'learning'
                    });
                }
                
            }, 'json');
        },

        _onFinishLearnLesson: function() {
            var $btn = this.element.find('[data-role=finish-lesson]'),
                toolbar = this._toolbar,
                self = this;

            var url = '/course/' + this.get('courseId') + '/lesson/' + this.get('lessonId') + '/learn/finish';

            $.post(url, function(response) {

                if (response.isLearned) {
                    $('#course-learned-modal').modal('show');
                    _updateModelZindex($('#course-learned-modal'));
                }

                if (!response.canFinish && response.html) {
                    $("#modal").html(response.html);
                    $("#modal").modal('show');
                    return false;
                } else if (response.canFinish && response.html != '') {
                    $("#modal").html(response.html);
                    $("#modal").modal('show');
                }

                if(response.nextLessonId)
                {
                    var url = '/course/certification/' + self.get('courseId') + '/learn#lesson/' + response.nextLessonId;
                    $('#course-item-list').find('.lesson-item-' + response.nextLessonId + " > a").attr('data-url',url).removeAttr('disabled');
                    changeIcon(self.get('lessonId'),'done');
                }
                
                $('[data-role=next-lesson]').show();

                $btn.addClass('btn-success');
                $btn.attr('disabled', true);
                $btn.find('.glyphicon').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                toolbar.trigger('learnStatusChange', {
                    lessonId: self.get('lessonId'),
                    status: 'finished'
                });



            }, 'json');

        },

        _onCancelLearnLesson: function() {
            var $btn = this.element.find('[data-role=finish-lesson]'),
                toolbar = this._toolbar,
                self = this;
            var url = '../../course/' + this.get('courseId') + '/lesson/' + this.get('lessonId') + '/learn/cancel';
            $.post(url, function(json) {
                $btn.removeClass('btn-success');
                $btn.find('.glyphicon').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                toolbar.trigger('learnStatusChange', {
                    lessonId: self.get('lessonId'),
                    status: 'learning'
                });
            }, 'json');
        },

        _readAttrsFromData: function() {
            this.set('courseId', this.element.data('courseId'));
            this.set('courseUri', this.element.data('courseUri'));
            this.set('dashboardUri', this.element.data('dashboardUri'));
            this.set('watchLimit', this.element.data('watchLimit'));
            this.set('starttime', this.element.data('starttime'));
        },

        _initToolbar: function() {
            this._toolbar = new Toolbar({
                element: '#lesson-dashboard-toolbar',
                activePlugins: app.arguments.plugins,
                courseId: this.get('courseId')
            }).render();

            $('#lesson-toolbar-primary li[data-plugin=lesson]').trigger('click');
        },

        _initRouter: function() {
            var that = this,
                DashboardRouter = Backbone.Router.extend({
                    routes: {
                        "lesson/:id": "lessonShow"
                    },

                    lessonShow: function(id) {
                        that.set('lessonId', id);
                    }
                });

            this._router = new DashboardRouter();
            Backbone.history.start({
                pushState: false,
                root: this.get('dashboardUri')
            });
        },

        _initListeners: function() {
            var that = this;
            this._toolbar.on('lessons_ready', function(lessons) {
                that._lessons = lessons;
                that._showOrHideNavBtn();

                if ($('.es-wrap [data-toggle="tooltip"]').length > 0) {
                    $('.es-wrap [data-toggle="tooltip"]').tooltip({
                        container: 'body'
                    });
                }
            });
        },

        _afterLoadLesson: function(lessonId) {
            if (this._counter && this._counter.timerId) {
                clearInterval(this._counter.timerId);
            }

            var self = this;
            this._counter = new Counter(self, this.get('courseId'), lessonId, this.get('watchLimit'));
            this._counter.setTimerId(setInterval(function() {
                self._counter.execute();
                if (!self._isRightMenuBarFocused()) {
                    $(".back-course-btn").focus();
                }
            }, 1000));
        },

        /*
         * 点中右边的栏目时, 不重置焦点, 防止无法提交问答, 笔记, 和其他数据
         */
        _isRightMenuBarFocused: function() {
            var isFocused = false;
            if (typeof document.activeElement != 'undefined' 
                    && typeof document.activeElement.className != 'undefined') {
                var className = document.activeElement.className;
                var checkedClasses = ['cke', 'form-control'];
                for (var i = 0; i < checkedClasses.length; i++) {
                    if (className.indexOf(checkedClasses[i]) != -1) {
                        isFocused = true;
                        break;
                    }
                }
            }
            return isFocused ; 
        },

        _onChangeLessonId: function(id) {
            
            clearVedioFinished();
            var self = this;
            if (!this._toolbar) {
                return;
            }
            this._toolbar.set('lessonId', id);

            swfobject.removeSWF('lesson-swf-player');

            $('#lesson-iframe-content').empty();
            $('#lesson-video-content').html("");

            this.element.find('[data-role=lesson-content]').hide();
            var that = this;
            var _readCourseTitle = function(lesson, name) { // chapter unit
                var data = {};
                if (app.arguments.customChapter == 1) {
                    data.number = that.element.find('[data-role=' + name + '-number]');
                    data.title = name == 'chapter' ? lesson.chapterNumber : lesson.unitNumber;
                } else {
                    data.number = that.element.find('[data-role=custom-' + name + '-number]');
                    data.title = name == 'chapter' ? (lesson.chapter == null ? "" : lesson.chapter.title) : (lesson.unit == null ? "" : lesson.unit.title);
                }
                return data;
            }
            $.get(this.get('courseUri') + '/lesson/' + id, function(lesson) {               

                if( (that.attrs.courseType.value == 'certification' && lesson.type == 'video') ||  lesson.mediaError)
                {
                    $('[data-role=finish-lesson]').parent().hide();                
                }
                else
                {
                    $('[data-role=finish-lesson]').parent().show();
                }                

                that.set('type', lesson.type);
                that.element.find('[data-role=lesson-title]').html(lesson.title);
                $(".watermarkEmbedded").html('<input type="hidden" id="videoWatermarkEmbedded" value="' + lesson.videoWatermarkEmbedded + '" />');
                var $titleStr = "";
                $titleArray = document.title.split(' - ');
                $.each($titleArray, function(key, val) {
                    $titleStr += val + ' - ';
                })
                document.title = lesson.title + ' - ' + $titleStr.substr(0, $titleStr.length - 3);
                if (app.arguments.customChapter == 1) {
                    that.element.find('[data-role=lesson-number]').html(lesson.number);
                }

                if (parseInt(lesson.chapterNumber) > 0) {
                    var data = _readCourseTitle(lesson, 'chapter');
                    data.number.html(data.title).parent().show().next().show();
                } else {
                    var data = _readCourseTitle(lesson, 'chapter');
                    data.number.parent().hide().next().hide();
                }

                if (parseInt(lesson.unitNumber) > 0) {
                    var data = _readCourseTitle(lesson, 'unit');
                    data.number.html(data.title).parent().show().next().show();
                } else {
                    var data = _readCourseTitle(lesson, 'unit');
                    data.number.parent().hide().next().hide();
                }

                if ((lesson.status != 'published') && !/preview=1/.test(window.location.href)) {
                    $("#lesson-unpublished-content").show();
                    return;
                }

                var number = lesson.number - 1;

                if (typeof lesson.isExceeding !== undefined && lesson.isExceeding) {
                    warmAsTimeExceed(lesson.maxLearningMinutes, that);
                }

                if (lesson.canLearn.status != 'yes') {
                    $("#lesson-alert-content .lesson-content-text-body").html(lesson.canLearn.message);
                    $("#lesson-alert-content").show();
                    return;
                }

                if (lesson.mediaError) {
                    Notify.danger(lesson.mediaError);
                    return;
                }

                if (lesson.mediaSource == 'iframe') {
                    var html = '<iframe src="' + lesson.mediaUri + '" style="position:absolute; left:0; top:0; height:100%; width:100%; border:0px;" scrolling="no"></iframe>';

                    $("#lesson-iframe-content").html(html);
                    $("#lesson-iframe-content").show();

                } else if (lesson.type == 'video' || lesson.type == 'audio') {      

                    if (lesson.mediaSource == 'self') {
                        var lessonVideoDiv = $('#lesson-video-content');

                        if ((lesson.mediaConvertStatus == 'waiting') || (lesson.mediaConvertStatus == 'doing')) {
                            Notify.warning('视频文件正在转换中，稍后完成后即可查看');
                            return;
                        }

                        var playerUrl = '/course/' + lesson.courseId + '/lesson/' + lesson.id + '/player';
                        if (self.get('starttime')) {
                            playerUrl += "?starttime=" + self.get('starttime');
                        }
                        var html = '<iframe src=\'' + playerUrl + '\' name=\'viewerIframe\' id=\'viewerIframe\' width=\'100%\'allowfullscreen webkitallowfullscreen height=\'100%\' style=\'border:0px\'></iframe>';

                        $("#lesson-video-content").show();
                        $("#lesson-video-content").html(html);

                        var messenger = new Messenger({
                            name: 'parent',
                            project: 'PlayerProject',
                            children: [document.getElementById('viewerIframe')],
                            type: 'parent'
                        });
                        g_cookie.set('watching_lessonId',lesson.id,{path: "/", expires: 1080});  //1080天后过期

                        messenger.on("ready", function() {
                            var viewer = window.frames["viewerIframe"].window.BalloonPlayer;
                            var markersUrl = '/course/certification/lesson/'+lesson.id+'/marker/show';
                            g_isDetecting = false;                           
                            
                            var status   = that.attrs.lessonlearnStatus.value;
                            if(status!='finished' && typeof g_alreadyLearned == "undefined"){
                                $.ajax({type:"get",url:markersUrl, dataType:"json",
                                    success:function(data){
                                        g_currentMarkers  = {};            
                                        var markers = new Array();
                                        for (var index in data) {
                                            var marker = {
                                                "id": data[index].id,
                                                "time": (parseInt(data[index].second) + data[index].videoHeaderTime),
                                                "text": "ads",
                                                "finished": data[index].finished,
                                                "isFaceMarker": data[index].isFaceMarker
                                            };

                                            if (marker.isFaceMarker) {
                                                marker.finished = true;
                                            }
                                            markers.push(marker);
                                            g_currentMarkers[marker.id] = marker;
                                        }

                                        if (markers.length != 0) {
                                            if ($(".previewQuestion").length != 0 ) {
                                                var questionMarkers = new Array();
                                                for (var i = 0; i < markers.length; i++) {
                                                    markers[i].finished = false;
                                                    if (!markers[i].isFaceMarker) {
                                                        questionMarkers.push(markers[i]);
                                                    } 
                                                }
                                                markers = questionMarkers;
                                            }

                                            _dealTimeDuplicatedMarkers(markers)
                                            viewer.setMarkers(markers);
                                        }                                        
                                    }
                                });

                                viewer.offKeyDown();
                                viewer.toggleProgress('hide'); 
                                addDocumentListener();      

                                //切换页面停止播放
                                listenPageVisible(viewer,lesson.id); 

                                listenWindowTab();                                 
                            }
                            listenPageClose(lesson.id,that);
                        });

                        messenger.on("ended", function() {
                            g_alreadyLearned = true;
                            var player = that.get("player");
                            player.playing = false;
                            that.set("player", player);
                            that._onFinishLearnLesson();  
                            $("[data-role='finish-lesson']").parent().show();
                            //var viewer = window.frames["viewerIframe"].window.BalloonPlayer;
                            //viewer.toggleProgress('show');

                            var next = that._getNextLessonId();
                            if (next > 0) {
                                $("#next-lesson-control-modal").modal('show');
                                _updateModelZindex($("#next-lesson-control-modal"));
                                $("#next-lesson-control-modal").on('click','#yesBtn',function(){
                                    var player = window.frames['viewerIframe'].window.BalloonPlayer; 
                                    currentTime = player.getCurrentTime(); 
                                    window.location.href = window.location.href.split("#lesson")[0] + "#lesson/" + next; 
                                    window.location.reload();
                                });
                                $("#next-lesson-control-modal").on('click',"[data-dismiss='modal']",function(){
                                   window.location.reload();
                                });                              
                            }                          
                        });

                        messenger.on("playing", function() {
                            var player = that.get("player");
                            player.playing = true;
                            that.set("player", player);  
                        });

                        messenger.on("paused", function() {
                            var player = that.get("player");
                            player.playing = false;
                            that.set("player", player);

                        });

                        messenger.on("onMarkerReached", function(marker){
                            var player = window.frames["viewerIframe"].window.BalloonPlayer;

                            //如果上一个人脸识别还未结束, 暂停播放
                            if (typeof g_isDetecting != "undefined" && g_isDetecting) {
                                player.pause();
                                return;
                            }

                            if (typeof previousMarkerId != "undefined" && previousMarkerId == marker.markerId) {
                                player.finishMarker(window.markerId, true, false);
                                return;
                            }

                            if (g_currentMarkers[marker.markerId].isFaceMarker) {
                                g_isDetecting = true;
                            }

                            var status = that.attrs.lessonlearnStatus.value;
                            if(status!='finished' && typeof g_alreadyLearned == "undefined"){                           
                                var faceable = $("#lesson-dashboard").data('face-able');
                                previousMarkerId = marker.markerId;        
                                if(g_currentMarkers[marker.markerId].isFaceMarker){
                                    window.markerId = marker.markerId;
                                    window.lessonViewId = that.attrs.lessonViewId.value;

                                    if (faceable == 0) {
                                        markAsPassed(window.markerId);
                                        player.finishMarker(window.markerId, true, false);
                                        g_isDetecting = false;
                                        return;
                                    } 
                                    if (g_isFirstDetecting) {
                                        require('./cameraInit');
                                        $("#camera-control-modal").modal({'show': true, 'backdrop': 'static'});
                                        _updateModelZindex($("#camera-control-modal"));
                                        if(player.isPlaying()){
                                            player.pause();
                                        }
                                        g_isFirstDetecting = false;
                                    } else {
                                        $(".cameraBtn").click();
                                    }     
                                }
                                
                                if(!g_currentMarkers[marker.markerId].isFaceMarker && marker.questionId!=0){   
                                    player.finishMarker(marker.markerId,true);
                                    if(player.isPlaying()){
                                        player.pause();
                                    }
                                                                     
                                    $.get('/course/lesson/' + marker.markerId + '/questionmarker/show',{"questionId":marker.questionId,"lessonId":lesson.id},function(data) {
                                        var $modal = $("#modal");
                                        if (data == "") {
                                            $modal.hide();
                                            if(!player.isPlaying()){
                                                player.play();
                                            }                                            
                                        } else {
                                            $modal.html(data);
                                            var $player = $(document.getElementById('viewerIframe').contentDocument);
                                            //判断是否全屏
                                            if ($player.width() == $('body').width()) {
                                                $modal.css('z-index','2147483647');
                                            } else {
                                                var $modaldialog = $modal.find('.modal-dialog');
                                                $modaldialog.css('margin-left', ($('body').width() - $('.toolbar').width() - $modaldialog.width()) / 2);
                                            }
                                            $modal.show();
                                        }
                                    });
                                }
                            }                           
                        });
                        that.set("player", {});
                    } else {
                        $("#lesson-swf-content").html('<div id="lesson-swf-player"></div>');
                        swfobject.embedSWF(lesson.mediaUri,
                            'lesson-swf-player', '100%', '100%', "9.0.0", null, null, {
                                wmode: 'opaque',
                                allowFullScreen: 'true'
                            });
                        $("#lesson-swf-content").show();
                    }
                } else if (lesson.type == 'text') {
                    $("#lesson-text-content").find('.lesson-content-text-body').html(lesson.content);
                    $("#lesson-text-content").show();
                    $("#lesson-text-content").perfectScrollbar({
                        wheelSpeed: 50
                    });
                    $("#lesson-text-content").scrollTop(0);
                    $("#lesson-text-content").perfectScrollbar('update');

                } else if (lesson.type == "live") {
                    var liveStartTimeFormat = lesson.startTimeFormat;
                    var liveEndTimeFormat = lesson.endTimeFormat;
                    var startTime = lesson.startTime;
                    var endTime = lesson.endTime;

                    var courseId = lesson.courseId;
                    var lessonId = lesson.id;
                    var $liveNotice = "<p>直播将于 <strong>" + liveStartTimeFormat + "</strong> 开始，于 <strong>" + liveEndTimeFormat + "</strong> 结束，请在课前10分钟内提早进入。</p>";
                    if (iID) {
                        clearInterval(iID);
                    }

                    var intervalSecond = 0;

                    function generateHtml() {
                        var nowDate = lesson.nowDate + intervalSecond;
                        var startLeftSeconds = parseInt(startTime - nowDate);
                        var endLeftSeconds = parseInt(endTime - nowDate);
                        var days = Math.floor(startLeftSeconds / (60 * 60 * 24));
                        var modulo = startLeftSeconds % (60 * 60 * 24);
                        var hours = Math.floor(modulo / (60 * 60));
                        modulo = modulo % (60 * 60);
                        var minutes = Math.floor(modulo / 60);
                        var seconds = modulo % 60;
                        var $replayGuid = "老师们：";
                        $replayGuid += "<br>";

                        if (lesson.liveProvider == 1) {
                            $replayGuid += "&nbsp;&nbsp;&nbsp;&nbsp;录制直播课程时，需在直播课程间点击“";
                            $replayGuid += "<span style='color:red'>录制面板</span>";
                            $replayGuid += "”，录制完成后点击“";
                            $replayGuid += "<span style='color:red'>暂停</span>”";
                            $replayGuid += "结束录播，录播结束后在“";
                            $replayGuid += "<span style='color:red'>录播管理</span>";
                            $replayGuid += "”界面生成回放。";
                            $replayGuid += "<br>";
                        } else {
                            $replayGuid += "&nbsp;&nbsp;&nbsp;&nbsp;";
                            $replayGuid += "直播结束后，在课时管理的“";
                            $replayGuid += "<span style='color:red'>录播管理</span>";
                            $replayGuid += "”点击生成回放。";
                            $replayGuid += "<br>";
                        }

                        $countDown = that._getCountDown(days, hours, minutes, seconds);


                        if (0 < startLeftSeconds && startLeftSeconds < 7200) {
                            $liveNotice = "<p>直播将于 <strong>" + liveStartTimeFormat + "</strong> 开始，于 <strong>" + liveEndTimeFormat + "</strong> 结束，请在课前10分钟内提早进入。</p>";
                            var url = self.get('courseUri') + '/lesson/' + id + '/live_entry';
                            if (lesson.isTeacher) {
                                $countDown = $replayGuid;
                                $countDown += "<p>还剩" + hours + "小时" + minutes + "分钟" + seconds + "秒&nbsp;<a class='btn btn-primary' href='" + url + "' target='_blank'>进入直播教室</a><br><br></p>";
                            } else {
                                $countDown = "<p>还剩" + hours + "小时" + minutes + "分钟" + seconds + "秒&nbsp;<a class='btn btn-primary' href='" + url + "' target='_blank'>进入直播教室</a><br><br></p>";
                            }
                        };

                        if (startLeftSeconds <= 0) {
                            clearInterval(iID);
                            $liveNotice = "<p>直播已经开始，直播将于 <strong>" + liveEndTimeFormat + "</strong> 结束。</p>";
                            var url = self.get('courseUri') + '/lesson/' + id + '/live_entry';
                            if (lesson.isTeacher) {
                                $countDown = $replayGuid;
                                $countDown += "<p><a class='btn btn-primary' href='" + url + "' target='_blank'>进入直播教室</a><br><br></p>";
                            } else {
                                $countDown = "<p><a class='btn btn-primary' href='" + url + "' target='_blank'>进入直播教室</a><br><br></p>";
                            }
                        };

                        if (endLeftSeconds <= 0) {
                            $liveNotice = "<p>直播已经结束</p>";
                            $countDown = "";
                            if (lesson.replays && lesson.replays.length > 0) {
                                $.each(lesson.replays, function(i, n) {
                                    $countDown += "<a class='btn btn-primary' href='" + n.url + "' target='_blank'>" + n.title + "</a>&nbsp;&nbsp;";
                                });
                            }
                        };

                        $("#lesson-live-content").find('.lesson-content-text-body').html($liveNotice + '<div style="padding-bottom:15px; border-bottom:1px dashed #ccc;">' + lesson.summary + '</div>' + '<br>' + $countDown);

                        intervalSecond++;
                    }

                    generateHtml();
                    iID = setInterval(generateHtml, 1000);

                    $("#lesson-live-content").show();
                    $("#lesson-live-content").perfectScrollbar({
                        wheelSpeed: 50
                    });
                    $("#lesson-live-content").scrollTop(0);
                    $("#lesson-live-content").perfectScrollbar('update');

                } else if (lesson.type == 'testpaper') {
                    var url = '/lesson/' + id + '/test/' + lesson.mediaId + '/do';
                    var html = '<span class="text-info">请点击「开始考试」按钮，在新开窗口中完成考试。<a href="' + url + '" class="btn btn-primary btn-sm" target="_blank">开始考试</a></span>';
                    var html = '<span class="text-info">正在载入，请稍等...</span>';
                    $("#lesson-testpaper-content").find('.lesson-content-text-body').html(html);
                    $("#lesson-testpaper-content").show();

                    //类型是实时考试
                    if (lesson.testMode == 'realTime') {
                        var testStartTimeFormat = lesson.testStartTimeFormat;
                        var testEndTimeFormat = lesson.testEndTimeFormat;
                        var testStartTime = lesson.testStartTime;
                        var testEndTime = lesson.testEndTime;
                        var limitedTime = lesson.limitedTime;

                        var courseId = lesson.courseId;
                        var lessonId = lesson.id;
                        var $testNotice = "<p>实时考试将于 <strong>" + testStartTimeFormat + "</strong> 开始，将于<strong>" + testEndTimeFormat + "</strong> 结束，请在课前10分钟内提早进入。</p>";
                        if (iID) {
                            clearInterval(iID);
                        }

                        var intervalSecond = 0;

                        function generateTestHtml() {
                            var nowDate = lesson.nowDate + intervalSecond;
                            var testStartLeftSeconds = parseInt(testStartTime - nowDate);
                            var testEndLeftSeconds = parseInt(testEndTime - nowDate);
                            var testStartRightSeconds = parseInt(nowDate - testStartTime);
                            var days = Math.floor(testStartLeftSeconds / (60 * 60 * 24));
                            var modulo = testStartLeftSeconds % (60 * 60 * 24);
                            var hours = Math.floor(modulo / (60 * 60));
                            modulo = modulo % (60 * 60);
                            var minutes = Math.floor(modulo / 60);
                            var seconds = modulo % 60;
                            var limitedHouse = Math.floor(testEndLeftSeconds / (60 * 60));
                            var limitedMinutes = Math.floor(testEndLeftSeconds % (60 * 60) / 60);
                            var limitedSeconds = (testEndLeftSeconds % 60)

                            if (0 < testStartLeftSeconds) {
                                $testNotice = '<p class="text-center mtl mbl"><i class="text-primary mrm es-icon es-icon-info"></i>欢迎参加考试，本考试离开考还有<span class="gray-darker plm">' + days + '天</span></p><p class="text-center text-primary mbl"><span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#46c37b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + days + '</span>天<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#46c37b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + hours + '</span>时<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#46c37b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + minutes + '</span>分<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#46c37b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + seconds + '</span>秒</p>   <p class="text-center color-gray">考试开始后，请点击<a class="mlm mrm btn btn-sm btn-default" disabled>开始考试</a>进入</p>';
                            };

                            if (0 < testStartRightSeconds) {
                                $testNotice = '<p class="text-center mtm mbm"><i class="color-warning mrm es-icon es-icon-info" ></i>考试剩余时间</p><p class="text-center text-primary mbl"><span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#ffcb4b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">0</span>天<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#ffcb4b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + limitedHouse + '</span>时<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#ffcb4b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + limitedMinutes + '</span>分<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#ffcb4b;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">' + limitedSeconds + '</span>秒</p>   <p class="text-center color-gray">请点击<a href="' + url + '" class="mlm mrm btn btn-sm  btn-primary" >开始考试</a>进入</p>';
                            };

                            if (testEndLeftSeconds <= 0) {
                                clearInterval(iID);
                                $testNotice = '<p class="text-center mtl mbl color-gray"><i class="color-gray mrm es-icon es-icon-info"></i>考试已经结束</p><p class="text-center color-gray mbl"><span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#e6e6e6;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">00</span>天<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#e6e6e6;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">00</span>时<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#e6e6e6;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">00</span>分<span style="display:inline-block;width:80px;height:80px;line-height:80px;background:#e6e6e6;color:#fff;font-size:48px;border-radius:4px;margin:0 10px;">00</span>秒</p>';

                            };

                            $("#lesson-testpaper-content").find('.lesson-content-text-body').html($testNotice);

                            intervalSecond++;
                        }

                        generateTestHtml();

                        iID = setInterval(generateTestHtml, 1000);

                        $("#lesson-testpaper-content").show();
                        $("#lesson-testpaper-content").perfectScrollbar({
                            wheelSpeed: 50
                        });
                        $("#lesson-testpaper-content").scrollTop(0);
                        $("#lesson-testpaper-content").perfectScrollbar('update');

                    } else {
                        $.get('../../../testpaper/' + lesson.mediaId + '/user_result/json?lessonId='+lesson.id, function(result) {
                            if (result.error) {
                                html = '<span class="text-danger">' + result.error + '</span>';
                            } else {
                                if (result.status == 'nodo') {
                                    html = '欢迎参加考试，请点击「开始考试」按钮。<a href="' + url + '" class="btn btn-primary btn-sm" target="_blank">开始考试</a>';
                                } else if (result.status == 'finished') {
                                    var redoUrl = '/lesson/' + id + '/test/' + lesson.mediaId + '/redo';
                                    var resultUrl = '/test/' + result.resultId + '/result?targetType=lesson&targetId=' + id;
                                    html = '试卷已批阅。' + '<a href="' + redoUrl + '" class="btn btn-default btn-sm" target="_blank">再做一次</a>' + '<a href="' + resultUrl + '" class="btn btn-link btn-sm" target="_blank">查看结果</a>';
                                } else if (result.status == 'doing' || result.status == 'paused') {
                                    html = '试卷未完全做完。<a href="' + url + '" class="btn btn-primary btn-sm" target="_blank">继续考试</a>';
                                } else if (result.status == 'reviewing') {
                                    html = '试卷正在批阅。<a href="' + url + '" class="btn btn-primary btn-sm" target="_blank">查看试卷</a>'
                                }
                            }

                            $("#lesson-testpaper-content").find('.lesson-content-text-body').html(html);

                        }, 'json');

                    }


                } else if (lesson.type == 'ppt') {
                    $.get(that.get('courseUri') + '/lesson/' + id + '/ppt', function(response) {
                        if (response.error) {
                            var html = '<div class="lesson-content-text-body text-danger">' + response.error.message + '</div>';
                            $("#lesson-ppt-content").html(html).show();
                            return;
                        }

                        var html = '<div class="slide-player"><div class="slide-player-body loading-background"></div><div class="slide-notice"><div class="header">已经到最后一张图片了哦<button type="button" class="close">×</button></div></div><div class="slide-player-control clearfix"><a href="javascript:" class="goto-first"><span class="glyphicon glyphicon-step-backward"></span></a><a href="javascript:" class="goto-prev"><span class="glyphicon glyphicon-chevron-left"></span></a><a href="javascript:" class="goto-next"><span class="glyphicon glyphicon-chevron-right"></span></span></a><a href="javascript:" class="goto-last"><span class="glyphicon glyphicon-step-forward"></span></a><a href="javascript:" class="fullscreen"><span class="glyphicon glyphicon-fullscreen"></span></a><div class="goto-index-input"><input type="text" class="goto-index form-control input-sm" value="1">&nbsp;/&nbsp;<span class="total"></span></div></div></div>';
                        $("#lesson-ppt-content").html(html).show();

                        var watermarkUrl = $("#lesson-ppt-content").data('watermarkUrl');
                        if (watermarkUrl) {
                            $.get(watermarkUrl, function(watermark) {
                                var player = new SlidePlayer({
                                    element: '.slide-player',
                                    slides: response,
                                    watermark: watermark
                                });
                            });

                        } else {
                            var player = new SlidePlayer({
                                element: '.slide-player',
                                slides: response
                            });
                        }


                    }, 'json');
                } else if (lesson.type == 'document') {

                    $.get(that.get('courseUri') + '/lesson/' + id + '/document', function(response) {
                        if (response.error) {
                            var html = '<div class="lesson-content-text-body text-danger">' + response.error.message + '</div>';
                            $("#lesson-document-content").html(html).show();
                            return;
                        }

                        var html = '<iframe id=\'viewerIframe\' width=\'100%\'allowfullscreen webkitallowfullscreen height=\'100%\'></iframe>';
                        $("#lesson-document-content").html(html).show();

                        var watermarkUrl = $("#lesson-document-content").data('watermarkUrl');
                        if (watermarkUrl) {
                            $.get(watermarkUrl, function(watermark) {
                                var player = new DocumentPlayer({
                                    element: '#lesson-document-content',
                                    swfFileUrl: response.swfUri,
                                    pdfFileUrl: response.pdfUri,
                                    watermark: {
                                        'xPosition': 'center',
                                        'yPosition': 'center',
                                        'rotate': 45,
                                        'contents': watermark
                                    }
                                });
                            });
                        } else {
                            var player = new DocumentPlayer({
                                element: '#lesson-document-content',
                                swfFileUrl: response.swfUri,
                                pdfFileUrl: response.pdfUri
                            });
                        }
                    }, 'json');
                } else if (lesson.type == 'flash') {

                    if (!swfobject.hasFlashPlayerVersion('11')) {
                        var html = '<div class="alert alert-warning alert-dismissible fade in" role="alert">';
                        html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                        html += '<span aria-hidden="true">×</span>';
                        html += '</button>';
                        html += '您的浏览器未装Flash播放器或版本太低，请先安装Flash播放器。';
                        html += '</div>';
                        $("#lesson-swf-content").html(html);
                        $("#lesson-swf-content").show();
                    } else {
                        $("#lesson-swf-content").html('<div id="lesson-swf-player"></div>');
                        swfobject.embedSWF(lesson.mediaUri,
                            'lesson-swf-player', '100%', '100%', "9.0.0", null, null, {
                                wmode: 'opaque',
                                allowFullScreen: 'true'
                            });
                        $("#lesson-swf-content").show();
                    }

                }


                if (lesson.type == 'testpaper') {
                    that.element.find('[data-role=finish-lesson]').hide();
                } else {
                    if (!that.element.data('hideMediaLessonLearnBtn')) {
                        that.element.find('[data-role=finish-lesson]').show();
                    } else {
                        if (lesson.type == 'video' || lesson.type == 'audio') {
                            that.element.find('[data-role=finish-lesson]').hide();
                        } else {
                            that.element.find('[data-role=finish-lesson]').show();
                        }
                    }
                }

                that._toolbar.set('lesson', lesson);
                that._startLesson();
                that._afterLoadLesson(id);                
            }, 'json');

            $.get(this.get('courseUri') + '/lesson/' + id + '/learn/status', function(json) {
                var $finishButton = that.element.find('[data-role=finish-lesson]');
                if (json.status != 'finished') {
                    $finishButton.removeClass('btn-success');
                    $finishButton.attr('disabled', false);
                    $finishButton.find('.glyphicon').removeClass('glyphicon-check').addClass('glyphicon-unchecked');
                    $('[data-role=next-lesson]').hide();   
                    changeIcon(self.get('lessonId'),'doing');            
                } else {
                    $finishButton.addClass('btn-success');
                    $finishButton.attr('disabled', true);
                    $finishButton.find('.glyphicon').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
                    $finishButton.parent().show();
                    $('[data-role=next-lesson]').show();    
                    $("body").append("<div class='videoFinished' />") ;               
                }
                that.attrs.lessonlearnStatus.value=json.status;
            }, 'json');

            this._showOrHideNavBtn();
        },

        _showOrHideNavBtn: function() {
            var $prevBtn = this.$('[data-role=prev-lesson]');
            var $nextBtn = this.$('[data-role=next-lesson]');
            var index = $.inArray(parseInt(this.get('lessonId')), this._lessons);

            //课时已学完
            if ($('#lesson-dashboard').data('courseLearned') === 1) {
                $nextBtn.show();
            }
            $prevBtn.show();

            if (index < 0) {
                return;
            }

            if (index === 0) {
                $prevBtn.hide();
            } else if (index === (this._lessons.length - 1)) {
                $nextBtn.hide();
            }

        },

        _getNextLessonId: function(e) {

            var index = $.inArray(parseInt(this.get('lessonId')), this._lessons);
            if (index < 0) {
                return -1;
            }

            if (index + 1 >= this._lessons.length) {
                return -1;
            }

            return this._lessons[index + 1];
        },

        _getPrevLessonId: function(e) {
            var index = $.inArray(parseInt(this.get('lessonId')), this._lessons);
            if (index < 0) {
                return -1;
            }

            if (index == 0) {
                return -1;
            }

            return this._lessons[index - 1];
        },
        _initChapter: function(e) {
            this.chapterAnimate = new chapterAnimate({
                'element': this.element
            });
        },

        _getCountDown: function(days, hours, minutes, seconds) {
            $countDown = "还剩: <strong class='text-info'>" + days + "</strong>天<strong class='text-info'>" + hours + "</strong>小时<strong class='text-info'>" + minutes + "</strong>分钟<strong>" + seconds + "</strong>秒<br><br>";

            if (days == 0) {
                $countDown = "还剩: <strong class='text-info'>" + hours + "</strong>小时<strong class='text-info'>" + minutes + "</strong>分钟<strong class='text-info'>" + seconds + "</strong>秒<br><br>";
            };

            if (hours == 0 && days != 0) {
                $countDown = "还剩: <strong class='text-info'>" + days + "</strong>天<strong class='text-info'>" + minutes + "</strong>分钟<strong class='text-info'>" + seconds + "</strong>秒<br><br>";
            };

            if (hours == 0 && days == 0) {
                $countDown = "还剩: <strong class='text-info'>" + minutes + "</strong>分钟<strong class='text-info'>" + seconds + "</strong>秒<br><br>";
            };

            return $countDown;          
        },

    });

    var Counter = Class.create({
        initialize: function(dashboard, courseId, lessonId, watchLimit) {
            this.dashboard = dashboard;
            this.courseId = courseId;
            this.lessonId = lessonId;
            this.interval = 60;
            this.watched = false;
            this.watchLimit = watchLimit;
        },

        setTimerId: function(timerId) {
            this.timerId = timerId;
        },

        execute: function() {
            var posted = this.addMediaPlayingCounter();
            this.addLearningCounter(posted);
        },

        addLearningCounter: function(promptlyPost) {           

            var learningCounter = Store.get("lesson_id_" + this.lessonId + "_learning_counter");
            if (!learningCounter) {
                learningCounter = 0;
            }
            learningCounter++;

            if (promptlyPost || learningCounter >= this.interval) {

                waveLessonLearnTime(this.lessonId);
                return true;
            }

            Store.set("lesson_id_" + this.lessonId + "_learning_counter", learningCounter);
        },

        addMediaPlayingCounter: function() {           
            
            var mediaPlayingCounter = Store.get("lesson_id_" + this.lessonId + "_playing_counter");

            if (!mediaPlayingCounter) {
                mediaPlayingCounter = 1;
            }

            if (this.dashboard == undefined || this.dashboard.get("player") == undefined) {
                return false;
            }

            var playing = this.dashboard.get("player").playing;
            var posted = false;             

            checkPlayingOnly(this.lessonId,playing);

            if (mediaPlayingCounter >= this.interval || (mediaPlayingCounter > 1 && !playing)) {          
                posted = waveLessonWatchTime(this.lessonId,this.dashboard,this.watchLimit); 
                if(!playing)
                {
                     posted = true;
                }
                return posted;  
            } else if (playing) {
                mediaPlayingCounter++;
            }

            Store.set("lesson_id_" + this.lessonId + "_playing_counter", mediaPlayingCounter);

            return posted;
        }
    });
    function waveLessonLearnTime(lessonId)
    {
        var learningCounter = Store.get("lesson_id_" + lessonId + "_learning_counter");
        learningCounter++;
        var url = "/course/certification/" + lessonId + '/learn/time/' + learningCounter;
        $.get(url);
        learningCounter = 0;
        Store.set("lesson_id_" + lessonId + "_learning_counter", learningCounter);
    }
    function waveLessonWatchTime(lessonId,dashboard,watchLimit)
    {   
        if(!lessonId)
        {
            return false;
        }
        var mediaPlayingCounter = Store.get("lesson_id_" + lessonId + "_playing_counter");     
        var lessonViewId = dashboard.attrs.lessonViewId.value;    

        if(!parseInt(lessonViewId) || !mediaPlayingCounter)
        {   
            Store.set("lesson_id_" + lessonId + "_playing_counter", 0);
            return false
        }

        var lessonUrl = "/course/certification/" + lessonId + '/watch/time/' + mediaPlayingCounter;
        var self = dashboard;
        $.get(lessonUrl, function(response) {
            if (watchLimit && response.watchLimited) {
                window.location.reload();
            } else if (response.isExceeding && (typeof g_isBackToCourse === 'undefined' || !g_isBackToCourse)) {
                g_isBackToCourse = true;
                warmAsTimeExceed(response.maxLearningMinutes, self);
            }
        }, 'json');

        var lesssonViewUrl = "/course/lessonView/" + lessonViewId + '/watch/time/' + mediaPlayingCounter;       
        $.get(lesssonViewUrl);        
        mediaPlayingCounter = 1;        
        Store.set("lesson_id_" + lessonId + "_playing_counter", mediaPlayingCounter);
        return true;
    }
    function checkPlayingOnly(lessonId,playing)
    {
        playingLessonId = g_cookie.get('watching_lessonId');

        if(!playingLessonId)
        {
           return false;
        }

        if(playingLessonId != lessonId)
        {   
            
            if(window.frames["viewerIframe"] && playing)
            {               
                window.frames["viewerIframe"].window.BalloonPlayer.pause();
            }
            window.location.href = $("#lesson-dashboard").attr('data-cancel-url');            
        }

    }
    exports.run = function() { 

        var dashboard = new LessonDashboard({
            element: '#lesson-dashboard'
        }).render();               
        
        $(".es-qrcode").click(function() {
            var $this = $(this);
            var url = document.location.href.split("#");
            var id = url[1].split("/");
            if ($this.hasClass('open')) {
                $this.removeClass('open');
            } else {
                $.ajax({
                    type: "post",
                    url: $this.data("url") + "/lesson/" + id[1] + "/qrcode",
                    dataType: "json",
                    success: function(data) {
                        $this.find(".qrcode-popover img").attr("src", data.img);
                        $this.addClass('open');
                    }
                });
            }
        });       
        addLessonLearningListener();
        isShowClassInteraction();
    };

    function isShowClassInteraction()
    {
        if (typeof $("[data-plugin = 'question']") !== 'undefined') {
            var isShowClassInteraction = $('#lesson-toolbar-primary').data('classInteraction');

            if (!isShowClassInteraction) {
                $("[data-plugin = 'question']").addClass('hidden');
            }
        }
    }

    function changeIcon(itemId,type)
    {
        
        switch(type)
        {
            case 'doing':
                _class = 'es-icon es-icon-doing text-success  status-icon';
                break;
            case 'done':
                _class = 'zh-icon zh-icon-jiesuo text-success status-icon';
                break;

        }

        $("#course-item-list").find('.lesson-item-' + itemId).find('i').removeClass().addClass(_class);
    }

    //切换页面停止播放
    function listenPageVisible(player,lessonId)
    {

        // 各种浏览器兼容
        var hidden, state, visibilityChange; 
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
            state = "visibilityState";
        } else if (typeof document.mozHidden !== "undefined") {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
            state = "mozVisibilityState";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
            state = "msVisibilityState";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
            state = "webkitVisibilityState";
        }
       

        $(document).on(visibilityChange,function(){
            if(document[state] == 'hidden')
            {
                togglePlayerPause();
            }
            else{
                togglePlayerPlay();                
            }
        })        
    }

    function listenWindowTab()
    {
        window.onblur = 'togglePlayerPause';
    }


    //页面关闭时记录当前观看的视频进度
    function listenPageClose(lessonId,dashboard)
    {   
        $(window).on('beforeunload', 
            function() {

                //页面关闭或者刷新时 记录watchTime和learnTime
                waveLessonWatchTime(lessonId,dashboard);
                waveLessonLearnTime(lessonId);               
                var currentLoopTime = getCurrentLoopTime();
                var playerTime = null;
                try {
                    var player = window.frames["viewerIframe"].window.BalloonPlayer;
                    playerTime = parseInt(player.getCurrentTime());
                } catch (e) {
                    playerTime = currentTime;
                }
                //show.js 内 使用 此cookie获取播放进度, 见 _restorePlayTime 方法, 30天后过期
                g_cookie.set($('#lesson-video-content').attr("data-user-id") + "_" + lessonId + "_" + currentLoopTime +  "_time", playerTime, {path: "/", expires: 30}); 
            }
        ); 
    }

    function _dealTimeDuplicatedMarkers(markers) {
        var timeMarkers = {}; //key为时间, value为时间, 如果存在已重复的时间,则延后一秒,直到不重复为止
        for (var i = 0; i < markers.length; i++) {
            _updateMarkerTimeIfNeed(markers[i], timeMarkers);
        }
    }

    function _updateMarkerTimeIfNeed(marker, timeMarkers) {
        if (typeof timeMarkers[marker.time] != 'undefined') {
            marker.time = marker.time + 2;  //延后2秒
            _updateMarkerTimeIfNeed(marker, timeMarkers);
        } else {
            timeMarkers[marker.time] = true;
            if (marker.isFaceMarker) {
                var DELAY = 10;
                for (var i = 1; i < DELAY; i++) {  //如果是弹人脸, 10秒内的其他事件都延后
                    timeMarkers[marker.time + 1] = true;
                }
            }
        }
    }
    
    function clearVedioFinished(){
        if($(".videoFinished").size()>0)
        {
            $(".videoFinished").remove();
        }
    }

    function addLessonLearningListener() {
        var netWarninning = 0;
        g_cookie.remove('REMEMBERME');
        var timer = setInterval(function(){            
            $.ajax({
                url:'/lesson/'  + $(".lesson-content").attr("data-user-id") + '/learning',
                success:function(result) {
                    if (result != 'true') {
                        if(window.frames["viewerIframe"]) {
                            window.frames["viewerIframe"].window.BalloonPlayer.pause();
                            alert("此帐号已在别处登录或退出，请重新登录");
                            window.location.href = '/logout';
                        } else {
                            alert("此帐号已在别处登录或退出，请重新登录");
                            window.location.href = '/logout';
                        }
                    }
                    if(netWarninning == 1) {
                        netWarninning = 0;
                    }
                },
                error:function(error){
                    msg = '网络出现问题，无网环境下学习课程学时不能被记录。请您检查网络！';
                    if(netWarninning == 0 && window.frames["viewerIframe"]) {
                        window.frames["viewerIframe"].window.BalloonPlayer.pause();
                        alert(msg);
                        netWarninning = 1;
                    }
                }
            });
        },5000);
        return timer;
    }
    
    function addDocumentListener(){ 

        $(document).on('mouseleave',function(){
            togglePlayerPause();
        });

        $(document).on('keydown',function(e){
            // 当点击 ctrl, alt 或 windows 键时, 暂时视频
            if (e.keyCode == 17 || e.keyCode == 18 || e.keyCode == 91) {
                togglePlayerPause();
            }
        });
    }

    function _updateModelZindex(modal) {
        modal.css('z-index','2147483647').before($(".modal-backdrop").css('z-index','2147483647'));
    }

    function togglePlayerPlay()
    {
        if(!window.frames["viewerIframe"])
        {
            return false;
        }
        var player = window.frames["viewerIframe"].window.BalloonPlayer;
        if($(".modal:visible").length == 0 && !player.isPlaying())
        {
            player.play();
        }
    }

    function togglePlayerPause()
    {   
        if(!window.frames["viewerIframe"])
        {
            return false;
        }
        var player = window.frames["viewerIframe"].window.BalloonPlayer;
        if(player.isPlaying())
        {
            player.pause();
        }
    }

    function warmAsTimeExceed(maxLearningMinutes, lessonDashboard) {
      if (window.frames['viewerIframe'] !== undefined) {
        var player = window.frames["viewerIframe"].window.BalloonPlayer
        if (player !== undefined && player.isPlaying()) {
          player.pause();
        }
      }
      alert('今日学习时间已超出每日最大限制: ' + maxLearningMinutes + '分钟');
      window.location.href = '/course/certification/' + lessonDashboard.get('courseId');
    }
});