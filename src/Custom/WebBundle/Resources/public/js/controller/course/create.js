define(function(require, exports, module) {

  var Validator = require('bootstrap.validator');
  
  require('customadminbundle/controller/course/manage');
  require('common/validator-rules').inject(Validator);

  exports.run = function() {

    if($("#course-create-form").length>0) {

      var validator = new Validator({
        element: '#course-create-form',
        triggerType: 'change',
        onFormValidated: function(error){
          if (error) {
            return false;
          }
          $('#course-create-btn').button('submiting').addClass('disabled');
        }
      });

      validator.addItem({
        element: '[name="title"]',
        required: true
      });
      validator.addItem({
        element: '[name="areaCode"]',
        required: true,
        display: "行政区域"
      });
      validator.addItem({
        element: '[name="certificationId"]',
        required: true,
        display: "证书"
      });
      validator.addItem({
        element: '[name="learnModeMonths"]',
        required: true,
        rule: 'validateLearnMode',
        display: "学制",
        errormessageValidateLearnMode: '学制必须要被周期整除'
      });
      validator.addItem({
        element: '[name="partIndex"]',
        required: true,
        rule: 'positive_integer validatePartIndex',
        display: "课时部分",
        errormessageValidatePartIndex: '课时部分不能超过周期除以学制'
      });

    }
  };

});