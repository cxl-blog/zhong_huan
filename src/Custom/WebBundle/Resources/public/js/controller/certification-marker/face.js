define(function(require, exports, module) {

    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    var DraggableWidget = require('./mange-face');
    // 未避免初始化前端排序操作，将questionMarkers按生序方式返回，可省略questionMarkers.seq
    var initMarkerArry = [];
    var videoHtml = $('#lesson-dashboard');
    var courseId = videoHtml.data("course-id");
    var lessonId = videoHtml.data("lesson-id");
    var mediaLength = 30;
    $.ajax({
      type: "get", 
      url: $('.toolbar-face-marker').data('marker-metas-url'),
      data:{'type':'face'},
      cache:false,
      async:false,
      success:function(data){
        initMarkerArry = data.markersMeta;
        mediaLength =data.videoTime;
      }
    });
    var tempid = 0;

    var myDraggableWidget = new DraggableWidget({
        element: "#lesson-dashboard",
        initMarkerArry:initMarkerArry,
        videotime:mediaLength,
        addScale: function(markerJson,$marker) {
            var url = $('.toolbar-face-marker').data('marker-add-url');

            $.post(url,{'second':markerJson.second},function(data) {
                if (data.id == undefined) {
                    return;
                }
                //显示时间轴
                if ($marker.is(":hidden")) {
                    $marker.show();
                }
            });
            return markerJson;
        },
        mergeScale: function(markerJson, $marker, $merg_emarker, childrenum) {
            var url = $('.toolbar-face-marker').data('marker-delete-url');

            $("#" + markerJson.merg_id).find(".lesson-list").find("li").each(
                function(index) {
                    if (index != 0) {
                        $(this).remove();
                    }
                }
            );

            $.post(url,{'id':markerJson.id},function(data) {
                if ($marker.is(":hidden")) {
                    $marker.remove();
                    //查找
                }
            });
        },
        updateScale: function($marker, markerJson, old_position, old_time) {
            var url = $('.toolbar-face-marker').data('marker-update-url');

            $.post(url,{'id':markerJson.id,'second':markerJson.second},function(data) {
                //删除驻点
            });

            return markerJson;
        },
        deleteScale: function(markerJson, $marker, $marker_list_item) {
            var url = $('.toolbar-face-marker').data('marker-delete-url');

            $.post(url,{'id':markerJson.id},function(data) {
                if ($marker.is(":hidden")) {
                    $marker.remove();
                    //查找
                }
            });
        }
    });

    exports.run = function() {

    };

});