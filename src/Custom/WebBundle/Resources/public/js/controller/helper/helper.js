define(function(require, exports, module) {
  require("customwebbundle/ajax");
  $("#revokeCertification").click(
    function() {
      var params = {
        url: '/zhHelper/revokeCertification',
        token: $("[name='csrf-token']").val(),
        param: {
          'userId': $.trim($("[name = 'userId']").val()),
          'certificationName': $.trim($("[name = 'revokeCertificationName']").val())
        },
        success: function(data, textStatus){
          if (data.result) {
            alert('吊销成功');
          } else {
            var errMsg = "吊销失败";
            if (typeof data !== "undefined" && typeof data.errMsg !== "undefined") {
              errMsg += ": " + data.errMsg;
            }
            alert(errMsg);
          }
        }
      };
      new AjaxContext(params).load();
  });

  $("#clearSingleUserLearnTime").click(
    function() {
      var params = {
        url: '/zhHelper/clearSingleUserLearnTime',
        token: $("[name='csrf-token']").val(),
        param: {
          'mobile': $.trim($("[name = 'clearUserMobile']").val())
        },
        success: function(data, textStatus){
          if (data.result) {
            alert('清零成功');
          } else {
            var errMsg = "清零失败";
            if (typeof data !== "undefined" && typeof data.errMsg !== "undefined") {
              errMsg += ": " + data.errMsg;
            }
            alert(errMsg);
          }
        }
      };
      new AjaxContext(params).load();
  });

  $("#reAwardCertification").click(
    function() {
      var params = {
        url: '/zhHelper/reAwardCertification',
        token: $("[name='csrf-token']").val(),
        param: {
          'userId': $.trim($("[name = 'userId']").val()),
          'certificationName': $.trim($("[name = 'revokeCertificationName']").val()),
          'reAwardTimeStr': $.trim($("[name='reAwardTimeStr']").val())
        },
        success: function(data, textStatus){
          if (data.result) {
            alert('重新颁发成功');
          } else {
            var errMsg = "重新颁发失败";
            if (typeof data !== "undefined" && typeof data.errMsg !== "undefined") {
              errMsg += ": " + data.errMsg;
            }
            alert(errMsg);
          }
        }
      };
      new AjaxContext(params).load();
  });

  $("#transforBtn").click(
    function() {
      var params = {
        url:  '/zhHelper/formatDateStrAndDateNum',
        token: $("[name='csrf-token']").val(), 
        param: {
          'dateStr': $("#dateStr").val(),
          'dateNum': $("#dateNum").val()
        },
        success: function(data, textStatus) {  
          $("#formatedDateNum").html(data.formatedDateNum);
          $("#formatedDateStr").html(data.formatedDateStr);
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#updateUserCertificationAwardTime").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/updateUserCertificationAwardTime',
        token: $("[name='csrf-token']").val(), 
        param: {
          'awardTimeStr': $.trim($("[name='awardTime']").val()),
          'mobile': $.trim($("[name='mobile']").val()),
          'certificationName': $.trim($("[name='certificationName']").val())
        },
        success: function(data, textStatus) {  
          if (data.result) {
            $("#updateUserCertificationAwardTime").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          } else {
            var errMsg = "修改失败";
            if (typeof data !== "undefined" && typeof data.errMsg !== "undefined") {
              errMsg += ": " + data.errMsg;
            }
            $("#updateUserCertificationAwardTime").parent().append("<div class='tip' style='color: blue'>" + errMsg + "</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#batchUpdateUserCertificationAwardTime").click(
    function() {
      $('.tip').remove();
      var awardTimes = [];
      $("[name='awardTimes']").each(
        function() {
          awardTimes.push($.trim($(this).val()));
        }
      );
      var params = {
        url:  '/zhHelper/batchUpdateUserCertificationAwardTime',
        token: $("[name='csrf-token']").val(), 
        param: {
          'awardTimes': awardTimes.join(","),
          'idCard': $.trim($("[name='idCard']").val())
        },
        success: function(data, textStatus) {  
          if (data.result) {
            $("#batchUpdateUserCertificationAwardTime").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#generateIdCard").click(
    function() {
      var params = {
        url:  '/zhHelper/randomGenerateIdCards',
        token: $("[name='csrf-token']").val(),
        success: function(data, textStatus) {
          var idCards = data.join(", ");
          $(".genreatedIdCards").html(idCards);
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#completeAsOfflineCourse").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/completeAsOfflineCourse',
        token: $("[name='csrf-token']").val(),
        param: {
          'mobile': $.trim($('#completeMobile').val()),
          'currentLoopTime': $.trim($("#completeCurrentLoopTime").val()),
          'courseId': $.trim($("#completeCourseId").val())
        },
        success: function(data, textStatus) {
          if (data.result) {
            $("#completeAsOfflineCourse").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#removeOfflineCompletion").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/removeOfflineCompletion',
        token: $("[name='csrf-token']").val(),
        param: {
          'mobile': $.trim($('#completeMobile').val()),
          'currentLoopTime': $.trim($("#completeCurrentLoopTime").val()),
          'courseId': $.trim($("#completeCourseId").val())
        },
        success: function(data, textStatus) {
          if (data.result) {
            $("#removeOfflineCompletion").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#toReleaseVesion").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/toggleDataSyncTest',
        token: $("[name='csrf-token']").val(),
        param: {
          'isOn': false
        },
        success: function(data, textStatus) {
          if (data.result) {
            $("#toReleaseVesion").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#toTestVesion").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/toggleDataSyncTest',
        token: $("[name='csrf-token']").val(),
        param: {
          'isOn': true
        },
        success: function(data, textStatus) {
          if (data.result) {
            $("#toTestVesion").parent().append("<div class='tip' style='color: blue'>修改成功</div>");
          }
        }
      };

      new AjaxContext(params).load();
    }
  );

  $("#generateUserCertCoursesBtn").click(
    function() {
      $("#generateUserCertCoursesBtn").attr("disabled", true);
      var totalCount = 0;
      var userCountParams = {
        url:  '/zhHelper/findUserCount',
        token: $("[name='csrf-token']").val(),
        async: false,
        success: function(data, textStatus) {
          if (data.result) {
            totalCount = parseInt(data.userCount, 10);
          }
        }
      };
      new AjaxContext(userCountParams).load();

      var size = 100;
      for (var page = 1; page*size < (totalCount + size); page++) {

        var params = {
          url:  '/zhHelper/generateUserCertCourses',
          token: $("[name='csrf-token']").val(),
          param: {
            'size' : size,
            'startIndex' : (page-1)*size
          },
          async: false,
          success: function(data, textStatus) {
            if (data.result) {
              if (page*size >= totalCount) {
                $(".generateUserCertCoursesProgress").html("进度: 100%");
              }
            }
          }
        };

        new AjaxContext(params).load();
      }
    }
  );

  $("#generateVideoFaceDetect").click(
    function() {
      $('.tip').remove();
      var params = {
        url:  '/zhHelper/generateVideoFaceDetect',
        token: $("[name='csrf-token']").val(),
        param: {
          'faceDetectUserId': $('#faceDetectUserId').val(),
          'faceDetectLessonId': $('#faceDetectLessonId').val(),
          'faceDetectCurrentLoopTime': $('#faceDetectCurrentLoopTime').val()
        },
        success: function(data, textStatus) {
          var message = '';
          if (data.result) {
            message = '生成成功, 生成' + data.count + '张照片';
          } else {
            message = '生成失败';
          }
          $("#generateVideoFaceDetect").parent().append("<div class='tip' style='color: blue'>" + message + "</div>");
        }
      };

      new AjaxContext(params).load();
    }
  );
});