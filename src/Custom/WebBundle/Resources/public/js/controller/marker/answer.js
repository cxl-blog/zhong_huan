define(function(require, exports, module) {
    exports.run = function() {
    	var btn =$('#continue');
        var data = $("#data");
        var markerId = data.data('markerid');
        var questionId = data.data('questionid');
        var questionType = data.data('type');
        var player = window.frames["viewerIframe"].window.BalloonPlayer;
        btn.on('click', function() {            
            $(this).closest('#modal').hide();
            if (!player.isPlaying()) {
                player.play();
            }
        });
        $(".marker-modal .close").on('click', function() {
            $(this).closest('#modal').hide();
            if (!player.isPlaying()) {
                player.play();
            }
        });
    }
});