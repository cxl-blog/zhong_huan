define(function(require, exports, module) {

  let NicknameFormat = function() {};

  NicknameFormat.prototype = {
    formatUserName: function(username) {
      let rule = /^1\d{10}/;

      if (rule.test(username)) {
        return username.toString().substr(0, 4) + '****' + username.toString().substr(-3);
      }

      return username;
    },

    formatUserNameForReg: function(username) {
      let rule = /^1\d{10}/;

      if (rule.test(username)) {
        return username.toString().substr(0, 4) + '\\*\\*\\*\\*' + username.toString().substr(-3);
      }

      return username;
    }
  }

  module.exports = NicknameFormat;
});