define(function(require, exports, module) {
  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  require("jquery.bootstrap-datetimepicker");
  require("customwebbundle/ajax");
  var Cookie = require('cookie');
  var AreaCodeSelector = require('customwebbundle/area-code-selector');

  Validator.addRule(
    'nickname',
    function(options, commit){
      var nickname = options.element.val();
      var reg_nickname = /^1\d{10}$/;
      var result = false;
      var isNickname = reg_nickname.test(nickname);

      if(!isNickname){
        result = true;
      }
      return result;
    },
    "{{display}}不允许以1开头的11位纯数字"
  );

  exports.run = function() {

    new AreaCodeSelector({
      idCardOrMobileSelector: '#register_idcard',
      areaCodeSelector: '#login_areacode',
      areaCodeWrapper: '.area-code-select'
    });

    $(".date").datetimepicker({
      language: 'zh-CN',
      autoclose: true,
      format: 'yyyy-mm-dd',
      minView: 'month'
    });

    var $form = $('#register-form');        
    var validator = new Validator({
      element: $form,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#register-btn').button('submiting').addClass('disabled');
      },
      failSilently: true
    });

    if ($("#getcode_num").length > 0){
      
      $("#getcode_num").click(function(){ 
        $(this).attr("src",$("#getcode_num").data("url")+ "?" + Math.random()); 
      }); 

      validator.addItem({
        element: '[name="captcha_code"]',
        required: true,
        rule: 'alphanumeric remote',
        onItemValidated: function(error, message, eleme) {
          if (message === "验证码错误"){
            $("#getcode_num").attr("src",$("#getcode_num").data("url")+ "?" + Math.random()); 
          }
        }                
      });
    };

    validator.addItem({
      element: '[name="nickname"]', 
      required: true,
      rule: 'chinese_alphanumeric byte_minlength{min:4} byte_maxlength{max:18} nickname remote'
    });

    validator.addItem({
      element: '#register_idcard',
      required: true,
      rule: 'idcard remote'
    });

    validator.addItem({
      element: '#register_mobile',
      required: true,
      rule: 'mobile remote'
    });

    validator.addItem({
      element: '#register_password',
      required: true,
      rule: 'remote_with_paramFieldIds'
    });

    validator.addItem({
      element: '#user_terms',
      required: true,
      errormessageRequired: '勾选同意此服务协议，才能继续注册'
    });

    var $smsSendBtn=$("#smsSendBtn");

    __secondCounting($smsSendBtn);
    
    $smsSendBtn.click(function(){
      var $this=$(this);
      _onclickSendPassBtn($this);
    });
  };

  function _onclickSendPassBtn($this) {
    
    if (_validateBeforeSendPass()) {
      var mobile = $("#register_mobile").val();
      $this.addClass('sending');     
      $.get(
        '/register/mobile/check',
        {value:mobile},
        function(data)
        {   
          if(!data.success)
          {   
            $this.removeClass('sending');
            alert(data.message);
            return false;
          } 
          $this.html('正在发送……');
          new AjaxContext(
            {
              url:  '/register/smsSendPass',
              param: {
                'idcard' : $("#register_idcard").val(),
                'verifiedMobile': $("#register_mobile").val()
              },
              //成功时 success = true, 
              success: function(data, textStatus) {  
                if (data.success && data.success === "true") {
                  Cookie.set('registerPassSendTime', new Date().getTime(), {path: "/", expires: 1}); //1天后过期
                  alert("发送成功");
                  __secondCounting($this);
                }
              }
            }
          ).load();
        }
      )

    }
  }

  function __secondCounting($this){
    
    var startTime = Cookie.get('registerPassSendTime');
    if (typeof startTime !== "undefined") {
      var endTime = new Date().getTime();
      var seconds = 60 - Math.floor((endTime - startTime)/1000); //60秒内不能重新发送
      if (seconds < 0) {
        $this.html('获取初始密码');
      } else {
        $this.addClass('sending');
        var timer = setInterval(
          function() {
            $this.html((seconds--)+'s后重新发送');
            if(seconds<0) {
              localStorage.clear();
              localStorage.startTime='';
              $this.removeClass('sending');
              $this.html('重新发送密码');
              clearInterval(timer);
            }
          },
          1000
        );
      }
    } else {
      $this.html('获取初始密码');
    }
  }

  /**
   * 
   */
  function _validateBeforeSendPass() {
    if($("#smsSendBtn").hasClass('sending')){
      return false;
    }

    var result = true;
    var verifiedFields = ["#register_idcard", "#register_mobile"];
    for (var i = 0; i < verifiedFields.length; i++) {
      result &= $(verifiedFields[i]).parents(".has-error").length === 0;
      result &= $(verifiedFields[i]).val() !== "";
    }
    
    if (!result) {
      alert("请输入正确的身份证和手机号码");
    }

    return result; 
  }

});