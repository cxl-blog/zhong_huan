define(function(require, exports, module) {

    require("customwebbundle/ajax");

    exports.run = function() {
        $('.sendUrl').val("http://" + window.location.host + "/mapi_v2/User/login");
        $(".sendBtn").click(
            function() {
                var container = $(this).parents(".senderSection");
                var ajaxParams = {
                    'sendUrl' : container.find($(".sendUrl")).val(),
                    'requestBody': $.trim(container.find($(".sendHeadParams")).val()),
                    'requestType': container.find($(".requestType")).val()
                };
                var ajaxConfig = {
                    url:  '/urlSender/send',
                    cache: false,   //默认cache = false
                    type: 'POST', //默认type = POST
                    dataType: 'json',  //默认为json, 即返回类型是什么
                    param: ajaxParams,
                    success: function(data, textStatus) {  
                        if (typeof data.responseText != "undefined") {
                            container.find($(".sendResult")).html(data.responseText);
                        } else {
                            container.find($(".sendResult")).html(JSON.stringify(data));
                        }
                    }, //成功后的信息
                    error: function(XMLHttpRequest, textStatus, errorThrown) {} //后台出错时执行的方法, 默认为 _errorFunc
                };

                new AjaxContext(ajaxConfig).load();
            }
        );
    }

});