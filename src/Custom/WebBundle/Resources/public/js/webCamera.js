/**
 * 用于打开摄像头, 默认使用html5, 如果浏览器不支持html5且是PC浏览器,则用flash
 *    此js需要jquery支持
 * @param config  json, 格式为
 *     {
 *         tokenDomId: "...."       //如果涉及到上传图片,需要指定token, 值为 '{{ csrf_token('site') }}'
 *                                               // 如 <div id="tokenDomId">{{ csrf_token('site') }}</div> 
 *         uploadUrl: "/camera/uploadImage",
 *         video: {   //以下为默认值
 *             width: 400,
 *             height: 280
 *         }
 *     }
 *  
 *  使用要求: 所有的html5 相关的按钮 必须放在一个 class = "html5Section" 的节点内
 *                    页面上必须要有一个 class = "flashSection" 的节点,当页面不支持html5时,
 *                    能转成flash
 */
function WebCamera(config) {

    this._config = config;
    this._VIDEO_CONFIG = {video: true};
    this._isUseHtml5;   //false表示用flash

    this._IMAGE_PARAM = "imageData"; 
            //上传图片时,传到后台使用base64转码的图片流,同时参数为 imageData

    var current = this;

    this.init = function() {
        current._initConfigIfNeed();
        current._initHtml5Section();
        current._video = document.getElementById("video");
        current._canvas = document.getElementById("canvas");
        if (typeof current._canvas.getContext == "function") {
            current._context = current._canvas.getContext("2d");

            // 支持 html5
            if (typeof Worker != "undefined") {
                current._stardardOpenCameraIfCould();
                current._webkitOpenCameraIfCould();
                current._firefoxOpenCameraIfCould();
            }
        } 

        if (typeof current._isUseHtml5 == "undefined") {
            current._initFlashSection();
        }
    };

    this.snapAndUpload = function() {
        if (current._isUseHtml5) {
            current._html5Snap();
            current._html5Upload();
        } else {
            current._flashSnap();
            current._flashUpload();
        }
    };

    this.reSnap = function() {
        $(".snap").show();
        $(".finishSnap").hide();
        if (current._isUseHtml5) {
            current._displayHtml5Video();
        } else {
            findFlashNode('flashField').reSnapPic();
        }
    };

    this._html5Snap = function() {
        current._displayHtml5Canvas();
        var width = current._config.video.width - 26;
        current._context.drawImage(video, 13, 0, 
            width, current._config.video.height);
    };

    this._flashSnap = function() {
        findFlashNode('flashField').snapPic();
    };

    this._html5Upload = function() {
        var imgData = current._canvas.toDataURL("image/jpeg").
                        replace("data:image/jpeg;base64,", "");
        var ajaxContextParams = {
            cache: false,
            headers: {
                "X-CSRF-Token": $("#" + current._config.tokenDomId).html()
            },
            success: function(data, textStatus) {
                afterImageUpload(data);
            },
            type: "POST",
            url: current._config.uploadUrl
        };
        ajaxContextParams.data = {};
        ajaxContextParams.data[current._IMAGE_PARAM] = imgData;
        $.ajax(ajaxContextParams);
    };

    this._flashUpload = function() {
        findFlashNode('flashField').uploadPic();
    };
    
    this._stardardOpenCameraIfCould = function() {
        if (navigator.getUserMedia) {
            current._isUseHtml5 = true;
            navigator.getUserMedia(current._VIDEO_CONFIG, 
                function(stream) {
                    current._video.src = stream;
                    current._video.play();
                    current._invokeAfterInitIfHas();
                }, current._errWhenOpenCamera
            );
        }
    };

    this._webkitOpenCameraIfCould = function() {
        if(navigator.webkitGetUserMedia) { 
            current._isUseHtml5 = true;
            navigator.webkitGetUserMedia(current._VIDEO_CONFIG, 
                function(stream) {
                    current._video.src = window.webkitURL.createObjectURL(stream);
                    current._video.play();
                    current._invokeAfterInitIfHas();
                }, current._errWhenOpenCamera
            );
        }
    };

    this._firefoxOpenCameraIfCould = function() {
        if (navigator.mozGetUserMedia) {
            current._isUseHtml5 = true;
            navigator.mozGetUserMedia(current._VIDEO_CONFIG,
                function(stream) {
                    current._video.src = window.URL.createObjectURL(stream);
                    current._video.play();
                    current._invokeAfterInitIfHas();
                }, current._errWhenOpenCamera
            );
        }
    };

    this._errWhenOpenCamera = function(errMsg) {
        //高版本的chrome浏览器不支持在http下直接使用html5打开摄像头(需要https)
        //  这种情况下使用 flash
        if (navigator.userAgent.indexOf("Chrome") != -1) {
            current._initFlashSection();
        } else {
            onUserForbidCamera();
        }
    };

    this._initHtml5Section = function() {
        var html5Nodes = 
                "<video id='video' class='video' autoplay " 
            +       "width='" + current._config.video.width + "' "
            +       "height='" + current._config.video.height + "'></video>"
            +  "<canvas id='canvas' class='canvas' style='display:none' "
            +       "width='" +  current._config.video.width + "' " 
            +       "height='" + current._config.video.height + "'></canvas>";
        $(".html5Section .cameraContainer").html(html5Nodes);
    };

    this._initFlashSection = function() {
        current._isUseHtml5 = false;
        current._hideHtml5Section();
        var swfFilePath = '/bundles/customweb/flash/CameraEx.swf';

        var containerWidth = current._config.video.width;
        var containerHeight = current._config.video.height;
        
        var configParamStr = current._getFlashConfigParams();
        var flashNode = 
                "<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' " 
            +      "codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=21,0,0,0' "
            +      "width='" + containerWidth + "' height='" + containerHeight + "' "
            +      "id='flashField' align='middle'>"
            +          "<param name='allowScriptAccess' value='always' />"
            +          "<param name='movie' value='" + swfFilePath + "' />"
            +          "<param name='quality' value='high' />"
            +          "<param name='bgcolor' value='#ffffff' />"
            +          "<param name='FlashVars' value='" + configParamStr + "' />"
            +          "<embed src='" + swfFilePath + "' quality='high' bgcolor='#ffffff' " 
            +              "width='" + containerWidth + "' height='" + containerHeight + "' "
            +              "id='flashField' align='middle' "
            +              "allowscriptaccess='always'  "
            +              "FlashVars='" + configParamStr + "' "
            +              "type='application/x-shockwave-flash' "
            +              "pluginspage='http://www.macromedia.com/go/getflashplayer' />"
            +      "</object>";
        $(".flashSection .cameraContainer").html(flashNode);
        $(".flashSection").show();
    };

    this._getFlashConfigParams = function() {
        var flashConfig = {
            flashUploadUrl: current._config.uploadUrl,
            '_csrf_token': $("#tokenDomId").html(),
            'base64ImageParamName': current._IMAGE_PARAM,
            'videoWidth': current._config.video.width * 1.2,
            'videoHeight': current._config.video.height * 1.35
        };

        var paramArray = new Array();
        for (var key in flashConfig) {
            paramArray.push(key + "=" + flashConfig[key]);
        }
        return paramArray.join("&");
    };

    this._hideHtml5Section = function() {
        $(".html5Section").hide();
    };

    this._initConfigIfNeed = function() {
        if (typeof current._config.video == "undefined") {
            current._config.video = {
                width: 400,
                height: 280
            }
        }
    };

    this._displayHtml5Video = function() {
        $(".video").show();
        $(".canvas").hide();
    };

    this._displayHtml5Canvas = function() {
        $(".canvas").show();
        $(".video").hide();
    };

    this._invokeAfterInitIfHas = function() {
        if (typeof afterInit == "function") {
            afterInit();
        }
    };
}

function findFlashNode(nodeId) {
    if (window.document[nodeId]) {
        return window.document[nodeId];
    } 

    if (navigator.appName.indexOf("Microsoft Internet") == -1) {
        if (document.embeds && document.embeds[nodeId]) {
            return document.embeds[nodeId];
        }
    }

    return document.getElementById(nodeId);
}