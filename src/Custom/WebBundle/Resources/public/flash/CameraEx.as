package 
{ 
    import com.adobe.images.JPGEncoder;
    import flash.display.Sprite;
    import flash.display.SimpleButton; 
    import flash.display.BitmapData;
    import flash.display.Bitmap;
    import flash.display.LoaderInfo;
    import flash.events.ActivityEvent;
    import flash.events.Event;
    import flash.events.StatusEvent;  
    import flash.external.ExternalInterface;
    import flash.geom.Matrix;
    import flash.media.Camera; 
    import flash.media.Video;
    import flash.text.*; 
    import flash.utils.ByteArray;
    import flash.net.URLRequestHeader; 
    import flash.net.URLRequest;
    import flash.net.URLRequestMethod;  
    import flash.net.URLLoader;
    import flash.net.URLVariables;
    import mx.utils.Base64Encoder;
    
    public class CameraEx extends Sprite 
    { 
        private var camera:Camera;
        private var video:Video;
        private var viewedPicData:BitmapData;
        private var viewedPic:Bitmap;
        private var config:Object;
         
        public function CameraEx() 
        { 
            initJsAction();
            config = LoaderInfo(this.root.loaderInfo).parameters;
            camera=Camera.getCamera(); 
            if (camera!=null) { 
                initVideoSection();
                initSnapPicSection();
                displayVideo();
            } else{ 
                showErrorMsg("摄像头被禁用! "); 
            } 
        }

        public function snapPic():void {
            try {
                viewedPicData.draw(video);
                displaySnappedPic();
            } catch (error:Error) {
                showError(error);
            }
        }

        public function reSnapPic():void {
        	displayVideo();
        }
        
        /*
         * 此方法必须在 snapPic调用之后才能执行
         */
        public function uploadPic():void {
            try {
                var imgEncoder:JPGEncoder = new JPGEncoder();
                var picStream:ByteArray = imgEncoder.encode(viewedPicData);
                var base64Encoder:Base64Encoder = new Base64Encoder();
                base64Encoder.encodeBytes(picStream);
                showMsg("finish base64 encoding");
                var picURLRequest:URLRequest=new URLRequest(config.flashUploadUrl); 
                picURLRequest.requestHeaders.push(new URLRequestHeader("Content-type", 
                    "application/x-www-form-urlencoded; charset=UTF-8"));
                picURLRequest.method = URLRequestMethod.POST; 
                
                var variables:URLVariables = new URLVariables();
                variables[config.base64ImageParamName] = base64Encoder.toString();
                variables["_csrf_token"] = config["_csrf_token"];
                picURLRequest.data = variables; 

                var loader:URLLoader=new URLLoader(); 
                loader.load(picURLRequest); 
                loader.addEventListener(Event.COMPLETE, uploadCompleteHandler);
            } catch (error:Error) {
                showError(error);
            }
        }
        
        private function initJsAction():void {
            if (ExternalInterface.available) {
                try {	
                    ExternalInterface.addCallback("snapPic", snapPic);
                    ExternalInterface.addCallback("uploadPic", uploadPic);
                    ExternalInterface.addCallback("reSnapPic", reSnapPic);
                } catch (error:Error) {
                    showError(error);
                }
            }
        }
        
        private function initVideoSection():void {
            var cameraWidth:int = config.videoWidth;
            var cameraHeight:int = config.videoHeight;
            camera.addEventListener(StatusEvent.STATUS, statusHandler);
            camera.addEventListener(ActivityEvent.ACTIVITY, activityHandler);
            camera.setQuality(0, 100);  //100表示清晰度最高
            camera.setMode(cameraWidth, cameraHeight, 15); // 每秒15帧
            video = new Video(camera.width, camera.height); 
            video.attachCamera(camera); 
            video.x = -15;
            addChild(video);
        }
        
        private function initSnapPicSection():void {
            viewedPicData = new BitmapData(video.width,video.height);
            viewedPic = new Bitmap(viewedPicData);
            viewedPic.x = -15;
            addChild(viewedPic);
        }
         
        /*
         * 检测用户是否允许使用摄像头，一旦用户单击 允许 或 拒绝，会调用此方法
         * camera.Muted 表示用户点了拒绝，否则，用户点了允许
         */
        private function statusHandler(evt:StatusEvent):void{ 
            if (camera.muted) {
                ExternalInterface.call("onUserForbidCamera");
            }
        } 
         
        private function activityHandler(evt:ActivityEvent):void{ 
            if (evt.activating) { 
                ExternalInterface.call("afterInit");
            }
        } 
        
        private function uploadCompleteHandler(e:Event):void {
            var loader:URLLoader = URLLoader(e.target);
            ExternalInterface.call("afterImageUpload", loader.data);
            showMsg("Successfully to upload: " + loader.data);
        }
        
        private function showError(error:Error):void {
            var msg:String = "error " + error.message + "   " + error.getStackTrace(); 
            ExternalInterface.call("onFlashErrorMsg", msg);                
            throw new Error("A general error occurred.");
        }

        private function showErrorMsg(msg:String):void {
            ExternalInterface.call("onFlashErrorMsg", msg); 
        }

        private function showMsg(msg:String):void {
            ExternalInterface.call("onFlashMsg", msg);
        }

        private function displayVideo():void {
        	viewedPic.visible = false;
        	video.visible = true;
        }

        private function displaySnappedPic():void {
        	viewedPic.visible = true;
        	video.visible = false;
        }
    } 
}