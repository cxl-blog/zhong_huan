<?php

namespace Custom\WebBundle\Twig\Extension;

use Topxia\WebBundle\Twig\Extension\WebExtension as BaseWebExtension;
use Custom\Common\Util\StringUtils;

class WebExtension extends BaseWebExtension
{
    public function getFilters()
    {
        $filters = parent::getFilters();
        array_push($filters, new \Twig_SimpleFilter('at_format', array($this, 'atFormatFilter')));
        return $filters;
    }

    public function atFormatFilter($text, $ats = array())
    {
        if (empty($ats) || !is_array($ats)) {
            return $text;
        }

        $router = $this->container->get('router');

        foreach ($ats as $nickname => $userId) {
            $formatNickname = StringUtils::formatStringForSecurity($nickname);
            $formatNicknameReg = StringUtils::formatStringForRegular($nickname);

            $path = $router->generate('user_show', array('id' => $userId));
            $html = "<a href=\"{$path}\" data-uid=\"{$userId}\" target=\"_blank\">@{$formatNickname}</a>";

            $text = preg_replace("/@{$formatNicknameReg}/ui", $html, $text);
        }

        return $text;
    }
}
