<?php

namespace Custom\WebBundle\Controller;

use Custom\Common\Util\DateUtils;
use Custom\Common\Constants\LearnStatus;
use Custom\Common\Constants\CertCourseStatus;
use Custom\Service\Sms\SmsService;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\BaseController;
use Topxia\Common\ArrayToolkit;
use Topxia\Service\Common\ServiceKernel;

class HelperController extends BaseController
{

    public function helperIndexAction() 
    {
        // $data = $this->getSyncService()->genereateSingleGraduateCertificateInfo(408, 350, 1);
        // var_dump($data);
        // $this->generateMissedFtpFiles();
        // $this->ftpSendFile();
        // $result = $this->getFaceDetectResultService()->searchFaceDetectResults(
        //             array(
        //                 'userId' => 1530, 
        //                 'result' => 'success', 
        //                 'endDateTimeInt' => 1504713600,
        //             ), 
        //             'created', 
        //             0, 
        //             1
        //         );
        // var_dump($result);
        // $this->getVideoFaceDetectMockService()->generateMissedVideoFaceDetect();
        // $this->generateMockData();
        //$this->randomGenerateIdCardsAction();
        //$this->mockData();
        // var_dump($this->generateIdCards('620104', 1000));
        // $this->getSyncService()->syncSingleGraduateCertificate(10101, 7, 3);
        // $this->syncSingleUser();
        // $this->syncSingleCertificate();
        // $this->syncSingleUserRegister();
        // $this->syncSingleUserCertificateFrozen();
        // $this->syncSingleUserCourseFrozen();
        // $this->syncUserRegisteredFace();
        // file_put_contents('graduateSync', "\n\n" . json_encode($this->getSyncService()->genereateSingleGraduateCertificateInfo(10100, 1, 7)), FILE_APPEND);
        return $this->render('CustomWebBundle:Helper:helperIndex.html.twig',
            array(
                'isSyncDataTestOn' => file_exists('../app/config/syncDataTest.php')
            )
        );
    }

    public function randomGenerateIdCardsAction()
    {
        $idCards = $this->generateRandomIdCards(10);
        return $this->createJsonResponse($idCards);
    }

    public function formatDateStrAndDateNumAction(Request $request)
    {
        $result = array();

        $dateNum = $request->get("dateNum");
        if (!empty($dateNum)) {
            $formatedDateStr = DateUtils::number2StrWithSeconds($dateNum);
            $result['formatedDateStr'] = $formatedDateStr;
        }

        $dateStr = $request->get("dateStr");
        if (!empty($dateStr)) {
            $formatedDateNum = strtotime($dateStr);
            $result['formatedDateNum'] = $formatedDateNum;
        }

        return $this->createJsonResponse($result);
    }

    public function clearSingleUserLearnTimeAction(Request $request)
    {
        $mobile = $request->get('mobile');
        $userInfo = $this->getUserService()->getUserByVerifiedMobile($mobile);
        $this->getUserLearnTimeService()->clearSingleUserLearnTime($userInfo['id']);
        return $this->createJsonResponse(array('result' => true));
    }

    public function updateUserCertificationAwardTimeAction(Request $request)
    {
        $awardTimeStr = $request->get('awardTimeStr');
        $mobile = $request->get('mobile');
        $certificationName = $request->get('certificationName');

        $userInfo = $this->getUserService()->getUserByVerifiedMobile($mobile);
        $certification = $this->getCertificationService()->findCertificateByName($certificationName);
        $awardTime = strtotime($awardTimeStr);

        if (!isset($userInfo['id'])) {
            $result = array('result' => false, 'errMsg' => '学员不存在');
        } else if (!isset($certification['id'])) {
            $result = array('result' => false, 'errMsg' => '证书不存在');
        } else {
            $this->getCertificationService()->resetUserCertificate(
                $userInfo['id'], $certification['id'], $awardTime);

            $this->regenerateSingleCertCourses($userInfo['id'], $certification['id']);

            $result = array('result' => true);
        }
        return $this->createJsonResponse($result);
    }

    public function batchUpdateUserCertificationAwardTimeAction(Request $request)
    {
        $awardTimeStr = $request->get('awardTimes');
        $idCard = $request->get('idCard');

        $userProfile = $this->getUserService()->findUserProfileByIdcard($idCard);
        $certifications = $this->getCertificationService()->getAllCertifications();
        
        $awardTimeStrArray = explode(',', $awardTimeStr);
        $index = 0;
        foreach ($awardTimeStrArray as $awardTimeStr) {
            if (!empty($awardTimeStr)) {
                $awardTime = strtotime($awardTimeStr);

                if (isset($certifications[$index])) {
                    $this->getCertificationService()->resetUserCertificate(
                            $userProfile['id'], $certifications[$index]['id'], $awardTime);

                    $this->regenerateSingleCertCourses($userProfile['id'], $certifications[$index]['id']);
                }
            } 
            $index ++;
        };

        $result = array('result' => true);
        return $this->createJsonResponse($result);
    }

    public function revokeCertificationAction(Request $request)
    {
        $userId = $request->get('userId');
        $certificationName = $request->get('certificationName');
        $user = $this->getUserService()->getUser($userId);
        $certification = $this->getCertificationService()->findCertificateByName($certificationName);
        if(!isset($user['id'])){
            $result = array('result' => false, 'errMsg' => '学员不存在');
        }elseif (!isset($certification['id'])) {
            $result = array('result' => false, 'errMsg' => '证书不存在');
        }else{
            $userCertification = $this->getCertificationService()->findCertificationIdAndUserId($userId, $certification['id']);
            $this->getCertificationService()->revokeUserCertification($userCertification['id']);
            $result = array('result' => true);
        }
        return $this->createJsonResponse($result);
    }

    public function reAwardCertificationAction(Request $request)
    {
        $userId = $request->get('userId');
        $certificationName = $request->get('certificationName');
        $user = $this->getUserService()->getUser($userId);
        $certification = $this->getCertificationService()->findCertificateByName($certificationName);
        if(!isset($user['id'])){
            $result = array('result' => false, 'errMsg' => '学员不存在');
        }elseif (!isset($certification['id'])) {
            $result = array('result' => false, 'errMsg' => '证书不存在');
        }else{
            if ($request->get('reAwardTimeStr') == null) {
                return array('result' => false, 'errMsg' => '颁发时间有误');
            }
            $awardTime = strtotime($request->get('reAwardTimeStr'));
            try {
                $this->getCertificationService()->reAwardUserCertification($userId, $certification['id'], $awardTime);
            } catch (\CertificationNotRevokedException $e) {
                return array('result' => false, 'errMsg' => '证书未吊销');
            }

            $this->getCertificationService()->generateUserCertCourses($userId);
            $result = array('result' => true);
        }
        return $this->createJsonResponse($result);
    }

    public function completeAsOfflineCourseAction(Request $request)
    {
        $mobile = $request->get('mobile');
        $currentLoopTime = $request->get('currentLoopTime');
        $courseId = $request->get('courseId');
        $dId = crc32($mobile . '_' . $currentLoopTime . '_' . $courseId);
        $userInfo = $this->getUserService()->getUserByVerifiedMobile($mobile);
        $course = $this->getCourseService()->getCourse($courseId);
        $this->getCourseService()->createOfflineFinishedCourse(
            array(
                'userId' => $userInfo['id'],
                'dId' => $dId,
                'courseId' => $courseId,
                'certificationId' => $course['certificationId'],
                'currentLoopTime' => $currentLoopTime
            )
        );
        return $this->createJsonResponse(array('result' => true));
    }

    public function removeOfflineCompletionAction(Request $request)
    {
        $mobile = $request->get('mobile');
        $currentLoopTime = $request->get('currentLoopTime');
        $courseId = $request->get('courseId');
        $dId = crc32($mobile . '_' . $currentLoopTime . '_' . $courseId);
        $userInfo = $this->getUserService()->getUserByVerifiedMobile($mobile);
        $this->getCourseService()->deleteOfflineFinishedCoursesByDid($dId);
        return $this->createJsonResponse(array('result' => true));
    }

    public function findUserCountAction()
    {
        $userCount = $this->getUserService()->searchUserCount(array());
        return $this->createJsonResponse(array('result' => true, 'userCount' => $userCount));
    }

    public function generateUserCertCoursesAction(Request $request)
    {
        $startIndex = $request->get('startIndex');
        $size = $request->get('size');
        $allUsers = $this->getUserService()->searchUsers(array(), array('id', ' '), $startIndex, $size);
        if (!empty($allUsers)) {
            $this->getCourseDao()->getConnection()->transactional(
                function($conn) use ($allUsers) {
                    foreach ($allUsers as $user) {
                        $this->getCertificationService()->generateUserCertCourses($user['id']);
                    }
                }
            );
        }
        return $this->createJsonResponse(array('result' => true));
    }

    /**
     * 开启或关闭数据同步测试
     */
    public function toggleDataSyncTestAction(Request $request)
    {
        $isOn = $request->get('isOn');
        $jobs = $this->getCrontabService()->searchJobs(
            array('name' => 'SyncDataJob'), 'created', 0, PHP_INT_MAX
        );
        foreach ($jobs as $job) {
            $this->getCrontabService()->deleteJob($job['id']);
        }

        if (file_exists('../app/config/syncDataTest.php')) {
            unlink('../app/config/syncDataTest.php');
        }

        $this->getJobDataDao()->deteleAllJobData();
        if ($isOn == 'true') {
            $data = file_get_contents('../app/config/syncDataTest.php.dist');
            file_put_contents('../app/config/syncDataTest.php', $data);
            for ($i=0; $i < 12; $i++) { 
                $addedMinutes = 5 * $i;
                $createdJob = array(
                    'name' => 'SyncDataJob',
                    'cycle' => 'everyhour',
                    'cycleTime' => '+' . $addedMinutes . ' minutes',
                    'nextExcutedTime' => strtotime("+{$addedMinutes} minutes", time()),
                    'jobClass' => "Custom\\Service\\Sync\\Job\\SyncDataJob"
                );
                $this->getCrontabService()->createJob($createdJob);
            }
        } else {
            $createdJob = array(
                'name' => 'SyncDataJob',
                'cycle' => 'everyday',
                'cycleTime' => '02:00:00',
                'nextExcutedTime' => time(),
                'jobClass' => "Custom\\Service\\Sync\\Job\\SyncDataJob"
            );
            $this->getCrontabService()->createJob($createdJob);
        }
        return $this->createJsonResponse(array('result' => true));
    }

    public function generateVideoFaceDetectAction(Request $request)
    {
        $userId = $request->get('faceDetectUserId');
        $lessonId = $request->get('faceDetectLessonId');
        $currentLoopTime = $request->get('faceDetectCurrentLoopTime');
        $count = $this->generateMissedLessonPics($userId, $lessonId, $currentLoopTime);

        return $this->createJsonResponse(array('result' => true, 'count' => $count));
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getCourseDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.CourseDao');
    }

    protected function getCourseMemberDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.CourseMemberDao');
    }

    protected function getUserLearnTimeService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.UserLearnTimeService');
    }

    protected function getSmsService()
    {
        return $this->getServiceKernel()->createService('Custom:Sms.SmsService');
    }

    protected function getCrontabService()
    {
        return $this->getServiceKernel()->createService('Crontab.CrontabService');
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getUserCertificationStatesService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getUserCertCourseDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Certificate.UserCertCourseDao');
    }

    protected function getUserSerivce()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getJobDataDao()
    {
        return $this->getServiceKernel()->createDao('Custom:JobData.JobDataDao');
    }

    protected function getFaceDetectResultDao()
    {
        return $this->getServiceKernel()->createDao('Custom:FaceDetectResult.FaceDetectResultDao');
    }

    protected function getLessonLearnDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.LessonLearnDao');
    }

    protected function getSyncFtpService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncFtpService');
    }

    protected function getMarkerService()
    {
        return $this->getServiceKernel()->createService('Custom:Marker.MarkerService');
    }

    protected function getVideoFaceDetectMockService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.VideoFaceDetectMockService');
    }

    private function generateRandomIdCards($count)
    {
        $areas = array(
            '110000', '120000', '130000', '140000', '150000', 
            '210000', '220000', '230000',
            '310000', '320000', '330000', '340000', '350000', '360000', '370000',
            '410000', '420000', '430000', '440000', '450000', '460000',
            '500000', '510000', '520000', '530000', '540000', 
            '610000', '620000', '630000', '640000', '650000'
        );

        $idCards = array();
        for ($i=0; $i < $count; $i++) { 
            $prefix = $areas[mt_rand(0, count($areas)-1)];
            $idCounts = $this->generateIdCards($prefix, 1000);
            $idCard = $idCounts[mt_rand(0, count($idCounts)-1)];
            array_push($idCards, $idCard);
        }

        return $idCards;
    }

    private function generateIdCards($prefix, $count)
    {
        $idCardPrefix = isset($prefix) ? $prefix : '620101';
        $idCards = array();

        for ($j=0; $j < $count; $j++) { 
            $day = $j % 18 + 10;
            $month = $j / 18 % 2 + 10;
            $year = $j / (2*18) % 365 + 1980;
            $bornDateTime = $year . $month . $day;
            $idCardMiddle = $bornDateTime . '001';

            $idCardsMul = array(
                7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2
            );

            $idCardWithoutLastLetter = $idCardPrefix.$idCardMiddle;

            $numArr = str_split($idCardWithoutLastLetter);
            $num = 0;
            for ($i=0; $i < sizeof($numArr); $i++) { 
                $num += $numArr[$i] * $idCardsMul[$i];
            }

            $lastNum = $num % 11;
            $converts = array(
                '0' => '1', 
                '1' => '0',
                '2' => 'X',
                '3' => '9',
                '4' => '8',
                '5' => '7',
                '6' => '6',
                '7' => '5',
                '8' => '4',
                '9' => '3',
                '10' => '2'
            );
            $idCard = $idCardWithoutLastLetter . $converts[$lastNum];

            array_push($idCards, $idCard);
        }

        return $idCards;
    }

    private function displayGenerateIdCards($prefix, $count)
    {
        $idCards = $this->generateIdCards($prefix, $count);

        $j = 0;
        foreach ($idCards as $idCard) {
            echo($idCard);
            echo('<br/>');

            if ($j % 10 == 9) {
                echo('<br/>');
            }
            $j ++;
        }
    }

    private function mockData()
    {
        // update user_cert_course set status = 'LEARNING', latestBuyTime = unix_timestamp(now()) where userId between 30001 and 33000;
        $startedUserId = 40000;
        $areaCode = '3429596817';
        $userCount = 4000;
        $courseId = 191; // 测试站344, 正式站57;

        $finalSql             = '';
        $generateIdCards = $this->generateIdCards('620121',  $userCount);
        $sqlUserCertification = 'INSERT INTO `user_certification` (`userId`, `certificationId`, `awardTime`) VALUES';
        $sqlUser              = 'INSERT INTO `user` (`id`, `email`, `password`, `salt`, `nickname`, `type`, `roles`, `approvalStatus`, `createdTime`, `toLearn`, `areaCode`) VALUES';
        $sqlUserProfile       = 'INSERT INTO `user_profile` (`id`, `truename`, `idcard`, `gender`) VALUES';
        $sqlCourseMember = 'INSERT INTO  course_member (courseId, joinedType, userId, orderId, isVisible, role) VALUES';
        $sqlOrders = 'INSERT INTO orders (id, sn, status, title, targetType, targetId, userId, paidTime) VALUES';
        $sqlUserLearnTime = 'INSERT INTO user_learn_time (id, userId, learnedSeconds, learnedMinutes, certificationId) values ';
        $count = $userCount + $startedUserId;
        for ($i = 1 + $startedUserId; $i <= $count; $i++) {
            $userId   = $i;
            $orderId = $i;
            $sqlUserLearnTimeId = $i;
            $sn = 'C20160603200115'.$userId;
            $email    = "mail$i@qq.com";  //mail13001@qq.com
            $userName = '黄明'.$i;
            
            $salt = 'g5k1a2rgwgg8gcc0o8ww000488ck88w';
            $password = 'INzzHsfObQKAgovQO0y1pdi0orixH45wQ21F7LLbtjY=';

            $sqlUser .= "\n($userId, '$email', '$password', '$salt', '$userName', 'web_email', '|ROLE_USER|','unapprove', 1457316452, 0, '$areaCode')";
            $sqlCourseMember .= "\n($courseId, 'course', $userId, $orderId, 1, 'student')";
            $sqlOrders .= "\n($orderId, '$sn', 'paid','购买课程《北京客运》', 'course', $courseId, $userId, '1464955275')";

            $idCard = $generateIdCards[$i - $startedUserId - 1];
            $sqlUserProfile .= "\n($userId, '$userName', '$idCard', 'male')";

            $certificationCount = 1;
            for ($j = 1; $j <= $certificationCount; $j++) {
                $sqlUserCertification .= "\n($userId, $j, 1460689930)";
                if ($i < $count || $j < $certificationCount) {
                    $sqlUserCertification .= ',';
                }

                $sqlUserLearnTime .= "\n($sqlUserLearnTimeId, $userId, 0, 0, $j)";
                if ($i < $count || $j < $certificationCount) {
                    $sqlUserLearnTime .= ',';
                }
            }

            if ($i < $count) {
                $sqlUser .= ',';
                $sqlUserProfile .= ',';
                $sqlOrders .= ',';
                $sqlCourseMember .= ',';
            }
        }

        $finalSql = $sqlUserCertification.";\n\n".$sqlUser.";\n\n".$sqlUserProfile.";\n\n".$sqlOrders.";\n\n".$sqlCourseMember.";\n\n".$sqlUserLearnTime.";\n\n";
        file_put_contents('allSqls.sql', $finalSql);
    }

    private function syncArea()
    {
        $json = file_get_contents('dataSync/sampleData/syncArea');    // web/dataSync/syncArea 目录下
        $this->getSyncService()->syncArea($json);
    }

    private function syncSingleUser()
    {
        $json = file_get_contents('dataSync/sampleData/syncSingleUser');    // web/dataSync/syncSingleUser 目录下

        $this->getSyncService()->syncSingleUser($json);
    }

    private function syncSingleUserRegister()
    {
        $json = file_get_contents('dataSync/sampleData/syncSingleUserRegister');    // web/dataSync/syncSingleUser 目录下

        $this->getSyncService()->syncSingleUserRegister($json);
    }

    private function syncSingleUserCertificateFrozen()
    {
        $json = file_get_contents('dataSync/sampleData/syncSingleUserCertificateFrozen');    // web/dataSync/syncSingleUser 目录下
        $this->getSyncService()->syncSingleUserCertificateFrozen($json);
    }

    private function syncSingleUserCourseFrozen()
    {
        $json = file_get_contents('dataSync/sampleData/syncSingleUserCourseFrozen');    // web/dataSync/syncSingleUser 目录下
        $this->getSyncService()->syncSingleUserCourseFrozen($json);
    }

    private function syncUserRegisteredFace()
    {
        $json = file_get_contents('dataSync/sampleData/syncUserRegisteredFace');

        $fields = json_decode($json, true);

        $user = $this->getUserService()->findUserByStuId($fields['stuId']);

        if (empty($user)) {
            return ;
        }

        $date = date('Y-m-d', strtotime("-1 day", time()));

        $conditions = array(
            'type' => 'register', //返回注册+修改人脸的信息
            'result' => 'success', //返回成功的人脸信息
            'userId' => $user['id'],
            'startDateTime' => $date.' 00:000:00',
            'endDateTime' => $date.' 23:59:59'
        );

        $faceDetectResults = $this->getFaceDetectResultService()->searchFaceDetectResults(
            $conditions,
            'createdByAsc',
            0,
            PHP_INT_MAX
        );

        foreach ($faceDetectResults as $faceDetectResult) {
            $this->getSyncService()->syncUserRegisteredFace($faceDetectResult['id']);
            usleep(500000); //延迟0.5s
        }
    }

    private function syncSingleCertificate()
    {
        $json = file_get_contents('dataSync/sampleData/syncSingleCertificate');    // web/dataSync/syncSingleUser 目录下
        $this->getSyncService()->syncSingleCertificate($json);
    }

    private function regenerateSingleCertCourses($userId, $certificationId)
    {
        $this->getUserCertCourseDao()->deleteUserCertCourseForWholeCert($userId, $certificationId);

        $courses = $this->getCourseDao()->searchCourses(
            array('certificationId' => $certificationId), array('id', ' '), 0, PHP_INT_MAX);
        foreach ($courses as $course) {
            $this->getCourseMemberDao()->deleteMemberByCourseIdAndUserId($course['id'], $userId);
            for ($currentLoopTime = CourseLoopUtils::$START_LOOP_TIME; 
                        $currentLoopTime  < CourseLoopUtils::$LOOP_TIMES; $currentLoopTime ++) {
                $this->getCourseService()->clearCourseHistory($userId, $course['id'], $currentLoopTime);
            }
        }
        $this->getUserCertificationStatesService()->deleteUserCertificationStates($userId, $certificationId);
        $this->getCourseService()->deleteOfflineFinishedCourses($userId, $certificationId);
        $this->getCertificationService()->deleteRevokedUserCerts($userId, $certificationId);
        $this->getCertificationService()->generateUserCertCourses($userId);

    }

    private function generateMockData()
    {
        $idCards = array(
            '321027197810253040'
        );
        $courseId = 62;

        $lessonViewsRecords = array(
            704 => array(
                'lessonId' => 704,
                'startTime' => '2017-4-06 15:03:29',
                'watchTime' => 10,
                'fileId' => 176
            ),
            705 => array(
                'lessonId' => 705,
                'startTime' => '2017-4-06 16:02:28',
                'watchTime' => 45,
                'fileId' => 173
            ),
            706 => array(
                'lessonId' => 706,
                'startTime' => '2017-4-06 16:10:24',
                'watchTime' => 8,
                'fileId' => 174
            ),
            720 => array(
                'lessonId' => 720,
                'startTime' => '2017-4-06 16:28:12',
                'watchTime' => 17.5,
                'fileId' => 174
            )
        );

        $faceRecords = array(
            array(
                'lessonId' => 704,
                'plusTime' => 300,
                'lessonName' => '道路危险货物运输押运人员职业技能和素质'
            ),
            array(
                'lessonId' => 705,
                'plusTime' => 904,
                'lessonName' => '各类危险货物运输押运安全要求及事故应急处置'
            ),
            array(
                'lessonId' => 705,
                'plusTime' => 1803,
                'lessonName' => '各类危险货物运输押运安全要求及事故应急处置'
            ),
            array(
                'lessonId' => 706,
                'plusTime' => 301,
                'lessonName' => '几种大宗危险货物押运要求'
            )
        );

        $sqls = array();
        $startViewId = 60000 + rand(0, 9000);
        foreach ($idCards as $idCard) {
            $userInfo = $this->getUserService()->searchUserProfiles(
                array('idcard' => $idCard),
                array('id', 'asc'),
                0,
                1
            );
            $userId = $userInfo[0]['id'];

            $conditions = array(
                'startDateTime' => '2017-4-1',   // 2017-4-1
                'endDateTime' =>'2017-5-1',      // 2017-5-1
                'type' => 'login',
                'userId' => $userId
            );
            $faces = $this->getFaceDetectResultService()->searchFaceDetectResults(
                $conditions,
                'createdByAsc',
                0,
                100
            );
            $successedFaces = array();
            foreach ($faces as $face) {
                if ($face['result'] == 'success') {
                    array_push($successedFaces, $face);
                }
            }

            foreach ($lessonViewsRecords as $key => $record) {
                $startViewId ++;
                $lessonViewsRecords[$key]['viewId'] = $startViewId;
                $fileId = $record['fileId'];
                $createdTime = strtotime($record['startTime']);
                $watchTime = $record['watchTime'] * 60;
                $lessonViewSql = 
                    "insert into course_lesson_view " 
                  . "(id, courseId, lessonId, fileId, userId, fileType, "
                  . "fileStorage, fileSource, createdTime, watchTime, viewDevice, currentLoopTime) "  //此为变量
                  . "values "
                  . "({$startViewId}, {$courseId}, {$key}, {$fileId}, {$userId}, 'video', "
                  .  "'cloud', 'self', {$createdTime}, {$watchTime}, 'desktop', 1);";

                array_push($sqls, $lessonViewSql);
            }

            foreach ($faceRecords as $key => $record) {
                if (isset($record['isTestpaper'])) {
                    continue;
                }

                if (isset($successedFaces[$key])) {
                    $faceInfo = $successedFaces[$key];
                } else {
                    $faceInfo = $successedFaces[0];
                }
                $fileUrl = $faceInfo['fileUrl'];
                $lessonId = $record['lessonId'];
                $startTime = $lessonViewsRecords[$lessonId]['startTime'] + $record['plusTime'];
                $regRate = $faceInfo['regRate'];
                $lessonName = $record['lessonName'];
                $viewId = $lessonViewsRecords[$lessonId]['viewId'];
                $course = $this->getCourseService()->getCourse($courseId);
                $courseTitle = $course['title'];
                $faceDetectResultSql = 
                    "insert into face_detect_result " 
                  . "(type, courseId, lessonId, content, result, currentLoopTime, "
                  . "userId, lessonViewId, fileUrl, createdTime, regRate) "  //此为变量
                  . "values "
                  . "('lesson', {$courseId}, {$lessonId}, '{$courseTitle}-${lessonName}', 'success', 1, "
                  .  "{$userId}, {$viewId}, '{$fileUrl}', {$startTime}, {$regRate});";
                array_push($sqls, $faceDetectResultSql);
            }
        }

        file_put_contents('hello', implode("\n\n", $sqls));
    }

    private function generateMissedLessonPics($userId, $lessonId, $currentLoopTime)
    {
        $faceMarkers = $this->getMarkerService()->findFaceMarkersMetaByMediaId($lessonId);

        $generatedCount = 0;
        if (!empty($faceMarkers) && !$this->hasLessonFaces($userId, $lessonId, $currentLoopTime)) {
            $fetchSize = 1;
            $lessonView = $this->getCourseService()->searchLessonView(
                array(
                    'userId' => $userId,
                    'lessonId' => $lessonId,
                    'currentLoopTime' => $currentLoopTime
                ),
                array('id', 'asc'),
                0,
                $fetchSize
            );
            if (!empty($lessonView)) {
                $generatedCount = $this->createFaceDetectResult($lessonView[0], $faceMarkers);
            }
        }
        
        return $generatedCount;
    }

    private function hasLessonFaces($userId, $lessonId, $currentLoopTime)
    {
        $fetchSize = 1;
        $lessonsFaces = $this->getFaceDetectResultService()->searchFaceDetectResults(
            array(
               'userId' => $userId,
               'lessonId' => $lessonId,
               'currentLoopTime' => $currentLoopTime,
               'type' => 'lesson'
            ),
            'created',
            0,
            $fetchSize
        );
        return !empty($lessonsFaces);
    }

    private function createFaceDetectResult($lessonView, $faceMarkers)
    {
        $course = $this->getCourseService()->getCourse($lessonView['courseId']);
        $lesson = $this->getCourseService()->getLesson($lessonView['lessonId']);
        $courseLessonLearn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($lessonView['userId'], $lessonView['lessonId'], $lessonView['currentLoopTime']);
        $loginFaces = $this->getFaceDetectResultService()->searchFaceDetectResults(
            array(
               'userId' => $lessonView['userId'],
               'currentLoopTime' => $lessonView['currentLoopTime'],
               'type' => 'login'
            ),
            'created',
            0,
            count($faceMarkers)
        );

        $loginFacesCount = count($loginFaces);
        $generatedCount = 0;

        $finalMarkers = array();
        foreach ($faceMarkers as $key => $marker) {
            array_push($finalMarkers, $marker);
        }

        for ($i=0; $i < count($finalMarkers); $i++) { 
            $loginFaceIndex = $i % $loginFacesCount;
            $loginFace = $loginFaces[$loginFaceIndex];

            $faceDetectResult = array(
                'userId' => $lessonView['userId'],
                'type' => 'lesson',
                'courseId' => $lessonView['courseId'],
                'lessonId' => $lessonView['lessonId'],
                'lessonViewId' => $lessonView['id'],
                'content' => $course['title'] . '-' . $lesson['title'],
                'fileUrl' => $loginFace['fileUrl'],
                'result' => 'success',
                'createdTime' => $courseLessonLearn['startTime'] + $finalMarkers[$i]['second'],
                'currentLoopTime' => $lessonView['currentLoopTime'],
                'regRate' => $loginFace['regRate'],
                'mobile' => $loginFace['mobile']
            );
            $this->getFaceDetectResultDao()->addFaceDetectResult($faceDetectResult);
            $generatedCount ++;
        }
        return $generatedCount;
    }

    private function ftpSendFile()
    {
        $ftpInfo = array(
            'host' => '211.139.111.13',
            'port' => 21,
            'account' => 'jn',
            'password'=> 'jn'
        );
        var_dump($ftpInfo);
        $conn = @ftp_connect($ftpInfo['host'], $ftpInfo['port'], 5);
        @ftp_login($conn, $ftpInfo['account'], $ftpInfo['password']);
        $isPassitive = @ftp_pasv($conn, 1);

        if (!$isPassitive) {
            var_dump('非被动模式，会被防火墙拦截');
        }

        var_dump('ftp 连接成功');
        var_dump(@ftp_pwd($conn));
        $filePath = '/var/www/projects/zhonghuan/sonar-project.properties';
        var_dump('file exists: ' . file_exists($filePath));
        @ftp_chdir($conn, '/jy');
        $result = @ftp_put($conn, 'a.jpg', $filePath, FTP_BINARY);
        var_dump($result);
        @ftp_close($conn);
    }

    private function generateMissedFtpFiles()
    {
        $projectPath = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') . '/../../';
        $path = ServiceKernel::instance()->getParameter('topxia.upload.public_directory') 
                . '/../../app/data/missedFaces.md';
        $missedFacesStr = file_get_contents($path);
        $missedFaces = explode("\n", $missedFacesStr);
        foreach ($missedFaces as $missedFace) {
            $segs = explode('|', $missedFace);
            $trimedSegs = array();
            foreach ($segs as $seg) {
                array_push($trimedSegs, trim($seg));
            }
            $userId = $trimedSegs[4];

            $user = $this->getUserService()->getUser($userId);
            $userProfile = $this->getUserSerivce()->getUserProfile($userId);
            $localFile = $projectPath . 'web/files/' . explode('public://', $trimedSegs[1])[1];

            $fileInfos = explode('/', $trimedSegs[1]);
            $fileName = $fileInfos[count($fileInfos) - 1];

            $dateStr = date('Y-m-d', strtotime($trimedSegs[2]));
            $remoteFile = '/jy/' . $dateStr . '/' . $userProfile['idcard'] . '/' . $fileName;
            $this->getSyncFtpService()->addSyncFtpInfo(
                array(
                    'areaCode' => $user['areaCode'],
                    'sourcefile' => $localFile,
                    'remotefile' => $remoteFile,
                    'type' => 'sync_certificate'
                )
            );
        }
    }

}

