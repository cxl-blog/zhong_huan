<?php
namespace Custom\WebBundle\Controller;

use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\OrderRefundController;
use Custom\Service\Order\OrderRefundProcessor\OrderRefundProcessorFactory;

class CertificationOrderRefundController extends OrderRefundController
{
    public function refundAction(Request $request, $id)
    {
        $targetType = $request->query->get("targetType");

        if (!in_array($targetType, array("course", "classroom"))) {
            throw $this->createAccessDeniedException('参数不对。');
        }

        $processor = OrderRefundProcessorFactory::create($targetType);

        $target          = $processor->getTarget($id);
        $user            = $this->getCurrentUser();
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);
        $member          = $processor->getTargetMember($id, $user["id"], $currentLoopTime);

        if (empty($member) || empty($member['orderId'])) {
            throw $this->createAccessDeniedException('您不是学员或尚未购买，不能退学。');
        }

        $course = $this->getCourseService()->getCourse($id);
        $status = array();

        if ($course['type'] == "certification") {
            $learnLessonStatus = $this->getCourseService()->getUserLearnLessonStatuses($user['id'], $id, $currentLoopTime);

            foreach ($learnLessonStatus as $learnStatus) {
                if ($learnStatus == "finished") {
                    array_push($status, $learnStatus);
                }
            }
        }

        $order = $this->getOrderService()->getOrder($member['orderId']);

        if (empty($order)) {
            throw $this->createNotFoundException();
        }

        if ($order['targetType'] == 'groupSell') {
            throw $this->createAccessDeniedException('组合购买课程不能退出。');
        }

        if ('POST' == $request->getMethod()) {
            $data   = $request->request->all();
            $reason = empty($data['reason']) ? array() : $data['reason'];
            $amount = empty($data['applyRefund']) ? 0 : null;

            if (isset($data["applyRefund"]) && $data["applyRefund"]) {
                $refund = $processor->applyRefundOrder($member['orderId'], $amount, $reason, $this->container);
            } else {
                $processor->removeStudent($order['targetId'], $user['id'], $currentLoopTime);
            }

            return $this->createJsonResponse(true);
        }

        $maxRefundDays = (int) $this->setting('refund.maxRefundDays', 0);
        $refundOverdue = (time() - $order['createdTime']) > ($maxRefundDays * 86400);

        return $this->render('CustomWebBundle:OrderRefund:refund-modal.html.twig', array(
            'target'        => $target,
            'targetType'    => $targetType,
            'order'         => $order,
            'maxRefundDays' => $maxRefundDays,
            'refundOverdue' => $refundOverdue,
            'learnStatus'   => empty($status[0]) ? "" : $status[0]
        ));
    }

    public function cancelRefundAction(Request $request, $id)
    {
        $targetType = $request->query->get("targetType");

        if (!in_array($targetType, array("course", "classroom"))) {
            throw $this->createAccessDeniedException('参数不对。');
        }

        $processor = OrderRefundProcessorFactory::create($targetType);

        $target = $processor->getTarget($id);

        if (empty($target)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if (empty($user)) {
            throw $this->createAccessDeniedException();
        }

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);
        $member          = $processor->getTargetMember($target['id'], $user['id'], $currentLoopTime);

        if (empty($member) || empty($member['orderId'])) {
            throw $this->createAccessDeniedException('您不是学员或尚未购买，不能取消退款。');
        }

        $processor->cancelRefundOrder($member['orderId']);

        return $this->createJsonResponse(true);
    }

    protected function getOrderService()
    {
        return $this->getServiceKernel()->createService('Order.OrderService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }
}
