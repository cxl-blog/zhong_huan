<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\SimpleValidator;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\RegisterController as TopxiaRegisterController;

class RegisterController extends TopxiaRegisterController
{
    public function smsSendPassAction(Request $request)
    {
        $idcard    = $request->request->get('idcard');
        $mobileNum = $request->request->get('verifiedMobile');

        $userProfile = $this->getUserService()->findUserProfileByIdcard($idcard);

        if ($userProfile) {
            $this->getRegisterService()->sendLoginPassBySms($userProfile['id'], $idcard, $mobileNum);
            return $this->createJsonResponse(array("success" => "true"));
        } else {
            throw new \RuntimeException("未知错误");
        }
    }

    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();
  
        if ($user->isLogin()) {
            return $this->createMessageResponse('info', '你已经登录了', null, 3000, $this->generateUrl('homepage'));
        }

        $registerEnable = $this->getAuthService()->isRegisterEnabled();

        if (!$registerEnable) {
            return $this->createMessageResponse('info', '注册已关闭，请联系管理员', null, 3000, $this->generateUrl('homepage'));
        }

        if ($request->getMethod() == 'POST') {
            $registration = $request->request->all();

            if (isset($registration['emailOrMobile']) && SimpleValidator::mobile($registration['emailOrMobile'])) {
                $registration['verifiedMobile'] = $registration['emailOrMobile'];
            }

            $registration['mobile']    = isset($registration['verifiedMobile']) ? $registration['verifiedMobile'] : '';
            $registration['createdIp'] = $request->getClientIp();
            $authSettings              = $this->getSettingService()->get('auth', array());

            //验证码校验
            $this->captchaEnabledValidator($authSettings, $registration, $request);

            if ($this->registerLimitValidator($registration, $authSettings, $request)) {
                return $this->createMessageResponse('info', '由于您注册次数过多，请稍候尝试');
            }

            $areaCode = isset($registration['areaCode']) ? $registration['areaCode'] : null;
            $userProfile = $this->getUserService()->findUserProfileByIdCardAndAreaCode($registration['idcard'], $areaCode);

            if (!$this->getUserService()->verifyPassword($userProfile['id'], $registration['password'])) {
                return $this->createMessageResponse('info', '密码不匹配，请返回重试');
            }

            $user = $this->getAuthService()->registBatch($userProfile['id'], $registration['verifiedMobile']);

            if (($authSettings
                && isset($authSettings['email_enabled'])
                && $authSettings['email_enabled'] == 'closed')
                || !$this->isEmptyVeryfyMobile($user)) {
                $this->authenticateUser($user);
            }
            
            $goto = $this->generateUrl('register_submited', array(
                'id'   => $user['id'],
                'hash' => $this->makeHash($user),
                'goto' => $this->getTargetPath($request)
            ));
            
            if ($this->getAuthService()->hasPartnerAuth()) {
                $currentUser = $this->getCurrentUser();

                if (!$currentUser->isLogin()) {
                    $this->authenticateUser($user);
                }

                $goto = $this->generateUrl('partner_login', array('goto' => $goto));
            }

            $this->getAuthService()->registBatchAddBranchSchoolInfo($user['id']);

            $this->setCookieWithLongDate('targetUrl', '/my/courses/learning');

            return $this->redirect($this->generateUrl('register_success', array('goto' => $goto)));
        }

        return $this->render("CustomWebBundle:Register:index.html.twig", array(
            'isRegisterEnabled' => $registerEnable,
            'registerSort'      => array(),
            '_target_path'      => $this->getTargetPath($request)
        ));
    }

    protected static function setCookieWithLongDate($name, $value)
    {
        setcookie($name, $value, time()+94608000, '/'); //cookie 有效期 3年, 60*60*24*365*3秒
    }

    public function idcardCheckAction(Request $request)
    {
        $idcard                 = $request->query->get('value');
        list($result, $message) = $this->getAuthService()->checkIdCard($idcard);
        return $this->validateResult($result, $message);
    }

    public function passwordCheckAction(Request $request)
    {
        $idcard                 = $request->query->get('idcard');
        $password               = $request->query->get('value');
        list($result, $message) = $this->getAuthService()->checkRegistPass($idcard, $password);
        return $this->validateResult($result, $message);
    }

    protected function validateResult($result, $message)
    {
        if ($result == 'success') {
            $response = array('success' => true, 'message' => '');
        } else {
            $response = array('success' => false, 'message' => $message);
        }

        return $this->createJsonResponse($response);
    }

    protected function getPassWord()
    {
        $password = substr(md5(time()), 0, 6);
        return $password;
    }

    protected function makeHash($user)
    {
        $string = $user['id'].$user['email'].$this->container->getParameter('secret');
        return md5($string);
    }

    protected function isEmptyVeryfyMobile($user)
    {
        if (isset($user['verifiedMobile']) && !empty($user['verifiedMobile'])) {
            return false;
        }

        return true;
    }

    protected function registerLimitValidator($registration, $authSettings, $request)
    {
        $registration['createdIp'] = $request->getClientIp();

        if (isset($authSettings['register_protective'])) {
            $status = $this->protectiveRule($authSettings['register_protective'], $registration['createdIp']);

            if (!$status) {
                return true;
            }
        }
    }

    protected function captchaEnabledValidator($authSettings, $registration, $request)
    {
        if (array_key_exists('captcha_enabled', $authSettings) && ($authSettings['captcha_enabled'] == 1) && !isset($registration['mobile'])) {
            $captchaCodePostedByUser = strtolower($registration['captcha_code']);
            $captchaCode             = $request->getSession()->get('captcha_code');

            if (!isset($captchaCodePostedByUser) || strlen($captchaCodePostedByUser) < 5) {
                throw new \RuntimeException('验证码错误。');
            }

            if (!isset($captchaCode) || strlen($captchaCode) < 5) {
                throw new \RuntimeException('验证码错误。');
            }

            if ($captchaCode != $captchaCodePostedByUser) {
                $request->getSession()->set('captcha_code', mt_rand(0, 999999999));
                throw new \RuntimeException('验证码错误。');
            }

            $request->getSession()->set('captcha_code', mt_rand(0, 999999999));
        }
    }

    protected function getSensitiveService()
    {
        return $this->getServiceKernel()->createService('SensitiveWord:Sensitive.SensitiveService');
    }

    protected function getAuthService()
    {
        return $this->getServiceKernel()->createService('Custom:User.AuthService');
    }

    protected function getRegisterService()
    {
        return $this->getServiceKernel()->createService('Custom:Register.RegisterService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService("BranchSchool:BranchSchool.BranchSchoolService");
    }

    protected function getChangeMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:User.ChangeMobileService');
    }
}
