<?php

namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\BaseController;
use Custom\Service\FaceDetect\FaceDetectService;
use Custom\Common\Util\CourseLoopUtils;

class CameraController extends BaseController
{
    public function webCameraAction(Request $request)
    {
        if (!$this->getCurrentUser()->isLogin()) {
            return $this->render('TopxiaWebBundle:Default:index.html.twig', array());
        }
        if ($request->getMethod() == 'POST') {
            try {
                $publicUploadDir = $this->container->getParameter('topxia.upload.public_directory');
                $imageData = $request->get('imageData');
                $userId = $this->getCurrentUser()->getId();

                $this->getFaceDetectService()->updateBatchUserFaceImg(
                    $userId,
                    $imageData,
                    $publicUploadDir
                );

                $this->markAsFaceDetected($request);

            } catch (\RuntimeException $e) {
                return $this->createJsonResponse(false);
            }
            return $this->createJsonResponse(true);
        }
        return $this->render('CustomWebBundle:Camera:webCamera.html.twig');
    }
    
    public function faceDetectAction(Request $request)
    {
        $post = $request->query->all();
        $imageData = $request->get('imageData');
        if (isset($post['lessonViewId']) && !empty($post['lessonViewId'])) {
            $faceDetectResult = $this->getFaceDetectResultService()->buildLessonDetectResultWithRequest($post['lessonViewId'], $imageData);
        } elseif (isset($post['testpaperLessonId']) && !empty($post['testpaperLessonId'])) {
            $lesson  = $this->getCourseService()->getLesson($post['testpaperLessonId']);
            $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->getId(), $lesson['courseId']);
            $faceDetectResult = $this->getFaceDetectResultService()->buildTestpaperDetectResultWithRequest(
                    $post['testpaperLessonId'], $imageData, $currentLoopTime);
        } else {
            $faceDetectResult = $this->getFaceDetectResultService()->buildLoginDetectResultWithRequest($imageData);
        }
        $faceDetectResult['userId'] = $this->getCurrentUser()->getId();
        try {
            $userId        = $this->getCurrentUser()->getId();
            $base64ImgData = $request->get('imageData');
            $post          = $request->query->all();

            $isFaceImgValid = $this->getFaceDetectService()->isFaceImgValid(
                $userId, $base64ImgData,$faceDetectResult);

            if ($isFaceImgValid) {
                $this->markAsFaceDetected($request);
                $this->logoutAppIfIsLoginDetect($faceDetectResult);
            }
            return $this->createJsonResponse($isFaceImgValid);
        } catch (\RuntimeException $e) {
            return $this->createJsonResponse(false);
        }
    }

    public static function markAsFaceDetected(Request $request)
    {
        $request->getSession()->set(
            FaceDetectService::SESSION_FACE_DETECTED, true);
    }

    protected function logoutAppIfIsLoginDetect($faceDetectResult)
    {
        if ($faceDetectResult['type'] == 'login') {
            $this->getTokenService()->deleteMobileLoginToken($faceDetectResult['userId']);
        }
    }

    protected function getFaceDetectService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetect.FaceDetectService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getTokenService()
    {
        return $this->getServiceKernel()->createService('Custom:User.TokenService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('User.UserService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }
}
