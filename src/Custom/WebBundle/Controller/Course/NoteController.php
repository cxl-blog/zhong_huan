<?php
namespace Custom\WebBundle\Controller\Course;

use Topxia\Common\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Topxia\WebBundle\Controller\Course\NoteController as BaseNoteController;

class NoteController extends BaseNoteController
{
    public function showListAction(Request $request, $courseId)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $courseId);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}课程，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }
        $lessons = $this->getCourseService()->getCourseLessons($courseId);
        $infos = array(
            'filters' => $this->getNoteSearchFilters($request),
            'lessons' => $lessons,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);
        return $this->render('CustomWebBundle:Course\Note:course-notes-list.html.twig', $headerInfos);
    }

    public function listAction(Request $request, $courseIds, $filters)
    {
        $conditions = $this->convertFiltersToConditions($courseIds, $filters);

        $notes           = array();
        $result['notes'] = $notes;

        if ((isset($conditions['courseIds']) && !empty($conditions['courseIds'])) ||
            (isset($conditions['courseId']) && !empty($conditions['courseId']))) {
            $paginator = new Paginator(
                $request,
                $this->getNoteService()->searchNoteCount($conditions),
                20
            );
            $orderBy = $this->convertFiltersToOrderBy($filters);

            $notes = $this->getNoteService()->searchNotes(
                $conditions,
                $orderBy,
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );

            $result              = $this->makeNotesRelated($notes, $courseIds);
            $result['paginator'] = $paginator;
        }

        return $this->render('CustomWebBundle:Course\Note:notes-list.html.twig', $result);
    }
}
