<?php
namespace Custom\WebBundle\Controller\Course;

use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Custom\WebBundle\Controller\Course\NoteController as BaseNoteController;

class CertificationNoteController extends BaseNoteController
{
    public function showListAction(Request $request, $courseId)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $courseId);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}课程，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }

        $lessons = $this->getCourseService()->getCourseLessons($courseId);
        $infos = array(
            'filters' => $this->getNoteSearchFilters($request),
            'lessons' => $lessons,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        return $this->render('CustomWebBundle:Course\Note:course-notes-list.html.twig', $headerInfos);
    }
}
