<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\SimpleValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\WebBundle\Controller\CourseStudentManageController as BaseController;

class CourseStudentManageController extends BaseController
{
    public function indexAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        $fields    = $request->query->all();
        $condition = array();

        if (isset($fields['keyword']) && !empty($fields['keyword'])) {
            if (SimpleValidator::email($fields['keyword'])) {
                $condition['email'] = $fields['keyword'];
                $user               = $this->getUserService()->getUserByEmail($condition['email']);

                $condition['userId'] = $user ? $user['id'] : -1;
                unset($condition['email']);
            } elseif (SimpleValidator::mobile($fields['keyword'])) {
                $condition['mobile'] = $fields['keyword'];
                $userIds             = array();
                $mobileVerifiedUser  = $this->getUserService()->getUserByVerifiedMobile($condition['mobile']);
                $profileUsers        = $this->getUserService()->searchUserProfiles(array('tel' => $condition['mobile']), array('id', 'DESC'), 0, PHP_INT_MAX);
                $mobileNameUser      = $this->getUserService()->getUserByNickname($condition['mobile']);
                $userIds             = $profileUsers ? ArrayToolkit::column($profileUsers, 'id') : null;

                $userIds[] = $mobileVerifiedUser ? $mobileVerifiedUser['id'] : null;
                $userIds[] = $mobileNameUser ? $mobileNameUser['id'] : null;

                $userIds = array_unique($userIds);

                $condition['userIds'] = $userIds ? $userIds : -1;
                unset($condition['mobile']);
            } else {
                $condition['nickname'] = $fields['keyword'];
                $user                  = $this->getUserService()->getUserByNickname($condition['nickname']);
                $condition['userId']   = $user ? $user['id'] : -1;
                unset($condition['nickname']);
            }
        }

        $condition = array_merge($condition, array('courseId' => $course['id'], 'role' => 'student'));

        $paginator = new Paginator(
            $request,
            $this->getCourseService()->searchMemberCount($condition),
            20
        );

        $students = $this->getCourseService()->searchMembers(
            $condition,
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $studentUserIds = ArrayToolkit::column($students, 'userId');
        $users          = $this->getUserService()->findUsersByIds($studentUserIds);
        $followingIds   = $this->getUserService()->filterFollowingIds($this->getCurrentUser()->id, $studentUserIds);

        $progresses = array();

        foreach ($students as $student) {
            $progresses[$student['userId']] = $this->calculateUserLearnProgress($course, $student);
        }

        $courseSetting              = $this->getSettingService()->get('course', array());
        $isTeacherAuthManageStudent = !empty($courseSetting['teacher_manage_student']) ? 1 : 0;
        $default                    = $this->getSettingService()->get('default', array());
        $isBranchAdmin              = $this->getCourseService()->canBranchAdminMangeCourse($id) ? true : false;        

        return $this->render('CustomWebBundle:CourseStudentManage:index.html.twig', array(
            'course'                     => $course,
            'students'                   => $students,
            'users'                      => $users,
            'progresses'                 => $progresses,
            'followingIds'               => $followingIds,
            'isTeacherAuthManageStudent' => $isTeacherAuthManageStudent,
            'paginator'                  => $paginator,
            'canManage'                  => $this->getCourseService()->canManageCourse($course['id']),
            'default'                    => $default,
            'isBranchAdmin'              => $isBranchAdmin
        ));
    }

    public function importAction($id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);
        return $this->render('CustomWebBundle:CourseStudentManage:normal-course-import.html.twig', array(
            'course' => $course
        ));
    }
}
