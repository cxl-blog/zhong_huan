<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Service\Util\EdusohoLiveClient;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Topxia\WebBundle\Controller\CourseController as BaseCourceController;

class CourseController extends BaseCourceController
{
    public function exploreAction(Request $request, $category)
    {
        $conditions    = $request->query->all();
        $categoryArray = array();
        $levels        = array();

        $conditions['code'] = $category;
        $conditions['schoolId'] = 0;

        if (!empty($conditions['code'])) {
            $categoryArray             = $this->getCategoryService()->getCategoryByCode($category);
            $childrenIds               = $this->getCategoryService()->findCategoryChildrenIds($categoryArray['id']);
            $categoryIds               = array_merge($childrenIds, array($categoryArray['id']));
            $conditions['categoryIds'] = $categoryIds;
        }

        unset($conditions['code']);

        if (!isset($conditions['fliter'])) {
            $conditions['fliter'] = array('type' => 'all', 'price' => 'all', 'currentLevelId' => 'all');
        }

        $fliter = $conditions['fliter'];

        if ($fliter['price'] == 'free') {
            $coinSetting = $this->getSettingService()->get("coin");
            $coinEnable  = isset($coinSetting["coin_enabled"]) && $coinSetting["coin_enabled"] == 1;
            $priceType   = "RMB";

            if ($coinEnable && !empty($coinSetting) && array_key_exists("price_type", $coinSetting)) {
                $priceType = $coinSetting["price_type"];
            }

            $conditions['price'] = '0.00';
        }

        if ($fliter['type'] == 'live') {
            $conditions['type'] = 'live';
        }

        if ($this->isPluginInstalled('Vip')) {
            $levels = ArrayToolkit::index($this->getLevelService()->searchLevels(array('enabled' => 1), 0, 100), 'id');

            if ($fliter['currentLevelId'] != 'all') {
                $vipLevelIds               = ArrayToolkit::column($this->getLevelService()->findPrevEnabledLevels($fliter['currentLevelId']), 'id');
                $conditions['vipLevelIds'] = array_merge(array($fliter['currentLevelId']), $vipLevelIds);
            }
        }

        unset($conditions['fliter']);

        $courseSetting = $this->getSettingService()->get('course', array());

        if (!isset($courseSetting['explore_default_orderBy'])) {
            $courseSetting['explore_default_orderBy'] = 'latest';
        }

        $orderBy = $courseSetting['explore_default_orderBy'];
        $orderBy = empty($conditions['orderBy']) ? $orderBy : $conditions['orderBy'];
        unset($conditions['orderBy']);

        $conditions['parentId'] = 0;
        $conditions['status']   = 'published';

        $paginator = new Paginator(
            $this->get('request'), $this->getCourseService()->searchCourseCount($conditions), 20
        );

        if ($orderBy != 'recommendedSeq') {
            $courses = $this->getCourseService()->searchCourses(
                $conditions, $orderBy,
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
        }

        if ($orderBy == 'recommendedSeq') {
            $conditions['recommended'] = 1;
            $recommendCount            = $this->getCourseService()->searchCourseCount($conditions);
            $currentPage               = $request->query->get('page') ? $request->query->get('page') : 1;
            $recommendPage             = intval($recommendCount / 20);
            $recommendLeft             = $recommendCount % 20;

            if ($currentPage <= $recommendPage) {
                $courses = $this->getCourseService()->searchCourses(
                    $conditions,
                    $orderBy,
                    ($currentPage - 1) * 20,
                    20
                );
            } elseif (($recommendPage + 1) == $currentPage) {
                $courses = $this->getCourseService()->searchCourses(
                    $conditions,
                    $orderBy,
                    ($currentPage - 1) * 20,
                    20
                );
                $conditions['recommended'] = 0;
                $coursesTemp               = $this->getCourseService()->searchCourses(
                    $conditions,
                    'createdTime',
                    0,
                    20 - $recommendLeft
                );
                $courses = array_merge($courses, $coursesTemp);
            } else {
                $conditions['recommended'] = 0;
                $courses                   = $this->getCourseService()->searchCourses(
                    $conditions,
                    'createdTime',
                    (20 - $recommendLeft) + ($currentPage - $recommendPage - 2) * 20,
                    20
                );
            }
        }

        $group = $this->getCategoryService()->getGroupByCode('course');

        if (empty($group)) {
            $categories = array();
        } else {
            $categories = $this->getCategoryService()->getCategoryTree($group['id']);
        }

        if (!$categoryArray) {
            $categoryArrayDescription = array();
        } else {
            $categoryArrayDescription = $categoryArray['description'];
            $categoryArrayDescription = strip_tags($categoryArrayDescription, '');
            $categoryArrayDescription = preg_replace("/ /", "", $categoryArrayDescription);
            $categoryArrayDescription = substr($categoryArrayDescription, 0, 100);
        }

        if (!$categoryArray) {
            $categoryParent = '';
        } else {
            if (!$categoryArray['parentId']) {
                $categoryParent = '';
            } else {
                $categoryParent = $this->getCategoryService()->getCategory($categoryArray['parentId']);
            }
        }

        return $this->render('TopxiaWebBundle:Course:explore.html.twig', array(
            'courses'                  => $courses,
            'category'                 => $category,
            'fliter'                   => $fliter,
            'orderBy'                  => $orderBy,
            'paginator'                => $paginator,
            'categories'               => $categories,
            'consultDisplay'           => true,
            'path'                     => 'course_explore',
            'categoryArray'            => $categoryArray,
            'group'                    => $group,
            'categoryArrayDescription' => $categoryArrayDescription,
            'categoryParent'           => $categoryParent,
            'levels'                   => $levels
        ));
    }

    public function createAction(Request $request)
    {
        $user        = $this->getUserService()->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        $isLive = $request->query->get('flag');
        $type   = ($isLive == "isLive") ? 'live' : 'normal';

        if ($type == 'live') {
            $courseSetting = $this->setting('course', array());

            if (empty($courseSetting['live_course_enabled'])) {
                return $this->createMessageResponse('info', '请前往后台开启直播,尝试创建！');
            }

            if (!empty($courseSetting['live_course_enabled'])) {
                $client   = new EdusohoLiveClient();
                $capacity = $client->getCapacity();
            } else {
                $capacity = array();
            }

            if (empty($capacity['capacity']) && !empty($courseSetting['live_course_enabled'])) {
                return $this->createMessageResponse('info', '请联系EduSoho官方购买直播教室，然后才能开启直播功能！');
            }
        }

        if (false === $this->get('security.context')->isGranted('ROLE_TEACHER')) {
            throw $this->createAccessDeniedException();
        }

        if ($request->getMethod() == 'POST') {
            $course = $request->request->all();
            $course = $this->getCourseService()->createCourse($course);

            return $this->redirect($this->generateUrl('course_manage', array('id' => $course['id'])));
        }

        return $this->render('CustomWebBundle:Course:create.html.twig', array(
            'userProfile' => $userProfile,
            'type'        => $type
        ));
    }

    public function showAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }

        if (empty($member)) {
            $user   = $this->getCurrentUser();
            $member = $this->getCourseService()->becomeStudentByClassroomJoined($id, $user->id);

            if (isset($member["id"])) {
                $course['studentNum']++;
            }
        }

        $this->getCourseService()->hitCourse($id);

        $items = $this->getCourseService()->getCourseItems($course['id']);

        if ($course['type'] == 'normal' || $course['type'] == 'certification') {
            $renderPage = "CustomWebBundle:Course:{$course['type']}-show.html.twig";
        } elseif ($course['type'] == 'live') {
            $renderPage = "TopxiaWebBundle:Course:{$course['type']}-show.html.twig";
        }

        $infos = array(
            'member' => $member,
            'items'  => $items
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render($renderPage, $headerInfos);
    }

    public function LessonListAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }
        $infos = array(
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render('CustomWebBundle:Course:normal-lesson-list.html.twig', $headerInfos);
    }


    public function coursesBlockAction($courses, $view = 'list', $mode = 'default')
    {
        $userIds = array();

        foreach ($courses as $key => $course) {
            $userIds = array_merge($userIds, $course['teacherIds']);
        }

        $users = $this->getUserService()->findUsersByIds($userIds);

        return $this->render("CustomWebBundle:Course:courses-block-{$view}.html.twig", array(
            'courses' => $courses,
            'users'   => $users,
            'mode'    => $mode
        ));
    }

    public function learnAction(Request $request, $id)
    {
        $user      = $this->getCurrentUser();
        $starttime = $request->query->get('starttime', '');

        if (!$user->isLogin()) {
            $request->getSession()->set('_target_path', $this->generateUrl('course_show', array('id' => $id)));

            return $this->createMessageResponse('info', '你好像忘了登录哦？', null, 3000, $this->generateUrl('login'));
        }

        $course = $this->getCourseService()->getCourse($id);

        if (empty($course)) {
            throw $this->createNotFoundException("课程不存在，或已删除。");
        }

        if ($course['approval'] == 1 && ($user['approvalStatus'] != 'approved')) {
            return $this->createMessageResponse('info', "该课程需要通过实名认证，你还没有通过实名认证。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);

        if (!$this->getCourseService()->canTakeCourse($id, $currentLoopTime)) {
            return $this->createMessageResponse('info', "您还不是课程《{$course['title']}》的学员，请先购买或加入学习。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        try {
            list($course, $member) = $this->getCourseService()->tryTakeCourse($id, $currentLoopTime);

            if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
                return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
            }

            if ($member && $member['levelId'] > 0) {
                if ($this->getVipService()->checkUserInMemberLevel($member['userId'], $course['vipLevelId']) != 'ok') {
                    return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
                }
            }
        } catch (Exception $e) {
            throw $this->createAccessDeniedException('抱歉，未发布课程不能学习！');
        }

        return $this->render('CustomWebBundle:Course:learn.html.twig', array(
            'course'    => $course,
            'starttime' => $starttime
        ));
    }

    public function searchAction(Request $request, $courseId)
    {
        $this->getCourseService()->tryManageCourse($courseId);
        $key = $request->request->get("key");

        $conditions             = array("title" => $key);
        $conditions['status']   = 'published';
        $conditions['type']     = "certification";
        $conditions['parentId'] = 0;

        $courses = $this->getCourseService()->searchCourses(
            $conditions,
            'latest',
            0,
            5
        );

        $courseIds = ArrayToolkit::column($courses, 'id');

        $userIds = array();

        foreach ($courses as &$course) {
            $course['tags'] = $this->getTagService()->findTagsByIds($course['tags']);
            $userIds        = array_merge($userIds, $course['teacherIds']);
        }

        $users = $this->getUserService()->findUsersByIds($userIds);

        return $this->render('TopxiaWebBundle:Course:course-select-list.html.twig', array(
            'users'   => $users,
            'courses' => $courses
        ));
    }

    public function pickAction(Request $request, $courseId)
    {
        $this->getCourseService()->tryManageCourse($courseId);

        $conditions = array(
            'status'   => 'published',
            'parentId' => 0,
            'type'     => 'certification'

        );

        $paginator = new Paginator(
            $this->get('request'),
            $this->getCourseService()->searchCourseCount($conditions),
            5
        );

        $courses = $this->getCourseService()->searchCourses(
            $conditions,
            'latest',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $courseIds = ArrayToolkit::column($courses, 'id');
        $userIds   = array();

        foreach ($courses as &$course) {
            $course['tags'] = $this->getTagService()->findTagsByIds($course['tags']);
            $userIds        = array_merge($userIds, $course['teacherIds']);
        }

        $users = $this->getUserService()->findUsersByIds($userIds);

        return $this->render("CustomWebBundle:CourseLessonManage/Course:course-pick-modal.html.twig", array(
            'users'     => $users,
            'courses'   => $courses,
            'courseId'  => $courseId,
            'paginator' => $paginator
        ));
    }

    public function singleNormalCourseGridAction($course, $mode)
    {
        return $this->render("CustomWebBundle:Course:normalCourseGrid_single.html.twig", array(
            'course' => $course,
            'mode'   => $mode

        ));
    }

    public function infoAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }

        $category = $this->getCategoryService()->getCategory($course['categoryId']);
        $tags     = $this->getTagService()->findTagsByIds($course['tags']);
        $infos = array(
            'category' => $category,
            'tags' => $tags,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render('CustomWebBundle:Course:info.html.twig', $headerInfos);
    }
}
