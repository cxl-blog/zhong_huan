<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseLessonController as BaseController;

class CertificationCourseLessonController extends BaseController
{
    public function listAction(Request $request, $courseId, $member, $previewUrl, $mode = 'full', $certificationCourse = null)
    {
        $user    = $this->getCurrentUser();
        $isLogin = $user->isLogin();
        $isAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']);

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId);
        $items  = $this->getCourseService()->getCourseItems($courseId);
        //$learnStatuses   = $this->getCourseService()->getUserLearnLessonStatuses($user['id'], $courseId, $currentLoopTime, $items);
        $learnStatuses = $this->getCourseService()->getLessonStatus($user['id'], $courseId, $currentLoopTime, $items);
        $course = $this->getCourseService()->getCourse($courseId);
        $order  = $this->getOrderService()->getOrder($member['orderId']);

        $nextLesson = null;
        if (!$isAdmin && $user['id'] != 0) {
            if (empty($certificationCourse)) {
                $certificationCourse = $this->getCertificateService()->getCertificationsForUser($user['id'], false, $course['id'], $currentLoopTime);
            }
            $nextLesson    = $this->getCourseService()->getUserValideNextLearnLesson($user['id'], $courseId, $currentLoopTime);
        } 

        $suggestHoursForAdmin = null;
        if ($isAdmin) {
            $suggestHoursForAdmin = $this->getCourseService()->getTextSuggestedHoursForAdmin($items, $user['id']);
            $nextLesson    = $this->getCourseService()->getUserValideNextLearnLesson($user['id'], $courseId, $currentLoopTime);
        }
        
        $homeworkPlugin       = $this->getAppService()->findInstallApp('Homework');
        $homeworkLessonIds    = array();
        $exercisesLessonIds   = array();
        $suggestHours         = $this->getCourseService()->getTextSuggestedHours($items, $user['id'], $currentLoopTime);
        $unitHours            = $this->getCourseService()->getUnitSuggestedHours($items, $isAdmin, $user['id'], $currentLoopTime);
        $chapterHours         = $this->getCourseService()->getChapterSuggestedHours($items, $isAdmin, $user['id']);

        if ($homeworkPlugin) {
            $lessonIds          = ArrayToolkit::column($items, 'id');
            $homeworks          = $this->getHomeworkService()->findHomeworksByCourseIdAndLessonIds($course['id'], $lessonIds);
            $exercises          = $this->getExerciseService()->findExercisesByLessonIds($lessonIds);
            $homeworkLessonIds  = ArrayToolkit::column($homeworks, 'lessonId');
            $exercisesLessonIds = ArrayToolkit::column($exercises, 'lessonId');
        }

        if ($this->setting('magic.lesson_watch_limit')) {
            $lessonLearns = $this->getCourseService()->findUserLearnedLessons($user['id'], $courseId, $currentLoopTime);
        } else {
            $lessonLearns = array();
        }

        $certificationLessonLearns = $this->findCertificationLessonLearnsByUserIdAndCourseId($user['id'], $courseId, $currentLoopTime);
        $lessonDetectCountGroups   = $this->findLessonDetectCountGroupsByUserIdAndLessonLearns($user['id'], $courseId, $certificationLessonLearns, $currentLoopTime);
        $testpaperIds              = array();
        array_walk($items, function ($item, $key) use (&$testpaperIds) {
            if ($item['type'] == 'testpaper') {
                array_push($testpaperIds, $item['mediaId']);
            }
        }
        );
        
        $testpapers    = $this->getTestpaperService()->findTestpapersByIds($testpaperIds);
        $watchTimeList = $this->getCourseService()->getSumWatchTimeGroupByLesson($user['id'], $courseId, $currentLoopTime);
        $watchTimeList = ArrayToolkit::index($watchTimeList, 'lessonId');
        $areaCertification = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'], $course['certificationId']);
        return $this->Render('CustomWebBundle:CourseLesson/Widget:list.html.twig', array(
            'unitHours'                 => $unitHours,
            'chapterHours'              => $chapterHours,
            'items'                     => $items,
            'course'                    => $course,
            'member'                    => $member,
            'previewUrl'                => $previewUrl,
            'learnStatuses'             => $learnStatuses,
            'lessonLearns'              => $lessonLearns,
            'currentTime'               => time(),
            'homeworkLessonIds'         => $homeworkLessonIds,
            'exercisesLessonIds'        => $exercisesLessonIds,
            'mode'                      => $mode,
            'testpapers'                => $testpapers,
            'suggestHours'              => $suggestHours,
            'isAdmin'                   => $isAdmin,
            'suggestHoursForAdmin'      => $suggestHoursForAdmin,
            'certificationCourse'       => $certificationCourse,
            'certificationLessonLearns' => $certificationLessonLearns,
            'lessonDetectCountGroups'   => $lessonDetectCountGroups,
            'showStates'                => true,
            'nextLessonId'              => !empty($nextLesson) ? $nextLesson['id'] : 0,
            'orderStatus'               => empty($order['status']) ? "" : $order['status'],
            'isLogin'                   => $isLogin,
            'watchTimeList'             => $watchTimeList,
            'areaCertification'   => $areaCertification
        ));
    }

    public function lessonViewListAction(Request $request, $lessonId)
    {
        $userId                        = $this->getCurrentUser()->id;
        $lesson                        = $this->getCourseService()->getLesson($lessonId);
        $currentLoopTime               = CourseLoopUtils::getCurrentLoopTime($request, $userId, $lesson['courseId']);
        $conditions['userId']          = $userId;
        $conditions['lessonId']        = $lessonId;
        $conditions['currentLoopTime'] = $currentLoopTime;
        $conditions['ignoredWatchTime'] = 0;
        $count                         = $this->getCourseService()->searchLessonViewCount($conditions);
        $paginator                     = new Paginator(
            $this->get('request'),
            $count,
            10
        );

        $lessonViews = $this->getCourseService()->searchLessonView(
            $conditions,
            array(
                'id',
                'desc'
            ),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        ); 
        $count = $count - $paginator->getOffsetCount();
        return $this->Render('CustomWebBundle:CourseLesson/Part:lessonView-table.html.twig', array(
            'lessonViews' => $lessonViews,
            'paginator'   => $paginator,
            'lessonId'    => $lessonId,
            'count'       => $count
        ));
    }

    public function findCertificationLessonLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime = null)
    {
        $certificationLessonLearns = $this->getCourseService()->findUserLearnedLessons($userId, $courseId, $currentLoopTime);
        $certificationLessonLearns = ArrayToolkit::index($certificationLessonLearns, 'lessonId');
        return $certificationLessonLearns;
    }

    public function findLessonDetectCountGroupsByUserIdAndLessonLearns($userId, $courseId, $lessonLearns, $currentLoopTime = null)
    {
        $lessonViews = $this->getCourseService()->findLessonViewsByUserIdAndCourseId($userId, $courseId, $currentLoopTime);

        if (empty($lessonViews)) {
            return array();
        }

        $lessonViewIds           = ArrayToolkit::column($lessonViews, 'id');
        $lessonDetectCountGroups = $this->getFaceDetectResultService()->findLessonCountGroupsByUserIdAndLessonViewIds($userId, $lessonViewIds, $currentLoopTime);
        $lessonDetectCountGroups = $lessonDetectCountGroups ? ArrayToolkit::index($lessonDetectCountGroups, 'lessonId') : array();
        return $lessonDetectCountGroups;
    }

    public function learnStatusAction(Request $request, $courseId, $lessonId)
    {
        $user   = $this->getCurrentUser();
        $status = $this->getCourseService()->getUserLearnLessonStatus($user['id'], $courseId, $lessonId);

        return $this->createJsonResponse(array('status' => $status ?: 'unstart'));
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getOrderService()
    {
        return $this->getServiceKernel()->createService('Order.OrderService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
