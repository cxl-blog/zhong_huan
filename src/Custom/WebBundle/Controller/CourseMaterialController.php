<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseMaterialController as BaseCourseMaterialController;

class CourseMaterialController extends BaseCourseMaterialController
{
    public function indexAction(Request $request, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $id);

        if ($response) {
            return $response;
        }

        $user = $this->getCurrentUser();
        $paginator = new Paginator(
            $request,
            $this->getMaterialService()->getMaterialCount($id),
            20
        );

        $materials = $this->getMaterialService()->findCourseMaterials(
            $id,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $lessons = $this->getCourseService()->getCourseLessons($course['id']);
        $lessons = ArrayToolkit::index($lessons, 'id');

        $infos = array(
            'lessons'   => $lessons,
            'materials' => $materials,
            'paginator' => $paginator,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render("CustomWebBundle:CourseMaterial:index.html.twig", $headerInfos);
    }

    protected function buildLayoutDataWithTakenAccess($request, $id)
    {
        $user                  = $this->getCurrentUser();
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id, $currentLoopTime);
        $response              = null;

        if (!$user->isLogin()) {
            $response = $this->createMessageResponse('info', '你好像忘了登录哦？', null, 3000, $this->generateUrl('login'));
        }

        $course['currentLoopTime'] = $currentLoopTime;

        if (!$this->getCourseService()->canTakeCourse($course, $currentLoopTime) && !$this->getCourseService()->canBranchAdminMangeCourse($course['id'])) {
            $response = $this->createMessageResponse('info', "您还不是课程《{$course['title']}》的学员，请先购买或加入学习。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        return array($course, $member, $response);
    }
}
