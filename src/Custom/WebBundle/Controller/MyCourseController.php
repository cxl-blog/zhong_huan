<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Custom\Common\Constants\LearnStatus;
use Custom\Common\Util\CourseGroupUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\MyCourseController as TopxiaWebController;

class MyCourseController extends TopxiaWebController
{
    public function learningAction(Request $request)
    {
        $userId = $this->getCurrentUser()['id'];
        $allNormalCourses = $this->getCourseService()->findUserLeaningCourses(
            $userId,
            0,
            PHP_INT_MAX,
            array('type' => 'normal')
        );
        $groupedParams = $this->getGroupedCourses($userId,
            $allNormalCourses, 
            array(
                LearnStatus::LEARNING,
                LearnStatus::MAKEUP,
                LearnStatus::FINISHED
            ),
            array(
                LearnStatus::LEARNING,
                LearnStatus::MAKEUP,
                LearnStatus::FINISHED
            )
        );
        $groupedParams['userId'] = $userId;
        $groupedParams['isApprovaled'] = $this->getUserService()->isUserApprovaled($userId);
        return $this->render('CustomWebBundle:MyCourse:learning.html.twig', $groupedParams);
    }

    public function learnedAction(Request $request)
    {
        $userId     = $this->getCurrentUser()['id'];
        
        $isGranted  = array();
        $allCourses = $this->getCourseService()->findUserLeanedCourses(
            $userId,
            0,
            PHP_INT_MAX
        );

        $allNormalCourses = array();

        foreach ($allCourses as $key => $course) {
            if ($course['type'] != 'certification') {
                array_push($allNormalCourses, $course);
            }
        }

        $groupedParams = $this->getGroupedCourses($userId,
            $allNormalCourses, array(LearnStatus::COMPLETION));

        $courses = $groupedParams['courses'];
        $userIds = array();

        foreach ($courses as $key => $course) {
            $userIds   = array_merge($userIds, $course['teacherIds']);
            $learnTime = $this->getCourseService()->searchLearnTime(array('courseId' => $course['id'], 'userId' => $userId));

            $courses[$key]['learnTime'] = intval($learnTime / 60 / 60)."小时".($learnTime / 60 % 60)."分钟";
        }

        $groupedParams['courses'] = $courses;
        $users                    = $this->getUserService()->findUsersByIds($userIds);
        $groupedParams['users']   = $users;
        $groupedParams['userId'] = $userId;
        $groupedParams['isApprovaled'] = $this->getUserService()->isUserApprovaled($userId);
        return $this->render('CustomWebBundle:MyCourse:learned.html.twig', $groupedParams);
    }

    public function favoritedAction(Request $request)
    {
        $userId = $this->getCurrentUser()['id'];
        
        $courses = $this->getCourseService()->findUserFavoritedCourses(
            $userId, 0, PHP_INT_MAX
        );
        $generatedCourses = CourseGroupUtils::generateNormalAndCertificationCourse(
                $courses, $userId, $this->getCertificationService(), true);

        $allCertificationCourses = $this->getCertificationService()->getCertificationsForUser($userId);
        $groupedParams = $this->generateCourseParams($allCertificationCourses,
            $generatedCourses['groupedCertificationCourses'], $generatedCourses['allNormalCourses']);

        $favoriteCourses = $groupedParams['courses'];
        $userIds         = array();

        foreach ($favoriteCourses as $favoriteCourse) {
            $userIds = array_merge($userIds, $favoriteCourse['teacherIds']);
        }

        $users                  = $this->getUserService()->findUsersByIds($userIds);
        $groupedParams['users'] = $users;
        $groupedParams['userId'] = $userId;
        $groupedParams['isApprovaled'] = $this->getUserService()->isUserApprovaled($userId);

        return $this->render('CustomWebBundle:MyCourse:favorited.html.twig', $groupedParams);
    }

    public function overtimeAction(Request $request)
    {
        $userId = $this->getCurrentUser()['id'];
        
        $groupedParams = $this->getGroupedCourses($userId, array(),
            array(
                LearnStatus::OVERTIME_MAKEUP_NO,
                LearnStatus::OVERTIME_MAKEUP_YES
            ),
            array(),
            array(
                LearnStatus::LEARNING,
                LearnStatus::MAKEUP,
                LearnStatus::FINISHED
            )
        );
        $groupedParams['userId'] = $userId;
        $groupedParams['isApprovaled'] = $this->getUserService()->isUserApprovaled($userId);

        return $this->render('CustomWebBundle:MyCourse:overtime.html.twig', $groupedParams);
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }

    /*
     * 分组, 先显示证书课程, 再显示普通课程, 支持分页
     * @param $learnStatus 学习状态, 见 LearnStatus.php
     * @param ignoredRevokedStatusArr 如果课程是吊销的且状态为ignoredRevokedStatusArr中任意一个, 则忽略
     * @param addedRevokedStatusArr 如果课程是吊销的且状态为iaddedRevokedStatusArr中任意一个, 则加入
     * @return
     * array(
    'courses'   => $courses,        //混合课程, 符合 $learnStatus 的证书课程 和 普通课程
    'paginator' => $paginator,
    'unpaidCertificationCourses' => $unpaidCertificationCourses  //待学证书课程
    )
     *
     */
    private function getGroupedCourses($userId, $allNormalCourses, $learnStatusArr, 
            $ignoredRevokedStatusArr = array(), $addedRevokedStatusArr = array())
    {
        return CourseGroupUtils::getGroupedCourses($userId, $allNormalCourses, $learnStatusArr,
            array(
                'certificateService' => $this->getCertificationService(),
                'request'               => $this->get('request'),
                'ignoredRevokedStatusArr' => $ignoredRevokedStatusArr,
                'addedRevokedStatusArr'   => $addedRevokedStatusArr
            )
        );
    }

    private function generateCourseParams($allCertificationCourses,
        $groupedCertificationCourses, $allNormalCourses) {
        return CourseGroupUtils::generateCourseParams($allCertificationCourses,
                $groupedCertificationCourses, $allNormalCourses,
                array(
                    'certificateService' => $this->getCertificationService(),
                    'request'               => $this->get('request'),
                )
        );
    }
}