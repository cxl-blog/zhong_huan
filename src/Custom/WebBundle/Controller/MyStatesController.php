<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\AnalysisController as BaseController;

class MyStatesController extends BaseController
{
    public function statesAction(Request $request, $tab)
    {
        $condition                     = $request->query->all();
        $condition['analysisDateType'] = 'login';
        $timeRange                     = $this->getTimeRange($condition);
        $dataInfo                      = $this->getDataInfo($condition, $timeRange);

        $data             = array();
        $loginCountsDatas = "";
        $user             = $this->getCurrentUser();

        $loginPaginator = new Paginator(
            $request,
            $this->getLogService()->searchLogCount(array('userId' => $user['id'], 'action' => "login_success", 'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']), 'endDateTime' => date("Y-m-d H:i:s", $timeRange['endTime']))),
            12
        );

        $loginDetail = $this->getLogService()->searchLogs(array('userId' => $user['id'], 'action' => "login_success", 'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']), 'endDateTime' => date("Y-m-d H:i:s", $timeRange['endTime'])),
            'created',
            $loginPaginator->getOffsetCount(),
            $loginPaginator->getPerPageCount()
        );

        if ($tab == 'trend') {
            $loginCountsDatas = $this->getLogService()->analysisLoginDataByTimeWithUserId($timeRange['startTime'], $timeRange['endTime'], $user['id']);

            $data = $this->fillAnalysisData($condition, $loginCountsDatas);
        }

        $faceDetectPaginator = new Paginator(
            $request,
            $this->getFaceDetectResultService()->searchFaceDetectResultsCount(
                array(
                    'userId'        => $user['id'],
                    'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']),
                    'endDateTime'   => date("Y-m-d H:i:s", $timeRange['endTime'])
                )
            ),
            12
        );
        $faceDetectResults = $this->getFaceDetectResultService()->searchFaceDetectResults(
            array(
                'userId'        => $user['id'],
                'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']),
                'endDateTime'   => date("Y-m-d H:i:s", $timeRange['endTime'])
            ),
            'created',
            $faceDetectPaginator->getOffsetCount(),
            $faceDetectPaginator->getPerPageCount()
        );

        return $this->render('CustomWebBundle:MyCourse:states.html.twig',
            array(
                'user'                => $user,
                'dataInfo'            => $dataInfo,
                'data'                => $data,
                'tab'                 => $tab,
                'loginDetail'         => $loginDetail,
                'loginPaginator'      => $loginPaginator,
                'faceDetectResults'   => $faceDetectResults,
                'faceDetectPaginator' => $faceDetectPaginator

            )
        );
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('Custom:System.LogService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }
}
