<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\SimpleValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\WebBundle\Controller\CourseStudentManageController as BaseCourseStudentManagerController;

class CertificationCourseStudentManageController extends BaseCourseStudentManagerController
{
    public function indexAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        $fields    = $request->query->all();
        $condition = array();

        if (isset($fields['keyword']) && !empty($fields['keyword'])) {
            if (SimpleValidator::email($fields['keyword'])) {
                $condition['email'] = $fields['keyword'];
                $user               = $this->getUserService()->getUserByEmail($condition['email']);

                $condition['userId'] = $user ? $user['id'] : -1;
                unset($condition['email']);
            } elseif (SimpleValidator::mobile($fields['keyword'])) {
                $condition['mobile'] = $fields['keyword'];
                $userIds             = array();
                $mobileVerifiedUser  = $this->getUserService()->getUserByVerifiedMobile($condition['mobile']);
                $profileUsers        = $this->getUserService()->searchUserProfiles(array('tel' => $condition['mobile']), array('id', 'DESC'), 0, PHP_INT_MAX);
                $mobileNameUser      = $this->getUserService()->getUserByNickname($condition['mobile']);
                $userIds             = $profileUsers ? ArrayToolkit::column($profileUsers, 'id') : null;

                $userIds[] = $mobileVerifiedUser ? $mobileVerifiedUser['id'] : null;
                $userIds[] = $mobileNameUser ? $mobileNameUser['id'] : null;

                $userIds = array_unique($userIds);

                $condition['userIds'] = $userIds ? $userIds : -1;
                unset($condition['mobile']);
            } else {
                $condition['nickname'] = $fields['keyword'];
                $user                  = $this->getUserService()->getUserByNickname($condition['nickname']);
                $condition['userId']   = $user ? $user['id'] : -1;
                unset($condition['nickname']);
            }
        }

        $condition = array_merge($condition, array('courseId' => $course['id'], 'role' => 'student'));

        $paginator = new Paginator(
            $request,
            $this->getCourseService()->searchMemberCount($condition),
            20
        );

        $students = $this->getCourseService()->searchMembers(
            $condition,
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $studentUserIds = ArrayToolkit::column($students, 'userId');
        $users          = $this->getUserService()->findUsersByIds($studentUserIds);
        $followingIds   = $this->getUserService()->filterFollowingIds($this->getCurrentUser()->id, $studentUserIds);

        $progresses = array();

        foreach ($students as $student) {
            $progresses[$student['userId']] = $this->calculateUserLearnProgress($course, $student);
        }

        $courseSetting              = $this->getSettingService()->get('course', array());
        $isTeacherAuthManageStudent = !empty($courseSetting['teacher_manage_student']) ? 1 : 0;
        $default                    = $this->getSettingService()->get('default', array());
        return $this->render('CustomWebBundle:CourseStudentManage:index-certificate.html.twig', array(
            'course'                     => $course,
            'students'                   => $students,
            'users'                      => $users,
            'progresses'                 => $progresses,
            'followingIds'               => $followingIds,
            'isTeacherAuthManageStudent' => $isTeacherAuthManageStudent,
            'paginator'                  => $paginator,
            'canManage'                  => $this->getCourseService()->canManageCourse($course['id']),
            'default'                    => $default
        ));
    }

    public function importAction($id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);
        return $this->render('CustomWebBundle:CourseStudentManage:import.html.twig', array(
            'course' => $course
        ));
    }

    public function excelDataImportAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($course['status'] != 'published') {
            throw $this->createNotFoundException("未发布课程不能导入学员!");
        }
        if($course['type']=='certification')
        {
            $targetType = 'certificateCourse';
        }else{
            $targetType = 'course';
        }
        return $this->forward('CustomWebBundle:Importer:importExcelData', array(
            'request'    => $request,
            'targetId'   => $id,
            'targetType' => $targetType
        ));
    }

    public function createAction(Request $request, $id)
    {
        $courseSetting = $this->getSettingService()->get('course', array());

        if (!empty($courseSetting['teacher_manage_student'])) {
            $course = $this->getCourseService()->tryManageCourse($id);
        } else {
            $course = $this->getCourseService()->tryAdminCourse($id);
        }
        $buyTime = date('Y-m-d H:i',time());
        $currentUser = $this->getCurrentUser();

        if ('POST' == $request->getMethod()) {
            $data = $request->request->all();
            $user = $this->getUserService()->getUserByLoginField($data['queryfield']);

            $data["isAdminAdded"] = 1;
            $data['currentLoopTime'] = 1;
            if(isset($data['buyTime']))
            {
                $toLearnCourse = $this->getCertificateService()->getCertificateCourseByBuyTime($user['id'], $id, $data['startPeriod'], $data['endPeriod']);
                $data['currentLoopTime'] = $toLearnCourse['currentLoopTime'];
            }
            list($course, $member, $order) = $this->getCourseMemberService()->becomeStudentAndCreateOrder($user["id"], $course["id"], $data);
            return $this->createStudentTrResponse($course, $member);
        }

        $default = $this->getSettingService()->get('default', array());
        return $this->render('CustomWebBundle:CourseStudentManage:create-modal.html.twig', array(
            'course'  => $course,
            'default' => $default,
            'buyTime' =>$buyTime
        ));
    }

    public function checkStudentAction(Request $request, $id)
    {
        $keyword = $request->query->get('value');
        $user    = $this->getUserService()->getUserByLoginField($keyword);
        
        if(!$user) {
            $response = array('success' => false, 'message' => '该用户不存在');
        } 
        elseif($user['toLearn'])
        {
            $response = array('success' => false, 'message' => '待学学员不可以加入课程');
        }
        else {
            $course = $this->getCourseService()->getCourse($id);
            if($course['type']=='certification')
            {   
                $response = $this->checkCertificateCourseStudent($request,$user,$course);
            }else
            {
                $response = $this->checkNormalCourseStudent($request,$user,$course);
            } 
          
        }

        return $this->createJsonResponse($response);
    }
    private function checkCertificateCourseStudent(Request $request,$user,$course)
    {
        if($user['areaCode'] != $course['areaCode'])
        {
            return array('success' => false, 'message' => '用户不是本课程所属行政区学员'); 
        }
        $userCertificate = $this->getCertificateService()->findCertificationIdAndUserId($user['id'], $course['certificationId']);
        if(empty($userCertificate))
        {
            return array('success' => false, 'message' => '用户没有该证书');
        }
        if($userCertificate['isRevoked'])
        {
            return array('success' => false, 'message' => '该用户证书已吊销');
        }

        $startPeriod = $request->query->get('startPeriod');
        $endPeriod = $request->query->get('endPeriod');
        $toLearnCourse = $this->getCertificateService()->getCertificateCourseByBuyTime($user['id'],$course['id'],$startPeriod, $endPeriod, false);

        if(empty($toLearnCourse))
        {
            return array('success' => false, 'message' => '该用户没有本证书课程'); 
        }

        if(!$toLearnCourse['buyable'])
        {
            if($this->getCourseService()->isCourseStudent($course['id'], $user['id'], $toLearnCourse['currentLoopTime']))
            {
                return array('success' => false, 'message' => '该用户已是本课程的学员了');
            }
            else
            {
                return array('success' => false, 'message' => '该用户不可以购买本课程');
            }
        }       
        
        if ($this->getCourseService()->isCourseTeacher($course['id'], $user['id'])) {
            return array('success' => false, 'message' => '该用户是本课程的教师，不能添加');
        }

        return array('success' => true, 'message' => '');
    }

    private function checkNormalCourseStudent(Request $request,$user,$course)
    {
        $isCourseStudent = $this->getCourseService()->isCourseStudent($course['id'], $user['id'],1);

        if ($isCourseStudent) {
            $response = array('success' => false, 'message' => '该用户已是本课程的学员了');
        } else {
            $response = array('success' => true, 'message' => '');
        }

        $isCourseTeacher = $this->getCourseService()->isCourseTeacher($course['id'], $user['id']);

        if ($isCourseTeacher) {
            $response = array('success' => false, 'message' => '该用户是本课程的教师，不能添加');
        }
        return $response;
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseMemberService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseMemberService');
    }    
    
}