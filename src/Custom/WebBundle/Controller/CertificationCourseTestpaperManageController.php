<?php
namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\Paginator;
use Topxia\Service\Question\Type\QuestionTypeFactory;
use Topxia\WebBundle\Controller\CourseTestpaperManageController as BaseCourseTestpaperManageController;

class CertificationCourseTestpaperManageController extends BaseCourseTestpaperManageController
{
    public function indexAction(Request $request, $courseId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);

        $conditions = array();
        $conditions['target'] = "course-{$course['id']}";
        $paginator = new Paginator(
            $this->get('request'),
            $this->getTestpaperService()->searchTestpapersCount($conditions),
            10
        );

        $testpapers = $this->getTestpaperService()->searchTestpapers(
            $conditions,
            array('createdTime' ,'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($testpapers, 'updatedUserId')); 
        
        return $this->render('CustomWebBundle:CourseTestpaperManage:index.html.twig', array(
            'course' => $course,
            'testpapers' => $testpapers,
            'users' => $users,
            'paginator' => $paginator,

        ));
    }
    public function createAction(Request $request, $courseId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();
            $fields['ranges'] = empty($fields['ranges']) ? array() : explode(',', $fields['ranges']);
            $fields['target'] = "course-{$course['id']}";
            $fields['pattern'] = 'QuestionType';
            $testpapers = array();
            $testpaperName = $fields['name'];
            for ($index = 0; $index < (int)$fields['testpaperNum']; $index++) { 
                list($testpapers[$index], $items) = $this->getTestpaperService()->createTestpaper($fields);
                $fields['name'] = $testpaperName;
            }
            $testpaperIds = ArrayToolkit::column($testpapers,'id');
            return $this->redirect($this->generateUrl('course_certification_manage_testpaper_items',array('courseId' => $course['id'],'testpaperIds'=>json_encode($testpaperIds))));
        }

        $typeNames = $this->get('topxia.twig.web_extension')->getDict('questionType');
        $types = array();
        
        foreach ($typeNames as $type => $name) {
            $typeObj = QuestionTypeFactory::create($type);
            $types[] = array(
                'key' => $type,
                'name' => $name,
                'hasMissScore' => $typeObj->hasMissScore(),
            );
        }

        $conditions["types"] = ArrayToolkit::column($types,"key");
        $conditions["courseId"] = $course["id"];
        $questionNums = $this->getQuestionService()->getQuestionCountGroupByTypes($conditions);
        $questionNums = ArrayToolkit::index($questionNums, "type");
        return $this->render('CustomWebBundle:CourseTestpaperManage:create.html.twig', array(
            'course'    => $course,
            'ranges' => $this->getQuestionRanges($course),
            'types' => $types,
            'questionNums' => $questionNums
        ));
    }

    public function itemsAction(Request $request, $courseId, $testpaperIds)
    {
        $testpaperIds = json_decode($testpaperIds);

        $course = $this->getCourseService()->tryManageCourse($courseId);

        $testpapers = array();
        for ($index=0; $index < count($testpaperIds); $index++) { 
            $testpapers[$index] = $this->tryGetTestpaper($courseId, $testpaperIds[$index]);
        }

        if(empty($testpapers)){
            throw $this->createNotFoundException('试卷不存在');
        }

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
            if (empty($data['questionId']) || empty($data['scores'])) {
                return $this->createMessageResponse('error', '试卷题目不能为空！');
            }
            if (count($data['questionId']) != count($data['scores'])) {
                return $this->createMessageResponse('error', '试卷题目数据不正确');
            }

            $data['questionId'] = array_values($data['questionId']);
            $data['scores'] = array_values($data['scores']);

            $items = array();
            foreach ($data['questionId'] as $index => $questionId) {
                $items[] = array('questionId' => $questionId, 'score' => $data['scores'][$index]);
            }

            if (isset($data['passedScore'])) {
                foreach ($testpapers as $key => $testpaper) {
                    $this->getTestpaperService()->updateTestpaper($testpaper['id'], array('passedScore'=>$data['passedScore']));
                }
            }
            foreach ($testpapers as $key => $testpaper) {
                $this->getTestpaperService()->publishTestpaper($testpaper['id']);
            }
            $this->setFlashMessage('success', '试卷题目保存成功！');
            return $this->redirect($this->generateUrl('course_certification_manage_testpaper',array( 'courseId' => $courseId)));
        }

        $items = $this->getTestpaperService()->getTestpaperItems($testpapers[0]['id']);

        $questions = $this->getQuestionService()->findQuestionsByIds(ArrayToolkit::column($items, 'questionId'));

        $targets = $this->get('topxia.target_helper')->getTargets(ArrayToolkit::column($questions, 'target'));

        $subItems = array();
        $hasEssay = false;
        $scoreTotal = 0;
        foreach ($items as $key => $item) {
            if ($item['questionType'] == 'essay') {
                $hasEssay = true;
            }

            $scoreTotal = $scoreTotal+$item['score'];
            if ($item['parentId'] > 0) {
                $subItems[$item['parentId']][] = $item;
                unset($items[$key]);
            }
        }

        $passedScoreDefault = ceil($scoreTotal*0.6);
        return $this->render('CustomWebBundle:CourseTestpaperManage:items.html.twig', array(
            'course' => $course,
            'testpaper' => $testpapers[0],
            'items' => ArrayToolkit::group($items, 'questionType'),
            'subItems' => $subItems,
            'questions' => $questions,
            'targets' => $targets,
            'hasEssay' => $hasEssay,
            'passedScoreDefault' => $passedScoreDefault
        ));
    }
}