<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\SmsToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\Order\OrderProcessor\OrderProcessorFactory;
use Topxia\WebBundle\Controller\OrderController;
use Custom\Service\Order\OrderProcessor\CertificationCourseOrderProcessor;
use Custom\Common\Util\CourseLoopUtils;

class CertificationOrderController extends OrderController
{
    public function showAction(Request $request)
    {
        $currentUser = $this->getCurrentUser();

        if (!$currentUser->isLogin()) {
            return $this->redirect($this->generateUrl('login'));
        }

        $targetType = $request->query->get('targetType');
        $targetId   = $request->query->get('targetId');
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $currentUser['id'], $targetId);
        if($targetType == 'course'){
            $currentCourse = $this->getCourseService()->getCourse($targetId);
            if($currentCourse['type'] == 'certification') {

                $conditions['areaCode']  = $currentCourse['areaCode'];
                $conditions['type']  = 'certification';
                $conditions['parentId']  = 0;

                $isLearnedCourses = $this->getCourseService()->findUserLearnCourses($currentUser['id'], 0, 999);
                $isCertificationCourseBuyable = $this->getCertificateService()->isCertificationCourseBuyable($currentUser['id'], $currentCourse['id'], $currentLoopTime);
                foreach($isLearnedCourses as $key=>$isLearnedCourse)
                {
                    if($isLearnedCourses[$key]['areaCode'] == $currentCourse['areaCode'] && $isLearnedCourses[$key]['certificationId'] == $currentCourse['certificationId'] && $isLearnedCourses[$key]['partIndex'] == $currentCourse['partIndex'])
                    {
                        if($isLearnedCourses[$key]['id'] == $currentCourse['id']){continue;}
                        if($isCertificationCourseBuyable == false){
                            return $this->createMessageResponse('info', "您已购买过《{$isLearnedCourse['title']}》的该版本证书课程，无法购买该版本课程。", null, 4000, $this->generateUrl('my_courses_learning'));
                        }else{
                            break;
                        }
                    }
                }
            }

        }
        if (empty($targetType)
            || empty($targetId)
            || !in_array($targetType, array("course", "vip", "classroom", "groupSell"))) {
            return $this->createMessageResponse('error', '参数不正确');
        }

        if ($targetType == 'course') {
        	$processor = new CertificationCourseOrderProcessor();
        } else {
        	$processor = OrderProcessorFactory::create($targetType);
        }
        $checkInfo = $processor->preCheck($targetId, $currentUser['id'], $currentLoopTime);

        if (isset($checkInfo['error'])) {
            return $this->createMessageResponse('error', $checkInfo['error']);
        }

        $fields    = $request->query->all();
        $orderInfo = $processor->getOrderInfo($targetId, $fields);

        if (((float) $orderInfo["totalPrice"]) == 0) {
            $formData               = array();
            $formData['userId']     = $currentUser["id"];
            $formData["targetId"]   = $fields["targetId"];
            $formData["targetType"] = $fields["targetType"];
            $formData['amount']     = 0;
            $formData['totalPrice'] = 0;
            $coinSetting            = $this->setting("coin");
            $formData['priceType']  = empty($coinSetting["priceType"]) ? 'RMB' : $coinSetting["priceType"];
            $formData['coinRate']   = empty($coinSetting["coinRate"]) ? 1 : $coinSetting["coinRate"];
            $formData['coinAmount'] = 0;
            $formData['payment']    = 'alipay';
            $order                  = $processor->createOrder($formData, $fields, $currentLoopTime);
            if ($order['status'] == 'paid') {
                return $this->redirect($processor->callbackUrl($order, $this->container));
            }
        }

        // $couponApp = $this->getAppService()->findInstallApp("Coupon");

        // // if (isset($couponApp["version"]) && version_compare("1.0.5", $couponApp["version"], "<=")) {
        // $orderInfo["showCoupon"] = true;
        // // }

        $verifiedMobile = '';

        if ((isset($currentUser['verifiedMobile'])) && (strlen($currentUser['verifiedMobile']) > 0)) {
            $verifiedMobile = $currentUser['verifiedMobile'];
        }

        $orderInfo['verifiedMobile'] = $verifiedMobile;
        return $this->render('CustomWebBundle:Order:order-create-certification.html.twig', $orderInfo);
    }

    public function createAction(Request $request)
    {
        $fields = $request->request->all();

        if (isset($fields['coinPayAmount']) && $fields['coinPayAmount'] > 0) {
            $scenario = "sms_user_pay";

            if ($this->setting('cloud_sms.sms_enabled') == '1' && $this->setting("cloud_sms.{$scenario}") == 'on') {
                list($result, $sessionField, $requestField) = SmsToolkit::smsCheck($request, $scenario);

                if (!$result) {
                    return $this->createMessageResponse('error', '短信验证失败。');
                }
            }
        }

        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            return $this->createMessageResponse('error', '用户未登录，创建订单失败。');
        }

        if (!array_key_exists("targetId", $fields) || !array_key_exists("targetType", $fields)) {
            return $this->createMessageResponse('error', '订单中没有购买的内容，不能创建!');
        }

        $targetType  = $fields["targetType"];
        $targetId    = $fields["targetId"];
        $maxRate     = $fields["maxRate"];
        $priceType   = "RMB";
        $coinSetting = $this->setting("coin");
        $coinEnabled = isset($coinSetting["coin_enabled"]) && $coinSetting["coin_enabled"];

        if ($coinEnabled && isset($coinSetting["price_type"])) {
            $priceType = $coinSetting["price_type"];
        }

        $cashRate = 1;

        if ($coinEnabled && isset($coinSetting["cash_rate"])) {
            $cashRate = $coinSetting["cash_rate"];
        }

        $processor = new CertificationCourseOrderProcessor();

        try {
            if (isset($fields["couponCode"]) && $fields["couponCode"] == "请输入优惠码") {
                $fields["couponCode"] = "";
            }

            list($amount, $totalPrice, $couponResult) = $processor->shouldPayAmount($targetId, $priceType, $cashRate, $coinEnabled, $fields);
            $amount                                   = (string) ((float) $amount);
            $shouldPayMoney                           = (string) ((float) $fields["shouldPayMoney"]);
            //价格比较

            if (intval($totalPrice * 100) != intval($fields["totalPrice"] * 100)) {
                $this->createMessageResponse('error', "实际价格不匹配，不能创建订单!");
            }

            //价格比较

            if (intval($amount * 100) != intval($shouldPayMoney * 100)) {
                return $this->createMessageResponse('error', '支付价格不匹配，不能创建订单!');
            }

            //虚拟币抵扣率比较

            if (isset($fields['coinPayAmount']) && (intval((float) $fields['coinPayAmount'] * 100) > intval($totalPrice * $maxRate * 100))) {
                return $this->createMessageResponse('error', '虚拟币抵扣超出限定，不能创建订单!');
            }

            if (isset($couponResult["useable"]) && $couponResult["useable"] == "yes") {
                $coupon         = $fields["couponCode"];
                $couponDiscount = $couponResult["decreaseAmount"];
            }

            $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $targetId);
            $orderFileds = array(
                'priceType'      => $priceType,
                'totalPrice'     => $totalPrice,
                'amount'         => $amount,
                'coinRate'       => $cashRate,
                'coinAmount'     => empty($fields["coinPayAmount"]) ? 0 : $fields["coinPayAmount"],
                'userId'         => $user["id"],
                'payment'        => 'alipay',
                'targetId'       => $targetId,
                'coupon'         => empty($coupon) ? '' : $coupon,
                'couponDiscount' => empty($couponDiscount) ? 0 : $couponDiscount
            );

            $order = $processor->createOrder($orderFileds, $fields, $currentLoopTime);

            if ($order["status"] == "paid") {
                return $this->redirect($processor->callbackUrl($order, $this->container));
            }

            return $this->redirect($this->generateUrl('pay_center_show', array(
                'sn'         => $order['sn'],
                'targetType' => $order['targetType']
            )));
        } catch (\Exception $e) {
            return $this->createMessageResponse('error', $e->getMessage());
        }
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}