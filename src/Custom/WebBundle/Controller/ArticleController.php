<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\ArticleController as BaseController;

class ArticleController extends BaseController
{
    public function postReplyAction(Request $request, $articleId, $postId)
    {
        $fields               = $request->request->all();

        $fields['content']    = $this->autoParagraph($fields['content']);

        $fields['targetId']   = $articleId;
        $fields['targetType'] = 'article';
        $fields['parentId']   = $postId;

        $post = $this->getThreadService()->createPost($fields);

        return $this->render('TopxiaWebBundle:Thread:subpost-item.html.twig', array(
            'post'    => $post,
            'author'  => $this->getCurrentUser(),
            'service' => $this->getThreadService()
        ));
    }

    protected function getThreadService()
    {
        return $this->getServiceKernel()->createService('Custom:Thread.ThreadService');
    }
}
