<?php
namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\BaseController;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\DateUtils;

class MyCertificationCoursesController extends BaseController
{
    public function listMyCertificationsAction(Request $request)
    {
        $userId = $this->getCurrentUser()->getId();
        $courses = $this->getCertificationService()->getCertificationsForUser($userId);

        $userCertifications = $this->getCertificationService()->findCertificatesIncludingRevokedByUserId($userId);

        $certificationIds = array_unique(ArrayToolkit::column($userCertifications, 'certificationId'));
        $certifications = $this->getCertificationService()->getCertifications($certificationIds);
        $indexCertifications = ArrayToolkit::index($certifications, 'id');

        if (isset($userCertifications)) {
            foreach ($userCertifications as $userCertKey => $userCert) {
                $userCert['courses'] = array();
                $userCert['certificationName'] = $indexCertifications[$userCert['certificationId']]['category'];

                if (isset($courses)) {
                    foreach ($courses as $courseKey => $course) {
                        if ($userCert['certificationId'] == $course['certificationId'] && $userCert['awardTime'] == $course['awardTime']) {
                            array_push($userCert['courses'], $course);
                        }
                    }
                }

                $userCert['awardTime'] = DateUtils::number2Str($userCert['awardTime']);
                $userCertifications[$userCertKey] = $userCert;
            }
        }

        return $this->render('CustomWebBundle:MyCertificationCourses:listMyCertifications.html.twig', 
            array('certifications' => $userCertifications)
        );
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
