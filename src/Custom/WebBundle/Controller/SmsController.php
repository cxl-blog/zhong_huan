<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\CurlToolkit;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\StringToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\SmsController as BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SmsController extends BaseController
{
    public function prepareRemindAction(Request $request, $targetType, $id)
    {
        $item                  = array();
        $url                   = '';

        $item = $this->getCourseService()->getCourse($id);
        $itemId         = ArrayToolkit::column($item, 'id');
        $url  = $this->generateUrl('course_show', array('id' => $id));

        /*$memberUserIds = $this->getCourseService()->findMemberUserIdsByCourseId($id);*/
        $registerUserIds = array_column($this->getUserService()->getUserIdsByTolearnAndAreaCode(false, $item['areaCode']), 'id');
        $haveThisCertificationUserIds = array_column($this->getCertificateService()->getUserIdsByCertificationId($item['certificationId']), 'userId');
        $memberUserIds = array_intersect($registerUserIds, $haveThisCertificationUserIds);
        $count = '0';

        $needToRemindStudentsItems = $this->getSmsService()->getNeedToRemindStudents($targetType,$memberUserIds,$id);

        if ( isset($needToRemindStudentsItems['students']['id']) ) {
        $count = count($needToRemindStudentsItems['students']['id']);
        }
        $item['title'] = StringToolkit::cutter($item['title'], 20, 15, 4);
        return $this->render('CustomWebBundle:Sms:sms-send.html.twig', array(
            'item'       => $item,
            'targetType' => $targetType,
            'url'        => $url,
            'count'  => $count,
            'index'      => 1,
            'isOpen'     => true,
            'students' =>$needToRemindStudentsItems['students'],
            'certification' =>$needToRemindStudentsItems
        ));
    }

    public function sendRemindAction(Request $request, $id)
    {
        $smsType     = 'sms_zhonghuan_course_buy_expire';
        $index       = $request->query->get('index');
        $onceSendNum = 1000;
        $url         = $request->query->get('url');
        $students =$request->query->get('students');
        $count       = $request->query->get('count');
        $certification = $request->query->get('certification');

        $parameters['mobile'] = $students['mobile'];
        $parameters['course'] = $certification['certificationName'];
        $parameters['student'] = $students['nickname'];
        $parameters['days'] = $students['leftDays'];
        $parameters['date'] = $students['term'];
        $description                = $parameters['course'].'到期';

        if (!empty($students)) {
            $result = $this->getSmsService()->smsRemindSend($smsType, $students, $description, $parameters );
        }

        if ($count > $index + $onceSendNum) {
            return $this->createJsonResponse(array('index' => $index + $onceSendNum, 'process' => intval(($index + $onceSendNum) / $count * 100)));
        } else {
            return $this->createJsonResponse(array('status' => 'success', 'process' => 100));
        }
    }
    protected function getSmsService()
    {
        return $this->getServiceKernel()->createService('Custom:Sms.SmsService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

}
