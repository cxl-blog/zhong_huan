<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\UrlUtils;
use Symfony\Component\HttpFoundation\Request;
use Custom\Service\FaceDetect\FaceDetectService;
use Custom\WebBundle\Controller\CameraController;
use Topxia\WebBundle\Controller\DefaultController as ParentDefaultController;

class DefaultController extends ParentDefaultController
{
    /**
     * 如果当前用户还没是使用摄像头上传图片, 则跳到上传图片的页面
     */
    public function indexAction(Request $request)
    {
        if ($this->getCurrentUser()->isLogin()) {
            $faceDetectStatus = $this->getFaceDetectStatus();
            $this->setCookieWithLongDate('latestLoginAreaCode', $this->getCurrentUser()['areaCode']);
            
            if ($faceDetectStatus['isNeedFaceDetected']) {
                $this->clearMobileTokenIfNeed();
                if (!$faceDetectStatus['isFaceCollected']) {
                    return $this->render('CustomWebBundle:Camera:webCamera.html.twig', array('user' => $this->getCurrentUser()));
                } elseif (!$request->getSession()->has(FaceDetectService::SESSION_FACE_DETECTED)) {
                    return $this->render('CustomWebBundle:Camera:faceDetect.html.twig', $faceDetectStatus);
                }
            } else {
                CameraController::markAsFaceDetected($request);

                if ($request->cookies->get('targetUrl') != null) {
                    $renderUrl = $request->cookies->get('targetUrl');
                }
            }
        }

        if (!$request->getSession()->has('alreadyRendered') &&
            isset($renderUrl) && $renderUrl != UrlUtils::getUrlPrefix($renderUrl)) {
            $request->getSession()->set('alreadyRendered', true);
            $this->clearMobileTokenIfNeed();
            return $this->redirect($renderUrl);
        }

        if ("/" != $request->server->get('REQUEST_URI')) {
            return $this->redirect($request->server->get('REQUEST_URI'));
        }

        return $this->render('CustomWebBundle:Default:index.html.twig', array());
    }

    public function indexCategoryNavAction(Request $request, $isMobile)
    {
        $categories = array();

        if ($this->isPluginInstalled('BranchSchool')) {
            $categories = $this->getCategoryService()->findGroupRootCategories('branchSchool');

            foreach ($categories as &$category) {
                $childCategories             = $this->getCategoryService()->findAllCategoriesByParentId($category['id']);
                $category['childCategories'] = $childCategories;
            }
        }

        return $this->render('CustomWebBundle:Default:branch-link.html.twig',
            array(
                'categories' => $categories,
                'isMobile'   => $isMobile
            )
        );
    }

    public function adminSchoolNavAction(Request $request)
    {
        $user        = $this->getCurrentUser();
        $schools     = array();
        $schoolUsers = array();

        if ($this->isPluginInstalled('BranchSchool')) {
            $schoolUsers = $this->getBranchSchoolService()->searchBranchSchoolUsers(
                array('userId' => $user['id']), array('id', 'DESC'), 0, 999);
            $schoolIds = ArrayToolkit::column($schoolUsers, 'schoolId');
            $schools   = $this->getBranchSchoolService()->searchBranchSchools(array('ids' => $schoolIds), array('id', 'ASC'), 0, 999);
            $schools   = ArrayToolkit::index($schools, 'id');
        }

        return $this->render('CustomWebBundle:Default:branch-nav.html.twig', array(
            'schools'     => $schools,
            'schoolUsers' => $schoolUsers
        )
        );
    }

    public function userTrueNameAction()
    {
        $user        = $this->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        if (!$userProfile['truename']) {
            $truename = $user['nickname'];
        } else {
            $truename = $userProfile['truename'];
        }

        return $this->render('CustomWebBundle:Default:user-truename.html.twig', array(
            'truename' => $truename
        ));
    }

    protected function getFaceDetectService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetect.FaceDetectService');
    }

    private function getFaceDetectStatus()
    {
        $service = $this->getFaceDetectService();

        $currentUser = $this->getCurrentUser();

        return $service->getFaceDetectStatus(
            $this->getCurrentUser()->getId(), $currentUser);
    }

    private function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }

    protected function getCategoryService()
    {
        return $this->getServiceKernel()->createService('Taxonomy.CategoryService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected static function setCookieWithLongDate($name, $value)
    {
        setcookie($name, $value, time() + 94608000, '/'); //cookie 有效期 3年, 60*60*24*365*3秒
    }

    private function getTokenService()
    {
        return $this->getServiceKernel()->createService('Custom:User.TokenService');
    }

    private function clearMobileTokenIfNeed()
    {
        if ($this->getCurrentUser()->isLogin()) {
            $user = $this->getCurrentUser();
            $result = $this->getTokenService()->deleteMobileLoginToken($user['id']);
        }
    }
}
