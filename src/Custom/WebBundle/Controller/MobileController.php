<?php

namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\CloudPlatform\CloudAPIFactory;
use Endroid\QrCode\QrCode;
use Symfony\Component\HttpFoundation\Response;
use Topxia\WebBundle\Controller\MobileController as BaseMobileController;

class MobileController extends BaseMobileController
{
    public function indexAction(Request $request)
    {
        $mobile = $this->setting('mobile', array());

        if (empty($mobile['enabled'])) {
            return $this->createMessageResponse('info', '客户端尚未开启！');
        }

        $filePath = dirname($_SERVER['DOCUMENT_ROOT']).'/app/config/';
        $fileName = 'versionConfig.yml';

        $appInfos = $this->getMobileService()->readMobileInfosFromFile($fileName, $filePath);

        return $this->render('CustomWebBundle:Mobile:index.html.twig', array(
            'host' => $request->getHttpHost(),
            'mobile' => $mobile,
            'downLoadPath' => $appInfos['appPath']
        ));
    }

    public function downloadQrcodeAction(Request $request)
    {
        $filePath = dirname($_SERVER['DOCUMENT_ROOT']).'/app/config/';
        $fileName = 'versionConfig.yml';
        $appInfos = $this->getMobileService()->readMobileInfosFromFile($fileName, $filePath);
        $qrCode = new QrCode();
        $qrCode->setText($appInfos['appPath']);
        $qrCode->setSize(150);
        $qrCode->setPadding(10);
        $img = $qrCode->get('png');

        $headers = array('Content-Type'     => 'image/png',
                         'Content-Disposition' => 'inline; filename="image.png"');
        return new Response($img, 200, $headers);
    }

    protected function getMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:Mobile.MobileService');
    }
} 