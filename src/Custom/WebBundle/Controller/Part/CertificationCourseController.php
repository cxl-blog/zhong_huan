<?php
namespace Custom\WebBundle\Controller\Part;

use Custom\Common\Constants\LearnMode;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\ImageUrlUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\Part\CourseController;

class CertificationCourseController extends CourseController
{
    public function headerAction(Request $request, $course, $member, $certificationCourse = null)
    {
        $user            = $this->getCurrentUser();
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $course['id']);

        $certificationForGuest  = null;
        $certification = null;
        $isAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']) || $this->getCourseService()->canBranchAdminMangeCourse($course['id']);
        $learnedHours = 0;
        if (!$isAdmin && $user['id'] != 0) {
            if (empty($certificationCourse)) {
                $certificationCourse = $this->getCertificateService()->getCertificationsForUser($user['id'], false, $course['id'], $currentLoopTime);
            }
            $learnedHours = $certificationCourse['learnedHours'];
        } else {
            $course                                 = $this->getCourseService()->getCertificationId($course['id']);
            $certificationForGuest  = array(
                'learnModeName' => LearnMode::formatSingleCertificationType($course)['certificationType']
            );
            $certification                          = $this->getCertificateService()->getCertification($course['certificationId']);
        }

        $isTestPassed    = $certificationCourse['isTestpaperPassed'];

        $testpaperItem = null;
        $nextLearnLesson = null;
        if (!$isTestPassed && $certificationCourse['isAllLessonsFinished']) {
            $testpaperItem = $this->getCourseService()->getTestpaperLesson($course['id']);
            $nextLearnLesson = $testpaperItem;
        } else if (!$certificationCourse['isAllLessonsFinished'] && !empty($member)) {
            $nextLearnLesson = $this->getCourseService()->getUserNextLearnLesson($user['id'], $course['id'], $currentLoopTime);
        }

        $isLogin         = $user->isLogin();

        $nowTime      = strtotime(date('Y-m-d H:i:s'));
        $dateForGuest = array('nowTime' => $nowTime);

        if (isset($certificationCourse['makeupDeadline'])) {
            $makeupDeadline                 = strtotime($certificationCourse['makeupDeadline']);
            $dateForGuest['makeupDeadline'] = $makeupDeadline;
        }

        if (($course['discountId'] > 0) && ($this->isPluginInstalled("Discount"))) {
            $course['discountObj'] = $this->getDiscountService()->getDiscount($course['discountId']);
        }

        $hasFavorited = $this->getCourseService()->hasFavoritedCourse($course['id'], $currentLoopTime);

        $user          = $this->getCurrentUser();
        $userVipStatus = $courseVip = null;

        if ($this->isPluginInstalled('Vip') && $this->setting('vip.enabled')) {
            $courseVip = $course['vipLevelId'] > 0 ? $this->getLevelService()->getLevel($course['vipLevelId']) : null;

            if ($courseVip) {
                $userVipStatus = $this->getVipService()->checkUserInMemberLevel($user['id'], $courseVip['id']);
            }
        }

        $status = array();

        if ($course['type'] == "certification") {
            $learnLessonStatus = $this->getCourseService()->getUserLearnLessonStatuses($user['id'], $course['id']);

            foreach ($learnLessonStatus as $learnStatus) {
                if ($learnStatus == "finished") {
                    array_push($status, $learnStatus);
                }
            }
        }

        $previewLesson = $this->getCourseService()->searchLessons(array('courseId' => $course['id'], 'type' => 'video', 'free' => 1), array('seq', 'ASC'), 0, 1);
        $breadcrumbs   = $this->getCategoryService()->findCategoryBreadcrumbs($course['categoryId']);

        return $this->render('CustomWebBundle:Course:Part/normal-header.html.twig', array(
            'learnedHours'          => $learnedHours,
            'sumHours'              => $course['totalHours'],
            'course'                => $course,
            'member'                => $member,
            'hasFavorited'          => $hasFavorited,
            'courseVip'             => $courseVip,
            'userVipStatus'         => $userVipStatus,
            'nextLearnLesson'       => $nextLearnLesson,
            'testpaperItem' => $testpaperItem,
            'previewLesson'         => empty($previewLesson) ? null : $previewLesson[0],
            'breadcrumbs'           => $breadcrumbs,
            'learnPercent'          => $this->getCourseService()->getPercent($course['totalHours'], $learnedHours),
            'certificationForGuest' => $certificationForGuest,
            'certification'         => $certification,
            'isLogin'               => $isLogin,
            'certificationCourse'   => $certificationCourse,
            'dateForGuest'          => $dateForGuest,
            'isAdmin'               => $isAdmin,
            'learnStatus'           => empty($status[0]) ? "" : $status[0],
            'isTestPassed'          => $isTestPassed
        ));
    }

    public function learnAction(Request $request, $id)
    {
        $user      = $this->getCurrentUser();
        $starttime = $request->query->get('starttime', '');

        if (!$user->isLogin()) {
            $request->getSession()->set('_target_path', $this->generateUrl('course_show', array('id' => $id)));

            return $this->createMessageResponse('info', '你好像忘了登录哦？', null, 3000, $this->generateUrl('login'));
        }

        $course                    = $this->getCourseService()->getCourse($id);
        $course['currentLoopTime'] = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);

        if (empty($course)) {
            throw $this->createNotFoundException("课程不存在，或已删除。");
        }

        if ($course['approval'] == 1 && ($user['approvalStatus'] != 'approved')) {
            return $this->createMessageResponse('info', "该课程需要通过实名认证，你还没有通过实名认证。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        if (!$this->getCourseService()->canTakeCourse($course, $course['currentLoopTime'])) {
            return $this->createMessageResponse('info', "您还不是课程《{$course['title']}》的学员，请先购买或加入学习。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        try {
            list($course, $member) = $this->getCourseService()->tryTakeCourse($id, $course['currentLoopTime']);

            if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
                return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
            }

            if ($member && $member['levelId'] > 0) {
                if ($this->getVipService()->checkUserInMemberLevel($member['userId'], $course['vipLevelId']) != 'ok') {
                    return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
                }
            }
        } catch (Exception $e) {
            throw $this->createAccessDeniedException('抱歉，未发布课程不能学习！');
        }

        $area = $this->getAreaService()->getAreaByCode($course['areaCode']);
        return $this->render('CustomWebBundle:Course:learn.html.twig', array(
            'faceImgPath'     => ImageUrlUtils::getImagePath($user['faceImgPath']),
            'course'          => $course,
            'area'            => $area,
            'starttime'       => $starttime,
            'courseMember' => $member,
            'currentLoopTime' => $member['currentLoopTime']
        ));
    }

    public function recordLearningTimeAction(Request $request, $lessonId, $time)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            $this->createAccessDeniedException();
        }

        $this->getCourseService()->waveLearningTime($user['id'], $lessonId, $time);

        return $this->createJsonResponse(true);
    }

    public function recordWatchingTimeAction(Request $request, $lessonId, $time)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            $this->createAccessDeniedException();
        }

        $learn = $this->getCourseService()->waveWatchingTime($user['id'], $lessonId, $time);

        $isLimit = $this->setting('magic.lesson_watch_limit');

        if ($isLimit) {
            $lesson         = $this->getCourseService()->getLesson($lessonId);
            $course         = $this->getCourseService()->getCourse($lesson['courseId']);
            $watchLimitTime = $course['watchLimit'] * $lesson['length'];

            if ($lesson['type'] == 'video' && ($course['watchLimit'] > 0) && ($learn['watchTime'] >= $watchLimitTime)) {
                $learn['watchLimited'] = true;
            }
        }

        return $this->createJsonResponse($learn);
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getTestpaperService()
    {
        return $this->getServiceKernel()->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
