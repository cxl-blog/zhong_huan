<?php
namespace Custom\WebBundle\Controller;

use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\LessonNotePluginController as BaseController;

class LessonNotePluginController extends BaseController
{
    public function saveAction(Request $request)
    {
        $form = $this->createNoteForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $note            = $form->getData();
                $note['status']  = $request->request->get('status') ? 1 : 0;
                $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $note['courseId']);
                $this->getCourseNoteService()->saveNote($note, $currentLoopTime);
                return $this->createJsonResponse(true);
            } else {
                return $this->createJsonResponse(false);
            }
        }

        return $this->createJsonResponse(false);
    }
}
