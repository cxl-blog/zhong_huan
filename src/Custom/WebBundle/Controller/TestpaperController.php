<?php
namespace Custom\WebBundle\Controller;

use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\ImageUrlUtils;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\TestpaperController as BaseController;

class TestpaperController extends BaseController
{
    /**
     * @param targetType 类型, 一般为 lesson
     * @param targetId   id, 一般为 lessonId
     * @param testId     课时所选的testpaperId 或 上次参加的考试
     */
    public function doTestpaperAction(Request $request, $targetType, $targetId, $testId)
    {
        $userId = $this->getCurrentUser()->id;

        //学习计划任务
        if ($this->isPluginInstalled('ClassroomPlan')) {
            $taskProcessor = $this->getTaskProcessor('studyPlan');
            $canFinish     = $taskProcessor->canFinish($targetId, 'testpaper', $userId);

            if (!$canFinish) {
                return $this->createMessageResponse('info', '在该任务之前，还有学习任务没有完成哦！');
            }
        }

        $testpaper = $this->getTestpaperService()->getTestpaper($testId);

        if (empty($testpaper)) {
            throw $this->createNotFoundException();
        }

        $testTarget = explode('-', $testpaper['target']);  
               // $testTarget为 array('course', '{courseId}')
        if ($testTarget[0] == 'course') {
            $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $userId, $testTarget[1]);
        } else {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
        }

        $testpaperResult = $this->getTestpaperService()->createTestpaperResultIfNeed(
            array(
                'testpaperId' => $testId,
                'userId' => $userId,
                'lessonId' => $targetId,
                'currentLoopTime' => $currentLoopTime
            )
        );

        
        if (in_array($testpaperResult['status'], array('doing', 'paused'))) {
            return $this->redirect($this->generateUrl('course_manage_show_test', array('id' => $testpaperResult['id'])));
        } else {
            return $this->redirect($this->generateUrl('course_manage_test_results', array('id' => $testpaperResult['id'])));
        }
    }

    public function showTestAction(Request $request, $id)
    {
        $testpaperResult = $this->getTestpaperService()->getTestpaperResult($id);

        if (in_array($testpaperResult['status'], array('reviewing', 'finished'))) {
            return $this->redirect($this->generateUrl('course_manage_test_results', array('id' => $testpaperResult['id'])));
        }

        $testpaper = $this->getTestpaperService()->getTestpaper($testpaperResult['testId']);

        $canLookTestpaper = $this->getTestpaperService()->canLookTestpaper($id);

        $result = $this->getTestpaperService()->showTestpaper($id);
        $items  = $result['formatItems'];
        $total  = $this->makeTestpaperTotal($testpaper, $items);

        $favorites = $this->getQuestionService()->findAllFavoriteQuestionsByUserId($testpaperResult['userId']);
        $targets   = $this->get('topxia.target_helper')->getTargets(array($testpaperResult['target']));

        //试卷对应的考试（课时）
        $target = array();

        if ($targets[$testpaperResult['target']]['type'] == 'lesson') {
            $target = $this->getCourseService()->getLesson($targets[$testpaperResult['target']]['id']);

            if ($target['testMode'] == 'realTime') {
                $testpaperResult['usedTime'] = time() - $target['testStartTime'];
            }
        }

        $userId = $this->getCurrentUser()['id'];
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $userId, $target['courseId']);
        $faceDetectStatus = $this->getFaceDetectService()->getFaceDetectStatusForTestpaper($userId, $testpaper['id'], $currentLoopTime);
        return $this->render('CustomWebBundle:QuizQuestionTest:testpaper-show.html.twig', array(
            'items'       => $items,
            'limitTime'   => $testpaperResult['limitedTime'] * 60,
            'paper'       => $testpaper,
            'paperResult' => $testpaperResult,
            'favorites'   => ArrayToolkit::column($favorites, 'questionId'),
            'id'          => $id,
            'total'       => $total,
            'target'      => $target,
            'faceDetectStatus' => $faceDetectStatus,
            'faceImgPath' => ImageUrlUtils::getImagePath($this->getCurrentUser()['faceImgPath'])
        ));
    }

    public function userResultJsonAction(Request $request, $id)
    {
        $user = $this->getCurrentUser()->id;

        if (empty($user)) {
            return $this->createJsonResponse(array('error' => '您尚未登录系统或登录已超时，请先登录。'));
        }

        $testpaper = $this->getTestpaperService()->getTestpaper($id);

        if (empty($testpaper)) {
            return $this->createJsonResponse(array('error' => '试卷已删除，请联系管理员。'));
        }

        $testTarget = explode('-', $testpaper['target']);

        if ($testTarget[0] == 'course') {
            $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user, $testTarget[1]) ? CourseLoopUtils::getCurrentLoopTime($request, $user, $testTarget[1]) : CourseLoopUtils::$START_LOOP_TIME;
        } else {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
        }
        $lessonId = $request->query->get('lessonId');        
        $testResult = $this->getTestpaperService()->findTestpaperResultByLessonIdAndUserIdAndActive($lessonId, $user, $currentLoopTime);
        if (empty($testResult)) {
            return $this->createJsonResponse(array('status' => 'nodo'));
        }

        return $this->createJsonResponse(array('status' => $testResult['status'], 'resultId' => $testResult['id']));
    }

    public function finishTestAction(Request $request, $id)
    {
        $testpaperResult = $this->getTestpaperService()->getTestpaperResult($id);

        if (!empty($testpaperResult) && !in_array($testpaperResult['status'], array('doing', 'paused'))) {
            return $this->createJsonResponse(true);
        }

        if ($request->getMethod() == 'POST') {
            $data     = $request->request->all();
            $answers  = array_key_exists('data', $data) ? $data['data'] : array();
            $usedTime = $data['usedTime'];
            $user     = $this->getCurrentUser();

            //提交变化的答案
            $results = $this->getTestpaperService()->submitTestpaperAnswer($id, $answers);

            //完成试卷，计算得分
            $testResults = $this->getTestpaperService()->makeTestpaperResultFinish($id);

            $testpaperResult = $this->getTestpaperService()->getTestpaperResult($id);

            $testpaper = $this->getTestpaperService()->getTestpaper($testpaperResult['testId']);

            $testTarget = explode('-', $testpaper['target']);

            if ($testTarget[0] == 'course') {
                $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $testTarget[1]);
            } else {
                $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
            }

            //试卷信息记录
            $this->getTestpaperService()->finishTest($id, $user['id'], $usedTime);

            $targets = $this->get('topxia.target_helper')->getTargets(array($testpaper['target']));

            $course = $this->getCourseService()->getCourse($targets[$testpaper['target']]['id']);

            if ($this->getTestpaperService()->isExistsEssay($testResults)) {
                $user = $this->getCurrentUser();

                $message = array(
                    'id'       => $testpaperResult['id'],
                    'name'     => $testpaperResult['paperName'],
                    'userId'   => $user['id'],
                    'userName' => $user['nickname'],
                    'type'     => 'perusal'
                );

                foreach ($course['teacherIds'] as $receiverId) {
                    $result = $this->getNotificationService()->notify($receiverId, 'test-paper', $message);
                }
            }

            // @todo refactor. , wellming
            $targets = $this->get('topxia.target_helper')->getTargets(array($testpaperResult['target']));

            if ($targets[$testpaperResult['target']]['type'] == 'lesson' && !empty($targets[$testpaperResult['target']]['id'])) {
                $lessons = $this->getCourseService()->findLessonsByIds(array($targets[$testpaperResult['target']]['id']));

                if (!empty($lessons[$targets[$testpaperResult['target']]['id']])) {
                    $lesson = $lessons[$targets[$testpaperResult['target']]['id']];
                    $this->getCourseService()->finishLearnLesson($lesson['courseId'], $lesson['id'], $currentLoopTime);
                }
            }

            return $this->createJsonResponse(true);
        }
    }

    public function reDoTestpaperAction(Request $request, $targetType, $targetId, $testId)
    {
        $userId    = $this->getCurrentUser()->id;
        $testpaper = $this->getTestpaperService()->getTestpaper($testId);

        if (empty($testpaper)) {
            throw $this->createNotFoundException();
        }

        $testTarget = explode('-', $testpaper['target']);

        if ($testTarget[0] == 'course') {
            $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $userId, $testTarget[1]);
        } else {
            $currentLoopTime = CourseLoopUtils::$START_LOOP_TIME;
        }

        $testpaperResult = $this->getTestpaperService()->createTestpaperResultIfNeed(
            array(
                'testpaperId' => $testId,
                'userId' => $userId,
                'lessonId' => $targetId,
                'currentLoopTime' => $currentLoopTime,
                'isCreate' => true
            )
        );
        return $this->redirect($this->generateUrl('course_manage_show_test', array('id' => $testpaperResult['id'])));
    }

    protected function getFaceDetectService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetect.FaceDetectService');
    }

    private function getTestpaperResultByCourseType($course,$testpaperIds,$currentLoopTime,$targetType,$targetId,$testId)
    {
        if($course['type'] == 'certification'){
            if($testpaperIds){
                $randTestpaper = $this->getTestpaperService()->getRandTestpaper($testpaperIds);
            }else{
                $randTestpaperIds = $this->getTestpaperService()->getAllTestpaperIdsByCourseId($course['id']);
                $randTestpaper = $this->getTestpaperService()->getRandTestpaper($randTestpaperIds);
            }
            $testpaper = $this->getTestpaperService()->getTestpaper($randTestpaper);
            if(empty($testpaper)){
                throw $this->createNotFoundException();
            }
            if ($testpaper['status'] == 'draft') {
                return $this->createMessageResponse('info', '该试卷未发布，如有疑问请联系老师！');
            }
            if ($testpaper['status'] == 'closed') {
                return $this->createMessageResponse('info', '该试卷已关闭，如有疑问请联系老师！');
            }            
            $testResult = $this->getTestpaperService()->startTestpaper($randTestpaper, array('type' => $targetType, 'id' => $targetId),$currentLoopTime);
        }else{
            $testResult = $this->getTestpaperService()->startTestpaper($testId, array('type' => $targetType, 'id' => $targetId),$currentLoopTime);            
        }       
        return $testResult;
    }

}
