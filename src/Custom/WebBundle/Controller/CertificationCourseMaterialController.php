<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\CourseHeaderUtils;
use Custom\WebBundle\Controller\CourseMaterialController as BaseCourseMaterialController;

class CertificationCourseMaterialController extends BaseCourseMaterialController
{
    public function indexAction(Request $request, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $id);

        if ($response) {
            return $response;
        }
        $user = $this->getCurrentUser();
        $paginator = new Paginator(
            $request,
            $this->getMaterialService()->getMaterialCount($id),
            20
        );

        $materials = $this->getMaterialService()->findCourseMaterials(
            $id,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $lessons = $this->getCourseService()->getCourseLessons($course['id']);
        $lessons = ArrayToolkit::index($lessons, 'id');

        $infos = array(
            'lessons' => $lessons,
            'materials' => $materials,
            'paginator' => $paginator,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        
        return $this->render("CustomWebBundle:CourseMaterial:index.html.twig", $headerInfos);
    }

    public function certNormalIndexAction(Request $request, $id)
    {
        $course = $this->getCourseService()->getCourse($id);
        if ($course['type'] == 'certification') {
            return $this->redirect(
                $this->generateUrl('course_certification_materials', array(
                    'id' => $id
                ))
            );
        } else {
            return $this->redirect(
                $this->generateUrl('course_materials', array(
                    'id' => $id
                ))
            );
        }
    }

    public function downloadAction(Request $request, $courseId, $materialId)
    {
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $courseId);
        list($course, $member) = $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime);

        if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
            return $this->redirect($this->generateUrl('course_materials', array('id' => $courseId)));
        }

        if ($member && $member['levelId'] > 0) {
            if ($this->getVipService()->checkUserInMemberLevel($member['userId'], $course['vipLevelId']) != 'ok') {
                return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
            }
        }

        $material = $this->getMaterialService()->getMaterial($courseId, $materialId);

        if (empty($material)) {
            throw $this->createNotFoundException();
        }

        return $this->forward('TopxiaWebBundle:UploadFile:download', array('fileId' => $material['fileId']));
    }
}
