<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Util\StringUtils;

class CertificationCourseThreadController extends CourseThreadController
{
    public function indexAction(Request $request, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $id);

        if ($response) {
            return $response;
        }
        $user = $this->getCurrentUser();
        $filters    = $this->getThreadSearchFilters($request);
        $conditions = $this->convertFiltersToConditions($course, $filters);

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->searchThreadCount($conditions),
            20
        );

        $threads = $this->getThreadService()->searchThreads(
            $conditions,
            $filters['sort'],
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        foreach ($threads as $key => $thread) {
            $threads[$key]['sticky']         = $thread['isStick'];
            $threads[$key]['nice']           = $thread['isElite'];
            $threads[$key]['lastPostTime']   = $thread['latestPostTime'];
            $threads[$key]['lastPostUserId'] = $thread['latestPostUserId'];
        }

        $lessons = $this->getCourseService()->findLessonsByIds(ArrayToolkit::column($threads, 'lessonId'));
        $userIds = array_merge(
            ArrayToolkit::column($threads, 'userId'),
            ArrayToolkit::column($threads, 'latestPostUserId')
        );
        $users   = $this->getUserService()->findUsersByIds($userIds);

        $infos = array(
            'threads' => $threads,
            'users' => $users,
            'paginator' => $paginator,
            'filters' => $filters,
            'lessons' => $lessons,
            'member' => $member,
            'target'    => array('type' => 'course', 'id' => $id)       
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        return $this->render("CustomWebBundle:CourseThread:index.html.twig", $headerInfos);
    }

    public function showAction(Request $request, $courseId, $threadId)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $courseId);

        if ($response) {
            return $response;
        }

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }

        $user = $this->getCurrentUser();

        if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
            $isMemberNonExpired = false;
        } else {
            $isMemberNonExpired = true;
        }

        $thread = $this->getThreadService()->getThread($course['id'], $threadId);

        if (empty($thread)) {
            throw $this->createNotFoundException("话题不存在，或已删除。");
        }

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->getThreadPostCount($course['id'], $thread['id']),
            30
        );

        $posts = $this->getThreadService()->findThreadPosts(
            $thread['courseId'],
            $thread['id'],
            'default',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        if ($thread['type'] == 'question' && $paginator->getCurrentPage() == 1) {
            $elitePosts = $this->getThreadService()->findThreadElitePosts($thread['courseId'], $thread['id'], 0, 10);
        } else {
            $elitePosts = array();
        }

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));

        $this->getThreadService()->hitThread($courseId, $threadId);

        $isManager = $this->getCourseService()->canManageCourse($course['id'], $member['currentLoopTime']);

        $lesson = $this->getCourseService()->getCourseLesson($course['id'], $thread['lessonId']);
        $infos = array(
            'lesson' => $lesson,
            'thread' => $thread,
            'author' => $this->getUserService()->getUser($thread['userId']),
            'posts' => $posts,
            'elitePosts' => $elitePosts,
            'users' => $users,
            'isManager' => $isManager,
            'isMemberNonExpired' => $isMemberNonExpired,
            'paginator' => $paginator,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);

        return $this->render("CustomWebBundle:CourseThread:show.html.twig", $headerInfos);
    }

    public function createAction(Request $request, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $id);

        if ($response) {
            return $response;
        }
        $user = $this->getCurrentUser();
        if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
            return $this->redirect($this->generateUrl('course_certification_threads', array('id' => $id)));
        }

        if ($member && $member['levelId'] > 0) {
            if ($this->getVipService()->checkUserInMemberLevel($member['userId'], $course['vipLevelId']) != 'ok') {
                return $this->redirect($this->generateUrl('course_certification_show', array('id' => $id)));
            }
        }

        $type = $request->query->get('type') ?: 'discussion';
        $form = $this->createThreadForm(array(
            'type'     => $type,
            'courseId' => $course['id']
        ));

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                try {
                    $thread = $this->getThreadService()->createThread($form->getData(), $member['currentLoopTime']);
                    return $this->redirect($this->generateUrl('course_certification_thread_show', array(
                        'courseId' => $thread['courseId'],
                        'threadId' => $thread['id']
                    )));
                } catch (\Exception $e) {
                    return $this->createMessageResponse('error', $e->getMessage(), '错误提示', 1, $request->getPathInfo());
                }
            }
        }
        $infos = array(
            'form'   => $form->createView(),
            'type'   => $type,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        return $this->render("CustomWebBundle:CourseThread:form.html.twig", $headerInfos);
    }

    public function editAction(Request $request, $courseId, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $courseId);

        if ($response) {
            return $response;
        }

        $thread = $this->getThreadService()->getThread($courseId, $id);

        if (empty($thread)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if ($user->isLogin() && $user->id == $thread['userId']) {
            $course = $this->getCourseService()->getCourse($courseId);
        } else {
            $course = $this->getCourseService()->tryManageCourse($courseId, $member['currentLoopTime']);
        }

        $form = $this->createThreadForm($thread);

        if ($request->getMethod() == 'POST') {
            try {
                $form->bind($request);

                if ($form->isValid()) {
                    $thread = $this->getThreadService()->updateThread($thread['courseId'], $thread['id'], $form->getData());

                    if ($user->isAdmin()) {
                        $threadUrl = $this->generateUrl('course_certification_thread_show', array('courseId' => $courseId, 'threadId' => $thread['id']), true);
                        $message   = array(
                            'courseId' => $courseId,
                            'id'       => $thread['id'],
                            'title'    => $thread['title'],
                            'type'     => 'modify'
                        );

                        $this->getNotifiactionService()->notify($thread['userId'], 'course-thread', $message);
                    }

                    return $this->redirect($this->generateUrl('course_certification_thread_show', array(
                        'courseId' => $thread['courseId'],
                        'threadId' => $thread['id']
                    )));
                }
            } catch (\Exception $e) {
                return $this->createMessageResponse('error', $e->getMessage(), '错误提示', 1, $request->getPathInfo());
            }
        }

        $infos = array(
            'form'   => $form->createView(),
            'thread' => $thread,
            'type'   => $thread['type'],
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);

        return $this->render("CustomWebBundle:CourseThread:form.html.twig", $headerInfos);
    }

    public function postAction(Request $request, $courseId, $id)
    {
        $currentUser           = $this->getCurrentUser();
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $currentUser['id'], $courseId);
        list($course, $member) = $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }

        $thread = $this->getThreadService()->getThread($course['id'], $id);
        $form   = $this->createPostForm(array(
            'courseId' => $thread['courseId'],
            'threadId' => $thread['id']
        ));

        if ($request->getMethod() == 'POST') {
            $form->bind($request);
            $userId = $currentUser->id;

            if ($form->isValid()) {
                $postData               = $form->getData();

                list($postData, $users) = $this->replaceMention($postData);

                $post                   = $this->getThreadService()->createPost($postData, $currentLoopTime);
                $threadUrl              = $this->generateUrl('course_certification_thread_show', array('courseId' => $courseId, 'threadId' => $id), true);
                $threadUrl .= "#post-".$post['id'];

                if ($thread['userId'] != $currentUser->id) {
                    $message = array(
                        'userId'   => $currentUser['id'],
                        'userName' => $currentUser['nickname'],
                        'courseId' => $courseId,
                        'id'       => $id,
                        'title'    => $thread['title'],
                        'postId'   => $post['id'],
                        'type'     => 'reply'

                    );
                    $this->getNotifiactionService()->notify($thread['userId'], 'course-thread', $message);
                }

                foreach ($users as $user) {
                    if ($thread['userId'] != $user['id']) {
                        if ($user['id'] != $userId) {
                            $message = array(
                                'userId'   => $currentUser['id'],
                                'userName' => $currentUser['nickname'],
                                'courseId' => $courseId,
                                'id'       => $id,
                                'title'    => $thread['title'],
                                'postId'   => $post['id'],
                                'type'     => 'replayat'
                            );

                            $this->getNotifiactionService()->notify($user['id'], 'course-thread', $message);
                        }
                    }
                }

                return $this->render('CustomWebBundle:CourseThread:post-list-item.html.twig', array(
                    'course'    => $course,
                    'thread'    => $thread,
                    'post'      => $post,
                    'author'    => $this->getUserService()->getUser($post['userId']),
                    'isManager' => $this->getCourseService()->canManageCourse($course['id'])
                ));
            } else {
                return $this->createJsonResponse(false);
            }
        }

        return $this->render('CustomWebBundle:CourseThread:post.html.twig', array(
            'course' => $course,
            'member' => $member,
            'thread' => $thread,
            'form'   => $form->createView()
        ));
    }

    public function editPostAction(Request $request, $courseId, $threadId, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $courseId);

        if ($response) {
            return $response;
        }

        $post = $this->getThreadService()->getPost($courseId, $id);

        if (empty($post)) {
            throw $this->createNotFoundException();
        }

        $user = $this->getCurrentUser();

        if ($user->isLogin() && $user->id == $post['userId']) {
            $course = $this->getCourseService()->getCourse($courseId);
        } else {
            $course = $this->getCourseService()->tryManageCourse($courseId, $member['currentLoopTime']);
        }

        $thread = $this->getThreadService()->getThread($courseId, $threadId);

        $form = $this->createPostForm($post);

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $post = $this->getThreadService()->updatePost($post['courseId'], $post['id'], $form->getData());

                if ($user->isAdmin()) {
                    $message = array(
                        'userId'   => $user['id'],
                        'userName' => $user['nickname'],
                        'courseId' => $courseId,
                        'id'       => $threadId,
                        'title'    => $thread['title'],
                        'postId'   => $post['id']
                    );
                    $message['type'] = 'modify-thread';
                    $this->getNotifiactionService()->notify($thread['userId'], 'course-thread', $message);
                    $message['type'] = 'modify-post';
                    $this->getNotifiactionService()->notify($post['userId'], 'course-thread', $message);
                }

                return $this->redirect($this->generateUrl('course_certification_thread_show', array(
                    'courseId' => $post['courseId'],
                    'threadId' => $post['threadId']
                )));
            }
        }
        
        $infos = array(
            'form'   => $form->createView(),
            'post'   => $post,
            'thread' => $thread,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        return $this->render('CustomWebBundle:CourseThread:post-form.html.twig', $headerInfos);
    }

    protected function replaceMention($postData)
    {
        $currentUser = $this->getCurrentUser();
        $content     = $postData['content'];

        $users       = array();
        preg_match_all('/@([\x{4e00}-\x{9fa5}\w]{2,16})/u', $content, $matches);
        $mentions = array_unique($matches[1]);

        $selectedUsername = array();

        if (!empty($postData['selectedUsername'])) {
            $selectedUsername = json_decode($postData['selectedUsername']);
        }

        $mentions = array_merge($mentions, $selectedUsername);

        foreach ($mentions as $mention) {
            $user = $this->getUserService()->getUserByNickname($mention);

            if ($user) {
                $path     = $this->generateUrl('user_show', array('id' => $user['id']));
                $nickname = $user['nickname'];
                $formattedNickname = StringUtils::formatStringForSecurity($user['nickname']);
                $regularNickname = StringUtils::formatStringForRegular($user['nickname']);
                $html     = "<a href=\"{$path}\" class=\"show-user\">@{$formattedNickname}</a>";

                $content = preg_replace("/@{$nickname}/ui", "@{$formattedNickname}", $content);

                $content = preg_replace("/@{$regularNickname}/ui", $html, $content);

                $users[] = $user;
            }
        }

        $postData['content'] = $content;

        unset($postData['selectedUsername']);

        return array($postData, $users);
    }

    protected function createPostForm($data = array())
    {
        return $this->createNamedFormBuilder('post', $data)
            ->add('content', 'textarea')
            ->add('courseId', 'hidden')
            ->add('threadId', 'hidden')
            ->add('selectedUsername', 'hidden')
            ->getForm();
    }
}
