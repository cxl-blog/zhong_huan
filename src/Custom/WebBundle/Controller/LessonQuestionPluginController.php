<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\LessonQuestionPluginController as BaseController;

class LessonQuestionPluginController extends BaseController
{
    public function initAction(Request $request)
    {
        $currentUser           = $this->getCurrentUser();
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $currentUser['id'], $request->query->get('courseId'));
        list($course, $member) = $this->getCourseService()->tryTakeCourse($request->query->get('courseId'), $currentLoopTime);

        $lesson = array(
            'id'       => $request->query->get('lessonId'),
            'courseId' => $course['id']
        );

        $threads = $this->getThreadService()->searchThreads(
            array(
                'lessonId' => $lesson['id'],
                'type'     => 'question'
            ),
            'createdNotStick',
            0, 20
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($threads, 'userId'));

        $form = $this->createQuestionForm(array(
            'courseId' => $course['id'],
            'lessonId' => $lesson['id']
        ));

        return $this->render('CustomWebBundle:LessonQuestionPlugin:index.html.twig', array(
            'threads' => $threads,
            'lesson'  => $lesson,
            'form'    => $form->createView(),
            'users'   => $users
        ));
    }

    public function showAction(Request $request)
    {
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $request->query->get('courseId'));
        list($course, $member) = $this->getCourseService()->tryTakeCourse($request->query->get('courseId'), $currentLoopTime);

        $thread = $this->getThreadService()->getThread(
            $course['id'],
            $request->query->get('id')
        );

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->getThreadPostCount($course['id'], $thread['id']),
            100
        );

        $posts = $this->getThreadService()->findThreadPosts(
            $thread['courseId'],
            $thread['id'],
            'default',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        $threader = $this->getUserService()->getUser($thread['userId']);
        $users    = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));

        $form = $this->createPostForm(array(
            'courseId' => $course['id'],
            'threadId' => $thread['id']
        ));

        $isManager = $this->getCourseService()->canManageCourse($course['id'], $currentLoopTime);
        return $this->render('CustomWebBundle:LessonQuestionPlugin:show.html.twig', array(
            'course'    => $course,
            'thread'    => $thread,
            'threader'  => $threader,
            'posts'     => $posts,
            'users'     => $users,
            'isManager' => $isManager,
            'form'      => $form->createView()
        ));
    }

    public function createAction(Request $request)
    {
        $form = $this->createQuestionForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $question         = $form->getData();
                $question['type'] = 'question';
                $currentLoopTime  = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $question['courseId']);
                $thread           = $this->getThreadService()->createThread($question, $currentLoopTime);

                return $this->render("CustomWebBundle:LessonQuestionPlugin:item.html.twig", array(
                    'thread' => $thread,
                    'user'   => $this->getCurrentUser()
                ));
            } else {
                return $this->createJsonResponse(false);
            }
        }

        return $this->render("TopxiaWebBundle:LessonQuestionPlugin:form.html.twig", array(
            'course' => $course,
            'form'   => $form->createView()
        ));
    }

    public function answerAction(Request $request)
    {
        $form = $this->createPostForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $post            = $form->getData();
                $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $post['courseId']);
                $post            = $this->getThreadService()->createPost($post, $currentLoopTime);

                return $this->render('CustomWebBundle:LessonQuestionPlugin:post-item.html.twig', array(
                    'post'   => $post,
                    'user'   => $this->getUserService()->getUser($post['userId']),
                    'course' => $this->getCourseService()->getCourse($post['courseId'])
                ));
            } else {
                return $this->createJsonResponse(false);
            }
        }

        return $this->render("TopxiaWebBundle:LessonQuestionPlugin:form.html.twig", array(
            'course' => $course,
            'form'   => $form->createView()
        ));
    }
}
