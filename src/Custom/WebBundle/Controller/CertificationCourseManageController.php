<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Constants\LearnMode;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseManageController as BaseCourseManagerController;

class CertificationCourseManageController extends BaseCourseManagerController
{
    public function indexAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($course['locked'] == '1') {
            return $this->redirect($this->generateUrl('course_manage_course_sync', array('id' => $id, 'type' => 'base')));
        }

        return $this->baseAction($request, $id);
    }

    public function baseAction(Request $request, $id)
    {
        $course                = $this->getCourseService()->tryManageCourse($id);
        $courseSetting         = $this->getSettingService()->get('course', array());
        $areaCode              = $this->getAreaService()->getAreaByCode($course['areaCode']);
        $lessonCount           = $this->getCourseService()->searchLessonCount(array('courseId' => $id));
        $course['lessonCount'] = $lessonCount;

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
            $course                = $this->getCourseService()->updateCourse($id, $data);
            if ($data['oldCourseId'] && $data['oldCourseId'] != $id) {
                $this->getCourseService()->updateCourse($data['oldCourseId'], array("versionStatus" => "old"));
                $this->getCertificateService()->updateNotBuyUserCertCourse($course);
            }

            $this->setFlashMessage('success', '课程基本信息已保存！');
            return $this->redirect($this->generateUrl('course_certification_manage_base', array('id' => $id)));
        }

        $tags    = $this->getTagService()->findTagsByIds($course['tags']);
        $default = $this->getSettingService()->get('default', array());

        $currentCertInfo  = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'], $course['certificationId']);
        $certificates = $this->getCertificateService()->findIndexCertCategoryByAreaCode($course['areaCode']);
        $areaCodes = array();
        $areas     = $this->getAreaService()->searchAreas(array(), array('id', 'DESC'), 0, PHP_INT_MAX);

        foreach ($areas as $area) {
            $areaCodes[$area['code']] = $area['name'];
        }

        return $this->render('CustomWebBundle:CourseManage:base_certification.html.twig', array(
            'course'       => $course,
            'area'         => $areaCode,
            'areaCodes'    => $areaCodes,
            'tags'         => ArrayToolkit::column($tags, 'name'),
            'currentCertInfo'  => $currentCertInfo,
            'certificates' => $certificates,
            'default'      => $default
        ));
    }

    public function detailAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($request->getMethod() == 'POST') {
            $detail              = $request->request->all();
            $detail['goals']     = (empty($detail['goals']) || !is_array($detail['goals'])) ? array() : $detail['goals'];
            $detail['audiences'] = (empty($detail['audiences']) || !is_array($detail['audiences'])) ? array() : $detail['audiences'];

            $this->getCourseService()->updateCourse($id, $detail);
            $this->setFlashMessage('success', '课程详细信息已保存！');

            return $this->redirect($this->generateUrl('course_certification_manage_detail', array('id' => $id)));
        }

        return $this->render('CustomWebBundle:CourseManage:detail.html.twig', array(
            'course' => $course
        ));
    }

    public function pictureAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        return $this->render('CustomWebBundle:CourseManage:picture.html.twig', array(
            'course' => $course
        ));
    }

    public function pictureCropAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();
            $this->getCourseService()->changeCoursePicture($course['id'], $data["images"]);
            return $this->redirect($this->generateUrl('course_certification_manage_picture', array('id' => $course['id'])));
        }

        $fileId                                      = $request->getSession()->get("fileId");
        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 480, 270);

        return $this->render('CustomWebBundle:CourseManage:picture-crop.html.twig', array(
            'course'      => $course,
            'pictureUrl'  => $pictureUrl,
            'naturalSize' => $naturalSize,
            'scaledSize'  => $scaledSize
        ));
    }

    public function priceAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        $canModifyPrice     = true;
        $teacherModifyPrice = $this->setting('course.teacher_modify_price', true);

        if (empty($teacherModifyPrice)) {
            if (!$this->getCurrentUser()->isAdmin()) {
                $canModifyPrice = false;
                goto response;
            }
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            if (isset($fields['price'])) {
                $this->getCourseService()->setCoursePrice($course['id'], 'default', $fields['price']);
                unset($fields['price']);
            }

            if (!empty($fields)) {
                $course = $this->getCourseService()->updateCourse($id, $fields);
            } else {
                $course = $this->getCourseService()->getCourse($id);
            }

            $this->setFlashMessage('success', '课程价格已经修改成功!');
        }

        response:

        if ($this->isPluginInstalled("Vip") && $this->setting('vip.enabled')) {
            $levels = $this->getLevelService()->findEnabledLevels();
        } else {
            $levels = array();
        }

        if (($course['discountId'] > 0) && ($this->isPluginInstalled("Discount"))) {
            $discount = $this->getDiscountService()->getDiscount($course['discountId']);
        } else {
            $discount = null;
        }

        return $this->render('CustomWebBundle:CourseManage:price.html.twig', array(
            'course'         => $course,
            'canModifyPrice' => $canModifyPrice,
            'levels'         => $this->makeLevelChoices($levels),
            'discount'       => $discount
        ));
    }

    public function teachersAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($request->getMethod() == 'POST') {
            $data        = $request->request->all();
            $data['ids'] = empty($data['ids']) ? array() : array_values($data['ids']);

            $teachers = array();

            foreach ($data['ids'] as $teacherId) {
                $teachers[] = array(
                    'id'        => $teacherId,
                    'isVisible' => empty($data['visible_'.$teacherId]) ? 0 : 1
                );
            }

            $this->getCourseService()->setCourseTeachers($id, $teachers);

            $classroomIds = $this->getClassroomService()->findClassroomIdsByCourseId($id);

            if ($classroomIds) {
                $this->getClassroomService()->updateClassroomTeachers($classroomIds[0]);
            }

            $this->setFlashMessage('success', '教师设置成功！');

            return $this->redirect($this->generateUrl('course_certification_manage_teachers', array('id' => $id)));
        }

        $teacherMembers = $this->getCourseService()->findCourseTeachers($id);

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($teacherMembers, 'userId'));

        $teachers = array();

        foreach ($teacherMembers as $member) {
            if (empty($users[$member['userId']])) {
                continue;
            }

            $teachers[] = array(
                'id'        => $member['userId'],
                'nickname'  => $users[$member['userId']]['nickname'],
                'avatar'    => $this->getWebExtension()->getFilePath($users[$member['userId']]['smallAvatar'], 'avatar.png'),
                'isVisible' => $member['isVisible'] ? true : false
            );
        }

        return $this->render('CustomWebBundle:CourseManage:teachers.html.twig', array(
            'course'   => $course,
            'teachers' => $teachers
        ));
    }

    public function dataAction($id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        $isLearnedNum = $this->getCourseService()->searchMemberCount(array('isLearned' => 1, 'courseId' => $id));

        $learnTime = $this->getCourseService()->searchLearnTime(array('courseId' => $id));
        $learnTime = $course["studentNum"] == 0 ? 0 : intval($learnTime / $course["studentNum"]);

        $noteCount = $this->getNoteService()->searchNoteCount(array('courseId' => $id));

        $questionCount = $this->getThreadService()->searchThreadCount(array('courseId' => $id, 'type' => 'question'));

        $lessons = $this->getCourseService()->searchLessons(array('courseId' => $id), array('seq', 'ASC'), 0, 1000);

        foreach ($lessons as $key => $value) {
            $lessonLearnedNum = $this->getCourseService()->findLearnsCountByLessonId($value['id']);

            $finishedNum = $this->getCourseService()->searchLearnCount(array('status' => 'finished', 'lessonId' => $value['id']));

            $lessonLearnTime = $this->getCourseService()->searchLearnTime(array('lessonId' => $value['id']));
            $lessonLearnTime = $lessonLearnedNum == 0 ? 0 : intval($lessonLearnTime / $lessonLearnedNum);

            $lessonWatchTime = $this->getCourseService()->searchWatchTime(array('lessonId' => $value['id']));
            $lessonWatchTime = $lessonWatchTime == 0 ? 0 : intval($lessonWatchTime / $lessonLearnedNum);

            $lessons[$key]['LearnedNum']  = $lessonLearnedNum;
            $lessons[$key]['length']      = intval($lessons[$key]['length'] / 60);
            $lessons[$key]['finishedNum'] = $finishedNum;
            $lessons[$key]['learnTime']   = $lessonLearnTime;
            $lessons[$key]['watchTime']   = $lessonWatchTime;

            if ($value['type'] == 'testpaper') {
                $paperId  = $value['mediaId'];
                $score    = $this->getTestpaperService()->searchTestpapersScore(array('testId' => $paperId));
                $paperNum = $this->getTestpaperService()->searchTestpaperResultsCount(array('testId' => $paperId));

                $lessons[$key]['score'] = $finishedNum == 0 ? 0 : intval($score / $paperNum);
            }
        }

        return $this->render('CustomWebBundle:CourseManage:learning-data.html.twig', array(
            'course'        => $course,
            'isLearnedNum'  => $isLearnedNum,
            'learnTime'     => $learnTime,
            'noteCount'     => $noteCount,
            'questionCount' => $questionCount,
            'lessons'       => $lessons
        ));
    }

    public function orderAction(Request $request, $id)
    {
        $this->getCourseService()->tryManageCourse($id);

        $courseSetting = $this->setting("course");

        if (!$this->getCurrentUser()->isAdmin() && (empty($courseSetting["teacher_search_order"]) || $courseSetting["teacher_search_order"] != 1)) {
            throw $this->createAccessDeniedException("查询订单已关闭，请联系管理员");
        }

        $conditions               = $request->query->all();
        $type                     = 'course';
        $conditions['targetType'] = $type;

        if (isset($conditions['keywordType'])) {
            $conditions[$conditions['keywordType']] = trim($conditions['keyword']);
        }

        $conditions['targetId'] = $id;
        $course                 = $this->getCourseService()->tryManageCourse($id);

        if (!empty($conditions['startDateTime']) && !empty($conditions['endDateTime'])) {
            $conditions['startTime'] = strtotime($conditions['startDateTime']);
            $conditions['endTime']   = strtotime($conditions['endDateTime']);
        }

        $paginator = new Paginator(
            $request,
            $this->getOrderService()->searchOrderCount($conditions),
            10
        );

        $orders = $this->getOrderService()->searchOrders(
            $conditions,
            'latest',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($orders, 'userId'));

        foreach ($orders as $index => $expiredOrderToBeUpdated) {
            if ((($expiredOrderToBeUpdated["createdTime"] + 48 * 60 * 60) < time()) && ($expiredOrderToBeUpdated["status"] == 'created')) {
                $this->getOrderService()->cancelOrder($expiredOrderToBeUpdated['id']);
                $orders[$index]['status'] = 'cancelled';
            }
        }

        return $this->render('CustomWebBundle:CourseManage:course-order.html.twig', array(
            'course'    => $course,
            'request'   => $request,
            'orders'    => $orders,
            'users'     => $users,
            'paginator' => $paginator
        ));
    }

    /**
     * 
     * @return json   
     * array(
     *      'courseCount' => 4,   //该学制, 该证书下有多少门课程   
     *      'oldCourseId'  => 4,
     *      'oldCourseName' => '课程'
     * )
     */
    public function queryCertStatusAction(Request $request)
    {
        $areaCode        = $request->get('areaCode');
        $certificationId = $request->get('certificationId');
        $learnModeMonths      = $request->get('learnModeMonths');
        $partIndex       = $request->get('partIndex');

        $certification   = $this->getCertificateService()->findByAreaCodeAndCertificationId($areaCode, $certificationId);
        $courseCount = LearnMode::getCourseCount($learnModeMonths, $certification['months']);

        $conditions = array(
            'areaCode' => $areaCode,
            'type' => 'certification',
            'certificationId' => $certificationId,
            'learnModeMonths' => $learnModeMonths,
            'partIndex' => $partIndex,
            'versionStatus' => 'latest'
        );

        if (!empty($request->get('courseId'))) {
            $conditions['excludeIds'] = array($request->get('courseId'));
        }
        $oldCourses = $this->getCourseService()->searchCourses($conditions, null, 0, PHP_INT_MAX);

        $responeArray = array();

        if (!empty($oldCourses)) {
            $responeArray['oldCourseName'] = $oldCourses[0]['title'];
            $responeArray['oldCourseId'] = $oldCourses[0]['id'];
        }
        return $this->createJsonResponse($responeArray);
    }

    private function checkCourse($conditions, $partIndex)
    {
        $courses = $this->getCourseService()->searchCourses($conditions, null, 0, PHP_INT_MAX);

        foreach ($courses as $course) {
            if ($course['partIndex'] == $partIndex) {
                return $course;
            }
        }

        return array();
    }

    public function isCreatedCertificateCourseAction($areaCode, $certificationId, $partIndex)
    {
        $conditions['parentId'] = 0;
        $conditions['areaCode'] = $areaCode;
        $conditions['type']     = "certification";
        $count                  = $this->getCourseService()->searchCourseCount($conditions);
        $courses                = $this->getCourseService()->searchCourses(
            $conditions, null, 0, $count
        );

        foreach ($courses as $key => $course) {
            if ($courses[$key]['areaCode'] == $areaCode && $courses[$key]['certificationId'] == $certificationId && $courses[$key]['partIndex'] == $partIndex) {
                return array("status" => true, "courseTitle" => $courses[$key]['title'], "oldCourseId" => $courses[$key]['id']);
            }
        }

        return array("status" => false, "courseTitle" => null);
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }
}
