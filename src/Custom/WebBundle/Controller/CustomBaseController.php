<?php
namespace Custom\WebBundle\Controller;

use Custom\WebBundle\Controller\TextResponse;
use Topxia\WebBundle\Controller\BaseController as TopxiaBaseController;

abstract class CustomBaseController extends TopxiaBaseController
{
    protected function createTextResponse($data)
    {
        return new TextResponse($data);
    }
}
