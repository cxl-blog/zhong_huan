<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\SmsToolkit;
use Topxia\Service\Common\Mail;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\PasswordResetController as BaseController;

class PasswordResetController extends BaseController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $data = array('email' => '');

        if ($user->isLogin()) {
            if (!$user['setup'] || stripos($user['email'], '@edusoho.net') != false) {
                return $this->redirect($this->generateUrl('settings_setup'));
            } else {
                $data['email'] = $user['email'];
            }
        }

        $form = $this->createFormBuilder($data)
                     ->add('email', 'email')
                     ->getForm();

        $error = null;

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $user = $this->getUserService()->getUserByEmail($data['email']);

                if (empty($user)) {
                    list($result, $message) = $this->getAuthService()->checkEmail($data['email']);

                    if ($result == 'error_duplicate') {
                        $error = '请通过论坛找回密码';
                        return $this->render("TopxiaWebBundle:PasswordReset:index.html.twig", array(
                            'form'  => $form->createView(),
                            'error' => $error
                        ));
                    }
                }

                if ($user) {
                    $token = $this->getUserService()->makeToken('password-reset', $user['id'], strtotime('+1 day'));
                    try {
                        $normalMail = array(
                            'to'     => $user['email'],
                            'title'  => "重设{$user['nickname']}在{$this->setting('site.name', 'EDUSOHO')}的密码",
                            'body'   => $this->renderView('TopxiaWebBundle:PasswordReset:reset.txt.twig', array(
                                'user'  => $user,
                                'token' => $token
                            )),
                            'format' => 'html'
                        );
                        $cloudMail = array(
                            'to'        => $user['email'],
                            'template'  => 'email_reset_password',
                            'verifyurl' => $this->generateUrl('password_reset_update', array('token' => $token), true),
                            'nickname'  => $user['nickname']
                        );
                        $mail = new Mail($normalMail, $cloudMail);
                        $this->sendEmail($mail);
                    } catch (\Exception $e) {
                        $this->getLogService()->error('user', 'password-reset', '重设密码邮件发送失败:'.$e->getMessage());
                        return $this->createMessageResponse('error', '重设密码邮件发送失败，请联系管理员。');
                    }

                    $this->getLogService()->info('user', 'password-reset', "{$user['email']}向发送了找回密码邮件。");

                    return $this->render('TopxiaWebBundle:PasswordReset:sent.html.twig', array(
                        'user'          => $user,
                        'emailLoginUrl' => $this->getEmailLoginUrl($user['email'])
                    ));
                } else {
                    $error = '该邮箱地址没有注册过帐号';
                }
            }
        }

        return $this->render("CustomWebBundle:PasswordReset:index.html.twig", array(
            'form'  => $form->createView(),
            'error' => $error
        ));
    }

    
}
