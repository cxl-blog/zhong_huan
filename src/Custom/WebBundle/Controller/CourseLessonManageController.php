<?php
namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\Util\EdusohoLiveClient;
use Topxia\WebBundle\Controller\CourseLessonManageController as BaseController;

class CourseLessonManageController extends BaseController
{
    public function editAction(Request $request, $courseId, $lessonId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($course['id'], $lessonId);

        if (empty($lesson)) {
            throw $this->createNotFoundException("课时(#{$lessonId})不存在！");
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            if ($fields['media']) {
                $fields['media'] = json_decode($fields['media'], true);
            }

            if ($fields['second']) {
                $fields['length'] = $this->textToSeconds($fields['minute'], $fields['second']);
                unset($fields['minute']);
                unset($fields['second']);
            }

            $fields['free'] = empty($fields['free']) ? 0 : 1;
            $lesson         = $this->getCourseService()->updateLesson($course['id'], $lesson['id'], $fields);
            $this->getCourseService()->deleteCourseDrafts($course['id'], $lesson['id'], $this->getCurrentUser()->id);

            $file = false;

            if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
                $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
                $lesson['mediaStatus'] = $file['convertStatus'];

                if ($file['type'] == "document" && $file['convertStatus'] == "none") {
                    $convertHash = $this->getUploadFileService()->reconvertFile(
                        $file['id'],
                        $this->generateUrl('uploadfile_cloud_convert_callback2', array(), true)
                    );
                }
            }

            $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);

            return $this->renderListItemPage($request,
                array(
                    'course' => $course,
                    'lesson' => $lesson,
                    'file'   => $file
                )
            );
        }

        $file = null;

        if ($lesson['mediaId']) {
            $file = $this->getUploadFileService()->getFile($lesson['mediaId']);

            if (!empty($file)) {
                $lesson['media'] = array(
                    'id'     => $file['id'],
                    'status' => $file['convertStatus'],
                    'source' => 'self',
                    'name'   => $file['filename'],
                    'uri'    => ''
                );
            } else {
                $lesson['media'] = array('id' => 0, 'status' => 'none', 'source' => '', 'name' => '文件已删除', 'uri' => '');
            }
        } else {
            $lesson['media'] = array(
                'id'     => 0,
                'status' => 'none',
                'source' => $lesson['mediaSource'],
                'name'   => $lesson['mediaName'],
                'uri'    => $lesson['mediaUri']
            );
        }

        list($lesson['minute'], $lesson['second']) = $this->secondsToText($lesson['length']);

        $user       = $this->getCurrentUser();
        $userId     = $user['id'];
        $randString = substr(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36), 0, 12);
        $filePath   = "courselesson/{$course['id']}";
        $fileKey    = "{$filePath}/".$randString;
        $convertKey = $randString;

        $targetType = 'courselesson';
        $targetId   = $course['id'];
        $draft      = $this->getCourseService()->findCourseDraft($courseId, $lessonId, $userId);
        $setting    = $this->setting('storage');

// if ($setting['upload_mode'] == 'local') {

//     // $videoUploadToken = $audioUploadToken = $pptUploadToken = array(

//     //     'token' => $this->getUserService()->makeToken('fileupload', $user['id'], strtotime('+ 2 hours')),

//     //     'url' => $this->generateUrl('uploadfile_upload', array('targetType' => $targetType, 'targetId' => $targetId)),

//     // );

// } else {

        // }
        $lesson['title'] = str_replace(array('"', "'"), array('&#34;', '&#39;'), $lesson['title']);

        $features = $this->container->hasParameter('enabled_features') ? $this->container->getParameter('enabled_features') : array();

        return $this->render('CustomWebBundle:CourseLessonManage:lesson-modal.html.twig', array(
            'course'         => $course,
            'lesson'         => $lesson,
            'file'           => $file,
            'targetType'     => $targetType,
            'targetId'       => $targetId,
            'filePath'       => $filePath,
            'fileKey'        => $fileKey,
            'convertKey'     => $convertKey,
            'storageSetting' => $setting,
            'features'       => $features,
            'draft'          => $draft
        ));
    }

    // @todo refactor it.
    public function createAction(Request $request, $id)
    {
        $course   = $this->getCourseService()->tryManageCourse($id);
        $parentId = $request->query->get('parentId');

        if ($request->getMethod() == 'POST') {
            $lesson             = $request->request->all();
            $lesson['courseId'] = $course['id'];

            if ($lesson['media']) {
                $lesson['media'] = json_decode($lesson['media'], true);
            }

            if (is_numeric($lesson['second'])) {
                $lesson['length'] = $this->textToSeconds($lesson['minute'], $lesson['second']);
                unset($lesson['minute']);
                unset($lesson['second']);
            }

            $lesson = $this->getCourseService()->createLesson($lesson);

            $file = false;

            if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
                $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
                $lesson['mediaStatus'] = $file['convertStatus'];
            }

            $lessonId = 0;
            $this->getCourseService()->deleteCourseDrafts($id, $lessonId, $this->getCurrentUser()->id);
            $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($id);

            return $this->renderListItemPage($request,
                array(
                    'course' => $course,
                    'lesson' => $lesson,
                    'file'   => $file
                )
            );
        }

        $user       = $this->getCurrentUser();
        $userId     = $user['id'];
        $randString = substr(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36), 0, 12);
        $filePath   = "courselesson/{$course['id']}";
        $fileKey    = "{$filePath}/".$randString;
        $convertKey = $randString;

        $targetType = 'courselesson';
        $targetId   = $course['id'];
        $draft      = $this->getCourseService()->findCourseDraft($targetId, 0, $userId);
        $setting    = $this->setting('storage');

        $features = $this->container->hasParameter('enabled_features') ? $this->container->getParameter('enabled_features') : array();

        return $this->render('CustomWebBundle:CourseLessonManage:lesson-modal.html.twig', array(
            'course'         => $course,
            'targetType'     => $targetType,
            'targetId'       => $targetId,
            'filePath'       => $filePath,
            'fileKey'        => $fileKey,
            'convertKey'     => $convertKey,
            'storageSetting' => $setting,
            'features'       => $features,
            'parentId'       => $parentId,
            'draft'          => $draft
        ));
    }

    public function deleteAction(Request $request, $courseId, $lessonId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        if ($course['type'] == 'live') {
            $client = new EdusohoLiveClient();

            if ($lesson['type'] == 'live') {
                $result = $client->deleteLive($lesson['mediaId'], $lesson['liveProvider']);
            }

            $this->getCourseService()->deleteCourseLessonReplayByLessonId($lessonId);
        }

        //$this->getCourseDeleteService()->deleteLessonResult($lesson['mediaId']);
        $this->getCourseService()->deleteLesson($course['id'], $lessonId);
        $this->getCourseMaterialService()->deleteMaterialsByLessonId($lessonId);

        if ($this->isPluginInstalled('Homework')) {
            //如果安装了作业插件那么也删除作业和练习
            $homework = $this->getHomeworkService()->getHomeworkByLessonId($lessonId);

            if (!empty($homework)) {
                $this->getHomeworkService()->removeHomework($homework['id']);
            }

            $this->getExerciseService()->deleteExercisesByLessonId($lesson['id']);
        }

        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);

        return $this->createJsonResponse(true);
    }

    public function createTestPaperAction(Request $request, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($id);

        if ($request->getMethod() == 'POST') {
            $lesson                  = $request->request->all();
            $lesson['type']          = 'testpaper';
            $lesson['courseId']      = $course['id'];
            $lesson['testStartTime'] = isset($lesson['testStartTime']) ? strtotime($lesson['testStartTime']) : 0;

            if (!$lesson['testStartTime']) {
                unset($lesson['testStartTime']);
            }

            $lesson = $this->getCourseService()->createLesson($lesson);

            $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($id);
            return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
                'course' => $course,
                'lesson' => $lesson
            ));
        }

        $parentId             = $request->query->get('parentId');
        $conditions           = array();
        $conditions['target'] = "course-{$course['id']}";
        $conditions['status'] = 'open';

        $testpapers = $this->getTestpaperService()->searchAndDistinctTestpapers(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            1000
        );

        $paperOptions = array();

        foreach ($testpapers as $testpaper) {
            $paperOptions[$testpaper['id']] = $testpaper['name'];
        }

        $features = $this->container->hasParameter('enabled_features') ? $this->container->getParameter('enabled_features') : array();

        return $this->render('CustomWebBundle:CourseTestpaperManage:testpaper-modal.html.twig', array(
            'course'       => $course,
            'paperOptions' => $paperOptions,
            'features'     => $features,
            'parentId'     => $parentId
        ));
    }

    public function editTestpaperAction(Request $request, $courseId, $lessonId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);

        $lesson = $this->getCourseService()->getCourseLesson($course['id'], $lessonId);

        if (empty($lesson)) {
            throw $this->createNotFoundException("课时(#{$lessonId})不存在！");
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            if (!empty($fields['testStartTime'])) {
                $fields['testStartTime'] = strtotime($fields['testStartTime']);
            }

            $lesson = $this->getCourseService()->updateLesson($course['id'], $lesson['id'], $fields);
            $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);
            return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
                'course' => $course,
                'lesson' => $lesson
            ));
        }

        $conditions           = array();
        $conditions['target'] = "course-{$course['id']}";
        $conditions['status'] = 'open';

        $testpapers = $this->getTestpaperService()->searchAndDistinctTestpapers(
            $conditions,
            array('createdTime', 'DESC'),
            0,
            1000
        );

        $paperOptions = array();

        foreach ($testpapers as $paper) {
            $paperOptions[$paper['id']] = $paper['name'];
        }

        $features = $this->container->hasParameter('enabled_features') ? $this->container->getParameter('enabled_features') : array();

        return $this->render('TopxiaWebBundle:CourseTestpaperManage:testpaper-modal.html.twig', array(
            'course'       => $course,
            'lesson'       => $lesson,
            'paperOptions' => $paperOptions,
            'features'     => $features

        ));
    }

    public function publishAction(Request $request, $courseId, $lessonId)
    {
        $this->getCourseService()->publishLesson($courseId, $lessonId);
        $course = $this->getCourseService()->getCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        $file = false;

        if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
            $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
            $lesson['mediaStatus'] = $file['convertStatus'];
        }

        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);
        return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
            'course' => $course,
            'lesson' => $lesson,
            'file'   => $file
        ));
    }

    public function unpublishAction(Request $request, $courseId, $lessonId)
    {
        $this->getCourseService()->unpublishLesson($courseId, $lessonId);

        $course = $this->getCourseService()->getCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);
        $file   = false;

        if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
            $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
            $lesson['mediaStatus'] = $file['convertStatus'];
        }

        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);
        return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
            'course' => $course,
            'lesson' => $lesson,
            'file'   => $file
        ));
    }

    private function renderListItemPage(Request $request, $params)
    {
        $isCertificationPage = strpos($request->server->get('HTTP_REFERER'), 'certification');

        if ($isCertificationPage) {
            return $this->render('CustomWebBundle:CourseLessonManage:list-item_certification.html.twig', $params);
        } else {
            return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', $params);
        }
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }
}
