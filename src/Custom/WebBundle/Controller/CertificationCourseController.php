<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Custom\Common\Constants\LearnMode;
use Custom\Common\Util\ImageUrlUtils;
use Custom\Common\Util\CourseHeaderUtils;

class CertificationCourseController extends CourseController
{
    public function headerAction(Request $request, $course, $manage = false)
    {
        $user = $this->getCurrentUser();

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $course['id']);
        $member          = $this->getCourseService()->getCourseMember($course['id'], $user['id'], $currentLoopTime);

        $users = empty($course['teacherIds']) ? array() : $this->getUserService()->findUsersByIds($course['teacherIds']);

        if (empty($member)) {
            $member['deadline'] = 0;
            $member['levelId']  = 0;
        }

        $isNonExpired = $this->getCourseService()->isMemberNonExpired($course, $member);

        if ($member['levelId'] > 0) {
            $vipChecked = $this->getVipService()->checkUserInMemberLevel($user['id'], $course['vipLevelId']);
        } else {
            $vipChecked = 'ok';
        }

        if ($this->isBecomeStudentFromCourse($member)
            || $this->isBecomeStudentFromClassroomButExitedClassroom($course, $member, $user)) {
            $canExit = true;
        } else {
            $canExit = false;
        }

        $isSchoolAdmin = $this->getCourseService()->canBranchAdminMangeCourse($course['id']) ? true : false;
        return $this->render('CustomWebBundle:Course:header.html.twig', array(
            'course'       => $course,
            'canManage'    => $this->getCourseService()->canManageCourse($course['id']),
            'canExit'      => $canExit,
            'member'       => $member,
            'users'        => $users,
            'manage'       => $manage,
            'isNonExpired' => $isNonExpired,
            'vipChecked'   => $vipChecked,
            'isAdmin'      => $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') || $isSchoolAdmin
        ));
    }

    public function createAction(Request $request)
    {
        $user        = $this->getUserService()->getCurrentUser();
        $userProfile = $this->getUserService()->getUserProfile($user['id']);

        $isLive = $request->query->get('flag');
        $type   = ($isLive == "isLive") ? 'live' : $this->getFilterType();

        if ($type == 'live') {
            $courseSetting = $this->setting('course', array());

            if (empty($courseSetting['live_course_enabled'])) {
                return $this->createMessageResponse('info', '请前往后台开启直播,尝试创建！');
            }

            if (!empty($courseSetting['live_course_enabled'])) {
                $client   = new EdusohoLiveClient();
                $capacity = $client->getCapacity();
            } else {
                $capacity = array();
            }

            if (empty($capacity['capacity']) && !empty($courseSetting['live_course_enabled'])) {
                return $this->createMessageResponse('info', '请联系EduSoho官方购买直播教室，然后才能开启直播功能！');
            }
        }

        if (false === $this->get('security.context')->isGranted('ROLE_TEACHER')) {
            throw $this->createAccessDeniedException();
        }

        if ($request->getMethod() == 'POST') {
            $fields = $request->request->all();

            $course = $this->getCourseService()->createCourse($fields);
            $field  = array(
                'areaCode'        => $fields['areaCode'],
                'certificationId' => $fields['certificationId'],
                'learnModeMonths' => $fields['learnModeMonths']
            );
            $field['partIndex']   = isset($fields['partIndex']) ? $fields['partIndex'] : 0;
            $course                 = $this->getCourseService()->updateCourse($course['id'], $field);

            $this->getCourseService()->updateOtherCoursesAsOld($course);
            $this->getCertificateService()->updateNotBuyUserCertCourse($course);
            return $this->redirect(
                $this->generateUrl('course_certification_manage', array('id' => $course['id']))
            );
        }

        $areaCodes = array();
        $areas     = $this->getAreaService()->searchAreas(array(), array('id', 'DESC'), 0, 999);

        foreach ($areas as $area) {
            $areaCodes[$area['code']] = $area['name'];
        }
        
        $createPage = "CustomWebBundle:Course:create_{$this->getFilterType()}.html.twig";
        return $this->render($createPage, array(
            'userProfile' => $userProfile,
            'type'        => $type,
            'areaCodes'   => $areaCodes
        ));
    }
    
    public function allCertificationCoursesBlockGridAction($courses)
    {
        return $this->render("CustomWebBundle:Course:certificationCourseGrid_all.html.twig", array(
            'courses' => $courses
        ));
    }

    public function singleCertificationCourseBlockGridAction($course, $isApprovaled = null, $userId = null)
    {
        return $this->render("CustomWebBundle:Course:certificationCourseGrid_single.html.twig", array(
            'course'              => $course,
            'learnPercent'        => $course['learnPercent']
        ));
    }

    public function showAction(Request $request, $id)
    {
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }

        $user = $this->getCurrentUser();

        if (empty($member)) {
            $member = $this->getCourseService()->becomeStudentByClassroomJoined($id, $user->id);

            if (isset($member["id"])) {
                $course['studentNum']++;
            }
        }

        $this->getCourseService()->hitCourse($id);
        $items = $this->getCourseService()->getCourseItems($course['id']);

        $infos = array(
            'items' => $items,
            'member' => $member
        );

        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);
        $currentUserLearnTime = $this->getUserLearnTimeService()->getCurrentLearnTimeInfo(
            $user['id'],
            null,
            $course['areaCode'],
            $course['certificationId']
        );
        $headerInfos = array_merge($headerInfos, $currentUserLearnTime);
        
        return $this->render('CustomWebBundle:Course:certification-show.html.twig', $headerInfos);
    }

    public function showCertOrNormalAction(Request $request, $id)
    {
        $course = $this->getCourseService()->getCourse($id);
        if ($course['type'] == 'certification') {
            return $this->redirect(
                $this->generateUrl('course_certification_show', array('id' => $id))
            );
        } else {
            return $this->redirect(
                $this->generateUrl('course_show', array('id' => $id))
            );
        }
        
    }

    public function infoAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }

        $category = $this->getCategoryService()->getCategory($course['categoryId']);
        $tags     = $this->getTagService()->findTagsByIds($course['tags']);

        $infos = array(
            'member' => $member,
            'tags' => $tags,
            'category' => $category
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);

        return $this->render('CustomWebBundle:Course:info.html.twig', $headerInfos);
    }

    public function LessonListAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', '非常抱歉，您无权限访问该班级，如有需要请联系客服', '', 3, $this->generateUrl('homepage'));
            }
        }

        $infos = array(
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getHeaderInfos($this, $member, $user, $course, $request, $infos);

        return $this->render('CustomWebBundle:Course:lesson-list.html.twig', $headerInfos);
    }

    public function recordLearningTimeAction(Request $request, $lessonId, $time)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            $this->createAccessDeniedException();
        }

        $lesson          = $this->getCourseService()->getLesson($lessonId);
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $lesson['courseId']);

        $this->getCourseService()->waveLearningTime($user['id'], $lessonId, $time, $currentLoopTime);

        return $this->createJsonResponse(true);
    }

    public function recordWatchingTimeAction(Request $request, $lessonId, $time)
    {
        $user = $this->getCurrentUser();

        if (!$user->isLogin()) {
            $this->createAccessDeniedException();
        }

        $lesson          = $this->getCourseService()->getLesson($lessonId);
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $lesson['courseId']);
        $learn           = $this->getCourseService()->waveWatchingTime($user['id'], $lessonId, $time, $currentLoopTime);

        $isLimit = $this->setting('magic.lesson_watch_limit');

        if ($isLimit) {
            $course         = $this->getCourseService()->getCourse($lesson['courseId']);
            $watchLimitTime = $course['watchLimit'] * $lesson['length'];

            if ($lesson['type'] == 'video' && ($course['watchLimit'] > 0) && ($learn['watchTime'] >= $watchLimitTime)) {
                $learn['watchLimited'] = true;
            }
        }

        return $this->createJsonResponse($learn);
    }

    public function recordLessonViewWatchingTimeAction($lessonViewId, $time)
    {
        if (!$lessonViewId || !$time) {
            return $this->createJsonResponse(false);
        }

        $lessonView = $this->getCourseService()->waveLessonViewWatchingTime($lessonViewId, $time);

        return $this->createJsonResponse($lessonView);
    }

    private function isBecomeStudentFromCourse($member)
    {
        return isset($member["role"]) && isset($member["joinedType"]) && $member["role"] == 'student' && $member["joinedType"] == 'course';
    }

    private function isBecomeStudentFromClassroomButExitedClassroom($course, $member, $user)
    {
        $classroomMembers     = $this->getClassroomService()->getClassroomMembersByCourseId($course["id"], $user->id);
        $classroomMemberRoles = ArrayToolkit::column($classroomMembers, "role");

        return isset($member["joinedType"]) && $member["joinedType"] == 'classroom' && (empty($classroomMemberRoles) || count($classroomMemberRoles) == 0);
    }

    public function previewAction(Request $request, $id, $type)
    {
        $user      = $this->getCurrentUser();
        $starttime = $request->query->get('starttime', '');

        if (!$user->isLogin()) {
            $request->getSession()->set('_target_path', $this->generateUrl('course_show', array('id' => $id)));

            return $this->createMessageResponse('info', '你好像忘了登录哦？', null, 3000, $this->generateUrl('login'));
        }

        $course = $this->getCourseService()->getCourse($id);

        if (empty($course)) {
            throw $this->createNotFoundException("课程不存在，或已删除。");
        }

        if ($course['approval'] == 1 && ($user['approvalStatus'] != 'approved')) {
            return $this->createMessageResponse('info', "该课程需要通过实名认证，你还没有通过实名认证。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);

        if (!$this->getCourseService()->canTakeCourse($id, $currentLoopTime)) {
            return $this->createMessageResponse('info', "您还不是课程《{$course['title']}》的学员，请先购买或加入学习。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        try {
            list($course, $member) = $this->getCourseService()->tryTakeCourse($id, $currentLoopTime);

            if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
                return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
            }

            if ($member && $member['levelId'] > 0) {
                if ($this->getVipService()->checkUserInMemberLevel($member['userId'], $course['vipLevelId']) != 'ok') {
                    return $this->redirect($this->generateUrl('course_show', array('id' => $id)));
                }
            }
        } catch (Exception $e) {
            throw $this->createAccessDeniedException('抱歉，未发布课程不能学习！');
        }

        $area = $this->getAreaService()->getAreaByCode($course['areaCode']);

        return $this->render('CustomWebBundle:Course:learn.html.twig', array(
            'type'            => $type,
            'faceImgPath'     => ImageUrlUtils::getImagePath($user['faceImgPath']),
            'course'          => $course,
            'area'            => $area,
            'starttime'       => $starttime,
            'previewQuestion' => true //弹题预览
        ));
    }

    public function querySessionAction(Request $request)
    {
        $rememberMe = $request->cookies->get('REMEMBERME');

        if ($rememberMe) {
            setcookie('REMEMBERME', 0, time() - 10, '/');
        }

        $user = $this->getCurrentUser()->islogin();

        if (!$user) {
            return $this->createJsonResponse('false');
        }

        return $this->createJsonResponse('true');
    }

    public function updateAsLatest(Request $request, $id)
    {
        $this->getCourseService()->updateAsLatestCourse($id);
    }

    public function favoriteAction(Request $request, $id)
    {
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()['id'], $id);
        $this->getCourseService()->favoriteCourse($id, $currentLoopTime);

        return $this->createJsonResponse(true);
    }

    public function unfavoriteAction(Request $request, $id)
    {
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()['id'], $id);
        $this->getCourseService()->unfavoriteCourse($id, $currentLoopTime);

        return $this->createJsonResponse(true);
    }

    protected function getStatisticsService()
    {
        return $this->getServiceKernel()->createService('Custom:System.StatisticsService');
    }

    protected function getFilterType()
    {
        return 'certification';
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getUserLearnTimeService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.UserLearnTimeService');
    }

    protected function getTestpaperService()
    {
        return $this->getServiceKernel()->createService('Custom:Testpaper.TestpaperService');
    }
}
