<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\SmsToolkit;
use Topxia\Common\FileToolkit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Topxia\WebBundle\Controller\SettingsController as BaseController;

class SettingsController extends BaseController
{
    public function bindMobileAction(Request $request)
    {
        $currentUser       = $this->getCurrentUser()->toArray();
        $verifiedMobile    = '';
        $hasVerifiedMobile = (isset($currentUser['verifiedMobile']) && (strlen($currentUser['verifiedMobile']) > 0));

        if ($hasVerifiedMobile) {
            $verifiedMobile = $currentUser['verifiedMobile'];
        }

        $setMobileResult = 'none';

        $scenario = "sms_bind";
        
        if ($this->setting('cloud_sms.sms_enabled') != '1' || $this->setting("cloud_sms.{$scenario}") != 'on') {
            return $this->render('TopxiaWebBundle:Settings:edu-cloud-error.html.twig', array());
        }
        
        if ($request->getMethod() == 'POST') {
            $password = $request->request->get('password');
            
            if (!$this->getAuthService()->checkPassword($currentUser['id'], $password)) {
                $this->setFlashMessage('danger', '您的登录密码错误');
                SmsToolkit::clearSmsSession($request, $scenario);
                return $this->bindMobileReturn($hasVerifiedMobile, $setMobileResult, $verifiedMobile);
            }
            
            list($result, $sessionField, $requestField) = SmsToolkit::smsCheck($request, $scenario);

            if ($result) {
                $verifiedMobile = $sessionField['to'];

                $this->getChangeMobileService()->updateBranchChangeMobileInfo($currentUser['id'], $verifiedMobile);

                $this->sendPasswordMsg($verifiedMobile, $password);
                $setMobileResult = 'success';
                $this->setFlashMessage('success', '绑定成功');
            } else {
                $setMobileResult = 'fail';
                $this->setFlashMessage('danger', '绑定失败，原短信失效');
            }
        }

        return $this->bindMobileReturn($hasVerifiedMobile, $setMobileResult, $verifiedMobile);
    }

    protected function bindMobileReturn($hasVerifiedMobile, $setMobileResult, $verifiedMobile)
    {
        return $this->render('CustomWebBundle:Settings:bind-mobile.html.twig', array(
            'hasVerifiedMobile' => $hasVerifiedMobile,
            'setMobileResult'   => $setMobileResult,
            'verifiedMobile'    => $verifiedMobile
        ));
    }

    public function sendPasswordMsg($verifiedMobile, $password)
    {
        $smsType      = 'sms_zhonghuan_login_password';
        $contentParam = array(
            'password' => $password
        );

        $params = array(
            'mobile'     => $verifiedMobile,
            'category'   => $smsType,
            'parameters' => $contentParam
        );
        $logMessage = '用户重置手机号码时发送登录密码的短信';
        $this->getSmsService()->sendCustomMsg($params, $logMessage);
    }

    public function approvalSubmitAction(Request $request)
    {
        $user = $this->getCurrentUser();

        if ($request->getMethod() == 'POST') {
            $faceImg = $request->request->get('faceImg');
            $backImg = $request->request->get('backImg');
            $jobsSeniorityCardImg = $request->request->get('jobsSeniorityCardImg');
            $driverLicenseImg = $request->request->get('driverLicenseImg');
            $otherImg = $request->request->get('otherImg');

            $this->getUserService()->newBatchApplyUserApproval($user['id'], $faceImg, $backImg, $jobsSeniorityCardImg, $driverLicenseImg, $otherImg);

            $this->setFlashMessage('success', '实名认证提交成功！');
            return $this->redirect($this->generateUrl('settings'));
        }

        return $this->render('CustomWebBundle:Settings:approval.html.twig', array(
        ));
    }
    public function showUploadAction(Request $request)
    {
        $fileId     = $request->request->get('id');

        $objectFile = $this->getFileService()->getFileObject($fileId);

        if (!FileToolkit::isImageFile($objectFile)) {
            throw $this->createAccessDeniedException('图片格式不正确！');
        }

        $file   = $this->getFileService()->getFile($fileId);
        $parsed = $this->getFileService()->parseFileUri($file["uri"]);

        $face = $this->getSettingService()->get('face', array());

        $oldFileId            = empty($face['picture_file_id']) ? null : $face['picture_file_id'];
        $face['picture_file_id'] = $fileId;

        $filename =  date("Y-m-d",time())."-".$parsed['name'];
        $directory = "{$this->container->getParameter('topxia.upload.public_directory')}/approval/".$file['id']."/";
        $files = $objectFile->move($directory, $filename);

        $face['picture']         = "{$this->container->getParameter('topxia.upload.public_url_path')}/approval/".$file['id']."/".$filename;
        $face['picture']         = ltrim($face['picture'], '/');

        $this->getSettingService()->set('face', $face);

        $response = array(
            'path' => $face['picture'],
            'url'  => $this->container->get('templating.helper.assets')->getUrl($face['picture'])
        );

        return new JsonResponse($response);
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getChangeMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:User.ChangeMobileService');
    }

    protected function getSmsService()
    {
        return $this->getServiceKernel()->createService('Custom:Sms.SmsService');
    }

    public function profileAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $profile = $this->getUserService()->getUserProfile($user['id']);

        $profile['title'] = $user['title'];

        if ($request->getMethod() == 'POST') {
            $profile = $request->request->get('profile');

            if (!((strlen($user['verifiedMobile']) > 0) && (isset($profile['mobile'])))) {
                $this->getUserService()->updateUserProfile($user['id'], $profile);
                $this->setFlashMessage('success', '基础信息保存成功。');
            } else {
                $this->setFlashMessage('danger', '不能修改已绑定的手机。');
            }

            return $this->redirect($this->generateUrl('settings'));
        }

        $fields = $this->getUserFieldService()->getAllFieldsOrderBySeqAndEnabled();

        if (array_key_exists('idcard', $profile) && $profile['idcard'] == "0") {
            $profile['idcard'] = "";
        }

        $fromCourse = $request->query->get('fromCourse');

        if (!$user['setup'] || stripos($user['email'], '@eduoho.net') != false) {
            return $this->redirect($this->generateUrl('settings_setup'));
        }

        if ($profile['gender'] == 'male') {
            $gender = '男';
        } elseif ($profile['gender'] == 'female') {
            $gender = '女';
        } else {
            $gender = '保密';
        }

        $form = $this->createFormBuilder()
            ->add('avatar', 'file')
            ->getForm();

        $hasPartnerAuth = $this->getAuthService()->hasPartnerAuth();

        if ($hasPartnerAuth) {
            $partnerAvatar = $this->getAuthService()->getPartnerAvatar($user['id'], 'big');
        } else {
            $partnerAvatar = null;
        }

        $fromCourse = $request->query->get('fromCourse');

        $hasLoginPassword           = strlen($user['password']) > 0;
        $hasPayPassword             = strlen($user['password']) > 0;
        $userSecureQuestions        = $this->getUserService()->getUserSecureQuestionsByUserId($user['id']);
        $hasFindPayPasswordQuestion = (isset($userSecureQuestions)) && (count($userSecureQuestions) > 0);
        $hasVerifiedMobile          = (isset($user['verifiedMobile']) && (strlen($user['verifiedMobile']) > 0));

        $cloudSmsSetting = $this->getSettingService()->get('cloud_sms');
        $showBindMobile  = (isset($cloudSmsSetting['sms_enabled'])) && ($cloudSmsSetting['sms_enabled'] == '1')
            && (isset($cloudSmsSetting['sms_bind'])) && ($cloudSmsSetting['sms_bind'] == 'on');

        $itemScore     = floor(100.0 / (3.0 + ($showBindMobile ? 1.0 : 0)));
        $progressScore = 1 + ($hasLoginPassword ? $itemScore : 0) + ($hasPayPassword ? $itemScore : 0) + ($hasFindPayPasswordQuestion ? $itemScore : 0) + ($showBindMobile && $hasVerifiedMobile ? $itemScore : 0);

        if ($progressScore <= 1) {
            $progressScore = 0;
        }

        return $this->render('CustomWebBundle:Settings:profile.html.twig', array(
            'profile'                    => $profile,
            'fields'                     => $fields,
            'fromCourse'                 => $fromCourse,
            'user'                       => $user,
            'gender'                     => $gender,
            'progressScore'              => $progressScore,
            'hasLoginPassword'           => $hasLoginPassword,
            'hasPayPassword'             => $hasPayPassword,
            'hasFindPayPasswordQuestion' => $hasFindPayPasswordQuestion,
            'hasVerifiedMobile'          => $hasVerifiedMobile,
            'form'                       => $form->createView(),
            'user'                       => $this->getUserService()->getUser($user['id']),
            'partnerAvatar'              => $partnerAvatar,
            'fromCourse'                 => $fromCourse
        ));
    }

    public function avatarCropAction(Request $request)
    {
        $currentUser = $this->getCurrentUser();

        if ($request->getMethod() == 'POST') {
            $options = $request->request->all();
            $this->getUserService()->changeBatchAvatar($currentUser['id'], $options["images"]);
            return $this->redirect($this->generateUrl('settings_avatar'));
        }

        $fileId                                      = $request->getSession()->get("fileId");
        list($pictureUrl, $naturalSize, $scaledSize) = $this->getFileService()->getImgFileMetaInfo($fileId, 270, 270);

        return $this->render('CustomWebBundle:Settings:avatar-crop.html.twig', array(
            'pictureUrl'  => $pictureUrl,
            'naturalSize' => $naturalSize,
            'scaledSize'  => $scaledSize
        ));
    }

    public function passwordAction(Request $request)
    {
        $user = $this->getCurrentUser();

        if (empty($user['setup'])) {
            return $this->redirect($this->generateUrl('settings_setup'));
        }

        $form = $this->createFormBuilder()
                     ->add('currentPassword', 'password')
                     ->add('newPassword', 'password')
                     ->add('confirmPassword', 'password')
                     ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $passwords = $form->getData();

                if (!$this->getAuthService()->checkPassword($user['id'], $passwords['currentPassword'])) {
                    $this->setFlashMessage('danger', '当前密码不正确，请重试！');
                } else {
                    $this->getAuthService()->changeBatchPassword($user['id'], $passwords['currentPassword'], $passwords['newPassword']);

                    $this->setFlashMessage('success', '密码修改成功。');
                }

                return $this->redirect($this->generateUrl('settings_password'));
            }
        }

        return $this->render('TopxiaWebBundle:Settings:password.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function payPasswordAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $hasPayPassword = strlen($user['password']) > 0;

        if ($hasPayPassword) {
            $this->setFlashMessage('danger', '不能直接设置新支付密码。');
            return $this->redirect($this->generateUrl('settings_reset_pay_password'));
        }

        $form = $this->createFormBuilder()
                     ->add('currentUserLoginPassword', 'password')
                     ->add('newPayPassword', 'password')
                     ->add('confirmPayPassword', 'password')
                     ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $passwords = $form->getData();

                if (!$this->getAuthService()->checkPassword($user['id'], $passwords['currentUserLoginPassword'])) {
                    $this->setFlashMessage('danger', '当前用户登录密码不正确，请重试！');
                    return $this->redirect($this->generateUrl('settings_pay_password'));
                } else {
                    $this->getAuthService()->changeBatchPayPassword($user['id'], $passwords['currentUserLoginPassword'], $passwords['newPayPassword']);
                    $this->setFlashMessage('success', '新支付密码设置成功，您可以在此重设密码。');
                }

                return $this->redirect($this->generateUrl('settings_reset_pay_password'));
            }
        }

        return $this->render('TopxiaWebBundle:Settings:pay-password.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function resetPayPasswordAction(Request $request)
    {
        $user = $this->getCurrentUser();

        $form = $this->createFormBuilder()
                     // ->add('currentUserLoginPassword','password')
                     ->add('oldPayPassword', 'password')
                     ->add('newPayPassword', 'password')
                     ->add('confirmPayPassword', 'password')
                     ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $passwords = $form->getData();

                if (!($this->getUserService()->verifyPayPassword($user['id'], $passwords['oldPayPassword']))) {
                    $this->setFlashMessage('danger', '支付密码不正确，请重试！');
                } else {
                    $this->getAuthService()->changeBatchPayPasswordWithoutLoginPassword($user['id'], $passwords['newPayPassword']);
                    $this->setFlashMessage('success', '重置支付密码成功。');
                }

                return $this->redirect($this->generateUrl('settings_reset_pay_password'));
            }
        }

        return $this->render('TopxiaWebBundle:Settings:reset-pay-password.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function securityQuestionsAction(Request $request)
    {
        $user                 = $this->getCurrentUser();
        $userSecureQuestions  = $this->getUserService()->getUserSecureQuestionsByUserId($user['id']);
        $hasSecurityQuestions = (isset($userSecureQuestions)) && (count($userSecureQuestions) > 0);

        if ($request->getMethod() == 'POST') {
            if (!$this->getAuthService()->checkPassword($user['id'], $request->request->get('userLoginPassword'))) {
                $this->setFlashMessage('danger', '您的登录密码错误，不能设置安全问题。');
                return $this->securityQuestionsActionReturn($hasSecurityQuestions, $userSecureQuestions);
            }

            if ($hasSecurityQuestions) {
                throw new \RuntimeException('您已经设置过安全问题，不可再次修改。');
            }

            if ($request->request->get('question-1') == $request->request->get('question-2')
                || $request->request->get('question-1') == $request->request->get('question-3')
                || $request->request->get('question-2') == $request->request->get('question-3')) {
                throw new \RuntimeException('2个问题不能一样。');
            }

            $fields = array(
                'securityQuestion1' => $request->request->get('question-1'),
                'securityAnswer1'   => $request->request->get('answer-1'),
                'securityQuestion2' => $request->request->get('question-2'),
                'securityAnswer2'   => $request->request->get('answer-2'),
                'securityQuestion3' => $request->request->get('question-3'),
                'securityAnswer3'   => $request->request->get('answer-3')
            );
            
            $this->getUserService()->addBatchUserSecureQuestionsWithUnHashedAnswers($user['id'], $fields);
            
            $this->setFlashMessage('success', '安全问题设置成功。');
            $hasSecurityQuestions = true;
            $userSecureQuestions  = $this->getUserService()->getUserSecureQuestionsByUserId($user['id']);
        }

        return $this->securityQuestionsActionReturn($hasSecurityQuestions, $userSecureQuestions);
    }

    protected function setPayPasswordPage($request, $userId)
    {
        $token = $this->getUserService()->makeToken('pay-password-reset', $userId, strtotime('+1 day'));
        $request->request->set('token', $token);
        return $this->forward('CustomWebBundle:Settings:updatePayPassword', array(
            'request' => $request
        ));
    }

    public function findPayPasswordAction(Request $request)
    {
        $user                 = $this->getCurrentUser();
        $userSecureQuestions  = $this->getUserService()->getUserSecureQuestionsByUserId($user['id']);
        $hasSecurityQuestions = (isset($userSecureQuestions)) && (count($userSecureQuestions) > 0);
        $verifiedMobile       = $user['verifiedMobile'];
        $hasVerifiedMobile    = (isset($verifiedMobile)) && (strlen($verifiedMobile) > 0);
        $canSmsFind           = ($hasVerifiedMobile) &&
        ($this->setting('cloud_sms.sms_enabled') == '1') &&
        ($this->setting('cloud_sms.sms_forget_pay_password') == 'on');

        if ((!$hasSecurityQuestions) && ($canSmsFind)) {
            return $this->redirect($this->generateUrl('settings_find_pay_password_by_sms', array()));
        }

        if (!$hasSecurityQuestions) {
            $this->setFlashMessage('danger', '您还没有安全问题，请先设置。');
            return $this->forward('CustomWebBundle:Settings:securityQuestions');
        }

        if ($request->getMethod() == 'POST') {
            $questionNum = $request->request->get('questionNum');
            $answer      = $request->request->get('answer');

            $userSecureQuestion = $userSecureQuestions[$questionNum];

            $isAnswerRight = $this->getUserService()->verifyInSaltOut(
                $answer, $userSecureQuestion['securityAnswerSalt'], $userSecureQuestion['securityAnswer']);

            if (!$isAnswerRight) {
                $this->setFlashMessage('danger', '回答错误。');
                return $this->findPayPasswordActionReturn($userSecureQuestions, $hasSecurityQuestions, $hasVerifiedMobile);
            }

            $this->setFlashMessage('success', '回答正确，你可以开始更新支付密码。');
            return $this->setPayPasswordPage($request, $user['id']);
        }

        return $this->findPayPasswordActionReturn($userSecureQuestions, $hasSecurityQuestions, $hasVerifiedMobile);
    }

    public function findPayPasswordBySmsAction(Request $request)
    {
        $scenario = "sms_forget_pay_password";
        
        if ($this->setting('cloud_sms.sms_enabled') != '1' || $this->setting("cloud_sms.{$scenario}") != 'on') {
            return $this->render('TopxiaWebBundle:Settings:edu-cloud-error.html.twig', array());
        }
        
        $currentUser = $this->getCurrentUser();

        $userSecureQuestions  = $this->getUserService()->getUserSecureQuestionsByUserId($currentUser['id']);
        $hasSecurityQuestions = (isset($userSecureQuestions)) && (count($userSecureQuestions) > 0);
        $verifiedMobile       = $currentUser['verifiedMobile'];
        $hasVerifiedMobile    = (isset($verifiedMobile)) && (strlen($verifiedMobile) > 0);

        if (!$hasVerifiedMobile) {
            $this->setFlashMessage('danger', '您还没有绑定手机，请先绑定。');
            return $this->redirect($this->generateUrl('settings_bind_mobile', array(
            )));
        }

        if ($request->getMethod() == 'POST') {
            if ($currentUser['verifiedMobile'] != $request->request->get('mobile')) {
                $this->setFlashMessage('danger', '您输入的手机号，不是已绑定的手机');
                SmsToolkit::clearSmsSession($request, $scenario);
                goto response;
            }

            list($result, $sessionField, $requestField) = SmsToolkit::smsCheck($request, $scenario);

            if ($result) {
                $this->setFlashMessage('success', '验证通过，你可以开始更新支付密码。');
                return $this->setPayPasswordPage($request, $currentUser['id']);
            } else {
                $this->setFlashMessage('danger', '验证错误。');
            }
        }

        response:
        return $this->render('TopxiaWebBundle:Settings:find-pay-password-by-sms.html.twig', array(
            'hasSecurityQuestions' => $hasSecurityQuestions,
            'hasVerifiedMobile'    => $hasVerifiedMobile,
            'verifiedMobile'       => $verifiedMobile
        ));
    }

    public function updatePayPasswordAction(Request $request)
    {
        $token = $this->getUserService()->getToken('pay-password-reset', $request->query->get('token') ?: $request->request->get('token'));

        if (empty($token)) {
            throw new \RuntimeException('Bad Token!');
        }

        $form = $this->createFormBuilder()
                     ->add('payPassword', 'password')
                     ->add('confirmPayPassword', 'password')
                     ->add('currentUserLoginPassword', 'password')
                     ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                if ($data['payPassword'] != $data['confirmPayPassword']) {
                    $this->setFlashMessage('danger', '两次输入的支付密码不一致。');
                    return $this->updatePayPasswordReturn($form, $token);
                }

                if ($this->getAuthService()->checkPassword($token['userId'], $data['currentUserLoginPassword'])) {
                    
                    $this->getAuthService()->changeBatchPayPassword($token['userId'], $data['currentUserLoginPassword'], $data['payPassword']);
                    
                    $this->getUserService()->deleteToken('pay-password-reset', $token['token']);
                    return $this->render('TopxiaWebBundle:Settings:pay-password-success.html.twig');
                } else {
                    $this->setFlashMessage('danger', '用户登录密码错误。');
                }
            }
        }

        return $this->updatePayPasswordReturn($form, $token);
    }
}
