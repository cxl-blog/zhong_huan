<?php

namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\BaseController;
use Topxia\Service\Common\ServiceException;
use Custom\Common\Util\DateUtils;

class DataSyncTestController extends BaseController
{

    public function testPageAction()
    {
        $fileNames = scandir('dataSync/sampleDataTest');

        $typeSamples = array();
        foreach ($fileNames as $fileName) {
            if (strpos($fileName, '.md') && !strpos($fileName, '_doc')) {
                $typeName = explode('.md', $fileName)[0];
                $fileContent = file_get_contents('dataSync/sampleDataTest/' . $fileName);
                $docContent = file_get_contents('dataSync/sampleDataTest/' . $typeName . '_doc.md');
                $keyValue = array();
                $keyValue['key'] = $typeName;
                $keyValue['value'] = $fileContent;
                $keyValue['doc'] = $docContent;
                $typeSamples[] = $keyValue;
            }
        }
        return $this->render(
            'CustomWebBundle:DataSyncTest:testPage.html.twig', 
            array('typeSamples' => $typeSamples)
        );
    }

    public function testSyncAction(Request $request, $type)
    {
        try {
            if ($this->getCurrentUser()['id'] != 1) {
                throw new ServiceException('Only super adminstrator is allowed');
            }
            $this->sync($type, $request->get('data'));
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    private function sync($type, $data)
    {
        if ($type == 'syncSingleUser') {
            $this->getSyncService()->syncSingleUser($data);
        } elseif ($type == 'syncSingleCertificate') {
            $this->getSyncService()->syncSingleCertificate($data);
        }
    }
}
