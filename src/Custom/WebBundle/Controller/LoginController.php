<?php
namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Topxia\WebBundle\Controller\LoginController as BaseLoginController;

class LoginController extends BaseLoginController
{
    public function indexAction(Request $request)
    {
        $user = $this->getCurrentUser();

        if ($user->isLogin()) {
            return $this->createMessageResponse('info', '你已经登录了', null, 3000, $this->generateUrl('homepage'));
        }

        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        if ($this->getWebExtension()->isMicroMessenger() && $this->setting('login_bind.enabled', 0) && $this->setting('login_bind.weixinmob_enabled', 0)) {
            return $this->forward('TopxiaWebBundle:LoginBind:index', array('type' => 'weixinmob', '_target_path' => $this->getTargetPath($request)));
        }

        return $this->render('CustomWebBundle:Login:index.html.twig', array(
            'last_username' => $request->getSession()->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
            '_target_path'  => $this->getTargetPath($request)
        ));
    }

    public function getAreaCodeAction(Request $request)
    {
        $username = $request->request->get('username');
        $areaCodes = $this->getUserService()->findAreaCodesByKeyword($username);

        $result = array();
        if (!empty($areaCodes)) {
             foreach ($areaCodes as $area) {
                $result[] = array(
                    'code' => $area['code'],
                    'name' => $area['name']
                );
             }
        }

        return $this->createJsonResponse($result);
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }
}
