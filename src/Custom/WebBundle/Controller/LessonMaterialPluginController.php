<?php
namespace Custom\WebBundle\Controller;

use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\LessonMaterialPluginController as BaseController;

class LessonMaterialPluginController extends BaseController
{
    public function initAction(Request $request)
    {
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $request->query->get('courseId'));
        list($course, $member) = $this->getCourseService()->tryTakeCourse($request->query->get('courseId'), $currentLoopTime);
        $lesson                = $this->getCourseService()->getCourseLesson($course['id'], $request->query->get('lessonId'));

        if ($lesson['mediaId'] > 0) {
            $file = $this->getUploadFileService()->getFile($lesson['mediaId']);
        } else {
            $file = null;
        }

        $lessonMaterials = $this->getMaterialService()->findLessonMaterials($lesson['id'], 0, 100);
        return $this->render('TopxiaWebBundle:LessonMaterialPlugin:index.html.twig', array(
            'materials' => $lessonMaterials,
            'course'    => $course,
            'lesson'    => $lesson,
            'file'      => $file
        ));
    }
}
