<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Service\Question\QuestionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\WebBundle\Controller\CourseQuestionManageController as BaseCourseQuestionManageController;

class CertificationCourseQuestionManageController extends BaseCourseQuestionManageController
{
    public function indexAction(Request $request, $courseId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);

        $conditions = $request->query->all();

        if (empty($conditions['target'])) {
            $conditions['targetPrefix'] = "course-{$course['id']}";
        }

        if (!empty($conditions['keyword'])) {
            $conditions['stem'] = $conditions['keyword'];
        }

        if (!empty($conditions['parentId'])) {
            $parentQuestion = $this->getQuestionService()->getQuestion($conditions['parentId']);

            if (empty($parentQuestion)) {
                return $this->redirect($this->generateUrl('course_certification_manage_question', array('courseId' => $courseId)));
            }

            $orderBy = array('createdTime', 'ASC');
        } else {
            $conditions['parentId'] = 0;
            $parentQuestion         = null;
            $orderBy                = array('createdTime', 'DESC');
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getQuestionService()->searchQuestionsCount($conditions),
            10
        );

        $questions = $this->getQuestionService()->searchQuestions(
            $conditions,
            $orderBy,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($questions, 'userId'));

        $targets = $this->get('topxia.target_helper')->getTargets(ArrayToolkit::column($questions, 'target'));

        return $this->render('CustomWebBundle:CourseQuestionManage:index.html.twig', array(
            'course'         => $course,
            'questions'      => $questions,
            'users'          => $users,
            'targets'        => $targets,
            'paginator'      => $paginator,
            'parentQuestion' => $parentQuestion,
            'conditions'     => $conditions,
            'targetChoices'  => $this->getQuestionTargetChoices($course)
        ));
    }
    public function createAction(Request $request, $courseId, $type)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();

            $question = $this->getQuestionService()->createQuestion($data);

            if ($data['submission'] == 'continue') {
                $urlParams             = ArrayToolkit::parts($question, array('target', 'difficulty', 'parentId'));
                $urlParams['type']     = $type;
                $urlParams['courseId'] = $courseId;
                $urlParams['goto']     = $request->query->get('goto', null);
                $this->setFlashMessage('success', '题目添加成功，请继续添加。');
                return $this->redirect($this->generateUrl('course_certification_manage_question_create', $urlParams));
            } elseif ($data['submission'] == 'continue_sub') {
                $this->setFlashMessage('success', '题目添加成功，请继续添加子题。');
                return $this->redirect($request->query->get('goto', $this->generateUrl('course_certification_manage_question', array('courseId' => $courseId, 'parentId' => $question['id']))));
            } else {
                $this->setFlashMessage('success', '题目添加成功。');
                return $this->redirect($request->query->get('goto', $this->generateUrl('course_certification_manage_question', array('courseId' => $courseId))));
            }
        }

        $question = array(
            'id'         => 0,
            'type'       => $type,
            'target'     => $request->query->get('target'),
            'difficulty' => $request->query->get('difficulty', 'normal'),
            'parentId'   => $request->query->get('parentId', 0)
        );

        if ($question['parentId'] > 0) {
            $parentQuestion = $this->getQuestionService()->getQuestion($question['parentId']);

            if (empty($parentQuestion)) {
                return $this->createMessageResponse('error', '父题不存在，不能创建子题！');
            }
        } else {
            $parentQuestion = null;
        }

        if ($this->container->hasParameter('enabled_features')) {
            $features = $this->container->getParameter('enabled_features');
        } else {
            $features = array();
        }

        $enabledAudioQuestion = in_array('audio_question', $features);

        return $this->render("CustomWebBundle:CourseQuestionManage:question-form-{$type}.html.twig", array(
            'course'               => $course,
            'question'             => $question,
            'parentQuestion'       => $parentQuestion,
            'targetsChoices'       => $this->getQuestionTargetChoices($course),
            'categoryChoices'      => $this->getQuestionCategoryChoices($course),
            'enabledAudioQuestion' => $enabledAudioQuestion
        ));
    }
    public function updateAction(Request $request, $courseId, $id)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $question = $this->tryGetQuestion($courseId, $id);

        if ($request->getMethod() == 'POST') {
            $question = $request->request->all();

            $question = $this->getQuestionService()->updateQuestion($id, $question);

            $this->setFlashMessage('success', '题目修改成功！');

            return $this->redirect($request->query->get('goto', $this->generateUrl('course_certification_manage_question', array('courseId' => $courseId, 'parentId' => $question['parentId']))));
        }

        if ($question['parentId'] > 0) {
            $parentQuestion = $this->getQuestionService()->getQuestion($question['parentId']);
        } else {
            $parentQuestion = null;
        }

        return $this->render("TopxiaWebBundle:CourseQuestionManage:question-form-{$question['type']}.html.twig", array(
            'course'          => $course,
            'question'        => $question,
            'parentQuestion'  => $parentQuestion,
            'targetsChoices'  => $this->getQuestionTargetChoices($course),
            'categoryChoices' => $this->getQuestionCategoryChoices($course)
        ));
    }
}