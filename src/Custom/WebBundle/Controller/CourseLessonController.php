<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Topxia\Service\Util\CloudClientFactory;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseLessonController as BaseController;

class CourseLessonController extends BaseController
{
    public function showAction(Request $request, $courseId, $lessonId)
    {
        $course          = $this->getCourseService()->getCourse($courseId);
        $user            = $this->getCurrentUser();
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId);

        $lessonLearn = $this->getLessonLearnDao()->getLearnByUserIdAndLessonId($user['id'], $lessonId, $currentLoopTime);
        $isFinished  = !empty($lessonLearn) && $lessonLearn['status'] == 'finished';

        $isPreview = strpos($request->server->get('HTTP_REFERER'), 'preview');

        if ($course['type'] == 'certification' && !$isFinished && !$isPreview) {
            if (!$this->getCourseService()->canManageCourse($course['id'], $currentLoopTime)) {
                $areaCertification = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'], $course['certificationId']);
                if ($areaCertification['sequentialLearning'] != 0) {
                    $nextLesson = $this->getCourseService()->getUserValideNextLearnLesson($user['id'], $course['id'], $currentLoopTime);
                    if ($nextLesson['id'] != $lessonId) {
                        $json['canLearn']['status'] = 'yes';
                        $json['status']             = 'published';
                        $json['title']              = $nextLesson['title'];
                        $json['mediaError']         = "请按顺序观看课程 ".$nextLesson['title'];
                        $json['next']               = $nextLesson['id'];
                        return $this->createJsonResponse($json);
                    }
                }
            }
        }

        return $this->parentShowAction($request, $courseId, $lessonId, $currentLoopTime, array('isFinished' => $isFinished, 'isPreview' => $isPreview));
    }

    public function learnStatusAction(Request $request, $courseId, $lessonId)
    {
        $user            = $this->getCurrentUser();
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId);
        $status          = $this->getCourseService()->getUserLearnLessonStatus($user['id'], $courseId, $lessonId, $currentLoopTime);

        return $this->createJsonResponse(array('status' => $status ?: 'unstart'));
    }

    public function playerAction(Request $request, $courseId, $lessonId = 0)
    {
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        if (empty($lesson)) {
            throw $this->createNotFoundException();
        }

        $isPreview = $request->query->get('isPreview', '');

        if (($isPreview && $lesson["free"])) {
            return $this->forward('TopxiaWebBundle:Player:show', array(
                'id' => $lesson["mediaId"]
            ));
        }

        $course = $this->getCourseService()->getCourse($courseId);

        if ($isPreview && !empty($course['tryLookable'])) {
            return $this->forward('TopxiaWebBundle:Player:show', array(
                'id'      => $lesson["mediaId"],
                'context' => array('watchTimeLimit' => $course['tryLookTime'] * 60)
            ));
        }

        $currentLoopTime            = $course['type'] == 'certification' ? CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $courseId) : 1;
        list($course, $member)      = $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime);
        $context                    = array();
        $context['starttime']       = $request->query->get('starttime');
        $context['hideBeginning']   = $request->query->get('hideBeginning', false);
        $context['currentLoopTime'] = $currentLoopTime;
        return $this->forward('CustomWebBundle:Player:show', array(
            'id'      => $lesson["mediaId"],
            'context' => $context
        ));
    }

    public function listAction(Request $request, $courseId, $member, $previewUrl, $mode = 'full')
    {
        $user          = $this->getCurrentUser();
        $isLogin       = $user->isLogin();
        $learnStatuses = $this->getCourseService()->getUserLearnLessonStatuses($user['id'], $courseId);
        $items         = $this->getCourseService()->getCourseItems($courseId);
        $course        = $this->getCourseService()->getCourse($courseId);

        $homeworkPlugin     = $this->getAppService()->findInstallApp('Homework');
        $homeworkLessonIds  = array();
        $exercisesLessonIds = array();

        if ($homeworkPlugin) {
            $lessonIds          = ArrayToolkit::column($items, 'id');
            $homeworks          = $this->getHomeworkService()->findHomeworksByCourseIdAndLessonIds($course['id'], $lessonIds);
            $exercises          = $this->getExerciseService()->findExercisesByLessonIds($lessonIds);
            $homeworkLessonIds  = ArrayToolkit::column($homeworks, 'lessonId');
            $exercisesLessonIds = ArrayToolkit::column($exercises, 'lessonId');
        }

        if ($this->setting('magic.lesson_watch_limit')) {
            $lessonLearns = $this->getCourseService()->findUserLearnedLessons($user['id'], $courseId);
        } else {
            $lessonLearns = array();
        }

        $testpaperIds = array();
        array_walk($items, function ($item, $key) use (&$testpaperIds) {
            if ($item['type'] == 'testpaper') {
                array_push($testpaperIds, $item['mediaId']);
            }
        }

        );

        $testpapers = $this->getTestpaperService()->findTestpapersByIds($testpaperIds);

        $isAdmin                        = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']) || $this->getCourseService()->canBranchAdminMangeCourse($course['id']);
        $certificationCourse            = $this->getCertificateService()->getCertificationsForUser($user['id'], false, $course['id']);
        $certificationCourse['nowTime'] = date('Y-m-d');
        return $this->Render('CustomWebBundle:CourseLesson/Widget:list.html.twig', array(
            'items'               => $items,
            'course'              => $course,
            'member'              => $member,
            'previewUrl'          => $previewUrl,
            'learnStatuses'       => $learnStatuses,
            'lessonLearns'        => $lessonLearns,
            'currentTime'         => time(),
            'homeworkLessonIds'   => $homeworkLessonIds,
            'exercisesLessonIds'  => $exercisesLessonIds,
            'mode'                => $mode,
            'testpapers'          => $testpapers,
            'isAdmin'             => $isAdmin,
            'certificationCourse' => $certificationCourse,
            'isLogin'             => $isLogin
        ));
    }

    public function learnFinishAction(Request $request, $courseId, $lessonId)
    {
        $user = $this->getCurrentUser();

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId) ? CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId): CourseLoopUtils::$START_LOOP_TIME;

        if ($this->isPluginInstalled('ClassroomPlan')) {
            return $this->forward('ClassroomPlanBundle:ClassroomPlan:lessonFinishModal', array(
                'lessonId' => $lessonId
            ));
        }

        $this->getCourseService()->finishLearnLesson($courseId, $lessonId, $currentLoopTime);

        $member     = $this->getCourseService()->getCourseMember($courseId, $user['id'], $currentLoopTime);
        $nextLesson = $this->getCourseService()->getUserValideNextLearnLesson($user['id'], $courseId, $currentLoopTime);
        if (empty($nextLesson)) {  //已学完所有课程
            $this->getCourseService()->passCourse($user['id'], $courseId, $currentLoopTime);
            $this->getCourseService()->completeCourse($user['id'], $courseId, $currentLoopTime);
        }
        $response   = array(
            'learnedNum'   => empty($member['learnedNum']) ? 0 : $member['learnedNum'],
            'isLearned'    => empty($member['isLearned']) ? 0 : $member['isLearned'],
            'nextLessonId' => !empty($nextLesson) ? $nextLesson['id'] : 0
        );

        return $this->createJsonResponse($response);
    }

    public function learnStartAction(Request $request, $courseId, $lessonId)
    {
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()['id'], $courseId) ? CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()['id'], $courseId) : CourseLoopUtils::$START_LOOP_TIME;
        $result          = $this->getCourseService()->startLearnLesson($courseId, $lessonId, $currentLoopTime);
        return $this->createJsonResponse($result);
    }
    
    public function documentAction(Request $request, $courseId, $lessonId)
    {
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);
        
        $course = $this->getCourseService()->getCourse($courseId);

        $currentLoopTime = $course['type'] == 'certification' ? CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()->id, $courseId) : 1;

        if (empty($lesson)) {
            throw $this->createNotFoundException();
        }

        if (!$lesson['free']) {
            $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime);
        }

        if ($lesson['type'] != 'document' || empty($lesson['mediaId'])) {
            throw $this->createNotFoundException();
        }

        $file = $this->getUploadFileService()->getFile($lesson['mediaId']);

        if (empty($file)) {
            throw $this->createNotFoundException();
        }

        if ($file['convertStatus'] != 'success') {
            if ($file['convertStatus'] == 'error') {
                $url     = $this->generateUrl('course_manage_files', array('id' => $courseId));
                $message = sprintf('文档转换失败，请到课程<a href="%s" target="_blank">文件管理</a>中，重新转换。', $url);

                return $this->createJsonResponse(array(
                    'error' => array('code' => 'error', 'message' => $message)
                ));
            } else {
                return $this->createJsonResponse(array(
                    'error' => array('code' => 'processing', 'message' => '文档还在转换中，还不能查看，请稍等。')
                ));
            }
        }

        $factory = new CloudClientFactory();
        $client  = $factory->createClient();

        $metas2           = $file['metas2'];
        $url              = $client->generateFileUrl($client->getBucket(), $metas2['pdf']['key'], 3600);
        $result['pdfUri'] = $url['url'];
        $url              = $client->generateFileUrl($client->getBucket(), $metas2['swf']['key'], 3600);
        $result['swfUri'] = $url['url'];

        return $this->createJsonResponse($result);
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getLessonLearnDao()
    {
        return $this->getServiceKernel()->createDao('Custom:Course.LessonLearnDao');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    private function parentShowAction(Request $request, $courseId, $lessonId, $currentLoopTime, $status)
    {
        list($course, $member) = $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime); //只有这一行不一样

        $lesson         = $this->getCourseService()->getCourseLesson($courseId, $lessonId);
        $json           = array();
        $json['number'] = $lesson['number'];
        $chapter        = empty($lesson['chapterId']) ? null : $this->getCourseService()->getChapter($course['id'], $lesson['chapterId']);

        if ($chapter['type'] == 'unit') {
            $unit               = $chapter;
            $json['unit']       = $unit;
            $json['unitNumber'] = $unit['number'];

            $chapter               = $this->getCourseService()->getChapter($course['id'], $unit['parentId']);
            $json['chapter']       = $chapter;
            $json['chapterNumber'] = empty($chapter) ? 0 : $chapter['number'];
        } else {
            $json['chapterNumber'] = empty($chapter) ? 0 : $chapter['number'];
            $json['unitNumber']    = 0;
        }

        $json['title']                  = $lesson['title'];
        $json['summary']                = $lesson['summary'];
        $json['type']                   = $lesson['type'];
        $json['content']                = $lesson['content'];
        $json['status']                 = $lesson['status'];
        $json['quizNum']                = $lesson['quizNum'];
        $json['materialNum']            = $lesson['materialNum'];
        $json['mediaId']                = $lesson['mediaId'];
        $json['mediaSource']            = $lesson['mediaSource'];
        $json['startTimeFormat']        = date("m-d H:i", $lesson['startTime']);
        $json['endTimeFormat']          = date("H:i", $lesson['endTime']);
        $json['startTime']              = $lesson['startTime'];
        $json['endTime']                = $lesson['endTime'];
        $json['id']                     = $lesson['id'];
        $json['courseId']               = $lesson['courseId'];
        $json['videoWatermarkEmbedded'] = 0;
        $json['liveProvider']           = $lesson["liveProvider"];
        $json['nowDate']                = time();
        $json['testMode']               = $lesson['testMode'];
        $json['testStartTime']          = $lesson['testStartTime'];
        $json['testStartTimeFormat']    = date("m-d H:i", $lesson['testStartTime']);

        if ($lesson['testMode'] == 'realTime') {
            $testpaper                 = $this->getTestpaperService()->getTestpaper($lesson['mediaId']);
            $json['limitedTime']       = $testpaper['limitedTime'];
            $minute                    = '+'.$testpaper['limitedTime'].'minute';
            $json['testEndTime']       = strtotime($minute, $lesson['testStartTime']);
            $json['testEndTimeFormat'] = date("m-d H:i", $json['testEndTime']);
        }

        $app = $this->getAppService()->findInstallApp('Homework');

        if (!empty($app)) {
            $homework                      = $this->getHomeworkService()->getHomeworkByLessonId($lesson['id']);
            $exercise                      = $this->getExerciseService()->getExerciseByLessonId($lesson['id']);
            $json['homeworkOrExerciseNum'] = $homework['itemCount'] + $exercise['itemCount'];
        } else {
            $json['homeworkOrExerciseNum'] = 0;
        }

        $json['isTeacher'] = $this->getCourseService()->isCourseTeacher($courseId, $this->getCurrentUser()->id);

        if ($lesson['type'] == 'live' && $lesson['replayStatus'] == 'generated') {
            $replaysLesson  = $this->getCourseService()->getCourseLessonReplayByLessonId($lesson['id']);
            $visableReplays = array();

            foreach ($replaysLesson as $key => $value) {
                if ($value['hidden'] == 0) {
                    $visableReplays[] = $value;
                }
            }

            $json['replays'] = $visableReplays;

            if (!empty($json['replays'])) {
                foreach ($json['replays'] as $key => $value) {
                    $url = $this->generateUrl('live_course_lesson_replay_entry', array(
                        'courseId'             => $lesson['courseId'],
                        'lessonId'             => $lesson['id'],
                        'courseLessonReplayId' => $value['id']
                    ), true);
                    $json['replays'][$key]["url"] = $url;
                }
            }
        }

        if ($json['mediaSource'] == 'self') {
            $file = $this->getUploadFileService()->getFile($lesson['mediaId']);

            if (!empty($file)) {
                if ($file['storage'] == 'cloud') {
                    $factory = new CloudClientFactory();
                    $client  = $factory->createClient();

                    $json['mediaConvertStatus'] = $file['convertStatus'];

                    if (!empty($file['convertParams']['hasVideoWatermark'])) {
                        $json['videoWatermarkEmbedded'] = 1;
                    }

                    if (!empty($file['metas2']) && !empty($file['metas2']['sd']['key'])) {
                        if (isset($file['convertParams']['convertor']) && ($file['convertParams']['convertor'] == 'HLSEncryptedVideo')) {
                            $token = $this->getTokenService()->makeToken('hls.playlist', array(
                                'data'     => $file['id'],
                                'times'    => $this->agentInWhiteList($request->headers->get("user-agent")) ? 0 : 3,
                                'duration' => 3600,
                                'userId'   => $this->getCurrentUser()->getId()
                            ));

                            $url = array(
                                'url' => $this->generateUrl('hls_playlist', array(
                                    'id'    => $file['id'],
                                    'token' => $token['token'],
                                    'line'  => $request->query->get('line')
                                ), true)
                            );
                        } else {
                            $url = $client->generateHLSQualitiyListUrl($file['metas2'], 3600);
                        }

                        $json['mediaHLSUri'] = $url['url'];

                        if ($this->setting('magic.lesson_watch_limit') && $course['watchLimit'] > 0) {
                            $user        = $this->getCurrentUser();
                            $watchStatus = $this->getCourseService()->checkWatchNum($user['id'], $lesson['id']);

                            if ($watchStatus['status'] == 'error') {
                                $wathcLimitTime     = $this->container->get('topxia.twig.web_extension')->durationTextFilter($watchStatus['watchLimitTime']);
                                $json['mediaError'] = "您的观看时长已到 <strong>{$wathcLimitTime}</strong>，不能再观看。";
                            }
                        }
                    } elseif ($file['type'] == 'ppt') {
                        $json['mediaUri'] = $this->generateUrl('course_lesson_ppt', array('courseId' => $course['id'], 'lessonId' => $lesson['id']));
                    } else {
                        if (!empty($file['metas']) && !empty($file['metas']['hd']['key'])) {
                            $key = $file['metas']['hd']['key'];
                        } else {
                            if ($file['type'] == 'video') {
                                $key = null;
                            } else {
                                $key = $file['hashId'];
                            }
                        }

                        if ($key) {
                            $url              = $client->generateFileUrl($client->getBucket(), $key, 3600);
                            $json['mediaUri'] = $url['url'];
                        } else {
                            $json['mediaUri'] = '';
                        }
                    }
                } else {
                    $json['mediaUri'] = $this->generateUrl('course_lesson_media', array('courseId' => $course['id'], 'lessonId' => $lesson['id']));

                    if ($this->setting('magic.lesson_watch_limit') && $course['watchLimit'] > 0) {
                        $user        = $this->getCurrentUser();
                        $watchStatus = $this->getCourseService()->checkWatchNum($user['id'], $lesson['id']);

                        if ($watchStatus['status'] == 'error') {
                            $wathcLimitTime     = $this->container->get('topxia.twig.web_extension')->durationTextFilter($watchStatus['watchLimitTime']);
                            $json['mediaError'] = "您的观看时长已到 <strong>{$wathcLimitTime}</strong>，不能再观看。";
                        }
                    }
                }
            } else {
                $json['mediaUri'] = '';

                if ($lesson['type'] == 'video') {
                    $json['mediaError'] = '抱歉，视频文件不存在，暂时无法学习。';
                } elseif ($lesson['type'] == 'audio') {
                    $json['mediaError'] = '抱歉，音频文件不存在，暂时无法学习。';
                } elseif ($lesson['type'] == 'ppt') {
                    $json['mediaError'] = '抱歉，PPT文件不存在，暂时无法学习。';
                }
            }
        } elseif ($json['mediaSource'] == 'youku' && $this->isMobile()) {
            $matched = preg_match('/\/sid\/(.*?)\/v\.swf/s', $lesson['mediaUri'], $matches);

            if ($matched) {
                $json['mediaUri']    = "http://player.youku.com/embed/{$matches[1]}";
                $json['mediaSource'] = 'iframe';
            } else {
                $json['mediaUri'] = $lesson['mediaUri'];
            }
        } elseif ($json['mediaSource'] == 'tudou') {
            $matched = preg_match('/\/v\/(.*?)\/v\.swf/s', $lesson['mediaUri'], $matches);

            if ($matched) {
                $json['mediaUri']    = "http://www.tudou.com/programs/view/html5embed.action?code={$matches[1]}";
                $json['mediaSource'] = 'iframe';
            } else {
                $json['mediaUri'] = $lesson['mediaUri'];
            }
        } else {
            $json['mediaUri'] = $lesson['mediaUri'];
        }

        $json['canLearn'] = $this->getCourseService()->canLearnLesson($lesson['courseId'], $lesson['id'], $currentLoopTime);
        if ($lesson['type'] == 'video' && !$status['isFinished'] && !$status['isPreview']) {
            $json = $this->appendUserLearnTimeInfo($json, $member['userId'], $course['areaCode'], $course['certificationId']);    
        }
        return $this->createJsonResponse($json);
    }

    public function doTestpaperAction(Request $request, $lessonId, $testId)
    {
        $status  = 'do';
        $message = $this->checkTestPaper($lessonId, $testId, $status, $request);

        if (!empty($message)) {
            return $this->createMessageResponse('info', $message);
        }
        return $this->forward('CustomWebBundle:Testpaper:doTestpaper', array('targetType' => 'lesson', 'targetId' => $lessonId, 'testId' => $testId));
    }

    public function reDoTestpaperAction(Request $request, $lessonId, $testId)
    {
        $status  = 'redo';
        $message = $this->checkTestPaper($lessonId, $testId, $status, $request);

        if (!empty($message)) {
            return $this->createMessageResponse('info', $message);
        }

        return $this->forward('CustomWebBundle:Testpaper:reDoTestpaper', array('targetType' => 'lesson', 'targetId' => $lessonId, 'testId' => $testId));
    }

    protected function getUserLearnTimeService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.UserLearnTimeService');
    }

    private function appendUserLearnTimeInfo($result, $userId, $areaCode, $certificationId)
    {
        $userLearnTimeInfo = $this->getUserLearnTimeService()->getCurrentLearnTimeInfo(
            $userId, 
            null, 
            $areaCode, 
            $certificationId
        );
        return array_merge($result, $userLearnTimeInfo);
    }

    private function checkTestPaper($lessonId, $testId, $status, $request)
    {
        $user = $this->getCurrentUser();

        $message   = '';
        $testpaper = $this->getTestpaperService()->getTestpaper($testId);

        $targets = $this->get('topxia.target_helper')->getTargets(array($testpaper['target']));

        if ($targets[$testpaper['target']]['type'] != 'course') {
            throw $this->createAccessDeniedException('试卷只能属于课程');
        }

        $courseId = $targets[$testpaper['target']]['id'];

        $course = $this->getCourseService()->getCourse($courseId);

        if (empty($course)) {
            return $message = '试卷所属课程不存在！';
        }

        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $this->getCurrentUser()['id'], $courseId);

        if (!$this->getCourseService()->canTakeCourse($course, $currentLoopTime)) {
            return $message = '不是试卷所属课程老师或学生';
        }

        $lesson = $this->getCourseService()->getLesson($lessonId);

        if ($lesson['testMode'] == 'realTime') {
            $testpaper = $this->getTestpaperService()->getTestpaper($testId);

            $testEndTime = $lesson['testStartTime'] + $testpaper['limitedTime'] * 60;

            if ($testEndTime < time()) {
                return $message = '实时考试已经结束!';
            }

            if ($status == 'do') {
                $testpaperResult = $this->getTestpaperService()->findTestpaperResultsByTestIdAndStatusAndUserId($testpaper['id'], $user['id'], array('finished'));

                if ($testpaperResult) {
                    return $message = '您已经提交试卷，不能继续考试!';
                }
            } else {
                return $message = '实时考试，不能再考一次!';
            }
        }
    }
}
