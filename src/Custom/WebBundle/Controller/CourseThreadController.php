<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseThreadController as BaseCourseThreadController;

class CourseThreadController extends BaseCourseThreadController
{
    public function indexAction(Request $request, $id)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $id);

        if ($response) {
            return $response;
        }

        $user = $this->getCurrentUser();
        $filters    = $this->getThreadSearchFilters($request);
        $conditions = $this->convertFiltersToConditions($course, $filters);

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->searchThreadCount($conditions),
            20
        );

        $threads = $this->getThreadService()->searchThreads(
            $conditions,
            $filters['sort'],
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        foreach ($threads as $key => $thread) {
            $threads[$key]['sticky']         = $thread['isStick'];
            $threads[$key]['nice']           = $thread['isElite'];
            $threads[$key]['lastPostTime']   = $thread['latestPostTime'];
            $threads[$key]['lastPostUserId'] = $thread['latestPostUserId'];
        }

        $lessons = $this->getCourseService()->findLessonsByIds(ArrayToolkit::column($threads, 'lessonId'));
        $userIds = array_merge(
            ArrayToolkit::column($threads, 'userId'),
            ArrayToolkit::column($threads, 'latestPostUserId')
        );
        $users   = $this->getUserService()->findUsersByIds($userIds);
        $infos = array(
            'threads' => $threads,
            'users' => $users,
            'paginator' => $paginator,
            'filters' => $filters,
            'lessons' => $lessons,
            'target' => array('type' => 'course', 'id' => $id),
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render("CustomWebBundle:CourseThread:index.html.twig", $headerInfos);
    }

    public function showAction(Request $request, $courseId, $threadId)
    {
        list($course, $member, $response) = $this->buildLayoutDataWithTakenAccess($request, $courseId);

        if ($response) {
            return $response;
        }

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }

        $user = $this->getCurrentUser();

        if ($member && !$this->getCourseService()->isMemberNonExpired($course, $member)) {
            $isMemberNonExpired = false;
        } else {
            $isMemberNonExpired = true;
        }

        $thread = $this->getThreadService()->getThread($course['id'], $threadId);

        if (empty($thread)) {
            throw $this->createNotFoundException("话题不存在，或已删除。");
        }

        $paginator = new Paginator(
            $request,
            $this->getThreadService()->getThreadPostCount($course['id'], $thread['id']),
            30
        );

        $posts = $this->getThreadService()->findThreadPosts(
            $thread['courseId'],
            $thread['id'],
            'default',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        if ($thread['type'] == 'question' && $paginator->getCurrentPage() == 1) {
            $elitePosts = $this->getThreadService()->findThreadElitePosts($thread['courseId'], $thread['id'], 0, 10);
        } else {
            $elitePosts = array();
        }

        $users = $this->getUserService()->findUsersByIds(ArrayToolkit::column($posts, 'userId'));

        $this->getThreadService()->hitThread($courseId, $threadId);

        $isManager = $this->getCourseService()->canManageCourse($course['id']);

        $lesson = $this->getCourseService()->getCourseLesson($course['id'], $thread['lessonId']);
        $infos = array(
            'lesson' => $lesson,
            'thread' => $thread,
            'author' => $this->getUserService()->getUser($thread['userId']),
            'posts' => $posts,
            'elitePosts' => $elitePosts,
            'users' => $users,
            'isManager' => $isManager,
            'isMemberNonExpired' => $isMemberNonExpired,
            'paginator' => $paginator,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render("CustomWebBundle:CourseThread:show.html.twig", $headerInfos);
    }

    public function certNormalShowAction(Request $request, $courseId, $threadId)
    {
        $course = $this->getCourseService()->getCourse($courseId);
        if ($course['type'] == 'certification') {
            return $this->redirect(
                $this->generateUrl('course_certification_thread_show', array(
                    'courseId' => $courseId,
                    'threadId' => $threadId
                ))
            );
        } else {
            return $this->redirect(
                $this->generateUrl('course_thread_show', array(
                    'courseId' => $courseId,
                    'threadId' => $threadId
                ))
            );
        }
    }

    public function certNormalIndexAction(Request $request, $id)
    {
        $course = $this->getCourseService()->getCourse($id);
        if ($course['type'] == 'certification') {
            return $this->redirect(
                $this->generateUrl('course_certification_threads', array(
                    'id' => $id
                ))
            );
        } else {
            return $this->redirect(
                $this->generateUrl('course_threads', array(
                    'id' => $id
                ))
            );
        }
    }

    public function certNormalEditAction(Request $request, $courseId, $id)
    {
        $course = $this->getCourseService()->getCourse($courseId);
        if ($course['type'] == 'certification') {
            return $this->redirect(
                $this->generateUrl('course_certification_thread_edit', array(
                    'courseId' => $courseId,
                    'id' => $id
                ))
            );
        } else {
            return $this->redirect(
                $this->generateUrl('course_thread_edit', array(
                    'courseId' => $courseId,
                    'id' => $id
                ))
            );
        }
    }

    protected function buildLayoutDataWithTakenAccess($request, $id)
    {
        $user                  = $this->getCurrentUser();
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id, $currentLoopTime);
        $response              = null;

        if (!$user->isLogin()) {
            $response = $this->createMessageResponse('info', '你好像忘了登录哦？', null, 3000, $this->generateUrl('login'));
        }

        $course['currentLoopTime'] = $currentLoopTime;

        if (!$this->getCourseService()->canTakeCourse($course, $currentLoopTime) && !$this->getCourseService()->canBranchAdminMangeCourse($course['id'])) {
            $response = $this->createMessageResponse('info', "您还不是课程《{$course['title']}》的学员，请先购买或加入学习。", null, 3000, $this->generateUrl('course_show', array('id' => $id)));
        }

        return array($course, $member, $response);
    }
}
