<?php

namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\BaseController;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Constants\DbValue;
use Custom\Common\Util\ImageUrlUtils;

class CertificationMarkerController extends BaseController
{
    public function manageAction(Request $request, $courseId, $lessonId, $type)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        $targetChoices = $this->getQuestionTargetChoices($course);

        $conditions = $request->request->all();

        list($paginator, $questions) = $this->getPaginatorAndQuestion($request, $conditions, $course, $lesson);

        $target  = isset($conditions['target']) ? $conditions['target'] : '';
        $keyword = isset($conditions['keyword']) ? $conditions['keyword'] : '';

        return $this->render("CustomWebBundle:CertificationMarker:index-{$type}.html.twig", array(
            'course'        => $course,
            'lesson'        => $lesson,
            'paginator'     => $paginator,
            'questions'     => $questions,
            'targetChoices' => $targetChoices,
            'target'        => $target,
            'keyword'       => $keyword,
            'type'          => $type
        ));
    }

    /**
     * 参数中mediaId 实际为 lessonId
     */
    public function metasAction(Request $request, $mediaId)
    {
        if (!$this->tryManageFaceMarker()) {
            return $this->createJsonResponse(false);
        }

        $lesson = $this->getCourseService()->getLesson($mediaId);
        $file   = $this->getUploadFileService()->getFile($lesson['mediaId']);

        $data = $request->query->all();

        if (isset($data['type']) && $data['type'] == 'face') {
            $markers = $this->getMarkerService()->findFaceMarkersMetaByMediaId($mediaId);
        } elseif (isset($data['type']) && $data['type'] == 'question') {
            $markers = $this->getMarkerService()->findQuestionMarkersMetaByMediaId($mediaId);
        } else {
            $markers = $this->getMarkerService()->findMarkersMetaByMediaId($mediaId);
        }

        $result = array('markersMeta' => $markers, 'videoTime' => $file['length']);

        return $this->createJsonResponse($result);
    }

    public function addMarkerAction(Request $request, $courseId, $lessonId)
    {
        if (!$this->tryManageFaceMarker()) {
            return $this->createJsonResponse(false);
        }

        $data = $request->request->all();

        $lesson = $this->getCourseService()->getLesson($lessonId);

        if (empty($lesson)) {
            return $this->createMessageResponse('error', '该课时不存在!');
        }

        $result = $this->getMarkerService()->addMarker($lesson['id'], $data);

        return $this->createJsonResponse($result);
    }

    public function editMarkerAction(Request $request)
    {
        if (!$this->tryManageFaceMarker()) {
            return $this->createJsonResponse(false);
        }

        $data       = $request->request->all();
        $data['id'] = isset($data['id']) ? $data['id'] : 0;
        $fields     = array(
            'updatedTime' => time(),
            'second'      => isset($data['second']) ? $data['second'] : ""
        );
        $marker = $this->getMarkerService()->updateMarker($data['id'], $fields);
        return $this->createJsonResponse($marker);
    }

    public function deleteMarkerAction(Request $request)
    {
        if (!$this->tryManageFaceMarker()) {
            return $this->createJsonResponse(false);
        }

        $data       = $request->request->all();
        $data['id'] = isset($data['id']) ? $data['id'] : 0;

        $result = $this->getMarkerService()->deleteMarker($data['id']);

        return $this->createJsonResponse($result);
    }

    public function showMarkersAction(Request $request, $lessonId)
    {
        if ($this->agentInWhiteList($request->headers->get("user-agent")) ? 1 : 0) {
            return $this->createJsonResponse(array());
        }

        $lesson       = $this->getCourseService()->getLesson($lessonId);
        $storage      = $this->getSettingService()->get('storage');
        $video_header = $this->getUploadFileService()->getFileByTargetType('headLeader');
        $user         = $this->getUserService()->getCurrentUser();
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $lesson['courseId']);

        $data = $request->query->all();

        $markers           = $this->getMarkerService()->findMarkersByMediaId($lesson['id']);
        $markerIds         = ArrayToolkit::column($markers, 'id');
        $questionMarkers   = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionMarkers   = ArrayToolkit::index($questionMarkers, 'markerId');
        $questionMarkerIds = ArrayToolkit::column($questionMarkers, 'markerId');
        $faceMarkerIds     = array_diff($markerIds, $questionMarkerIds);

// $allQuestionMarkers = $this->getMarkerService()->getMarkersByIds($questionMarkerIds);
        // $allFaceMarkers = $this->getMarkerService()->getMarkersByIds($faceMarkerIds);

        $results           = array();
        $faceMarkerResults = $this->getMarkerService()->getFaceMarkerStatus($user['id'], $faceMarkerIds, $currentLoopTime);

        foreach ($markers as $key => $marker) {
            $results[$key] = $marker;

            if (!empty($questionMarkers[$marker['id']])) {
                $marker['questionId']          = $questionMarkers[$marker['id']]['id'];
                $results[$key]['finished']     = true;
                $results[$key]['isFaceMarker'] = false;
            } else {
                if($faceMarkerResults[$marker['id']] == DbValue::TRUE) {
                    unset($results[$key]);
                    continue;
                }
                $marker['questionId']          = 0;
                $results[$key]['finished']     = $faceMarkerResults[$marker['id']];
                $results[$key]['isFaceMarker'] = true;
            }

            $results[$key]['videoHeaderTime'] = $storage['video_header'] ? intval($video_header['length']) : 0;
        }

        if (!empty($result)) {
            $result = ArrayToolkit::sortTwoDimensionArray($result, 'second');
        }
       
        return $this->createJsonResponse($results);
    }

    public function statusAction(Request $request, $mediaId)
    {
        $markers           = $this->getMarkerService()->findMarkersByMediaId($mediaId);
        $markerIds         = ArrayToolkit::column($markers, 'id');
        $questionMarkers   = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds);
        $questionMarkerIds = ArrayToolkit::column($questionMarkers, 'markerId');
        $faceMarkerIds     = array_diff($markerIds, $questionMarkerIds);
        $faceMarkers       = $this->getMarkerService()->searchMarkers(array('ids' => $faceMarkerIds), array('second', 'ASC'), 0, 1);
        return $this->createJsonResponse(empty($faceMarkers) ? 0 : $faceMarkers[0]['id']);
    }

    public function questionAction(Request $request, $courseId, $lessonId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        $targetChoices = $this->getQuestionTargetChoices($course);

        $conditions = $request->query->all();

        list($paginator, $questions) = $this->getPaginatorAndQuestion($request, $conditions, $course, $lesson);

        return $this->render('CustomWebBundle:CertificationMarker:question.html.twig', array(
            'course'        => $course,
            'lesson'        => $lesson,
            'paginator'     => $paginator,
            'questions'     => $questions,
            'targetChoices' => $targetChoices
        ));
    }

    public function searchAction(Request $request, $courseId, $lessonId)
    {
        $course = $this->getCourseService()->tryManageCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        $targetChoices = $this->getQuestionTargetChoices($course);

        $conditions = $request->request->all();

        list($paginator, $questions) = $this->getPaginatorAndQuestion($request, $conditions, $course, $lesson);

        return $this->render('CustomWebBundle:CertificationMarker:question-tr.html.twig', array(
            'course'        => $course,
            'lesson'        => $lesson,
            'paginator'     => $paginator,
            'questions'     => $questions,
            'targetChoices' => $targetChoices
        )
        );
    }

    public function markAsPassedAction(Request $request)
    {
        $markerId        = $request->request->get('markerId');
        $currentLoopTime = $request->request->get('currentLoopTime');
        $userId          = $this->getUserService()->getCurrentUser()['id'];
        $this->getMarkerService()->addFaceMarkerResult($markerId, $userId, true, $currentLoopTime);
        return $this->createJsonResponse(array('result' => true));
    }

    protected function getQuestionTargetChoices($course)
    {
        $lessons                           = $this->getCourseService()->getCourseLessons($course['id']);
        $choices                           = array();
        $choices["course-{$course['id']}"] = '本课程';

        foreach ($lessons as $lesson) {
            $choices["course-{$course['id']}/lesson-{$lesson['id']}"] = "课时{$lesson['number']}：{$lesson['title']}";
        }

        return $choices;
    }

    protected function getPaginatorAndQuestion($request, $conditions, $course, $lesson)
    {
        if (!isset($conditions['target']) || empty($conditions['target'])) {
            unset($conditions['target']);
            $conditions['targetPrefix'] = "course-{$course['id']}";
        }

        if (!empty($conditions['keyword'])) {
            $conditions['stem'] = $conditions['keyword'];
        }

        $conditions['parentId'] = 0;
        $conditions['types']    = array('determine', 'single_choice', 'uncertain_choice', 'fill', "choice");
        $orderBy                = array('createdTime', 'DESC');
        $paginator              = new Paginator(
            $request,
            $this->getQuestionService()->searchQuestionsCount($conditions),
            5
        );

        $paginator->setPageRange(4);

        $questions = $this->getQuestionService()->searchQuestions(
            $conditions,
            $orderBy,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $markerIds         = ArrayToolkit::column($this->getMarkerService()->findMarkersByMediaId($lesson['id']), 'id');
        $questionMarkerIds = ArrayToolkit::column($this->getQuestionMarkerService()->findQuestionMarkersByMarkerIds($markerIds), 'questionId');

        foreach ($questions as $key => $question) {
            $questions[$key]['exist'] = in_array($question['id'], $questionMarkerIds) ? true : false;
        }

        return array($paginator, $questions);
    }

    public function showMarkerFaceAction(Request $request, $lessonId)
    {
        $user = $this->getCurrentUser();

        $lesson = $this->getCourseService()->getLesson($lessonId);

        if (empty($lesson)) {
            return $this->createMessageResponse('error', '该课时不存在!');
        }

        $course = $this->getCourseService()->getCourse($lesson['courseId']);

        return $this->render('CustomWebBundle:CertificationMarker:camera-modal.html.twig', array(
            'course'      => $course,
            'faceImgPath' => ImageUrlUtils::getImagePath($user['faceImgPath'])
        ));
    }

    protected function tryManageFaceMarker()
    {
        $user = $this->getUserService()->getCurrentUser();

        if ($this->getUserService()->hasAdminRoles($user['id'])) {
            return true;
        }

        if (in_array("ROLE_TEACHER", $user['roles'])) {
            return true;
        }

        return false;
    }

    protected function getMarkerService()
    {
        return $this->getServiceKernel()->createService('Custom:Marker.MarkerService');
    }

    protected function getQuestionMarkerService()
    {
        return $this->getServiceKernel()->createService('Marker.QuestionMarkerService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }

    protected function getUploadFileService()
    {
        return $this->getServiceKernel()->createService('File.UploadFileService');
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    protected function getQuestionService()
    {
        return $this->getServiceKernel()->createService('Question.QuestionService');
    }
}
