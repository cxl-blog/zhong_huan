<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\Common\Paginator;
use Symfony\Component\HttpFoundation\Response;
use Topxia\WebBundle\Controller\CourseLessonManageController as BaseCourseLessionManageController;

class CertificationCourseLessonManageController extends BaseCourseLessionManageController
{
    public function indexAction(Request $request, $id)
    {
        $course      = $this->getCourseService()->tryManageCourse($id);
        $courseItems = $this->getCourseService()->getCourseItems($course['id']);

        $user          = $this->getCurrentUser();
        $isAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) || in_array('ROLE_ADMIN', $user['roles']);
        $items         = $this->getCourseService()->getCourseItems($id);
        $suggestHours = $this->getCourseService()->getTextSuggestedHoursForAdmin($items,$user['id']);
        $unitHours =$this->getCourseService()->getUnitSuggestedHours($items, $isAdmin, $user['id']);
        $chapterHours =$this->getCourseService()->getChapterSuggestedHours($items, $isAdmin, $user['id']);

        $lessonIds = ArrayToolkit::column($courseItems, 'id');

        if ($this->isPluginInstalled('Homework')) {
            $exercises = $this->getServiceKernel()->createService('Homework:Homework.ExerciseService')->findExercisesByLessonIds($lessonIds);
            $homeworks = $this->getServiceKernel()->createService('Homework:Homework.HomeworkService')->findHomeworksByCourseIdAndLessonIds($course['id'], $lessonIds);
        }

        $mediaMap = array();

        foreach ($courseItems as $item) {
            if ($item['itemType'] != 'lesson') {
                continue;
            }

            if (empty($item['mediaId'])) {
                continue;
            }

            if (empty($mediaMap[$item['mediaId']])) {
                $mediaMap[$item['mediaId']] = array();
            }

            $mediaMap[$item['mediaId']][] = $item['id'];
        }

        $mediaIds = array_keys($mediaMap);
        $files    = $this->getUploadFileService()->findFilesByIds($mediaIds);

        foreach ($files as $file) {
            $lessonIds = $mediaMap[$file['id']];

            foreach ($lessonIds as $lessonId) {
                $courseItems["lesson-{$lessonId}"]['mediaStatus'] = $file['convertStatus'];
            }
        }

        return $this->render('CustomWebBundle:CourseLessonManage:index_certification.html.twig', array(
            'course'    => $course,
            'items'     => $courseItems,
            'exercises' => empty($exercises) ? array() : $exercises,
            'homeworks' => empty($homeworks) ? array() : $homeworks,
            'files'     => ArrayToolkit::index($files, 'id'),
            'suggestHours' => $suggestHours,
            'unitHours' => $unitHours,
            'chapterHours' => $chapterHours
        ));
    }

    public function publishAction(Request $request, $courseId, $lessonId)
    {
        $this->getCourseService()->publishLesson($courseId, $lessonId);
        $course = $this->getCourseService()->getCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);

        $file = false;

        if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
            $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
            $lesson['mediaStatus'] = $file['convertStatus'];
        }
    
        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);

        return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
            'course' => $course,
            'lesson' => $lesson,
            'file'   => $file
        ));
    }

    public function unpublishAction(Request $request, $courseId, $lessonId)
    {
        $this->getCourseService()->unpublishLesson($courseId, $lessonId);

        $course = $this->getCourseService()->getCourse($courseId);
        $lesson = $this->getCourseService()->getCourseLesson($courseId, $lessonId);
        $file   = false;

        if ($lesson['mediaId'] > 0 && ($lesson['type'] != 'testpaper')) {
            $file                  = $this->getUploadFileService()->getFile($lesson['mediaId']);
            $lesson['mediaStatus'] = $file['convertStatus'];
        }

        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);

        return $this->render('TopxiaWebBundle:CourseLessonManage:list-item.html.twig', array(
            'course' => $course,
            'lesson' => $lesson,
            'file'   => $file
        ));
    }

    public function copyLessonAction(Request $request,$id)
    {
        return $this->copyLessonsAction($request, $id, true);
    }

    public function copyAllLessonAction(Request $request,$id)
    {
        return $this->copyLessonsAction($request, $id, false);
    }

    public function coursesSelectAction(Request $request, $courseId)
    {
        $this->getCourseService()->tryManageCourse($courseId);

        $data = $request->request->all();
        $ids  = array();


        if (isset($data['ids']) && $data['ids'] != "") {
            $ids = $data['ids'];
            $ids = explode(",", $ids);
        } else {
            return new Response('success');
        }

        foreach ($ids as $id) {
            $newCourse = $this->getCourseService()->getCourse($id);
            $this->getCourseCopyService()->newCopy($courseId,$newCourse);
        }
        $lessons = $this->getCourseService()->getCourseItems($courseId);
        $sortIds = array();
        foreach ($lessons as $lesson) {
            array_push($sortIds,$lesson['itemType'] . '-' . $lesson['id']);
        }
        
        $this->getCourseService()->sortCourseItems($courseId,$sortIds);
        $this->getCourseService()->updateCourseTotalHoursAndHasTestPaperFields($courseId);

        $this->setFlashMessage('success', "版本课程复制成功");

        return new Response('success');
    }


    protected function getTagService()
    {
        return $this->getServiceKernel()->createService('Taxonomy.TagService');
    }

    protected function getCourseCopyService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseCopyService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    private function copyLessonsAction(Request $request, $id, $onlyCurrentCert)
    {
        $this->getCourseService()->tryManageCourse($id);
        $currentCourse = $this->getCourseService()->getCourse($id);
        $conditions['type']     = 'certification';

        if (!empty($request->query->get('title'))) {
            $conditions['title'] = $request->query->get('title');
        }

        if ($onlyCurrentCert) {
            $conditions['areaCode']  = $currentCourse['areaCode'];
        }
        
        $allCourses = $this->getCourseService()->searchCourses(
            $conditions, 
            null, 
            0, 
            PHP_INT_MAX
        );
        $courseIds = array();
        foreach ($allCourses as $key => $course) {
            if (!$onlyCurrentCert || $this->isSameCert($course, $currentCourse)) {
                if($course['id'] != $currentCourse['id'] && $currentCourse['versionStatus'] != 'old') {
                    array_push($courseIds, $course['id']);
                }
            }
        }
        if (empty($courseIds)) {
            $courseIds = array(null);
        }
        $conditions['courseIds'] = $courseIds;
        $paginator = new Paginator(
            $this->get('request'),
            $this->getCourseService()->searchCourseCount($conditions),
            5
        );

        $courses = $this->getCourseService()->searchCourses(
            $conditions,
            'latest',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $userIds   = array();

        foreach ($courses as &$course) {
            $course['tags'] = $this->getTagService()->findTagsByIds($course['tags']);
            $userIds        = array_merge($userIds, $course['teacherIds']);
        }

        $users = $this->getUserService()->findUsersByIds($userIds);
        return $this->render(
            'CustomWebBundle:CourseLessonManage/Course:course-pick-modal.html.twig',
            array(
                'users'       => $users,
                'courses'     => $courses,
                'courseId'    => $id,
                'paginator'   => $paginator,
                'all'         => !$onlyCurrentCert,
                'conditions'  => $conditions
            )
        );
    }

    private function isSameCert($course, $currentCourse)
    {
        return $course['areaCode'] == $currentCourse['areaCode'] 
            && $course['certificationId'] == $currentCourse['certificationId'];
    }
}
