<?php

namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Custom\WebBundle\Controller\CustomBaseController;
use Symfony\Component\HttpFoundation\Response;
use Custom\Common\Util\CompressionUtils;
use Custom\Common\Exception\UnencryptedException;

class DataSyncController extends CustomBaseController
{
    public function syncSingleAreaAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleArea($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleUserAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleUser($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleCertificateAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleCertificate($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleOfflineFinishedCourseAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleOfflineFinishedCourse($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleApprovalResultAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleApprovalResult($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleStuCertificateAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $result = $this->getSyncService()->syncSingleStuCertificate($this->getUncompressedText($request->get('data')));
                return $this->createTextResponse($result);
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleRevokeCertificateAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleRevokeCertificate($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleResetPasswordAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleResetPassword($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleUserRegisterAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleUserRegister($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleUserCertificateFrozenAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleUserCertificateFrozen($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    public function syncSingleUserCourseFrozenAction(Request $request)
    {
        try {
            if ($this->isClientIpValid($request->get('clientIp'))) {
                $this->getSyncService()->syncSingleUserCourseFrozen($this->getUncompressedText($request->get('data')));
            }
        } catch (\Exception $e) {
            return $this->createJsonResponse(array('result' => false, 'errorMsg' => $e->getMessage()));
        }

        return $this->createJsonResponse(array('result' => true));
    }

    protected function getSyncService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncService');
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    private function isClientIpValid($clientIp)
    {
        $ips = $this->getSettingService()->get('whitelist_ip', array());
        if(empty($ips) || empty($ips['ips'])){
            throw new \Exception("ip error,whitelist is empty");
        }
        else{
            if(in_array($clientIp, $ips['ips'])){
                return true;
            }
            else{
                throw new \Exception("ip error,current ip:".$clientIp);
            }
        }
    }

    private function getUncompressedText($text)
    {
        try {
            $convertedText = str_replace(' ', '+', $text);  //http传输时, 会自动将 + 转化为空格, 需转回来
            $unencryptedText = CompressionUtils::decodeByBase64Gzip($convertedText);
            return $unencryptedText;
        } catch (\Exception $e) {
            //throw new UnencryptedException();
        }
    }
}
