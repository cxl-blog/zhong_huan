<?php
namespace Custom\WebBundle\Controller;

use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\QuestionMarkerController as BaseQuestionMarkerController;

class QuestionMarkerController extends BaseQuestionMarkerController
{
    //新增弹题
    public function addQuestionMarkerAction(Request $request, $courseId, $lessonId)
    {
        if (!$this->tryManageQuestionMarker()) {
            return $this->createJsonResponse(false);
        }

        $data = $request->request->all();

        $lesson = $this->getCourseService()->getLesson($lessonId);

        if (empty($lesson)) {
            return $this->createMessageResponse('error', '该课时不存在!');
        }

        $data['questionId'] = isset($data['questionId']) ? $data['questionId'] : 0;
        $question           = $this->getQuestionService()->getQuestion($data['questionId']);

        if (empty($question)) {
            return $this->createMessageResponse('error', '该题目不存在!');
        }

        if (empty($data['markerId'])) {
            $result = $this->getMarkerService()->addQuestionMarker($lesson['id'], $data);
            return $this->createJsonResponse($result);
        } else {
            $marker = $this->getMarkerService()->getMarker($data['markerId']);

            if (!empty($marker)) {
                $questionmarker = $this->getQuestionMarkerService()->addQuestionMarker($data['questionId'], $marker['id'], $data['seq']);
                return $this->createJsonResponse($questionmarker);
            } else {
                return $this->createJsonResponse(false);
            }
        }
    }

    public function doNextTestAction(Request $request)
    {
        $data               = $request->query->all();
        $data['markerId']   = isset($data['markerId']) ? $data['markerId'] : 0;
        $data['questionId'] = isset($data['questionId']) ? $data['questionId'] : 0;
        $data['answer']     = isset($data['answer']) ? $data['answer'] : null;
        $data['type']       = isset($data['type']) ? $data['type'] : null;
        $user               = $this->getUserService()->getCurrentUser();

        $questionMaker = $this->getQuestionMarkerService()->getQuestionMarker($data['questionId']);
        $question      = $this->getQuestionService()->getQuestion($questionMaker['questionId']);
        $target        = explode('-', $question['target']);

        if ($target[0] == 'course') {
            $data['currentLoopTime'] = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $target[1]);
        } else {
            $data['currentLoopTime'] = 1;
        }

        $questionMarkerResult = $this->getQuestionMarkerResultService()->finishCurrentQuestion($data['markerId'], $user['id'], $data['questionId'], $data['answer'], $data['type'], $data['lessonId'], $data['currentLoopTime']);

        $data = array(
            'markerId'               => $data['markerId'],
            'questionMarkerResultId' => $questionMarkerResult['id']
        );
        return $this->createJsonResponse($data);
    }

    //获取驻点弹题
    public function showMarkerQuestionAction(Request $request, $markerId)
    {
        $user      = $this->getUserService()->getCurrentUser();
        $question  = array();
        $data      = $request->query->all();
        $questions = $this->getQuestionMarkerService()->findQuestionMarkersByMarkerId($markerId);
        $lesson = $this->getCourseService()->getLesson($data['lessonId']);        
        $currentLoopTime = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $lesson['courseId']) ? CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $lesson['courseId']) : CourseLoopUtils::$START_LOOP_TIME;

        if ($this->getMarkerService()->isFinishMarker($user['id'], $markerId,$currentLoopTime)) {
            if (isset($data['questionId'])) {
                $question   = $this->getQuestionMarkerService()->getQuestionMarker($data['questionId']);
                $conditions = array(
                    'seq'      => ++$question['seq'],
                    'markerId' => $markerId
                );
                $question = $this->getQuestionMarkerService()->searchQuestionMarkers($conditions, array('seq', 'ASC'), 0, 1);

                if (!empty($question)) {
                    $question = $question['0'];
                }
            } else {
                $conditions = array(
                    'seq'      => 1,
                    'markerId' => $markerId
                );
                $question = $this->getQuestionMarkerService()->searchQuestionMarkers($conditions, array('seq', 'ASC'), 0, 1);
                $question = $question[0];
            }
        } else {
            foreach ($questions as $key => $value) {
                $questionResult = $this->getQuestionMarkerResultService()->findByUserIdAndQuestionMarkerId($user['id'], $value['id']);

                if (empty($questionResult)) {
                    $question = $value;
                    break;
                }
            }
        }

        return $this->render('TopxiaWebBundle:Marker:question-modal.html.twig', array(
            'markerId' => $markerId,
            'question' => $question,
            'lessonId' => $data['lessonId']
        ));
    }

    public function showQuestionAnswerAction(Request $request, $questionId)
    {
        $data                 = $request->query->all();
        $user                 = $this->getUserService()->getCurrentUser();
        $questionMarker       = $this->getQuestionMarkerService()->getQuestionMarker($questionId);
        $questionMarkerResult = $this->getQuestionMarkerResultService()->getQuestionMarkerResult($data['questionMarkerResultId']);
        $conditions           = array(
            'markerId' => $data['markerId']
        );
        $count                 = $this->getQuestionMarkerService()->searchQuestionMarkersCount($conditions);
        $questionMarker['seq'] = isset($questionMarker['seq']) ? $questionMarker['seq'] : 1;
        $progress              = array(
            'seq'     => $questionMarker['seq'],
            'count'   => $count,
            'percent' => floor($questionMarker['seq'] / $count * 100)
        );
        $compelete = $progress['percent'] == 100 ? true : false;

        return $this->render('CustomWebBundle:Marker:answer.html.twig', array(
            'markerId'   => $data['markerId'],
            'question'   => $questionMarker,
            'answer'     => $questionMarker['answer'],
            'selfAnswer' => unserialize($questionMarkerResult['answer']),
            'status'     => $questionMarkerResult['status'],
            'progress'   => $progress,
            'compelete'  => $compelete
        ));
    }

    protected function getMarkerService()
    {
        return $this->getServiceKernel()->createService('Custom:Marker.MarkerService');
    }
}
