<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Service\Util\EdusohoLiveClient;
use Symfony\Component\HttpFoundation\Request;
use Topxia\Service\Order\OrderProcessor\OrderProcessorFactory;
use Custom\Common\Util\PublicWeixinPayment;
use Custom\Common\Util\WeChatUtils;
use Topxia\WebBundle\Controller\BaseController as TopxiaBaseController;

class AwardController extends TopxiaBaseController
{   
    public function buyDetailAction(Request $request)
    {   
        $fields    = $request->request->all();

        if ($request->getMethod() == 'POST') {
            return $this->createOrder($fields);
        } else {
            $openid = $request->query->get('openid');
            if (empty($openid)) {
                $openid = $this->authorizeUser($request);
            }
            //$openid = "ozQqSwgKVxOnARBNolT8Ds5MfzgQ";    //调试专用
        }

        return $this->render('CustomWebBundle:Award:buy-info.html.twig', array(
            'openid' => $openid
        ));
    }

    protected function authorizeUser(Request $request)
    { 
        $code = $request->query->get('code');
        list($appid, $secret) = $this->getPaymentInfo();
        $weChatInfos = WeChatUtils::getAccessTokenAndOpenId($appid, $secret, $code);
        
        return $weChatInfos['openid'];
    }

    protected function getPaymentInfo()
    {
        $payment = $this->getSettingService()->get('payment', array());
        return array($payment['wxpay_public_key'], $payment['wxpay_public_key_secret']);
    }

    protected function createOrder($fields)
    {
        try {
            if (empty($fields['areacode'])) {
                $user = $this->getUserService()->getUserByLoginField($fields['mobile']);
            } else {
                $user = $this->getUserService()->getUserByLoginFieldAndAreaCode($fields['mobile'], $fields['areacode']);
            }

            if (empty($user)) {
                return $this->createJsonResponse(array('success' => false, 'message' => '用户不存在'));
            }

            $userProfile = $this->getUserService()->getUserProfile($user['id']);
            if ($userProfile['idcard'] != $fields['idcard']) {
                return $this->createJsonResponse(array('success' => false, 'message' => '身份证与手机号不匹配'));
            }
             
            $conditions = array(
                'amount' => $fields['amount'],
                'payment' => 'wxpay_public',
                'userId' => $user['id'],
                'targetType' => 'coin'
            );
            $order = $this->getCashOrdersService()->addOrder($conditions);
            $order['openid'] = $fields['openid'];
            
            $payment = new PublicWeixinPayment();
            $result = $payment->submitPayment($this,
                $order, $this->setting('payment', array()), '');
            
            return $this->createJsonResponse(array('success' => true,  'params' => $result,
                    'order' => $order));
        } catch (\RuntimeException $e) {
            return $this->createJsonResponse(
                array('success' => false, 'message' => $e->getMessage())
            );
        }
    }

    public function buySuccessAction(Request $request, $orderId, $openid)
    {   
        $order = $this->getCashOrdersService()->getOrder($orderId);
        $account = $this->getCashAccountService()->getAccountByUserId($order['userId']);
        $coin = $this->getSettingService()->get('coin');
        return $this->render('CustomWebBundle:Award:buy-success.html.twig', array(
            'order' => $order,
            'account' => $account,
            'coin' => $coin,
            'openid' => $openid
        ));
    }

    public function orderInfoAction(Request $request, $orderId, $openid)
    {   
        $order = $this->getCashOrdersService()->getOrder($orderId);
        $user = $this->getUserService()->getUser($order['userId']);
        $userProfile = $this->getUserService()->getUserProfile($order['userId']);
        $areaCode = $this->getAreaService()->getAreaByCode($user['areaCode']);
        return $this->render('CustomWebBundle:Award:order-info.html.twig', array(
            'order' => $order,
            'idcard' => $this->hideData($userProfile['idcard']),
            'mobile' => $this->hideData($userProfile['mobile']),
            'areaCode' => $areaCode,
            'openid' => $openid
        ));
    }

    protected function hideData($data)
    {
        $length = strlen($data);
        if ($length == 0) {
            return;
        }
        return substr($data, 0, 4) . str_repeat('*', $length - 8) . substr($data, $length - 4, 4);
    }

    protected function getCashOrdersService()
    {
        return $this->getServiceKernel()->createService('Cash.CashOrdersService');
    }

    protected function getCashAccountService()
    {
        return $this->getServiceKernel()->createService('Cash.CashAccountService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }
}
