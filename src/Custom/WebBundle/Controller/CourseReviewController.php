<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Custom\Common\Util\CourseHeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\CourseReviewController as BaseCourseReviewController;

class CourseReviewController extends BaseCourseReviewController
{
    public function listAction(Request $request, $id)
    {
        list($course, $member) = $this->getCourseService()->buildCourseLayoutData($request, $id);

        if ($course['parentId']) {
            $classroom = $this->getClassroomService()->findClassroomByCourseId($course['id']);

            if (!$this->getClassroomService()->canLookClassroom($classroom['classroomId'])) {
                return $this->createMessageResponse('info', "非常抱歉，您无权限访问该{$classroomSetting['name']}，如有需要请联系客服", '', 3, $this->generateUrl('homepage'));
            }
        }

        $paginator = new Paginator(
            $this->get('request'),
            $this->getReviewService()->getCourseReviewCount($id)
            , 10
        );

        $reviews = $this->getReviewService()->findCourseReviews(
            $id,
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $user       = $this->getCurrentUser();
        $userReview = $user->isLogin() ? $this->getReviewService()->getUserCourseReview($user['id'], $course['id']) : null;

        $users   = $this->getUserService()->findUsersByIds(ArrayToolkit::column($reviews, 'userId'));

        $infos = array(
            'reviewSaveUrl' => $this->generateUrl('course_review_create', array('id' => $course['id'])),
            'userReview' => $userReview,
            'reviews' => $reviews,
            'users' => $users,
            'paginator' => $paginator,
            'member' => $member
        );
        $headerInfos = CourseHeaderUtils::getNormalHeaderInfos($this, $user, $course, $member,$infos);

        return $this->render('CustomWebBundle:Course:reviews.html.twig', $headerInfos);
    }

    public function createAction(Request $request, $id)
    {
        $user = $this->getCurrentUser();

        $fields             = $request->request->all();
        $fields['userId']   = $user['id'];
        $fields['courseId'] = $id;
        $currentLoopTime    = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $id);
        $this->getReviewService()->saveReview($fields, $currentLoopTime);

        return $this->createJsonResponse(true);
    }
}
