<?php

namespace Custom\WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Custom\WebBundle\Controller\CustomBaseController;
use Custom\Common\Util\CompressionUtils;
use Topxia\Service\Common\ServiceKernel;
use Custom\Common\ClassFtp;
use Topxia\Common\ArrayToolkit;

class UrlSenderController extends CustomBaseController
{
    public function sendIndexAction(Request $request)
    {
        //$this->testFtp();
        //$this->generateWsdl();
        // $this->testWebservice();
        return $this->render('CustomWebBundle:UrlSender:sendIndex.html.twig');
    }

    public function sendAction(Request $request)
    {
        $sendUrl = $request->get('sendUrl');
        $requestParams = json_decode($request->get('requestBody'), true);
        $requestType = $request->get('requestType');

        $ch = curl_init();  
        $timeout = 5;  

        $header = array(
            'content-type: application/x-www-form-urlencoded; charset=UTF-8'
        );

        $paramStrs = '';
        $isFirstParam = true;

        if ($requestParams == null) {
            $responseJson = array(
                'responseText' => '参数必须是json(key 和 value 都要用双引号)'
            );
            return $this->createJsonResponse($responseJson);
        }

        foreach ($requestParams as $key => $value) {
            if (!$isFirstParam) {
                $paramStrs .=  '&';
            } else {
                $isFirstParam = false;
            }
            $paramStrs .= $key . '=' . $value;

            if ($key != 'base64ImgData') {
                array_push($header, $key . ':' . $value);
            }
        }

        if (isset($requestParams['token'])) {
            array_push($header, 'X-Auth-Token' . ':' . $requestParams['token']);
        }

        if ($requestType == 'POST') {
            curl_setopt ($ch, CURLOPT_POSTFIELDS, $paramStrs);
            curl_setopt($ch, CURLOPT_POST, 1);
        } else {
            $sendUrl .= '?' . $paramStrs;
            curl_setopt($ch, CURLOPT_POST, 0);
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt ($ch, CURLOPT_URL, $sendUrl);  
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 

        $responseText = curl_exec($ch);  
        curl_close($ch); 
        try {
            $responseJson= json_decode($responseText, true);
            if (empty($responseJson)) {
                $responseJson = array('responseText' => $responseText);
            }
        } catch (\Exception $e) {
            $responseJson = array('responseText' => $responseText);
        }
        
        return $this->createJsonResponse($responseJson);
    }

    private function testWebservice()
    {
        $client = new \SoapClient("http://zhonghuan.dev11.edusoho.cn/dataSync/DataSyncServer.php?wsdl");
        echo($client->syncData('syncSingleOfflineFinishedCourse', $this->getEncryptedText('dataSync/sampleData/syncSingleOfflineFinishedCourse')));
        echo "<br />"; 
    }

    private function testFtp()
    {
        $ftpHost = '123.232.113.166';
        $ftpPort = 21;
        $ftpName = 'zh';
        $ftpPassword = 'zh';
        $sourceFile = '/var/www/edusoho/README.md';
        $remoteFile = '/test/README.md';
        $ftp = new ClassFtp($ftpHost, $ftpPort, $ftpName, $ftpPassword);
        $ftp->up_file($sourceFile, $remoteFile);
        $ftp->close();

        var_dump('success upload');
    }

    private function generateWsdl()
    {
        $service = $this->getSoapDiscoveryService();
        $service->setClassName('DataSyncService');
        $service->setServiceName('soap');   //服务名, 随便写
        $service->getWSDL();
    }

    private function getSoapDiscoveryService()
    {
        return $this->getServiceKernel()->createService('Custom:WsdlGenerator.SoapDiscoveryService');
    }

    private function getEncryptedText($filePath)
    {
        $text = file_get_contents($filePath);
        $encryptedText = CompressionUtils::encodeByGzipBase64($text);
        return $encryptedText;
    }

    private function getUserCertCourseDao() 
    {
        return $this->getServiceKernel()->createDao('Custom:Certification.UserCertCourseDao');
    }

    protected function getSyncFtpService()
    {
        return $this->getServiceKernel()->createService('Custom:Sync.SyncFtpService');
    }

    private function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getStatisticsService()
    {
        return $this->getServiceKernel()->createService('Custom:System.StatisticsService');
    }
}
