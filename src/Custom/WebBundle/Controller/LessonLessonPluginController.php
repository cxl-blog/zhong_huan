<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Custom\Common\Util\CourseLoopUtils;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\LessonLessonPluginController as TopxiaLessonLessonPluginController;

class LessonLessonPluginController extends TopxiaLessonLessonPluginController
{
    public function listAction(Request $request)
    {
        $user                  = $this->getCurrentUser();
        $courseId              = $request->query->get('courseId');
        $isLogin               = $user->isLogin();
        $currentLoopTime       = CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId) ? CourseLoopUtils::getCurrentLoopTime($request, $user['id'], $courseId) : CourseLoopUtils::$START_LOOP_TIME;
        list($course, $member) = $this->getCourseService()->tryTakeCourse($courseId, $currentLoopTime);

        $items         = $this->getCourseService()->getCourseItems($course['id']);
        $learnStatuses = $this->getCourseService()->getLessonStatus($user['id'], $course['id'], $currentLoopTime, $items);
        $certificationCourse = $this->getCertificateService()->getCertificationsForUser($user['id'], false, $course['id'], $currentLoopTime);

        $homeworkPlugin     = $this->getAppService()->findInstallApp('Homework');
        $homeworkLessonIds  = array();
        $exercisesLessonIds = array();

        $testpaperIds = array();
        array_walk($items, function ($item, $key) use (&$testpaperIds) {
            if ($item['type'] == 'testpaper') {
                array_push($testpaperIds, $item['mediaId']);
            }
        });

        $testpapers = $this->getTestpaperService()->findTestpapersByIds($testpaperIds);

        if ($homeworkPlugin) {
            $lessons            = $this->getCourseService()->getCourseLessons($course['id']);
            $lessonIds          = ArrayToolkit::column($lessons, 'id');
            $homeworks          = $this->getHomeworkService()->findHomeworksByCourseIdAndLessonIds($course['id'], $lessonIds);
            $exercises          = $this->getExerciseService()->findExercisesByLessonIds($lessonIds);
            $homeworkLessonIds  = ArrayToolkit::column($homeworks, 'lessonId');
            $exercisesLessonIds = ArrayToolkit::column($exercises, 'lessonId');
        }

        $renderPage = "TopxiaWebBundle:LessonLessonPlugin:list.html.twig";

        if ($course['type'] == 'certification') {
            $renderPage = "CustomWebBundle:LessonLessonPlugin:list.html.twig";
        }

        $certificationLessonLearns = $this->findCertificationLessonLearnsByUserIdAndCourseId($user['id'], $course['id'], $currentLoopTime);
        $nextLesson                = $this->getCourseService()->getUserValideNextLearnLesson($user['id'], $course['id'], $currentLoopTime);
        $params = array(
            'course'                    => $course,
            'items'                     => $items,
            'learnStatuses'             => $learnStatuses,
            'currentTime'               => time(),
            'weeks'                     => array("日", "一", "二", "三", "四", "五", "六"),
            'homeworkLessonIds'         => $homeworkLessonIds,
            'exercisesLessonIds'        => $exercisesLessonIds,
            'member'                    => $member,
            'testpapers'                => $testpapers,
            'suggestHours'              => array(),
            'isAdmin'                   => $user->isSuperAdmin(),
            'suggestHoursForAdmin'      => array(),
            'certificationCourse'       => $certificationCourse,
            'certificationLessonLearns' => $certificationLessonLearns,
            'nextLessonId'              => !empty($nextLesson) ? $nextLesson['id'] : 0,
            'isLogin'                   => $isLogin
        );

        if ($course['type'] == 'certification') {
            $params['areaCertification'] = $this->getCertificateService()->findByAreaCodeAndCertificationId(
                $course['areaCode'], 
                $course['certificationId']
            );
        }
        return $this->render($renderPage, $params);
    }

    public function findCertificationLessonLearnsByUserIdAndCourseId($userId, $courseId, $currentLoopTime)
    {
        $certificationLessonLearns = $this->getCourseService()->findUserLearnedLessons($userId, $courseId, $currentLoopTime);
        $certificationLessonLearns = ArrayToolkit::index($certificationLessonLearns, 'lessonId');
        return $certificationLessonLearns;
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }
}
