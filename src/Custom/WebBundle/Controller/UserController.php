<?php
namespace Custom\WebBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\ImgConverToData;
use Custom\Common\Constants\DbValue;
use Symfony\Component\HttpFoundation\Request;
use Topxia\WebBundle\Controller\UserController as TopxiaUserController;

class UserController extends TopxiaUserController
{
    public function headerBlockAction($user)
    {
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $user        = array_merge($user, $userProfile);

        if ($this->getCurrentUser()->isLogin()) {
            $isFollowed = $this->getUserService()->isFollowed($this->getCurrentUser()->id, $user['id']);
        } else {
            $isFollowed = false;
        }

        // 关注数
        $following = $this->getUserService()->findUserFollowingCount($user['id']);
        // 粉丝数
        $follower = $this->getUserService()->findUserFollowerCount($user['id']);

        return $this->render('CustomWebBundle:User:header-block.html.twig', array(
            'user'       => $user,
            'isFollowed' => $isFollowed,
            'following'  => $following,
            'follower'   => $follower
        ));
    }

    public function showAction(Request $request, $id)
    {
        $user                 = $this->tryGetUser($id);
        $userProfile          = $this->getUserService()->getUserProfile($user['id']);
        $userProfile['about'] = strip_tags($userProfile['about'], '');
        $userProfile['about'] = preg_replace("/ /", "", $userProfile['about']);
        $user                 = array_merge($user, $userProfile);

        return $this->followingAction($request,$user['id']);
    }

    protected function _learnAction($user)
    {
        $paginator = new Paginator(
            $this->get('request'),
            $this->getCourseService()->findUserLearnCourseCountNotInClassroom($user['id']),
            20
        );

        $courses = $this->getCourseService()->findUserLearnCoursesNotInClassroom(
            $user['id'],
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('CustomWebBundle:User:courses.html.twig', array(
            'user'      => $user,
            'courses'   => $courses,
            'paginator' => $paginator,
            'type'      => 'about'
        ));
    }

    public function aboutAction(Request $request, $id)
    {
        $user = $this->tryGetUser($id);
        return $this->_aboutAction($user);
    }

    protected function _aboutAction($user)
    {
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        return $this->render('CustomWebBundle:User:about.html.twig', array(
            'user'        => $user,
            'userProfile' => $userProfile,
            'type'        => 'about'
        ));
    }

    public function followingAction(Request $request, $id)
    {
        $user                 = $this->tryGetUser($id);
        $userProfile          = $this->getUserService()->getUserProfile($user['id']);
        $userProfile['about'] = strip_tags($userProfile['about'], '');
        $userProfile['about'] = preg_replace("/ /", "", $userProfile['about']);
        $user                 = array_merge($user, $userProfile);

        $paginator = new Paginator(
            $this->get('request'),
            $this->getUserService()->findUserFollowingCount($user['id']),
            20
        );

        $followings = $this->getUserService()->findUserFollowing($user['id'], $paginator->getOffsetCount(), $paginator->getPerPageCount());

        if ($followings) {
            $followingIds          = ArrayToolkit::column($followings, 'id');
            $followingUserProfiles = ArrayToolkit::index($this->getUserService()->searchUserProfiles(array('ids' => $followingIds), array('id', 'ASC'), 0, count($followingIds)), 'id');
        }

        $myfollowings = $this->_getUserFollowing();

        return $this->render('CustomWebBundle:User:friend.html.twig', array(
            'user'           => $user,
            'paginator'      => $paginator,
            'friends'        => $followings,
            'userProfile'    => $userProfile,
            'myfollowings'   => $myfollowings,
            'allUserProfile' => isset($followingUserProfiles) ? $followingUserProfiles : array(),
            'friendNav'      => 'following'
        ));
    }

    public function followerAction(Request $request, $id)
    {
        $user                 = $this->tryGetUser($id);
        $userProfile          = $this->getUserService()->getUserProfile($user['id']);
        $userProfile['about'] = strip_tags($userProfile['about'], '');
        $userProfile['about'] = preg_replace("/ /", "", $userProfile['about']);
        $user                 = array_merge($user, $userProfile);
        $myfollowings         = $this->_getUserFollowing();

        $paginator = new Paginator(
            $this->get('request'),
            $this->getUserService()->findUserFollowerCount($user['id']),
            20
        );

        $followers = $this->getUserService()->findUserFollowers($user['id'], $paginator->getOffsetCount(), $paginator->getPerPageCount());

        if ($followers) {
            $followerIds          = ArrayToolkit::column($followers, 'id');
            $followerUserProfiles = ArrayToolkit::index($this->getUserService()->searchUserProfiles(array('ids' => $followerIds), array('id', 'ASC'), 0, count($followerIds)), 'id');
        }

        return $this->render('CustomWebBundle:User:friend.html.twig', array(
            'user'           => $user,
            'paginator'      => $paginator,
            'friends'        => $followers,
            'userProfile'    => $userProfile,
            'myfollowings'   => $myfollowings,
            'allUserProfile' => isset($followerUserProfiles) ? $followerUserProfiles : array(),
            'friendNav'      => 'follower'
        ));
    }

    public function showRegisterFaceAction($userId)
    {
        $user = $this->getUserService()->getUser($userId);

        $imgConverToData = new ImgConverToData;
        $imgConverToData->getImgDir($user['faceImgPath']);
        $imgConverToData->img2Data();
        $imgData = $imgConverToData->data2Img();
        echo $imgData;
        exit;
    }

    public function facePhotoFinishedAction(Request $request, $id)
    {
        $this->getUserService()->setBatchFacePhotoFinish($id, DbValue::TRUE);
        return $this->createJsonResponse(array('result' => true));
    }
}
