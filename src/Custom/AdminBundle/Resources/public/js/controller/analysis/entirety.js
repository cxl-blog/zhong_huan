define(function(require,exports,module) {
   
	require('../echarts/echarts-all');
	var autoSubmitCondition=require("./autoSubmitCondition");

	exports.run = function() {

		if($('#data').length > 0){
			var data   = $.parseJSON($('#data').val());
			//基于准备好的dom，初始化echarts图表
		    var chart 　= echarts.init(document.getElementById('container'));
            var option = {
			    title : {
			        text: data.title+'已注册学员分布图',
			        x:'left'
			    },
			    tooltip : {
			        trigger: 'item',
                    enterable: true,
                    formatter: function(p){
                        //切开字符串,但是字符串最后是一个 空格, 所以会多一个元素
                        var keyLises = p[0].split(" ");
                        //删掉多余的那个元素.我们的key就准备好了
                        keyLises.pop();

                        //取出values 准备拼接
                        var values = p.data;
                        //用来放结果的数组
                        var ret = [];
                        //把keyList做循环,拼上对应的数值放到结果数组里
                        $.each(keyLises,function(i,e){

                            //根据名字找下标,这是一个自执行的函数,根据名字去找到对应的index ,比如: "新疆" 找到 0 .
                            var valueIndex = (function(key){
                                for(var i = 0; i <data.series.keys.length;i++){
                                    if (data.series.keys[i].value == key) return i;
                                }
                            })(e);
                            //拼接名字和数字,放到数组里
                            ret.push(e);
                        });

                        //用"<br/>"把数组里的元素都链接起来(这样就不用拼接的时候加换行了,而且不用处理最后一个元素多一个换行的问题)
                        ret.push("&nbsp&nbsp"+values.name+"&nbsp:&nbsp&nbsp"+values.value);
                        return ret.join("<br/>");
                    }

                },
			    legend: {
			        orient: 'vertical',
			        x:'right',
			        data:data.category
			    },
			    dataRange: {
			        min: 0,
			        max: data.max,
			        x: 'left',
			        y: 'bottom',
			        text:['高','低'],
			        calculable : true
			    },
			    toolbox: {
			        show: false,
			        orient : 'vertical',
			        x: 'right',
			        y: 'center',
			        feature : {
			            mark : {show: true},
			            dataView : {show: true, readOnly: false},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    series : data.series
			};
		    // 为echarts对象加载数据
			chart.setOption(option);
		}
		autoSubmitCondition.autoSubmitCondition();
	};
})