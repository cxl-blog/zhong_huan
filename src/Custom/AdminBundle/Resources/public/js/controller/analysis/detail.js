define(function(require, exports, module) {

    require('../highcharts/highcharts');
    require("jquery.bootstrap-datetimepicker");
    var autoSubmitCondition=require("./autoSubmitCondition");
    var Validator = require('bootstrap.validator');
    require('common/validator-rules').inject(Validator);
    var now = new Date();

    exports.run = function() {
        if($('#data').length > 0){
            var data   = $.parseJSON($('#data').val());
            $('#container').highcharts({
                title: {
                    text: '',
                    x: -20 //center
                },
                xAxis: {
                    categories: data.date
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                tooltip: {
                    formatter: function () {
                        var tip = '<b>'+this.x+'</b>';
                        $.each(this.points,function (){
                            tip += '<br/>'+this.series.name+': '+this.y;
                        });
                        return tip;
                    },
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: data.series
            });
        }
        $("[name=endTime]").datetimepicker({
            language: 'zh-CN',
            autoclose: true,
            format: 'yyyy-mm-dd',
            minView: 'month'
        });
        $('[name=endTime]').datetimepicker('setEndDate',now);
        $('[name=endTime]').datetimepicker('setStartDate',$('#registerStartDate').attr("value"));
        $("[name=startTime]").datetimepicker({
            language: 'zh-CN',
            autoclose: true,
            format: 'yyyy-mm-dd',
            minView: 'month'
        });
        $('[name=startTime]').datetimepicker('setEndDate',now);
        $('[name=startTime]').datetimepicker('setStartDate',$('#registerStartDate').attr("value"));

        var validator = new Validator({
            element: '#analysis-form'
        });

        validator.addItem({
            element: '[name=startTime]',
            required: true,
            rule:'date_check'
        });

        validator.addItem({
            element: '[name=endTime]',
            required: true,
            rule:'date_check'
        });
        
        autoSubmitCondition.autoSubmitCondition();
    }

    if($('#graduationData').length > 0){
            var graduationData   = $.parseJSON($('#graduationData').val());
            $('#graduationContainer').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                  text: ''
                },
                xAxis: {
                    categories: graduationData.timeRange
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: '结业数量统计'
                    },
                    allowDecimals:false,
                    lineWidth: 1
                },
                legend: {
                    reversed: true
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                series: graduationData.series
            });
            autoSubmitCondition.autoSubmitCondition();
        }
})