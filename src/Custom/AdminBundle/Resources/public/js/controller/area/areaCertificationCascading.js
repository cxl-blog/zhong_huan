/**
 * 根据行政区域的编码动态刷新出证书列表
 */
function cascadeCertificationPerAreaCode() {
    var eleAreaCode = $(".eleAreaCode");
    var eleCertification = $(".eleCertification");

    eleAreaCode.on("change",
        function() {
            var prevSelectedCertification =  eleCertification.val();
            eleCertification.empty().append("<option value=''>请选择证书</option>");
            if (this.value != "") {
                new AjaxContext({
                    url:  "/area/" + eleAreaCode.val() + "/getCertificationsPerAreaCode",
                    token: $("[name='csrf-token']").attr("content"), 
                    dataType: "JSON",  
                    success: function(data, textStatus) { 
                        var certIdMonths = {};
                        for (var key in data) {
                            var selected = data[key].id == prevSelectedCertification ? "selected" : "";
                            eleCertification.append("<option value='" + data[key].id + "' "  +  selected + " >" + data[key].category + " [当前周期: "  + data[key]["months"] + "个月]</option>");
                            certIdMonths[key] = data[key]['months'];
                        }
                        $(".hiddenCertMonths").val(JSON.stringify(certIdMonths));
                        if (eleCertification.val() != "") {
                            eleCertification.change();
                        }   
                    }
                }).load();
            }
        }
    );
}

/**
 * 替换占位符  如 
 *   replaceHolderStr('str_0 : {0} str_1 : {1} str_2 : {2}', 'x', 'y', 'z')  输出为 str_0 : x str_1 : y str_2 : z
 */
function replaceHolderStr() {
    var strArray = [];
    for (var i = 1 ; i < arguments.length ; i++) {
        strArray.push(arguments[i]);
    }
    return arguments[0].replace(/\{(\d+)\}/g, function (m, i) {
        return strArray[i];
    });
}

/*
 * 根据输入的行政区域, 证书, 学制, 课时部分 查询 
 */
function refreshPartAndVersionStatus() {
    var eleAreaCode = $(".eleAreaCode");
    var eleLearnMode = $(".eleLearnMode");
    var eleCertification = $(".eleCertification");
    var currentPartIndex = $(".elePartIndex").val();

    if (eleAreaCode.val() != "" && eleLearnMode.val() != "" 
            && eleCertification.val() != "" && currentPartIndex != "") {
        var requestParam = {
            areaCode: eleAreaCode.val(),
            certificationId: eleCertification.val(),
            learnModeMonths: eleLearnMode.val(),
            courseId: $("#courseId").val(),
            partIndex: currentPartIndex
        };

        var ajaxParam = {
            url: "/course/certification/manage/queryCertStatus",
            token: $("[name='csrf-token']").attr("content"),
            param: requestParam,
            success: function(data) {
                $(".oldCourseId").val("");
                //不是最新版本的课程, 不会覆盖新版本课程
                if ($(".versionStatus").val() == 'latest' && typeof data.oldCourseId != "undefined") {
                    var msgTemplate = "该证书已经存在课程(#{0}) 中，继续保存，将会导致购买该证书时，只能购买当前创建的课程，但已购买该证书的用户会继续学习课程(#{0})";
                    var msg = replaceHolderStr(msgTemplate, data.oldCourseName);
                    $errMsgContainer = $("<span class='text-danger'>" + msg + "</span>")
                    $('.oldCourseIdTip').find(".danger").empty().append($errMsgContainer);
                    $('.oldCourseIdTip').show();
                    $(".oldCourseId").val(data.oldCourseId);
                } else {
                    $('.oldCourseIdTip').hide();
                }
            }
        };

        new AjaxContext(ajaxParam).load();
    }
}