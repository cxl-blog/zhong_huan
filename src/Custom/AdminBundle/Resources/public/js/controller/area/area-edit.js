define(function(require, exports, module) {

  var Validator = require('bootstrap.validator');
  require('common/validator-rules').inject(Validator);
  var Notify = require('common/bootstrap-notify');

  exports.run = function() {

    var $modal = $('#area-edit-form').parents('.modal');

    var validator = new Validator({
      element: '#area-edit-form',
      autoSubmit: false,
      failSilently: true,
      onFormValidated: function(error, results, $form) {
        if (error) {
          return false;
        }
        $('#edit-area-btn').button('submiting').addClass('disabled');

        var data = {};

        var faceable = $(":radio[name='faceable']:checked").val();                               
        data['faceable'] = faceable;

        var schoolId = $("select option:selected").val();
        data['schoolId'] = schoolId;
        
        var isApproval = $(":radio[name='isApproval']:checked").val();
        data['isApproval'] = isApproval;

        var isTestpaperFaceable = $(":radio[name='isTestpaperFaceable']:checked").val();
        data['isTestpaperFaceable'] = isTestpaperFaceable;

        var isClassInteraction = $(":radio[name='isClassInteraction']:checked").val();
        data['isClassInteraction'] = isClassInteraction;
        
        data['maxSnapNum'] = $("[name='maxSnapNum']").val();
        data['snapInterval'] = $("[name='snapInterval']").val();

        var areaCertification = new Array();
        $("#area-edit-form").find(".area_certification").each(function(){
          areaCertification.push({
            "id": $(this).find(":hidden").val(), 
            "learnMode": $(this).find(":radio[name*='learnMode']:checked").val(), 
            "relearn": $(this).find(":radio[name*='relearn']:checked").val(), 
            'months': $(this).find("[name='months']").val(), 
            'learnModeMonths': $(this).find('[name="learnModeMonths"]').val(),
            'sequentialLearning': $(this).find(':radio[name*="sequentialLearning"]:checked').val(),
            'maxLearningMinutes': $(this).find('[name*="maxLearningMinutes"]').val(),
            'showTestpaperAnswer': $(this).find(':radio[name*="showTestpaperAnswer"]:checked').val()
          });
        });                   
        if(areaCertification.length>0){
          data['areaCertification']=areaCertification;
        }

        $.post($form.attr('action'),data,function(html) {
          $modal.modal('hide');
          Notify.success('行政区设置成功');
          var table = $(html).find('table').html();
          $('table').html(table);
        }).error(function(){
          Notify.danger('操作失败');
        });
      }
    });
    
    $(".months").each(function(){
      validator.addItem({
        element: this,
        required: true,
        rule:'positive_integer min{min: 1} max{max: 120} minusBy',
        errormessageMinusBy: '周期必须要能整除学制'
      });
    });

    $('[name="maxLearningMinutes"]').each(function(){
      validator.addItem({
        element: this,
        required: true,
        rule:'positive_integer min{min: 1} max{max: 1440}'
      });
    });

    $("[name='isTestpaperFaceable']").click(
      function() {
        if ($(this).val() == 1) {
          $(".testpaperFaceableSection").show();
        } else {
          $(".testpaperFaceableSection").hide();
        }
      }
    );
    
    validator.addItem({
      element: $("#maxSnapNum")[0],
      required: true,
      rule:'positive_integer min{min: 1} max{max: 100}',
      display: '最大拍摄数量',
      skipHidden: true
    });

    validator.addItem({
      element: $("#snapInterval")[0],
      required: true,
      rule:'positive_integer min{min: 1} max{max: 1000}',
      display: '拍摄时间间隔',
      skipHidden: true
    });
  }

});