<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\AdminBundle\Controller\CoinController as BaseController;

class CoinController extends BaseController
{    
    /**
     * @param [type] $cashType RMB | Coin
     */
    public function exportCsvAction(Request $request, $cashType)
    {
        $payment    = array('alipay' => '支付宝', 'wxpay' => '微信支付', 'wxpay_mobile' => '微信支付(APP)', 'coin' => '虚拟币支付', 'none' => '--', 'offlinepay' => '线下支付');
        $conditions = $request->query->all();

        if (!empty($conditions) && $cashType == 'Coin') {
            $conditions = $this->filterCondition($conditions);
        }

        if (!empty($conditions) && $cashType == 'RMB') {
            $conditions = $this->filterConditionBill($conditions);
        }

        $conditions['cashType'] = $cashType;

        $num            = $this->getCashService()->searchFlowsCount($conditions);
        $orders         = $this->getCashService()->searchFlows($conditions, array('ID', 'DESC'), 0, $num);
        $studentUserIds = ArrayToolkit::column($orders, 'userId');

        $users = $this->getUserService()->findUsersByIds($studentUserIds);
        $users = ArrayToolkit::index($users, 'id');

        $profiles = $this->getUserService()->findUserProfilesByIds($studentUserIds);
        $profiles = ArrayToolkit::index($profiles, 'id');

        $str = "流水号,账目名称,购买者,姓名,收支,支付方式,创建时间";

        $str .= "\r\n";

        $results = array();

        foreach ($orders as $key => $orders) {
            $member = "";
            $member .= "流水号".$orders['sn'].",";
            $member .= $orders['name'].",";
            $member .= $users[$orders['userId']]['nickname'].",";
            $member .= $profiles[$orders['userId']]['truename'] ? $profiles[$orders['userId']]['truename']."," : "-".",";

            if ($orders['type'] == 'inflow') {
                $member .= "+".$orders['amount'].",";
            }

            if ($orders['type'] == 'outflow') {
                $member .= "-".$orders['amount'].",";
            }

            if (!empty($orders['payment'])) {
                $member .= $payment[$orders['payment']].",";
            } else {
                $member .= "-".",";
            }

            $member .= date('Y-n-d H:i:s', $orders['createdTime']).",";
            $results[] = $member;
        }

        $str .= implode("\r\n", $results);
        $str = chr(239).chr(187).chr(191).$str;

        $filename = sprintf("%s-order-(%s).csv", $cashType, date('Y-n-d'));

        $response = new Response();
        $response->headers->set('Content-type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
        $response->headers->set('Content-length', strlen($str));
        $response->setContent($str);

        return $response;
    }
}