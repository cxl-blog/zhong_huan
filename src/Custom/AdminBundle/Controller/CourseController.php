<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\CourseController as BaseCourceController;

abstract class CourseController extends BaseCourceController
{
    public function indexAction(Request $request, $filter)
    {
        $conditions = $request->query->all();

        if ($this->getFilterType() == 'normal') {
            $conditions["parentId"] = 0;
            $conditions['types']     = array('normal', 'live');
        }

        if ($this->getFilterType() == 'certification') {
            $conditions["parentId"] = 0;
            $conditions['type']     = 'certification';
        }

        if (isset($conditions["categoryId"]) && $conditions["categoryId"] == "") {
            unset($conditions["categoryId"]);
        }

        if (isset($conditions["status"]) && $conditions["status"] == "") {
            unset($conditions["status"]);
        }

        if (isset($conditions["title"]) && $conditions["title"] == "") {
            unset($conditions["title"]);
        }

        if (isset($conditions["creator"]) && $conditions["creator"] == "") {
            unset($conditions["creator"]);
        }

        if (isset($conditions["areaCode"]) && empty($conditions['areaCode'])) {
            unset($conditions["areaCode"]);
        }

        if (isset($conditions["certificationId"]) && empty($conditions['certificationId'])) {
            unset($conditions["certificationId"]);
        }

        $coinSetting = $this->getSettingService()->get("coin");
        $coinEnable  = isset($coinSetting["coin_enabled"]) && $coinSetting["coin_enabled"] == 1 && $coinSetting['cash_model'] == 'currency';

        if (isset($conditions["chargeStatus"]) && $conditions["chargeStatus"] == "free") {
            $conditions['price'] = '0.00';
        }

        if (isset($conditions["chargeStatus"]) && $conditions["chargeStatus"] == "charge") {
            $conditions['price_GT'] = '0.00';
        }
        $count     = $this->getCourseService()->searchCourseCount($conditions);
        $paginator = new Paginator($this->get('request'),$count,20);
        $courses   = $this->getCourseService()->searchCourses(
            $conditions,null,$paginator->getOffsetCount(),$paginator->getPerPageCount()
        );
        $categories    = $this->getCategoryService()->findCategoriesByIds(ArrayToolkit::column($courses,'categoryId'));

        $users         = $this->getUserService()->findUsersByIds(ArrayToolkit::column($courses,'userId'));

        $courseSetting = $this->getSettingService()->get('course', array());

        if (!isset($courseSetting['live_course_enabled'])) {
            $courseSetting['live_course_enabled'] = "";
        }

        $default       = $this->getSettingService()->get('default', array());

        $params        = array(
            'conditions'     => $conditions,
            'courses'        => $courses,
            'users'          => $users,
            'categories'     => $categories,
            'paginator'      => $paginator,
            'liveSetEnabled' => $courseSetting['live_course_enabled'],
            'default'        => $default,
            'filter'         => $this->getFilterType()
        );

        $params = $this->setIndexResultParams($params, $courses);

        return $this->render($this->getRenderedIndexPage(), $params);
    }

    public function dataAction(Request $request,$filter)
    {
        $conditions = $request->query->all();
        $conditions['parentId'] = 0;
        $conditions['status']   = 'published';

        if($filter == 'normal'){
            $conditions['type'] = 'normal';
        }else{
            $conditions['type'] = 'certification';
        }

        if (isset($conditions["title"]) && empty($conditions["title"])) {
            unset($conditions["title"]);
        }

        if (isset($conditions["creator"]) && empty($conditions["creator"])) {
            unset($conditions["creator"]);
        }

        if (isset($conditions["areaCode"]) && empty($conditions['areaCode'])) {
            unset($conditions["areaCode"]);
        }

        if (isset($conditions["categoryId"]) && empty($conditions["categoryId"])) {
            unset($conditions["categoryId"]);
        }
        
        if (isset($conditions["certificationId"]) && empty($conditions['certificationId'])) {
            unset($conditions["certificationId"]);
        }

        $count     = $this->getCourseService()->searchCourseCount($conditions);
        $paginator = new Paginator($this->get('request'),$count,20);
        $courses   = $this->getCourseService()->searchCourses(
            $conditions,null,$paginator->getOffsetCount(),$paginator->getPerPageCount()
        );
        foreach ($courses as $key => $course) {
            $isLearnedNum = $this->getCourseService()->searchMemberCount(array('isLearned'=>1,'courseId'=>$course['id']));

            $learnTime = $this->getCourseService()->searchLearnTime(array('courseId'=>$course['id']));

            $lessonCount = $this->getCourseService()->searchLessonCount(array('courseId'=>$course['id']));

            $courses[$key]['isLearnedNum'] = $isLearnedNum;
            $courses[$key]['learnTime']    = $learnTime;
            $courses[$key]['lessonCount']  = $lessonCount;
        }

        $params = array('filter'=>$filter,'courses'=>$courses,'paginator'=>$paginator);

        $params = $this->setIndexResultParams($params,$courses);

        return $this->render('CustomAdminBundle:Course:data.html.twig',$params);
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function renderCourseTr($courseId, $request)
    {
        $fields     = $request->query->all();
        $course     = $this->getCourseService()->getCourse($courseId);
        $default    = $this->getSettingService()->get('default', array());
        $classrooms = array();

        if ($fields['filter'] == 'classroom') {
            $classrooms = $this->getClassroomService()->findClassroomsByCoursesIds(array($course['id']));
            $classrooms = ArrayToolkit::index($classrooms, 'courseId');

            foreach ($classrooms as $key => $classroom) {
                $classroomInfo                      = $this->getClassroomService()->getClassroom($classroom['classroomId']);
                $classrooms[$key]['classroomTitle'] = $classroomInfo['title'];
            }
        }

        return $this->render('CustomAdminBundle:Course:tr.html.twig', array(
            'user'       => $this->getUserService()->getUser($course['userId']),
            'category'   => $this->getCategoryService()->getCategory($course['categoryId']),
            'course'     => $course,
            'default'    => $default,
            'classrooms' => $classrooms,
            'filter'     => $fields["filter"]
        ));
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    /**
     * 课程类型, 目前主要分为normal, classroom, certification
     */
    abstract protected function getFilterType();

    abstract protected function getRenderedIndexPage();

    abstract protected function setIndexResultParams(array $params,array $courses);

}
