<?php

namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AreaController extends BaseController
{
    public function areaAction(Request $request)
    {
        $conditions = $request->query->all();

        if (isset($conditions['keyword'])) {
            $conditions['name'] = $conditions['keyword'];
            unset($conditions['keyword']);
        }

        $areaCount = $this->getAreaService()->searchAreasCount($conditions);

        $paginator = new Paginator($request, $areaCount, 20);

        $areas = $this->getAreaService()->searchAreas(
            $conditions, array('id', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('CustomAdminBundle:Area:area-show.html.twig', array(
            'areas' => $areas,
            'paginator' => $paginator
        ));
    }

    public function editAction(Request $request, $id)
    {
        $area = $this->getAreaService()->getArea($id);          
        if (empty($area)) {
            throw $this->createNotFoundException();
        }

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();
            $areaField['faceable']   = $formData['faceable'];
            $areaField['isApproval'] = $formData['isApproval'];
            $areaField['isTestpaperFaceable'] = $formData['isTestpaperFaceable'];
            $areaField['maxSnapNum'] = $formData['maxSnapNum'];
            $areaField['snapInterval'] = $formData['snapInterval'];
            $areaField['isClassInteraction'] = $formData['isClassInteraction'];

            if (isset($formData['schoolId'])) {
                $areaField['schoolId'] = $formData['schoolId'];
            }

            $this->getAreaService()->updateArea($id, $areaField);

            // 从 需要实名认证 改为 非实名认证时, 需要更新
            if (!$areaField['isApproval'] && $area['schoolId']>0 && $area['isApproval']) {
                $this->getCourseService()->completeSchoolCourses($area['schoolId']);
            }

            if (isset($formData['areaCertification'])) {
                $certificationFields = $formData['areaCertification'];
                foreach ($certificationFields as $certificationField) {
                    $areaCertificationId = $certificationField['id'];
                    unset($certificationField['id']);
                    $this->getCertificationService()->updateAreaCertification($areaCertificationId, $certificationField);
                }
            }

            return $this->redirect($this->generateUrl('admin_area'));
        }

        $areaCertifications = $this->getCertificationService()->findAreaCertificationsByAreaCode($area['code']);
        $certificateIds     = ArrayToolkit::column($areaCertifications, 'certificationId');
        $certifications     = $this->getCertificationService()->getCertifications($certificateIds);
        $certifications     = ArrayToolkit::index($certifications, 'id');

        $schools = array();

        if ($this->isPluginInstalled('BranchSchool')) {
            $areas         = $this->getAreaService()->searchAreas(array('NoSchoolId' => 0), array('id', 'DESC'), 0, 999);
            $schoolId      = $area['schoolId'];
            $schoolIds     = ArrayToolkit::column($areas, 'schoolId');
            $schoolIds     = $schoolId == 0 ? $schoolIds : array_diff($schoolIds, array($schoolId));
            $branchSchools = $this->getBranchSchoolService()->searchBranchSchools(array('excludeIds' => $schoolIds), array('id', 'DESC'), 0, 999);

            foreach ($branchSchools as $branchSchool) {
                $schools[$branchSchool['id']] = $branchSchool['name'];
            }
        }

        return $this->render('CustomAdminBundle:Area:area-model.html.twig',
            array(
                'area'               => $area,
                'schools'            => $schools,
                'certifications'     => $certifications,
                'areaCertifications' => $areaCertifications
            )
        );
    }

    /**
     * 返回
     * [
     *      {
     *          id: 1,
     *          category: '自行车'
     *      }  ...
     * ]
     */
    public function getCertificationsPerAreaCodeAction($areaCode)
    {
        $areaCertifications = $this->getCertificationService()->findAreaCertificationsByAreaCode($areaCode);
        $indexAreaCerts = ArrayToolkit::index($areaCertifications, 'certificationId');
        $certificationIds   = ArrayToolkit::column($areaCertifications, 'certificationId');
        $certifications     = $this->getCertificationService()->getCertifications($certificationIds);
        $certifications     = ArrayToolkit::index($certifications, 'id');
        foreach ($certifications as $key => $cert) {
            $certifications[$key]['months'] = $indexAreaCerts[$key]['months'];
        }
        return $this->createJsonResponse($certifications);
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getBranchSchoolService()
    {
        return $this->getServiceKernel()->createService('BranchSchool:BranchSchool.BranchSchoolService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }
}
