<?php

namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Constants\DbValue;
use Topxia\WebBundle\DataDict\UserRoleDict;
use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\UserController as TopxiaUserController;

class UserController extends TopxiaUserController
{
    public function indexAction(Request $request)
    {
        $fields = $request->query->all();

        $conditions = array(
            'roles'           => '',
            'keywordType'     => '',
            'keyword'         => '',
            'keywordUserType' => '',
            'toLearn'         => 0
        );

        if (empty($fields)) {
            $fields = array();
        }

        $conditions = array_merge($conditions, $fields);

        $userCount = $this->getUserService()->searchUserCount($conditions);
        $paginator = new Paginator(
            $this->get('request'), $userCount, 20
        );

        $users = $this->getUserService()->searchUsers(
            $conditions,
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $app = $this->getAppService()->findInstallApp("UserImporter");

        $showUserExport = false;

        if (!empty($app) && array_key_exists('version', $app)) {
            $showUserExport = version_compare($app['version'], "1.0.2", ">=");
        }

        $userIds  = ArrayToolkit::column($users, 'id');
        $profiles = $this->getUserService()->findUserProfilesByIds($userIds);

        $areaCodes = ArrayToolkit::column($users, 'areaCode');
        $areas     = $this->getAreaService()->searchAreas(array('codes' => $areaCodes), array('id', 'DESC'), 0, 999);
        $areas     = ArrayToolkit::index($areas, 'code');

        if (!empty($users)) {
            $users = $this->getUserService()->updateAdminAreaNameIfNeed($users, $conditions);
        }

        return $this->render('CustomAdminBundle:User:index.html.twig', array(
            'users'          => $users,
            'areas'          => $areas,
            'userCount'      => $userCount,
            'paginator'      => $paginator,
            'profiles'       => $profiles,
            'showUserExport' => $showUserExport,
            'role'           => $conditions['roles']
        ));
    }

    public function editAction(Request $request, $id)
    {
        $user = $this->getUserService()->getUser($id);

        $profile          = $this->getUserService()->getUserProfile($user['id']);
        $profile['title'] = $user['title'];

        if ($request->getMethod() == 'POST') {
            $profile = $request->request->all();

            if (!((strlen($user['verifiedMobile']) > 0) && isset($profile['mobile']))) {
                $profile = $this->getUserService()->updateUserProfile($user['id'], $profile);
                $this->getLogService()->info('user', 'edit', "管理员编辑用户资料 {$user['nickname']} (#{$user['id']})", $profile);
            } else {
                $this->setFlashMessage('danger', '用户已绑定的手机不能修改。');
            }

            return $this->redirect($this->generateUrl('admin_user'));
        }

        $fields = $this->getFields();
        return $this->render('CustomAdminBundle:User:edit-modal.html.twig', array(
            'user'    => $user,
            'profile' => $profile,
            'fields'  => $fields
        ));
    }

    public function checkRepeatAction(Request $request, $userId)
    {
        $idcard      = $request->query->get('value');
        $userProfile = $this->getUserService()->findUserProfileByIdcard($idcard);

        if ($userProfile['id'] == $userId) {
            $response = array('success' => true, 'message' => '');
        } else {
            $response = array('success' => false, 'message' => '身份证号已存在');
        }

        return $this->createJsonResponse($response);
    }

    public function clearFaceAction(Request $request, $id)
    {
        $this->getUserService()->clearBatchFace($id);
        return $this->createJsonResponse(true);
    }

    public function showAction(Request $request, $id)
    {
        $user                 = $this->getUserService()->getUser($id);
        $user['certificates'] = $userCertifications = $this->getCertificateService()->findCertificatesIncludingRevokedByUserId($user['id']);
        $profile              = $this->getUserService()->getUserProfile($id);
        $profile['title']     = $user['title'];

        $fields       = $this->getFields();
        $isSuperAdmin = in_array('ROLE_SUPER_ADMIN', $user['roles']) ? true : false;

        return $this->render('CustomAdminBundle:User:show-modal.html.twig', array(
            'user'         => $user,
            'profile'      => $profile,
            'fields'       => $fields,
            'isSuperAdmin' => $isSuperAdmin
        ));
    }

    /**
     * 此方法不对外开放, 只是为了方便测试, 将用户管理最后一列显示出来后,点击,再手动刷新即可
     */
    public function reset2ToLearnerAction($id)
    {
        $this->getUserService()->updateFields($id,
            array(
                'toLearn'     => DbValue::TRUE,
                'nickname'    => $id,
                'faceImgPath' => ''
            )
        );
        $this->getUserService()->setMobile($id, '');
    }

    public function rolesAction(Request $request, $id)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')
            && false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $user        = $this->getUserService()->getUser($id);
        $currentUser = $this->getCurrentUser();

        if ($request->getMethod() == 'POST') {
            $roles = $request->request->get('roles');

            $this->getUserService()->changeUserRoles($user['id'], $roles);

            $dataDict     = new UserRoleDict();
            $roleDict     = $dataDict->getDict();
            $role         = "";
            $roleCount    = count($roles);
            $deletedRoles = array_diff($user['roles'], $roles);
            $addedRoles   = array_diff($roles, $user['roles']);

            if (!empty($deletedRoles) || !empty($addedRoles)) {
                for ($i = 0; $i < $roleCount; $i++) {
                    $role .= $roleDict[$roles[$i]];

                    if ($i < $roleCount - 1) {
                        $role .= "、";
                    }
                }

                $message = array(
                    'userId'   => $currentUser['id'],
                    'userName' => $currentUser['nickname'],
                    'role'     => $role);
                $this->getNotifiactionService()->notify($user['id'], 'role', $message);
            }

            if (in_array('ROLE_TEACHER', $user['roles']) && !in_array('ROLE_TEACHER', $roles)) {
                $this->getCourseService()->cancelTeacherInAllCourses($user['id']);
            }

            $user = $this->getUserService()->getUser($id);
            return $this->render('TopxiaAdminBundle:User:user-table-tr.html.twig', array(
                'user'    => $user,
                'profile' => $this->getUserService()->getUserProfile($id)
            ));
        }

        $default = $this->getSettingService()->get('default', array());
        return $this->render('CustomAdminBundle:User:roles-modal.html.twig', array(
            'user'    => $user,
            'default' => $default
        ));
    }

    public function changePasswordAction(Request $request, $userId)
    {
        $currentUser = $this->getCurrentUser();
        $user        = $this->getUserService()->getUser($userId);

        if (!in_array('ROLE_SUPER_ADMIN', $currentUser['roles'])) {
            throw $this->createAccessDeniedException();
        }

        if ($request->getMethod() == 'POST') {
            $formData = $request->request->all();
            
            $this->getAuthService()->changeBatchPassword($userId, null, $formData['newPassword']);

            return $this->createJsonResponse(true);
        }

        return $this->render('TopxiaAdminBundle:User:change-password-modal.html.twig', array(
            'user' => $user
        ));
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }
}
