<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\MobileController as BaseMobileController;

abstract class MobileBaseController extends BaseMobileController
{
    public function versionSettingAction(Request $request)
    {
        $config = $this->getConfig();
        $filePath = dirname($_SERVER['DOCUMENT_ROOT']).'/app/config/';
        $fileName = $config['configName'];

        if ($request->getMethod() == 'POST') {
            $versionInfos = $request->request->all();

            $this->getMobileService()->writeMobileInfosToFile($fileName, $filePath, $versionInfos);
            $this->setFlashMessage('success', '移动端设置已保存！');
            return $this->redirect($this->generateUrl($config['redirectRouterUrl']));
        }

        $fileContent = $this->getMobileService()->readMobileInfosFromFile($fileName, $filePath);
        return $this->render($config['renderPage'], array(
            'fileContent' => $fileContent
        ));
    }

    protected function getMobileService()
    {
        return $this->getServiceKernel()->createService('Custom:Mobile.MobileService');
    }

    /**
     * @return 
     * {
     *     'configName': 'versionConfig.yml',  // app/config 目录下
     *     'redirectRouterUrl': 'admin_mobile_version_setting_android',
     *     'renderPage': 'CustomAdminBundle:Mobile:android_index.html.twig'
     * }
     */
    protected abstract function getConfig();
}