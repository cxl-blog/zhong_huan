<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Custom\Service\FaceDetect\FaceDetectService;
use Topxia\Component\OAuthClient\OAuthClientFactory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Topxia\AdminBundle\Controller\UserSettingController as BaseUserSettingController;

class UserSettingController extends BaseUserSettingController
{
    public function loginConnectAction(Request $request)
    {
        $loginConnect = $this->getSettingService()->get('login_bind', array());

        $default = array(
            'login_limit'                     => 0,
            'enabled'                         => 0,
            'verify_code'                     => '',
            'captcha_enabled'                 => 0,
            'temporary_lock_enabled'          => 0,
            'temporary_lock_allowed_times'    => 5,
            'ip_temporary_lock_allowed_times' => 20,
            'temporary_lock_minutes'          => 20,
            'face_detect_rate'                => FaceDetectService::DEFAULT_FACE_DETECT_RATE
        );

        $clients = OAuthClientFactory::clients();

        foreach ($clients as $type => $client) {
            $default["{$type}_enabled"]          = 0;
            $default["{$type}_key"]              = '';
            $default["{$type}_secret"]           = '';
            $default["{$type}_set_fill_account"] = 0;
        }

        $loginConnect = array_merge($default, $loginConnect);

        if ($request->getMethod() == 'POST') {
            $loginConnect = $request->request->all();
            $this->getSettingService()->set('login_bind', $loginConnect);
            $this->getLogService()->info('system', 'update_settings', "更新登录设置", $loginConnect);
            $this->setFlashMessage('success', '登录设置已保存！');
        }

        return $this->render('CustomAdminBundle:System:login-connect.html.twig', array(
            'loginConnect' => $loginConnect,
            'clients'      => $clients
        ));
    }
}
