<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Custom\Common\Constants\LearnMode;

class CertificationCourseController extends CourseController
{

    protected function getFilterType()
    {
        return 'certification';
    }

    protected function getRenderedIndexPage()
    {
        return 'CustomAdminBundle:Course:index_certification.html.twig';
    }

    public function publishAction(Request $request,$id)
    {
        $course = $this->getCourseService()->getCourse($id);
        if ($course['lessonNum'] == 0 ) {
            return new Response('publishResultFailed');
        } 
        $this->getCourseService()->publishCourse($id);

        return $this->renderCourseTr($id,$request);
    }

    public function updateAsLatestAction(Request $request,$id)
    {
        $oldCourseIds = $this->getCourseService()->updateAsLatestCourse($id);

        return $this->renderCourseTr($id, $request, $oldCourseIds);
    }

    public function closeAction(Request $request,$id)
    {
        $this->getCourseService()->closeCourse($id);

        return $this->renderCourseTr($id,$request);
    }

    protected function renderCourseTr($courseId,$request, $additionalParams = array())
    {
        $course        = $this->getCourseService()->getCourse($courseId);
        $default       = $this->getSettingService()->get('default',array());
        $area          = $this->getAreaService()->getAreaByCode($course['areaCode']);
        $certification = $this->getCertificateService()->getCertification($course['certificationId']);
        $areaCertification = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'],$course['certificationId']);

        return $this->render('CustomAdminBundle:Course:tr_certification.html.twig',array(
            'user'          => $this->getUserService()->getUser($course['userId']),
            'course'        => $course,
            'default'       => $default,
            'area'          => $area,
            'certification' => $certification,
            'areaCertification' => $areaCertification,
            'additionalParams' => join(',', $additionalParams)
        ));
    }

    protected function setIndexResultParams(array $params, array $courses)
    {
        $areas              = $this->getAreaService()->searchAreas(array(),array('id','DESC'),0,99);
        $areas              = ArrayToolkit::index($areas,'code');
        $areaCodes          = array();
        foreach ($areas as $area) {     
            $areaCodes[$area['code']] = $area['name'];
        }

        $certifications     = $this->getCertificateService()->getAllCertifications();
        $certifications     = ArrayToolkit::index($certifications,'id');
        $certificates       = array();
        foreach ($certifications as $certification) {
            $certificates[$certification['id']] = $certification['category'];
        }

        $params['areas']              = $areas;
        $params['areaCodes']          = $areaCodes;
        $params['certificates']       = $certificates;
        $params['certifications']     = $certifications;
        $params['courses'] = LearnMode::formatCertificationType($params['courses']);

        if (!empty($courses)) {
            $areaCertifications = $this->getLearnMode($courses);
            $areaCertifications = ArrayToolkit::column($areaCertifications, 'areaCertification');

            $params['areaCertifications'] = $areaCertifications;
        }
        return $params;
    }

    protected function getLearnMode(array $courses)
    {
        $params = array();
        foreach ($courses as &$course ) {
            $fields = $this->getCertificateService()->findByAreaCodeAndCertificationId($course['areaCode'],$course['certificationId']);
            if(!empty($fields)){
                $course['areaCertification'] = $fields;
                $params[$course['id']] = $course;
            }
        }
        return $params;      
    }

}
