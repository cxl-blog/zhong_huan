<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\AdminBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SettingController extends BaseController
{
    public function ipWhitelistAction(Request $request)
    {
        $ips = $this->getSettingService()->get('whitelist_ip', array());

        if (!empty($ips)) {
            $default['ips'] = join("\n", $ips['ips']);
            $ips            = array_merge($ips, $default);
        }

        if ($request->getMethod() == 'POST') {
            $data       = $request->request->all();
            $ips['ips'] = array_filter(explode(' ', str_replace(array("\r\n", "\n", "\r"), " ", $data['ips'])));
            $this->getSettingService()->set('whitelist_ip', $ips);
            $this->getLogService()->info('system', 'update_settings', "更新IP白名单", $ips);

            $ips        = $this->getSettingService()->get('whitelist_ip', array());
            $ips['ips'] = join("\n", $ips['ips']);

            $this->setFlashMessage('success', '保存成功！');
        }

        return $this->render('CustomAdminBundle:System:ip-whitelist.html.twig', array(
            'ips' => $ips
        ));
    }

    public function mobileIapProductAction(Request $request)
    {
        $products = $this->getSettingService()->get('mobile_iap_product', array());
        if ($request->getMethod() == 'POST') {
            $fileds = $request->request->all();

            //新增校验
            if (empty($fileds['productId']) || empty($fileds['title']) || empty($fileds['price']) || !is_numeric($fileds['price'])) {
                $this->setFlashMessage('danger', '产品ID或商品名称或价格输入不正确');
                return $this->redirect($this->generateUrl('admin_setting_mobile_iap_product'));
            }

            //新增
            $products[$fileds['productId']] = array(
                'productId' => $fileds['productId'],
                'title' => $fileds['title'],
                'price' => $fileds['price']
            );
            $this->getSettingService()->set('mobile_iap_product', $products);

            $this->getLogService()->info('system', 'update_settings', '更新IOS内购产品设置', $products);
            $this->setFlashMessage('success', 'IOS内购产品设置已保存');
            return $this->redirect($this->generateUrl('admin_setting_mobile_iap_product'));
        }

        return $this->render('CustomAdminBundle:System:mobile-iap-product.html.twig', array(
            'products' => $products
        ));
    }

    public function mobileIapProductDeleteAction(Request $request, $productId)
    {
        $products = $this->getSettingService()->get('mobile_iap_product', array());

        if (array_key_exists($productId, $products)) {
            unset($products[$productId]);
        }

        $this->getSettingService()->set('mobile_iap_product', $products);

        return $this->createJsonResponse(true);
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    protected function getLogService()
    {
        return $this->getServiceKernel()->createService('System.LogService');
    }
}
