<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\BaseController;

class CourseOrderController extends BaseController
{
    public function manageAction(Request $request)
    {
        return $this->forward('CustomAdminBundle:Order:manage', array(
            'request' => $request,
            'targetType' => 'course',
            'layout' => 'CustomAdminBundle:CourseOrder:order.html.twig',
        ));
    }

}