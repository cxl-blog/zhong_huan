<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\Common\ImgConverToData;
use Topxia\AdminBundle\Controller\UserApprovalController as BaseController;

class UserApprovalController extends BaseController
{
    public function approveAction(Request $request, $id)
    {
        list($user, $userApprovalInfo) = $this->getApprovalInfo($request, $id);

        $area = $this->getAreaService()->getAreaByCode($user['areaCode']);

        if ($request->getMethod() == 'POST') {
            $data = $request->request->all();

            if ($data['form_status'] == 'success') {
                $this->getUserService()->passBatchApproval($id, $data['note']);
            } elseif ($data['form_status'] == 'fail') {
                /*
                if ($this->isPluginInstalled('TeacherAudit')) {
                    $approval = $this->getTeacherAuditService()->getApprovalByUserId($user['id']);

                    if (!empty($approval)) {
                        $this->getTeacherAuditService()->rejectApproval($user['id'], '教师资格申请因实名认证未通过而失败');
                    }
                }
                */
                $this->getUserService()->rejectBatchApproval($id, $data['note']);
            }

            return $this->createJsonResponse(array('status' => 'ok'));
        }

        return $this->render("CustomAdminBundle:User:user-approve-modal.html.twig",
            array(
                'user'             => $user,
                'userApprovalInfo' => $userApprovalInfo,
                'area'             => $area
            )
        );
    }

    public function showIdcardAction($userId, $type)
    {
        $user        = $this->getUserService()->getUser($userId);
        $currentUser = $this->getCurrentUser();

        if (empty($currentUser)) {
            throw $this->createAccessDeniedException();
        }

        $userApprovalInfo = $this->getUserService()->getLastestApprovalByUserIdAndStatus($user['id'], 'approving');

        if($type === 'face'){
            $idcardPath = $userApprovalInfo['faceImg'];
        }elseif($type === 'back'){
            $idcardPath = $userApprovalInfo['backImg'];
        }elseif($type === 'jobsSeniorityCard'){
            $idcardPath = $userApprovalInfo['jobsSeniorityCardImg'];
        }elseif($type === 'driverLicense'){
            $idcardPath = $userApprovalInfo['driverLicenseImg'];
        }elseif($type === 'other'){
            if($userApprovalInfo['otherImg']){
                $idcardPath = $userApprovalInfo['otherImg'];
            }else{
                exit;
            }
        }
        $imgConverToData = new ImgConverToData;
        $imgConverToData->getImgDir($idcardPath);
        $imgConverToData->img2Data();
        $imgData = $imgConverToData->data2Img();
        echo $imgData;
        exit;
    }

    public function viewApprovalInfoAction(Request $request, $id)
    {
        list($user, $userApprovalInfo) = $this->getApprovalInfo($request, $id);
        $area = $this->getAreaService()->getAreaByCode($user['areaCode']);

        return $this->render("CustomAdminBundle:User:user-approve-info-modal.html.twig",
            array(
                'user'             => $user,
                'userApprovalInfo' => $userApprovalInfo,
                'area'             => $area
            )
        );
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }
}
