<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class MobileAndroidController extends MobileBaseController
{
    protected function getConfig()
    {
        return array(
            'configName' => 'versionConfig.yml',
            'redirectRouterUrl' => 'admin_mobile_version_setting_android',
             'renderPage' => 'CustomAdminBundle:Mobile:android_index.html.twig'
        );
    }
}