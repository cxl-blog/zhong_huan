<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

class MobileIosController extends MobileBaseController
{
    protected function getConfig()
    {
        return array(
            'configName' => 'versionConfigIos.yml',
            'redirectRouterUrl' => 'admin_mobile_version_setting_ios',
            'renderPage' => 'CustomAdminBundle:Mobile:ios_index.html.twig'
        );
    }
}