<?php

namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Topxia\Common\ArrayToolkit;
use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ToLearnController extends BaseController
{
    public function indexAction(Request $request)
    {
        $fields     = $request->query->all();
        $conditions = array(
            'keywordType' => '',
            'keyword'     => ''
        );

        if (empty($fields)) {
            $fields = array();
        }

        $conditions = array_merge($conditions, $fields);

        if (!empty($fields)) {
            if (isset($conditions['keyWordType']) && isset($conditions['keyWord'])) {
                $conditions[$conditions['keyWordType']] = $conditions['keyWord'];
                unset($conditions['keyWordType']);
                unset($conditions['keyWord']);
            }
        }

        $conditions['toLearn'] = 1;

        $userCount = $this->getUserService()->searchUserCount($conditions);

        $paginator = new Paginator(
            $this->get('request'),
            $userCount,
            20
        );
        $users = $this->getUserService()->searchUsers(
            $conditions,
            array('user.id', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        $userIds      = ArrayToolkit::column($users, 'id');

        $userProfiles = $this->getUserService()->findUserProfilesByIds($userIds);
        foreach ($users as &$user) {
            $certificateCount         = $this->getCertificateService()->findCertificateCountByUserId($user['id']);
            $user['certificateCount'] = $certificateCount;
        }

        return $this->render('CustomAdminBundle:ToLearn:index.html.twig', array(
            'users'     => $users,
            'userCount' => $userCount,
            'paginator' => $paginator,
            'profiles'  => $userProfiles
        ));
    }

    public function toLearnShowAction(Request $request, $id)
    {
        $user                 = $this->getUserService()->getUser($id);
        $user['certificates'] = $this->getCertificateService()->findCertificatesByUserId($user['id']);
        $profile              = $this->getUserService()->getUserProfile($id);
        $fields               = $this->getFields();

        return $this->render('CustomAdminBundle:ToLearn:show-modal.html.twig', array(
            'user'    => $user,
            'profile' => $profile,
            'fields'  => $fields
        ));
    }

    protected function getFields()
    {
        $fields = $this->getUserFieldService()->getAllFieldsOrderBySeqAndEnabled();

        for ($i = 0; $i < count($fields); $i++) {
            if (strstr($fields[$i]['fieldName'], "textField")) {
                $fields[$i]['type'] = "text";
            }

            if (strstr($fields[$i]['fieldName'], "varcharField")) {
                $fields[$i]['type'] = "varchar";
            }

            if (strstr($fields[$i]['fieldName'], "intField")) {
                $fields[$i]['type'] = "int";
            }

            if (strstr($fields[$i]['fieldName'], "floatField")) {
                $fields[$i]['type'] = "float";
            }

            if (strstr($fields[$i]['fieldName'], "dateField")) {
                $fields[$i]['type'] = "date";
            }
        }

        return $fields;
    }

    protected function getUserFieldService()
    {
        return $this->getServiceKernel()->createService('User.UserFieldService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.ToLearnUserService');
    }
}
