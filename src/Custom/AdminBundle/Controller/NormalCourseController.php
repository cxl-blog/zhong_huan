<?php
namespace Custom\AdminBundle\Controller;

class NormalCourseController extends CourseController
{
    protected function getFilterType()
    {
        return 'normal';
    }

    protected function getRenderedIndexPage()
    {
        return 'CustomAdminBundle:Course:index.html.twig';
    }

    protected function setIndexResultParams(array $params,array $courses)
    {
        return $params;
    }
}
