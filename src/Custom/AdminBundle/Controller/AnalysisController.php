<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Custom\Common\Util\CourseLoopUtils;
use Topxia\Common\ArrayToolkit;
use Custom\Common\Constants\Action;
use Custom\Common\Constants\DbValue;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Topxia\AdminBundle\Controller\AnalysisController as BaseController;

class AnalysisController extends BaseController
{
    public function rountByanalysisDateTypeAction(Request $request, $tab)
    {
        $analysisDateType = $request->query->get("analysisDateType");
        return $this->forward('CustomAdminBundle:Analysis:'.$analysisDateType, array(
            'request' => $request,
            'tab'     => $tab
        ));
    }

    public function entiretyAction(Request $request)
    {
        $province = $request->query->get('province','');
        $codes = $this->getAreaService()->getAreaCodeByProvince($province);
        
        $onlineCount  = $this->getStatisticsService()->getOnlineCountNotInAdminAndTeacher(array('retentionTime' =>15 * 60, 'areaCode' =>$codes['code']));
        $studentCount = $this->getUserService()->findRegisterUserCount(array('toLearn' => DbValue::FALSE, 'areaCode' =>$codes['code']));

        $learningMembers = $this->getCourseService()->searchMember(array('classroomId' => 0, 'role' => 'student', 'isLearned' => DbValue::FALSE), 0, PHP_INT_MAX);
        $learningUserIds = array_unique(ArrayToolKit::column($learningMembers, 'userId'));
        $areaLearningUserIds=$this->getUserService()->getAreaLearnUserIds($learningUserIds,$codes['code']);
        $learningCount   = count($areaLearningUserIds);

        $learnedMembers = $this->getCourseService()->searchMember(array('classroomId' => 0, 'role' => 'student', 'isLearned' => DbValue::TRUE), 0, PHP_INT_MAX);
        $learnedUserIds = array_unique(ArrayToolKit::column($learnedMembers, 'userId'));
        $areaLearnedUserIds=$this->getUserService()->getAreaLearnUserIds($learnedUserIds,$codes['code']);
        $learnedCount = 0;
    
        foreach ($areaLearnedUserIds as $userId) {
            $completedCoursesNumber = $this->getCertificateService()->getUserIdCompletionCertificateCoursesNumber($userId);
            $learnedCount +=$completedCoursesNumber;
        }
 
        $areas     = $this->getAreaService()->getAreas();
        $areaCodes = array();

        foreach ($areas as $area) {
            $areaCodes[$area['code']] = $area['province'];
        }
        
        $data            = $this->buildMapData($request);
        $provinces = ArrayToolKit::column($areas, 'province');
        $mapType         = array();
        foreach ($provinces as $province) {
            $mapType[$province] = $province;
        }

        return $this->render('CustomAdminBundle:OperationAnalysis:entirety.html.twig',array(
                'data'          => $data,
                'mapType'       => $mapType, 
                'areaCodes'     => $areaCodes,
                'onlineCount'   => $onlineCount,
                'learnedCount'  => $learnedCount,
                'studentCount'  => $studentCount,
                'learningCount' => $learningCount,
                'areaCodes'           =>$areaCodes
            )
        );
    }
    public function courseSumAction(Request $request, $tab)
    {
        $data               = array();
        $courseSumStartDate = "";

        $condition = $request->query->all();
        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_course_sum', array(
                'tab' => "trend"
            )));
        }
        $timeRange['type'] = 'all';
        $timeRange['parentId'] = 0;
        $paginator             = new Paginator(
            $request,
            $this->getCourseService()->searchCourseCount($timeRange),
            20
        );

        $courseSumDetail = $this->getCourseService()->searchCourses(
            $timeRange,
            '',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $courseSumData = "";

        if ($tab == "trend") {
            $courseSumData = $this->getCourseService()->analysisCourseSumByTime($timeRange['endTime']);

            $data = $this->fillAnalysisCourseSum($condition, $courseSumData);
        }

        $userIds = ArrayToolkit::column($courseSumDetail, 'userId');

        $users = $this->getUserService()->findUsersByIds($userIds);

        $categories = $this->getCategoryService()->findCategoriesByIds(ArrayToolkit::column($courseSumDetail, 'categoryId'));

        $courseSumStartData = $this->getCourseService()->searchCourses(array(), 'createdTimeByAsc', 0, 1);

        if ($courseSumStartData) {
            $courseSumStartDate = date("Y-m-d", $courseSumStartData[0]['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);
        
        return $this->render("CustomAdminBundle:OperationAnalysis:course-sum.html.twig", array(
            'courseSumDetail'    => $courseSumDetail,
            'paginator'          => $paginator,
            'tab'                => $tab,
            'categories'         => $categories,
            'data'               => $data,
            'users'              => $users,
            'courseSumStartDate' => $courseSumStartDate,
            'dataInfo'           => $dataInfo
        ));
    }

    public function paidLessonAction(Request $request, $tab)
    {
        $data                = array();
        $paidLessonStartDate = "";

        $condition = $request->query->all();

        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_lesson_paid', array(
                'tab' => "trend"
            )));
        }

        $paginator = new Paginator(
            $request,
            $this->getOrderService()->searchOrderCount(array("paidStartTime" => $timeRange['startTime'], "paidEndTime" => $timeRange['endTime'], "status" => "paid", "amount" => "0.00", "targetType" => 'course')),
            20
        );

        $paidCourseDetail = $this->getOrderService()->searchOrders(
            array("paidStartTime" => $timeRange['startTime'], "paidEndTime" => $timeRange['endTime'], "status" => "paid", "amount" => "0.00", "targetType" => 'course'),
            "latest",
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $paidLessonData = "";

        if ($tab == "trend") {
            $paidLessonData = $this->getOrderService()->analysisPaidCourseOrderDataByTime($timeRange['startTime'], $timeRange['endTime']);

            $data = $this->fillAnalysisData($condition, $paidLessonData);
        }

        $courseIds = ArrayToolkit::column($paidCourseDetail, 'targetId'); //订单中的课程

        $courses = $this->getCourseService()->searchCourses( //订单中的课程zai剔除班级中的课程
            array('courseIds' => $courseIds, 'parentId' => '0'),
            "latest",
            0,
            count($paidCourseDetail)
        );
        $userIds = ArrayToolkit::column($paidCourseDetail, 'userId');
        $courses = ArrayToolkit::index($courses, 'id');

        $users = $this->getUserService()->findUsersByIds($userIds);

        $paidLessonStartData = $this->getOrderService()->searchOrders(array("status" => "paid", "amount" => "0.00"), "early", 0, 1);

        foreach ($paidLessonStartData as $key) {
            $paidLessonStartDate = date("Y-m-d", $key['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);

        return $this->render("CustomAdminBundle:OperationAnalysis:paid-lesson.html.twig", array(
            'paidCourseDetail'    => $paidCourseDetail,
            'paginator'           => $paginator,
            'tab'                 => $tab,
            'data'                => $data,
            'courses'             => $courses,
            'users'               => $users,
            'paidLessonStartDate' => $paidLessonStartDate,
            'dataInfo'            => $dataInfo
        ));
    }

    public function courseAction(Request $request, $tab)
    {
        $data            = array();
        $courseStartDate = "";

        $condition = $request->query->all();
        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_course', array(
                'tab' => "trend"
            )));
        }
        $timeRange['type'] = 'all';
        $paginator = new Paginator(
            $request,
            $this->getCourseService()->searchCourseCount($timeRange),
            20
        );

        $courseDetail = $this->getCourseService()->searchCourses(
            $timeRange,
            '',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $courseData = "";

        if ($tab == "trend") {
            $courseData = $this->getCourseService()->analysisCourseDataByTime($timeRange['startTime'], $timeRange['endTime']);

            $data = $this->fillAnalysisData($condition, $courseData);
        }

        $userIds = ArrayToolkit::column($courseDetail, 'userId');

        $users = $this->getUserService()->findUsersByIds($userIds);

        $categories = $this->getCategoryService()->findCategoriesByIds(ArrayToolkit::column($courseDetail, 'categoryId'));

        $courseStartData = $this->getCourseService()->searchCourses(array(), 'createdTimeByAsc', 0, 1);

        if ($courseStartData) {
            $courseStartDate = date("Y-m-d", $courseStartData[0]['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);
        return $this->render("CustomAdminBundle:OperationAnalysis:course.html.twig", array(
            'courseDetail'    => $courseDetail,
            'paginator'       => $paginator,
            'tab'             => $tab,
            'categories'      => $categories,
            'data'            => $data,
            'users'           => $users,
            'courseStartDate' => $courseStartDate,
            'dataInfo'        => $dataInfo
        ));
    }

    private function buildMapData($request)
    {
        $datas      = array();
        $series     = array();
        $province   = $request->query->get('province','');
        if(empty($province)){
            $title      = '全国';
            $mapType    = 'china';
            $areas      = $this->getAreaService()->searchAreas(
                array(),array('id','ASC'),0,PHP_INT_MAX
            );

            $analysises = $this->getUserService()->findAnalysisRegistedUserData(array());
        }else{
            $title      = $province;
            $mapType    = $province;
            $conditions = array('province'=>$province);
            $areas      = $this->getAreaService()->searchAreas(
                $conditions,array('id','ASC'),0,PHP_INT_MAX
            );
        }
        $codes      = ArrayToolkit::column($areas,'code');
        $maps       = ArrayToolkit::index($areas,'code');
        $analysises = $this->getUserService()->findAnalysisRegistedUserData($codes);
        $max        = $this->getUserService()->findAnalysisRegistedUserDataMaxCount($codes);

        $certificationIds = ArrayToolkit::column($analysises,'certificationId');
        $certifications   = $this->getCertificationService()->getCertifications($certificationIds);
        $categories       = ArrayToolkit::column($certifications,'category');
        foreach ($certifications as $key => $certification) {
            $series[$key] = array(
              'type'      => 'map',
              'mapType'   => $mapType,
              'name'      => $certification['category'],
              'itemStyle' => array(
                 'normal'  =>array('label'=>array('show'=>true)),
                 'emphasis'=>array('label'=>array('show'=>true)),
              )
            );
            $data = array();
            foreach ($analysises as $analysis) {
              if($certification['id']==$analysis['certificationId']){
                 $area   = $maps[$analysis['areaCode']];
                 $name   = empty($province)?$area['province']:$area['name'];
                 $data[] = array(
                    'name'  => $name,
                    'value' => $analysis['count']
                 );
              }
            }
            $series[$key]['data'] = $data;
        }
        $datas['max']      = $max;
        $datas['title']    = $title;
        $datas['series']   = $series;
        $datas['category'] = $categories;
        
        return json_encode($datas);
    }

    public function joinLessonAction(Request $request, $tab)
    {
        $data                = array();
        $joinLessonStartDate = "";

        $condition = $request->query->all();
        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_lesson_join', array(
                'tab' => "trend"
            )));
        }

        $paginator = new Paginator(
            $request,
            $this->getOrderService()->searchOrderCount(array("paidStartTime" => $timeRange['startTime'], "paidEndTime" => $timeRange['endTime'], "status" => "paid")),
            20
        );

        $joinLessonDetail = $this->getOrderService()->searchOrders(
            array("paidStartTime" => $timeRange['startTime'], "paidEndTime" => $timeRange['endTime'], "status" => "paid"),
            "latest",
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $joinLessonData = "";

        if ($tab == "trend") {
            $joinLessonData = $this->getOrderService()->analysisCourseOrderDataByTimeAndStatus($timeRange['startTime'], $timeRange['endTime'], "paid");

            $data = $this->fillAnalysisData($condition, $joinLessonData);
        }

        $courseIds = ArrayToolkit::column($joinLessonDetail, 'targetId');

        $courses = $this->getCourseService()->findCoursesByIds($courseIds);

        $userIds = ArrayToolkit::column($joinLessonDetail, 'userId');

        $users = $this->getUserService()->findUsersByIds($userIds);

        $joinLessonStartData = $this->getOrderService()->searchOrders(array("status" => "paid"), "early", 0, 1);

        foreach ($joinLessonStartData as $key) {
            $joinLessonStartDate = date("Y-m-d", $key['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);
        return $this->render("CustomAdminBundle:OperationAnalysis:join-lesson.html.twig", array(
            'JoinLessonDetail'    => $joinLessonDetail,
            'paginator'           => $paginator,
            'tab'                 => $tab,
            'data'                => $data,
            'courses'             => $courses,
            'users'               => $users,
            'joinLessonStartDate' => $joinLessonStartDate,
            'dataInfo'            => $dataInfo
        ));
    }

    public function detailAction(Request $request, $filter)
    {
        $data       = array();
        $graduationData = array();
        $conditions = $request->query->all();
        $timeRange  = $this->getTimeRange($conditions);
        $areas     = $this->getAreaService()->getAreas();
        $areaCodes = array();
        
        foreach ($areas as $area) {
            $areaCodes[$area['code']] = $area['province'];
        }

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_detail', array('filter' => $filter)
            ));
        }

        $dataInfo = $this->getCustomDataInfo($conditions, $timeRange);
        $code     = isset($conditions['areaCode']) ? $conditions['areaCode'] : "";

        list($certifications, $areas) = $this->buildDatas();

        $registerCount = $this->getUserService()->findRegisterUserCount(array('toLearn' => DbValue::FALSE, 'areaCode' => $code,'startDateTime' => $timeRange['startTime'],
                      'endDateTime'   => $timeRange['endTime']));

        $buyCount = $this->getStatesService()->searchStatesCount(array('action' => Action::BUYED, 'areaCode' => $code,'startDateTime' => $timeRange['startTime'],
                     'endDateTime'   => $timeRange['endTime']), true);

        $graduationCount = $this->getStatesService()->searchGraduationCount(array('action' => Action::GRADUATED, 'areaCode' => $code,
                     'startDateTime' => $timeRange['startTime'],
                     'endDateTime'   => $timeRange['endTime']));
      
        if ($filter == 'register') {
            $analysis = array(
                'toLearn'       => DbValue::FALSE,
                'areaCode'      => $code,
                'startDateTime' => $timeRange['startTime'],
                'endDateTime'   => $timeRange['endTime']
            );
            
            $registers = $this->getUserService()->findAnalysisRegisterData($analysis);
            $data      = $this->fillAnalysisData($conditions, $registers);
        } elseif ($filter == 'buy') {
            $analysis = array(
                'action'        => Action::BUYED,
                'areaCode'      => $code,
                'startDateTime' => $timeRange['startTime'],
                'endDateTime'   => $timeRange['endTime']
            );
            $buys = $this->getStatesService()->findAnalysisBuyData($analysis);
            $data = $this->fillAnalysisDatas($conditions, $buys);
        } else {
            $analysis = array(
                'action'        => Action::GRADUATED,
                'areaCode'      => $code,
                'startDateTime' => $timeRange['startTime'],
                'endDateTime'   => $timeRange['endTime']
            );
            $graduations = $this->getStatesService()->findAnalysisGraduationData($analysis);
            $graduationsDatas = array();
            foreach ($graduations as $key => $graduation) {
                $graduation['count'] = '1';
                $graduationsDatas[] = $graduation;
            }
            $data        = $this->fillAnalysisDatas($conditions, $graduationsDatas);

            $graduationsInfo=$this->getStatesService()->findDetailAnalysisGraduationData($analysis);
            
            $graduationInfos = array();
           foreach ($graduationsInfo as $key => $graduationInfo) {
            $courseMember = $this->getCourseService()->getCourseMember($graduationInfo['courseId'],$graduationInfo['userId'],$graduationInfo['currentLoopTime']);
            $graduationInfo['userStartTime'] = $courseMember['createdTime'];
            $graduationInfos[] = $graduationInfo;
        }
            $graduationData = $this->getUserService()->fillAnalysisGraduationDatas($graduationInfos);

        }

        $registerStartData = $this->getUserService()->findStartRegisterUser();

        if ($registerStartData) {
            $registerStartDate = date("Y-m-d", $registerStartData['createdTime']);
        }

        return $this->render("CustomAdminBundle:OperationAnalysis:{$filter}-detail.html.twig", array(
            'data'              => $data,
            'areas'             => $areas,
            'filter'            => $filter,
            'dataInfo'          => $dataInfo,
            'buyCount'          => $buyCount,
            'registerCount'     => $registerCount,
            'certifications'    => $certifications,
            'graduationCount'   => $graduationCount,
            "registerStartDate" => $registerStartDate,
            'areaCodes'           =>$areaCodes,
            'graduationData'   => $graduationData
        )
        );
    }

    public function registerAction(Request $request, $tab)
    {
        $data              = array();
        $registerStartDate = "";

        $condition = $request->query->all();
        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_register', array(
                'tab' => "trend"
            )));
        }

        $paginator = new Paginator(
            $request,
            $this->getUserService()->searchUserCount($timeRange),
            20
        );

        $registerDetail = $this->getUserService()->searchUsers(
            $this->appendUserRegistedConditions($timeRange),
            array('createdTime', 'DESC'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $registerData = "";

        if ($tab == "trend") {
            $registerData = $this->getUserService()->analysisRegisterDataByTime($timeRange['startTime'], $timeRange['endTime']);
            $data         = $this->fillAnalysisData($condition, $registerData);
        }

        $registerStartData = $this->getUserService()->searchUsers(array(), array('createdTime', 'ASC'), 0, 1);

        if ($registerStartData) {
            $registerStartDate = date("Y-m-d", $registerStartData[0]['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);

        $registerIds      = ArrayToolkit::column($registerDetail, 'id');
        $registerProfiles = $this->getUserService()->findUserProfilesByIds($registerIds);

        return $this->render('CustomAdminBundle:OperationAnalysis:register.html.twig', array(
            'registerDetail'    => $registerDetail,
            'paginator'         => $paginator,
            'tab'               => $tab,
            'registerProfiles'  => $registerProfiles,
            'data'              => $data,
            "registerStartDate" => $registerStartDate,
            "dataInfo"          => $dataInfo
        ));
    }

    public function userSumAction(Request $request, $tab)
    {
        $data             = array();
        $userSumStartDate = "";
        $userSumDetail    = array();

        $condition = $request->query->all();
        $timeRange = $this->getTimeRange($condition);

        if (!$timeRange) {
            $this->setFlashMessage("danger", "输入的日期有误!");
            return $this->redirect($this->generateUrl('admin_operation_analysis_user_sum', array(
                'tab' => "trend"
            )));
        }

        $result = array(
            'tab' => $tab
        );

        if ($tab == "trend") {
            $userSumData    = $this->getUserService()->analysisUserSumByTime($timeRange['endTime']);
            $data           = $this->fillAnalysisUserSum($condition, $userSumData);
            $result["data"] = $data;
        } else {
            $paginator = new Paginator(
                $request,
                $this->getUserService()->searchUserCount($timeRange),
                20
            );

            $userSumDetail = $this->getUserService()->searchUsers(
                $this->appendUserRegistedConditions($timeRange),
                array('createdTime', 'DESC'),
                $paginator->getOffsetCount(),
                $paginator->getPerPageCount()
            );
            $result['userSumDetail'] = $userSumDetail;
            $result['paginator']     = $paginator;
        }

        $userSumStartData = $this->getUserService()->searchUsers(array(), array('createdTime', 'ASC'), 0, 1);

        if ($userSumStartData) {
            $userSumStartDate = date("Y-m-d", $userSumStartData[0]['createdTime']);
        }

        $dataInfo = $this->getDataInfo($condition, $timeRange);

        $result['userSumStartDate'] = $userSumStartDate;
        $result['dataInfo']         = $dataInfo;

        $userSumIds                = ArrayToolkit::column($userSumDetail, 'id');
        $userSumProfiles           = $this->getUserService()->findUserProfilesByIds($userSumIds);
        $result['userSumProfiles'] = $userSumProfiles;
        return $this->render('CustomAdminBundle:OperationAnalysis:user-sum.html.twig', $result);
    }

    protected function fillAnalysisDatas($conditions,$analysisDatas)
    {
        $data         = array();
        $dates        = $this->getDatesByCondition($conditions);
        $data['date'] = $dates;
        $series       = array();
        
        foreach ($dates as $date) {
            foreach ($analysisDatas as $analysis) {
                if ($date == $analysis['date']) {
                    $series[$analysis['name']][$date] = intval($analysis['count']);
                } elseif (!isset($series[$analysis['name']][$date])) {
                    $series[$analysis['name']][$date] = 0;
                }
            }
        }
        if (!empty($series)) {
            foreach ($series as $key => $serie) {
                $data['series'][] = array(
                    'name'  => $key,
                    'data'  => array_values($serie)
                );
            }
        } else {
            $data['series'] = array(
                'name'  => '',
                'data'  => []
            );
        }

        return json_encode($data);
    }

    protected function getCustomDataInfo($condition, $timeRange)
    {
        return array(
            'startTime'            => date("Y-m-d", $timeRange['startTime']),
            'endTime'              => date("Y-m-d", $timeRange['endTime'] - 24 * 3600),
            'currentMonthStart'    => date("Y-m-d", strtotime(date("Y-m", time()))),
            'currentMonthEnd'      => date("Y-m-d", strtotime(date("Y-m-d", time()))),
            'lastMonthStart'       => date("Y-m-d", strtotime(date("Y-m", strtotime("-1 month")))),
            'lastMonthEnd'         => date("Y-m-d", strtotime(date("Y-m", time())) - 24 * 3600),
            'lastThreeMonthsStart' => date("Y-m-d", strtotime(date("Y-m", strtotime("-2 month")))),
            'lastThreeMonthsEnd'   => date("Y-m-d", strtotime(date("Y-m-d", time())))
        );
    }

    private function buildDatas()
    {
        $areas     = $this->getAreaService()->getAreas();
        $areaCodes = array();

        foreach ($areas as $area) {
            $areaCodes[$area['code']] = $area['name'];
        }

        $certifications = $this->getCertificationService()->getAllCertifications();

        return array($certifications, $areaCodes);
    }

    public function facePictureAction(Request $request)
    {
        $query = $request->query->all();

        $conditions = array(
            'analysisDateType' => '',
            'startTime'        => '',
            'endTime'          => '',
            'keywordType'      => '',
            'keyword'          => ''
        );

        if (empty($query)) {
            $query = array();
        }

        $conditions = array_merge($conditions, $query);
        $timeRange  = $this->getTimeRange($conditions);
        $dataInfo   = $this->getDataInfo($conditions, $timeRange);

        $userIds = array();

        if (isset($conditions['keywordType']) && !empty($conditions['keyword'])) {
            $userCount = $this->getUserService()->searchUserCount(
                array('keywordType' => $conditions['keywordType'], 'keyword' => $conditions['keyword']));
            $users   = $this->getUserService()->searchUsers(array('keywordType' => $conditions['keywordType'], 'keyword' => $conditions['keyword']), array('id', 'desc'), 0, $userCount);
            $userIds = $users ? ArrayToolkit::column($users, 'id') : array(0);
        }

        $facePictureCount = $this->getFaceDetectResultService()->searchFaceDetectResultsCount(
            array(
                'type'          => $conditions['analysisDateType'],
                'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']),
                'endDateTime'   => date("Y-m-d H:i:s", $timeRange['endTime']),
                'userIds'       => $userIds
            )
        );
        $paginator    = new Paginator($request, $facePictureCount, 21);
        $facePictures = $this->getFaceDetectResultService()->searchFaceDetectResults(
            array(
                'type'          => $conditions['analysisDateType'],
                'startDateTime' => date("Y-m-d H:i:s", $timeRange['startTime']),
                'endDateTime'   => date("Y-m-d H:i:s", $timeRange['endTime']),
                'userIds'       => $userIds
            ),
            'created',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        $faceUserIds = ArrayToolkit::column($facePictures, 'userId');
        $faceUsers   = $this->getUserService()->searchUsers(array('userIds' => $faceUserIds), array('id', 'desc'), 0, count($faceUserIds));
        $faceUsers   = ArrayToolkit::index($faceUsers, 'id');

        return $this->render('CustomAdminBundle:OperationAnalysis:facePicture.html.twig', array(
            'dataInfo'     => $dataInfo,
            'keywordType'  => $conditions['keywordType'],
            'keyword'      => $conditions['keyword'],
            'facePictures' => $facePictures,
            'paginator'    => $paginator,
            'faceUsers'    => $faceUsers
        )

        );
    }

    protected function getStatesService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getCertificationService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getStatisticsService()
    {
        return $this->getServiceKernel()->createService('Custom:System.StatisticsService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    private function appendUserRegistedConditions($conditions)
    {
        $result = $conditions;
        $result['toLearn'] = DbValue::FALSE;
        return $result;
    }
}
