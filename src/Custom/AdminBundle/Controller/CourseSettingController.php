<?php
namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\CourseSettingController as BaseController;
use Topxia\Common\ArrayToolkit;

class CourseSettingController extends BaseController
{
    public function courseSettingAction(Request $request)
    {
        $courseSetting        = $this->getSettingService()->get('course', array());
        $liveCourseSetting    = $this->getSettingService()->get('live-course', array());
        $defaultSettings      = $this->getSettingService()->get('default', array());
        $userDefaultSetting   = $this->getSettingService()->get('user_default', array());
        $courseDefaultSetting = $this->getSettingService()->get('course_default', array());
        $path                 = $this->container->getParameter('kernel.root_dir').'/../web/assets/img/default/';
        $courseDefaultSet     = $this->getCourseDefaultSet();
        $defaultSetting       = array_merge($courseDefaultSet, $courseDefaultSetting);

        $default = array(
            'is_show_classInteraction' => '0',
            'welcome_message_enabled'  => '0',
            'welcome_message_body'     => '{{nickname}},欢迎加入课程{{course}}',
            'teacher_modify_price'     => '1',
            'teacher_search_order'     => '0',
            'teacher_manage_student'   => '0',
            'teacher_export_student'   => '0',
            'student_download_media'   => '0',
            'explore_default_orderBy'  => 'latest',
            'free_course_nologin_view' => '1',
            'relatedCourses'           => '0',
            'coursesPrice'             => '0',
            'allowAnonymousPreview'    => '1',
            "copy_enabled"             => '0',
            "testpaperCopy_enabled"    => '0',
            "custom_chapter_enabled"   => '1'
        );

        $this->getSettingService()->set('course', $courseSetting);
        $this->getSettingService()->set('live-course', $liveCourseSetting);
        $courseSetting = array_merge($default, $courseSetting);

        if ($request->getMethod() == 'POST') {
            $defaultSetting = $request->request->all();

            if (isset($defaultSetting['chapter_name'])) {
                $defaultSetting['chapter_name'] = $defaultSetting['chapter_name'];
            } else {
                $defaultSetting['chapter_name'] = '章';
            }

            if (isset($defaultSetting['part_name'])) {
                $defaultSetting['part_name'] = $defaultSetting['part_name'];
            } else {
                $defaultSetting['part_name'] = '节';
            }

            $courseDefaultSetting = ArrayToolkit::parts($defaultSetting, array(
                'chapter_name',
                'part_name'
            ));
            $this->getSettingService()->set('course_default', $courseDefaultSetting);

            $default        = $this->getSettingService()->get('default', array());
            $defaultSetting = array_merge($default, $userDefaultSetting, $courseDefaultSetting);
            $this->getSettingService()->set('default', $defaultSetting);

            $courseUpdateSetting = $request->request->all();

            $courseSetting = array_merge($courseSetting, $courseUpdateSetting, $liveCourseSetting);

            $this->getSettingService()->set('live-course', $liveCourseSetting);
            $this->getSettingService()->set('course', $courseSetting);
            $this->getLogService()->info('system', 'update_settings', "更新课程设置", $courseSetting);
            $this->setFlashMessage('success', '课程设置已保存！');
        }

        return $this->render('CustomAdminBundle:System:course-setting.html.twig', array(
            'courseSetting'   => $courseSetting,
            'defaultSetting'  => $defaultSetting,
            'hasOwnCopyright' => false
        ));
    }
}