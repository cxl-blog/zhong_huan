<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\Common\ArrayToolkit;
use Topxia\Common\ExtensionManager;
use ZipArchive;
use Custom\Common\Util\PHPExcelToolkit;
use Topxia\AdminBundle\Controller\BaseController as BaseController;

class UserStatesController extends BaseController
{
    public function indexAction(Request $request)
    {
        $query = $request->query->all();

        $areas       = $this->getAreaService()->getAreas();
        $areasOption = $this->getAreasOption($areas);

        $certifications       = $this->getCertificateService()->getAllCertifications();
        $certificationsOption = $this->getCertificationsOption($certifications);

        $conditions = array(
            'areaCode'        => '',
            'certificationId' => '',
            'action'          => 2,
            'startDateTime'   => '',
            'endDateTime'     => '',
            'keywordType'     => '',
            'keyword'         => ''
        );
        $conditions = array_merge($conditions, $query);

        $userCount = $this->getUserCertificationStatesService()->searchStatesCount($conditions);

        $paginator = new Paginator(
            $request,
            $userCount,
            20
        );

        $users = $this->getUserCertificationStatesService()->searchStates(
            $conditions,
            array('id', 'desc'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        return $this->render('CustomAdminBundle:UserStates:index.html.twig',
            array(
                'users'                => $users,
                'userCount'            => $userCount,
                'paginator'            => $paginator,
                'areasOption'          => $areasOption,
                'certificationsOption' => $certificationsOption

            )
        );
    }

    public function exportCsvAction(Request $request)
    {
        $user = $this->getCurrentUser();
        $conditions = $request->query->all();
        if(!isset($conditions['action'])){
            $conditions['action'] = 2;
        }
        if(!empty($conditions['startTime']) && !empty($conditions['endDate']))
        {
            $conditions['startTime'] = strtotime($conditions['startTime']);
            $conditions['endDate']   = strtotime($conditions['endDate']);
        }
        $pages = $this->getUserCertificationStatesService()->getExportPageInfos($conditions);
        if(!isset($pages['limit']))
        {
            return $this->redirect($this->generateUrl('admin_user_states'));
        }
        $pagesLimit = $pages['limit'];
        $userStates = array();
        foreach ($pagesLimit as $key => $pageLimit) 
        {
            $states = $this->getUserCertificationStatesService()->searchStates(
                $conditions,
                array('id', 'desc'),
                $pageLimit['start'],
                $pageLimit['limit']
                );
            $userStates[$pageLimit['page']] = $states;
        }

        $areas = array();
        $areas = $this->getUserStateAreaField($userStates);

        $certifications = array();
        $certifications = $this->getUserStateCertificationField($userStates);

        $action = ExtensionManager::instance()->getDataDict('userCertificationStates');
        $results = array();
        $results = $this->getExportInfos($userStates,$areas,$certifications,$action);
        $filenames = array();

        foreach ($results as $key=>$result) { 
            $filename = $this->createCsvFile($result,$user,$key);
            $filenames[] = $filename;
        }

        $zipname = sprintf("userstateInfo-(%s).zip",date('Y-n-d'));
        $zippath = sprintf($_SERVER['DOCUMENT_ROOT'].'/'.$zipname);

        $zip = new ZipArchive();
        $zip->open($zippath,ZipArchive::CREATE);
        for ($index=0; $index < count($filenames); $index++) { 
            $zip->addFile($filenames[$index],basename($filenames[$index]));
        }
        $zip->close();

        header('Content-type: application/zip;charset=UTF-8');
        header('Content-disposition: attachment; filename="'.$zipname.'"');
        header('Content-Transfer-Encoding: binary');
        readfile($zippath);
        $this->deleteTemporaryFiles($zippath,$filenames);
        exit();
    }

    public function deleteTemporaryFiles($zippath,$filename)
    {
        unlink($zippath);
        for ($index=0; $index < count($filename) ; $index++) { 
            unlink($filename[$index]);
        }
    }
    
    public function createCsvFile($results,$user,$i)
    {
        $filename = sprintf($_SERVER['DOCUMENT_ROOT']."/userstatepage-(%s)-%d.xls",date('Y-n-d'),$i);
        if(file_exists($filename))
        {
            unlink($filename);
        }
        
        $header = array(
            'areaName'=>'行政区域',
            'idcard'=>'学员身份证',
            'nickName'=>'学员姓名',
            'category'=>'证书类型',
            'courseTitle'=>'课程名称',
            'action'=>'学员动作',
            'learnTime'=>'学习周期'
        ); 

        $objWriter =  PHPExcelToolkit::export($results,$header);
        $objWriter->save(iconv("utf-8", "gb2312", $filename)); 
        return $filename;        
    }

    public function getExportInfos($userStates,$areas,$certifications,$action)
    {
        $results = array();
        $field = array();
        foreach ($userStates as $key => $userState) 
        {
           
            foreach ($userState as $userState) 
            {
                
                $field['areaName'] = $areas[$userState['areaCode']]['name'];
                $field['idcard'] = $userState['idcard'];
                $field['nickName'] = $userState['nickname'];
                $field['category']= $certifications[$userState['certificationId']]['category'];
                $field['courseTitle'] = $userState['courseTitle'];
                $field['action'] = $action[$userState['action']];
                $field['learnTime'] = date('Y-n-d H:i:s',$userState['learnStartTime'])."-".date('Y-n-d H:i:s',$userState['learnEndTime']);
                $results[$key][] = $field;     
                unset($field);           
            }
            
        }       

        return $results;
    }

    public function getUserStateCertificationField($userStates)
    {
        foreach ($userStates as $key => $userState)
        {
            foreach ($userState as $key => $users) 
            {
                $certificationIds[] = $users['certificationId'];
            }
        }

        $certificationIds = $this->getCertificateService()->getCertifications($certificationIds);
        $certificationIds = ArrayToolkit::index($certificationIds,'id');
        return $certificationIds;
    }

    public function getUserStateAreaField($userStates)
    {
        foreach ($userStates as $key => $userState)
        {
            foreach ($userState as $key => $users) 
            {
                $areaCodes[] = $users['areaCode'];
            }
        }

        $areaCodes = $this->getAreaService()->getAreaByCodes($areaCodes);
        $areaCodes = ArrayToolkit::index($areaCodes,'code');
        return $areaCodes;
    }

    public function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    public function getCertificationsOption($certifications)
    {
        $certificationsOption = array();

        foreach ($certifications as $certification) {
            $certificationsOption[$certification['id']] = $certification['category'];
        }

        return $certificationsOption;
    }

    public function getAreasOption($areas)
    {
        $areasOption = array();

        foreach ($areas as $area) {
            $areasOption[$area['code']] = $area['name'];
        }

        return $areasOption;
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getUserCertificationStatesService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserCertificationStatesService');
    }

    protected function getAreaService()
    {
        return $this->getServiceKernel()->createService('Custom:Area.AreaService');
    }

}
