<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Topxia\AdminBundle\Controller\FinanceSettingController as BaseFinanceSettingController;

class FinanceSettingController extends BaseFinanceSettingController
{
    public function paymentAction(Request $request)
    {
        $payment = $this->getSettingService()->get('payment', array());
        $default = array(
            'enabled'          => 0,
            'disabled_message' => '尚未开启支付模块，无法购买课程。',
            'bank_gateway'     => 'none',
            'alipay_enabled'   => 0,
            'alipay_key'       => '',
            'alipay_secret'    => '',
            'alipay_account'   => '',
            'alipay_type'      => 'direct',
            'tenpay_enabled'   => 0,
            'tenpay_key'       => '',
            'tenpay_secret'    => '',
            'wxpay_enabled'    => 0,
            'wxpay_key'        => '',
            'wxpay_secret'     => '',
            'wxpay_account'    => '',
            'wxpay_mobile_enabled'    => 0,
            'wxpay_mobile_key'        => '',
            'wxpay_mobile_secret'     => '',
            'wxpay_mobile_account'    => '',
            'wxpay_public_enabled'    => 0,
            'wxpay_public_key'     => '',
            'wxpay_public_key_secret'        => '',
            'wxpay_public_account'    => '',
            'wxpay_public_secret'     => '',
            'heepay_enabled'   => 0,
            'heepay_key'       => '',
            'heepay_secret'    => '',
            'quickpay_enabled' => 0,
            'quickpay_key'     => '',
            'quickpay_secret'  => '',
            'quickpay_aes'     => ''
        );

        $payment = array_merge($default, $payment);

        if ($request->getMethod() == 'POST') {
            $payment                    = $request->request->all();
            $payment['alipay_key']      = trim($payment['alipay_key']);
            $payment['alipay_secret']   = trim($payment['alipay_secret']);
            $payment['wxpay_key']       = trim($payment['wxpay_key']);
            $payment['wxpay_secret']    = trim($payment['wxpay_secret']);
            $payment['wxpay_account']    = trim($payment['wxpay_account']);
            $payment['wxpay_mobile_key']       = trim($payment['wxpay_mobile_key']);
            $payment['wxpay_mobile_secret']    = trim($payment['wxpay_mobile_secret']);
            $payment['wxpay_mobile_account']    = trim($payment['wxpay_mobile_account']);
            $payment['wxpay_public_key']       = trim($payment['wxpay_public_key']);
            $payment['wxpay_public_key_secret']       = trim($payment['wxpay_public_key_secret']);
            $payment['wxpay_public_account']       = trim($payment['wxpay_public_account']);
            $payment['wxpay_public_secret']       = trim($payment['wxpay_public_secret']);
            $payment['heepay_key']      = trim($payment['heepay_key']);
            $payment['heepay_secret']   = trim($payment['heepay_secret']);
            $payment['quickpay_key']    = trim($payment['quickpay_key']);
            $payment['quickpay_secret'] = trim($payment['quickpay_secret']);
            $payment['quickpay_aes']    = trim($payment['quickpay_aes']);
            $this->getSettingService()->set('payment', $payment);
            $this->getLogService()->info('system', 'update_settings', "更支付方式设置", $payment);
            $this->setFlashMessage('success', '支付方式设置已保存！');
        }

        return $this->render('CustomAdminBundle:System:payment.html.twig', array(
            'payment' => $payment
        ));
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Course.CourseService');
    }

    protected function getUploadFileService()
    {
        return $this->getServiceKernel()->createService('File.UploadFileService');
    }

    protected function getAppService()
    {
        return $this->getServiceKernel()->createService('CloudPlatform.AppService');
    }

    protected function getSettingService()
    {
        return $this->getServiceKernel()->createService('System.SettingService');
    }

    protected function getUserFieldService()
    {
        return $this->getServiceKernel()->createService('User.UserFieldService');
    }

    protected function getAuthService()
    {
        return $this->getServiceKernel()->createService('User.AuthService');
    }
}
