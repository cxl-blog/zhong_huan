<?php

namespace Custom\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Topxia\AdminBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends BaseController
{
    public function indexAction($name)
    {
        return $this->render('CustomAdminBundle:Default:index.html.twig', array('name' => $name));
    }

    public function onlineCountAction(Request $request)
    {
        $onlineCount = $this->getStatisticsService()->getOnlineCount(15 * 60);
        return $this->createJsonResponse(array('onlineCount' => $onlineCount, 'message' => 'ok'));
    }

    public function loginCountAction(Request $request)
    {
        $loginCount = $this->getStatisticsService()->getloginCount(15 * 60);
        return $this->createJsonResponse(array('loginCount' => $loginCount, 'message' => 'ok'));
    }

    protected function getStatisticsService()
    {
        return $this->getServiceKernel()->createService('Custom:System.StatisticsService');
    }
}
