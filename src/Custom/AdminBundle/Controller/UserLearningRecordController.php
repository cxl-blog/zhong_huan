<?php
namespace Custom\AdminBundle\Controller;

use Topxia\Common\Paginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Topxia\AdminBundle\Controller\BaseController as BaseController;

class UserLearningRecordController extends BaseController
{
    public function learningRecordAction(Request $request)
    {
        $fields = $request->query->all();
        $stateId = $fields['id'];
        $state = $this->getUserCertificationStatesService()->getUserCertificationStates($stateId);
        $userId = $state['userId'];
        $courseId = $state['courseId'];
        $currentLoopTime = $state['currentLoopTime'];
        $items        = $this->getCourseService()->getCourseItems($courseId);
        $sumHours     = $this->getCourseService()->getSumHours($items);
        $user = $this->getUserService()->getUser($userId);

        $learns = $this->getCourseService()->findUserLearnedLessons($userId, $courseId, $currentLoopTime);
        $certification = $this->getCertificateService()->getCertification($state['certificationId']);
        $lessonArray = $this->getCourseService()->getLearnLessons($learns, $userId, $currentLoopTime);

        return $this->render('CustomAdminBundle:LerningRocord:index.html.twig',
            array(
                'lessons'           => $lessonArray,
                'userName'          => $state['truename'],
                'certification'     => $certification,
                'userId'            => $userId,
                "courseId"          => $courseId,
                "sumHours"          => $sumHours,
                'currentLoopTime' => $currentLoopTime,
                'state' =>$state
            )
        );
    }

    public function learningDetailAction(Request $request)
    {
        $conditions = $request->query->all();
        $user       = $this->getUserService()->getUser($conditions['userId']);
        $userProfile = $this->getUserService()->getUserProfile($user['id']);
        $lesson     = $this->getCourseService()->getLesson($conditions['lessonId']);

        $chapter                       = $this->getCourseService()->getChapter($conditions['courseId'], $lesson['chapterId']);
        $parentChapter                 = $this->getCourseService()->getChapter($lesson['courseId'], $chapter['parentId']);
        $lesson['parentChapterNumber'] = !empty($parentChapter) ? $parentChapter['number'] : "";
        $lesson['chapterNumber']       = $chapter['number'];
        $lesson['chapterType']         = $chapter['type'];
        $lesson['userName']            = $user['nickname'];
        $lesson['lessonId']            = $conditions['lessonId'];
        $lesson['userId']              = $conditions['userId'];
        $conditions                    = array(
            "userId"          => $conditions['userId'],
            "courseId"        => $conditions['courseId'],
            "lessonId"        => $conditions['lessonId'],
            "currentLoopTime" => $conditions['currentLoopTime'],
            'ignoredWatchTime' => 0
        );

        $paginator = new Paginator(
            $this->get('request'),
            $this->getCourseService()->searchLessonViewCount($conditions),
            10
        );

        $lessonViews = $this->getCourseService()->searchLessonView(
            $conditions,
            array('id', 'desc'),
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );
        return $this->render('CustomAdminBundle:LerningRocord:learning-detail.html.twig',
            array(
                'lessonViews' => $lessonViews,
                'paginator'   => $paginator,
                'lesson'      => $lesson,
                'username'  => $userProfile['truename']
            )
        );
    }

    public function viewFacePictureAction(Request $request)
    {
        $fields     = $request->query->all();
        $time       = $fields['time'];
        $conditions = array(
            'type'         => 'lesson',
            'userId'       => $fields['userId'],
            'courseId'     => $fields['courseId'],
            'lessonId'     => $fields['lessonId'],
            'lessonViewId' => $fields['viewId']
        );
        $registerConditions = array(
            'type'   => 'register',
            'userId' => $fields['userId']
        );

        $facePictureCount         = $this->getFaceDetectResultService()->searchFaceDetectResultsCount($conditions);
        $faceRegisterPictureCount = $this->getFaceDetectResultService()->searchFaceDetectResultsCount($registerConditions);

        $paginator    = new Paginator($request, $facePictureCount, 11);
        $facePictures = $this->getFaceDetectResultService()->searchFaceDetectResults(
            $conditions,
            'created',
            $paginator->getOffsetCount(),
            $paginator->getPerPageCount()
        );

        $faceRegisterPictures = $this->getFaceDetectResultService()->searchFaceDetectResults(
            $registerConditions,
            'created',
            0,
            $faceRegisterPictureCount
        );

        $RegisterPictures = array();

        foreach ($faceRegisterPictures as $faceRegisterPicture) {
            if ($faceRegisterPicture['result'] == 'success') {
                array_push($RegisterPictures, $faceRegisterPicture);
                break;
            }
        }

        $facePictures = array_merge($facePictures, $RegisterPictures);
        return $this->render('CustomAdminBundle:LerningRocord:face-modal.html.twig',
            array(
                'facePictures' => $facePictures,
                'time'         => $time,
                'paginator'    => $paginator
            ));
    }

    public function exportLessonDataAction(Request $request)
    {
        $fields = $request->query->all();
        $stateId = $fields['id'];
        $state = $this->getUserCertificationStatesService()->getUserCertificationStates($stateId);     
        $courseId = $state['courseId'];
        $currentLoopTime = $state['currentLoopTime'];
        $userId = $state['userId'];
        $items        = $this->getCourseService()->getCourseItems($courseId);
        $sumHours     = $this->getCourseService()->getSumHours($items);
        $user = $this->getUserService()->getUser($userId);
        $learns = $this->getCourseService()->findUserLearnedLessons($userId, $courseId, $currentLoopTime);
        $lessonArray = $this->getCourseService()->getLearnLessons($learns, $userId, $currentLoopTime);
        
        $results = array();
        $params = array();
        foreach ($lessonArray as $key => $lesson)
        {
            $chapterTitle = '';
            if ($lesson['parentChapterNumber']) {
                $chapterTitle .= "第".$lesson['parentChapterNumber'].'章 ';
            }

            if ($lesson['chapterNumber']) {
                $chapterTitle .= $lesson['chapterType'] == 'chapter' ?  "第".$lesson['chapterNumber']."章 " : "第".$lesson['chapterNumber']."节 ";
            }

            $params[]= $chapterTitle . $lesson['title'];
            $params[]= date('Y-m-d H:i:s', $lesson['startTime']);
            $params[]= ($lesson['finishedTime'] == 0 ? 0 : date('Y-m-d H:i:s', $lesson['finishedTime']));
            $params[]= $lesson['status'] == "finished" ? $lesson['suggestHours']."分钟" : "-";
            $params[]= $lesson['status'] == "finished" ? round(($lesson['learnSum'] / $sumHours) * 100, 2)."%" : round((($lesson['learnSum'] - $lesson['suggestHours']) / $sumHours) * 100, 2)."%";            
            $results[] = $params;
            unset($params);
        }

        $header = array('所学课时','学习开始时间','学习结束时间','所获学时','学习进度');
        $filename = sprintf("用户%s《%s》的学习记录%s.xls", $state['nickname'], $state['courseTitle'], date('Y-n-d'));
        \Custom\Common\Util\PHPExcelToolkit::outPutExcel($filename,$results,$header);
        exit();
    }

    public function exportLessonDetailDataAction(Request $request)
    {
        $conditions                    = $request->query->all();
        $userProfile             = $this->getUserService()->getUserProfile($conditions['userId']);
        $lesson                        = $this->getCourseService()->getLesson($conditions['lessonId']);
        $chapter                       = $this->getCourseService()->getChapter($conditions['courseId'], $lesson['chapterId']);
        $parentChapter                 = $this->getCourseService()->getChapter($lesson['courseId'], $chapter['parentId']);
        $lesson['parentChapterNumber'] = !empty($parentChapter) ? $parentChapter['number'] : "";
        $lesson['chapterNumber']       = $chapter['number'];
        $lesson['chapterType']         = $chapter['type'];
        $lesson['lessonId']            = $conditions['lessonId'];
        $lesson['userId']              = $conditions['userId'];
        $conditions                    = array(
            "userId"   => $conditions['userId'],
            "courseId" => $conditions['courseId'],
            "lessonId" => $conditions['lessonId'],
            'ignoredWatchTime' => 0
        );

        $lessonViews = $this->getCourseService()->searchLessonView(
            $conditions,
            array('id', 'desc'),
            0,
            $this->getCourseService()->searchLessonViewCount($conditions)
        );

        $results = array();
        $params = array();
        foreach ($lessonViews as $key => $lessonView) {
           

            if ($lessonView['viewDevice'] == 'desktop') {
                $params[] = "PC";
            } else {
                $params[] = "移动设备";
            }

            $params[] = date('Y-m-d H:i:s', $lessonView['createdTime']);
            $params[] = date('Y-m-d H:i:s', $lessonView['createdTime'] + $lessonView['watchTime']);
            $params[] = $this->getCourseService()->durationTextFilter($lessonView['watchTime']);
            $results[] = $params;
            unset($params);
        }

        $header = array('学习设备','学习开始时间','学习结束时间','课时长度');       
        $filename = sprintf("用户%s对课时%s的详情明细%s.xls", $userProfile['truename'], $lesson['title'], date('Y-n-d'));
        \Custom\Common\Util\PHPExcelToolkit::outPutExcel($filename,$results,$header);
        exit();
    }

    protected function getCourseService()
    {
        return $this->getServiceKernel()->createService('Custom:Course.CourseService');
    }

    protected function getUserService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserService');
    }

    protected function getCertificateService()
    {
        return $this->getServiceKernel()->createService('Custom:Certificate.CertificateService');
    }

    protected function getFaceDetectResultService()
    {
        return $this->getServiceKernel()->createService('Custom:FaceDetectResult.FaceDetectResultService');
    }

    protected function getTestpaperService()
    {
        return $this->getServiceKernel()->createService('Custom:Testpaper.TestpaperService');
    }

    protected function getUserCertificationStatesService()
    {
        return $this->getServiceKernel()->createService('Custom:User.UserCertificationStatesService');
    }
}
