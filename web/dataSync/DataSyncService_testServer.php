<?php
class DataSyncService {

    function syncData($methodName, $params)
    {
        $clientIp = $_SERVER['REMOTE_ADDR'];
        $sendUrl = 'http://zhonghuan.dev11.edusoho.cn/data/sync/' . $methodName;

        $ch = curl_init();  
        $timeout = 5;  

        $header = array(
            'content-type: application/x-www-form-urlencoded; charset=UTF-8'
        );

        $paramStr= 'clientIp=' . $clientIp . '&data=' . $params;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramStr);
        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt ($ch, CURLOPT_URL, $sendUrl);  
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);  
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout); 

        $responseText = curl_exec($ch);  
        curl_close($ch); 

        return $responseText;
    }
}
